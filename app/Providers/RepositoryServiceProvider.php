<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(\App\Repositories\Systems\BranchRepository::class, \App\Repositories\Systems\BranchRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Users\UserRepository::class, \App\Repositories\Users\UserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Systems\PositionRepository::class, \App\Repositories\Systems\PositionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Systems\DepartmentRepository::class, \App\Repositories\Systems\DepartmentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Systems\CompanyRepository::class, \App\Repositories\Systems\CompanyRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Projects\TypeRepository::class, \App\Repositories\Projects\TypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Projects\ProjectRepository::class, \App\Repositories\Projects\ProjectRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Customers\LevelRepository::class, \App\Repositories\Customers\LevelRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Customers\CustomerRepository::class, \App\Repositories\Customers\CustomerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Customers\TypeCustomerRepository::class, \App\Repositories\Customers\TypeCustomerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Customers\HistoriesRepository::class, \App\Repositories\Customers\HistoriesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Projects\NewsCategoryRepository::class, \App\Repositories\Projects\NewsCategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Projects\NewsRepository::class, \App\Repositories\Projects\NewsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Permissions\RoleRepository::class, \App\Repositories\Permissions\RoleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Permissions\PermissionRepository::class, \App\Repositories\Permissions\PermissionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Projects\DocumentRepository::class, \App\Repositories\Projects\DocumentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Projects\DocumentCategoryRepository::class, \App\Repositories\Projects\DocumentCategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TaskRepository::class, \App\Repositories\TaskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Jobs\TaskRepository::class, \App\Repositories\Jobs\TaskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Jobs\TargetRepository::class, \App\Repositories\Jobs\TargetRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Jobs\ReportRepository::class, \App\Repositories\Jobs\ReportRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Jobs\CategoryTaskRepository::class, \App\Repositories\Jobs\CategoryTaskRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Jobs\TaskCategoryRepository::class, \App\Repositories\Jobs\TaskCategoryRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Education\ProgramRepository::class, \App\Repositories\Education\ProgramRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Users\TypeRepository::class, \App\Repositories\Users\TypeRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Education\QuestionRepository::class, \App\Repositories\Education\QuestionRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Education\ExamRepository::class, \App\Repositories\Education\ExamRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Education\SubjectRepository::class, \App\Repositories\Education\SubjectRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Educations\LessonRepository::class, \App\Repositories\Educations\LessonRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Projects\SegmentRepository::class, \App\Repositories\Projects\SegmentRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Education\AreaRepository::class, \App\Repositories\Education\AreaRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Education\ClassRoomRepository::class, \App\Repositories\Education\ClassRoomRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Jobs\PlanRepository::class, \App\Repositories\Jobs\PlanRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Users\EducationUserInfoRepository::class, \App\Repositories\Users\EducationUserInfoRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Permissions\GroupRoleRepository::class, \App\Repositories\Permissions\GroupRoleRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Education\ReportRepository::class, \App\Repositories\Education\ReportRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CareRepository::class, \App\Repositories\CareRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Customers\CustomerSharingRepository::class, \App\Repositories\Customers\CustomerSharingRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Customers\CustomerSharingUserRepository::class, \App\Repositories\Customers\CustomerSharingUserRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Customers\CustomerSharingLogRepository::class, \App\Repositories\Customers\CustomerSharingLogRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Projects\ProjectManagerRepository::class, \App\Repositories\Projects\ProjectManagerRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\Customers\AccountsRepository::class, \App\Repositories\Customers\AccountsRepositoryEloquent::class);
        //:end-bindings:
    }
}
