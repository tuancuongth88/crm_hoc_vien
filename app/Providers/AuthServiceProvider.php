<?php

namespace App\Providers;

use App\Models\Education\Exam;
use App\Models\Education\Lesson;
use App\Models\Education\Program;
use App\Models\Education\Question;
use App\Models\Education\Subject;
use App\Models\Jobs\Report;
use App\Models\Jobs\Target;
use App\Models\Jobs\Task;
use App\Models\Jobs\TaskCategory;
use App\Models\Projects\DocumentCategory;
use App\Models\Projects\News;
use App\Models\Projects\NewsCategory;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectManager;
use App\Models\Projects\Type;
use App\Policies\Educations\ExamPolicy;
use App\Policies\Educations\LessonPolicy;
use App\Policies\Educations\ProgramPolicy;
use App\Policies\Educations\QuestionPolicy;
use App\Policies\Educations\SubjectPolicy;
use App\Policies\Jobs\ReportPolicy;
use App\Policies\Jobs\TargetPolicy;
use App\Policies\Jobs\TaskCategoryPolicy;
use App\Policies\Jobs\TaskPolicy;
use App\Policies\Projects\DocumentCategoryPolicy;
use App\Policies\Projects\NewsCategoryPolicy;
use App\Policies\Projects\NewsPolicy;
use App\Policies\Projects\ProjectManagerPolicy;
use App\Policies\Projects\ProjectPolicy;
use App\Policies\Projects\TypePolicy;
use App\Policies\Users\EducationUserPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider {
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        News::class             => NewsPolicy::class,
        NewsCategory::class     => NewsCategoryPolicy::class,
        Report::class           => ReportPolicy::class,
        Type::class             => TypePolicy::class,
        Project::class          => ProjectPolicy::class,
        DocumentCategory::class => DocumentCategoryPolicy::class,
        Target::class           => TargetPolicy::class,
        TaskCategory::class     => TaskCategoryPolicy::class,
        Task::class             => TaskPolicy::class,

        //education
        Program::class          => ProgramPolicy::class,
        Subject::class          => SubjectPolicy::class,
        Lesson::class           => LessonPolicy::class,
        Question::class         => QuestionPolicy::class,
        Exam::class             => ExamPolicy::class,
        User::class             => EducationUserPolicy::class,
        ProjectManager::class   => ProjectManagerPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot() {
        $this->registerPolicies();
        Gate::resource('report', ReportPolicy::class);
        Gate::resource('report', ReportPolicy::class, [
            'approve' => 'approve',
            'list'    => 'list',
        ]);
        Gate::resource('news', NewsPolicy::class);
        Gate::resource('news', NewsPolicy::class, [
            'approve' => 'approve',
        ]);
        Gate::resource('task', TaskPolicy::class, [
            'rate' => 'rate',
        ]);
        Gate::resource('type', TypePolicy::class);
        Gate::resource('document-category', DocumentCategoryPolicy::class);
        Gate::resource('news-category', NewsCategoryPolicy::class);
        Gate::resource('project', ProjectPolicy::class);
        Gate::resource('target', TargetPolicy::class);
        Gate::resource('task-category', TaskCategoryPolicy::class);

        //education
        Gate::resource('program', ProgramPolicy::class);
        Gate::resource('subject', SubjectPolicy::class);
        Gate::resource('subject', SubjectPolicy::class, [
            'list.user.reg.subject'  => 'getListUserRegBySubject',
            'set.user.assessment'    => 'setAssessment',
            'list.lesson.by_subject' => 'getListLessonBySubject',
            'add.lesson.to_subject'  => 'addLessonToSubject',
            'remove.lesson.to_subject'  => 'removeLessonToSubject',
        ]);

        Gate::resource('lesson', LessonPolicy::class);
        Gate::resource('question', QuestionPolicy::class);
        Gate::resource('exam', ExamPolicy::class);
        Gate::resource('exam', ExamPolicy::class, [
            'import.point' => 'importPoint',
        ]);
        Gate::resource('education-user', EducationUserPolicy::class);

        Gate::resource('project-manager', ProjectManagerPolicy::class);
    }
}
