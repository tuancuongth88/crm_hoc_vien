<?php

namespace App\Notifications;

use App\Models\Customers\Accounts;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AppHelpdeskNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $account;
    public $title;
    public $content;
    public $subject;
    public function __construct($account, $title, $content, $subject = '')
    {
        $this->account = $account;
        $this->title = $title;
        $this->content = $content;
        $this->subject = $subject;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    public function toDatabase(){
        return [
            'title' => $this->title,
            'content'  => $this->content,
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $data['title'] = $this->title;
        $data['content'] = $this->content;
        $data['link'] =  route('user.read.notification', $this->id);
        $data['fullname'] = $this->account->fullname;
        if(!$this->subject){
            $this->subject = 'Hệ thống quản lý công việc';
        }
        return (new MailMessage)
            ->from('haiphattech.alert@gmail.com', $this->subject)
            ->greeting('Hi there,')
            ->subject($this->title)
            ->line($this->content)
            ->markdown('administrator.email.create-account', ['data' => $data]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
