<?php

namespace App\Notifications;

use App\Jobs\SendEmail;
use App\Models\Users\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Contracts\Bus\Dispatcher;

class CustomerNotifications extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $userId;
    public $title;
    public $content;
    public function __construct($userId, $title, $content)
    {
        $this->userId = $userId;
        $this->title = $title;
        $this->content = $content;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    public function toDatabase(){
        return [
            'title' => $this->title,
            'content'  => $this->content,
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = User::find($this->userId);
        $data['title'] = $this->title;
        $data['content'] = $this->content;
        $data['link'] =  route('user.read.notification', $this->id);
        $data['fullname'] = $user->fullname;

        return (new MailMessage)
            ->from('haiphattech.alert@gmail.com', 'Hệ thống quản lý công việc')
            ->greeting('Hi there,')
            ->subject($this->title)
            ->line($this->content)
            ->markdown('administrator.email.notification', ['data' => $data]);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [

        ];
    }
}
