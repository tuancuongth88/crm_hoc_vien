<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuddenlyTask extends Model {
    use SoftDeletes;
    const REPORT_ID   = 'report_id';
    const TASK        = 'task';
    const CATEGORY_ID = 'category_id';
    const LOCATION    = 'location';
    const DURATION    = 'duration';
    const ATTACHMENT  = 'attachment';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';
    const CREATED_AT  = 'created_at';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        self::REPORT_ID,
        self::CATEGORY_ID,
        self::TASK,
        self::LOCATION,
        self::DURATION,
        self::ATTACHMENT,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::TASK => trans('field.task'),
        ];
    }

    public function setCreatedByAttribute($value) {
        $this->attributes[self::CREATED_BY] = \Auth::user()->id;
    }

    public function hasFiles() {
        return $this->hasMany(File::class, File::TASK_SUDDENLY_ID, 'id');
    }

    public function getCreatedAtAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

    public function report() {
        return $this->belongsTo(Report::class, self::REPORT_ID, 'id');
    }

    public function category() {
        return $this->belongsTo(TaskCategory::class, self::CATEGORY_ID, 'id');
    }
}
