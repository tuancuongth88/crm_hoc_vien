<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attachment extends Model {
    use SoftDeletes;

    protected $table = 'report_attachments';

    const REPORT_ID   = 'report_id';
    const ORIGINAL_ID = 'original_id';
    const FIELD       = 'field';
    const SIZE        = 'size';
    const EXTENDSION  = 'extendsion';
    const URL         = 'url';
    const NAME        = 'name';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';
    const CATEGORY_ID = 'category_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::REPORT_ID,
        self::ORIGINAL_ID,
        self::FIELD,
        self::NAME,
        self::SIZE,
        self::EXTENDSION,
        self::URL,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME       => trans('field.name'),
            self::SIZE       => trans('field.size'),
            self::EXTENDSION => trans('field.extendsion'),
            self::URL        => trans('field.url'),
        ];
    }
}
