<?php

namespace App\Models\Jobs;

use App\Models\Jobs\SuddenlyTask;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class TaskCategory.
 *
 * @package namespace App\Models\Jobs;
 */
class TaskCategory extends Model implements Transformable {
    use TransformableTrait,
        SoftDeletes;

    const NAME        = 'name';
    const DESCRIPTION = 'description';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        self::NAME,
        self::DESCRIPTION,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::NAME        => trans('field.name'),
            self::DESCRIPTION => trans('field.description'),
        ];
    }

    public function hasTasks() {
        return $this->hasMany(SuddenlyTask::class, 'category_id', 'id');
    }
}
