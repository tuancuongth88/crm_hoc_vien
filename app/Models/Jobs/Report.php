<?php

namespace App\Models\Jobs;

use App\Models\Jobs\File;
use App\Models\Jobs\StabilityTask;
use App\Models\Jobs\SuddenlyTask;
use App\Models\Projects\InfoManager;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Report.
 *
 * @package namespace App\Models\Jobs;
 */
class Report extends Model implements Transformable {
    use TransformableTrait,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const IS_REPORT                   = 3;
    const NAME                        = 'name';
    const POST_WEBSITE                = 'post_website';
    const POST_WEBSITE_DETAIL         = 'post_website_detail';
    const POST_FACEBOOK               = 'post_facebook';
    const POST_FACEBOOK_DETAIL        = 'post_facebook_detail';
    const POST_NEWS                   = 'post_news';
    const POST_NEWS_DETAIL            = 'post_news_detail';
    const POST_FORUM                  = 'post_forum';
    const POST_FORUM_DETAIL           = 'post_forum_detail';
    const UPDATE_INFO_MARKET          = 'update_info_market';
    const UPDATE_INFO_PROJECT         = 'update_info_project';
    const FLYPAPER_PERSONAL           = 'flypaper_personal';
    const FLYPAPER_PERSONAL_TIME      = 'flypaper_personal_time';
    const FLYPAPER_GROUP              = 'flypaper_group';
    const FLYPAPER_GROUP_TIME         = 'flypaper_group_time';
    const QUESTIONAIR                 = 'questionair';
    const QUESTIONAIR_TIME            = 'questionair_time';
    const GUARD_PROJECT_IN_HOUR       = 'guard_project_in_hour';
    const GUARD_PROJECT_IN_HOUR_TIME  = 'guard_project_in_hour_time';
    const GUARD_PROJECT_OUT_HOUR      = 'guard_project_out_hour';
    const GUARD_PROJECT_OUT_HOUR_TIME = 'guard_project_out_hour_time';
    const OPEN_SALE                   = 'open_sale';
    const OPEN_SALE_TIME              = 'open_sale_time';
    const ROADSHOW                    = 'roadshow';
    const ROADSHOW_TIME               = 'roadshow_time';
    const EVENT                       = 'event';
    const EVENT_TIME                  = 'event_time';
    const FIND_PUBLIC                 = 'find_public';
    const FIND_PUBLIC_TIME            = 'find_public_time';
    const SETUP_APPOINTMENT           = 'setup_appointment';
    const SETUP_APPOINTMENT_TIME      = 'setup_appointment_time';
    const TOTAL                       = 'total';
    const TOTAL_TIME                  = 'total_time';
    const LIKE                        = 'like';
    const COMMENT                     = 'comment';
    const SHARE                       = 'share';
    const EVALUATE                    = 'evaluate';
    const CREATED_BY                  = 'created_by';
    const UPDATED_BY                  = 'updated_by';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        self::IS_REPORT,
        self::POST_WEBSITE,
        self::POST_WEBSITE_DETAIL,
        self::POST_FACEBOOK,
        self::POST_FACEBOOK_DETAIL,
        self::POST_NEWS,
        self::POST_NEWS_DETAIL,
        self::POST_FORUM,
        self::POST_FORUM_DETAIL,
        self::UPDATE_INFO_MARKET,
        self::UPDATE_INFO_PROJECT,
        self::FLYPAPER_PERSONAL,
        self::FLYPAPER_PERSONAL_TIME,
        self::FLYPAPER_GROUP,
        self::FLYPAPER_GROUP_TIME,
        self::QUESTIONAIR,
        self::QUESTIONAIR_TIME,
        self::GUARD_PROJECT_IN_HOUR,
        self::GUARD_PROJECT_IN_HOUR_TIME,
        self::GUARD_PROJECT_OUT_HOUR,
        self::GUARD_PROJECT_OUT_HOUR_TIME,
        self::OPEN_SALE,
        self::OPEN_SALE_TIME,
        self::ROADSHOW,
        self::ROADSHOW_TIME,
        self::EVENT,
        self::EVENT_TIME,
        self::FIND_PUBLIC,
        self::FIND_PUBLIC_TIME,
        self::SETUP_APPOINTMENT,
        self::SETUP_APPOINTMENT_TIME,
        self::TOTAL,
        self::TOTAL_TIME,
        self::LIKE,
        self::COMMENT,
        self::SHARE,
        self::EVALUATE,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::NAME,
        self::EVALUATE,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }

    public function setCreatedByAttribute($value) {
        $this->attributes[self::CREATED_BY] = \Auth::user()->id;
    }

    public function getCreatedBy() {
        return $this->hasOne(User::class, 'id', self::CREATED_BY)->where('active', ONE);
    }

    public function getUpdatedBy() {
        return $this->hasOne(User::class, 'id', self::UPDATED_BY)->where('active', ONE);
    }

    /*public function hasStabilityTasks() {
    return $this->hasMany(StabilityTask::class, StabilityTask::REPORT_ID, 'id');
    }

    public function hasSuddenlyTasks() {
    return $this->hasMany(SuddenlyTask::class, SuddenlyTask::REPORT_ID, 'id');
    }

    public static function saveRelateStability($array, $obj) {
    $dataRelated = [];
    foreach ($array as $input) {
    if (is_null($input[StabilityTask::QUANTITY])) {
    continue;
    }
    $dataRelated[] = new StabilityTask([
    StabilityTask::TASK      => $input[StabilityTask::TASK],
    StabilityTask::QUANTITY  => $input[StabilityTask::QUANTITY],
    StabilityTask::REPORT_ID => $obj->id,
    self::CREATED_BY         => $obj->created_by,
    ]);
    }
    if (count($dataRelated) >= ZERO) {
    $obj->hasStabilityTasks()->saveMany($dataRelated);
    }
    return true;
    }

    public static function saveRelateSuddenly($array, $obj) {
    $dataRelated = [];
    foreach ($array as $input) {
    if (is_null($input[SuddenlyTask::TASK])) {
    continue;
    }
    $dataTask                            = [];
    $fileRelated                         = [];
    $dataTask[SuddenlyTask::REPORT_ID]   = $obj->id;
    $dataTask[SuddenlyTask::CATEGORY_ID] = (isset($input[SuddenlyTask::CATEGORY_ID])) ? $input[SuddenlyTask::CATEGORY_ID] : ZERO;
    $dataTask[SuddenlyTask::TASK]        = $input[SuddenlyTask::TASK];
    $dataTask[SuddenlyTask::LOCATION]    = $input[SuddenlyTask::LOCATION];
    $dataTask[SuddenlyTask::DURATION]    = $input[SuddenlyTask::DURATION];
    $dataTask[SuddenlyTask::CREATED_BY]  = $obj->created_by;
    $task                                = SuddenlyTask::create($dataTask);
    if (isset($input['file_id[']) && count($input['file_id[']) > ZERO) {
    $files = File::whereIn('id', $input['file_id['])->update([File::TASK_SUDDENLY_ID => $task->id]);
    }
    $attachment = [];
    if (isset($input['attachment'])) {
    foreach ($input['attachment'] as $file) {
    $attachment[File::FILE]             = $file->getClientOriginalName();
    $attachment[File::SIZE]             = $file->getSize();
    $attachment[File::EXTENDSION]       = $file->getClientOriginalExtension();
    $attachment[File::TASK_SUDDENLY_ID] = $task->id;
    $attachment[self::CREATED_BY]       = $obj->created_by;
    if ($file->isValid()) {
    $attachment[File::URL] = uploadFile($file, JOB_ATTACHMENT);
    }
    $fileRelated[] = new File($attachment);
    }
    }
    if (count($fileRelated) > ZERO) {
    $task->hasFiles()->saveMany($fileRelated);
    }
    }
    return true;
    }*/

    public static $rateLevel = [
        'unrate' => 'Đánh giá',
        'good'   => 'Tốt',
        'medium' => 'Trung bình',
        'weak'   => 'Yếu',
    ];

    public function getCreatedAtAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

    public static function saveRelateInfoManager($array, $obj) {
        $dataRelated = [];
        foreach ($array as $input) {
            if($input[InfoManager::NAME] && $input[InfoManager::EMAIL] && $input[InfoManager::PHONE]){
                $dataRelated[] = new InfoManager([
                    InfoManager::NAME       => $input[InfoManager::NAME],
                    InfoManager::EMAIL      => $input[InfoManager::EMAIL],
                    InfoManager::PHONE      => $input[InfoManager::PHONE],
                    InfoManager::MODEL_NAME => static::class,
                    InfoManager::MODEL_ID   => $obj->id,
                    // InfoManager::PROJECT_ID => $obj->id,
                    InfoManager::TYPE       => self::IS_REPORT,
                    // self::CREATED_BY        => $obj->created_by,
                ]);
            }
        }
        if (count($dataRelated) >= ZERO) {
            $obj->hasInfoManager()->saveMany($dataRelated);
        }
        return true;
    }

    public function hasInfoManager() {
        return $this->hasMany(InfoManager::class, InfoManager::MODEL_ID, 'id')->where(InfoManager::MODEL_NAME, static::class)->where(InfoManager::TYPE, self::IS_REPORT);
    }

    public static function saveRelateAttachment($array, $obj) {
        $dataRelated = [];
        foreach ($array as $input) {
            $dataRelated[] = new Attachment([
                Attachment::NAME        => $input->name,
                Attachment::SIZE        => $input->size,
                Attachment::EXTENDSION  => $input->extendsion,
                Attachment::URL         => $input->url,
                Attachment::FIELD       => $input->type,
                Attachment::ORIGINAL_ID => $input->id,
                Attachment::REPORT_ID   => $obj->id,
                self::CREATED_BY        => $obj->created_by,
            ]);
        }
        if (count($dataRelated) >= ZERO) {
            $obj->hasAttachment()->saveMany($dataRelated);
        }
        return true;
    }

    public function hasAttachment() {
        return $this->hasMany(Attachment::class, Attachment::REPORT_ID, 'id');
    }

}
