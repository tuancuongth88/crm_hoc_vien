<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StabilityTask extends Model {
    use SoftDeletes;

    const REPORT_ID  = 'report_id';
    const TASK       = 'task';
    const QUANTITY   = 'quantity';
    const CREATED_AT = 'created_at';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        self::REPORT_ID,
        self::TASK,
        self::QUANTITY,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::TASK => trans('field.task'),
        ];
    }

    public function setCreatedByAttribute($value) {
        $this->attributes[self::CREATED_BY] = \Auth::user()->id;
    }
}
