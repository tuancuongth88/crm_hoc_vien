<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Task.
 *
 * @package namespace App\Models\Jobs;
 */
class Task extends Model implements Transformable {
    use TransformableTrait,
        SoftDeletes;

    const TITLE       = 'title';
    const CATEGORY_ID = 'category_id';
    const DESCRIPTION = 'description';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        self::TITLE,
        self::CATEGORY_ID,
        self::DESCRIPTION,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::TITLE       => trans('field.title'),
            self::DESCRIPTION => trans('field.description'),
            self::CATEGORY_ID => trans('field.category_id'),
        ];
    }

    public function setCreatedByAttribute($value) {
        $this->attributes[self::CREATED_BY] = \Auth::user()->id;
    }

    public static function getTaskStatic() {
        // return [
        //     'fa-headphones'      => 'Telesale',
        //     'fa-pencil-square-o' => 'Đăng tin rao vặt',
        //     'fa-envelope-o'      => 'Email',
        //     'fa-comments'        => 'SMS',
        //     'fa-file-text-o'     => 'Content',
        // ];
        return [
            'telesale' => ['icon' => 'fa-headphones', 'name' => 'Telesale'],
            'post'     => ['icon' => 'fa-pencil-square-o', 'name' => 'Đăng tin rao vặt'],
            'email'    => ['icon' => 'fa-envelope-o', 'name' => 'Email'],
            'content'  => ['icon' => 'fa-file-text-o', 'name' => 'Content'],
            'sms'      => ['icon' => 'fa-comments', 'name' => 'SMS'],
        ];

    }
}
