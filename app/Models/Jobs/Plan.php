<?php

namespace App\Models\Jobs;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Plan.
 *
 * @package namespace App\Models\Jobs;
 */
class Plan extends Model implements Transformable {
    use TransformableTrait;
    use SoftDeletes;

    protected $table = 'plans';

    const TITLE       = 'title';
    const DESCRIPTION = 'description';
    const DATA        = 'data';
    const MONTH       = 'month';
    const YEAR        = 'year';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::TITLE,
        self::DESCRIPTION,
        self::DATA,
        self::MONTH,
        self::YEAR,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    protected $casts = [
        'data' => 'json',
    ];

}
