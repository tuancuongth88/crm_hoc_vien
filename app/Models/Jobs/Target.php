<?php

namespace App\Models\Jobs;

use App\Models\Jobs\Task;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Target.
 *
 * @package namespace App\Models\Jobs;
 */
class Target extends Model implements Transformable {
    use TransformableTrait;

    const TASK       = 'task';
    const TARGET     = 'target';
    const DAY        = 'day';
    const MONTH      = 'month';
    const YEAR       = 'year';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        self::TASK,
        self::TARGET,
        self::DAY,
        self::MONTH,
        self::YEAR,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::TARGET => trans('field.target'),
        ];
    }

    public function setDayAttribute($value) {
        $this->attributes[self::DAY] = \Carbon\Carbon::parse($value);
    }

    public function setCreatedByAttribute($value) {
        $this->attributes[self::CREATED_BY] = \Auth::user()->id;
    }

    public function belongToTask() {
        return $this->hasOne(Task::class, 'id', self::TASK);
    }

    public function getDayAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

}
