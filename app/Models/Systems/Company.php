<?php

namespace App\Models\Systems;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Company.
 *
 * @package namespace App\Models\Systems;
 */
class Company extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;

    protected $table = 'companies';
    protected $dates = ['deleted_at'];

    const NAME          = 'name';
    const DESCRIPTION   = 'description';
    const TAX           = 'tax';
    const ADDRESS       = 'address';
    const PHONE         = 'phone';
    const LOGO          = 'logo';
    const CREATED_BY    = 'created_by';
    const UPDATED_BY    = 'updated_by';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::DESCRIPTION,
        self::TAX,
        self::ADDRESS,
        self::PHONE,
        self::LOGO,
        self::CREATED_BY,
        self::UPDATED_BY
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
            self::TAX => trans('field.tax'),
            self::PHONE => trans('field.phone'),
            self::ADDRESS => trans('field.address'),
        ];
    }

    public function setCreatedByAttribute($value) {
        $this->attributes[self::CREATED_BY] = Auth::user()->id;
    }

    public function setUpdatedByAttribute($value) {
        $this->attributes[self::UPDATED_BY] = Auth::user()->id;
    }

    public function branchs(){
        return $this->hasMany(Branch::class, 'company_id', 'id');
    }
    public function departments(){
        return $this->hasMany(Department::class, 'company_id', 'id');
    }

}
