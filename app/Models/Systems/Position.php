<?php

namespace App\Models\Systems;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Position.
 *
 * @package namespace App\Models\Systems;
 */
class Position extends Model implements Transformable {
    use TransformableTrait,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    const NAME       = 'name';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const COMPANY_ID = 'company_id';

    protected $fillable = [
        self::NAME,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::COMPANY_ID,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }

    public function setCreatedByAttribute($value) {
        $this->attributes[self::CREATED_BY] = \Auth::user()->id;
    }

}
