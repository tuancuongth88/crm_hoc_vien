<?php

namespace App\Models\Systems;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Department.
 *
 * @package namespace App\Models\Systems;
 */
class Department extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'departments';

    const NAME = 'name';
    const DESCRIPTION = 'description';
    const COMPANY_ID = 'company_id';
    const BRANCH_ID = 'branch_id';

    public $fillable = [
        self::NAME,
        self::DESCRIPTION,
        self::COMPANY_ID,
        self::BRANCH_ID,
    ];

    protected $dates = ['deleted_at'];

    public function scopeCompany($query, $company_id) {
        return $query->where('company_id', $company_id);
    }

    public static function getFieldVietnamese() {
        return [
            self::NAME         => trans('field.name'),
            self::COMPANY_ID    => trans('field.company_id'),
            self::BRANCH_ID    => trans('field.branch_id'),
        ];
    }

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function branch(){
        return $this->belongsTo(Branch::class);
    }
}
