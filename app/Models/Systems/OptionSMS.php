<?php

namespace App\Models\Systems;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OptionSMS extends Model
{
    use SoftDeletes;

    const NAME      = 'name';
    const CONTENT   = 'content';
    const TYPE      = 'type';

    const TYPE_REG = 1;
    const TYPE_ADD_TRANSACTION = 2;
    const TYPE_BIRTHDAY = 3;
    const TYPE_HAPPY_NEW_YEAR = 4;
    const TYPE_8_3 = 5;
    const TYPE_20_10 = 6;
    const TYPE_ORTHER = 0;

    public $table   = 'options_sms';

    public $dates = ['deleted_at'];

    public $fillable = ['name', 'message', 'type'];

    public static $listType = [
        self::TYPE_REG => 'Tin tạo tài khoản',
        self::TYPE_ADD_TRANSACTION => 'Tin thêm mới giao dịch',
        self::TYPE_BIRTHDAY => 'Tin chúc mừng sinh nhật',
        self::TYPE_HAPPY_NEW_YEAR => 'Tin chúc mừng năm mới',
        self::TYPE_8_3 => 'Tin chúc mừng 8-3',
        self::TYPE_20_10 => 'Tin chúc mừng 20-10',
        self::TYPE_ORTHER => 'Khác',
    ];
}
