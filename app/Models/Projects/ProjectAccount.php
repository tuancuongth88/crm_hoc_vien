<?php

namespace App\Models\Projects;

use App\Models\Customers\Accounts;
use App\Models\Users\AccountTransactions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProjectAccount extends Model
{
    use SoftDeletes;
    const PROJECT_ID    = 'project_id';
    const ACCOUNT_ID    = 'account_id';
    const RANK_PROJECT  = 'rank_project';
    const CREATED_BY    = 'created_by';
    const UPDATED_BY    = 'updated_by';

    public $fillable    = [self::PROJECT_ID, self::ACCOUNT_ID, self::RANK_PROJECT, self::CREATED_BY, self::UPDATED_BY];

    public $table       = 'account_projects';

    protected $dates = ['deleted_at'];

    public function account(){
        return $this->belongsTo(Accounts::class, 'account_id');
    }

    public function project(){
        return $this->belongsTo(ProjectManager::class, 'project_id');
    }

    public function totalHouse(){
        return $this->hasMany(AccountTransactions::class, 'project_id', 'project_id')
                    ->where('account_id', $this->account_id)
                    ->where('parent_id', 0);
    }
}
