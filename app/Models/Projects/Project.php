<?php

namespace App\Models\Projects;

use App\Models\Projects\Advantages;
use App\Models\Projects\Attachment;
use App\Models\Projects\Defect;
use App\Models\Projects\District;
use App\Models\Projects\Province;
use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Project.
 *
 * @package namespace App\Models\Projects;
 */
class Project extends Model implements Transformable {
    use TransformableTrait;

    const NAME                 = 'name';
    const INVESTOR             = 'investor';
    const TYPE                 = 'type';
    const ADDRESS              = 'address';
    const DISTRICT_ID          = 'district_id';
    const CITY_ID              = 'city_id';
    const CONTENT              = 'content';
    const AREA_POSITION        = 'area_position';
    const UTILITIE_LOCAL       = 'utilitie_local';
    const AREA_CONNECT         = 'area_connect';
    const AREA_GROUND          = 'area_ground';
    const IMAGE_URL            = 'image_url';
    const MALE                 = 'male';
    const FEMALE               = 'female';
    const SEGMENT              = 'segment';
    const CUSTOMER_AGE_FROM    = 'customer_age_from';
    const CUSTOMER_AGE_TO      = 'customer_age_to';
    const CUSTOMER_JOB         = 'customer_job';
    const CUSTOMER_DESCRIPTION = 'customer_description';
    const CREATED_BY           = 'created_by';
    const UPDATED_BY           = 'updated_by';
    const COMPANY_ID           = 'company_id';
    const FINANCE              = 'finance';
    const PROFILE              = 'profile';
    const SALEKIT              = 'salekit';
    const QA                   = 'qa';
    const CONTRACT             = 'contract';
    const POLICY               = 'policy';
    const CUSTOMER_REQUIRE     = 'customer_require';
    const CUSTOMER_HOBBY       = 'customer_hobby';
    const CUSTOMER_LEVEL       = 'customer_level';
    const CUSTOMER_AREA        = 'customer_area';
    const HAS_HOUSE            = 'has_house';
    const PROJECT_MANAGER_ID   = 'project_manager_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        self::NAME,
        self::INVESTOR,
        self::TYPE,
        self::ADDRESS,
        self::DISTRICT_ID,
        self::CITY_ID,
        self::CONTENT,
        self::AREA_POSITION,
        self::UTILITIE_LOCAL,
        self::AREA_CONNECT,
        self::AREA_GROUND,
        // self::IMAGE_URL,
        self::MALE,
        self::FEMALE,
        self::CUSTOMER_AGE_FROM,
        self::CUSTOMER_AGE_TO,
        self::CUSTOMER_JOB,
        self::CUSTOMER_DESCRIPTION,
        self::SEGMENT,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::COMPANY_ID,
        self::FINANCE,
        self::PROFILE,
        self::SALEKIT,
        self::QA,
        self::CONTRACT,
        self::POLICY,
        self::CUSTOMER_REQUIRE,
        self::CUSTOMER_HOBBY,
        self::CUSTOMER_LEVEL,
        self::CUSTOMER_AREA,
        self::HAS_HOUSE,
        self::PROJECT_MANAGER_ID,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME      => trans('field.name'),
            self::IMAGE_URL => trans('field.image_url'),
            self::CONTENT   => trans('field.content'),
            self::ADDRESS   => trans('field.address'),
        ];
    }

    public function setCreatedByAttribute($value) {
        $this->attributes[self::CREATED_BY] = Auth::user()->id;
    }

    public function typeProject() {
        return $this->belongsTo(Type::class, 'type', 'id');
    }

    public function province() {
        return $this->belongsTo(Province::class, 'city_id', 'city_id');
    }

    public function district() {
        return $this->belongsTo(District::class, 'district_id', 'district_id');
    }


    public static function saveRelateInfoManager($array, $obj, $type) {
        $dataRelated = [];
        foreach ($array as $input) {
            $dataRelated[] = new InfoManager([
                InfoManager::NAME       => $input[InfoManager::NAME],
                InfoManager::EMAIL      => $input[InfoManager::EMAIL],
                InfoManager::PHONE      => $input[InfoManager::PHONE],
                InfoManager::MODEL_NAME => static::class,
                InfoManager::MODEL_ID   => $obj->id,
                // InfoManager::PROJECT_ID => $obj->id,
                InfoManager::TYPE       => $type,
                // self::CREATED_BY        => $obj->created_by,
            ]);
        }
        if (count($dataRelated) >= ZERO) {
            $obj->hasInfoManager()->saveMany($dataRelated);
        }
        return true;
    }

    public function hasInfoManager() {
        return $this->hasMany(InfoManager::class, InfoManager::MODEL_ID, 'id')->where(InfoManager::MODEL_NAME, static::class);
        // return $this->hasMany(InfoManager::class, InfoManager::PROJECT_ID, 'id');
    }

    public static function getTableName() {
        return with(new static )->getTable();
    }

    public function projectManager(){
        return $this->hasOne(ProjectManager::class, 'project_manager_id', 'id');
    }

}
