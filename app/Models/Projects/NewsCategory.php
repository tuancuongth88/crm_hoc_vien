<?php

namespace App\Models\Projects;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class NewsCategory.
 *
 * @package namespace App\Models\Projects;
 */
class NewsCategory extends Model implements Transformable {
    use TransformableTrait,
    Sluggable,
        SoftDeletes;

    const NAME       = 'name';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    //id = 3 la tin khuyen mai tinh theo migrate
    const NEW_PROMOTION = 3;

    protected $table = 'projects_news_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => self::NAME,
            ],
        ];
    }

}
