<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class Province extends Model {

    public $fillable = ['city_id', 'name'];
    public $table = 'provinces';

    public function district(){
        return $this->hasMany(District::class, 'city_id', 'city_id');
    }
}
