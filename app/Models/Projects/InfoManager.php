<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

class InfoManager extends Model {
    protected $table = 'info_managers';

    const NAME       = 'name';
    const EMAIL      = 'email';
    const PHONE      = 'phone';
    const MODEL_NAME = 'model_name';
    const MODEL_ID   = 'model_id';
    // const PROJECT_ID  = 'project_id';
    const TYPE        = 'type';
    const IS_ADMIN    = 1;
    const IS_DIRECTOR = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::PHONE,
        self::NAME,
        self::EMAIL,
        // self::PROJECT_ID,
        self::TYPE,
        self::MODEL_NAME,
        self::MODEL_ID,
    ];

}
