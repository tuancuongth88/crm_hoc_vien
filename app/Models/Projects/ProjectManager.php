<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ProjectManager.
 *
 * @package namespace App\Models;
 */
class ProjectManager extends Model implements Transformable
{
    use TransformableTrait,SoftDeletes;

    const NAME           = 'name';
    const DESCRIPTION    = 'description';
    const IMAGE_URL      = 'image_url';
    const EMAIL          = 'email';
    const HOTLINE        = 'hotline';

    public $table = 'project_managers';

    protected $fillable = [
        self::NAME,
        self::DESCRIPTION,
        self::IMAGE_URL,
        self::EMAIL,
        self::HOTLINE,
    ];

}
