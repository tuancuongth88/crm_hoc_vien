<?php

namespace App\Models\Projects;

use App\Models\Users\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class News.
 *
 * @package namespace App\Models\Projects;
 */
class News extends Model implements Transformable {
    use TransformableTrait,
    Sluggable,
        SoftDeletes;

    const TITLE            = 'title';
    const DESCRIPTION      = 'description';
    const IMAGE_URL        = 'image_url';
    const URL              = 'url';
    const CONTENT          = 'content';
    const TYPE             = 'type';
    const CATEGORY_ID      = 'category_id';
    const IS_APPROVE       = 'is_approve';
    const CREATED_BY       = 'created_by';
    const UPDATED_BY       = 'updated_by';
    const COMPANY_ID       = 'company_id';
    const IS_COMMENT       = 'is_comment';
    const TITLE_META       = 'title_meta';
    const DESCRIPTION_META = 'description_meta';
    const KEYWORD_META     = 'keyword_meta';
    const SEND_AT          = 'send_at';
    const AUTHOR_ID        = 'author_id';
    const PROJECT_ID       = 'project_id';

    protected $table = 'projects_news';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::TITLE,
        self::DESCRIPTION,
        self::IMAGE_URL,
        self::URL,
        self::CONTENT,
        self::TYPE,
        self::CATEGORY_ID,
        self::IS_APPROVE,
        self::CREATED_BY,
        self::COMPANY_ID,
        self::IS_COMMENT,
        self::TITLE_META,
        self::DESCRIPTION_META,
        self::KEYWORD_META,
        self::SEND_AT,
        self::AUTHOR_ID,
        self::PROJECT_ID,
    ];

    public static function getFieldVietnamese() {
        return [
            self::TITLE       => trans('field.title'),
            self::DESCRIPTION => trans('field.description'),
            self::IMAGE_URL   => trans('field.image_url'),
            self::CONTENT     => trans('field.content'),
            self::TYPE        => trans('field.type'),
            self::CATEGORY_ID => trans('field.category_id'),
            self::CREATED_BY  => trans('field.created_by'),
            self::COMPANY_ID  => trans('field.company_id'),
            self::IS_COMMENT  => trans('field.is_comment'),
            self::SEND_AT     => trans('field.send_at'),
            self::AUTHOR_ID   => trans('field.author_id'),
            self::PROJECT_ID  => trans('field.project_id'),
        ];
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => self::TITLE,
            ],
        ];
    }

    public function author() {
        return $this->hasOne(User::class, 'id', self::CREATED_BY)->where('active', ONE);
    }

    protected $dates = ['deleted_at'];

    public function setSendAtAttribute($value) {
        $this->attributes[self::SEND_AT] = \Carbon\Carbon::parse($value);
    }

    public function getSendAtAttribute($value) {
        return date('d-m-Y H:i', strtotime($value));
    }

    public function scopeApprove($query) {
        return $query->where(self::IS_APPROVE, self::ONE);
    }

    public function scopeSlug($query, $slug) {
        return $query->where('slug', $slug);
    }

    public function categories(){
        return $this->belongsTo(NewsCategory::class, self::CATEGORY_ID);
    }

    public function project(){
        return $this->belongsTo(ProjectManager::class, self::PROJECT_ID);
    }
}
