<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Document.
 *
 * @package namespace App\Models\Projects;
 */
class Document extends Model implements Transformable {
    use TransformableTrait;

    const NAME        = 'name';
    const SIZE        = 'size';
    const EXTENDSION  = 'extendsion';
    const URL         = 'url';
    const FILE        = 'file';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';
    const CATEGORY_ID = 'category_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::SIZE,
        self::EXTENDSION,
        self::URL,
        self::CATEGORY_ID,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME        => trans('field.name'),
            self::SIZE        => trans('field.size'),
            self::EXTENDSION  => trans('field.extendsion'),
            self::URL         => trans('field.url'),
            self::CATEGORY_ID => trans('field.category_id'),
        ];
    }

    protected $table = 'project_documents';

    public function author() {
        return $this->hasOne(DocumentCategory::class, 'id', self::CATEGORY_ID);
    }

    public static $allowExtendsion = ['jpg', 'jpeg', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx', 'png', 'pdf'];
}
