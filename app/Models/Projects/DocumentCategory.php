<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class DocumentCategory.
 *
 * @package namespace App\Models\Projects;
 */
class DocumentCategory extends Model implements Transformable {
    use TransformableTrait,
        SoftDeletes;

    const NAME       = 'name';
    const PARENT_ID  = 'parent_id';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::PARENT_ID,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }

    protected $table = 'project_document_categories';

    public function files() {
        return $this->hasMany(Document::class, 'category_id', 'id');
    }

    public function getCreatedAtAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }
}
