<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

// class Attachment extends Model {

//     protected $table = 'project_attachments';

//     const NAME       = 'name';
//     const SIZE       = 'size';
//     const EXTENDSION = 'extendsion';
//     const URL        = 'url';
//     const FILE       = 'file';
//     const CREATED_BY = 'created_by';
//     const UPDATED_BY = 'updated_by';
//     const PROJECT_ID = 'project_id';

//     /**
//      * The attributes that are mass assignable.
//      *
//      * @var array
//      */
//     protected $fillable = [
//         self::NAME,
//         self::SIZE,
//         self::EXTENDSION,
//         self::URL,
//         self::PROJECT_ID,
//         self::CREATED_BY,
//         self::UPDATED_BY,
//     ];

//     public static function getFieldVietnamese() {
//         return [
//             self::NAME       => trans('field.name'),
//             self::SIZE       => trans('field.size'),
//             self::EXTENDSION => trans('field.extendsion'),
//             self::URL        => trans('field.url'),
//             self::PROJECT_ID => trans('field.project_id'),
//         ];
//     }

// }
