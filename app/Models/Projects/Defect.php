<?php

namespace App\Models\Projects;

use Illuminate\Database\Eloquent\Model;

// class Defect extends Model {

//     protected $table = 'project_defects';

//     const WEAKNESS   = 'weakness';
//     const RESOLVE    = 'resolve';
//     const PROJECT_ID = 'project_id';
//     const CREATED_BY = 'created_by';
//     const UPDATED_BY = 'updated_by';

//     /**
//      * The attributes that are mass assignable.
//      *
//      * @var array
//      */
//     protected $fillable = [
//         self::PROJECT_ID,
//         self::WEAKNESS,
//         self::RESOLVE,
//         self::CREATED_BY,
//         self::UPDATED_BY,
//     ];

//     public static function getFieldVietnamese() {
//         return [
//             self::WEAKNESS => trans('field.weakness'),
//             self::RESOLVE  => trans('field.resolve'),
//         ];
//     }
// }
