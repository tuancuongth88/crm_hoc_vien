<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SammyK\LaravelFacebookSdk\SyncableGraphNodeTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class EducationUserInfo.
 *
 * @package namespace App\Models;
 */
class EducationUserInfo extends Model implements Transformable
{
    use TransformableTrait,
        Notifiable,
        SyncableGraphNodeTrait;
    use EntrustUserTrait {
        restore as private restoreA;
    }
    use SoftDeletes {
        restore as private restoreB;
    }

    public function restore()
    {
        $this->restoreA();
        $this->restoreB();
    }


    protected $table = 'education_user_info';
    protected $primaryKey = 'id';

    const ONE = 1;
    const ZERO = 0;
    const TABLE = 'education_user_info';
    const USER_ID = 'user_id';
    const HOBBY = 'hobby';
    const TARGET = 'target';
    const ADVANTAGE = 'advantage';
    const DEFECT = 'defect';
    const MARRIAGE = 'marriage';
    const IDENTITY_DATE = 'identity_date';
    const IDENTITY_PLACE_RELEASE = 'identity_place_release';
    const LITERACY = 'literacy';
    const LITERACY_PARAM = 'literacy_param';
    const EXPERIENCED_PARAM = 'experienced_param';
    const SKYPE = 'skype';
    const ZALO = 'zalo';
    const VIBER = 'viber';
    const SKILL_TEACHER = 'skill_teacher';

    const MARRIAGE_0 = 0;
    const MARRIAGE_1 = 1;

    const LITERACY_1 = 1;
    const LITERACY_2 = 2;
    const LITERACY_3 = 3;
    const LITERACY_4 = 4;
    const LITERACY_5 = 5;
    const LITERACY_6 = 6;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::USER_ID,
        self::HOBBY,
        self::TARGET,
        self::ADVANTAGE,
        self::DEFECT,
        self::MARRIAGE,
        self::IDENTITY_DATE,
        self::IDENTITY_PLACE_RELEASE,
        self::LITERACY,
        self::LITERACY_PARAM,
        self::EXPERIENCED_PARAM,
        self::SKYPE,
        self::ZALO,
        self::VIBER,
        self::SKILL_TEACHER,
    ];

    public static function getFieldVietnamese()
    {
        return [
            self::HOBBY => trans('field.hobby'),
            self::ADVANTAGE => trans('field.hobby'),
            self::DEFECT => trans('field.defect'),
            self::SKYPE => trans('field.skype'),
            self::ZALO => trans('field.zalo'),
            self::VIBER => trans('field.viber'),
            self::MARRIAGE => trans('field.marriage'),
            self::IDENTITY_DATE => trans('field.identity_date'),
            self::IDENTITY_PLACE_RELEASE => trans('field.identity_place_release'),
        ];
    }

    public static $listMarriage = [
        self::MARRIAGE_0 => 'Chưa',
        self::MARRIAGE_1 => 'Có',
    ];
    public static $listLiteracy = [
        self::LITERACY_1 => 'Trên đại học',
        self::LITERACY_2 => 'Đại học',
        self::LITERACY_3 => 'Cao đẳng',
        self::LITERACY_4 => 'Trung cấp',
        self::LITERACY_5 => 'Trung học',
        self::LITERACY_6 => 'Chứng chỉ nghề',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
