<?php

namespace App\Models\Users;

use App\Models\Permissions\Role;
use App\Models\Systems\Branch;
use App\Models\Systems\Company;
use App\Models\Systems\Department;
use App\Models\Systems\Position;
use Hash;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use SammyK\LaravelFacebookSdk\SyncableGraphNodeTrait;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * Class User.
 *
 * @package namespace App\Models\Users;
 */
class User extends Authenticatable implements Transformable {
    use TransformableTrait,
    Notifiable,
        SyncableGraphNodeTrait;
    use EntrustUserTrait {restore as private restoreA;}
    use SoftDeletes {restore as private restoreB;}

    public $timestamps = true;

    public function restore() {
        $this->restoreA();
        $this->restoreB();
    }

    const ONE               = 1;
    const ZERO              = 0;
    const TABLE             = 'users';
    const FULLNAME          = 'fullname';
    const USERNAME          = 'username';
    const EMAIL             = 'email';
    const PHONE             = 'phone';
    const DEPARTMENT_ID     = 'department_id';
    const BRANCH_ID         = 'branch_id';
    const POSITION_ID       = 'position_id';
    const CREATE_BY         = 'create_by';
    const UPDATED_BY        = 'updated_by';
    const AVATAR            = 'avatar';
    const BIRTHDAY          = 'birthday';
    const COMPANY_ID        = 'company_id';
    const GENDER            = 'gender';
    const ADDRESS           = 'address';
    const ACTIVE            = 'active';
    const PASSWORD          = 'password';
    const CURRENT           = 'current';
    const DELETED           = 'deleted';
    const ITEMS             = 'items';
    const TYPE              = 'type';
    const IDENTITY          = 'identity';
    const PROVIDER          = 'provider';
    const PROVIDER_ID       = 'provider_id';
    const PARENT_ID         = 'parent_id';
    const MALE              = 1;
    const FEMALE            = 2;
    const WEBSITE           = 'website';
    const FACEBOOK          = 'facebook';
    const TWITTER           = 'twitter';
    const INSTAGRAM         = 'instagram';
    const GOOGLE            = 'google';
    const TOTAL_REG_PROGRAM = 'total_reg_program';

//    const TYPE_ADMIN    = 1;
    //    const TYPE_TEACHER  = 2;
    //    const TYPE_SALE     = 3;
    //    const TYPE_STUDENT  = 4;

    //nhân viên thử việc
    const TYPE_TRIAL_PERIOD = 1;
    const TYPE_OFFICIAL_PERIOD = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::USERNAME,
        self::FULLNAME,
        self::EMAIL,
        self::PASSWORD,
        self::ADDRESS,
        self::BRANCH_ID,
        self::DEPARTMENT_ID,
        self::POSITION_ID,
        self::BIRTHDAY,
        self::GENDER,
        self::PHONE,
        self::AVATAR,
        self::IDENTITY,
        self::PROVIDER,
        self::PROVIDER_ID,
        self::ACTIVE,
        self::PARENT_ID,
        self::WEBSITE,
        self::FACEBOOK,
        self::TWITTER,
        self::INSTAGRAM,
        self::GOOGLE,
        self::TOTAL_REG_PROGRAM,
        self::TYPE,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PASSWORD, 'remember_token',
    ];

    public function routeNotificationForMail() {
        return $this->email;
    }

    public static function getFieldVietnamese() {
        return [
            self::EMAIL    => trans('field.email'),
            self::PASSWORD => trans('field.password'),
            self::PHONE    => trans('field.phone'),
            self::BIRTHDAY => trans('field.birthday'),
            self::FULLNAME => trans('field.fullname'),
            self::IDENTITY => trans('field.identity'),
        ];
    }

    public function setEmailAttribute($value) {
        $this->attributes[self::EMAIL] = strtolower($value);
    }

    public function setBirthdayAttribute($value) {
        $this->attributes[self::BIRTHDAY] = \Carbon\Carbon::parse($value);
    }

    /**
     * Scope a query to only include active users.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query) {
        return $query->where(self::ACTIVE, self::ONE);
    }

    public function getBirthdayAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

    public static $listGender = [
        self::MALE   => 'Nam',
        self::FEMALE => 'Nữ',
    ];

    public function getAllChildUser($userId, $array = array()) {
        if ($this->isHanhChinh() || $this->isAdmin() || $this->isTeleSale()) {
            return $this->pluck('id')->toArray();
        } else {
            $return = $this->where('parent_id', $userId)->where('id', '<>', $userId)->get();
            for ($i = 0; $i < sizeof($return); $i++) {
                $sub_cat = $return[$i]['id'];
                $array[] = $sub_cat;
                $array   = $this->getAllChildUser($sub_cat, $array);
            }
            return $array;
        }
    }

    public function isManager() {
        if (\Auth::user()->can('is-manager')) {
            return true;
        }
        return false;
    }

    public function isAdmin() {
        if (\Auth::user()->can('is-admin')) {
            return true;
        }
        return false;
    }

    public function isHanhChinh() {
        if (\Auth::user()->can('is-hanh-chinh')) {
            return true;
        }
        return false;
    }

    public function isStudent() {
        if (\Auth::user()->can('is-student')) {
            return true;
        }
        return false;
    }

    public function isEducation() {
        if (\Auth::user()->can('is-education')) {
            return true;
        }
        return false;
    }

    public function isTeleSale() {
        if (\Auth::user()->can('is-telesale')) {
            return true;
        }
        return false;
    }
    public function getListUserByDespartment($id) {
        return $this->where('department_id', $id)->get();
    }

    protected static $graph_node_field_aliases = [
        'id'   => 'facebook_user_id',
        'name' => self::FULLNAME,
    ];

    public function department() {
        return $this->belongsTo(Department::class, self::DEPARTMENT_ID, 'id');
    }

    public function position() {
        return $this->belongsTo(Position::class, self::POSITION_ID, 'id');
    }

    public function branch() {
        return $this->belongsTo(Branch::class, self::BRANCH_ID, 'id');
    }

    public function company() {
        return $this->belongsTo(Company::class, self::COMPANY_ID, 'id');
    }

    public function scopeSearch($query, $search = '', $field = '') {
        if (empty($field)) {
            $fields = array(self::FULLNAME, self::EMAIL, self::PHONE);
        } else {
            $fields = explode(',', $field);
        }
        $search = str_replace('%', '\%', $search);
        return $query->where(function ($q) use ($search, $fields) {
            if (!empty($fields)) {
                foreach ($fields as $value) {
                    $array_search = explode('"', $search);
                    foreach ($array_search as $k => $str) {
                        if (!empty($str)) {
                            if ($k % 2 == 1) {
                                $q->orWhere($value, $str);
                            } else {
                                $q->orWhere($value, 'like', '%' . $str . '%');
                            }
                        }

                    }
                }
            }
        });
    }

    public function EducationUserInfo() {
        return $this->hasOne(EducationUserInfo::class, 'user_id', 'id');
    }

    public function getListUserByRole($permission) {
        $listUser = $this->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('permission_role', 'role_user.role_id', '=', 'permission_role.role_id')
            ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
            ->where('permissions.name', $permission)->select('users.id', 'users.fullname')->get();
        return $listUser;
    }
    public function getListTeacherByRole($permission, $name_search = '', $email_search = null) {
        $listTeacher = $this->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('permission_role', 'role_user.role_id', '=', 'permission_role.role_id')
            ->join('permissions', 'permission_role.permission_id', '=', 'permissions.id')
            ->where([['permissions.name', '=', $permission], ['users.fullname', 'like', '%' . $name_search . '%']])
            ->orWhere([['permissions.name', '=', $permission], ['users.email', '=', $email_search]])
            ->select('users.id', 'users.fullname', 'users.email', 'users.phone')->paginate();
        return $listTeacher;
    }

    public static $typeUser = [
        self::TYPE_TRIAL_PERIOD => 'Thử việc',
        self::TYPE_OFFICIAL_PERIOD => 'Chính thức',
    ];

    public static function innerOptionHtml($where = [], $idSelect = '') {
        $html = '<option value=""> Chọn Sale </option>';
        $users = User::where(Self::ACTIVE, self::ONE);
        if($where){
            if(count($where) > 0){
                foreach ($where as $key => $item){
                    $users->whereIn($key, $item);
                }
            }
        }
        $users = $users->get();
        foreach ($users as $value){
            $selected = '';
            if($idSelect == $value->id){
                $selected = 'selected';
            }
            $html .= '<option value="' . @$value->id . '"'.$selected.'>Phòng: ' .  @$value->department->name .' | Chức vụ: '.@$value->position->name. ' | Tên: '. @$value->fullname  .'</option>';
        }
        return $html;
    }

    public function roles() {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }
}
