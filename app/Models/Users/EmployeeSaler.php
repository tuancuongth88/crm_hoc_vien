<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;

class EmployeeSaler extends Model
{
    use TransformableTrait,
        SoftDeletes;

    const FULLNAME       = 'fullname';
    const USER_ID       = 'user_id';
    const EMAIL       = 'email';
    const PHONE       = 'phone';

    protected $table = 'employee_saler';

    protected $fillable = [self::FULLNAME, self::USER_ID, self::EMAIL, self::PHONE];

    protected $dates = ['deleted_at'];
}
