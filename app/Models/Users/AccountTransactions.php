<?php

namespace App\Models\Users;

use App\Models\Customers\Accounts;
use App\Models\Projects\ProjectManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;

class AccountTransactions extends Model
{
    use TransformableTrait,
        SoftDeletes;

    const NAME          = 'name';
    const PROJECT_ID    = 'project_id';
    const PARENT_ID     = 'parent_id';
    const DATE_PAYMENT  = 'date_payment';
    const STATUS        = 'status';
    const ACCOUNT_ID    = 'account_id';
    const SALE_ID       = 'sale_id';
    const TOTAL_MONEY   = 'total_money';

    const STATUS_ACTIVE_PAYMENT     = 1;
    const STATUS_NOT_ACTIVE_PAYMENT = 0;

    public $fillable    = [self::NAME, self::PROJECT_ID, self::PARENT_ID, self::DATE_PAYMENT, self::DATE_PAYMENT,
        self::STATUS, self::ACCOUNT_ID, self::SALE_ID, self::TOTAL_MONEY];
    public $table       = 'account_transaction';

    public $dates = ['deleted_at'];

    public function setDatePaymentAttribute($value) {
        $this->attributes[self::DATE_PAYMENT] = \Carbon\Carbon::parse($value);
    }
    public function getDatePaymentAttribute($value){
        return date('d-m-Y', strtotime($value));
    }

    public function account()
    {
        return $this->belongsTo(Accounts::class, self::ACCOUNT_ID);
    }

    public function sale(){
        return $this->belongsTo(EmployeeSaler::class, self::SALE_ID);
    }

    public function project(){
        return $this->belongsTo(ProjectManager::class, self::PROJECT_ID);
    }

    public function children()
    {
        return $this->hasMany($this, 'parent_id', 'id');
    }

    public function parent()
    {
        return $this->belongsTo($this, 'parent_id');
    }

    public static $statusPayment = [
        self::STATUS_ACTIVE_PAYMENT     => 'Đã thanh toán',
        self::STATUS_NOT_ACTIVE_PAYMENT => 'Chưa thanh toán',
    ];
}
