<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SendSmsLog extends Model
{
    use SoftDeletes;

    const MODEL = 'model';
    const ITEM_ID = 'item_id';
    const USER_ID = 'user_id';
    const PHONE = 'phone';
    const CONTENT = 'content';
    const SMS_CODE = 'sms_code';
    const SMS_RESPONSE = 'sms_response';
    const IS_SEND = 'is_send';

    protected $table = 'send_sms_log';

    protected $fillable = [self::MODEL, self::ITEM_ID, self::USER_ID, self::PHONE, self::CONTENT, self::SMS_CODE, self::IS_SEND, self::SMS_RESPONSE];

    protected $dates = ['deleted_at'];
}
