<?php

namespace App\Models\Customers;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Customer.
 *
 * @package namespace App\Models\Customers;
 */
class Customer extends Model implements Transformable {
    use TransformableTrait, SoftDeletes;

    const FULLNAME   = 'fullname';
    const AVATAR     = 'avatar';
    const PHONE      = 'phone';
    const EMAIL      = 'email';
    const BIRTHDAY   = 'birthday';
    const LEVEL      = 'level';
    const TYPE       = 'type';
    const USER_ID    = 'user_id';
    const ADDRESS    = 'address';
    const ASSIGN_AT  = 'assign_at';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    const SEX          = 'sex';
    const LAST_NAME    = 'last_name';
    const FIRST_NAME   = 'first_name';
    const CITY_ID      = 'city_id';
    const DISTRICT_ID  = 'district_id';
    const CMT          = 'cmt';
    const NGAY_CAP_CMT = 'ngay_cap_cmt';
    const NOI_CAP_CMT  = 'noi_cap_cmt';
    const SOURCE_ID    = 'source_id';
    const CHANNEL      = 'channel';

    const PARENT_ID   = 'parent';
    const RELATION    = 'relation';
    const FINANCE_MIN = 'finance_min';
    const FINANCE_MAX = 'finance_max';
    const INFO        = 'info';

    const DELETED_AT      = 'deleted_at';
    const CREATED_AT      = 'created_at';
    const UPDATED_AT      = 'updated_at';
    const HISTORY_CARE_AT = 'history_care_at';

    const TYPE_CUSTOMER_NORMAL    = 1;
    const TYPE_CUSTOMER_POTENTIAL = 2;
    const TYPE_CUSTOMER_GOOD      = 3;

    //nguồn khách hàng
    const SOURCE_CUSTOMER_COMPANY = 1;
    const SOURCE_CUSTOMER_SALE    = 2;

    const SHARE_ID     = 'share_id';
    const COMMENT      = 'comment';
    const STATUS       = 'status';
    const TOTAL_ASSIGN = 'total_assign';

    //Giới tính
    const MALE   = 1;
    const FEMALE = 2;

    //Trạng thái
    const ACTIVE   = 1;
    const DEACTIVE = 2;

    protected $table = 'customers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FULLNAME,
        self::AVATAR,
        self::PHONE,
        self::EMAIL,
        self::BIRTHDAY,
        self::LEVEL,
        self::TYPE,
        self::ADDRESS,
        self::USER_ID,
        self::ASSIGN_AT,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::SEX,
        self::LAST_NAME,
        self::FIRST_NAME,
        self::CITY_ID,
        self::DISTRICT_ID,
        self::CMT,
        self::NGAY_CAP_CMT,
        self::NOI_CAP_CMT,
        self::SOURCE_ID,
        self::CHANNEL,
        self::PARENT_ID,
        self::RELATION,
        self::FINANCE_MIN,
        self::FINANCE_MAX,
        self::INFO,
        self::SHARE_ID,
        self::COMMENT,
        self::STATUS,
        self::TOTAL_ASSIGN,
        self::HISTORY_CARE_AT,
    ];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::PHONE       => trans('field.phone'),
            self::EMAIL       => trans('field.email'),
            self::CITY_ID     => trans('field.city_id'),
            self::DISTRICT_ID => trans('field.district_id'),
            self::LAST_NAME   => trans('field.last_name'),
            self::FIRST_NAME  => trans('field.first_name'),
        ];
    }

    public function customerType() {
        return $this->hasOne(TypeCustomer::class, 'id', 'type');
    }

    public function customerLevel() {
        return $this->hasOne(Level::class, 'id', self::LEVEL);
    }

    public function agent() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function setBirthdayAttribute($value) {
        $this->attributes[self::BIRTHDAY] = \Carbon\Carbon::parse($value);
    }

    public function setNgayCapCmtAttribute($value) {
        $this->attributes[self::NGAY_CAP_CMT] = \Carbon\Carbon::parse($value);
    }

    public function getBirthdayAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

    public function getNgayCapCmtAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

    public function histories() {
        return $this->hasMany(Histories::class, 'customer_id', 'id')->orderBy('id', 'desc');
    }

    public function projectCustomer() {
        return $this->hasMany(ProjectCustomer::class, 'customer_id', 'id');
    }

    public static $typeCustomer = [
        self::TYPE_CUSTOMER_NORMAL    => 'Khách hàng Thường',
        self::TYPE_CUSTOMER_POTENTIAL => 'Khách hàng tiềm năng',
        self::TYPE_CUSTOMER_GOOD      => 'Khách hàng nét',
    ];

    public static $sourceCustomer = [
        self::SOURCE_CUSTOMER_COMPANY => 'Khách do công ty chuyển',
        self::SOURCE_CUSTOMER_SALE    => 'Khách hàng tự tìm kiếm',
    ];

    public static $listSex = [
        self::MALE   => 'Nam',
        self::FEMALE => 'Nữ',
    ];

    public static $listStatus = [
        self::ACTIVE   => 'Tiếp tục chăm sóc',
        self::DEACTIVE => 'Dừng chăm sóc',
    ];

    public function children() {
        return $this->hasMany($this, 'parent', 'id');
    }

    public static $listRelation = [
        'ong'  => 'Ông',
        'ba'   => 'Bà',
        'bo'   => 'Bố',
        'me'   => 'Mẹ',
        'anh'  => 'Anh',
        'chi'  => 'Chị',
        'em'   => 'Em',
        'co'   => 'Cô',
        'di'   => 'Dì',
        'chu'  => 'Chú',
        'bac'  => 'Bác',
        'thim' => 'Thím',
        'ban'  => 'Bạn',
        'cau'  => 'Cậu',
        'mo'   => 'Mợ',
        'con'  => 'Con',
    ];

    protected $casts = [
        'info' => 'array',
    ];

}
