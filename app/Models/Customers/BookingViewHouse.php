<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BookingViewHouse extends Model
{
    use SoftDeletes;

    const ACCOUNT_ID    =   'account_id';
    const FULLNAME      =   'fullname';
    const PHONE         =   'phone';
    const EMAIL         =   'email';
    const TIME_VIEW     =   'time_view';
    const NƠTE          =   'note';
    const STATUS        =   'status';

    const STATUS_NEW    = 0;
    const STATUS_APPROVE= 1;
    const STATUS_REJECT = 2;

    protected $table    =   'booking_view_house';

    protected $fillable = [ self::ACCOUNT_ID, self::FULLNAME, self::PHONE, self::EMAIL, self::TIME_VIEW, self::NƠTE, self::STATUS ];

    protected $dates = ['deleted_at'];

    public function account(){
        return $this->belongsTo(Accounts::class, 'account_id');
    }

    public function setTimeViewAttribute($value){
        $date = str_replace('/', '-', $value);
        $result = date('d-m-Y H:i', strtotime($date));
        $this->attributes[self::TIME_VIEW] = \Carbon\Carbon::parse($result);
    }

    public static $listStatus = [
        self::STATUS_NEW        => 'Mới đăng',
        self::STATUS_APPROVE    => 'Hoàn thành',
        self::STATUS_REJECT     => 'Hủy',
    ];
}
