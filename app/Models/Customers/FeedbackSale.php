<?php

namespace App\Models\Customers;

use App\Models\Users\AccountTransactions;
use App\Models\Users\EmployeeSaler;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedbackSale extends Model
{
    use SoftDeletes;

    const ACCOUNT_ID = 'account_id';
    const TRANSACTION_ID = 'transaction_id';
    const SALE_ID = 'sale_id';
    const RANK_SALE = 'rank_sale';
    const NOTE = 'note';

    public $dates = ['deleted_at'];

    public $fillable = [self::ACCOUNT_ID, self::TRANSACTION_ID, self::SALE_ID, self::RANK_SALE, self::NOTE];

    public $table = 'feedback_sale';

    public function transaction(){
        return $this->belongsTo(AccountTransactions::class, 'transaction_id');
    }

    public function sale(){
        return $this->belongsTo(EmployeeSaler::class, 'sale_id');
    }

    public static $listRank = [0 => '0 sao', 1 => '1 sao', 2 => '2 sao', 3 => '3 sao', 4 => '4 sao', 5 => '5 sao'];
}
