<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Model;

class FollowerNews extends Model
{
    const ACCOUNT_ID = 'account_id';
    const CATEGORY_NEW_ID = 'category_new_id';

    protected $table = 'follower_news';

    protected $fillable = [self::ACCOUNT_ID, self::CATEGORY_NEW_ID];

    public function account(){
        return $this->hasMany(Accounts::class, 'account_id');
    }
    
}
