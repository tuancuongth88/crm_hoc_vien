<?php

namespace App\Models\Customers;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class CustomerSharingUser.
 *
 * @package namespace App\Models;
 */
class CustomerSharingUser extends Model implements Transformable {
    use TransformableTrait;

    const USER_ID        = 'user_id';
    const CUS_SHARING_ID = 'cus_sharing_id';
    const NUMBER_SHARE   = 'number_share';
    const NUMBER_RECOVER = 'number_recover';
    const RECOVER_LIST   = 'recover_list';
    const PARENT_ID   = 'parent_id';

    const ASSIGN_AT  = 'assign_at';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const PARAMS = 'params';

    protected $table = 'customer_sharing_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::USER_ID,
        self::NUMBER_SHARE,
        self::CUS_SHARING_ID,
        self::PARENT_ID,
        self::CREATED_BY,
        self::UPDATED_BY,
        self::PARAMS,
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'recover_list' => 'array',
        'params' => 'array'
    ];

    public static function getFieldVietnamese() {
        return [
            self::NUMBER_SHARE => trans('field.number_share'),
            self::USER_ID      => trans('field.user_id'),
        ];
    }

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function cusSharing() {
        return $this->belongsTo(CustomerSharing::class, 'cus_sharing_id', 'id');
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function children()
    {
        return $this->hasMany($this, 'parent_id', 'id');
    }

}
