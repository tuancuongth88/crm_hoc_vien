<?php

namespace App\Models\Customers;

use App\Models\Customers\Level;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Histories.
 *
 * @package namespace App\Models\Customers;
 */
class Histories extends Model implements Transformable {
    use TransformableTrait, SoftDeletes;

    const USER_ID        = 'user_id';
    const CUSTOMER_ID    = 'customer_id';
    const STATUS         = 'status';
    const TIME_CALL_BACK = 'time_call_back';
    const NOTE           = 'note';
    const PHONE          = 'phone';
    const CHANNEL        = 'channel';
    const UPDATED_BY        = 'updated_by';
    const CREATED_BY        = 'created_by';

    // kenh cham soc
    const CAll_PHONE      = 1;
    const SEND_SMS        = 2;
    const SEND_EMAIL      = 3;
    const SOCIAL_NETWORK  = 4;


    protected $table = 'histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::USER_ID,
        self::CUSTOMER_ID,
        self::STATUS,
        self::TIME_CALL_BACK,
        self::NOTE,
        self::PHONE,
        self::CHANNEL,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    protected $dates = ['deleted_at', 'time_call_back'];

    public function customer() {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function agent() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function level() {
        return $this->belongsTo(Level::class, 'status', 'id');
    }

    public static function getFieldVietnamese() {
        return [
            self::PHONE => trans('field.phone'),
            self::EMAIL => trans('field.note'),
        ];
    }
    public static $listChannels = [
        self::CAll_PHONE        => 'Gọi điện',
        self::SEND_SMS          => 'Nhắn tin',
        self::SEND_EMAIL        => 'Gửi email',
        self::SOCIAL_NETWORK    => 'Tương tác Social',
    ];
}
