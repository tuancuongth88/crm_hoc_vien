<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class AccountFireBase  extends Model implements Transformable {
    use TransformableTrait, SoftDeletes;

    const ACCOUNT_ID = 'account_id';
    const TYPE_DEVICE = 'type_device';
    const DEVICE_NAME = 'device_name';
    const DEVICE_ID = 'device_id';
    const TOKEN = 'token';

    public $fillable = [self::ACCOUNT_ID, self::TYPE_DEVICE, self::DEVICE_NAME, self::DEVICE_ID, self::TOKEN ];

    public $dates = ['deleted_at'];

    public $table = 'account_token_firebase';


}
