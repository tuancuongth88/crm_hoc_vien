<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class NotificationFirebase extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;

    const ACCOUNT_ID    = 'account_id';
    const NEWS_ID       = 'news_id';
    const DEVICE_ID     = 'device_id';
    const TOKEN         = 'token';
    const RESPONSE_CODE = 'response_code';
    const SEND_AT       = 'send_at';
    const READ_AT       = 'read_at';

    protected $fillable = [self::ACCOUNT_ID, self::NEWS_ID, self::DEVICE_ID,
                           self::TOKEN, self::RESPONSE_CODE, self::SEND_AT, self::READ_AT];

    protected $table = 'notification_firebase';

    public $dates = ['deleted_at'];
}
