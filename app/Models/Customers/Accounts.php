<?php

namespace App\Models\Customers;

use App\Models\Projects\ProjectAccount;
use App\Models\Users\AccountTransactions;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Mpociot\Firebase\SyncsWithFirebase;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class Level.
 *
 * @package namespace App\Models\Customers;
 */
class Accounts  extends Authenticatable implements JWTSubject {
    use Notifiable;
    use TransformableTrait, SoftDeletes;

    const PHONE         = 'phone';
    const PASSWORD      = 'password';
    const FULLNAME      = 'fullname';
    const CUSTOMER_ID   = 'customer_id';
    const EMAIL         = 'email';
    const AVATAR        = 'avatar';
    const GENDER        = 'gender';
    const BIRTHDAY      = 'birthday';
    const ADDRESS       = 'address';
    const ACTIVE        = 'active';
    const UPDATED_BY    = 'created_by';
    const CREATED_BY    = 'updated_by';
    const IDENTITY      = 'identity';
    const SMS_CODE      = 'sms_code';
    const SMS_RESPONSE  = 'sms_response';

    const COUNT_NOTIFICATION= 'count_notification';
    const LAST_SEND_TIME  = 'last_send_time';


    const ONE    = 1;

    protected $table    = 'accounts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::PHONE,
        self::PASSWORD,
        self::FULLNAME,
        self::CUSTOMER_ID,
        self::EMAIL,
        self::AVATAR,
        self::GENDER,
        self::BIRTHDAY,
        self::ADDRESS,
        self::ACTIVE,
        self::UPDATED_BY,
        self::CREATED_BY,
        self::IDENTITY,
        self::SMS_CODE,
        self::SMS_RESPONSE,
        self::COUNT_NOTIFICATION,
        self::LAST_SEND_TIME,
    ];

    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::PASSWORD, 'remember_token',
    ];

    public function setBirthdayAttribute($value) {
        $this->attributes[self::BIRTHDAY] = \Carbon\Carbon::parse($value);
    }

    public function getBirthdayAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public static function getFieldVietnamese() {
        return [
            self::FULLNAME       => trans('field.fullname'),
            self::PHONE       => trans('field.phone'),
            self::EMAIL       => trans('field.email'),
            self::PASSWORD     => trans('field.password'),
        ];
    }

    public function transaction(){
        return $this->hasMany(AccountTransactions::class, 'account_id');
    }

    public function project(){
        return $this->hasMany(ProjectAccount::class, 'account_id');
    }

    public function followers(){
        return $this->hasMany(FollowerNews::class,'account_id', 'id');
    }

    public function consignments(){
        return $this->hasMany(Consignments::class, 'account_id', 'id');
    }

    public function bookingViewHouse(){
        return $this->hasMany(BookingViewHouse::class, 'account_id', 'id');
    }

    public function feedbackSale(){
        return $this->hasMany(FeedbackSale::class, 'account_id', 'id');
    }

    public static $listMonth = [    1 => 'Tháng 1',
                                    2 => 'Tháng 2',
                                    3 => 'Tháng 3',
                                    4 => 'Tháng 4',
                                    5 => 'Tháng 5',
                                    6 => 'Tháng 6' ,
                                    7 => 'Tháng 7',
                                    8 => 'Tháng 8',
                                    9 => 'Tháng 9',
                                    10 => 'Tháng 10',
                                    11 => 'Tháng 11',
                                    12 => 'Tháng 12'];

    public static $listRank = [
        3000000000 => 'Hạng Bạc',
        6000000000 => 'Hạng Vàng',
        10000000000 => 'Hạng Bạch kim',
        0 => 'Hạng Kim cương',
    ];
}
