<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OptionConsignments extends Model
{
    use SoftDeletes;
    const NAME      = 'name';
    const TYPE      = 'type';
    const PARENT      = 'parent';

    const PRODUCT_TYPE  =   1;
    const LAND_TYPE     =   2;
    const AREA_TYPE     =   3;
    const PRICE_TYPE    =   4;

    public $fillable = [self::NAME, self::TYPE, self::PARENT];

    public $table = 'option_consignment';
    public $dates = ['deleted_at'];

    public function children(){
        return $this->hasMany( $this, 'parent', 'id' );
    }
}
