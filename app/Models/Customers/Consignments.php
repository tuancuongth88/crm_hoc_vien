<?php

namespace App\Models\Customers;

use App\Models\Projects\District;
use App\Models\Projects\Province;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consignments extends Model
{
    use SoftDeletes;

    const ACCOUNT_ID    = 'account_id';
    const TITLE         = 'title';
    const PRODUCT_TYPE  = 'product_type';
    const LAND_TYPE     = 'land_type';
    const ADDRESS       = 'address';
    const CITY_ID       = 'city_id';
    const DISTRICT_ID   = 'district_id';
    const AREA          = 'area';
    const AREA_TYPE     = 'area_type';
    const PRICE         = 'price';
    const PRICE_TYPE    = 'price_type';
    const DESCRIPTION   = 'description';
    const FULLNAME      = 'fullname';
    const PHONE         = 'phone';
    const EMAIL         = 'email';
    const STATUS        = 'status';
    const IMAGES        = 'images';

    const STATUS_TIEP_NHAN    = 1;
    const STATUS_DA_NHAN      = 2;
    const STATUS_HOAN_THANH   = 3;
    const STATUS_HUY          = 4;

    public $table = 'consignment';
    public $dates = ['deleted_at'];

    public $fillable = [self::ACCOUNT_ID, self::TITLE, self::PRODUCT_TYPE, self::LAND_TYPE,
                        self::ADDRESS, self::CITY_ID, self::DISTRICT_ID, self::AREA, self::AREA_TYPE,
                        self::PRICE, self::PRICE_TYPE, self::DESCRIPTION, self::FULLNAME, self::PHONE,
                        self::EMAIL, self::STATUS, self::IMAGES];

    public function city(){
        return $this->belongsTo(Province::class, 'city_id');
    }

    public function district(){
        return $this->belongsTo(District::class, 'district_id');
    }

    public function getCreatedAtAttribute($value){
        return date('d-m-Y', strtotime($value));
    }
    public function getUpdatedAtAttribute($value){
        return date('d-m-Y', strtotime($value));
    }

    public function productType(){
        return $this->belongsTo(OptionConsignments::class, 'product_type');
    }

    public function landType(){
        return $this->belongsTo(OptionConsignments::class, 'land_type');
    }

    public function areaType(){
        return $this->belongsTo(OptionConsignments::class, 'area_type');
    }

    public function priceType(){
        return $this->belongsTo(OptionConsignments::class, 'price_type');
    }

    public function account(){
        return $this->belongsTo(Accounts::class, 'account_id');
    }

    public static $arrayStatus = [
        self::STATUS_TIEP_NHAN  => 'Chờ tiếp nhận',
        self::STATUS_DA_NHAN    => 'Đã chuyển Sale',
        self::STATUS_HOAN_THANH => 'Hoàn thành',
        self::STATUS_HUY        => 'Hủy',
    ];
}
