<?php

namespace App\Models\Customers;

use App\Models\Customers\Customer;
use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Auth;

/**
 * Class CustomerSharing.
 *
 * @package namespace App\Models;
 */
class CustomerSharing extends Model implements Transformable {

    use TransformableTrait;

    const NAME       = 'name';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    //trạng thái chia khách hàng
    const GROUP_NEW = 0;
    const GROUP_SHARED = 1;
    const GROUP_NO_NEED = 2;

    protected $table = 'customer_sharings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    //
    public static $listCusGroup = [
        self::GROUP_NEW => 'Khách hàng mới',
        self::GROUP_SHARED => 'Khách hàng đã chia và còn tiềm năng',
        self::GROUP_NO_NEED => 'Khách hàng đã chia và không có nhu cầu',
    ];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }

    public function createdBy() {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    public function sharingUser() {
        return $this->hasMany(CustomerSharingUser::class, CustomerSharingUser::CUS_SHARING_ID, 'id');
    }

    public static function getAllSharingByUser() {
        $user = new User();
        $arrUser = [];
        $arrUser = $user->getAllChildUser(\Auth::user()->id);
        array_push($arrUser, \Auth::user()->id);
        if (Auth::user()->isHanhChinh() || Auth::user()->isAdmin() || Auth::user()->isTeleSale()) {
            return CustomerSharing::get()->pluck('name', 'id')->toArray();
        }
        $listShare = Customer::whereIn('created_by', $arrUser)->get()->pluck('share_id')->toArray();
        return CustomerSharing::whereIn('id', $listShare)->orderBy('id', 'desc')->get()->pluck('name', 'id')->toArray();
    }

}
