<?php

namespace App\Models\Customers;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use \Auth;

/**
 * Class TypeCustomer.
 *
 * @package namespace App\Models\Customers;
 */
class TypeCustomer extends Model implements Transformable {
    use TransformableTrait, SoftDeletes;
    const NAME = 'name';

    protected $table = 'customer_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [self::NAME];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }

    public static function getAllTypeCustomerByUser() {
        $userId = Auth::user()->id;
        $user = new User();
        $arrUser = [];
        $arrUser = $user->getAllChildUser($userId);
        array_push($arrUser, \Auth::user()->id);
        if (Auth::user()->isHanhChinh() || Auth::user()->isAdmin() || Auth::user()->isTeleSale()) {
            return TypeCustomer::get()->pluck('name', 'id')->toArray();
        }
        $listType = Customer::whereIn('created_by', $arrUser)->get()->pluck('type')->toArray();
        return TypeCustomer::whereIn('id', $listType)->get()->pluck('name', 'id')->toArray();
    }

}
