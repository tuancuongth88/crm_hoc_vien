<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class CustomerSharingLog.
 *
 * @package namespace App\Models;
 */
class CustomerSharingLog extends Model implements Transformable
{
    use TransformableTrait;

    const CUSTOMER_ID = 'customer_id';
    const PARAMS = 'params';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    protected $table = 'customer_sharing_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::CUSTOMER_ID,
        self::PARAMS,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];


    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }


    protected $dates = ['deleted_at'];

    protected $casts = [
        'params' => 'array'
    ];
}
