<?php

namespace App\Models\Customers;

use App\Models\Projects\Project;
use App\Models\Projects\ProjectManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;

class ProjectCustomer extends Model
{
    use TransformableTrait;

    protected $table = 'project_customer';

    protected $fillable = [
        'project_id',
        'customer_id',
        'level',
        'created_by',
    ];

    public function level(){
        return $this->belongsTo(Level::class, 'level', 'id');
    }

    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function project(){
        return $this->belongsTo(ProjectManager::class, 'project_id', 'id');
    }

}
