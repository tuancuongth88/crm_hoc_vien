<?php

namespace App\Models\Customers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Level.
 *
 * @package namespace App\Models\Customers;
 */
class Level extends Model implements Transformable {
    use TransformableTrait, SoftDeletes;

    const NAME       = 'name';
    const TYPE       = 'type';
    const UPDATED_BY = 'created_by';
    const CREATED_BY = 'updated_by';

    const TYPE_CUSTOMER = 1;

    protected $table = 'levels';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::TYPE,
        self::UPDATED_BY,
        self::CREATED_BY,
    ];

    protected $dates = ['deleted_at'];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }
}
