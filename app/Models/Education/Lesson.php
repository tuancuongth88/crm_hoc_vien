<?php

namespace App\Models\Education;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Lesson.
 *
 * @package namespace App\Models\Educations;
 */
class Lesson extends Model implements Transformable
{
    use TransformableTrait, Sluggable,
        SoftDeletes;

    const NAME          = 'name';
    const SLUG          = 'slug';
    const PROGRAM_ID    = 'program_id';
    const SUBJECT_ID    = 'subject_id';
    const DESCRIPTION   = 'description';
    const NUMBER_SESSION= 'number_session';
    const NUMBER_HOURS  = 'number_hours';
    const PARENT        = 'parent';
    const CREATED_BY    = 'created_by';
    const UPDATED_BY    = 'updated_by';

    protected $table    = 'education_lessons';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        self::PROGRAM_ID,
        self::SUBJECT_ID,
        self::NAME,
        self::SLUG,
        self::DESCRIPTION,
        self::NUMBER_SESSION,
        self::NUMBER_HOURS,
        self::PARENT,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
            self::PROGRAM_ID => trans('field.program_id'),
            self::SUBJECT_ID => trans('field.subject_id'),
            self::DESCRIPTION => trans('field.description'),
        ];
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => self::NAME,
            ],
        ];
    }

    public function documents() {
        return $this->hasMany(Document::class, 'item_id', 'id')->where('education_document.model', '=', 'Lesson');
    }

    public function subject() {
        return $this->belongsToMany(Subject::class,  'education_subject_lesson', 'subject_id');
    }
    public function question() {
        return $this->hasMany(Question::class, 'lesson_id', 'id');
    }

    public function subject_lesson() {
        return $this->hasMany(SubjectLesson::class, 'lesson_id', 'id');
    }
}
