<?php

namespace App\Models\Education;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Answer.
 *
 * @package namespace App\Models\Education;
 */
class Answer extends Model implements Transformable {
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const NAME        = 'name';
    const IS_CORRECT  = 'is_correct';
    const QUESTION_ID = 'question_id';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';

    protected $table = 'education_answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::IS_CORRECT,
        self::QUESTION_ID,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }

}
