<?php

namespace App\Models\Education;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Question.
 *
 * @package namespace App\Models\Education;
 */
class Question extends Model implements Transformable {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use TransformableTrait,
        SoftDeletes;

    const NAME       = 'name';
    const LEVEL      = 'level';
    const SUBJECT_ID = 'subject_id';
    const LESSON_ID  = 'lesson_id';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    const EASY       = 1;
    const MEDIUM     = 2;
    const HARD       = 3;

    protected $table = 'education_questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::LEVEL,
        self::SUBJECT_ID,
        self::LESSON_ID,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
            self::SUBJECT_ID => trans('field.subject_id'),
            self::LESSON_ID => trans('field.lesson_id'),
            self::LEVEL => trans('field.education_question_level')
        ];
    }

    public function hasAnswers() {
        return $this->hasMany(Answer::class, Answer::QUESTION_ID, 'id');
    }

    public static function saveRelateAnswer($array, $obj) {
        $dataRelated = [];
        foreach ($array as $input) {
            if (!isset($input['answer'])) {
                continue;
            }
            $dataRelated[] = new Answer([
                Answer::NAME        => $input['answer'],
                Answer::IS_CORRECT  => (isset($input[Answer::IS_CORRECT][0]) && $input[Answer::IS_CORRECT][0] == ONE) ? ONE : ZERO,
                Answer::QUESTION_ID => $obj->id,
                self::CREATED_BY    => $obj->created_by,
            ]);
        }
        if (count($dataRelated) >= ZERO) {
            $obj->hasAnswers()->saveMany($dataRelated);
        }
        return true;
    }

    public static $listLevel = [
        self::EASY   => 'Dễ',
        self::MEDIUM => 'Trung bình',
        self::HARD   => 'Khó',
    ];

    public function subject() {
        return $this->belongsTo(Subject::class, self::SUBJECT_ID, 'id');
    }
    public function lesson() {
        return $this->belongsTo(Lesson::class, self::LESSON_ID, 'id');
    }
}