<?php

namespace App\Models\Education;

use App\Presenters\Education\ClassRoomPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ClassRoom.
 *
 * @package namespace App\Models\Education;
 */
class ClassRoom extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;

    protected $table = 'education_class_room';

    const NAME       = 'name';
    const DESCRIPTION= 'description';
    const TAGET      = 'taget';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [self::NAME, self::DESCRIPTION, self::TAGET, self::CREATED_BY, self::UPDATED_BY];

    protected $dates = ['deleted_at'];
    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new ClassRoomPresenter($this);
    }
}
