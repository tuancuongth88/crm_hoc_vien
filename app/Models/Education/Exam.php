<?php

namespace App\Models\Education;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Exam.
 *
 * @package namespace App\Models\Education;
 */
class Exam extends Model implements Transformable {
    use TransformableTrait,
        SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const NAME        = 'name';
    const DESCRIPTION = 'description';
    const QUESTIONS   = 'questions';
    const SUBJECT_ID  = 'subject_id';
    const TEACHER     = 'teacher';
    const STATUS      = 'status';
    const START_TIME  = 'start_time';
    const FINISH_TIME = 'finish_time';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';

    const ONLINE_EXAM  = 0;
    const OFFLINE_EXAM = 1;

    protected $table = 'education_exams';



    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::DESCRIPTION,
        self::SUBJECT_ID,
        self::TEACHER,
        self::STATUS,
        self::START_TIME,
        self::FINISH_TIME,
        self::QUESTIONS,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }

    public static $typeExam = [self::OFFLINE_EXAM => 'Bài Thi trên giấy',
                                self::ONLINE_EXAM => 'Bài Thi Online'];

    public function setStartTimeAttribute($value) {
        $this->attributes[self::START_TIME] = \Carbon\Carbon::parse($value);
    }

    public function setFinishTimeAttribute($value) {
        $this->attributes[self::FINISH_TIME] = \Carbon\Carbon::parse($value);
    }

    public function getStartTimeAttribute($value) {
        return date('d-m-Y H:i', strtotime($value));
    }

    public function getFinishTimeAttribute($value) {
        return date('d-m-Y H:i', strtotime($value));
    }

    public function subject() {
        return $this->belongsTo(Subject::class, self::SUBJECT_ID, 'id');
    }

    public function teaccher() {
        return $this->belongsTo(User::class, self::TEACHER, 'id');
    }

    public function examResult(){
        return $this->hasMany(ExamResult::class, ExamResult::EXAM_ID, 'id');
    }

}