<?php

namespace App\Models\Education;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class ExamResult.
 *
 * @package namespace App\Models\Education;
 */
class ExamResult extends Model implements Transformable {
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    const POINT      = 'point';
    const EXAM_ID    = 'exam_id';
    const ANSWERS    = 'answers';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    const RATE_EXCELLENT = 1;
    const RATE_GOOD      = 2;
    const RATE_AVERAGE   = 3;
    const RATE_WEAK      = 4;

    protected $table = 'education_exam_results';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::POINT,
        self::EXAM_ID,
        self::ANSWERS,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public function getCreatedAtAttribute($value) {
        return date('d-m-Y H:i', strtotime($value));
    }

    public function exam(){
        return $this->belongsTo(Exam::class,'exam_id');
    }

    public function ratePoint(){
        if($this->point > 8)
            return self::RATE_EXCELLENT;
        if($this->point > 7 && $this->point <= 8 )
            return self::RATE_GOOD;
        if($this->point >= 5 && $this->point <=7 )
            return self::RATE_AVERAGE;
        return self::RATE_WEAK;
    }

    public static function boot() {
        parent::boot();
        self::creating(function ($model) {
            // cập nhật điểm trung bình va tổng điểm của học viên theo chương trình
            $userId = Auth::user()->id;
            $exam = Exam::find($model->exam_id);
            $listExamResult =  ExamResult::join('education_exams as exam', 'exam.id', '=', 'education_exam_results.exam_id')
                ->where(Exam::SUBJECT_ID, $exam->subject_id)
                ->where('education_exam_results.'.ExamResult::CREATED_BY, $userId)->get();
            $updateTotalPoint = UserRegProgram::where(UserRegProgram::USER_ID, $userId)
                                              ->where(UserRegProgram::SUBJECT_ID, $exam->subject_id)->first();
            if($updateTotalPoint){
                $totalExam = $listExamResult->count() == 0 ? 1 : $listExamResult->count() + 1;
                $totalPoint['total_point'] = $updateTotalPoint->total_point + $model->point;
                $totalPoint['avg_point']   = $totalPoint['total_point'] / $totalExam;
                if($model->ratePoint() == ExamResult::RATE_WEAK){
                    $totalPoint['pass'] = UserRegProgram::POINT_NOT_PASS;
                }
                $updateTotalPoint->update($totalPoint);
            }
        });

        self::updating(function ($model) {
            //
        });

        self::updated(function ($model) {
            //
        });
    }
}
