<?php

namespace App\Models\Education;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Area.
 *
 * @package namespace App\Models\Education;
 */
class SubjectLesson extends Model implements Transformable {
    use TransformableTrait;

    const SUBJECT_ID = 'subject_id';
    const LESSON_ID = 'lesson_id';
    const TYPE_USER  = 'type_user';
    const USER_ID    = 'user_id';

    protected $table = 'education_subject_lesson';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::SUBJECT_ID,
        self::LESSON_ID,
        self::TYPE_USER,
        self::USER_ID,
    ];

    public function subject(){
        return $this->hasOne(Subject::class, 'id', 'subject_id');
    }

    public function lesson(){
        return $this->hasOne(Lesson::class, 'id', 'lesson_id');
    }

    public function teacher(){
        return $this->hasOne(User::class, 'id', 'user_id');
    }

}
