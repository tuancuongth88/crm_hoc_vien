<?php

namespace App\Models\Education;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Area.
 *
 * @package namespace App\Models\Education;
 */
class Area extends Model implements Transformable {
    use TransformableTrait,
        SoftDeletes;

    const NAME       = 'name';
    const DESCRIPTION= 'description';
    const CREATED_BY = 'created_by';
    const UPDATED_BY = 'updated_by';

    protected $table = 'education_areas';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::DESCRIPTION,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME => trans('field.name'),
        ];
    }

}
