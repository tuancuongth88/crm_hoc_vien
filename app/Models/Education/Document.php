<?php
/**
 * Created by PhpStorm.
 * User: Cuongnt
 * Date: 20/06/2018
 * Time: 4:49 CH
 */

namespace App\Models\Education;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Document extends Model implements Transformable {
    use TransformableTrait, SoftDeletes;
    const NAME        = 'name';
    const MODEL       = 'model';
    const ITEM_ID     = 'item_id';
    const FILE_URL    = 'file_url';
    const DESCRIPTION = 'description';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';

    protected $table = 'education_document';

    protected $fillable = [
        self::NAME,
        self::MODEL,
        self::ITEM_ID,
        self::FILE_URL,
        self::DESCRIPTION,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME        => trans('field.name'),
            self::DESCRIPTION => trans('field.description'),
        ];
    }

}