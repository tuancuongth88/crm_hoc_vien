<?php

namespace App\Models\Education;

use App\Models\Users\User;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Subject.
 *
 * @package namespace App\Models\Educations;
 */
class UserRegProgram extends Model implements Transformable {
    use TransformableTrait;

    const USER_ID     = 'user_id';
    const SUBJECT_ID  = 'subject_id';
    const OPEN_TIME   = 'open_time';
    const CLOSE_TIME  = 'close_time';
    const PASS        = 'pass';
    const AVG_POINT   = 'avg_point';
    const TOTAL_POINT = 'total_point';

    const POINT_PASS        = 1;
    const POINT_NOT_PASS    = 2;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::USER_ID,
        self::SUBJECT_ID,
        self::OPEN_TIME,
        self::CLOSE_TIME,
        self::PASS,
        self::AVG_POINT,
        self::TOTAL_POINT,
    ];

    protected $table = 'education_user_reg_program';

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class, 'subject_id', 'id');
    }

    public static function statusPass()
    {
        return [self::POINT_PASS     => 'Đạt',
            self::POINT_NOT_PASS     => 'Không đạt'];
    }
}
