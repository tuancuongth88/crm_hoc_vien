<?php

namespace App\Models\Education;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Program.
 *
 * @package namespace App\Models\Education;
 */
class Program extends Model implements Transformable {
    use TransformableTrait,
    Sluggable,
        SoftDeletes;

    const NAME        = 'name';
    const DESCRIPTION = 'description';
    const IMAGE_URL   = 'image_url';
    const ICON_URL    = 'icon_url';
    const POSITION    = 'position';
    const CREATED_BY  = 'created_by';
    const UPDATED_BY  = 'updated_by';

    protected $table = 'education_programs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::DESCRIPTION,
        self::IMAGE_URL,
        self::ICON_URL,
        self::POSITION,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    public static function getFieldVietnamese() {
        return [
            self::NAME        => trans('field.education_program_name'),
            self::IMAGE_URL   => trans('field.education_program_image_url'),
            self::ICON_URL    => trans('field.education_program_icon_url'),
            self::DESCRIPTION => trans('field.description'),
        ];
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => self::NAME,
            ],
        ];
    }

    public function subject(){
        return $this->hasMany(Subject::class, Subject::PROGRAM_ID, 'id');
    }

}
