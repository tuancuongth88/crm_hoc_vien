<?php

namespace App\Models\Education;

use App\Models\Education\Document;
use App\Models\Users\User;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Subject.
 *
 * @package namespace App\Models\Educations;
 */
class Subject extends Model implements Transformable {
    use TransformableTrait, Sluggable,
        SoftDeletes;

    const NAME         = 'name';
    const SHORT_NAME   = 'short_name';
    const IS_HOT       = 'is_hot';
    const SLUG         = 'slug';
    const DESCRIPTION  = 'description';
    const MESSAGE      = 'message';
    const TARGET       = 'target';
    const POSITION     = 'position';
    const SCHOOL_TIME  = 'school_time';
    const OPEN_TIME    = 'open_time';
    const CLOSE_TIME   = 'close_time';
    const SCHEDULE     = 'schedule';
    const TOTAL_TIME   = 'total_time';
    const TOTAL_FEE    = 'total_fee';
    const FEE_DISCOUNT = 'fee_discount';
    const CREATED_BY   = 'created_by';
    const UPDATED_BY   = 'updated_by';
    const USER_ID      = 'user_id';
    const PROGRAM_ID   = 'program_id';
    const STATUS       = 'status';
    const TYPE_USER    = 'type_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::SHORT_NAME,
        self::IS_HOT,
        self::PROGRAM_ID,
        self::USER_ID,
        self::DESCRIPTION,
        self::MESSAGE,
        self::TARGET,
        self::POSITION,
        self::SCHOOL_TIME,
        self::OPEN_TIME,
        self::CLOSE_TIME,
        self::SCHEDULE,
        self::TOTAL_TIME,
        self::TOTAL_FEE,
        self::FEE_DISCOUNT,
        self::SLUG,
        self::STATUS,
        self::TYPE_USER,
        self::CREATED_BY,
        self::UPDATED_BY,
    ];

    protected $table = 'education_subjects';

    public static function getFieldVietnamese() {
        return [
            self::NAME        => trans('field.name'),
            self::SHORT_NAME  => trans('field.education_subject_short_name'),
            self::TYPE_USER   => 'Loại tài khoản',
            self::PROGRAM_ID  => trans('field.program_id'),
            self::DESCRIPTION => trans('field.description'),
            self::OPEN_TIME   => trans('field.education_open_time'),
            self::CLOSE_TIME  => trans('field.education_close_time'),
        ];
    }
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable() {
        return [
            'slug' => [
                'source' => self::NAME,
            ],
        ];
    }

    public function documents() {
        return $this->hasMany(Document::class, 'item_id', 'id')->where('education_document.model', '=', 'Subject');
    }

    public function program() {
        return $this->hasOne(Program::class, 'id', 'program_id');
    }

    public function teacher() {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function getStatus() {
        if ($this->status == 0) {
            return 'chưa xuất bản';
        }
        return 'Đã xuất bản';

    }

    public function setOpenTimeAttribute($value) {
        $this->attributes[self::OPEN_TIME] = \Carbon\Carbon::parse($value);
    }

    public function setCloseTimeAttribute($value) {
        $this->attributes[self::CLOSE_TIME] = \Carbon\Carbon::parse($value);
    }

    public function getOpenTimeAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

    public function getCloseTimeAttribute($value) {
        return date('d-m-Y', strtotime($value));
    }

    public function exam(){
        return $this->hasMany(Exam::class, 'subject_id', 'id');
    }

    public function lesson() {
        return $this->belongsToMany(Lesson::class,  'education_subject_lesson', 'subject_id', 'lesson_id');
    }

}
