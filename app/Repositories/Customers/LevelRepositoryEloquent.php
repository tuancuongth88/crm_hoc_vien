<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Customers\LevelRepository;
use App\Models\Customers\Level;
use App\Validators\Customers\LevelValidator;

/**
 * Class LevelRepositoryEloquent.
 *
 * @package namespace App\Repositories\Customers;
 */
class LevelRepositoryEloquent extends BaseRepository implements LevelRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Level::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return LevelValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
