<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LevelRepository.
 *
 * @package namespace App\Repositories\Customers;
 */
interface LevelRepository extends RepositoryInterface
{
    //
}
