<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Customers\HistoriesRepository;
use App\Models\Customers\Histories;
use App\Validators\Customers\HistoriesValidator;

/**
 * Class HistoriesRepositoryEloquent.
 *
 * @package namespace App\Repositories\Customers;
 */
class HistoriesRepositoryEloquent extends BaseRepository implements HistoriesRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Histories::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {
        return HistoriesValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    protected $fieldSearchable = [
        'customer_id',
    ];
}
