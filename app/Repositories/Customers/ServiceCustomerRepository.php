<?php
namespace App\Repositories\Customers;

use App\Models\Customers\Customer;
use App\Models\Customers\CustomerSharing;
use App\Models\Customers\CustomerSharingLog;
use App\Models\Customers\CustomerSharingUser;
use App\Models\Users\User;
use App\Notifications\CustomerNotifications;
use App\Services\AuthService;
use App\Services\ResponseService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Mail;

/**
 * Created by PhpStorm.
 * User: Cuongnt
 * Date: 17/08/2018
 * Time: 2:50 CH
 */

class ServiceCustomerRepository {
    private $model;

    private $request;

    private $response;

    private $user;

    protected $auth;

    private $perpages;

    private $current;

    function __construct(Customer $customer, ResponseService $response, Request $request, AuthService $auth, User $user, $companyId = 0, $perpages = 20, $current = 1) {
        $this->model    = $customer;
        $this->user     = $user;
        $this->response = $response;
        $this->request  = $request;
        $this->auth     = $auth;
        $this->perpages = $perpages;
        $this->current  = $current;
    }

    public function getShareCustomerByLeader() {
        $listShare = CustomerSharing::orderBy('id', 'desc')->pluck('name', 'id')->toArray();

        $listSale = $this->user->getAllChildUser(Auth::user()->id);
        $lstUser  = User::whereIn('id', $listSale)->orderBy('position_id')->get();
        return view('administrator.customers.store_customer.share-by-leader', compact('listShare', 'listSale', 'lstUser'));
    }

    public function getTotalCustomerByShare($id) {
        $totalCustomer = Customer::where(Customer::SHARE_ID, $id)
            ->where(Customer::CREATED_BY, Auth::user()->id)
            ->whereNull(Customer::PARENT_ID);

        $html = view('administrator.customers.store_customer.list-customer', ['totalCustomer' => $totalCustomer->paginate(5)])->render();
        return $this->response->json(200, ['total' => $totalCustomer->count(), 'html' => $html], '');
    }

    public function postShareCustomerByLeader() {
        $input           = $this->request->all();
        $listShare       = CustomerSharing::pluck('name', 'id')->toArray();
        $listSale        = $input['sales'];
        $customerSharing = CustomerSharing::find($input['share_id']);
        if (!$input['share_id']) {
            return $this->alertMessage('Bạn chưa chọn chiến dịch');
        }
        if (!$input['number_customer_input']) {
            return $this->alertMessage('Bạn phải nhập số lượng giao');
        }
        if ($input['number_customer_input'] > $input['total_customer']) {
            return $this->alertMessage('Số lượng bạn nhập nhiều hơn số lượng trong chiến dịch');
        }
        if (count($listSale) < 1) {
            return $this->alertMessage('Bạn không có nhân viên nào.');
        }
        $result = $this->assignSale($input);
        $html   = '';
        foreach ($result as $key => $value) {
            $sale = User::find($key);
            $html .= 'Tài khoản ' . $sale->fullname . ' được nhận thêm ' . count($value) . ' khách hàng <br/>';
            $sale->notify(new CustomerNotifications($sale->id, 'CRM Quản lý khách hàng thông báo',
                'Bạn vừa được bàn giao  ' . count($value) . 'KH từ chiến dịch: ' . $customerSharing->name));
        }

        return redirect()->route('customer.get-share-customer-by-leader')->with('message', $html);
    }

    private function assignSale($input) {
        $listsale = CustomerSharingUser::whereIn('user_id', $input['sales'])
            ->where('cus_sharing_id', $input['share_id'])
            ->orderBy('created_at', 'desc')->get()->pluck('user_id')->toArray();
        //sắp xếp những người đã nhân rồi trong 1 chiến dịch xuống dưới
        if (count($listsale) > 0) {
            foreach ($input['sales'] as $key => $item) {
                if (in_array($item, $listsale)) {
                    unset($input['sales'][$key]);
                }
            }
        }
        $totalSale = $input['sales'] + $listsale;
        $sales     = $totalSale;
        $listSale  = $totalSale;
        //lay lai list sale order by theo tổng số được đổ asc

        $getListCustomer = Customer::where(Customer::SHARE_ID, $input['share_id'])
            ->where(Customer::CREATED_BY, Auth::user()->id)
            ->whereNull(Customer::PARENT_ID)
            ->skip(0)->take($input['number_customer_input'])
            ->orderBy(Customer::CREATED_AT, 'asc')->get()->toArray();
        $result = array();
        foreach ($getListCustomer as $key => $value) {
            if (count($sales) < 1) {
                $sales = $listSale;
            }
            foreach ($sales as $k => $sale_id) {

                $result[$sale_id][]     = $value['id'];
                $customer               = Customer::find($value['id']);
                $customer->user_id      = $sale_id;
                $customer->created_by   = $sale_id;
                $customer->total_assign = $customer->total_assign + 1;
                $customer->assign_at    = date('Y-m-d H:i:s', strtotime(Carbon::now()));
                $customer->save();
                // update log
                $comment = "Khách hàng được Quản lý của bạn chuyển cho. Quản lý: " . Auth::user()->fullname;
                $this->addSharingLog($value['id'], $sale_id, $input['share_id'], $comment);
                // update contact vào bảng chien dich
                $this->updateCustomerShareUser($sale_id, $input['share_id'], $value['id']);
                unset($sales[$k]);
                break;
            }
        }
        return $result;
    }

    private function updateCustomerShareUser($saleId, $shareId, $customerId) {
        $shareUser = CustomerSharingUser::where('user_id', $saleId)
            ->where('cus_sharing_id', $shareId)->first();
        $getCustomerShareUser = CustomerSharingUser::where('user_id', Auth::user()->id)
            ->where('cus_sharing_id', $shareId)->first();
        if ($shareUser) {
            $log                       = $shareUser->params;
            $key                       = count($shareUser->params) + 1;
            $shareUser['number_share'] = $shareUser->number_share + 1;
            $shareUser['parent_id']    = $getCustomerShareUser->id;
            $shareUser['updated_by']   = Auth::user()->id;
            $log[$key]['customer_id']  = $customerId;
            $log[$key]['assign_at']    = date('Y-m-d H:i:s', strtotime(Carbon::now()));
            $shareUser->params         = $log;
            $shareUser->save();
        } else {
            $customerShare                   = new CustomerSharingUser();
            $customerShare['user_id']        = $saleId;
            $customerShare['cus_sharing_id'] = $shareId;
            $customerShare['parent_id']      = $getCustomerShareUser->id;
            $customerShare['number_share']   = 1;
            $customerShare['created_by']     = Auth::user()->id;
            $log                             = [];
            $log["1"]['customer_id']         = $customerId;
            $log["1"]['assign_at']           = date('Y-m-d H:i:s', strtotime(Carbon::now()));
            $customerShare->params           = $log;
            $customerShare->save();

        }
    }

    private function addSharingLog($id, $saleId, $shareId, $comment) {
        $sharingLog              = CustomerSharingLog::where(CustomerSharingLog::CUSTOMER_ID, $id)->first();
        $log                     = $sharingLog->params;
        $key                     = count($sharingLog->params) + 1;
        $log[$key]['assign_by']  = Auth::user()->id;
        $log[$key]['sharing_id'] = $shareId;
        $log[$key]['sale_id']    = $saleId;
        $log[$key]['comment']    = $comment;
        $log[$key]['created_at'] = date('Y-m-d H:i:s', strtotime(Carbon::now()));
        $sharingLog->params      = $log;
        $sharingLog->updated_by  = Auth::user()->id;
        $sharingLog->save();
    }

    public function alertMessage($message) {
        return redirect()->route('customer.get-share-customer-by-leader')->with('error', $message);
    }

    public function searchCustomer() {

    }

    public function deactiveCutomer($id, $type = 0) {
        $customer = $this->model->find($id);
        if ($type == 1) {
            $this->request['description'] = 'Khách hàng không có nhu cầu chăm sóc.';
            $content                      = 'Khách hàng: ' . $customer->fullname . ' Vừa bị thu hồi do bạn cập nhật trạng thái khách hàng không có nhu cầu.';
        } else {
            $content = 'Khách hàng: ' . $customer->fullname . '. Vừa bị thu hồi bởi Telesale: ' . Auth::user()->fullname;
        }
        //nitification
        $user = User::find($customer->created_by);
        $user->notify(new CustomerNotifications($user->id, 'CRM Quản lý khách hàng thông báo', $content));
        self::updateSharingUser($customer->share_id, $customer->created_by, $id);
        // Update customer_sharing_logs
        self::updateSharingLog($id, $customer->share_id, $customer->created_by);
        // Update null for created_by customers
        self::updateCustomer($id);
        return true;
    }
    private function updateSharingUser($shareId, $saleId, $customerId) {
        $sharingUsers = CustomerSharingUser::where(CustomerSharingUser::CUS_SHARING_ID, $shareId)
            ->where(CustomerSharingUser::USER_ID, $saleId)->first();
        $recoverList = $sharingUsers->recover_list;
        if ($recoverList == NULL) {
            $key = 1;
        } else {
            $key = count($sharingUsers->recover_list) + 1;
        }
        $recoverList[$key]['customer_id'] = (int) $customerId;
        $recoverList[$key]['created_by']  = (int) ADMIN_ID;
        $recoverList[$key]['created_at']  = date('Y-m-d H:i:s', strtotime(Carbon::now()));
        $recoverList[$key]['message']     = $this->request->description;
        $recoverList[$key]['file']        = $this->request->file_collection;
        $sharingUsers->recover_list       = $recoverList;
        $sharingUsers->number_recover     = $key;

        $sharingUsers->save();
    }

    private function updateSharingLog($id, $shareId, $saleId) {
        $sharingLog = CustomerSharingLog::where(CustomerSharingLog::CUSTOMER_ID, $id)->first();
        $log        = $sharingLog->params;
        if ($log == NULL) {
            $key = 1;
        } else {
            $key = count($sharingLog->params) + 1;
        }
        $log[$key]['assign_by']  = Auth::user()->id;
        $log[$key]['sharing_id'] = (int) $shareId;
        $log[$key]['sale_id']    = null;
        $log[$key]['comment']    = $this->request->description;
        $log[$key]['created_at'] = date('Y-m-d H:i:s  ', strtotime(Carbon::now()));
        $sharingLog->params      = $log;
        $sharingLog->updated_by  = ADMIN_ID;
        $sharingLog->save();
    }

    private function updateCustomer($id) {
        Customer::where('id', $id)->update(['user_id' => NULL, 'created_by' => NULL, 'updated_by' => ADMIN_ID, 'share_id' => NULL]);

    }
}