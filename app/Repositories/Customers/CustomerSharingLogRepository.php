<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CustomerSharingLogRepository.
 *
 * @package namespace App\Repositories;
 */
interface CustomerSharingLogRepository extends RepositoryInterface
{
    //
}
