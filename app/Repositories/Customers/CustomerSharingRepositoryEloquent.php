<?php

namespace App\Repositories\Customers;

use App\Models\Customers\CustomerSharing;
use App\Validators\Customers\CustomerSharingValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;


/**
 * Class CustomerSharingRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CustomerSharingRepositoryEloquent extends BaseRepository implements CustomerSharingRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CustomerSharing::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return CustomerSharingValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
