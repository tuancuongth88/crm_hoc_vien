<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HistoriesRepository.
 *
 * @package namespace App\Repositories\Customers;
 */
interface HistoriesRepository extends RepositoryInterface
{
    //
}
