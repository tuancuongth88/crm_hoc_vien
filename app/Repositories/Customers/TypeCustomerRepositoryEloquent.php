<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Customers\TypeCustomerRepository;
use App\Models\Customers\TypeCustomer;
use App\Validators\Customers\TypeCustomerValidator;

/**
 * Class TypeCustomerRepositoryEloquent.
 *
 * @package namespace App\Repositories\Customers;
 */
class TypeCustomerRepositoryEloquent extends BaseRepository implements TypeCustomerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TypeCustomer::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return TypeCustomerValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
