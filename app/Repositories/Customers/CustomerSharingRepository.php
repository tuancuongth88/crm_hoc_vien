<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CustomerSharingRepository.
 *
 * @package namespace App\Repositories;
 */
interface CustomerSharingRepository extends RepositoryInterface
{
    //
}
