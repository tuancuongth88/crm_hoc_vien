<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Customers\AccountsRepository;
use App\Models\Customers\Accounts;
use App\Validators\Customers\AccountsValidator;

/**
 * Class AccountsRepositoryEloquent.
 *
 * @package namespace App\Repositories\Customers;
 */
class AccountsRepositoryEloquent extends BaseRepository implements AccountsRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Accounts::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return AccountsValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
