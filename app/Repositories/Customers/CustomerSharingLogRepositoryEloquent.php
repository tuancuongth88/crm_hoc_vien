<?php

namespace App\Repositories\Customers;

use App\Models\Customers\CustomerSharingLog;
use App\Validators\CustomerSharingLogValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;


/**
 * Class CustomerSharingLogRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CustomerSharingLogRepositoryEloquent extends BaseRepository implements CustomerSharingLogRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CustomerSharingLog::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return CustomerSharingLogValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
