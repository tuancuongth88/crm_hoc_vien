<?php

namespace App\Repositories\Customers;

use App\Models\Customers\CustomerSharingUser;
use App\Validators\Customers\CustomerSharingUserValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class CustomerSharingUserRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CustomerSharingUserRepositoryEloquent extends BaseRepository implements CustomerSharingUserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return CustomerSharingUser::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return CustomerSharingUserValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
