<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TypeCustomerRepository.
 *
 * @package namespace App\Repositories\Customers;
 */
interface TypeCustomerRepository extends RepositoryInterface
{
    //
}
