<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CustomerSharingUserRepository.
 *
 * @package namespace App\Repositories;
 */
interface CustomerSharingUserRepository extends RepositoryInterface
{
    //
}
