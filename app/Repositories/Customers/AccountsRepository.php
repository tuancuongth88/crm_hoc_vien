<?php

namespace App\Repositories\Customers;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AccountsRepository.
 *
 * @package namespace App\Repositories\Customers;
 */
interface AccountsRepository extends RepositoryInterface
{
    //
}
