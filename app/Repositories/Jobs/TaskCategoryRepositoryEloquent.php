<?php

namespace App\Repositories\Jobs;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Jobs\TaskCategoryRepository;
use App\Models\Jobs\TaskCategory;
use App\Validators\Jobs\TaskCategoryValidator;

/**
 * Class TaskCategoryRepositoryEloquent.
 *
 * @package namespace App\Repositories\Jobs;
 */
class TaskCategoryRepositoryEloquent extends BaseRepository implements TaskCategoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return TaskCategory::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return TaskCategoryValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
