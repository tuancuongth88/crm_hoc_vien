<?php

namespace App\Repositories\Jobs;

use App\Models\Jobs\Plan;
use App\Repositories\Jobs\PlanRepository;
use App\Validators\Jobs\PlanValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PlanRepositoryEloquent.
 *
 * @package namespace App\Repositories\Jobs;
 */
class PlanRepositoryEloquent extends BaseRepository implements PlanRepository {

    /**
     * Get list array format data
     *
     */
    public static $modelList = [
        'call_customer_potential'=> 'Gọi điện cho khách hàng tiềm năng',
        'send_sms'               => 'Gửi tin nhắn cho khách hàng',
        'send_email'             => 'Gửi email cho khách hàng',
        'total_social'           => 'Tương tác Social',
        'post_website'           => 'Đăng tin dự án trên Website cá nhân',
        'post_facebook'          => 'Đăng tin Face & Fanpage',
//        'post_news'              => 'Đăng tin Trang BDS',
//        'post_forum'             => 'Đăng tin Forum',
        'post_news_and_forum'    => 'Đăng tin trang BĐS + Forum',
        'flypaper_personal'      => 'Phát tờ rơi cá nhân',
        'flypaper_group'         => 'Phát tờ rơi & treo phướn tập thể',
        'questionair'            => 'Thực hiện Questionair tìm kiếm khách hàng tiềm năng cá nhân',
//        'guard_project_in_hour'  => 'Trực dự án và nhà mẫu trong giờ hành chính',
//        'guard_project_out_hour' => 'Trực dự án và nhà mẫu ngoài giờ hành chính',
        'guard_project'          => 'Trực dự án và nhà mẫu',
        'open_sale'              => 'Tham gia sự kiện mở bán và giới thiệu dự án phân phối',
        'roadshow'               => 'Tham gia các hoạt động Roadshow, quảng cáo',
        'event'                  => 'Tham gia sự kiện, event, hội thảo bên ngoài',
        'find_public'            => 'Tìm kiếm khách hàng tại các địa điểm công cộng',
        'setup_appointment'      => 'Thiết lập cuộc hẹn tư vấn khách hàng',
        'total'                  => 'Tổng kết, đánh giá công việc',
        'like'                   => 'Hoạt động truyền thông like bài viết',
        'comment'                => 'Hoạt động truyền thông comment bài viết',
        'share'                  => 'Hoạt động truyền thông share bài viết',
        'update_info_market'     => 'Cập nhật thông tin thị trường (Cafef, Vneconomy.vn, Tapchitaichinh.vn, Batdongsan.com.vn, Cafeland.vn)',
        'update_info_project'    => 'Cập nhật thông tin dự án (Email của sàn, Trao đổi với quản lý)',
        'kpi_levels_1'           => 'Khách hàng quan tâm',
        'kpi_levels_2'           => 'Khách hàng gặp tư vấn trực tiếp',
        'kpi_levels_3'           => 'Khách hàng chốt giao dịch',
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return Plan::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator() {

        return PlanValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
