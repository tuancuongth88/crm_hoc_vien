<?php

namespace App\Repositories\Jobs;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TargetRepository.
 *
 * @package namespace App\Repositories\Jobs;
 */
interface TargetRepository extends RepositoryInterface
{
    //
}
