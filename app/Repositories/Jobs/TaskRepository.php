<?php

namespace App\Repositories\Jobs;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TaskRepository.
 *
 * @package namespace App\Repositories\Jobs;
 */
interface TaskRepository extends RepositoryInterface
{
    //
}
