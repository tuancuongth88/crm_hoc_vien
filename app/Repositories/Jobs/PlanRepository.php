<?php

namespace App\Repositories\Jobs;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PlanRepository.
 *
 * @package namespace App\Repositories\Jobs;
 */
interface PlanRepository extends RepositoryInterface
{
    //
}
