<?php

namespace App\Repositories\Jobs;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReportRepository.
 *
 * @package namespace App\Repositories\Jobs;
 */
interface ReportRepository extends RepositoryInterface
{
    //
}
