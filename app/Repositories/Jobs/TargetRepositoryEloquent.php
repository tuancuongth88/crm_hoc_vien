<?php

namespace App\Repositories\Jobs;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Jobs\TargetRepository;
use App\Models\Jobs\Target;
use App\Validators\Jobs\TargetValidator;

/**
 * Class TargetRepositoryEloquent.
 *
 * @package namespace App\Repositories\Jobs;
 */
class TargetRepositoryEloquent extends BaseRepository implements TargetRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Target::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return TargetValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
