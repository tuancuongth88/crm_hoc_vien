<?php

namespace App\Repositories\Jobs;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TaskCategoryRepository.
 *
 * @package namespace App\Repositories\Jobs;
 */
interface TaskCategoryRepository extends RepositoryInterface
{
    //
}
