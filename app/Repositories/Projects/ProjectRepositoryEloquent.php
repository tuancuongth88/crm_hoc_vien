<?php

namespace App\Repositories\Projects;

use App\Models\Projects\Project;
use App\Repositories\Projects\ProjectRepository;
use App\Validators\Projects\ProjectValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class ProjectRepositoryEloquent.
 *
 * @package namespace App\Repositories\Projects;
 */
class ProjectRepositoryEloquent extends BaseRepository implements ProjectRepository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return Project::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator() {

        return ProjectValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    protected $fieldSearchable = [
        Project::NAME    => 'like',
        Project::ADDRESS => 'like',
    ];

}
