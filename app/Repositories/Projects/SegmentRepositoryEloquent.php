<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Projects\SegmentRepository;
use App\Models\Projects\Segment;
use App\Validators\Projects\SegmentValidator;

/**
 * Class SegmentRepositoryEloquent.
 *
 * @package namespace App\Repositories\Projects;
 */
class SegmentRepositoryEloquent extends BaseRepository implements SegmentRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Segment::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return SegmentValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
