<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsCategoryRepository.
 *
 * @package namespace App\Repositories\Projects;
 */
interface NewsCategoryRepository extends RepositoryInterface
{
    //
}
