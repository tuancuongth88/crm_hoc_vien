<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectManagerRepository.
 *
 * @package namespace App\Repositories;
 */
interface ProjectManagerRepository extends RepositoryInterface
{
    //
}
