<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsRepository.
 *
 * @package namespace App\Repositories\Projects;
 */
interface NewsRepository extends RepositoryInterface
{
    //
}
