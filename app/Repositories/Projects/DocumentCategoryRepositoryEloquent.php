<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Projects\DocumentCategoryRepository;
use App\Models\Projects\DocumentCategory;
use App\Validators\Projects\DocumentCategoryValidator;

/**
 * Class DocumentCategoryRepositoryEloquent.
 *
 * @package namespace App\Repositories\Projects;
 */
class DocumentCategoryRepositoryEloquent extends BaseRepository implements DocumentCategoryRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return DocumentCategory::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return DocumentCategoryValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
