<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DocumentRepository.
 *
 * @package namespace App\Repositories\Projects;
 */
interface DocumentRepository extends RepositoryInterface
{
    //
}
