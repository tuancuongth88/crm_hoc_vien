<?php

namespace App\Repositories\Projects;

use App\Models\Projects\ProjectManager;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Validators\ProjectManagerValidator;

/**
 * Class ProjectManagerRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProjectManagerRepositoryEloquent extends BaseRepository implements ProjectManagerRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ProjectManager::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ProjectManagerValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
