<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DocumentCategoryRepository.
 *
 * @package namespace App\Repositories\Projects;
 */
interface DocumentCategoryRepository extends RepositoryInterface
{
    //
}
