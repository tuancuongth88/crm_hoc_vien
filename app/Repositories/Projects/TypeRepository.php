<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TypeRepository.
 *
 * @package namespace App\Repositories\Projects;
 */
interface TypeRepository extends RepositoryInterface
{
    //
}
