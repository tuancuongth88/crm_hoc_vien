<?php

namespace App\Repositories\Projects;

use App\Models\Projects\News;
use App\Repositories\Projects\NewsRepository;
use App\Validators\Projects\NewsValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class NewsRepositoryEloquent.
 *
 * @package namespace App\Repositories\Projects;
 */
class NewsRepositoryEloquent extends BaseRepository implements NewsRepository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return News::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator() {

        return NewsValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    protected $fieldSearchable = [
        News::TITLE       => 'like',
        News::DESCRIPTION => 'like',
    ];

}
