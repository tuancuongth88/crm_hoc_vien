<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SegmentRepository.
 *
 * @package namespace App\Repositories\Projects;
 */
interface SegmentRepository extends RepositoryInterface
{
    //
}
