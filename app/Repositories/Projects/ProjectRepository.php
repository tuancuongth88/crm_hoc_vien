<?php

namespace App\Repositories\Projects;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectRepository.
 *
 * @package namespace App\Repositories\Projects;
 */
interface ProjectRepository extends RepositoryInterface
{
    //
}
