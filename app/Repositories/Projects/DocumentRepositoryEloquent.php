<?php

namespace App\Repositories\Projects;

use App\Models\Projects\Document;
use App\Repositories\Projects\DocumentRepository;
use App\Validators\Projects\DocumentValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class DocumentRepositoryEloquent.
 *
 * @package namespace App\Repositories\Projects;
 */
class DocumentRepositoryEloquent extends BaseRepository implements DocumentRepository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return Document::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator() {

        return DocumentValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    protected $fieldSearchable = [
        Document::NAME       => 'like',
        Document::EXTENDSION => 'like',
        Document::CATEGORY_ID,
    ];

}
