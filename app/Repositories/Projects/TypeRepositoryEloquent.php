<?php

namespace App\Repositories\Projects;

use App\Models\Projects\Type;
use App\Repositories\Projects\TypeRepository;
use App\Validators\Projects\TypeValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class TypeRepositoryEloquent.
 *
 * @package namespace App\Repositories\Projects;
 */
class TypeRepositoryEloquent extends BaseRepository implements TypeRepository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return Type::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator() {

        return TypeValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    protected $fieldSearchable = [
        Type::NAME => 'like',
    ];

}
