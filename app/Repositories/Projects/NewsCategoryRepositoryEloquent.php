<?php

namespace App\Repositories\Projects;

use App\Models\Projects\NewsCategory;
use App\Repositories\Projects\NewsCategoryRepository;
use App\Validators\Projects\NewsCategoryValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class NewsCategoryRepositoryEloquent.
 *
 * @package namespace App\Repositories\Projects;
 */
class NewsCategoryRepositoryEloquent extends BaseRepository implements NewsCategoryRepository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return NewsCategory::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator() {

        return NewsCategoryValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    protected $fieldSearchable = [
        NewsCategory::NAME => 'like',
    ];

}
