<?php

namespace App\Repositories\Education;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Education\ExamResultRepository;
use App\Models\Education\ExamResult;
use App\Validators\Education\ExamResultValidator;

/**
 * Class ExamResultRepositoryEloquent.
 *
 * @package namespace App\Repositories\Education;
 */
class ExamResultRepositoryEloquent extends BaseRepository implements ExamResultRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExamResult::class;
    }

    

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
