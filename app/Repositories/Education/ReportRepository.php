<?php

namespace App\Repositories\Education;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ReportRepository.
 *
 * @package namespace App\Repositories\Education;
 */
interface ReportRepository extends RepositoryInterface
{
    //
}
