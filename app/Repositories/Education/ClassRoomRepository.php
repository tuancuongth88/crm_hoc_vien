<?php

namespace App\Repositories\Education;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ClassRoomRepository.
 *
 * @package namespace App\Repositories\Education;
 */
interface ClassRoomRepository extends RepositoryInterface
{
    //
}
