<?php

namespace App\Repositories\Education;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SubjectRepository.
 *
 * @package namespace App\Repositories\Educations;
 */
interface SubjectRepository extends RepositoryInterface
{
    //
}
