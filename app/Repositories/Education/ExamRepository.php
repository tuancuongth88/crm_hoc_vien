<?php

namespace App\Repositories\Education;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ExamRepository.
 *
 * @package namespace App\Repositories\Education;
 */
interface ExamRepository extends RepositoryInterface
{
    //
}
