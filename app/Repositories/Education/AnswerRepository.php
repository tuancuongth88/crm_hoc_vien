<?php

namespace App\Repositories\Education;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AnswerRepository.
 *
 * @package namespace App\Repositories\Education;
 */
interface AnswerRepository extends RepositoryInterface
{
    //
}
