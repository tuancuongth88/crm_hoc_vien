<?php

namespace App\Repositories\Education;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProgramRepository.
 *
 * @package namespace App\Repositories\Education;
 */
interface ProgramRepository extends RepositoryInterface
{
    //
}
