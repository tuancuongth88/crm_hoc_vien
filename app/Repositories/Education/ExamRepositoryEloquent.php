<?php

namespace App\Repositories\Education;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Education\ExamRepository;
use App\Models\Education\Exam;
use App\Validators\Education\ExamValidator;

/**
 * Class ExamRepositoryEloquent.
 *
 * @package namespace App\Repositories\Education;
 */
class ExamRepositoryEloquent extends BaseRepository implements ExamRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Exam::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ExamValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
