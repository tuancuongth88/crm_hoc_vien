<?php

namespace App\Repositories\Education;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ExamResultRepository.
 *
 * @package namespace App\Repositories\Education;
 */
interface ExamResultRepository extends RepositoryInterface
{
    //
}
