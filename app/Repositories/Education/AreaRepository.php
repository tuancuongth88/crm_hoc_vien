<?php

namespace App\Repositories\Education;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface AreaRepository.
 *
 * @package namespace App\Repositories\Education;
 */
interface AreaRepository extends RepositoryInterface
{
    //
}
