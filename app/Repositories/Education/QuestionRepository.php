<?php

namespace App\Repositories\Education;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface QuestionRepository.
 *
 * @package namespace App\Repositories\Education;
 */
interface QuestionRepository extends RepositoryInterface
{
    //
}
