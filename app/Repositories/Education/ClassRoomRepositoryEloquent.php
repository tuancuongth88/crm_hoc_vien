<?php

namespace App\Repositories\Education;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Education\ClassRoomRepository;
use App\Models\Education\ClassRoom;
use App\Validators\Education\ClassRoomValidator;

/**
 * Class ClassRoomRepositoryEloquent.
 *
 * @package namespace App\Repositories\Education;
 */
class ClassRoomRepositoryEloquent extends BaseRepository implements ClassRoomRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ClassRoom::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return ClassRoomValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    
}
