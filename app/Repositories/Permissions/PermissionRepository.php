<?php

namespace App\Repositories\Permissions;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PermissionRepository.
 *
 * @package namespace App\Repositories\Permissions;
 */
interface PermissionRepository extends RepositoryInterface
{
    //
}
