<?php

namespace App\Repositories\Educations;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LessonRepository.
 *
 * @package namespace App\Repositories\Educations;
 */
interface LessonRepository extends RepositoryInterface
{
    //
}
