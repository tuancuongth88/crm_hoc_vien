<?php

namespace App\Repositories\Systems;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface CompanyRepository.
 *
 * @package namespace App\Repositories\Systems;
 */
interface CompanyRepository extends RepositoryInterface
{
    //
}
