<?php

namespace App\Repositories\Systems;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PositionRepository.
 *
 * @package namespace App\Repositories\Systems;
 */
interface PositionRepository extends RepositoryInterface
{
    //
}
