<?php

namespace App\Repositories\Systems;

use App\Models\Systems\Position;
use App\Repositories\Systems\PositionRepository;
use App\Validators\Systems\PositionValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class PositionRepositoryEloquent.
 *
 * @package namespace App\Repositories\Systems;
 */
class PositionRepositoryEloquent extends BaseRepository implements PositionRepository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return Position::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator() {

        return PositionValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    protected $fieldSearchable = [
        Position::NAME => 'like',
    ];

}
