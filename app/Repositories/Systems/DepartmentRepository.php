<?php

namespace App\Repositories\Systems;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface DepartmentRepository.
 *
 * @package namespace App\Repositories\Systems;
 */
interface DepartmentRepository extends RepositoryInterface
{
    //
}
