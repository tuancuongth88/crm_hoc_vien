<?php

namespace App\Repositories\Users;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface EducationUserInfoRepository.
 *
 * @package namespace App\Repositories;
 */
interface EducationUserInfoRepository extends RepositoryInterface
{
    //
}
