<?php

namespace App\Repositories\Users;

use App\Models\Users\EducationUserInfo;
use App\Validators\Users\EducationUserInfoValidator;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;


/**
 * Class EducationUserInfoRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class EducationUserInfoRepositoryEloquent extends BaseRepository implements EducationUserInfoRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return EducationUserInfo::class;
    }

    /**
    * Specify Validator class name
    *
    * @return mixed
    */
    public function validator()
    {

        return EducationUserInfoValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

}
