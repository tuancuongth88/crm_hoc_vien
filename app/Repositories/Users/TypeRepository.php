<?php

namespace App\Repositories\Users;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TypeRepository.
 *
 * @package namespace App\Repositories\Users;
 */
interface TypeRepository extends RepositoryInterface
{
    //
}
