<?php

namespace App\Repositories\Users;

use App\Models\Users\User;
use App\Repositories\Users\UserRepository;
use App\Validators\Users\UserValidator;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class UserRepositoryEloquent.
 *
 * @package namespace App\Repositories\Users;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository {
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model() {
        return User::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator() {

        return UserValidator::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot() {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    protected $fieldSearchable = [
        User::FULLNAME => 'like',
        User::EMAIL,
    ];

}
