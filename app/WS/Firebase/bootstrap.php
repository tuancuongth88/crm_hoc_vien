<?php

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
$serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/config/google-service-account.json');

$firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    ->withDatabaseUri('https://crm-land.firebaseio.com')
    ->create();
$database = $firebase->getDatabase();

$GLOBALS['database_firebase'] = $database;