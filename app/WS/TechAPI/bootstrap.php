<?php
require_once realpath(__DIR__) . '/Autoload.php';

TechAPIAutoloader::register();

use TechAPI\Constant;
use TechAPI\Client;
use TechAPI\Auth\ClientCredentials;

// config api
Constant::configs(array(
    'mode'            => Constant::MODE_LIVE,
//    'mode'            => Constant::MODE_SANDBOX,
    'connect_timeout' => 15,
    'enable_cache'    => false,
    'enable_log'      => true,
    'log_path'    =>  realpath($_SERVER['DOCUMENT_ROOT']).APP_LOG_SMS,
));


// config client and authorization grant type
function getTechAuthorization()
{
    $client = new Client(
        env('CLIEND_ID'),
        env('CLIENT_SECRET'),
        array('send_brandname_otp')
    );

    return new ClientCredentials($client);
}