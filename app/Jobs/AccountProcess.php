<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class AccountProcess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $account;
    public $action;
    private $database;
    /**
     * Số lần job sẽ thử thực hiện lại
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Số giây job có thể chạy trước khi timeout
     *
     * @var int
     */
    public $timeout = 30;
    /**
     * Create a new job instance.
     *
     * @return void
     */

    public function __construct($account, $action)
    {
        $this->account = $account;
        $this->action  = $action;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->database = $GLOBALS['database_firebase']->getReference('accounts/'.$this->account->id);

        switch ($this->action){
            case 'store':
                $this->store();
                break;
            case 'update':
                $this->update();
                break;
            case 'destroy':
                $this->destroy();
                break;
        }
    }

    public function store(){
        $data = $this->account->toArray();
        $data['count_notification']  = 0;
        $data['last_send_time']  = 0;
        $this->database->set($data);
    }
    public function update(){
        $this->database->update($this->account->toArray());
    }
    public function destroy(){
        $this->database->remove();
        $this->destroyChatAccount();
    }

    public function destroyChatAccount(){
        $databaseChat = $GLOBALS['database_firebase']->getReference('messages/'.$this->account->phone);
        $databaseChat->remove();
    }

}
