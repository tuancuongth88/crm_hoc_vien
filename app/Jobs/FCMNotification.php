<?php

namespace App\Jobs;


use App\Models\Customers\AccountFireBase;
use App\Models\Customers\FollowerNews;
use App\Models\Customers\NotificationFirebase;
use App\Models\Projects\News;
use App\Models\Projects\ProjectAccount;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class FCMNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $news;
    /**
     * Số lần job sẽ thử thực hiện lại
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Số giây job có thể chạy trước khi timeout
     *
     * @var int
     */
    public $timeout = 30;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(News $news)
    {
        $this->news = $news;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->addAccountToNotification();
    }

    public function addAccountToNotification(){
        // lấy danh sách account follower
        $listAccount = FollowerNews::where('category_new_id', $this->news->category_id)->get();
        foreach ($listAccount as $account){
            //tin khuyem mai.
            if($this->news->category_id == 3){
                $this->processNews($account);
            }else{
                //check tai khoan co mua du an hay khong
                $checkProject = ProjectAccount::where('account_id', $account->account_id)->where('project_id', $this->news->project_id)->first();
                if($checkProject)
                {
                    $this->processNews($account);
                }
            }
        }
    }
    //ham xu ly cap nhan notification
    public function processNews($account){
        $input['account_id'] = $account->account_id;
        $input['news_id']    = $this->news->id;
        $notification = NotificationFirebase::create($input);
        // check account online hay không
        $listDeviceByAccount = AccountFireBase::where('account_id', $account->account_id)->get();
        foreach ($listDeviceByAccount as $item){
            $resultJson = $this->notification($item->token);
            //parser json
            $resultObj = json_decode($resultJson);
            $notification['device_id'] = $item->device_id;
            $notification['token'] = $item->token;
            $notification['response_code'] = $resultObj->success;
            $notification['results'] = $resultObj->success;
            if($resultObj->success > 0){
                $notification['results'] = $resultObj->results[0]->message_id;
            }else{
                $notification['results'] = $resultObj->results[0]->error;
            }
            $notification['send_at'] = Carbon::now();
            $notification->save();
        }
    }

    public function notification($token)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $notification = [
            'title' => $this->news->title,
            'slug' => $this->news->slug,
            'body' => $this->news->description,
            'sound' => true,
        ];
        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $notification
        ];

        $headers = [
            'Authorization: key=AIzaSyAaOmzSYeujKAJV0IU6pXDedMGI3ewf46U',
            'Content-Type: application/json'
        ];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

}
