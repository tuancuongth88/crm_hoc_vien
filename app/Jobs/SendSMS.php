<?php

namespace App\Jobs;

use App\Models\Systems\OptionSMS;
use App\Models\SendSmsLog;
use App\Services\Lib;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Response;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use function Psy\debug;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $account;
    public $password;
    /**
     * Số lần job sẽ thử thực hiện lại
     *
     * @var int
     */
    public $tries = 3;

    /**
     * Số giây job có thể chạy trước khi timeout
     *
     * @var int
     */
    public $timeout = 30;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($account, $password)
    {
        $this->account = $account;
        $this->password = $password;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->sendSMSCreateAccount($this->account, $this->password);
    }
    public function sendSMSCreateAccount($account, $password){
        $params = ['phone', 'password', 'android', 'ios'];
        $data['phone'] = $account->phone;
        $data['password'] = $password;
        $data['android'] = env('LINK_ANDROID');
        $data['ios'] = env('LINK_IOS');
        $template = OptionSMS::where('type', 1)->first();
        $content = render_template_sms($template->message, $data, $params);
        $resultSms = Lib::sendSMSOTP($account->phone, $content);
        $account['sms_code'] = $resultSms['sms_code'];
        if($resultSms['sms_code'] != 200){
            $account['sms_response'] = $resultSms['sms_message'];
        }
        $account->save();
        $this->logSendSMS($account, $content, $resultSms['sms_message']);
        return true;
    }

    public function logSendSMS($account, $content, $response){
        $log['model'] = class_basename($account);
        $log['item_id'] = $account->id;
        $log['user_id'] = $account->id;
        $log['phone']   = $account->phone;
        $log['content']   = $content;
        $log['sms_code']   = $account->sms_code;
        $log['sms_response']   = $response;
        $log['is_send']   = ONE;
        SendSmsLog::create($log);
    }


}
