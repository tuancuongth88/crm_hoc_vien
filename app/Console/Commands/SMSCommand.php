<?php

namespace App\Console\Commands;

use App\Jobs\SendSMS;
use App\Models\SendSmsLog;
use App\Services\Lib;
use Illuminate\Console\Command;

class SMSCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendsms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gửi sms';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sendSMSPromotion();
    }

    public function sendSMSPromotion(){
        $listPromotion = SendSmsLog::where(SendSmsLog::IS_SEND, 0)
                                    ->where('model', 'KHUYENMAI')->limit(100)->get();
        if($listPromotion->count() > 0){
            foreach ($listPromotion as $key => $value){
                $resultSms = Lib::sendSMSOTP($value->phone, $value->content);
                $value['sms_code'] = $resultSms['sms_code'];
                $value['sms_response'] =  $resultSms['sms_message'];
                $value['is_send'] = ONE;
                $value->save();
            }
        }
    }
}
