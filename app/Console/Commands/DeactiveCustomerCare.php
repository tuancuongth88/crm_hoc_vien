<?php

namespace App\Console\Commands;

use App\Models\Customers\Customer;
use App\Models\Customers\CustomerSharing;
use App\Models\Customers\CustomerSharingLog;
use App\Models\Customers\CustomerSharingUser;
use Illuminate\Console\Command;
use Carbon\Carbon;

class DeactiveCustomerCare extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeactiveCustomerCare:deactivecustomercare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check history customer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get customer not cared in week
        $expDate = Carbon::now()->subDays(7);
        $results = Customer::where(Customer::HISTORY_CARE_AT, '<=', $expDate)
                            ->whereNotNull(Customer::SHARE_ID)
                            ->whereNotNull(Customer::CREATED_BY)->get()->toArray();
        foreach ($results as $result) {
            $saleId            = $result['created_by'];
            $shareId           = $result['share_id'];
            $customerId        = $result['id'];
            if($shareId != NULL) {
                self::updateSharingUser($shareId, $saleId, $customerId);
                // Update customer_sharing_logs
                self::updateSharingLog($customerId, $shareId, $saleId);
                // Update null for created_by customers
                self::updateCustomer($customerId);
            }
        }
    }

    private function updateSharingUser($shareId, $saleId, $customerId)
    {
        $sharingUsers = CustomerSharingUser::where([CustomerSharingUser::CUS_SHARING_ID => $shareId, CustomerSharingUser::USER_ID => $saleId])->first();
        $recoverList  = $sharingUsers->recover_list;
        if($recoverList == NULL) {
            $key = 1;
        } else {
            $key = count($sharingUsers->recover_list) + 1;
        }
        $recoverList[$key]['customer_id'] = (int)$customerId;
        $recoverList[$key]['created_by']  = (int)ADMIN_ID;
        $recoverList[$key]['created_at']  =  date('Y-m-d H:i:s', strtotime(Carbon::now()));
        $recoverList[$key]['message']     = 'Khách hàng bị thu lại sau một tuần không chăm sóc';
        $recoverList[$key]['file']        = null;
        $sharingUsers->recover_list   = $recoverList;
        $sharingUsers->number_recover = $key;
        $sharingUsers->save();

    }

    private function updateSharingLog($id, $shareId, $saleId)
    {
        $sharingLog = CustomerSharingLog::where(CustomerSharingLog::CUSTOMER_ID, $id)->first();
        $log = $sharingLog->params;
        if($log == NULL) {
            $key = 1;
        } else {
            $key = count($sharingLog->params) + 1;
        }
        $log[$key]['assign_by'] = '1';
        $log[$key]['sharing_id'] = (int)$shareId;
        $log[$key]['sale_id'] = (int)$saleId;
        $log[$key]['comment'] = 'Thu khách hàng sau 1 tuần';
        $log[$key]['created_at'] = date('Y-m-d H:i:s  ', strtotime(Carbon::now()));
        $sharingLog->params = $log;
        $sharingLog->updated_by = ADMIN_ID;
        $sharingLog->save();
    }

    private function updateCustomer($id)
    {
        Customer::where('id', $id)->update(['user_id' => NULL, 'created_by' => NULL, 'updated_by' => ADMIN_ID, 'share_id' => NULL]);
    }
}
