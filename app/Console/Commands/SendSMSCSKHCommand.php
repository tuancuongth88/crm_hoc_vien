<?php

namespace App\Console\Commands;

use App\Models\Customers\Accounts;
use App\Models\SendSmsLog;
use App\Models\Systems\OptionSMS;
use App\Services\Lib;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class SendSMSCSKHCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendSmsCSKH';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Chay gui tin nhan cskh';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sendSMSBirthDay();
    }

    public function sendSMSBirthDay(){
        $listAccountBirthday = Accounts::whereRaw('DAYOFYEAR(curdate()) <= DAYOFYEAR(birthday) AND DAYOFYEAR(curdate()) + 7 >=  dayofyear(birthday)')
                                        ->orderByRaw('DAYOFYEAR(birthday)')->get();
        $template = OptionSMS::where(OptionSMS::TYPE, OptionSMS::TYPE_BIRTHDAY)->first();
        if($template){
            foreach ($listAccountBirthday as $key => $value){
                $resultSms = Lib::sendSMSOTP($value->phone, $template->message);
                $value['sms_code'] = $resultSms['sms_code'];
                $value['sms_response'] = $resultSms['sms_message'];
                $this->saveLog('Accounts', $value, $template);
            }
        }else{
            Log::error('Template SMS Birthday chưa được cấu hình');
        }
    }
    public function sendSMSNewYear(){
        $listAccount = Accounts::all();
        $template = OptionSMS::where(OptionSMS::TYPE, OptionSMS::TYPE_HAPPY_NEW_YEAR)->first();
        foreach ($listAccount as $key => $value){
            $resultSms = Lib::sendSMSOTP($value->phone, $template->message);
            $value['sms_code'] = $resultSms['sms_code'];
            $value['sms_response'] = $resultSms['sms_message'];
            $this->saveLog('Accounts', $value, $template);
        }
    }

    public function sendSMSDay8_3(){
        $listAccount = Accounts::where(Accounts::GENDER, Accounts::Ma);
        $template = OptionSMS::where(OptionSMS::TYPE, OptionSMS::TYPE_HAPPY_NEW_YEAR)->first();
    }
    public function sendSMSDay20_10(){

    }

    private function saveLog($model, $data, $template){
        $log['model'] = $model;
        $log['item_id'] = $data->id;
        $log['user_id'] = $data->id;
        $log['phone'] = $data->phone;
        $log['content'] = $template;
        $log['sms_code'] = $data['sms_code'];
        $log['sms_response'] =  $data['sms_message'];
        $log['is_send'] = ONE;
        SendSmsLog::create($log);
    }

}
