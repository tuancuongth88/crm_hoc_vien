<?php
define('PAGINATE', 20);
define('IMAGENEWS', '/upload/news/');
define('IMAGEUSER', '/upload/user/');
define('IMAGE_PROJECT', '/upload/project/');
define('JOB_ATTACHMENT', '/upload/job/');
define('ONE', 1);
define('ZERO', 0);
define('IMAGE_COMPANY', '/upload/company/');
define('IMAGE_CUSTOMER', '/upload/customer/');
define('IMAGE_PROGRAM', '/upload/program/');
define('EDUCATION_FILE_LESSON', '/upload/education/lesson/');
define('EDUCATION_FILE_SUBJECT', '/upload/education/subject/');
define('APPROVE', 1);
define('STATUS_ONE',1);
define('STATUS_ZERO',0);
define('ISHOT_ONE',1);
define('ISHOT_ZERO',0);

//active user
define('USER_ACTIVE', 1);


//Thang điểm
define('EDUCATION_EXAM_WIDTH_POINT', 10);

define('NO_AVATAR', '/no-avatar.ico');

define('ADMIN_ID', '1');

define('NAME_SHARING_DEFAULT', 'Danh sách khách hàng');

// config default kpi
define('KPI_LEVELS_1', 200);
define('KPI_LEVELS_2', 20);
define('KPI_LEVELS_3', 7);

define('CODE_SUCCESS', 200);
define('CODE_CREATE_FAILED', 201);
define('CODE_MULTI_STATUS', 207);
define('CODE_NO_ACCESS', 403);
define('CODE_NOT_FOUND', 404);
define('CODE_ERROR_SERVER', 500);
define('CODE_UNAUTHORIZED', 401);


define('DEFAULT_PASSWORD', 123456);

define('APP_LOG_SMS', '/storage/logs/sms/');
