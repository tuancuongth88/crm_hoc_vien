<?php

namespace App\Http\Controllers\AppHelperDesk;

use App\Services\ResponseService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class CustomerTmpController extends Controller
{
    private $response;
    public function __construct(ResponseService $response)
    {
        $this->response = $response;
    }

    public function index(){
        $data = DB::table('customer_promotion_tmp')->orderBy('id', 'desc')->paginate();

        return view('administrator.app-helpdesk.customer_tmp.index', compact('data'));
    }

    public function search(Request $request){
        $data = DB::table('customer_promotion_tmp');
        if($request->fullname){
            $data = $data->where(function ($query) use ($request) {
                                    $query->where('fullname', $request->fullname)
                                          ->orWhere('phone',$request->fullname);
                    });
        }
        if($request->status != null){
            $data = $data->where('status', $request->status);
        }
        if($request->du_an != null){
            $data = $data->where('du_an', 'like', '%'.$request->du_an.'%');
        }
        if($request->ma_can != null){
            $data = $data->where('ma_can', $request->ma_can);
        }
        if($request->maso){
            $data = $data->where('maso', $request->maso);
        }
        $data = $data->orderBy('id','desc')->paginate();
        return view('administrator.app-helpdesk.customer_tmp.index', compact('data', 'request'));
    }


    public function import(Request $request){
        $listFail = [];
        if ($request->hasFile('import_file')) {
            $validator = Validator::make($request->all(), [
                //or this
                'import_file' => 'required|max:50000|mimetypes:application/csv,application/excel,' .
                    'application/vnd.ms-excel, application/vnd.msexcel,' .
                    'text/csv, text/anytext, text/plain, text/x-c,' .
                    'text/comma-separated-values,' .
                    'inode/x-empty,' .
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ]);
            if ($validator->fails()) {
                $listFail[] = 'File import phải là file Excel';
                return redirect()->back()->with('error', trans('Lỗi import dữ liệu'))
                    ->with('list-fail', $listFail);
            }
            $check_success = false;
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) use (&$listFail, &$check_success) {
                $data = [];
                $row_check = $reader->toArray()[0];
                if (!array_key_exists('ho_va_ten', $row_check) ||
                    !array_key_exists('sdt', $row_check) ||
                    !array_key_exists('ma_can', $row_check) ||
                    !array_key_exists('du_an', $row_check) ||
                    !array_key_exists('ngay_vao_hop_dong', $row_check) ||
                    !array_key_exists('cmt', $row_check) ||
                    !array_key_exists('ma_so', $row_check)
                ) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }
                foreach ($reader->toArray() as $key => $row) {
                    if(!$row['ho_va_ten'] || !$row['sdt'] || !$row['ma_so']){
                        continue;
                    }
                    $customer['fullname'] = $row['ho_va_ten'];
                    $customer['phone'] = $row['sdt'];
                    $customer['ma_can'] = $row['ma_can'];
                    $customer['du_an'] = $row['du_an'];
                    $customer['ngay_vao_hop_dong'] = $row['ngay_vao_hop_dong'];
                    $customer['cmt'] = $row['cmt'];
                    $customer['maso'] = $row['ma_so'];
                    $customer['so_ma'] = $row['so_luong_ma'];
                    $customer['ma_phu'] = $row['ma_phu'];
                    DB::table('customer_promotion_tmp')->insert($customer);
                }
                $check_success = true;
            });
            if (count($listFail) && !$check_success) {
                return redirect()->back()->with('list-fail', $listFail);
            } else {
                return redirect()->route('app.customer.tmp')->with('message', trans('messages.import_success'));
            }
        }
        return redirect()->back()->with('error', 'Bạn phải chọn file Import');
    }

    public function updateStatus(Request $request, $id){
        $customer = DB::table('customer_promotion_tmp')->where('id', $id);
        if($request->status == "true"){
            $objCus['status'] = ONE;
            $objCus['updated_at'] = Carbon::now();
            $objCus['update_by'] = Auth::user()->id;
            $customer->update($objCus);
        }else{
            $objCus['status'] = 0;
            $objCus['updated_at'] = Carbon::now();
            $objCus['update_by'] = Auth::user()->id;
            $customer->update($objCus);
        }
        return $this->response->json(Response::HTTP_OK, null, 'Cập nhật thành công');
    }
}
