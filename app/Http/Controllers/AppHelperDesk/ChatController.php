<?php

namespace App\Http\Controllers\AppHelperDesk;

use App\Models\Customers\Accounts;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChatController extends Controller
{
    private $response;
    public function __construct(ResponseService $response)
    {
        $this->response = $response;
    }
    public function index(){
        $listAccount = Accounts::all();
        return view('administrator.app-helpdesk.chat.messages', compact('listAccount'));
    }

    public function search(Request $request){
        $listAccount = Accounts::where('fullname', 'like', '%'. $request['search'].'%')
            ->orWhere('phone', 'like', '%'. $request['search'].'%')
            ->orWhere('email', 'like', '%'. $request['search'].'%')->get();
        return view('administrator.app-helpdesk.chat.account', compact('listAccount'));
    }
}
