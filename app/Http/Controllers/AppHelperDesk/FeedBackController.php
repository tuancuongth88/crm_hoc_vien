<?php

namespace App\Http\Controllers\AppHelperDesk;

use App\Models\Customers\FeedbackSale;
use App\Models\Projects\News;
use App\Models\Users\AccountTransactions;
use App\Models\Users\EmployeeSaler;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedBackController extends Controller
{
    /**
     * @var AccountsRepository
     */
    protected $model;

    private $partView;
    /**
     * AccountsController constructor.
     *
     * @param AccountsRepository $repository
     * @param AccountsValidator $validator
     */
    public function __construct(FeedbackSale $model)
    {
        $this->model = $model;
        $this->partView = 'administrator.app-helpdesk.feedback';
    }

    public function index(){
        $data = $this->model->orderBy('id', 'desc')->paginate();
        $listRank = $this->model::$listRank;
        $listSale = EmployeeSaler::pluck('fullname', 'id')->toArray();
        return view($this->partView.'.index', compact('data', 'listRank', 'listSale'));

    }

    public function search(Request $request){
        $input = $request->all();
        $listRank = $this->model::$listRank;
        $listSale = EmployeeSaler::pluck('fullname', 'id')->toArray();
        $data = $this->model->whereNotNull('created_at');
        if(isset($input['project_id'])){
            $listTrasactionId = AccountTransactions::where('project_id', $input['project_id'])->pluck('id')->toArray();
            $data = $data->whereIn('transaction_id', $listTrasactionId);
        }
        if(isset($input['sale_id'])){
            $data =$data->where('sale_id', $input['sale_id']);
        }
        if(isset($input['rank_sale'])){
            $data =$data->where('rank_sale', $input['rank_sale']);
        }
        $data = $data->orderBy('id', 'desc')->paginate();
        return view($this->partView.'.index', compact('data', 'input','listRank', 'listSale'));
    }
}
