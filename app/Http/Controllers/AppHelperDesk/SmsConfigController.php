<?php

namespace App\Http\Controllers\AppHelperDesk;

use App\Models\SendSmsLog;
use App\Models\Systems\OptionSMS;
use App\Services\Lib;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;

class SmsConfigController extends Controller
{
    /**
     * @var AccountsRepository
     */
    protected $model;

    private $partView;
    /**
     * AccountsController constructor.
     *
     * @param AccountsRepository $repository
     * @param AccountsValidator $validator
     */
    public function __construct(OptionSMS $model)
    {
        $this->model = $model;
        $this->partView = 'administrator.app-helpdesk.systems.sms';
    }

    public function index(){
        $listType = OptionSMS::$listType;
        $data = $this->model->paginate();
        return view($this->partView.'.index', compact('data', 'listType'));
    }

    public function create(){
        $listType = OptionSMS::$listType;
        return view($this->partView.'.create', compact('listType'));
    }

    public function edit($id){
        $data = $this->model->find($id);
        return view($this->partView.'.edit', compact('data'));
    }

    public function update(Request $request, $id){
        $input = $request->all();
        $data = $this->model->find($id);
        $data['message'] = $input['message'];
        $data['name'] = $input['name'];
        $data->save();
        return redirect()->route('sms.index')->with('message', trans('messages.update_success'));
    }

    public function store(Request $request){
        $input  = $request->all();
        $template = $this->model->where('type', $input['type'])->where('type', '<>', OptionSMS::TYPE_ORTHER)->first();
        if(!$template){
            $this->model->create($input);
            return redirect()->route('sms.index')->with('message', trans('messages.create_success'));
        }
        return redirect()->back()->with('error', 'Đã tồn tại mẫu loại này rồi. vui lòng cập nhật lại mẫu đã có!');
    }

    public function destroy($id){
        $this->model->find($id)->delete();
        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function promotion(){
        $listTemplate = $this->model->where(OptionSMS::TYPE, OptionSMS::TYPE_ORTHER)->pluck('name', 'id')->toArray();
        return view($this->partView.'.promotion', compact('listTemplate'));
    }

    public function exportFileImport(){
        $dataDefault = ['0936405189', '88888'];
        Excel::create('Danh_sach_khach_hang', function ($excel) use ($dataDefault) {
            $excel->sheet('Danh_sach_khach_hang', function ($sheet) use ($dataDefault) {
                $sheet->fromArray($dataDefault, null, 'A2', true);
                $sheet->row(1, array(
                    'Điện Thoại', 'Mã khuyến mại'
                ));
                $sheet->row(1, function($row) {
                    $row->setBackground('#A5B6CB');
                });
                $this->laravelExcel($sheet, 1);
                $sheet->freezeFirstRow();
                $sheet->cell('A1:C1', function ($cell) {
                    // Set font
                    $cell->setFont(array(
                        'name' => 'Times New Roman',
                        'size' => '14',
                        'bold' => true,
                    ));

                });
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'borders'   => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'outline'    => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                    ),
                );
                $sheet->getStyle('A1:C1')->applyFromArray($style);
            });
        })->download('xlsx');
    }

    public function laravelExcel($sheet = null, $intRowNumber = null) {
        $arrColumn = ['A', 'B', 'C'];
        try {
            foreach ($arrColumn as $key => $value) {
                $sheet->setSize($value . $intRowNumber, 30, 30);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function sendSMSFromFile(Request $request)
    {
        $listFail = [];
        if ($request->hasFile('import_file')) {
            $validator = Validator::make($request->all(), [
                //or this
                'import_file' => 'required|max:50000|mimetypes:application/csv,application/excel,' .
                    'application/vnd.ms-excel, application/vnd.msexcel,' .
                    'text/csv, text/anytext, text/plain, text/x-c,' .
                    'text/comma-separated-values,' .
                    'inode/x-empty,' .
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ]);
            if ($validator->fails()) {
                $listFail[] = 'File import phải là file Excel';
                return redirect()->back()->with('error', trans('Lỗi import dữ liệu'))
                    ->with('list-fail', $listFail);
            }
            $check_success = false;
            $template = OptionSMS::where('id', $request->type)->first();
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) use (&$listFail, &$check_success, $template) {
                $data = [];
                $row_check = $reader->toArray()[0];
                if (!array_key_exists('dien_thoai', $row_check) ||
                    !array_key_exists('ma_khuyen_mai', $row_check)
                ) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }
                foreach ($reader->toArray() as $key => $row) {
                    // check DT ton tai khong
                    if (isset($data[$row['dien_thoai']])) {
                        $listFail[] = 'Dữ liệu dòng '.$key. ' bị bỏ trống';
                        continue;
                    }
                    if(!validatePhone($row['dien_thoai'])){
                        $listFail[] = $row['dien_thoai'].' không đúng định dạng';
                    }
                    if (!isset($row['ma_khuyen_mai'])) {
                        $listFail[] = 'Dữ liệu dòng '.$key. ' bị bỏ trống';
                        continue;
                    }
                    $data['phone'] = $row['dien_thoai'];
                    $data['ma_khuyen_mai'] = $row['ma_khuyen_mai'];
                    $content = render_template_sms($template->message, $data, ['phone', 'ma_khuyen_mai']);
                    $log['model'] = "KHUYENMAI";
                    $log['item_id'] = 0;
                    $log['user_id'] = 0;
                    $log['phone']   = $row['dien_thoai'];
                    $log['content']   = $content;
                    $log['is_send']   = 0;
                    SendSmsLog::create($log);
                }
                $check_success = true;
            });
            if (count($listFail) && !$check_success) {
                return redirect()->back()->with('list-fail', $listFail);
            } else {
                return redirect()->route('sms.promotion')->with('message', trans('messages.import_success'));
            }
        }
        return redirect()->back()->with('error', trans('Không tồn tại file import'));
    }
}
