<?php

namespace App\Http\Controllers\AppHelperDesk;

use App\Models\Customers\Consignments;
use App\Models\Customers\OptionConsignments;
use App\Services\Lib;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class ConsignmentController extends Controller
{
    private $request;
    public function __construct(Consignments $model, Request $request)
    {
        $this->request = $request;
        $this->model = $model;
        $this->partView = 'administrator.app-helpdesk.consignment';
    }

    public function index(){
        $data = $this->model->orderBy('id', 'desc')->paginate();
        $productType = OptionConsignments::where('type', OptionConsignments::PRODUCT_TYPE)->pluck('name', 'id')->toArray();
        $listStatus = $this->model::$arrayStatus;
        return view($this->partView.'.index', compact('data', 'productType', 'listStatus'));
    }

    public function show($id){
        $data = $this->model->find($id);
        $listImages = explode(',', $data->images);
        if($data){
            return view($this->partView.'.show', compact('data', 'listImages'));
        }else{
            return abort(404);
        }
    }
    public function search(){
        $input = $this->request->all();

        $data = $this->model->whereNotNull('created_at');
        if(isset($input['search'])){
            $data = $data->where('phone', 'like', '%'.$input['search'].'%')
                        ->orWhere('fullname', 'like', '%'. $input['search']. '%')
                        ->orWhere('email', 'like', '%'. $input['search']. '%');
        }
        if(isset($input['product_type'])){
            $data =$data->where('product_type', $input['product_type']);
        }
        if(isset($input['status'])){
            $data =$data->where('status', $input['status']);
        }
        $data = $data->paginate();

        $productType = OptionConsignments::where('type', OptionConsignments::PRODUCT_TYPE)->pluck('name', 'id')->toArray();
        $listStatus = $this->model::$arrayStatus;
        return view($this->partView.'.index', compact('data','productType', 'input', 'listStatus'));
    }

    public function updateStatus($id, $status){

        $data = $this->model->find($id);
        $data['status'] =$status;
        $data->save();
        return redirect()->back()->with('message', 'Cập nhật trạng thái thành công');
    }

    public function destroy($id){
        $data = $this->model->find($id);
        $data->delete();
        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function exportExcel(){
        $input = $this->request->all();
        $data = $this->model->whereNotNull('created_at');
        if(isset($input['search'])){
            $data = $data->where('phone', 'like', '%'.$input['search'].'%')
                ->orWhere('fullname', 'like', '%'. $input['search']. '%')
                ->orWhere('email', 'like', '%'. $input['search']. '%');
        }
        if(isset($input['product_type'])){
            $data =$data->where('product_type', $input['product_type']);
        }
        if(isset($input['status'])){
            $data =$data->where('status', $input['status']);
        }
        $data = $data->orderBy('id', 'desc')->get();
        $nameFile = 'file_khach_hang_ky_gui';
        $nameSheet = 'danh_sach_khach_hang';
        $title = 'Danh sách khách hàng ký gửi sản phẩm';
        $listColunm = array(
            'STT', 'Tài khoản', 'Tiêu đề','Hình thức', 'Giá', 'diện tích',
            'Tên khách hàng', 'Số điện thoại', 'Email khách hàng', 'Địa chỉ', 'Mô tả'
        );
        $listData = array();
        foreach ($data as $key => $value){
            $dataDefault = array();
            $listImages = null;
            $dataDefault['key'] = $key +1;
            $dataDefault['account'] = @$value->account->fullname;
            $dataDefault['title'] = $value->title;
            $dataDefault['product_type'] = @$value->productType->name;
            $dataDefault['price'] = $value->price.' '. @$value->priceType->name;
            $dataDefault['area'] = $value->area .' '. @$value->areaType->name;
            $dataDefault['fullname'] = $value->fullname;
            $dataDefault['phone'] = $value->phone;
            $dataDefault['email'] = $value->email;
            $dataDefault['address'] = $value->address;
            $dataDefault['description'] = $value->description;
            if ($value->images) {
                $i = 1;
                $listImages = explode(',', $value->images);
                foreach ($listImages as $k => $itemImage) {
                    $dataDefault['image' . $i] = $itemImage;
                    $i++;
                }
            }
            $listData[$key] = $dataDefault;
        }
        $withCol = ['j' => 40];
        Lib::exportExcel($title, $nameFile, $nameSheet, $listColunm, $listData, $withCol);

    }

}
