<?php

namespace App\Http\Controllers\AppHelperDesk;
use App\Http\Controllers\Controller;
use App\Jobs\AccountProcess;
use App\Jobs\SendSMS;
use App\Jobs\SensSMSCreateAccount;
use App\Models\Customers\AccountFireBase;
use App\Models\Customers\Accounts;
use App\Models\Customers\FollowerNews;
use App\Models\Projects\NewsCategory;
use App\Models\Projects\ProjectAccount;
use App\Models\Projects\ProjectManager;
use App\Models\Systems\Department;
use App\Models\Systems\OptionSMS;
use App\Models\Users\AccountTransactions;
use App\Models\Users\EmployeeSaler;
use App\Models\Users\User;
use App\Notifications\AppHelpdeskNotification;
use App\Notifications\CustomerNotifications;
use App\Services\ActivationService;
use App\Services\Lib;
use App\Services\ResponseService;
use Carbon\Carbon;
use Illuminate\Database\Events\TransactionBeginning;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\Customers\AccountsRepository;
use App\Validators\Customers\AccountsValidator;
use App\Http\Requests\AppHelpDesk\AccountsCreateRequest;
use App\Http\Requests\AppHelpDesk\AccountsUpdateRequest;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Worksheet;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Maatwebsite\Excel\Facades\Excel;

/**
 * Class AccountsController.
 *
 * @package namespace App\Http\Controllers\Customers;
 */
class AccountsController extends Controller
{
    /**
     * @var AccountsRepository
     */
    protected $repository;

    /**
     * @var AccountsValidator
     */
    protected $validator;

    private $partView = 'administrator.app-helpdesk.accounts';
    private $serviceAccount;
    private $responseService;
    /**
     * AccountsController constructor.
     *
     * @param AccountsRepository $repository
     * @param AccountsValidator $validator
     */
    public function __construct(AccountsRepository $repository, AccountsValidator $validator, ResponseService $responseService)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->responseService = $responseService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = Accounts::where('active', USER_ACTIVE);
        if(isset($request->search)){
            $data = $data->where('phone', 'like', '%'.$request->search.'%')
                            ->orWhere('email', 'like', '%'.$request->search.'%')
                            ->orWhere('fullname', 'like', '%'.$request->search.'%')->paginate();
        }
        if(isset($request->birthday)){
            $data = $data->whereMonth('birthday', $request->birthday);
        }
        $data = $data->paginate();
        if (request()->wantsJson()) {
            return response()->json([
                'data' => $data,
            ]);
        }
        $listMonth = Accounts::$listMonth;
        return view($this->partView.'.index', compact('data','request', 'listMonth'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AccountsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(AccountsCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            if(!validatePhone($request['phone'])){
                return redirect()->route('account.create')->with('error', 'Số điện thoại không đúng định dạng')->withInput();;
            }
            $password = randomPassword(6);
            $request['password'] = Hash::make($password);
            $request['active']  = Accounts::ONE;

            $account = $this->repository->create($request->all());
            if($account){
                //active all notification send email
                if($request->email){
                    $this->sendNotificationEmail_SMS($account, $password);
                }
                //send sms
                dispatch(new SensSMSCreateAccount($account, $password));
                dispatch(new AccountProcess($account, 'store'));
                $categoryNew = NewsCategory::all();
                foreach ($categoryNew as $key => $value){
                    $follower['account_id'] = $account->id;
                    $follower['category_new_id'] = $value->id;
                    FollowerNews::create($follower);
                }

                $response = [
                    'message' => trans('messages.create_success'),
                    'data'    => $account->toArray(),
                ];

                return redirect()->route('account.index')->with('message', $response['message']);
            }else{
                $response = [
                    'message' => trans('messages.create_failed'),
                    'data'    => $account->toArray(),
                ];
                return redirect()->route('account.index')->with('error', $response['message']);
            }
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->route()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    public function sendNotificationEmail_SMS($account, $password){
        $linkAndroid = env('LINK_ANDROID');
        $linkIOS = env('LINK_IOS');
        $title    = 'Hệ thống chăm sóc khách hàng Hải Phát';
        $message  = 'Tài khoản của bạn là:<span style="color:#ff9f00;"><b> '.$account->phone.'</b></span><br>';
        $message .= 'Mật khẩu:<span style="color:#ff9f00;"><b> '. $password .'</b></span><br>';
        $message .= 'Vui lòng tải ứng dụng tại: <br>';
        $message .= 'Android: <a href="'.$linkAndroid.'">Tại đây</a> <br>';
        $message .= 'IOS: <a href="'.$linkIOS.'">Tại đây</a> <br>';
        $account->notify(new AppHelpdeskNotification($account, $title, $message, 'Hệ thống CSKH Hải Phát Land thông báo'));
    }



    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $account,
            ]);
        }

        return view('accounts.show', compact('account'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = $this->repository->find($id);

        return view($this->partView.'.info', compact('account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  AccountsUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(AccountsUpdateRequest $request, $id)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $account = $this->repository->update($request->all(), $id);
            if($account){
                dispatch(new AccountProcess($account, 'update'));
            }
            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $account->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('account.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = $this->repository->find($id);
        $account->project()->delete();
        $account->transaction()->delete();
        $account->consignments()->delete();
        $account->bookingViewHouse()->delete();
        $account->followers()->delete();
        $account->feedbackSale()->delete();

        //xoa trong firebase
        $deleted = $this->repository->delete($id);
        if($deleted){
            dispatch(new AccountProcess($account, 'destroy'));
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create(){
        return view($this->partView.'.create');
    }

    public function getChangePassword($id){
        return view($this->partView.'.change-password', compact('id'));
    }

    public function postChangePassword(Request $request){
        $account = Accounts::find($request->account_id);
        if (!(Hash::check($request->get('current_password'), $account->password))) {
            return redirect()->back()->with("error", 'Mật khẩu hiện tại không đúng');
        }

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            return redirect()->back()->with("error", "Mật khẩu cũ không được giống mật khẩu mới.");
        }
        $validatedData = $request->validate([
            'current_password' => 'required',
            'new_password'     => 'required|string|min:6|confirmed',
        ]);
        //Change Password
        $account->password = Hash::make($request->get('new_password'));
        $account->save();
        return redirect()->route('account.index')->with("message", "Thay đổi mật khẩu thành công !");
    }

    public function listHouse(Request $request, $accountId){
        $account = Accounts::find($accountId);
        $input = $request->all();
        $result = AccountTransactions::where('parent_id', 0)->where('account_id', $accountId);
        if (request()->wantsJson()) {
            $total = $result->count();
            $result = $result->take($input['pagination']['perpage'])->skip($input['pagination']['perpage'] * ($input['pagination']['page'] - 1));
            $result = $result->with('project', 'sale', 'account', 'children');
            $data             = $result->orderBy('id', 'DESC')->get()->toArray();
            $response['data'] = $data;
            $meta             = new \stdClass();
            $meta->page       = $input['pagination']['page'];
            $meta->perpage    = $input['pagination']['perpage'];
            $meta->total      = $total;
            $response['meta'] = $meta;
            return response()->json($response);
        }
        $listProject = ProjectManager::pluck('name', 'id')->toArray();
        return view($this->partView.'.list-transaction', compact('accountId', 'projectId', 'account', 'listProject'));
    }

    public function listTransaction(Request $request){
        $input = $request->all();
        $listTransaction = AccountTransactions::where('account_id', $input['accountId'])
                                            ->where('project_id', $input['projectId'])
                                            ->where('parent_id', $input['id'])->orderBy('id', 'desc')
                                            ->with('project', 'sale', 'account')->get();
        $response['data'] = $listTransaction->toArray();
        $meta             = new \stdClass();
        $meta->total      = $listTransaction->count();
        $response['meta'] = $meta;
        return response()->json($response);
    }

    public function addTransaction(Request $request){
        $input = $request->except('_token');
        $checkAccountExist = ProjectAccount::where('project_id',$input['project_id'])
                                           ->where('account_id', $input['account_id'])->first();
        if(!$checkAccountExist){
            $accountProject['project_id'] = $input['project_id'];
            $accountProject['account_id'] = $input['account_id'];
            $accountProject['created_by'] = Auth::user()->id;
            ProjectAccount::create($accountProject);
        }

        $input['parent_id'] = 0;
        $userSale = User::find($input['sale_id']);
        $employeeSale = EmployeeSaler::where('email', $userSale->email)
                                    ->orWhere('phone', $userSale->phone)->first();
        if(!$employeeSale){
            $employeeSale['fullname'] = $userSale->fullname;
            $employeeSale['user_id'] = $userSale->id;
            $employeeSale['email'] = $userSale->email;
            $employeeSale['phone'] = $userSale->phone;
            $employeeSale = EmployeeSaler::create($employeeSale);
        }
        $input['sale_id'] = $employeeSale->id;
        $transaction = AccountTransactions::create($input);

        if($transaction){
            return redirect()->back()->with("message", 'Thêm mới căn thành công');
        }
        return redirect()->back()->with("error", 'Không thêm được căn');
    }

    public function delTransaction(Request $request){
        $listChild = AccountTransactions::find($request->id)->children();
        $transaction = AccountTransactions::find($request->id);
        $countProjectByAccount = AccountTransactions::where('project_id', $transaction->project_id)
                                                    ->where('account_id', $transaction->account_id)
                                                    ->where('parent_id', 0)->count();
        if($countProjectByAccount < 2){
            //xu ly xoa khoi account project
            $projectAccount =  ProjectAccount::where('account_id', $transaction->account_id)
                            ->where('project_id', $transaction->project_id)
                            ->first();
            if($projectAccount){
                $projectAccount->delete();
            }
        }
        if($listChild->count() > 0){
            $listChild->delete();
        }
        $result = AccountTransactions::find($request->id)->delete();
        if($result){
            return response()->json([
                'status'  => Response::HTTP_OK,
                'message' => trans('messages.delete_success')
            ]);
        }

    }

    public function addHistoriesTransaction(Request $request){
        $input = $request->all();
        $house = AccountTransactions::find($input['parent_id']);
        $input['sale_id'] = $house->sale_id;
        $account = AccountTransactions::create($input);
        if($account){
            return response()->json([
                'status'  => Response::HTTP_OK,
                'message' => trans('messages.create_success')
            ]);
        }
        return response()->json([
            'status'  => Response::HTTP_NOT_FOUND,
            'message' => trans('messages.create_failed')
        ]);
    }

    public function delHistoriesTransaction(Request $request){
        $result = AccountTransactions::find($request->id)->delete();
        if($result){
            return response()->json([
                'status'  => Response::HTTP_OK,
                'message' => trans('messages.delete_success')
            ]);
        }
        return response()->json([
            'status'  => Response::HTTP_NOT_FOUND,
            'message' => trans('messages.delete_failed')
        ]);
    }

    public function editTransaction(Request $request){
        $result = AccountTransactions::find($request->id);
        return $this->responseService->json(Response::HTTP_OK, $result, "");
    }

    public function updateTransaction(Request $request){
    }

    public function uploadAccount(Request $request){
        if ($request->hasFile('import_file')) {
            $allowed = array('xls', 'xlsx');
            $path = $_FILES['import_file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
                return redirect()->back()->with('error', trans('messages.import_file_extension'));
            } else {
                $path = $request->file('import_file')->getRealPath();
            }
            $data = [];
            Excel::load($path, function ($reader) use ($data, &$listUserFail){
                foreach ($reader->toArray() as $row) {
                    // check DT ton tai khong
                    if (isset($data[$row['phone']])) {
                        continue;
                    }
                    $account = Accounts::where('phone', $row['phone'])->first();
                    if ($account) {

                    }

                }
            });
        }
    }

    public function importAccount(Request $request){
        $listFail = [];
        if ($request->hasFile('import_file')) {
            $validator = Validator::make($request->all(), [
                //or this
                'import_file' => 'required|max:50000|mimetypes:application/csv,application/excel,' .
                    'application/vnd.ms-excel, application/vnd.msexcel,' .
                    'text/csv, text/anytext, text/plain, text/x-c,' .
                    'text/comma-separated-values,' .
                    'inode/x-empty,' .
                    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ]);
            if ($validator->fails()) {
                $listFail[] = 'File import phải là file Excel';
                return redirect()->back()->with('error', trans('Lỗi import dữ liệu'))
                    ->with('list-fail', $listFail);
            }
            $check_success = false;
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) use (&$listFail, &$check_success) {
                $data = [];
                if (!isset($reader->toArray()[0][0])) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }

                $row_check = $reader->toArray()[0][0];
                if (!array_key_exists('stt', $row_check) ||
                    !array_key_exists('ho_ten_kh', $row_check) ||
                    !array_key_exists('so_dien_thoai', $row_check) ||
                    !array_key_exists('ngay_sinh', $row_check) ||
                    !array_key_exists('email', $row_check) ||
                    !array_key_exists('gioi_tinh_nam_nu', $row_check) ||
                    !array_key_exists('can_ho', $row_check) ||
                    !array_key_exists('du_an', $row_check) ||
                    !array_key_exists('ngay_dat_coc', $row_check) ||
                    !array_key_exists('ngay_vao_hop_dong', $row_check) ||
                    !array_key_exists('tong_gia_ban', $row_check) ||
                    !array_key_exists('nvkd', $row_check) ||
                    !array_key_exists('email_nvkd', $row_check) ||
                    !array_key_exists('dien_thoai_nvkd', $row_check) ||
                    !array_key_exists('khoi_kinh_doanh', $row_check)
                ) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }
                    foreach ($reader->toArray() as $sheet) {
                    foreach ($sheet as $key => $row) {
                        if (!isset($row['ho_ten_kh'])) {
                            continue;
                        }
                        $checkInput = $this->validateAccount($row);
                        if($checkInput){

                            //check project
                            $project = ProjectManager::where('name', $row['du_an'])->first();
                            if($project){
                                $account = Accounts::where('phone', $row['so_dien_thoai'])->first();
                                if (!$account) {
                                    // tao moi tai khoan
                                    //Validate
                                    $account['fullname'] = $row['ho_ten_kh'];
                                    //check phone

                                    if(validatePhone($row['so_dien_thoai'])){
                                        $account['phone'] = $row['so_dien_thoai'];
                                    }else{
                                        $listFail[] = $row['so_dien_thoai'].' Số điện thoại không đúng định dạng';
                                        continue;
                                    }
                                    //check formant date
                                    if($row['ngay_sinh']){

                                        if(isDate($row['ngay_sinh'])){
                                            $account['birthday'] = $row['ngay_sinh'];
                                        }else{
                                            $checkDOB = explode('/', $row['ngay_sinh']);
                                            if(count($checkDOB) == 3 && $checkDOB[0] < 32 && $checkDOB[1] < 13 && $checkDOB[2] > 1900 ){
                                                $account['birthday'] = \Carbon\Carbon::createFromFormat('d/m/Y', $row['ngay_sinh']);
                                            }
                                        }
                                    }
                                    $account['email'] = $row['email'];
                                    if($row['gioi_tinh_nam_nu'] == 'Nam'){
                                        $account['gender'] = 1;
                                    }else{
                                        $account['gender'] = 2;
                                    }
                                    $password = randomPassword(6);
                                    $account['password'] = Hash::make($password);
                                    $account['active']  = Accounts::ONE;

                                    $account = Accounts::create($account);
                                    if($account) {
                                        //active all notification send email
                                        if ($account->email) {
                                            $this->sendNotificationEmail_SMS($account, $password);
                                        }
                                        //send sms
                                        dispatch(new SensSMSCreateAccount($account, $password));
                                        dispatch(new AccountProcess($account, 'store'));
                                        $categoryNew = NewsCategory::all();
                                        foreach ($categoryNew as $key => $value) {
                                            $follower['account_id'] = $account->id;
                                            $follower['category_new_id'] = $value->id;
                                            FollowerNews::create($follower);
                                        }
                                    }
                                }
                                //xử lý them du an va giao dich
                                //check du an
                                //check xem tai khoan này đã có project hay chưa(chua co thi them)
                                $projectAccount = ProjectAccount::where('account_id', $account->id)->where('project_id', $project->id)->first();
                                if(!$projectAccount){
                                    //thêm account vao project
                                    $projectAccount['account_id'] = $account->id;
                                    $projectAccount['project_id'] = $project->id;
                                    ProjectAccount::create($projectAccount);
                                }
                                //thêm sale vào bảng mới
                                //check sale da ton tai chua
                                $employee = EmployeeSaler::where('phone', $row['dien_thoai_nvkd'])->first();
                                if(!$employee){
                                    $employee['fullname'] = $row['nvkd'];
                                    $employee['phone'] = $row['dien_thoai_nvkd'];
                                    $employee['email'] = $row['email_nvkd'];
                                    //check xem nhan vien do co trong he thong khong
                                    $user = User::where('phone', $row['dien_thoai_nvkd'])->orWhere('email', $row['email_nvkd'])->first();
                                    if($user){
                                        $employee['user_id'] = $user->id;
                                    }
                                    $employee = EmployeeSaler::create($employee);
                                }

                                // add giao dịch vào transaction
                                //check duplicate transaction
                                $checkTransaction = AccountTransactions::where('project_id', $project->id)
                                                                  ->where('account_id', $account->id)
                                                                  ->where('parent_id', 0)->where('name', $row['can_ho'])->get();
                                if($checkTransaction->count() > 0){
                                    $listFail[] = 'Dữ liệu dòng ' .$row['stt']. ' Bị trùng giao dịch đã có';
                                    continue;
                                }

                                $transaction['project_id'] = $project->id;
                                $transaction['name'] = $row['can_ho'];
                                if(isDate($row['ngay_vao_hop_dong'])){
                                    $transaction['date_payment'] = \Carbon\Carbon::createFromFormat('d/m/Y', $row['ngay_vao_hop_dong']);
                                }else{
                                    $checkDatePayment = explode('/', $row['ngay_vao_hop_dong']);
                                    if(count($checkDatePayment) == 3 && $checkDatePayment[0] < 32 && $checkDatePayment[1] < 13 && $checkDatePayment[2] > 1900 ){
                                        $transaction['date_payment'] = \Carbon\Carbon::createFromFormat('d/m/Y', $row['ngay_vao_hop_dong']);
                                    }
                                }

                                $transaction['account_id'] = $account->id;
                                $transaction['sale_id'] = $employee->id;
                                $transaction['total_money'] = filter_var($row['tong_gia_ban'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
                                AccountTransactions::create($transaction);
                            }else{
                                $listFail[] = 'Dữ liệu dòng ' .$row['stt']. ' Không hợp lệ. Dự án không tồn tại';
                                continue;
                            }

                        }else{
                            $listFail[] = 'Dữ liệu dòng ' .$row['stt']. ' Không hợp lệ';
                            continue;
                        }
                    }
                }
                $check_success = true;
            });

            return redirect()->route('account.index')->with('list-fail', $listFail)->with('message', trans('messages.import_success'));
        }
        return redirect()->back()->with('error', trans('Không tồn tại file import'));
    }

    public function validateAccount($input){
        if(!isset($input['ho_ten_kh'])
            || !isset($input['so_dien_thoai'])
            || !isset($input['gioi_tinh_nam_nu'])
            || !isset($input['du_an'])
            || !isset($input['can_ho'])
            || !isset($input['ngay_vao_hop_dong'])
            || !isset($input['tong_gia_ban'])
            || !isset($input['nvkd'])
            || !isset($input['dien_thoai_nvkd'])){
            return false;
        }
        return true;
    }

    public function downloadFileImport()
    {
        $listProject = ProjectManager::all(['name'])->toArray();
        $dataDefault = [
            'stt' => '1',
            'ho_ten_khach_hang' => 'Nguyễn Văn A',
            'so_dien_thoai' => '0982633825',
            'ngay_sinh' => '16/12/1991',
            'email' => 'nguyenvana@gmail.com',
            'gioi_tinh' => 'Nam',
            'can_ho' => 'CT101',
            'du_an' => 'Roman Plaza',
            'ngay_dat_coc' => '20/03/2019',
            'ngay_vao_hop_dong' => '20/03/2019',
            'tong_gia_ban' => '1200000000',
            'nvkd' => 'Tuấn Cường',
            'email_nvkd' => 'tuancuongth88@gmail.com',
            'phone_nvld' => '0982838271',
            'khoi_kd' => 'Khoi_1',
        ];
        $sex = array(
            array('Nam'),
            array('Nữ')
        );
        Excel::create('file_import_khach_hang', function ($excel) use ($dataDefault, $listProject, $sex) {
            $excel->sheet('danh_sach_khach_hang', function ($sheet) use ($dataDefault, $listProject, $sex) {
                $sheet->fromArray($dataDefault, null, 'A2', true);
                $sheet->row(1, array(
                    'STT', 'Họ tên KH', 'Số Điện Thoại','Ngày sinh', 'Email', 'Giới tính (Nam/ Nữ)',
                    'Căn hộ', 'Dự án', 'Ngày đặt cọc', 'Ngày vào hợp đồng', 'Tổng giá bán', 'NVKD', 'Email NVKD', 'Điện thoại NVKD','Khối Kinh doanh'
                ));
                $sheet->row(1, function($row) {
                    $row->setBackground('#A5B6CB');
                });
                $countProject = count($listProject) + 1;
                $countSex = count($sex) + 1;
                $this->laravelExcel($sheet, 1);
                $sheet->freezeFirstRow();
                $sheet->cell('A1:P1', function ($cell) {
                    // Set font
                    $cell->setFont(array(
                        'name' => 'Times New Roman',
                        'size' => '14',
                        'bold' => true,
                    ));

                });
                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'borders'   => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'outline'    => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                    ),
                );

                $sheet->getStyle('A1:P1')->applyFromArray($style);
                $typeDate = \PHPExcel_Cell_DataValidation::TYPE_DATE;
                $this->setCellFormula1($sheet, 'D', 1000, null, $typeDate);
                $this->setCellFormula1($sheet, 'I', 1000, null, $typeDate);
                $this->setCellFormula1($sheet, 'J', 1000, null, $typeDate);
                $this->setCellFormula1($sheet, 'F', 1000, 'sex!$A$2:$A$' . $countSex);
                $this->setCellFormula1($sheet, 'H', 1000, 'project!$A$2:$A$' . $countProject);
                $this->setCellFormula1($sheet, 'K', 1000, null,\PHPExcel_Cell_DataValidation::TYPE_DECIMAL);
            });


            $excel->sheet('sex', function ($sheet) use ($sex) {
                $sheet->fromArray($sex, null, 'A1', false);
                $sheet->getProtection()->setSheet(true);
                $sheet->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
            });
            $excel->sheet('project', function ($sheet) use ($listProject) {
                $sheet->fromArray($listProject, null, 'A1', true);
                $sheet->getProtection()->setSheet(true);
                $sheet->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);
            });
        })->download('xlsx');

    }
    public function laravelExcel($sheet = null, $intRowNumber = null) {
        $arrColumn = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];
        try {
            foreach ($arrColumn as $key => $value) {
                $sheet->setSize($value . $intRowNumber, 30, 30);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function setCellFormula1($sheet, $cellName, $numberCell, $cellFormula1, $type = null) {
        for ($i = 1; $i <= $numberCell; $i++) {
            $objValidation = $sheet->getCell($cellName . $i)->getDataValidation();
            if($type){
                $objValidation->setType($type);
                $objValidation->setError('Giá trị không đúng định dạng');
                $objValidation->setPromptTitle('Nhập dữ liệu');
                $objValidation->setPrompt('Vui lòng nhập dữ liệu vào cột này.');
            }else{
                $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
                $objValidation->setError('Value is not in list.');
                $objValidation->setPromptTitle('Pick from list');
                $objValidation->setPrompt('Please pick a value from the drop-down list.');
            }
            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_STOP);
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Thông báo');
            $objValidation->setFormula1($cellFormula1); //note this!
        }
    }

    public function chart(){
        $transactionProject = ProjectManager::leftJoin('account_transaction', 'project_managers.id', '=', 'account_transaction.project_id')
                                            ->groupBy('project_managers.id')->groupBy('project_managers.id', 'project_managers.name')
                                            ->select(DB::raw('project_managers.id as id , project_managers.name as name , sum(account_transaction.total_money) as total_money'))->get();
        
       return view($this->partView.'.chart', compact('transactionProject'));
    }
}
