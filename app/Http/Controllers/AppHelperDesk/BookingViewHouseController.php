<?php

namespace App\Http\Controllers\AppHelperDesk;

use App\Models\Customers\BookingViewHouse;
use App\Services\Lib;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BookingViewHouseController extends Controller
{
    /**
     * @var AccountsRepository
     */
    protected $model;

    private $partView;
    /**
     * AccountsController constructor.
     *
     * @param AccountsRepository $repository
     * @param AccountsValidator $validator
     */
    public function __construct(BookingViewHouse $model)
    {
        $this->model = $model;
        $this->partView = 'administrator.app-helpdesk.booking-view';
    }

    public function index(){
        $data = $this->model->orderBy('id', 'desc')->paginate();
        $listStatus = $this->model::$listStatus;
        return view($this->partView.'.index', compact('data', 'listStatus'));
    }

    public function search(Request $request){
        $input = $request->all();
        $data = $this->model;
        if(isset($input['search'])){
            $data = $data->where('phone', 'like', '%'.$input['search'].'%')
                        ->orWhere('fullname', 'like', '%'. $input['search']. '%')
                        ->orWhere('email', 'like', '%'. $input['search']. '%');
        }
        if(isset($input['status'])){
            $data =$data->where('status', $input['status']);
        }
        $data = $data->orderBy('id', 'desc')->paginate();
        $listStatus = $this->model::$listStatus;
        return view($this->partView.'.index', compact('data', 'listStatus', 'input'));
    }

    public function updateStatus($id, $status){
        $data = $this->model->find($id);
        $data['status'] = $status;
        $data->save();
        return redirect()->back()->with('message', trans('messages.update_success'));
    }

    public function exportExcel(Request $request){
        $input = $request->all();
        $data = $this->model;
        if(isset($input['search'])){
            $data = $data->where('phone', 'like', '%'.$input['search'].'%')
                ->orWhere('fullname', 'like', '%'. $input['search']. '%')
                ->orWhere('email', 'like', '%'. $input['search']. '%');
        }
        if(isset($input['status'])){
            $data =$data->where('status', $input['status']);
        }
        $data = $data->orderBy('id', 'desc')->get();

        $nameFile = 'DS_dat_lich_xem_nha_mau';
        $nameSheet = 'DS_dat_lich_xem_nha_mau';
        $title = 'Danh sách khách hàng đặt lịch xem nhà mẫu';
        $listColunm = array(
            'STT', 'Tài khoản', 'Khách hàng','Điện thoại', 'Email', 'Thời gian hẹn xem',
            'Nội dung'
        );
        $listData = array();
        foreach ($data as $key => $value){
            $dataDefault = array();
            $dataDefault['key'] = $key +1;
            $dataDefault['account'] = @$value->account->fullname;
            $dataDefault['fullname'] = $value->fullname;
            $dataDefault['phone'] = $value->phone;
            $dataDefault['email'] = $value->email;
            $dataDefault['time_view'] = $value->time_view;
            $dataDefault['note'] = $value->note;
            $listData[$key] = $dataDefault;
        }
        $withCol = ['A' => 10];
        Lib::exportExcel($title, $nameFile, $nameSheet, $listColunm, $listData, $withCol);

    }
}
