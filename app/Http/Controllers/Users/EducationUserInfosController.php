<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\EducationUserInfoCreateRequest;
use App\Http\Requests\Users\EducationUserInfoUpdateRequest;
use App\Models\Education\Subject;
use App\Models\Education\UserRegProgram;
use App\Models\Users\EducationUserInfo;
use App\Models\Users\User;
use App\Repositories\Users\EducationUserInfoRepository;
use App\Validators\Users\EducationUserInfoValidator;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;


/**
 * Class EducationUserInfosController.
 *
 * @package namespace App\Http\Controllers;
 */
class EducationUserInfosController extends Controller
{
    /**
     * @var EducationUserInfoRepository
     */
    protected $repository;

    /**
     * @var EducationUserInfoValidator
     */
    protected $validator;

    /**
     * EducationUserInfosController constructor.
     *
     * @param EducationUserInfoRepository $repository
     * @param EducationUserInfoValidator $validator
     */
    public function __construct(EducationUserInfoRepository $repository, EducationUserInfoValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $educationUserInfos = $this->repository->all();
        $userInfo = User::where('id', \Auth::user()->id)->get();

        $userInfo_literacy_param = null;
        $userInfo_experienced_param = null;
        if ($userInfo->count() > 0) {
            $userInfo_literacy_param = isset($userInfo[0]->EducationUserInfo->literacy_param)
                ? json_decode($userInfo[0]->EducationUserInfo->literacy_param) : null;
            $userInfo_experienced_param = isset($userInfo[0]->EducationUserInfo->experienced_param)
                ? json_decode($userInfo[0]->EducationUserInfo->experienced_param) : null;
        }

        $user_reg_program = UserRegProgram::where([['id', \Auth::user()->id]])->get();


        if (request()->wantsJson()) {
            return response()->json([
                'data' => $educationUserInfos,
            ]);
        }

        return view(
            'administrator.users.student.education-info',
            compact('userInfo', 'userInfo_literacy_param', 'userInfo_experienced_param', 'user_reg_program')
        );
    }

    public function viewInfo($id)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $educationUserInfos = $this->repository->all();
        $userInfo = User::where('id', $id)->get();


        $userInfo_literacy_param = null;
        $userInfo_experienced_param = null;
        if ($userInfo->count() > 0) {
            $userInfo_literacy_param = isset($userInfo[0]->EducationUserInfo->literacy_param)
                ? json_decode($userInfo[0]->EducationUserInfo->literacy_param) : null;
            $userInfo_experienced_param = isset($userInfo[0]->EducationUserInfo->experienced_param)
                ? json_decode($userInfo[0]->EducationUserInfo->experienced_param) : null;
        }

        $user_reg_program = UserRegProgram::where([['id', $id]])->get();


        if (request()->wantsJson()) {
            return response()->json([
                'data' => $educationUserInfos,
            ]);
        }

        return view(
            'administrator.users.student.view-info',
            compact(
                'userInfo',
                'userInfo_literacy_param',
                'userInfo_experienced_param',
                'user_reg_program'
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EducationUserInfoCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(EducationUserInfoCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $data_user[User::FULLNAME] = $request->fullname;
            $data_user[User::GENDER] = $request->gender;
            $data_user[User::BIRTHDAY] = DateTime::createFromFormat('d/m/Y', $request->birthday)->format('Y-m-d');
            $data_user[User::PHONE] = $request->phone;
            $data_user[User::ADDRESS] = $request->address;
            $data_user[User::IDENTITY] = $request->identity;
            $data_user[User::FACEBOOK] = $request->facebook;
            User::where('id', \Auth::user()->id)->update($data_user);

            $data_user_info = array();

            $literacy_param = $request->literacy_param;
            $experienced_param = $request->experienced_param;
            $literacy_str = '';
            if (!$literacy_param) {
                return response()->json([
                    'error' => true,
                    'message' => 'Trình độ học vấn không được trống'
                ]);
            } else {
                $i = 0;
                foreach ($literacy_param as $key => $val) {
                    if (++$i === count($literacy_param)) {
                        $literacy_str = $literacy_str . $val['str_literacy'];
                    } else {
                        $literacy_str = $literacy_str . $val['str_literacy'] . ',';
                    }
                }
                $data_user_info[EducationUserInfo::LITERACY] = $literacy_str;
                $data_user_info[EducationUserInfo::LITERACY_PARAM] = json_encode($literacy_param);
            }

            if (!$experienced_param) {
                return response()->json([
                    'error' => true,
                    'message' => 'Kinh nghiệm làm việc không được trống'
                ]);
            } else {
                $data_user_info[EducationUserInfo::EXPERIENCED_PARAM] = json_encode($experienced_param);
            }
            $data_user_info[EducationUserInfo::HOBBY] = $request->hobby;
            $data_user_info[EducationUserInfo::USER_ID] = \Auth::user()->id;
            $data_user_info[EducationUserInfo::TARGET] = $request->hobby;
            $data_user_info[EducationUserInfo::ADVANTAGE] = $request->advantage;
            $data_user_info[EducationUserInfo::DEFECT] = $request->defect;
            $data_user_info[EducationUserInfo::SKYPE] = $request->skype;
            $data_user_info[EducationUserInfo::ZALO] = $request->zalo;
            $data_user_info[EducationUserInfo::VIBER] = $request->viber;
            $data_user_info[EducationUserInfo::MARRIAGE] = $request->marriage;
            $data_user_info[EducationUserInfo::IDENTITY_DATE] = Carbon::createFromFormat('d/m/Y', $request->identity_date)->format('Y-m-d');
            $data_user_info[EducationUserInfo::IDENTITY_PLACE_RELEASE] = $request->identity_place_release;
            $data_user_info[EducationUserInfo::SKILL_TEACHER] = $request->skill_teacher;

            $user_info_id = $request->user_info_id;
            $check_user_info = EducationUserInfo::find($user_info_id);


            if ($check_user_info) {
                $educationUserInfo = EducationUserInfo::where('id', $user_info_id)->update($data_user_info);
            } else {
                $educationUserInfo = EducationUserInfo::create($data_user_info);
            }

            $response = [
                'message' => 'Cập nhật thông tin user thành công',
                'data' => $educationUserInfo,
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);

        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $educationUserInfo = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $educationUserInfo,
            ]);
        }

        return view('educationUserInfos.show', compact('educationUserInfo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $educationUserInfo = $this->repository->find($id);

        return view('educationUserInfos.edit', compact('educationUserInfo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EducationUserInfoUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(EducationUserInfoUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $educationUserInfo = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'EducationUserInfo updated.',
                'data' => $educationUserInfo->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'EducationUserInfo deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'EducationUserInfo deleted.');
    }

    public function updateAvatar(Request $request)
    {
        //check user
        $id = \Auth::user()->id;
        $obj_user = User::where('id', $id)->get()->toArray();
        if ($request->hasFile('uploadAvatar')) {
            $file = $request->uploadAvatar;
            $destinationPath = public_path() . IMAGEUSER;
            $filename = time() . '_' . $file->getClientOriginalName();
            $uploadSuccess = $file->move($destinationPath, $filename);
            $data['avatar'] = IMAGEUSER . $filename;
        }
        if (isset($data['avatar'])) {
            User::where('id', '=', $id)->update($data);
            if (isset($obj_user[0]) && isset($obj_user[0]['avatar']) && ($obj_user[0]['avatar'] != '/no-avatar.ico')) {
                unlink(public_path() . $obj_user[0]['avatar']);
            }
            return response()->json(array('success' => true, 'Result' => $data['avatar']));
        }
    }

    public function viewInfoTeacher($id)
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $educationTeacherInfos = $this->repository->all();
        $teacherInfo = User::where('id', $id)->get();

        $teacherInfo_literacy_param = null;
        $teacherInfo_experienced_param = null;
        if ($teacherInfo->count() > 0) {
            $teacherInfo_literacy_param = isset($teacherInfo[0]->EducationUserInfo->literacy_param)
                ? json_decode($teacherInfo[0]->EducationUserInfo->literacy_param) : null;
            $teacherInfo_experienced_param =  isset($teacherInfo[0]->EducationUserInfo->experienced_param)
                ? json_decode($teacherInfo[0]->EducationUserInfo->experienced_param) : null;
        }
        if (request()->wantsJson()) {
            return response()->json([
                'data' => $educationTeacherInfos,
            ]);
        }
        return view('administrator.users.teacher.view-info',
            compact('teacherInfo', 'teacherInfo_literacy_param', 'teacherInfo_experienced_param'));
    }
    public function editInfoTeacher() {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $educationTeacherInfos = $this->repository->all();
        $teacherInfo = User::where('id', \Auth::user()->id)->get();
        $teacherInfo_literacy_param = null;
        $teacherInfo_experienced_param = null;
        if ($teacherInfo->count() > 0) {
            $teacherInfo_literacy_param = isset($teacherInfo[0]->EducationUserInfo->literacy_param)
                ? json_decode($teacherInfo[0]->EducationUserInfo->literacy_param) : null;
            $teacherInfo_experienced_param =  isset($teacherInfo[0]->EducationUserInfo->experienced_param)
                ? json_decode($teacherInfo[0]->EducationUserInfo->experienced_param) : null;
        }
        if (request()->wantsJson()) {
            return response()->json([
                'data' => $educationTeacherInfos,
            ]);
        }
        return view('administrator.users.teacher.edit-info', compact('teacherInfo','teacherInfo_literacy_param', 'teacherInfo_experienced_param'));
    }
}
