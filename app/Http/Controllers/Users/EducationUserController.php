<?php

namespace App\Http\Controllers\Users;

use App\Models\Systems\Branch;
use App\Models\Systems\Company;
use App\Models\Systems\Department;
use App\Models\Systems\Position;
use App\Models\Users\Type;
use App\Models\Users\User;
use App\Repositories\Users\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EducationUserController extends Controller
{

    protected $repository;


    protected $validator;

    protected $branch;

    protected $department;

    protected $position;

    protected $company;

    protected $type;


    public function __construct(
        UserRepository $repository,
        Branch $branch,
        Department $department,
        Position $position,
        Company $company,
        Type $type
    )
    {
        $this->repository = $repository;
        $this->branch = $branch;
        $this->position = $position;
        $this->department = $department;
        $this->company = $company;
        $this->type = $type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search[] = ['total_reg_program', '>', 0];
        $or_search = null;
        $txt_search = '';
        if ($request->has('txt_search')) {
            $txt_search = $request->txt_search;
            $search[] = ['fullname', 'like', '%' . $txt_search . '%'];
            $or_search[] = ['email', '=', $txt_search];
            $data = User::where($search)->orWhere($or_search)->paginate();
        } else {
            $data = User::where($search)->paginate();
        }


        return view('administrator.users.student.index', compact('data', 'txt_search'));
    }

    public function getTeacher(Request $request)
    {
        $user = new User();
        $search = '';
        if ($request['search']) {
            $search = $request->search;
            $data = $user->getListTeacherByRole('is-teacher', $search, $search);
        } else {
            $data = $user->getListTeacherByRole('is-teacher');
        }
        return view('administrator.users.teacher.index', compact('data', 'search'));
    }
}
