<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Projects\NewsCreateRequest;
use App\Http\Requests\Projects\NewsUpdateRequest;
use App\Jobs\FCMNotification;
use App\Jobs\TestFireBase;
use App\Models\Customers\FollowerNews;
use App\Models\Customers\NotificationFirebase;
use App\Models\Projects\News;
use App\Models\Projects\NewsCategory;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectManager;
use App\Repositories\Projects\NewsRepository;
use App\Validators\Projects\NewsValidator;
use Auth;
use Carbon\Carbon;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class NewsController.
 *
 * @package namespace App\Http\Controllers\Projects;
 */
class NewsController extends Controller {
    /**
     * @var NewsRepository
     */
    protected $repository;

    /**
     * @var NewsValidator
     */
    protected $validator;

    /**
     * @var NewsValidator
     */
    protected $category;

    /**
     * @var NewsValidator
     */
    protected $project;

    /**
     * @var NewsValidator
     */
    protected $auth;

    /**
     * NewsController constructor.
     *
     * @param NewsRepository $repository
     * @param NewsValidator $validator
     */
    public function __construct(NewsRepository $repository, NewsValidator $validator, NewsCategory $category, ProjectManager $project, Auth $auth) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->category   = $category;
        $this->project    = $project;
        $this->auth       = $auth;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

//        if (Gate::denies('news.view')) {
//            abort(505, trans('messages.you_do_not_have_permission'));
//        }
        $listCategory = NewsCategory::where('id', '<>', NewsCategory::NEW_PROMOTION)->pluck('name', 'id')->toArray();
        $data = News::where('category_id', '<>', NewsCategory::NEW_PROMOTION)->orderBy('id', 'desc')->paginate();
        return view('administrator.projects.news.index', compact('data', 'listCategory'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  NewsCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(NewsCreateRequest $request) {
        try {
            if (Gate::denies('news.create')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $model                     = $this->repository->model();
            $input                     = $request->all();
            $input['is_approve']       = APPROVE;
            $input['approve_time']     = Carbon::now();
            $input[$model::CREATED_BY] = Auth::user()->id;
            if ($request->hasFile($model::IMAGE_URL)) {
                $input[$model::IMAGE_URL] = request()->getSchemeAndHttpHost().uploadFile($request->image_url, IMAGE_PROJECT);
            }
            $news = $this->repository->create($input);

            dispatch(new FCMNotification($news));

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $news->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('news.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
//        if (Gate::denies('news.view')) {
//            abort(505, trans('messages.you_do_not_have_permission'));
//        }
        $news = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $news,
            ]);
        }

        return view('administrator.projects.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('news.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data     = $this->repository->with('author')->find($id);
        $category = $this->category->where('id', '<>', NewsCategory::NEW_PROMOTION)->get()->toArray();
        $project  = $this->project->get()->toArray();
        return view('administrator.projects.news.edit', compact('data', 'category', 'project'));
    }

    /**
     * Update the specified resource in storage.
     *e
     * @param  NewsUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(NewsUpdateRequest $request, $id) {
        try {
            if (Gate::denies('news.update')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $model                     = $this->repository->model();
            $input                     = $request->all();
            $input[$model::UPDATED_BY] = Auth::user()->id;
            if ($request->hasFile($model::IMAGE_URL)) {
                $input[$model::IMAGE_URL] =  request()->getSchemeAndHttpHost().uploadFile($request->image_url, IMAGE_PROJECT);
            }
            $news = $this->repository->update($input, $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $news->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('news.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }
            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('news.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('news.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $category = $this->category->where('id', '<>', NewsCategory::NEW_PROMOTION)->get()->toArray();
        $project  = $this->project->get()->toArray();
        return view('administrator.projects.news.create', compact('category', 'project'));
    }

    /*
    |--------------------------------------------------------------------------
    | ACCEPT NEWS
    |--------------------------------------------------------------------------
    | @params id news
    | @return object news
    | @Author : haind
     */

    public function putApprove($id) {
//        if (!$this->auth->user()->can('news.approve')) {
//            abort(505, trans('messages.you_do_not_have_permission'));
//        }
        $obj = $this->repository->find($id);
        $obj->is_approve   = ($obj->is_approve == APPROVE) ? ZERO : APPROVE;
        $obj->approve_time = Carbon::now();
        $obj->save();
        if (!$obj->image_url) {
            $obj->image_url = '/upload/no-image.png';
        }
        if (!$obj) {
            return redirect()->route('news.index')->with('error', true)->with('message', trans('messages.update_failed'));
        }
        return redirect()->route('news.index')->with('message', true)->with('message', trans('messages.update_success'));
    }

    public function search(Request $request){
        $input = $request->all();
        $data = News::where('is_approve', APPROVE)->where('category_id', '<>', NewsCategory::NEW_PROMOTION);
        if(isset($input['name'])){
            $data->where('title', 'like', '%'.$input['name'].'%');
        }
        if(isset($input['project_id'])){
            $data->where('project_id', $input['project_id']);
        }
        if(isset($input['category_id'])){
            $data->where('category_id', $input['category_id']);
        }
        $data = $data->paginate();
        $listCategory = NewsCategory::where('id', '<>', NewsCategory::NEW_PROMOTION)->pluck('name', 'id')->toArray();
        return view('administrator.projects.news.index', compact('data', 'input', 'listCategory'));
    }

    public function promotion(){
        $data = News::where('category_id', NewsCategory::NEW_PROMOTION)->orderBy('id', 'desc')->paginate();
        return view('administrator.projects.news.promotion.index', compact('data'));
    }

    public function promotionSearch(Request $request){
        $input = $request->all();
        $data = News::where('category_id', NewsCategory::NEW_PROMOTION);
        if(isset($input['name'])){
            $data->where('title', 'like', '%'.$input['name'].'%');
        }
        $data = $data->paginate();
        return view('administrator.projects.news.promotion.index', compact('data', 'input'));
    }

    public function promotionEdit($id){
        $data = News::find($id);
        return view('administrator.projects.news.promotion.edit', compact('data'));
    }

    public function promotionUpdate(Request $request,$id){
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
        ], [],['title' => 'Tiêu đề', 'description' => 'Mô tả', 'content' => 'Nội dung']);
        if ($validator->fails()) {
            return redirect()->route('news.promotion.edit', $id)
                ->withErrors($validator)
                ->withInput();
        }
        $data = News::find($id);
        $input['updated_by'] = Auth::user()->id;
        if ($request->hasFile(News::IMAGE_URL)) {
            $input[News::IMAGE_URL] =  request()->getSchemeAndHttpHost().uploadFile($request->image_url, IMAGE_PROJECT);
        }
        $input['category_id'] = NewsCategory::NEW_PROMOTION;
        $data->update($input);
        return redirect()->route('news.promotion')->with('message', trans('messages.update_success'));
    }

    public function promotionCreate(){
        return view('administrator.projects.news.promotion.create');
    }

    public function promotionStore(Request $request){
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'description' => 'required',
            'content' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->route('news.promotion.create')
                ->withErrors($validator)
                ->withInput();
        }

        $input['created_by'] = Auth::user()->id;
        if ($request->hasFile(News::IMAGE_URL)) {
            $input[News::IMAGE_URL] =  request()->getSchemeAndHttpHost().uploadFile($request->image_url, IMAGE_PROJECT);
        }
        $input['category_id'] = NewsCategory::NEW_PROMOTION;
        $input['is_approve'] = APPROVE;
        $news = News::create($input);
        dispatch(new FCMNotification($news));
        return redirect()->route('news.promotion')->with('message', trans('messages.create_success'));
    }
}
