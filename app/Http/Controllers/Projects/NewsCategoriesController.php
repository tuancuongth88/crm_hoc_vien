<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Projects\NewsCategoryCreateRequest;
use App\Http\Requests\Projects\NewsCategoryUpdateRequest;
use App\Repositories\Projects\NewsCategoryRepository;
use App\Validators\Projects\NewsCategoryValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class NewsCategoriesController.
 *
 * @package namespace App\Http\Controllers\Projects;
 */
class NewsCategoriesController extends Controller {
    /**
     * @var NewsCategoryRepository
     */
    protected $repository;

    /**
     * @var NewsCategoryValidator
     */
    protected $validator;

    /**
     * NewsCategoriesController constructor.
     *
     * @param NewsCategoryRepository $repository
     * @param NewsCategoryValidator $validator
     */
    public function __construct(NewsCategoryRepository $repository, NewsCategoryValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::denies('news-category.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view('administrator.projects.news-category.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  NewsCategoryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(NewsCategoryCreateRequest $request) {
        try {
            if (Gate::denies('news-category.create')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $model                     = $this->repository->model();
            $input                     = $request->all();
            $input[$model::CREATED_BY] = \Auth::user()->id;
            $newsCategory              = $this->repository->create($input);

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $newsCategory->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('news-category.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (Gate::denies('news-category.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $newsCategory = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $newsCategory,
            ]);
        }

        return view('administrator.projects.news-category.show', compact('newsCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('news-category.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);

        return view('administrator.projects.news-category.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  NewsCategoryUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(NewsCategoryUpdateRequest $request, $id) {
        try {
            if (Gate::denies('news-category.update')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $newsCategory = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $newsCategory->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('news-category.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('news-category.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('news-category.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        return view('administrator.projects.news-category.create');
    }
}
