<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Projects\ProjectCreateRequest;
use App\Http\Requests\Projects\ProjectUpdateRequest;
use App\Models\Projects\Attachment;
use App\Models\Projects\InfoManager;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectManager;
use App\Models\Projects\Province;
use App\Models\Projects\Segment;
use App\Models\Projects\Type;
use App\Models\Users\User;
use App\Repositories\Projects\ProjectRepository;
use App\Validators\Projects\ProjectValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class ProjectsController.
 *
 * @package namespace App\Http\Controllers\Projects;
 */
class ProjectsController extends Controller {
    /**
     * @var ProjectRepository
     */
    protected $repository;

    /**
     * @var ProjectValidator
     */
    protected $validator;

    /**
     * @var ProjectValidator
     */
    protected $type;

    /**
     * ProjectsController constructor.
     *
     * @param ProjectRepository $repository
     * @param ProjectValidator $validator
     */
    public function __construct(ProjectRepository $repository, ProjectValidator $validator, Type $type) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->type       = $type;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::denies('project.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }

        $listCity = Province::pluck('name', 'city_id')->toArray();
        $user     = new User();
        $arrUser  = $user->getAllChildUser(\Auth::user()->id);
        // if (count($arrUser) == ZERO) {
        array_push($arrUser, \Auth::user()->id);
        // }
        $listUser = User::whereIn('id', $arrUser)->pluck('fullname', 'id')->toArray();
        return view('administrator.projects.project.index', compact('listCity', 'listUser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProjectCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ProjectCreateRequest $request) {
        try {
            if (Gate::denies('project.create')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input                            = $request-> all();
            $model                            = $this->repository->model();
            $input[$model::CREATED_BY]        = \Auth::user()->id;
            if(isset($input['customer_age'])){
                $age                              = explode(';', $input['customer_age']);
                $input[$model::CUSTOMER_AGE_FROM] = $age[ZERO];
                $input[$model::CUSTOMER_AGE_TO]   = $age[ONE];
            }
            $project = $this->repository->create($input);
            if(isset($input['director'])){
                $model::saveRelateInfoManager($input['director'], $project, InfoManager::IS_DIRECTOR);
            }
            if(isset($input['admin'])){
                $model::saveRelateInfoManager($input['admin'], $project, InfoManager::IS_ADMIN);
            }

            if ($project) {
                $response = [
                    'message' => trans('messages.create_success'),
                    'data'    => $project->toArray(),
                    'status'  => true,
                ];
            } else {
                $response = [
                    'message' => trans('messages.create_failed'),
                    'status'  => false,
                ];
            }
            return response()->json($response);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (Gate::denies('project.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $project = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $project,
            ]);
        }

        return view('administrator.projects.project.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = $this->repository->find($id);
        if (Gate::denies('project.update', $data)) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $type     = $this->type->get();
        $segments = Segment::get();
        $listCity = \DB::table('provinces')->get()->pluck('name', 'city_id');
        $listProjectManager = ProjectManager::get()->pluck('name', 'id')->toArray();
        return view('administrator.projects.project.edit-new', compact('data', 'type', 'listCity', 'segments', 'listProjectManager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProjectUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ProjectUpdateRequest $request, $id) {
        try {
            $data = $this->repository->find($id);
            if (Gate::denies('project.update', $data)) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $input = $request->all();
            // $listAttachmentId = json_decode($input['attachment']);
            // $attachmentFree   = array_filter($listAttachmentId, 'filterAttachmentFree');

            $model                            = $this->repository->model();
            $input[$model::UPDATED_BY]        = \Auth::user()->id;
            if(isset($input['customer_age'])){
                $age                              = explode(';', $input['customer_age']);
                $input[$model::CUSTOMER_AGE_FROM] = $age[ZERO];
                $input[$model::CUSTOMER_AGE_TO]   = $age[ONE];
            }

            $project = $this->repository->update($input, $id);
            $project->hasInfoManager()->delete();
            if(isset($input['director'])){
                $model::saveRelateInfoManager($input['director'], $project, InfoManager::IS_DIRECTOR);
            }
            if(isset($input['admin'])){
                $model::saveRelateInfoManager($input['admin'], $project, InfoManager::IS_ADMIN);
            }
            if (!$project) {
                $response = [
                    'message' => trans('messages.update_failed'),
                    'status'  => false,
                ];
            }else{
                $response = [
                    'message' => trans('messages.update_success'),
                    'data'    => $project->toArray(),
                    'status'  => true,
                ];
            }
            return response()->json($response);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $data = $this->repository->find($id);
        if (Gate::denies('project.delete', $data)) {
            if (request()->isJson()) {
                return response()->json([
                    'message' => trans('messages.you_do_not_have_permission'),
                    'status'  => false,
                ]);
            }
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        // if (request()->wantsJson()) {

        return response()->json([
            'message' => trans('messages.delete_success'),
            'status'  => $deleted,
        ]);
        // }

        // return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('project.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $type     = $this->type->get();
        $segments = Segment::get();
        $listCity = \DB::table('provinces')->get()->pluck('name', 'city_id');
        $listProjectManager = ProjectManager::get()->pluck('name', 'id')->toArray();
        return view('administrator.projects.project.create-new', compact('type', 'listCity', 'segments', 'listProjectManager'));
    }

    public function getDistrict($cityId) {
        $listDistrict = \DB::table('districts')->where('city_id', $cityId)->get();
        return response()->json($listDistrict);
    }

    public function postUploadAttachment(Request $request) {
        $file                               = $request->file;
        $attachment[Attachment::URL]        = uploadFile($file, IMAGE_PROJECT);
        $attachment[Attachment::NAME]       = $file->getClientOriginalName();
        $attachment[Attachment::SIZE]       = $file->getClientSize();
        $attachment[Attachment::EXTENDSION] = $file->getClientOriginalExtension();
        $obj                                = Attachment::create($attachment);
        return response()->json([
            'message' => trans('messages.upload_success'),
            'data'    => $obj,
        ]);
    }

    public function postDeleteAttachment(Request $request) {
        $file = $request->all();
        $data = Attachment::find($file['id']);
        if ($data) {
            $data->delete();
            unlink(public_path() . $file['url']);
        };
        return response()->json([
            'message' => trans('messages.delete_success'),
            'status'  => true,
        ]);
    }

    public function getList(Request $request) {
        $input = $request->all();
        // $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        // $data = $this->repository->with(['typeProject', 'province', 'district', 'hasAttachment'])->paginate(2)->toArray();
        // $model            = $this->repository->model();
        $user    = new User();
        $arrUser = $user->getAllChildUser(\Auth::user()->id);
        array_push($arrUser, \Auth::user()->id);
        $model = new Project();
        $total = $model::count();
        $model = $model->whereIn('created_by', $arrUser);
        if (isset($input['query'])) {
            foreach ($input['query'] as $key => $value) {
                if ($value == strval(ZERO) || $value == '') {
                    unset($input['query'][$key]);
                }
            }
            if (isset($input['query']['name'])) {
                $model = $model->where('name', 'LIKE', '%' . $input['query']['name'] . '%');
                unset($input['query']['name']);
            }
            $model = $model->where($input['query']);
        } else {
            $model = $model->take($input['pagination']['perpage'])
                ->skip($input['pagination']['perpage'] * ($input['pagination']['page'] - 1));
        }
        $model = $model->with(['typeProject', 'province', 'district', 'hasInfoManager']);

        $data             = $model->orderBy('id', 'DESC')->get()->toArray();
        $response['data'] = $data;
        $meta             = new \stdClass();
        $meta->page       = $input['pagination']['page'];
        $meta->perpage    = $input['pagination']['perpage'];
        $meta->total      = $total;
        $response['meta'] = $meta;

        return response()->json($response);

    }
}
