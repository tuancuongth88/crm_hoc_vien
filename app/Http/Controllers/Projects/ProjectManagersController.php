<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Projects\ProjectManagerCreateRequest;
use App\Http\Requests\Projects\ProjectManagerUpdateRequest;
use App\Repositories\Projects\ProjectManagerRepository;
use App\Validators\ProjectManagerValidator;
use App\Models\Projects\ProjectManager;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

/**
 * Class ProjectManagersController.
 *
 * @package namespace App\Http\Controllers;
 */
class ProjectManagersController extends Controller
{
    /**
     * @var ProjectManagerRepository
     */
    protected $repository;

    /**
     * @var ProjectManagerValidator
     */
    protected $validator;

    /**
     * ProjectManagersController constructor.
     *
     * @param ProjectManagerRepository $repository
     * @param ProjectManagerValidator $validator
     */
    public function __construct(ProjectManagerRepository $repository, ProjectManagerValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('project-manager.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view('administrator.projects.project-manager.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProjectManagerCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ProjectManagerCreateRequest $request)
    {
        try {
            $input = $request->except('file');
            if (Gate::denies('project-manager.create')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $projectManager = $this->repository->create($input);

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $projectManager->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('project-manager.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Gate::denies('project-manager.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $projectManager = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $projectManager,
            ]);
        }

        return view('administrator.projects.project-manager.show', compact('projectManager'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->repository->find($id);
//        if (Gate::denies('project-manager.update')) {
//            abort(505, trans('messages.you_do_not_have_permission'));
//        }

        return view('administrator.projects.project-manager.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProjectManagerUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ProjectManagerUpdateRequest $request, $id)
    {
        try {
            $input = $request->except('file');
            $data = $this->repository->find($id);
            if (Gate::denies('project-manager.update', $data)) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $projectManager = $this->repository->update($input, $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $projectManager->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('project-manager.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->repository->find($id);
        if (Gate::denies('project-manager.delete', $data)) {
            if (request()->isJson()) {
                return response()->json([
                    'message' => trans('messages.you_do_not_have_permission'),
                    'status'  => false,
                ]);
            }
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);
        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'ProjectManager deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'ProjectManager deleted.');
    }

    public function create() {
        if (Gate::denies('project-manager.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }

        return view('administrator.projects.project-manager.create');
    }
}
