<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Projects\TypeCreateRequest;
use App\Http\Requests\Projects\TypeUpdateRequest;
use App\Models\Projects\Project;
use App\Repositories\Projects\TypeRepository;
use App\Validators\Projects\TypeValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class TypesController.
 *
 * @package namespace App\Http\Controllers\Projects;
 */
class TypesController extends Controller {
    /**
     * @var TypeRepository
     */
    protected $repository;

    /**
     * @var TypeValidator
     */
    protected $validator;

    /**
     * @var TypeValidator
     */
    protected $project;

    /**
     * TypesController constructor.
     *
     * @param TypeRepository $repository
     * @param TypeValidator $validator
     */
    public function __construct(TypeRepository $repository, TypeValidator $validator, Project $project) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->project    = $project;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::denies('type.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view('administrator.projects.type.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TypeCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(TypeCreateRequest $request) {
        try {
            if (Gate::denies('type.create')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $model                     = $this->repository->model();
            $input                     = $request->all();
            $input[$model::CREATED_BY] = \Auth::user()->id;
            $type                      = $this->repository->create($input);

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $type->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('type.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (Gate::denies('type.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view('administrator.projects.type.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('type.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);

        return view('administrator.projects.type.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TypeUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(TypeUpdateRequest $request, $id) {
        try {
            if (Gate::denies('type.update')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $type = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $type->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('type.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('type.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $obj = $this->repository->find($id);
        if ($obj && $obj->projects->count() > 0) {
            return redirect()->route('type.index')->with('message', trans('messages.delete_relation_failed'));
        }
        $deleted = $obj->delete();

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('type.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        return view('administrator.projects.type.create');
    }
}
