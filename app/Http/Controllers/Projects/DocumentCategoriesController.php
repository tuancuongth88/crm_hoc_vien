<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Projects\DocumentCategoryCreateRequest;
use App\Http\Requests\Projects\DocumentCategoryUpdateRequest;
use App\Models\Projects\Document;
use App\Repositories\Projects\DocumentCategoryRepository;
use App\Validators\Projects\DocumentCategoryValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class DocumentCategoriesController.
 *
 * @package namespace App\Http\Controllers\Projects;
 */
class DocumentCategoriesController extends Controller {
    /**
     * @var DocumentCategoryRepository
     */
    protected $repository;

    /**
     * @var DocumentCategoryValidator
     */
    protected $validator;

    /**
     * @var DocumentCategoryValidator
     */
    protected $document;

    /**
     * DocumentCategoriesController constructor.
     *
     * @param DocumentCategoryRepository $repository
     * @param DocumentCategoryValidator $validator
     */
    public function __construct(DocumentCategoryRepository $repository, DocumentCategoryValidator $validator, Document $document) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->document   = $document;
        $this->pathView   = 'administrator.projects.document-category';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::denies('document-category.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        // $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        // $data = $this->repository->paginate();

        // if (request()->wantsJson()) {

        //     return response()->json([
        //         'data' => $data,
        //     ]);
        // }

        // return view($this->pathView . '.index', compact('data'));
        return view($this->pathView . '.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DocumentCategoryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(DocumentCategoryCreateRequest $request) {
        try {
            if (Gate::denies('document-category.create')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input                     = $request->all();
            $model                     = $this->repository->model();
            $input[$model::CREATED_BY] = \Auth::user()->id;
            $documentCategory          = $this->repository->create($input);
            $documentCategory->state   = 'closed';
            $response                  = [
                'message' => trans('messages.create_success'),
                'data'    => $documentCategory->toArray(),
            ];

            // if ($request->wantsJson()) {

            return response()->json($response);
            // }

            return redirect()->route('document-category.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (Gate::denies('document-category.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $documentCategory = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $documentCategory,
            ]);
        }

        return view($this->pathView . '.show', compact('documentCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('document-category.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);

        return view($this->pathView . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DocumentCategoryUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(DocumentCategoryUpdateRequest $request, $id) {
        try {
            if (Gate::denies('document-category.update')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $input                     = $request->all();
            $model                     = $this->repository->model();
            $input[$model::UPDATED_BY] = \Auth::user()->id;
            unset($input['created_at']);
            $documentCategory = $this->repository->update($input, $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $documentCategory->toArray(),
            ];

            // if ($request->wantsJson()) {

            return response()->json($response);
            // }

            return redirect()->route('document-category.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('document-category.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $check = $this->document->where('category_id', $id)->count();
        if ($check > 0) {
            return response()->json([
                'message' => trans('messages.please_remove_data_belong_to_this'),
                'status'  => false,
            ]);
        }
        $deleted = $this->repository->delete($id);
        // if (request()->wantsJson()) {

        return response()->json([
            'message' => trans('messages.delete_success'),
            'deleted' => $deleted,
            'status'  => true,
        ]);
        // }

        // return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('document-category.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        return view($this->pathView . '.create');
    }
}
