<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Projects\DocumentCreateRequest;
use App\Http\Requests\Projects\DocumentUpdateRequest;
use App\Models\Education\Document;
use App\Models\Projects\DocumentCategory;
use App\Repositories\Projects\DocumentRepository;
use App\Validators\Projects\DocumentValidator;
use Auth;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class DocumentsController.
 *
 * @package namespace App\Http\Controllers\Projects;
 */
class DocumentsController extends Controller {
    /**
     * @var DocumentRepository
     */
    protected $repository;

    /**
     * @var DocumentValidator
     */
    protected $validator;

    /**
     * @var DocumentCategory
     */
    protected $category;

    /**
     * DocumentsController constructor.
     *
     * @param DocumentRepository $repository
     * @param DocumentValidator $validator
     */
    public function __construct(DocumentRepository $repository, DocumentValidator $validator, DocumentCategory $category) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->category   = $category;
        $this->pathView   = 'administrator.projects.document';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }
        return view($this->pathView . '.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DocumentCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(DocumentCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $model = $this->repository->model();
            $array = (array) $request->files;
            $file  = reset(reset($array)['files']);
            if (!in_array($file->getClientOriginalExtension(), $model::$allowExtendsion)) {
                return response()->json([
                    'error'   => true,
                    'message' => trans('messages.create_failed'),
                ]);
            }
            // if ($file->getSize() >= 10000000) {
            //     dd(333);
            //     return response()->json([
            //         'error'   => true,
            //         'message' => trans('messages.create_failed'),
            //     ]);
            // }
            // $file                       = $request->file;
            $input                      = [];
            $input[$model::CATEGORY_ID] = $request->input($model::CATEGORY_ID);
            $input[$model::CREATED_BY]  = Auth::user()->id;
            $input[$model::NAME]        = $file->getClientOriginalName();
            $input[$model::SIZE]        = $file->getSize();
            $input[$model::EXTENDSION]  = $file->getClientOriginalExtension();
            // if ($file->isValid()) {
            //     dd(44);
            $input[$model::URL] = uploadFile($file, IMAGE_PROJECT);
            // }
            // dd(3333);
            $document             = $this->repository->create($input);
            $document->deleteType = "DELETE";
            $document->deleteUrl  = route('document.destroy', $document->id);
            // $response = [
            //     'message' => trans('messages.create_success'),
            //     'files'   => $document,
            // ];
            $response          = new \stdClass();
            $response->files[] = $document;
            if ($request->wantsJson()) {
                return response()->json($response);
            }
            return redirect()->route('document-category.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $document = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $document,
            ]);
        }

        return view($this->pathView . '.show', compact('document'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $document = $this->repository->find($id);

        return view($this->pathView . '.edit', compact('document'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  DocumentUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(DocumentUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $document = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $document->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('document.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function getUpload($id) {
        $category = $this->category->all();
        return view($this->pathView . '.create', compact('category'));
    }

    public function getTreeData(Request $request) {
        $id       = $request->has('id') ? $request->input('id') : null;
        $category = $this->category->where('parent_id', $id)->get();
        // $category = $this->category->where('parent_id', $id)->with(['files'])->get();
        $data = [];
        foreach ($category as $key => $value) {
            $data[$key] = $value;
            if ($value->parent_id) {
                $data[$key]['_parentId'] = $value->parent_id;
            }
            $data[$key]['state'] = 'closed';
        }
        return response()->json([
            'rows' => $data,
        ]);
    }

}
