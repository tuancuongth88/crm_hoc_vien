<?php

namespace App\Http\Controllers\Projects;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Projects\SegmentCreateRequest;
use App\Http\Requests\Projects\SegmentUpdateRequest;
use App\Repositories\Projects\SegmentRepository;
use App\Validators\Projects\SegmentValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class SegmentsController.
 *
 * @package namespace App\Http\Controllers\Projects;
 */
class SegmentsController extends Controller {
    /**
     * @var SegmentRepository
     */
    protected $repository;

    /**
     * @var SegmentValidator
     */
    protected $validator;

    /**
     * SegmentsController constructor.
     *
     * @param SegmentRepository $repository
     * @param SegmentValidator $validator
     */
    public function __construct(SegmentRepository $repository, SegmentValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->view       = 'administrator.projects.segment';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->view . '.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SegmentCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(SegmentCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $segment = $this->repository->create($request->all());

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $segment->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('segment.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->view . '.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = $this->repository->find($id);

        return view($this->view . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  SegmentUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(SegmentUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $segment = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $segment->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('segment.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        return view($this->view . '.create');
    }
}
