<?php

namespace App\Http\Controllers\Apis;

use Illuminate\Http\Request;
use App\Services\ResponseService;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class BaseApiController extends Controller
{
    public function __construct() {
    }

    public function validateInput($input, $rule)
    {
        $listRule = $this->requiredInput($rule);
        $checkValidate =  Validator::make($input, $listRule);
        if($checkValidate->fails()){
            $errors = $checkValidate->errors();
            $response = new ResponseService();
            return $response->api_json(false, $errors, 'Thiếu trường dữ liệu', 204);
        }else{
            return false;
        }
    }

    public function requiredInput($rule, $typeValidate = 'required'){
        $listRule = array();
        foreach ($rule as $value){
            $listRule[$value] = $typeValidate ;
        }
        return $listRule;
    }


}
