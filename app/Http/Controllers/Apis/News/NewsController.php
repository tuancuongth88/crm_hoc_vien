<?php

namespace App\Http\Controllers\Apis\News;

use App\Models\Projects\News;
use App\Models\Projects\NewsCategory;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    private $request;

    private $response;

    private $model;

    private $account;

    public function __construct(Request $request, NewsCategory $model, ResponseService $response)
    {
        $this->request  = $request;
        $this->response = $response;
        $this->model    = $model;
        $this->account = Auth::guard('api')->user();
    }

    public function index(){
        $listOption = $this->model->get();
        return $this->response->json(CODE_SUCCESS, $listOption, "");
    }

    public function view(Request $request){
        $new = News::find($request['id']);
        return $this->response->json(CODE_SUCCESS, $new, "");
    }

    public function getListNew(){
        $input = $this->request->all();
        $listRule = [
            'project_id'    => 'required',
            'category_id'   => 'required',
            'page'          => 'required',
            'number_record' => 'required',
        ];
        $validator = validateInput($this->request->all(), $listRule, []);
        if ($validator) {
            return $this->response->json(CODE_NO_ACCESS, $validator->getMessageBag(), 'Lỗi dữ liệu');
        }
        $page           = $this->request['page'];
        $number_record  = isset($input['number_record']) ? $this->request['number_record'] : PAGINATE;
        if($this->request['category_id'] != NewsCategory::NEW_PROMOTION){
            $listNew = News::where('category_id', $this->request['category_id'])
                ->where('project_id', $this->request['project_id'])->orderBy('id', 'desc')
                ->skip(($page - 1) * $number_record)
                ->take($number_record)
                ->get();
        }else{
            $listNew = News::where('category_id', $this->request['category_id'])
                            ->orderBy('id', 'desc')
                            ->skip(($page - 1) * $number_record)
                            ->take($number_record)->get();
        }
        return $this->response->json(Response::HTTP_OK, $listNew, null);
    }

    public function webView($slug){
        $news = News::Slug($slug)->first();
        return view('api.news.index', compact('news'));
    }

}
