<?php

namespace App\Http\Controllers\Apis\Projects;

use App\Models\Customers\Accounts;
use App\Models\Customers\FeedbackSale;
use App\Models\Projects\ProjectAccount;
use App\Models\Projects\ProjectManager;
use App\Models\Users\AccountTransactions;
use App\Models\Users\User;
use App\Services\AuthService;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\JWTAuth;

class ProjectsController extends Controller
{

    private $request;

    private $response;

    private $model;

    private $account;

    public function __construct(Request $request, ProjectManager $model, ResponseService $response,JWTAuth $auth)
    {
        $this->request  = $request;
        $this->response = $response;
        $this->model    = $model;
        $this->account = Auth::guard('api')->user();
    }

    public function index(){
        $listProject    = ProjectAccount::join('project_managers', 'project_managers.id', '=', 'account_projects.project_id')
                        ->where('account_projects.account_id', Auth::guard('api')->user()->id)
                        ->whereNull('account_projects.deleted_at')
                        ->select('project_managers.id', 'name', 'image_url', 'hotline', 'email', 'description', 'rank_project', 'project_managers.created_at', 'project_managers.updated_at')
                        ->get();
        if($listProject->count() > 0){
            return $this->response->json(Response::HTTP_OK, $listProject, 'Danh sách dự án');
        }
        return $this->response->json(Response::HTTP_OK, $listProject, 'Chưa có dự án nào được cập nhật cho bạn');
    }

    public function postRankProject(){
        $listRule = [
            'id' => 'required',
            'rank_project' => 'required',
        ];
        $fieldVN = [
            'id' => 'ID dự án',
            'rank_project' => 'Xếp hạng dự án',
        ];

        $validator = validateInput($this->request->all(), $listRule, $fieldVN);
        if ($validator) {
            return $this->response->json(CODE_NO_ACCESS, $validator->getMessageBag(), 'Lỗi dữ liệu');
        }
        $projectAccount = ProjectAccount::where('account_id', $this->account->id)
                                        ->where('project_id', $this->request->get('id'))->first();
        if($projectAccount){
            $projectAccount->rank_project = $this->request->get('rank_project');
            $projectAccount->save();
            return $this->response->json(Response::HTTP_OK, '', 'Đánh giá dự án thành công');
        }else{
            return $this->response->json(CODE_NOT_FOUND, '', 'Dự án không tồn tại');
        }
    }

    public function getHistoryPayment(){
        $listRule = [ 'id' => 'required'];
        $fieldVN = [ 'id' => 'ID dự án'];
        $result = $this->validateInputApi($listRule, $fieldVN);
        if($result){
            return $result;
        }
        $data = AccountTransactions::where('account_id', $this->account->id)
            ->where('project_id', $this->request->id)
            ->where('parent_id', 0)->select('id', 'name')->with('children')->get();
        return $this->response->json(Response::HTTP_OK, $data, '');
    }

    //TODO chua lam xong
    public function listSaleProject(){
        $listRule = [ 'id' => 'required'];
        $fieldVN = [ 'id' => 'ID dự án'];
        $result = $this->validateInputApi($listRule, $fieldVN);
        if($result) {
            return $result;
        }
        $listSale = AccountTransactions::where('parent_id', 0)
                                    ->where('account_id', $this->account->id)
                                    ->where('project_id', $this->request->id)->get();
        $data = array();
        foreach ($listSale as $key => $value){

            $data[$key]['id'] = $value->sale->id;
            $data[$key]['transaction_id'] = $value->id;
            $data[$key]['fullname'] = $value->sale->fullname;
            $data[$key]['avatar'] = $value->sale->avatar;
            $data[$key]['name'] = $value->name ;
            $data[$key]['project_name'] = $value->project->name;
            $data[$key]['rank'] = 0;
            $totalRank = FeedbackSale::where('transaction_id', $value->id)->sum('rank_sale');
            if($totalRank > 0){
                $data[$key]['rank'] = round($totalRank / FeedbackSale::where('transaction_id', $value->id)->count());
            }
        }
        return $this->response->json(Response::HTTP_OK, $data, '');
    }

    public function postFeedBack(){
        $feedback['account_id']     = $this->account->id;
        $feedback['transaction_id'] = $this->request->transaction_id;
        $feedback['sale_id']        = $this->request->sale_id;
        $feedback['rank_sale']        = $this->request->rank_sale;
        $feedback['note']        = $this->request->note;
        $result = FeedbackSale::create($feedback);
        return $this->response->json(Response::HTTP_OK, $result, trans('messages.create_success'));
    }

    private function validateInputApi($listRule, $fieldVN){
        $validator = validateInput($this->request->all(), $listRule, $fieldVN);
        if ($validator) {
            return $this->response->json(CODE_NO_ACCESS, $validator->getMessageBag(), 'Lỗi dữ liệu');
        }
        if(isset($listRule['id'])){
            $checkProject = ProjectManager::find($this->request->id);
            if(!$checkProject){
                return $this->response->json(CODE_NOT_FOUND, '', 'Dự án không tồn tại');
            }
        }
        return false;
    }
}
