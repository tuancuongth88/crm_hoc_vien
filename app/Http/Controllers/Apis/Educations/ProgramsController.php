<?php

namespace App\Http\Controllers\Apis\Educations;

use App\Http\Controllers\Apis\BaseApiController;
use App\Models\Education\Program;
use App\Services\ResponseService;
use Illuminate\Http\Request;

class ProgramsController extends BaseApiController
{
    private $response;
    private $model;

    public function __construct(ResponseService $response) {
        $this->response = $response;
        $this->model    = new Program();
    }
    public function getIndex() {
        $listProgram = $this->model->select('id', 'name')->get()->toArray();
        $result      =  $this->response->api_json(true, $listProgram, '', 200);
        return $result;
    }
}