<?php

namespace App\Http\Controllers\Apis\Educations;

use App\Http\Controllers\Apis\BaseApiController;
use App\Models\Education\Lesson;
use App\Models\Education\Subject;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SubjectsController extends BaseApiController
{
    private $response;
    private $model;

    public function __construct(ResponseService $response) {
        $this->response = $response;
        $this->model    = new Subject();
    }

    public function getDetail(Request $request) {
        $input  = $request->all();
        $checkValidation =  $this->validateInput($input, ['id']);
        if($checkValidation) {
            return $checkValidation->content();
        }
        $id     = $input['id'];
        $listSubject = $this->model->where('status', STATUS_ONE)->find($id);

        if($listSubject) {
            $result =  $this->response->api_json(true, $listSubject, '', 200);
        } else {
            $result =  $this->response->api_json(false, $listSubject, 'Không có dữ liệu trả về', 204);
        }
        return $result;
    }

    public function getCategory(Request $request) {
        $input      = $request->all();
        $checkValidation =  $this->validateInput($input, ['id','page', 'number_record']);
        if($checkValidation) {
            return $checkValidation->content();
        }
        $id               = $input['id'];
        $page             = $input['page'];
        $number_record    = isset($input['number_record']) ? $input['number_record'] : PAGINATE;
        $data_store       = $this->model->where([['program_id', '=', $id], ['status', '=', STATUS_ONE]])
                                        ->take($number_record)->skip(($page - 1) * $number_record)->get()->toArray();
        if(empty($data_store)) {
            $result = $this->response->api_json(false, $data_store, 'Không có dữ liệu trả về', 204);
        } else {
            $result = $this->response->api_json(true, $data_store, '', 200);
        }
        return $result;
    }

    public function getSubjectHot(Request $request) {
        $input      = $request->all();
        $checkValidation =  $this->validateInput($input, ['page', 'number_record']);
        if($checkValidation) {
            return $checkValidation->content();
        }
        $page           = $input['page'];
        $number_record  = isset($input['number_record']) ? $input['number_record'] : PAGINATE;
        $data_store     = $this->model->where([['is_hot', '=', ISHOT_ONE], ['status', '=', STATUS_ONE]])
                                      ->take($number_record)->skip(($page - 1) * $number_record)->get()->toArray();
        if(empty($data_store)) {
            $result = $this->response->api_json(false, $data_store, 'Không có dữ liệu trả về', 204);
        } else {
            $result = $this->response->api_json(true, $data_store, '', 200);
        }
        return $result;
    }

    public function getLessons(Request $request) {
        $input           = $request->all();
        $checkValidation =  $this->validateInput($input, ['id']);
        if($checkValidation) {
            return $checkValidation->content();
        }
        $id          = $input['id'];
        $data        = $this->model->find($id);
        foreach ($data->lesson as $lessons) {
            $parent = $lessons->parent;
        }
        $data_store = [];

        $listParent = Lesson::where('id', '=', $parent)->select('name')->get();
        foreach ($listParent as $key => $value) {
            $item = new \stdClass();
            $item->name = $value->name;
            $item->item= Lesson::where('parent', '=', $parent)->select('name', 'number_session', 'number_hours')->get()->toArray();
            $data_store[$key] = $item;
        }
        if(empty($data_store)) {
            $result = $this->response->api_json(false, $data_store, 'Không có dữ liệu trả về', 204);
        } else {
            $result = $this->response->api_json(true, $data_store, '', 200);
        }
        return $result;
    }
}