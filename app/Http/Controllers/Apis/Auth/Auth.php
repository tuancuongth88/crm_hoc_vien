<?php
namespace App\Http\Controllers\Apis\Auth;


use App\Models\Customers\Account;
use App\Models\Customers\AccountFireBase;
use Illuminate\Support\Facades\Config;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;

trait Auth
{

    public function login()
    {
        $listRule = [
            'phone' => 'required',
            'password' => 'required|min:6',
        ];
        $fieldVN = [
            'phone' => 'Điện thoại',
            'password' => 'mật khẩu',
        ];

        $validator = validateInput($this->request->all(), $listRule, $fieldVN);
        if ($validator) {
            return $this->response->json(CODE_NO_ACCESS, $validator->getMessageBag(), 'Tài khoản và mật khẩu không được bỏ trống.');
        }
        $account = $this->account->where('phone', $this->request->input('phone'))->first();
        if (!$account) {
            return $this->response->json(CODE_UNAUTHORIZED, '', 'Tài khoản không tồn tại');
        }
        if ($account->active == 0) {
            return $this->response->json(CODE_UNAUTHORIZED, '', 'Tài khoản chưa kích hoạt');
        }
        $credentials = $this->request->only('phone', 'password');
        try {
            Config::set('auth.defaults.guard', 'api');
            Config::set('auth.defaults.passwords', 'accounts');
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->response->json(CODE_UNAUTHORIZED, '', 'mật khẩu và tài khoản không hợp lệ');
            }
        } catch (JWTException $e) {
            return $this->response->json(CODE_ERROR_SERVER, '', trans('Có lỗi xảy ra'));
        }
        $account['token'] = $token;
        return $this->response->json(CODE_SUCCESS, $account, 'Đăng nhập thành công');
    }


    public function logout()
    {
        $input = $this->request->all();
        $rules = [
            'device_id' => 'required',
            'token'     => 'required',
        ];
        $validator = validateInput($input, $rules, []);
        if ($validator) {
            return $this->response
                ->json(CODE_NO_ACCESS, $validator->getMessageBag(), trans('messages.something_went_wrong'));
        }
        $removeTokenFB = $this->removeTokenFireBase();
        if(!$removeTokenFB){
            return $this->response->json(CODE_NO_ACCESS, null, trans('messages.device_not_fount'));
        }

        $token = $this->request->header('Authorization');
        try {
            \Illuminate\Support\Facades\Auth::logout();
            // Invalidate the token
            JWTAuth::invalidate($token);
            return $this->response->json(CODE_SUCCESS, '', trans('messages.logout_success'));
        } catch (\Exception $e) {
            return $this->response->json(CODE_ERROR_SERVER, $e->getMessage(), trans('Có lỗi sảy ra'));
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return $this->response->json(CODE_ERROR_SERVER, '', trans('Failed to logout, please try again.'));
        }
    }

    private function removeTokenFireBase(){
        $user = \Auth::guard('api')->user();
        $accountFireBase = AccountFireBase::where('account_id', $user->id)
                                            ->where('device_id', $this->request['device_id'])->first();
        if($accountFireBase){
            $accountFireBase->delete();
            return true;
        }
        return false;
    }

    private function validator(array $array)
    {
        return Validator::make($array, [
            'email' => 'required',
            'password' => 'required|min:6',
        ]);
    }
}
