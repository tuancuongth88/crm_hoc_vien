<?php

namespace App\Http\Controllers\Apis\Auth;

use App\Jobs\SendSMS;
use App\Models\Customers\Accounts;
use App\Notifications\AppHelpdeskNotification;
use App\Services\AuthService;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    use Auth;
    private $request;

    private $response;

    private $account;

    public function __construct(Request $request, Accounts $account, ResponseService $response, AuthService $auth)
    {
        $this->request = $request;
        $this->account = $account;
        $this->auth = $auth;
        $this->response = $response;
    }

    public function postLogin()
    {
        return $this->login();
    }
    public function postLogout()
    {
        return $this->logout();
    }

    public function postForgot(Request $request){
        $input = $request->all();
        $listRule = [
            'phone' => 'required',
        ];
        $fieldVN = [
            'phone' => 'Tài khoản',
        ];
        $validator = validateInput($input, $listRule, $fieldVN);
        if($validator){
            return $this->response->json(CODE_NO_ACCESS, $validator->getMessageBag(), 'Lỗi dữ liệu');
        }
        $account = $this->account->where('phone', $input['phone'])->first();
        if (!$account) {
            return $this->response->json(CODE_UNAUTHORIZED, '', 'Tài khoản không tồn tại');
        }
        //xử lý reset pass

        $password = randomPassword(6);
        $account['password'] = Hash::make($password);

        //send email
        $this->sendNotificationEmail_SMS($account, $password);
        //send sms
        dispatch(new SendSMS($account, $password));
        $account->save();
        return $this->response->json(CODE_SUCCESS, '', trans('messages.app_forgot_password_success'));
    }

    public function sendNotificationEmail_SMS($account, $password){
        $title    = 'Hệ thống chăm sóc khách hàng Hải Phát';
        $message  = 'Tài khoản của bạn là:<span style="color:#ff9f00;"><b> '.$account->phone.'</b></span><br>';
        $message .= 'Mật khẩu:<span style="color:#ff9f00;"><b> '. $password .'</b></span><br>';
        $message .= 'Vui lòng tải ứng dụng tại: <br>';
        $message .= 'Android: <a href="'.env('LINK_ANDROID').'">Tại đây</a> <br>';
        $message .= 'IOS: <a href="'.env('LINK_IOS').'">Tại đây</a> <br>';
        $account->notify(new AppHelpdeskNotification($account, $title, $message, 'Hệ thống CSKH Hải Phát Land thông báo'));
    }

}
