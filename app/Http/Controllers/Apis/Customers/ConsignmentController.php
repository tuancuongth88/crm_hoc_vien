<?php

namespace App\Http\Controllers\Apis\Customers;

use App\Models\Customers\Consignments;
use App\Models\Customers\OptionConsignments;
use App\Models\Projects\District;
use App\Models\Projects\Province;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ConsignmentController extends Controller
{
    private $request;
    private $account;
    private $model;
    private $response;
    public function __construct(Request $request, Consignments $consignments, ResponseService $response)
    {
        $this->request = $request;
        $this->account = Auth::guard('api')->user();
        $this->model   = $consignments;
        $this->response= $response;
    }
    public function index(){
        $data = $this->model->where('account_id', $this->account->id)->orderBy('id', 'desc')->get();
        return $this->response->json(CODE_SUCCESS, $data, '');
    }

    public function storage(){
        $input = $this->request->all();
        $rules = [
            'fullname' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'title' => 'required',
            'product_type' => 'required',
            'land_type' => 'required',
            'address' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'area' => 'required',
            'area_type' => 'required',
            'price' => 'required',
            'price_type' => 'required',
            'description' => 'required',
            'images' => 'required',
        ];
        $validator = validateInput($input, $rules, []);
        if ($validator) {
            return $this->response
                ->json(CODE_NO_ACCESS, $validator->getMessageBag(), trans('messages.something_went_wrong'));
        }
        $input['status'] = Consignments::STATUS_TIEP_NHAN;
        $input['account_id'] = $this->account->id;
        $result = $this->model->create($input);
        if($result){
            return $this->response->json(CODE_SUCCESS, '', trans('messages.create_success'));
        }else{
            return $this->response->json(CODE_NO_ACCESS, '', trans('messages.create_failed'));
        }
    }

    public function update(){
        $input = $this->request->all();
        $rules = [
            'id' => 'required',
            'fullname' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'title' => 'required',
            'product_type' => 'required',
            'land_type' => 'required',
            'address' => 'required',
            'city_id' => 'required',
            'district_id' => 'required',
            'area' => 'required',
            'area_type' => 'required',
            'price' => 'required',
            'price_type' => 'required',
            'description' => 'required',
        ];
        $validator = validateInput($input, $rules, []);
        if ($validator) {
            return $this->response
                ->json(CODE_NO_ACCESS, $validator->getMessageBag(), trans('messages.something_went_wrong'));
        }
        $consigment = $this->model->find($input['id']);
        $consigment->update($input);
        return $this->response->json(CODE_SUCCESS, '', trans('messages.update_success'));
    }

    public function delete(){

    }

    public function updateStatus()
    {
        $input = $this->request->all();
        $rules = [
            'id' => 'required',
        ];
        $validator = validateInput($input, $rules, []);
        if ($validator) {
            return $this->response
                ->json(CODE_NO_ACCESS, $validator->getMessageBag(), trans('messages.something_went_wrong'));
        }
        $consigment = $this->model->find($input['id']);
        $consigment->status = Consignments::STATUS_HUY;
        $consigment->save();
        return $this->response->json(CODE_SUCCESS, '', trans('messages.update_success'));
    }

    public function optionConsignment(){
        $data['product_type'] = OptionConsignments::where('type', OptionConsignments::PRODUCT_TYPE)->with('children')->get();
        $data['city']         = Province::with('district')->get();
        $data['area']         = OptionConsignments::where('type', OptionConsignments::AREA_TYPE)->get();
        $data['price_type']   = OptionConsignments::where('type', OptionConsignments::PRICE_TYPE)->get();
        return $this->response->json(CODE_SUCCESS, $data, '');
    }

}
