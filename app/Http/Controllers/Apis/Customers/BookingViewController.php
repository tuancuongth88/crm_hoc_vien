<?php

namespace App\Http\Controllers\Apis\Customers;

use App\Models\Customers\BookingViewHouse;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BookingViewController extends Controller
{
    private $request;
    private $account;
    private $model;
    private $response;
    public function __construct(Request $request, BookingViewHouse $consignments, ResponseService $response)
    {
        $this->request = $request;
        $this->account = Auth::guard('api')->user();
        $this->model   = $consignments;
        $this->response= $response;
    }

    public function store(){
        $input = $this->request->all();
        $input[$this->model::ACCOUNT_ID] = $this->account->id;
        $rules = [
            'fullname' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'time_view' => 'required',
            'note' => 'required',
        ];
        $validator = validateInput($input, $rules, []);
        if ($validator) {
            return $this->response
                ->json(CODE_NO_ACCESS, $validator->getMessageBag(), trans('messages.something_went_wrong'));
        }
        $result = BookingViewHouse::create($input);
//        dd($input);
        if($result){
            return $this->response->json(CODE_SUCCESS, '', trans('messages.create_success'));
        }else{
            return $this->response->json(CODE_NO_ACCESS, '', trans('messages.create_failed'));
        }
    }
}
