<?php

namespace App\Http\Controllers\Apis\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AppHelpDesk\AccountsUpdateRequest;
use App\Jobs\AccountProcess;
use App\Models\Customers\AccountFireBase;
use App\Models\Customers\Accounts;
use App\Models\Customers\FollowerNews;
use App\Models\Projects\NewsCategory;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\Customers\AccountsRepository;
use App\Validators\Customers\AccountsValidator;

/**
 * Class AccountsController.
 *
 * @package namespace App\Http\Controllers\Customers;
 */
class AccountsController extends Controller
{
    /**
     * @var AccountsRepository
     */
    protected $repository;

    /**
     * @var AccountsValidator
     */
    protected $validator;

    private $response;

    private $user;

    /**
     * AccountsController constructor.
     *
     * @param AccountsRepository $repository
     * @param AccountsValidator $validator
     */
    public function __construct(AccountsRepository $repository, AccountsValidator $validator, ResponseService $response)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->response = $response;
        $this->user = Auth::guard('api')->user();
    }

    public function postChangePassword(Request $request)
    {
        $changePasRules = [
            'current_password' => 'required',
            'new_password' => 'required|string|min:6|confirmed',
        ];

        $validator = validateInput($request->all(), $changePasRules, Accounts::getFieldVietnamese());
        if ($validator) {
            return $this->response
                ->json(CODE_CREATE_FAILED, $validator->getMessageBag(), trans('messages.create_failed'));
        }

        $account = Auth::guard('api')->user();
        if (!(Hash::check($request->get('current_password'), $account->password))) {
            return $this->response->json(CODE_MULTI_STATUS, '', 'Mật khẩu hiện tại không đúng');
        }

        if (strcmp($request->get('current_password'), $request->get('new_password')) == 0) {
            return $this->response->json(CODE_MULTI_STATUS, '', "Mật khẩu cũ không được giống mật khẩu mới.");
        }
        try {

            //Change Password
            $account->password = Hash::make($request->get('new_password'));
            $account->save();
            return $this->response->json(CODE_SUCCESS, '', "Thay đổi mật khẩu thành công !");
        } catch (ErrorException $e) {
            return $this->response->json(CODE_ERROR_SERVER, '', trans('Có lỗi xảy ra'));
        }
    }

    public function update(Request $request)
    {
        try {
            $updateRules = [
                Accounts::FULLNAME => 'required|max:60',
            ];
            $input = $request->all();
            $validator = validateInput($input, $updateRules, Accounts::getFieldVietnamese());
            if ($validator) {
                return $this->response
                    ->json(CODE_CREATE_FAILED, $validator->getMessageBag(), trans('messages.create_failed'));
            }
            $account = $this->repository->update($input, $this->user->id);
            if($account){
                dispatch(new AccountProcess($account, 'update'));
            }
            return $this->response->json(Response::HTTP_OK, $account->toArray(), 'Cập nhật thành công');
        } catch (ErrorException $e) {
            return response()->json([
                'error' => true,
                'message' => $e->getMessageBag()
            ]);

        }
    }

    public function getProfile(){
        $account = $this->user;
        return $this->response->json(CODE_SUCCESS, $account, 'Thông tin cá nhân');
    }

    public function postChangeAvatar(Request $request){
        $rules['file'] = 'required|image|mimes:jpeg,bmp,png|max:2000';
        $validator = validateInput($request->all(), $rules, []);
        if ($validator) {
            return $this->response->json(Response::HTTP_NOT_FOUND, $validator->getMessageBag(), trans('messages.update_failed'));
        }
        try {
            if ($request->hasFile('file')) {
                $file = $request->file;
                $destinationPath = public_path() . IMAGEUSER;
                $filename = time() . '_' . str_replace(' ', '', $file->getClientOriginalName());
                $uploadSuccess = $file->move($destinationPath, $filename);
                $data['avatar'] = str_replace('/api/user/update-avatar', '', $request->url()).IMAGEUSER . $filename;
            }
            if (isset($data['avatar'])) {
                $user = $this->user;
                $avatar_old = $user->avatar;
                $user->avatar = $data['avatar'];
                $user->save();
                if (isset($user) && isset($avatar_old)) {
                    if (file_exists(public_path() . $avatar_old)) {
                        unlink(public_path() . $avatar_old);
                    }
                }
                dispatch(new AccountProcess($user, 'update'));
                return $this->response->json(CODE_SUCCESS, $user, "Thay đổi ảnh thành công!");
            }
        } catch (\Exception $e) {
            return $this->response->json(CODE_ERROR_SERVER, '', trans('Có lỗi xảy ra'));
        }
    }

    public function getFollower(){
        $listNews = NewsCategory::select('id', 'name')->get();
        $listOption = array();
        foreach ($listNews as $key => $value){
            $checkFollower = FollowerNews::where(FollowerNews::ACCOUNT_ID, $this->user->id)->where('category_new_id', $value->id)->first();
            $listOption[$key]['id'] = $value->id;
            $listOption[$key]['name'] = $value->name;
            if($checkFollower){
                $listOption[$key]['status'] = true;
            }else{
                $listOption[$key]['status'] = false;
            }
        }
        return $this->response->json(CODE_SUCCESS, $listOption, "");
    }

    public function postFollowNew(Request $request){
        $rules = [
            'id' => 'required',
            'status' => 'required',
        ];

        $validator = validateInput($request->all(), $rules, []);
        if ($validator) {
            return $this->response
                ->json(CODE_NO_ACCESS, $validator->getMessageBag(), trans('messages.something_went_wrong'));
        }
        $category = NewsCategory::find($request['id']);
        if(!$category){
            return $this->response
                ->json(CODE_NO_ACCESS, null, 'Id không tồn tại');
        }
        $data = FollowerNews::where(FollowerNews::ACCOUNT_ID, $this->user->id)->where('category_new_id', $request['id']);
        if(json_decode($request['status'])){
            $input['account_id'] = $this->user->id;
            $input['category_new_id'] = $request['id'];
            if($data->count() > 0){
                $result = $data->update($input);
            }else{
                $result = FollowerNews::create($input);
            }
        }else{
            $result = $data->delete();
        }
        return $this->response->json(Response::HTTP_OK, null, trans('messages.update_success'));
    }

    public function addTokenFireBase(Request $request){
        $input = $request->all();
        $rules = [
            'type_device' => 'required',
            'device_name' => 'required',
            'device_id' => 'required',
            'token' => 'required',
        ];

        $validator = validateInput($input, $rules, []);
        if ($validator) {
            return $this->response
                ->json(CODE_NO_ACCESS, $validator->getMessageBag(), trans('messages.something_went_wrong'));
        }
        $input['account_id'] = $this->user->id;
        //check device da ton tại chua
        $checkToken = AccountFireBase::where('device_id', $input['device_id'])->where('account_id', $this->user->id)->first();
        if($checkToken){
            $result = $checkToken->update($input);
        }else{
            $result = AccountFireBase::create($input);
        }
        if($result){
            return $this->response->json(Response::HTTP_OK, null, trans('messages.update_success'));
        }
        return $this->response->json(CODE_NO_ACCESS, null, trans('messages.update_failed'));
    }


}
