<?php

namespace App\Http\Controllers\Apis\Customers;

use App\Http\Controllers\Apis\BaseApiController;
use App\Models\Customers\Customer;
use App\Services\ResponseService;
use Illuminate\Http\Request;

class CustomersController extends BaseApiController
{
    private $response;
    private $model;

    public function __construct(ResponseService $response) {
        $this->response = $response;
        $this->model    = new Customer();
    }

    public function getIndex(Request $request) {
        $input      = $request->all();
        $checkValidation =  $this->validateInput($input, ['page', 'number_record']);
        if($checkValidation) {
            return $checkValidation->content();
        }
        $name           = isset($input['name']) ? $input['name'] : '';
        $page           = $input['page'];
        $number_record  = isset($input['number_record']) ? $input['number_record'] : PAGINATE;
        $data_store     = $this->model->where('fullname',  'like', '%' . $name . '%')
                                      ->take($number_record)->skip(($page - 1) * $number_record)->get()->toArray();
        if(empty($data_store)) {
            $result = $this->response->api_json(false, $data_store, 'Không có dữ liệu trả về', 204);
        } else {
            $result = $this->response->api_json(true, $data_store, '', 200);
        }
        return $result;
    }

}