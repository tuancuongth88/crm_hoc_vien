<?php

namespace App\Http\Controllers\Systems;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Systems\PositionCreateRequest;
use App\Http\Requests\Systems\PositionUpdateRequest;
use App\Repositories\Systems\PositionRepository;
use App\Validators\Systems\PositionValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class PositionsController.
 *
 * @package namespace App\Http\Controllers\Systems;
 */
class PositionsController extends Controller {
    /**
     * @var PositionRepository
     */
    protected $repository;

    /**
     * @var PositionValidator
     */
    protected $validator;

    /**
     * PositionsController constructor.
     *
     * @param PositionRepository $repository
     * @param PositionValidator $validator
     */
    public function __construct(PositionRepository $repository, PositionValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->orderBy('id', 'desc')->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view('administrator.systems.position.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PositionCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(PositionCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $model                     = $this->repository->model();
            $input                     = $request->all();
            $input[$model::CREATED_BY] = \Auth::user()->id;
            $position                  = $this->repository->create($input);

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $position->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('position.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $position = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $position,
            ]);
        }

        return view('administrator.systems.position.show', compact('position'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = $this->repository->find($id);

        return view('administrator.systems.position.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PositionUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(PositionUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $position = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $position->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('position.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        return view('administrator.systems.position.create');
    }
}
