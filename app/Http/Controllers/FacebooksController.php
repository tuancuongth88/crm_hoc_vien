<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Session;

class FacebooksController extends Controller {

    public function __construct() {
        Session::put('fb_user_access_token', 'EAAcJKurDr08BAHWdoB6ZCC6BcKtQzamYoYw4PxNqXWIbQRemyrAhR42UAR56KazZChwfKpQrYvIzbL7GF6Dj13sdj8TXI8rdEZBwLODyRqZBzqvI6CZBBv5uuURRYuiLElg1sLUmnPWkruNpxktPDZC9Oc0g6a0LTaot8gK2gpSEXWXBkxKzmBY8shNwPittwZD');
        $this->accessToken = Session::get('fb_user_access_token');
        $this->url         = 'https://graph.facebook.com/';
    }

    public function getFanpage() {
        $client = new Client();
        $url    = $this->url . 'me/accounts?access_token=' . $this->accessToken;
        $res    = $client->request('GET', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody())->data;
        return view('administrator.facebooks.fanpages.index', compact('data'));
    }

    public function getPostByPage($pageId, Request $request) {
        $client          = new Client();
        $pageAccessToken = $request->input('access_token');
        $url             = $this->url . $pageId . '?access_token=' . $pageAccessToken . '&fields=posts{created_time,message,full_picture,story},picture,name';
        $res             = $client->request('GET', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody());
        return view('administrator.facebooks.fanpages.posts', compact('data', 'pageId', 'pageAccessToken'));
    }

    public function getCommentByPost($postId) {
        $client = new Client();
        $url    = $this->url . $postId . '?access_token=' . $this->accessToken . '&fields=comments,message,from';
        $res    = $client->request('GET', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody());
        dd($data);
        return view('administrator.facebooks.fanpages.comments', compact('data'));
    }

    public function getPageByUrl($urlPage) {
        $client = new Client();
        $url    = $this->url . '?access_token=' . $this->accessToken . '&id=' . $urlPage;
        $res    = $client->request('GET', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody());
        return view('administrator.facebooks.fanpages.posts', compact('data'));
    }

    public function getInboxByPage($pageId, Request $request) {
        $pageAccessToken = $request->input('access_token');

        $client = new Client();
        $url    = $this->url . $pageId . '?access_token=' . $pageAccessToken . '&fields=conversations{message_count,unread_count,senders,id}';
        $res    = $client->request('GET', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody())->conversations->data;
        return view('administrator.facebooks.fanpages.inbox-chat', compact('data', 'pageAccessToken', 'pageId'));
    }

    public function getConversationDetail($conversationId, Request $request) {
        $pageAccessToken = $request->input('access_token');
        $pageId          = $request->input('page_id');
        $client          = new Client();
        $url             = $this->url . $conversationId . '/messages?access_token=' . $pageAccessToken . '&fields=id,created_time,message,from';
        $res             = $client->request('GET', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody())->data;
        return view('administrator.facebooks.fanpages.conversation', compact('data', 'pageId'));
    }

    public function getFeedsByPage($pageId, Request $request) {
        $pageAccessToken = $request->input('access_token');
        $client          = new Client();
        $url             = $this->url . $pageId . '/feed?access_token=' . $pageAccessToken . '&fields=message,from';
        $res             = $client->request('GET', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody());
        dd($data);
    }

    public function postFeedToPage($pageId, Request $request) {
        $pageAccessToken = $request->input('access_token');
        $message         = $request->input('message');
        $client          = new Client();
        $url             = $this->url . $pageId . '/feed?access_token=' . $pageAccessToken . '&message=' . $message;
        $res             = $client->request('POST', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody());
        dd($data);
    }

    public function postCommentToPost($pageId, Request $request) {
        $postId          = $request->input('post_id');
        $id              = $pageId . '_' . $postId;
        $pageAccessToken = $request->input('access_token');
        $message         = $request->input('message');
        $client          = new Client();
        $url             = $this->url . $id . '/comments?access_token=' . $pageAccessToken . '&message=' . $message;
        $res             = $client->request('POST', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody());
        dd($data);
    }

    public function postCommentToConversation($conversationId, Request $request) {
        $pageAccessToken = $request->input('access_token');
        $message         = $request->input('message');
        $client          = new Client();
        // $url             = $this->url . $conversationId . '/messages?access_token=' . $pageAccessToken . '&message=' . $message;
        $url = $this->url . $conversationId . '/messages';
        $res = $client->request('POST', $url, [
            'form_params' => [
                'message'        => $message,
                'access_token'   => $pageAccessToken,
                'attachment_url' => 'http://healthhub.co/wp-content/uploads/2014/02/Group-Slider.jpg',
            ],
        ]);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody());
        dd($data);
    }

    public function getCommentChild($parentCommentId) {
        $client = new Client();
        $url    = $this->url . $parentCommentId . '?access_token=' . $this->accessToken . '&fields=comment_count,comments{created_time,message,id,from,like_count}';
        $res    = $client->request('GET', $url);
        if ($res->getStatusCode() != 200) {
            dd('thất bại');
        }
        $data = json_decode($res->getBody());
        dd($data);
    }

    public function getContent() {
        $a = \App\Models\Users\User::all();
        return response()->json(
            $a
        );
    }

    public function getInbox() {
        return view('administrator.facebooks.fanpages.inbox-chat');
    }

}
