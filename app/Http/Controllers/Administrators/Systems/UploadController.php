<?php

namespace App\Http\Controllers\Administrators\Systems;

use App\Http\Controllers\Controller;
use App\Services\AuthService;
use App\Services\ResponseService;
use Illuminate\Http\Request;

class UploadController extends Controller {
    private $auth;

    private $company;

    private $response;

    private $request;

    public function __construct(AuthService $auth, ResponseService $response, Request $request) {
        $this->auth     = $auth;
        $this->response = $response;
        $this->request  = $request;
    }

    public function postUpload() {
        $file            = $this->request->image;
        $destinationPath = public_path() . IMAGENEWS;
        $filename        = time() . '_' . $file->getClientOriginalName();
        $uploadSuccess   = $file->move($destinationPath, $filename);
        if ($uploadSuccess) {
            $upload = url(IMAGENEWS . $filename);
            return $this->response->json(true, $upload, trans('messages.upload_success'));
        }
        return $this->response->json(false, '', trans('messages.upload_failed'));
    }
}