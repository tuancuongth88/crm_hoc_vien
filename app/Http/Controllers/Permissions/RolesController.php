<?php

namespace App\Http\Controllers\Permissions;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Permissions\RoleCreateRequest;
use App\Http\Requests\Permissions\RoleUpdateRequest;
use App\Models\Permissions\Permission;
use App\Models\Permissions\Role;
use App\Models\Users\User;
use App\Repositories\Permissions\RoleRepository;
use App\Validators\Permissions\RoleValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class RolesController.
 *
 * @package namespace App\Http\Controllers\Permissions;
 */
class RolesController extends Controller {
    /**
     * @var RoleRepository
     */
    protected $repository;

    /**
     * @var RoleValidator
     */
    protected $validator;

    /**
     * @var user
     */
    protected $user;
    /**
     * @var permission
     */
    protected $permission;

    /**
     * RolesController constructor.
     *
     * @param RoleRepository $repository
     * @param RoleValidator $validator
     */
    public function __construct(RoleRepository $repository, RoleValidator $validator, User $user, Permission $permission) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->user       = $user;
        $this->permission = $permission;
        $this->pathView   = 'administrator.permissions.role';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->pathView . '.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RoleCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(RoleCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input = $request->all();
            // $input['name'] = str_replace(' ', '', $input['name']);
            $input['name'] = strtolower(preg_replace('/\s+/', '', $input['name']));
            $role          = $this->repository->create($input);

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $role->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('role.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $role = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $role,
            ]);
        }

        return view($this->pathView . '.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = $this->repository->find($id);

        return view($this->pathView . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RoleUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(RoleUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $input = $request->all();
            // $input['name'] = str_replace(' ', '', $input['name']);
            $input['name'] = strtolower(preg_replace('/\s+/', '', $input['name']));
            $role          = $this->repository->update($input, $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $role->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('role.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->route('role.index')->with('message', trans('messages.delete_success'));
    }

    public function create() {
        return view($this->pathView . '.create');
    }

    public function getConfigRole() {
        $listRole       = $this->repository->all()->pluck('display_name', 'id')->toArray();
        $listPermission = $this->permission->all();
        return view($this->pathView . '.setup-permission', compact('listRole', 'listPermission'));
    }

    public function getConfigUser() {
        $listRole = $this->repository->all();
        $listUser = $this->user->all();
        return view($this->pathView . '.setup-user', compact('listRole', 'listUser'));
    }

    public function getPermissionByRole(Request $request) {
        $input = $request->all();
        if (isset($input['query']['role_id'])) {
            $role = $this->repository->find($input['query']['role_id']);
            if ($role) {
                $allPermission  = $this->permission->get();
                $listPermission = [];
                foreach ($role->perms as $permission) {
                    $listPermission[$permission->id] = $permission->display_name;
                }
                $data = [];
                foreach ($allPermission as $key => $value) {
                    $data[$value->id]['id']           = $value->id;
                    $data[$value->id]['name']         = $value->name;
                    $data[$value->id]['display_name'] = $value->display_name;
                    $data[$value->id]['description']  = $value->description;
                    $data[$value->id]['checked']      = (isset($listPermission[$value->id])) ? true : false;
                }
                return response()->json($data);
                // return view('administrator.append.list-permission', compact('listPermission', 'allPermission'));
            }
        }
        // $allPermission = $this->permission->pluck('display_name', 'id')->toArray();
        return null;
    }

    public function putPermissionIntoRole(RoleCreateRequest $request) {
        $inputAll = $request->all();
        $role     = $this->repository->find($inputAll['role_id']);
        if (!$role) {
            dd(trans('messages.data_is_empty'));
        }
        $arrPermission = [];
        if (isset($inputAll['permission'])) {
            foreach ($inputAll['permission'] as $key => $value) {
                if ($value == 'on') {
                    $arrPermission[] = $key;
                }
            }
        }
        $role->perms()->sync([]);
        $role->perms()->sync($arrPermission);
        return redirect()->route('role.index')->with('message', trans('messages.update_success'));
    }

    public function putRoleIntoUser(RoleCreateRequest $request) {
        $inputAll = $request->all();
        if ((int) $inputAll['role_id'] == ZERO) {
            dd(trans('messages.data_is_empty'));
        }
        $user = $this->user->findOrFail($inputAll['user_id']);
        $role = $this->repository->find($inputAll['role_id']);
        $user->roles()->sync([]);
        $user->attachRole($role);
        return redirect()->route('role.index')->with('message', trans('messages.update_success'));
    }

    public function postImport(Request $request) {

        if ($request->hasFile('import_file')) {
            \Excel::load($request->file('import_file')->getRealPath(), function ($reader) {
                $data = [];
                foreach ($reader->toArray() as $row) {
                    $data[] = $row;

                    $user  = $this->user->where('email', $row['email'])->first();
                    $model = $this->repository->model();
                    $role  = $model::where('name', $row['vai_tro'])->first();
                    if ($user) {
                        $user->roles()->sync([]);
                        $user->attachRole($role);
                    }
                }
            });

            return redirect()->route('role.index')->with('message', trans('messages.import_success'));
        }
        dd('Không tồn tại file import');
    }

    public function getConfigRoleUser(Request $request)
    {
        $input = $request->all();
        $mUsers = new User();
        if (isset($input['search_name'])) {
            $mUsers = $mUsers->where(function ($q) use ($input) {
                $q->where(User::FULLNAME, 'like', '%' . $input['search_name'] . '%');
                $q->orWhere(User::EMAIL, 'like', '%' . $input['search_name'] . '%');
            });
        }

        if (isset($input['search_role_id']) && (int)$input['search_role_id'] !== 0) {
            $mUsers = $mUsers->whereHas('roles', function ($q) use ($input) {
                $q->where('id', (int)$input['search_role_id']);
            });
        }

        $data = $mUsers->paginate(20);

        $listRole = $this->repository->pluck('display_name', 'id')->toArray();
        if (isset($input['page'])) {
            unset($input['page']);
        }
        return view($this->pathView . '.config-role-user', compact('listRole', 'data', 'input'));
    }

    public function postConfigRoleUser(Request $request)
    {
        $cf_role_u = $request->get('cf_role_u');
        if (isset($cf_role_u) && isset($cf_role_u['role_id']) && isset($cf_role_u['user_id'])) {
            $role_id = (int)$cf_role_u['role_id'];
            $arr_user_id = $cf_role_u['user_id'];
            foreach ($arr_user_id as $key => $value) {
                $user = $this->user->findOrFail((int)$value);
                $role = $this->repository->find($role_id);
                $user->roles()->sync([]);
                $user->attachRole($role);
            }
        }

        $data = $this->user->paginate(20);
        $listRole = $this->repository->pluck('display_name', 'id')->toArray();
        return view($this->pathView . '.config-role-user', compact('listRole', 'data'));
    }
}
