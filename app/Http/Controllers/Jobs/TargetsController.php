<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Jobs\TargetCreateRequest;
use App\Http\Requests\Jobs\TargetUpdateRequest;
use App\Models\Jobs\Task;
use App\Repositories\Jobs\TargetRepository;
use App\Validators\Jobs\TargetValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class TargetsController.
 *
 * @package namespace App\Http\Controllers\Jobs;
 */
class TargetsController extends Controller {
    /**
     * @var TargetRepository
     */
    protected $repository;

    /**
     * @var TargetValidator
     */
    protected $validator;

    /**
     * @var Task
     */
    protected $task;

    /**
     * TargetsController constructor.
     *
     * @param TargetRepository $repository
     * @param TargetValidator $validator
     */
    public function __construct(TargetRepository $repository, TargetValidator $validator, Task $task) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->task       = $task;
        $this->pathView   = 'administrator.jobs.target';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::denies('target.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }
        $task = $this->task->getTaskStatic();
        return view($this->pathView . '.index', compact('data', 'task'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TargetCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(TargetCreateRequest $request) {
        try {
            if (Gate::denies('target.create')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $target = $this->repository->create($request->all());

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $target->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('target.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (Gate::denies('target.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $target = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $target,
            ]);
        }

        return view($this->pathView . '.show', compact('target'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('target.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);
        // $task = $this->task->get()->pluck('title', 'id');
        $task = $this->task->getTaskStatic();
        return view($this->pathView . '.edit', compact('data', 'task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TargetUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(TargetUpdateRequest $request, $id) {
        try {
            if (Gate::denies('target.update')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $target = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $target->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('target.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('target.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('target.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        // $task = $this->task->get()->pluck('title', 'id');
        $task = $this->task->getTaskStatic();
        return view($this->pathView . '.create', compact('task'));
    }
}
