<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Jobs\TaskCategoryCreateRequest;
use App\Http\Requests\Jobs\TaskCategoryUpdateRequest;
use App\Repositories\Jobs\TaskCategoryRepository;
use App\Validators\Jobs\TaskCategoryValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class TaskCategoriesController.
 *
 * @package namespace App\Http\Controllers\Jobs;
 */
class TaskCategoriesController extends Controller {
    /**
     * @var TaskCategoryRepository
     */
    protected $repository;

    /**
     * @var TaskCategoryValidator
     */
    protected $validator;

    /**
     * TaskCategoriesController constructor.
     *
     * @param TaskCategoryRepository $repository
     * @param TaskCategoryValidator $validator
     */
    public function __construct(TaskCategoryRepository $repository, TaskCategoryValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->pathView   = 'administrator.jobs.task-category';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::denies('task-category.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->pathView . '.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TaskCategoryCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(TaskCategoryCreateRequest $request) {
        try {
            if (Gate::denies('task-category.create')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $taskCategory = $this->repository->create($request->all());

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $taskCategory->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('task-category.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        if (Gate::denies('task-category.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $taskCategory = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $taskCategory,
            ]);
        }

        return view($this->pathView . '.show', compact('taskCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('task-category.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);

        return view($this->pathView . '.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TaskCategoryUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(TaskCategoryUpdateRequest $request, $id) {
        try {
            if (Gate::denies('task-category.update')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $taskCategory = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $taskCategory->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('task-category.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('task-category.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->route('task-category.index')->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('task-category.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        return view($this->pathView . '.create');
    }
}
