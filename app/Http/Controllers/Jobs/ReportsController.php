<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Jobs\ReportCreateRequest;
use App\Http\Requests\Jobs\ReportUpdateRequest;
use App\Models\Customers\Customer;
use App\Models\Customers\Histories;
use App\Models\Customers\Level;
use App\Models\Customers\ProjectCustomer;
use App\Models\Jobs\Attachment;
use App\Models\Jobs\Plan;
use App\Models\Jobs\Report;
use App\Models\Jobs\StabilityTask;
use App\Models\Jobs\SuddenlyTask;
use App\Models\Jobs\Target;
use App\Models\Jobs\Task;
use App\Models\Jobs\TaskCategory;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectManager;
use App\Models\Users\User;
use App\Repositories\Jobs\ReportRepository;
use App\Validators\Jobs\ReportValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class ReportsController.
 *
 * @package namespace App\Http\Controllers\Jobs;
 */
class ReportsController extends Controller {
    /**
     * @var ReportRepository
     */
    protected $repository;

    /**
     * @var ReportValidator
     */
    protected $validator;

    protected $task;

    protected $target;

    /**
     * ReportsController constructor.
     *
     * @param ReportRepository $repository
     * @param ReportValidator $validator
     */
    public function __construct(ReportRepository $repository, ReportValidator $validator, Task $task, Target $target) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->task       = $task;
        $this->target     = $target;
        $this->pathView   = 'administrator.jobs.report';
        $this->pathViewTrial   = 'administrator.jobs.report-trial';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        if (Gate::denies('report.list')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        // $data    = $this->repository->findWhereIn('id', [1, 2, 3, 4, 5])->orderBy('id', 'desc')->paginate();
        $user    = new User();
        $arrUser = $user->getAllChildUser(\Auth::user()->id);
        array_push($arrUser, \Auth::user()->id);
        $listSale = User::whereIn('id', $arrUser)->pluck('email', 'id')->toArray();
        $model = $this->repository->model();
        if($request->user_id){
            $data  = $model::where('created_by', $request->user_id)->orderBy('id', 'desc')->paginate(20);
        }else{
            $data  = $model::whereIn('created_by', $arrUser)->orderBy('id', 'desc')->paginate(20);
        }
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->pathView . '.index', compact('data', 'listSale'));
    }

    public function indexTrialPeriod(Request $request)
    {
        if (!Auth::user()->isHanhChinh()) {
            if(Auth::user()->type != User::TYPE_TRIAL_PERIOD){
                abort(505, trans('messages.you_do_not_have_permission'));
            }
        }
        $input = $request->all();

        $user = new User();
        if(Auth::user()->type == User::TYPE_TRIAL_PERIOD){
            $arrUser = $user->where('id', Auth::user()->id);
        }else{
            $arrUser = $user->where(User::TYPE, User::TYPE_TRIAL_PERIOD);
        }

        if (isset($input['name'])) {
            $arrUser = $arrUser->where(User::FULLNAME, 'like', '%' . $input['name'] . '%');
        }
        if (isset($input['phone'])) {
            $arrUser = $arrUser->where(User::PHONE, 'like', '%' . $input['phone'] . '%');
        }
        if (isset($input['email'])) {
            $arrUser = $arrUser->where(User::EMAIL, 'like', '%' . $input['email'] . '%');
        }


        $data = $arrUser->paginate(50);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->pathViewTrial . '.index', compact('data','input'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ReportCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ReportCreateRequest $request) {

        try {
            if (Gate::denies('report.create')) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $model                     = $this->repository->model();
            $input                     = $request->all();
            $listAtachment             = json_decode($request->input('list_attachment'));
            $input[$model::CREATED_BY] = \Auth::user()->id;

            // check không cho tao 2 bao cao tren ngày
            $startDay = Carbon::now()->startOfDay();
            $endDay   = Carbon::now()->endOfDay();
            $listReport = Report::where('created_by', Auth::user()->id)->whereBetween('created_at', [$startDay, $endDay])->get();
            if($listReport->count() > 0){
                $response = [
                    'error'   => true,
                    'message' => trans('messages.only_one_report_in_day'),
                ];
                return  response()->json($response);
            }
            $report                    = $this->repository->create($input);
            if($listAtachment != null){
                if(count($listAtachment) > 0){
                    $model::saveRelateAttachment($listAtachment, $report);
                }
            }
            if (isset($input['info']) && count($input['info']) > ZERO) {
                $model::saveRelateInfoManager($input['info'], $report);
            }

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $report->toArray(),
            ];

            // if ($request->wantsJson()) {

            return response()->json($response);
            // }

            // return redirect()->route('report.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $report = $this->repository->find($id);
        if (Gate::allows('report.view', $report) || Gate::allows('report.approve')) {
            if (request()->wantsJson()) {

                return response()->json([
                    'data' => $report,
                ]);
            }
            $now = Carbon::now();
            $timestamp = strtotime($report->created_at);
            $month = date('m', $timestamp);
            $year = date('Y', $timestamp);
            $target = Plan::where(Plan::MONTH, $month)->where(Plan::YEAR, $year)->first();

            return view($this->pathView . '.show-new', compact('report', 'target'));
        }
        abort(505, trans('messages.you_do_not_have_permission'));
        // $taskStability = $this->task->get()->pluck('title', 'id');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $report = $this->repository->find($id);
        if (Gate::denies('report.update', $report)) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        // $taskStability = $this->task->getTaskStatic();
        // $category      = TaskCategory::all();
        return view($this->pathView . '.edit-new', compact('report'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ReportUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ReportUpdateRequest $request, $id) {
        try {
            $report = $this->repository->find($id);
            if (Gate::denies('report.update', $report)) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $model = $this->repository->model();
            $input = $request->all();

            $input[$model::NAME]       = $request->input($model::NAME);
            $input[$model::UPDATED_BY] = \Auth::user()->id;

            // chỉ cho sửa bao cáo trong ngày
            if (Carbon::now()->endOfDay()->getTimestamp() > Carbon::parse($report->created_at)->endOfDay()->getTimestamp()) {
                $response = [
                    'status'   => false,
                    'message' => trans('messages.edit_one_report_in_day'),
                ];
                return $response;
            }


            $report = $this->repository->update($input, $id);
            if (isset($input['info']) && count($input['info']) > ZERO) {
                $report->hasInfoManager()->delete();
                $model::saveRelateInfoManager($input['info'], $report);
            }
            $listAtachment = json_decode($request->input('list_attachment'));
            if($listAtachment != null){
                if (count($listAtachment) > 0) {
                    foreach ($listAtachment as $value) {
                        if (isset($input[$value->type])) {
                            $report->hasAttachment()->where('field', $value->type)->where('report_id', $id)->delete();
                        }
                    }
                    $model::saveRelateAttachment($listAtachment, $report);
                }
            }

            $response = [
                'status'  => true,
                'message' => trans('messages.update_success'),
                'data'    => $report->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return response()->json($response);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $obj = $this->repository->find($id);
        if (Gate::denies('report.delete', $obj)) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        $startDay = Carbon::now()->startOfDay();
        $endDay   = Carbon::now()->endOfDay();

        if (Gate::denies('report.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $plans = Plan::where(Plan::MONTH, Carbon::now()->month)->where(Plan::YEAR, Carbon::now()->year)->first();
        if(!$plans){
            return redirect()->back()->with('error', trans('messages.job_report_create_not_plans_in_month'))->withInput();
        }
        return view($this->pathView . '.create-new', compact('task', 'taskStability', 'category'));
    }

    public function postEvaluate($id, Request $request) {
        $report = $this->repository->find($id);
        if (!Gate::allows('report.approve', $report)) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $report->evaluate   = $request->input('evaluate');
        $report->is_approve = ($report->is_approve == ZERO) ? APPROVE : ZERO;
        $report->updated_by = \Auth::user()->id;
        $report->save();
        return redirect()->route('report.index')->with('message', trans('messages.update_success'));
        // return response()->json([
        //     'redirect' => route('report.index'),
        //     'message'  => trans('messages.update_success'),
        //     'status'   => true,
        // ]);
    }

    public function postApprove($id) {
        $report             = $this->repository->find($id);
        $report->is_approve = ($report->is_approve == ZERO) ? APPROVE : ZERO;
        $report->save();
        return response()->json([
            'redirect' => route('report.index'),
            'message'  => trans('messages.update_success'),
            'status'   => true,
        ]);

    }

    public function getDashboard(Request $request) {
        $arrUser = [];
        if (!$request->input('user_id')) {
            $user    = new User();
            $arrUser = $user->getAllChildUser(\Auth::user()->id);
            array_push($arrUser, \Auth::user()->id);
        } else {
            array_push($arrUser, $request->input('user_id'));
        }
        $totalProject           = Project::whereIn(Project::CREATED_BY, $arrUser)->count();
        $totalCustomer          = Customer::whereIn(Customer::CREATED_BY, $arrUser)->count();
        $totalCustomerLevel1          = Customer::whereIn(Customer::CREATED_BY, $arrUser)->where('level', 1)->count();
        $totalCustomerLevel2          = Customer::whereIn(Customer::CREATED_BY, $arrUser)->where('level', 2)->count();
        $totalCustomerLevel3          = Customer::whereIn(Customer::CREATED_BY, $arrUser)->where('level', 3)->count();

        $user    = new User();
        $listUser = User::whereIn('id', $arrUser)->with(['position'])->orderBy('id', 'asc')->simplePaginate(5);

        return view('administrator.dashboard', compact('totalProject', 'totalCustomer', 'totalCustomerLevel1', 'totalCustomerLevel2', 'totalCustomerLevel3', 'listUser'));

    }

    public function getReportCustomerByWeek(Request $request) {
        $arrUser = [];
        if ($request->input('user_id')) {
            array_push($arrUser, $request->input('user_id'));
        } else {
            $user    = new User();
            $arrUser = $user->getAllChildUser(\Auth::user()->id);
            array_push($arrUser, \Auth::user()->id);
        }

        $startDay = Carbon::now()->startOfDay();
        $endDay   = Carbon::now()->endOfWeek();

        $result = Customer::whereIn('created_by', $arrUser)
            ->whereBetween('created_at', [$startDay, $endDay])
            ->select(array(DB::Raw('count(*) as totalCustomer'), DB::Raw('DATE(created_at) day'), DB::Raw('count(IF(level=2,1,NULL)) customer_potential')))
            ->groupBy('day')
        // ->orderBy('created_at')
            ->get();
        $data = array();
        foreach ($result as $value) {
            $item['totalCustomer']      = $value->totalCustomer;
            $item['day']                = $value->day;
            $item['customer_potential'] = $value->customer_potential;
            $data[]                     = $item;
        }
        return response()->json($data);

    }

    public function getReport($durationType, Request $request) {
        $arrUser = [];
        if ($request->input('user_id')) {
            array_push($arrUser, $request->input('user_id'));
        } else {
            $user    = new User();
            $arrUser = $user->getAllChildUser(\Auth::user()->id);
            array_push($arrUser, \Auth::user()->id);
        }

        if ($durationType == 'day') {
            $startDay = Carbon::now()->startOfDay();
            $endDay   = Carbon::now()->endOfDay();
        }
        if ($durationType == 'month') {
            $startDay = Carbon::now()->startOfMonth();
            $endDay   = Carbon::now()->endOfMonth();
        }
        if ($durationType == 'week') {
            $startDay = Carbon::now()->startOfWeek();
            $endDay   = Carbon::now()->endOfWeek();
        }
        $listHistory  = Histories::whereBetween('created_at', [$startDay, $endDay])->whereIn('user_id', $arrUser)
            ->whereNotNull('channel')->pluck('channel');
        $history = 0;
        if (!empty($listHistory)) {
            $history = array_count_values($listHistory->toArray());
        }
        $report = Report::whereBetween('created_at', [$startDay, $endDay])->whereIn(Report::CREATED_BY, $arrUser)
            ->orderBy('created_at', 'desc');

        $plan = Plan::where(Plan::MONTH, Carbon::now()->month)->where(Plan::YEAR, Carbon::now()->year)->first();
        return view('administrator.append.report-new', compact('report', 'durationType', 'history', 'plan'));
    }

    public function getReportCustomerPotentialByProject(Request $request) {
        $arrUser = [];
        if ($request->input('user_id')){
            array_push($arrUser, $request->input('user_id'));
        } else {
            $user    = new User();
            $arrUser = $user->getAllChildUser(\Auth::user()->id);
            array_push($arrUser, \Auth::user()->id);
        }

        //cuongnt sua lai theo project manager
        $result = ProjectCustomer::join('projects', 'project_customer.project_id', '=', 'projects.id')
                    ->join('project_managers', 'projects.project_manager_id', '=', 'project_managers.id')
                    ->where('level', Customer::TYPE_CUSTOMER_NORMAL)
                    ->whereIn('project_customer.created_by', $arrUser)
                    ->select('project_managers.id', DB::raw('count(*) as total_customer'))->groupBy('project_managers.id')->get();
        $totalCustomer = ProjectCustomer::where('level', Customer::TYPE_CUSTOMER_NORMAL)
            ->whereIn('created_by', $arrUser)->count();
        $data = array();
        foreach ($result as $value) {
            $project = ProjectManager::find($value->id);
            $item['project_id']   = $value->id;
            $item['project_name'] = @$project->name;
            $item['percent']      = round(($value->total_customer / $totalCustomer) * 100);
            $data[]               = $item;
        }
        return response()->json($data);
    }

    public function exportReport($durationType, Request $request)
    {
        if (!Auth::user()->isHanhChinh()) {
            if (Auth::user()->type != User::TYPE_TRIAL_PERIOD) {
                abort(505, trans('messages.you_do_not_have_permission'));
            }
        }
        $arrUser = [];
        $input = $request->all();

        // $arrUser = User::where(User::TYPE, User::TYPE_TRIAL_PERIOD);
        if (Auth::user()->type == User::TYPE_TRIAL_PERIOD) {
            $arrUser = User::where('id', Auth::user()->id);
        } else {
            $arrUser = User::where(User::TYPE, User::TYPE_TRIAL_PERIOD);
        }

        if (isset($input['name'])) {
            $arrUser = $arrUser->where(User::FULLNAME, 'like', '%' . $input['name'] . '%');
        }
        if (isset($input['phone'])) {
            $arrUser = $arrUser->where(User::PHONE, 'like', '%' . $input['phone'] . '%');
        }
        if (isset($input['email'])) {
            $arrUser = $arrUser->where(User::EMAIL, 'like', '%' . $input['email'] . '%');
        }
        if (isset($input['month_report'])) {
            $dateMonthArray = explode('-', $input['month_report']);
            $month_report = $month = $dateMonthArray[0];
            $year = $dateMonthArray[1];
        } else {
            $year = Carbon::now()->year;
            $month_report = Carbon::now()->month;
        }
        $arrUser = $arrUser->pluck('id');
        if ($durationType == 'day') {
            $startDay = Carbon::now()->startOfDay();
            $endDay = Carbon::now()->endOfDay();
        }
        if ($durationType == 'month') {
            if (isset($input['month_report'])) {
                $startDay = Carbon::createFromDate($year, $month, 1)->startOfMonth();
                $endDay = Carbon::createFromDate($year, $month, 1)->endOfMonth();
            } else {
                $startDay = Carbon::now()->startOfMonth();
                $endDay = Carbon::now()->endOfMonth();
            }
        }
        if ($durationType == 'week') {
            $startDay = Carbon::now()->startOfWeek();
            $endDay = Carbon::now()->endOfWeek();
        }

        $data = [];

        $plan = Plan::where(Plan::MONTH, $month)->where(Plan::YEAR, $year)->first();
        if (!$plan) {
            return redirect()->route('report.index')->with('error', 'Lỗi báo cáo, thiếu chỉ tiêu');
        }

        foreach ($arrUser as $key => $value) {
            $item = new \stdClass();
            $item->listHistory = $listHistory = Histories::whereBetween('created_at', [$startDay, $endDay])->where('user_id', $value)
                ->whereNotNull('channel')->pluck('channel');
            $history = 0;
            if (!empty($listHistory)) {
                $history = array_count_values($listHistory->toArray());
            }
            $item->history = $history;
            $item->report = $report = Report::whereBetween('created_at', [$startDay, $endDay])->where(Report::CREATED_BY, $value)
                ->orderBy('created_at', 'desc');
            $User = User::where('id', $value)->first();
            $item->fullname = $User->fullname;

            $item->created_at = $User->created_at;
            $item->email = $User->email;
            $item->department_name = isset($User->department->name) ? $User->department->name : '';
            $item->position_name = isset($User->position->name) ? $User->position->name : '';

            //Công việc triển khai tại văn phòng công ty
            $item->total_call = $total_call = (int)isset($history[Histories::CAll_PHONE]) ? $history[Histories::CAll_PHONE] : 0;
            $item->conf_call_phone = $conf_call_phone = isset($plan->data['call_customer_potential'])
                ? ($plan->data['call_customer_potential']['number']) : 0;
            $item->rate_call = $rate_call = $this->funRate($total_call, $conf_call_phone);

            $item->total_post_website = $total_post_website = (int)$report->pluck('post_website')->sum();
            $item->conf_post_website = $conf_post_website = isset($plan->data['post_website'])
                ? ($plan->data['post_website']['number']) : 0;
            $item->rate_post_website = $rate_post_website = $this->funRate($total_post_website, $conf_post_website);

            $item->total_post_facebook = $total_post_facebook = (int)$report->pluck('post_facebook')->sum();
            $item->conf_post_facebook = $conf_post_facebook = isset($plan->data['post_facebook'])
                ? ($plan->data['post_facebook']['number']) : 0;
            $item->rate_post_facebook = $rate_post_facebook = $this->funRate($total_post_facebook, $conf_post_facebook);

            $item->total_post_news = $total_post_news = (int)$report->pluck('post_news')->sum();
//            $item->conf_post_news = $conf_post_news = isset($plan->data['post_news'])
//                ? ($plan->data['post_news']['number']) : 0;
//            $item->rate_post_news = $rate_post_news = $this->funRate($total_post_news, $conf_post_news);

            $item->total_post_forum = $total_post_forum = (int)$report->pluck('post_forum')->sum();
//            $item->conf_post_forum = $conf_post_forum = isset($plan->data['post_forum'])
//                ? ($plan->data['post_forum']['number']) : 0;
//            $item->rate_post_forum = $rate_post_forum = $this->funRate($total_post_forum, $conf_post_forum);

            $item->total_post_news_and_forum = $total_post_news_and_forum = $item->total_post_forum + $item->total_post_news;
            $item->conf_post_news_and_forum = $conf_post_news_and_forum = isset($plan->data['post_news_and_forum'])
                ? ($plan->data['post_news_and_forum']['number']) : 0;
            $item->rate_post_news_and_forum = $rate_post_news_and_forum = $this->funRate($total_post_news_and_forum, $conf_post_news_and_forum);

            $item->total_send_email = $total_send_email = (int)isset($history[Histories::SEND_EMAIL])
                ? $history[Histories::SEND_EMAIL] : 0;
            $item->conf_send_email = $conf_send_email = isset($plan->data['send_email'])
                ? ($plan->data['send_email']['number']) : 0;
            $item->rate_send_email = $rate_send_email = $this->funRate($total_send_email, $conf_send_email);

//            $item->total_info_market = $total_info_market = (int)$report->pluck('update_info_market')->sum();
//            $item->conf_update_info_market = $conf_update_info_market = isset($plan->data['update_info_market'])
//                ? ($plan->data['update_info_market']['number']) : 0;
//            $item->rate_info_market = $rate_info_market = $this->funRate($total_info_market, $conf_update_info_market);

            $item->total_info_project = $total_info_project = (int)$report->pluck('update_info_project')->sum();
            $item->conf_update_info_project = $conf_update_info_project = isset($plan->data['update_info_project'])
                ? ($plan->data['update_info_project']['number']) : 0;
            $item->rate_info_project = $rate_info_project = $this->funRate($total_info_project, $conf_update_info_project);

//            $item->total_social_network = $total_social_network = (int)isset($history[\App\Models\Customers\Histories::SOCIAL_NETWORK])
//                ? $history[\App\Models\Customers\Histories::SOCIAL_NETWORK] : 0;
//            $item->conf_social_network = $conf_social_network = isset($plan->data['total_social'])
//                ? ($plan->data['total_social']['number']) : 0;
//            $item->rate_social_network = $rate_social_network = $this->funRate($total_social_network, $conf_social_network);

            $total_rate_job_company = ($rate_call + $rate_post_website + $rate_post_facebook + $rate_post_news_and_forum
                + $rate_send_email  + $rate_info_project)/6;
            $item->rating_work_company_office = ($total_rate_job_company * 10) / 100;


            //Công việc triển khai ngoài hiện trường
            $item->total_flypaper_personal = $total_flypaper_personal = (int)$report->pluck('flypaper_personal')->sum();
            $item->conf_flypaper_personal = $conf_flypaper_personal = isset($plan->data['flypaper_personal'])
                ? ($plan->data['flypaper_personal']['number']) : 0;
            $item->rate_flypaper_personal = $rate_flypaper_personal = $this->funRate($total_flypaper_personal, $conf_flypaper_personal);

            $item->total_flypaper_group = $total_flypaper_group = (int)$report->pluck('flypaper_group')->sum();
            $item->conf_flypaper_group = $conf_flypaper_group = isset($plan->data['flypaper_group'])
                ? ($plan->data['flypaper_group']['number']) : 0;
            $item->rate_flypaper_group = $rate_flypaper_group = $this->funRate($total_flypaper_group, $conf_flypaper_group);

            $item->total_questionair = $total_questionair = (int)$report->pluck('questionair')->sum();
            $item->conf_questionair = $conf_questionair = isset($plan->data['questionair'])
                ? ($plan->data['questionair']['number']) : 0;
            $item->rate_questionair = $rate_questionair = $this->funRate($total_questionair, $conf_questionair);

            $item->total_project_in_hour = $total_project_in_hour = (int)$report->pluck('guard_project_in_hour')->sum();
//            $item->conf_guard_project_in_hour = $conf_guard_project_in_hour = isset($plan->data['guard_project_in_hour'])
//                ? ($plan->data['guard_project_in_hour']['number']) : 0;
//            $item->rate_project_in_hour = $rate_project_in_hour = $this->funRate($total_project_in_hour, $conf_guard_project_in_hour);

            $item->total_project_out_hour = $total_project_out_hour = (int)$report->pluck('guard_project_out_hour')->sum();
//            $item->conf_guard_project_out_hour = $conf_guard_project_out_hour = isset($plan->data['guard_project_out_hour'])
//                ? ($plan->data['guard_project_out_hour']['number']) : 0;
//            $item->rate_project_out_hour = $rate_project_out_hour = $this->funRate($total_project_out_hour, $conf_guard_project_out_hour);

            $item->total_guard_project = $total_guard_project = $total_project_in_hour + $total_project_out_hour;
            $item->conf_guard_project = $conf_guard_project = isset($plan->data['guard_project'])
                ? ($plan->data['guard_project']['number']) : 0;
            $item->rate_guard_project = $rate_guard_project = $this->funRate($total_guard_project, $conf_guard_project);

            $item->total_open_sale = $total_open_sale = (int)$report->pluck('open_sale')->sum();
            $item->conf_open_sale = $conf_open_sale = isset($plan->data['open_sale'])
                ? ($plan->data['open_sale']['number']) : 0;
            $item->rate_open_sale = $rate_open_sale = $this->funRate($total_open_sale, $conf_open_sale);

            $item->total_roadshow = $total_roadshow = (int)$report->pluck('roadshow')->sum();
            $item->conf_roadshow = $conf_roadshow = isset($plan->data['roadshow'])
                ? ($plan->data['roadshow']['number']) : 0;
            $item->rate_roadshow = $rate_roadshow = $this->funRate($total_roadshow, $conf_roadshow);

            $item->total_event = $total_event = (int)$report->pluck('event')->sum();
            $item->conf_event = $conf_event = isset($plan->data['event'])
                ? ($plan->data['event']['number']) : 0;
            $item->rate_event = $rate_event = $this->funRate($total_event, $conf_event);

            $total_rate_field_work = ($rate_flypaper_personal + $rate_flypaper_group + $rate_questionair
                + $rate_guard_project + $rate_open_sale + $rate_roadshow + $rate_event);

            $item->total_job_chart = $total_rate_search_customer = $total_rate_field_work / 7;
            $item->rating_complete_job_chart = ($total_rate_search_customer * 10) / 100;

            //Chỉ tiêu khách hàng
            $item->total_levels_1 = $total_levels_1 = Customer::where(
                function ($query) use ($startDay, $endDay) {
                    $query->whereBetween('created_at', [$startDay, $endDay]);
                    $query->orWhereBetween('updated_at', [$startDay, $endDay]);
                })
                ->where(Customer::CREATED_BY, $value)
                ->where(Customer::LEVEL, 1)->count();
            $item->conf_kpi_levels_1 = $conf_kpi_levels_1 = isset($plan->data['kpi_levels_1'])
                ? ($plan->data['kpi_levels_1']['number']) : KPI_LEVELS_1;
            $item->rate_levels_1 = $rate_levels_1 = $this->funRate($total_levels_1, $conf_kpi_levels_1);
            $item->total_levels_2 = $total_levels_2 = Customer::where(
                function ($query) use ($startDay, $endDay) {
                    $query->whereBetween('created_at', [$startDay, $endDay]);
                    $query->orWhereBetween('updated_at', [$startDay, $endDay]);
                })
                ->where(Customer::CREATED_BY, $value)
                ->where(Customer::LEVEL, 2)->count();
            $item->conf_kpi_levels_2 = $conf_kpi_levels_2 = isset($plan->data['kpi_levels_2'])
                ? ($plan->data['kpi_levels_2']['number']) : KPI_LEVELS_2;
            $item->rate_levels_2 = $rate_levels_2 = $this->funRate($total_levels_2, $conf_kpi_levels_2);
            $item->total_levels_3 = $total_levels_3 = Customer::where(
                function ($query) use ($startDay, $endDay) {
                    $query->whereBetween('created_at', [$startDay, $endDay]);
                    $query->orWhereBetween('updated_at', [$startDay, $endDay]);
                })
                ->where(Customer::CREATED_BY, $value)
                ->where(Customer::LEVEL, 3)->count();
            $item->conf_kpi_levels_3 = $conf_kpi_levels_3 = isset($plan->data['kpi_levels_3'])
                ? ($plan->data['kpi_levels_3']['number']) : KPI_LEVELS_3;
            $item->rate_levels_3 = $rate_levels_3 = 100;//$this->funRate($total_levels_3, $conf_kpi_levels_3);

//            $item->total_find_public = $total_find_public = (int)$report->pluck('find_public')->sum();
//            $item->conf_find_public = $conf_find_public = isset($plan->data['find_public'])
//                ? ($plan->data['find_public']['number']) : 0;
//            $item->rate_find_public = $rate_find_public = $this->funRate($total_find_public, $conf_find_public);
//
            $item->total_setup_appointment = $total_setup_appointment = (int)$report->pluck('setup_appointment')->sum();
            $item->conf_setup_appointment = $conf_setup_appointment = isset($plan->data['setup_appointment'])
                ? ($plan->data['setup_appointment']['number']) : 0;
            $item->rate_setup_appointment = $rate_setup_appointment = $this->funRate($total_setup_appointment, $conf_setup_appointment);

            $item->total_rate_search_customer = $total_rate_search_customer = ($rate_levels_1 + $rate_levels_2 + $rate_levels_3 + $rate_setup_appointment) / 4;
            $item->rating_complete_search_customer = ($total_rate_search_customer * 40) / 100;
//
//            $item->total_total = $total_total = (int)$report->pluck('total')->sum();
//            $item->conf_total = $conf_total = isset($plan->data['total'])
//                ? ($plan->data['total']['number']) : 0;
//            $item->rate_total = $rate_total = $this->funRate($total_total, $conf_total);


            //Hoạt động truyền thông nội bộ

            $item->total_like = $total_like = (int)$report->pluck('like')->sum();
            $item->conf_like = $conf_like = isset($plan->data['like'])
                ? ($plan->data['like']['number']) : 0;
            $item->rate_like = $rate_like = $this->funRate($total_like, $conf_like);

            $item->total_share = $total_share = (int)$report->pluck('share')->sum();
            $item->conf_share = $conf_share = isset($plan->data['share'])
                ? ($plan->data['share']['number']) : 0;
            $item->rate_share = $rate_share = $this->funRate($total_share, $conf_share);

            $item->total_comment = $total_comment = (int)$report->pluck('comment')->sum();
            $item->conf_comment = $conf_comment = isset($plan->data['comment'])
                ? ($plan->data['comment']['number']) : 0;
            $item->rate_comment = $rate_comment = $this->funRate($total_comment, $conf_comment);

            $item->total_rate_brand_work = $total_rate_brand_work = ($rate_like + $rate_share + $rate_comment) / 3;
            $item->rating_complete_brand_work = ($total_rate_brand_work * 10) / 100;
            //xxxxx

            $data[$key] = $item;

        }
        return view('administrator.jobs.exports.export', compact('data', 'plan', 'month_report'));

    }

    public function funRate($val_rate, $conf_rate)
    {
        $rate_math = 0;
        if ((int)$conf_rate == 0 || (int)$val_rate < 0) {
            return $rate_math;
        }

        $rate_math = ($val_rate * 100) / (int)$conf_rate;
        if ((int)$rate_math > 100) {
            $rate_math = 100;
        }
        return $rate_math;
    }

}
