<?php
namespace App\Http\Controllers\Jobs;

use App\Models\Jobs\Plan;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller as Controller;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\Jobs\PlanCreateRequest;
use App\Http\Requests\Jobs\PlanUpdateRequest;
use App\Repositories\Jobs\PlanRepository;
use App\Validators\Jobs\PlanValidator;


/**
 * Class PlansController.
 *
 * @package namespace App\Http\Controllers\Jobs;
 */
class PlansController extends Controller
{
    /**
     * @var PlanRepository
     */
    protected $repository;

    /**
     * @var PlanValidator
     */
    protected $validator;

    /**
     * PlansController constructor.
     *
     * @param PlanRepository $repository
     * @param PlanValidator $validator
     */
    public function __construct(PlanRepository $repository, PlanValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate(PAGINATE);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view('administrator.jobs.plan.index', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $workList = $this->repository::$modelList;
        return view('administrator.jobs.plan.form', compact('workList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  PlanCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(PlanCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input = $request->except('month_of_year');
            $monthOfYear = $request->month_of_year;
            $data = [];
            if(!empty($input['target_title'])){
                foreach ($input['target_title'] as $key => $value) {
                    $data[$value] = [
                        'number' => $input['target_number'][$key],
                        'time' => $input['time_unit'][$key],
                    ];
                }
            }

            $month = explode('-', $monthOfYear)[0];
            $year = explode('-',$monthOfYear)[1];
            $input['month'] = $month;
            $input['year'] = $year;
            $checkPlans = Plan::where('month', $month)->where('year', $year)->get();
            if($checkPlans->count() > 0){
                return redirect()->back()->withErrors("Mục tiêu này đã tồn tại trong tháng")->withInput();
            }
            $input['data'] = $data;

            $plan = $this->repository->create($input);

            $response = [
                'message' => trans('messages.success'),
                'data'    => $plan->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }
            return redirect()->route('target.index')->with('message', $response['message']);
        }
        catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $plan = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $plan,
            ]);
        }

        return view('plans.show', compact('plan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->repository->find($id);
        $data->month_of_year = $data->month.'-'.$data->year;
        $workList = $this->repository::$modelList;
        return view('administrator.jobs.plan.form', compact('workList', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PlanUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(PlanUpdateRequest $request, $id)
    {
        try {
            $plans = $this->repository->find($id);
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $monthOfYear = $request->month_of_year;
            $input = $request->all();
            $data = [];
            if(!empty($input['target_title'])){
                foreach ($input['target_title'] as $key => $value) {
                    $data[($value)] = [
                        'number' => $input['target_number'][$key],
                        'time' => $input['time_unit'][$key],
                    ];
                }
            }

            $input['data'] = $data;
            $month = explode('-', $monthOfYear)[0];
            $year = explode('-',$monthOfYear)[1];
            $input['month'] = $month;
            $input['year'] = $year;
            $checkPlans = Plan::where('month', $month)
                                ->where('year', $year)->first();

            if($checkPlans && $checkPlans->id != $plans->id){
                return redirect()->back()->withErrors("Không thể ghi đè 2 mục tiêu trong 1 tháng!")->withInput();
            }
            $plan = $this->repository->update($input, $id);
            $response = [
                'message' => trans('messages.success'),
                'data'    => $plan->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('target.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Plan deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Plan deleted.');
    }
}
