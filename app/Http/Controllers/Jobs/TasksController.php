<?php

namespace App\Http\Controllers\Jobs;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Jobs\TaskCreateRequest;
use App\Http\Requests\Jobs\TaskUpdateRequest;
use App\Models\Jobs\StabilityTask;
use App\Models\Jobs\SuddenlyTask;
use App\Models\Jobs\TaskCategory;
use App\Repositories\Jobs\TaskRepository;
use App\Validators\Jobs\TaskValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class TasksController.
 *
 * @package namespace App\Http\Controllers\Jobs;
 */
class TasksController extends Controller {
    /**
     * @var TaskRepository
     */
    protected $repository;

    /**
     * @var TaskValidator
     */
    protected $validator;

    protected $stabilityTask;

    protected $suddenlyTask;

    /**
     * TasksController constructor.
     *
     * @param TaskRepository $repository
     * @param TaskValidator $validator
     */
    public function __construct(TaskRepository $repository, TaskValidator $validator, StabilityTask $stabilityTask, SuddenlyTask $suddenlyTask) {
        $this->repository    = $repository;
        $this->validator     = $validator;
        $this->stabilityTask = $stabilityTask;
        $this->suddenlyTask  = $suddenlyTask;
        $this->pathView      = 'administrator.jobs.task';
        // $this->pathView = 'admin.jobs.tasks';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->pathView . '.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TaskCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(TaskCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $data = $this->repository->create($request->all());

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $data->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('task.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $task = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $task,
            ]);
        }

        return view($this->pathView . '.show', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data     = $this->repository->find($id);
        $category = TaskCategory::get()->toArray();
        return view($this->pathView . '.edit', compact('data', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TaskUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(TaskUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $task = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $task->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('task.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        $category = TaskCategory::all();
        return view($this->pathView . '.create', compact('category'));
    }

    public function postStabilityRate($id, Request $request) {
        if (Gate::denies('task.rate')) {
            return response()->json([
                'message' => trans('messages.you_do_not_have_permission'),
                'status'  => false,
            ]);
        }
        if ($request->input('rate')) {
            $task       = $this->stabilityTask->findOrFail($id);
            $task->rate = $request->input('rate');
            $task->save();
            return response()->json([
                'message' => trans('messages.update_success'),
                'status'  => true,
            ]);
        }
        return response()->json([
            'message' => trans('messages.update_failed'),
            'status'  => false,
        ]);
    }

    public function postSuddenlyRate($id, Request $request) {
        if (Gate::denies('task.rate')) {
            return response()->json([
                'message' => trans('messages.you_do_not_have_permission'),
                'status'  => false,
            ]);
        }
        if ($request->input('rate')) {
            $task       = $this->suddenlyTask->findOrFail($id);
            $task->rate = $request->input('rate');
            $task->save();
            return response()->json([
                'message' => trans('messages.update_success'),
                'status'  => true,
            ]);
        }
        return response()->json([
            'message' => trans('messages.update_failed'),
            'status'  => false,
        ]);
    }

}
