<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Customers\CustomerCreateRequest;
use App\Http\Requests\Customers\CustomerUpdateRequest;
use App\Models\Customers\Customer;
use App\Models\Customers\CustomerSharing;
use App\Models\Customers\CustomerSharingLog;
use App\Models\Customers\CustomerSharingUser;
use App\Models\Customers\Histories;
use App\Models\Customers\Level;
use App\Models\Customers\ProjectCustomer;
use App\Models\Customers\TypeCustomer;
use App\Models\Projects\District;
use App\Models\Projects\Project;
use App\Models\Projects\ProjectManager;
use App\Models\Projects\Province;
use App\Models\Users\User;
use App\Repositories\Customers\CustomerRepository;
use App\Repositories\Customers\ServiceCustomerRepository;
use App\Validators\Customers\CustomerValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_Border;
use PHPExcel_Worksheet;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Swift_Mailer;
use Swift_MailTransport;
use Swift_Message;
use \Auth;

/**
 * Class CustomersController.
 *
 * @package namespace App\Http\Controllers\Customers;
 */
class CustomersController extends Controller {
    /**
     * @var CustomerRepository
     */
    protected $repository;
    protected $serviceCustomer;

    /**
     * @var CustomerValidator
     */
    protected $validator;

    private $partView;
    private $partViewStore;

    private $user;

    private $model;

    /**
     * CustomersController constructor.
     *
     * @param CustomerRepository $repository
     * @param CustomerValidator $validator
     */
    public function __construct(CustomerRepository $repository, CustomerValidator $validator, ServiceCustomerRepository $serviceCustomer) {
        $this->repository      = $repository;
        $this->serviceCustomer = $serviceCustomer;
        $this->validator       = $validator;
        $this->partView        = 'administrator.customers.customer';
        $this->partViewStore   = 'administrator.customers.stores';
        $this->user            = new User();
        $this->model           = new Customer();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $input        = $request->all();
        $userId       = Auth::user()->id;
        $listUserId   = $this->user->getAllChildUser($userId);
        $listUserId[] = $userId;
        $listProject = Project::join('project_managers', 'projects.project_manager_id', '=', 'project_managers.id')->whereIn(Project::CREATED_BY, $listUserId)->pluck('project_managers.name', 'project_managers.id')->toArray();
        $listLevel = Level::where(Level::TYPE, Level::TYPE_CUSTOMER)->get()->pluck('name', 'id')->toArray();
        $listSale = User::whereIn('id', $listUserId)->pluck('email', 'id')->toArray();

        $customers    = $this->model->whereIn(Customer::CREATED_BY, $listUserId);

        if ($request['fullname']) {
            $customers->where('fullname', 'like', '%' . $request['fullname'] . '%');
        }
        if ($request['phone']) {
            $customers->where('phone', $request['phone']);
        }
        if ($request['level']) {
            $customers->where('level', $request['level']);
        }
        if ($request['project_id']) {
            $listCustomerId = ProjectCustomer::where('project_id', $request['project_id'])->pluck('customer_id')->toArray();
            $customers->whereIn('id', $listCustomerId);
        }
        if ($request['source_id']) {
            $customers->where('source_id', $request['source_id']);
        }
        if ($request['user_id']) {
            $customers->where('created_by', $request['user_id']);
        }

        if ($request['share_id']) {
            $customers->where('share_id', $request['share_id']);
        }

        if ($request['type']) {
            $customers->where('type', $request['type']);
        }
        $customers->whereNull(Customer::PARENT_ID);
        if (request()->wantsJson()) {
            $total = $customers->count();
            $customers = $customers->take($input['pagination']['perpage'])->skip($input['pagination']['perpage'] * ($input['pagination']['page'] - 1))
                        ->orderBy('id', 'DESC')->get();
            foreach ($customers as $key => $value) {
                $listIdProject                   = $value->projectCustomer->pluck('project_id')->toArray();
                $listProjectByCustomer           = Project::join('project_managers', 'projects.project_manager_id', '=', 'project_managers.id')->whereIn('projects.id', $listIdProject)->pluck('project_managers.name')->toArray();
                $customers[$key]['project_name'] = implode('<br>, ', $listProjectByCustomer);
                $customers[$key]['level']        = @Level::find($value['level'])->name;
                $customers[$key]['type']         = @$value->customerType->name;
                if ($value->created_by != $userId && !Auth::user()->can('is-admin-customer')) {
                    $customers[$key]['email']                   = hiddenText($value['email']);
                    $customers[$key]['phone']                   = hiddenText($value['phone']);
                    $customers[$key]['delete_edit_add_history'] = 0;
                } else {
                    $customers[$key]['delete_edit_add_history'] = 1;
                }
            }
            $response['data'] = $customers->toArray();
            $meta             = new \stdClass();
            $meta->page       = $input['pagination']['page'];
            $meta->perpage    = $input['pagination']['perpage'];
            $meta->total      = $total;
            $response['meta'] = $meta;
            $response['listProject'] = $listProject;
            $response['listSale'] = $listSale;
            return response()->json($response);
        }
        return view($this->partView . '.index', ['data' => $customers, 'listSale' => $listSale, 'listProject' => $listProject, 'listLevel' => $listLevel,'input' => $request->all()]);
    }

    public function indexStores(Request $request) {
        $input        = $request->all();
        $listCusType = TypeCustomer::orderBy('name')->pluck('name', 'id')->toArray();
        $data_store  = $this->model->whereNull('created_by');

        if (isset($request['search'])) {
            $data_store->where('fullname', 'like', '%' . $request['search'] . '%');
        }
        if (isset($request['type'])) {
            $data_store->where('type', $request['type']);
        }

        if (request()->wantsJson()) {
            $total = $data_store->count();
            $data_store = $data_store->take($input['pagination']['perpage'])
                ->skip($input['pagination']['perpage'] * ($input['pagination']['page'] - 1))
                ->orderBy('id', 'DESC')->get();
            $response['data'] = $data_store->toArray();
            $meta             = new \stdClass();
            $meta->page       = $input['pagination']['page'];
            $meta->perpage    = $input['pagination']['perpage'];
            $meta->total      = $total;
            $response['meta'] = $meta;
            return response()->json($response);
        }
        return view($this->partViewStore . '.index',
            [
                'data'        => $data_store,
                'listCusType' => $listCusType,
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CustomerCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CustomerCreateRequest $request) {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $input   = $request->except('level', 'project_id', 'director');
            $project = $request->only('level', 'project_id');
            if ($request->hasFile('avatar')) {
                $upload = uploadFile($input->avatar, IMAGE_CUSTOMER);
                if ($upload) {
                    $input['avatar'] = $upload;
                }
            }
            $input['fullname'] = $input['last_name'] . ' ' . $input['first_name'];
            if (isset($input['ngay_cap_cmt'])) {
                $input['ngay_cap_cmt'] = date('Y-m-d', strtotime($input['ngay_cap_cmt']));
            }

            $input['created_by']          = \Auth::user()->id;
            $input['user_id']             = \Auth::user()->id;
            $input['level']               = $project['level'];
            $finance                      = explode(';', $input['finance']);
            $input[Customer::FINANCE_MIN] = $finance[ZERO];
            $input[Customer::FINANCE_MAX] = $finance[ONE];
            $info                         = ['nhu_cau' => $input['info_hobby'],
                'so_thich'                                 => $input['info_need']];

            $input[Customer::INFO] = $info;

            $customer = $this->repository->create($input);

            if (isset($project['project_id'])) {
                foreach ($project['project_id'] as $value) {
                    $item['project_id']  = $value;
                    $item['customer_id'] = $customer->id;
                    $item['level']       = $project['level'];
                    $item['created_by']  = \Auth::user()->id;
                    ProjectCustomer::create($item);
                }
            }

            if (isset($request->director)) {
                foreach ($request->director as $value) {
                    if ($value['email_child'] && $value['phone_child'] && $value['last_name_child'] && $value['first_name_child']) {
                        $item[Customer::PARENT_ID] = $customer->id;
                        if (isset($value['relation'])) {
                            $item[Customer::RELATION] = $value['relation'];
                        }
                        $item[Customer::PHONE]      = $value['phone_child'];
                        $item[Customer::LAST_NAME]  = $value['last_name_child'];
                        $item[Customer::FIRST_NAME] = $value['first_name_child'];
                        $item[Customer::FULLNAME]   = $value['last_name_child'] . ' ' . $value['first_name_child'];
                        $item[Customer::EMAIL]      = $value['email_child'];
                        $item[Customer::CMT]        = $value['cmt_child'];
                        if (isset($value['birthday_child'])) {
                            $item[Customer::BIRTHDAY] = \Carbon\Carbon::createFromFormat('d-m-Y', $value['birthday_child']);
                        }

                        Customer::create($item);
                    }
                }
            }

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $customer->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }

            return redirect()->route('list.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $customer = $this->repository->find($id);

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $customer,
            ]);
        }

        return view('customers.show', compact('customer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function edit($id) {
        $data        = $this->repository->find($id);
        $listCity    = DB::table('provinces')->pluck('name', 'city_id')->toArray();
        $levels      = Level::where('type', Level::TYPE_CUSTOMER)->pluck('name', 'id')->toArray();
        $listProject = Project::join('project_managers', 'projects.project_manager_id', '=', 'project_managers.id')
            ->where('projects.created_by', Auth::user()->id)->pluck('project_managers.name', 'project_managers.id')->toArray();
        $sourceCustomer    = Customer::$sourceCustomer;
        $listDistrict      = DB::table('districts')->where('city_id', $data->city_id)->pluck('name', 'id')->toArray();
        $projectByCustomer = ProjectCustomer::where('customer_id', $id);
        $arrProject        = ProjectCustomer::where('customer_id', $id)->pluck('project_id')->toArray();
        return view($this->partView . '.edit', compact('data', 'listCity', 'levels', 'listProject', 'sourceCustomer', 'listDistrict', 'projectByCustomer', 'arrProject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CustomerUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CustomerUpdateRequest $request, $id) {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $input               = $request->except('level', 'project_id');
            $project             = $request->only('level', 'project_id');
            $input['updated_by'] = \Auth::user()->id;
            $input['level']      = $project['level'];
            $projectCustomer     = ProjectCustomer::where('customer_id', $id)->delete();

            ProjectCustomer::where('customer_id', $id)->delete();
            if (isset($project['project_id'])) {
                foreach ($project['project_id'] as $value) {
                    $item['project_id']  = $value;
                    $item['customer_id'] = $id;
                    $item['level']       = $project['level'];
                    $item['created_by']  = \Auth::user()->id;
                    ProjectCustomer::create($item);
                }
            }
            $finance                      = explode(';', $input['finance']);
            $input[Customer::FINANCE_MIN] = $finance[ZERO];
            $input[Customer::FINANCE_MAX] = $finance[ONE];
            $info                         = ['nhu_cau' => $input['info_hobby'],
                'so_thich'                                 => $input['info_need']];

            $input[Customer::INFO] = $info;
            $customer              = $this->repository->update($input, $id);
            Customer::where(Customer::PARENT_ID, $id)->forceDelete();

            if (isset($request->director)) {
                foreach ($request->director as $value) {
                    if ($value['phone_child'] && $value['last_name_child'] && $value['first_name_child'] && $value['email_child']) {
                        $item[Customer::PARENT_ID] = $id;
                        if (isset($value['relation'])) {
                            $item[Customer::RELATION] = $value['relation'];
                        }
                        $item[Customer::PHONE]      = $value['phone_child'];
                        $item[Customer::LAST_NAME]  = $value['last_name_child'];
                        $item[Customer::FIRST_NAME] = $value['first_name_child'];
                        $item[Customer::FULLNAME]   = $value['last_name_child'] . ' ' . $value['first_name_child'];
                        $item[Customer::EMAIL]      = $value['email_child'];
                        $item[Customer::CMT]        = $value['cmt_child'];
                        if (isset($value['birthday_child'])) {
                            $item[Customer::BIRTHDAY] = \Carbon\Carbon::createFromFormat('d-m-Y', $value['birthday_child']);
                        }

                        Customer::create($item);
                    }

                }
            }

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $customer->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return redirect()->route('list.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }
            return redirect()->route('list.index')->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $deleted = $this->repository->delete($id);
        $history = Histories::where('customer_id', $id)->delete();
        if (request()->wantsJson()) {
            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'Customer deleted.');
    }

    public function create() {
        $listCity    = DB::table('provinces')->pluck('name', 'city_id')->toArray();
        $levels      = Level::where('type', Level::TYPE_CUSTOMER)->pluck('name', 'id')->toArray();
        $listProject = Project::join('project_managers', 'projects.project_manager_id', '=', 'project_managers.id')
            ->where('projects.created_by', Auth::user()->id)->pluck('project_managers.name', 'project_managers.id')->toArray();
        $sourceCustomer = Customer::$sourceCustomer;
        return view($this->partView . '.add', compact('listCity', 'levels', 'listProject', 'sourceCustomer'));
    }

    public function setCustomerToSale() {

    }

    public function getHistoryToId($id) {

        $data      = Histories::where('customer_id', $id)->orderBy('id', 'desc')->get();
        $histories = array();
        $userId    = Auth::user()->id;
        foreach ($data as $key => $value) {
            $result['id']             = $value->id;
            $result['sale']           = $value->agent->fullname;
            $result['fullname']       = $value->customer->fullname;
            $result['phone']          = $value->phone;
            $result['channel']        = isset($value->channel) ? Histories::$listChannels[$value->channel] : '';
            $result['time_call_back'] = isset($value->time_call_back) ? date('d-m-Y H:i', strtotime($value->time_call_back)) : '';
            $result['note']           = $value->note;
            $result['created_at']     = date('d-m-Y H:i', strtotime($value->created_at));
            if ($userId == $value->user_id) {
                $result['delete_edit_add_history'] = 1;
            } else {
                $result['delete_edit_add_history'] = 0;
                $result['phone']                   = hiddenText($value->phone);
            }
            $histories[] = $result;
        }
        return response()->json([
            'data' => $histories,
        ]);
    }

    public function getDistrictOnCity($id) {
        $listCity = DB::table('districts')->where('city_id', $id)->pluck('name', 'id');
        return response()->json([
            'data' => $listCity,
        ]);
    }

    public function listTimeCallBackCustomerOnday(Request $request) {
        $arrUser = [];
        if (!$request->input('user_id')) {
            $user    = new User();
            $arrUser = $user->getAllChildUser(\Auth::user()->id);
            array_push($arrUser, \Auth::user()->id);
        } else {
            array_push($arrUser, $request->input('user_id'));
        }

        $startDay = Carbon::now()->startOfDay();
        $endDay   = Carbon::now()->endOfDay();
        $result   = Histories::whereIn(Histories::USER_ID, $arrUser)
            ->whereBetween(Histories::TIME_CALL_BACK, [$startDay, $endDay])
            ->orderBy('id', 'desc')->limit(10)->get();
        foreach ($result as $key => $value) {
            $result[$key]['time_call'] = $value['time_call_back']->format('H:i:s');
            $result[$key]['fullname']  = $value->customer->fullname;
        }
        return response()->json([
            'data' => $result->toArray(),
        ]);
    }

    public function historyCustomer() {

        $userId       = Auth::user()->id;
        $listCustomer = Customer::where(Customer::CREATED_BY, $userId)->pluck(Customer::FULLNAME, 'id')->toArray();

        return view($this->partView . '.historyCus', compact('listCustomer'));
    }

    public function getHistoryCustomer($id) {
        $customer            = Customer::find($id);
        $listHistory         = Histories::where(Histories::CUSTOMER_ID, $id)->get();
        $data['customer']    = $customer;
        $data['listHistore'] = $listHistory;

        return response()->json([
            'data' => $data,
        ]);
    }

    public function showHistoryCustomer(Request $request) {
        $histories         = new Histories();
        $channel_histories = $request->channel;
        if ($channel_histories = '') {
            $histories->channel = null;
        } else {
            $histories->channel = '1';
        }
        $histories->phone          = $request->phone;
        $histories->time_call_back = $request->time_call_back;
        $histories->note           = $request->note;
        $histories->customer_id    = $request->customer_id;
        $histories->user_id        = $request->employer_id;
        $histories->save();

        $CustomerCare                  = Customer::find($request['customer_id']);
        $CustomerCare->history_care_at = \Carbon\Carbon::now();
        $CustomerCare->status          = $request['status'];
        $CustomerCare->save();
        $data[] = array(
            'phone'          => $histories->phone,
            'time_call_back' => $histories->time_call_back,
            'note'           => $histories->note,
            'channel'        => $request->channel,
            'employer_name'  => $request->employer_name,
            'employer_id'    => $histories->user_id,
        );
        return response()->json([
            'data' => $data,
        ]);
    }

    public function getImportForm() {
        $userId       = Auth::user()->id;
        $listUserId   = $this->user->getAllChildUser($userId);
        $listUserId[] = $userId;
        $listUser     = User::whereIn('id', $listUserId)->pluck('email', 'id')->toArray();
        return view('administrator.customers.customer.import', compact('listUser'));
    }

    public function getImportStoreForm() {
        return view($this->partViewStore . '.import');
    }

    public function postImport(Request $request) {
        $sale = User::find($request->input('sale_id'));
        if (!$sale) {
            return redirect()->route('list.index')->with('message', 'Bạn chưa chọn nhân viên');
        }
        $project_id = $request->project_id;
        // if (!$project) {
        //     return redirect()->route('list.index')->with('message', 'Bạn chưa chọn dự án');
        // }
        $listFail = [];
        if ($request->hasFile('import_file')) {
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) use ($sale, $project_id, &$listFail) {
                $data = [];
                foreach ($reader->toArray() as $sheet) {
                    foreach ($sheet as $key => $row) {
                        if (!isset($row['tinhthanh_pho'])) {
                            continue;
                        }
                        if (!isset($row['dien_thoai'])) {
                            continue;
                        }

                        $check = Customer::where('phone', $row['dien_thoai'])->where('created_by', Auth::user()->id)->first();
                        if ($check) {
                            $listFail[] = 'Đã tồn tại số điện thoại: ' . $row['dien_thoai'];
                            continue;
                        }

                        $city = \App\Models\Projects\Province::where('name', 'like', '%' . $row['tinhthanh_pho'] . '%')
                            ->orderBy('id', 'desc')
                            ->first();
                        if (!$city) {
                            $listFail[] = 'Không tồn tại tỉnh thành: ' . $row['tinhthanh_pho'] . '<br>';
                            continue;
                        }
                        $district = \App\Models\Projects\District::where('name', 'like', '%' . $row['quanhuyen'] . '%')
                            ->orderBy('id', 'desc')
                            ->first();
                        if (!$district) {
                            $listFail[] = 'Không tồn tại quận huyện: ' . $row['quanhuyen'];
                            continue;
                        }
                        $type = Level::where('name', 'like', '%' . $row['phan_loai_kh'] . '%')
                            ->orderBy('id', 'desc')
                            ->first();
                        if (!$type) {
                            $listFail[] = 'Không tồn tại loại khách hàng: ' . $row['phan_loai_kh'];
                        }
                        $data[$key]['fullname']    = $row['ho'] . ' ' . $row['ten'];
                        $data[$key]['city_id']     = $city->id;
                        $data[$key]['district_id'] = $district->id;
                        $data[$key]['last_name']   = $row['ho'];
                        $data[$key]['first_name']  = $row['ten'];
                        $data[$key]['phone']       = $row['dien_thoai'];
                        if (isset($row['quan_he'])) {
                            $relation               = array_search($row['quan_he'], Customer::$listRelation);
                            $data[$key]['relation'] = $relation;
                        }
                        $data[$key]['user_id']          = $sale->id;
                        $data[$key]['created_by']       = $sale->id;
                        $data[$key]['info']['nhu_cau']  = $row['nhu_cau'];
                        $data[$key]['info']['so_thich'] = $row['so_thich'];
                        $info                           = ['nhu_cau' => $row['nhu_cau'],
                            'so_thich'                                   => $row['so_thich']];
                        $data[$key]['info']                = $info;
                        $data[$key]['cmt']                 = $row['chung_minh_thu'];
                        $data[$key]['noi_cap_cmt']         = $row['noi_cap'];
                        $data[$key]['email']               = $row['email_kh'];
                        $data[$key]['address']             = $row['dia_chi'];
                        $data[$key][Customer::FINANCE_MAX] = $row['khoang_tai_chinh'];
                        $data[$key][Customer::CHANNEL]     = $row['kenh_marketing'];
                        if (isset($row['nguon_khach_tu_kenh_nao'])) {
                            $source_id = array_search($row['nguon_khach_tu_kenh_nao'], Customer::$sourceCustomer);
                            if ($source_id > 0) {
                                $data[$key][Customer::SOURCE_ID] = $source_id;
                            }
                        }
                        if (isset($row['nguoi_than'])) {
                            $customerParent = Customer::where('phone', $row['nguoi_than'])->first();
                            if ($customerParent) {
                                $data[$key]['parent'] = ($customerParent) ? $customerParent->id : 0;
                            }
                        }
                        $data[$key]['level'] = ($type) ? $type->id : Customer::TYPE_CUSTOMER_NORMAL;
                        $data[$key]['sex']   = (strtolower($row['gioi_tinh']) == 'nam') ? Customer::MALE : Customer::FEMALE;

                        if (validateDate($row['ngay_sinh'], 'd/m/Y')) {
                            $data[$key]['birthday'] = \Carbon\Carbon::createFromFormat('d/m/Y', $row['ngay_sinh']);
                        } else {
                            $data[$key]['birthday'] = null;
                        }

                        if (validateDate($row['ngay_cap'], 'd/m/Y')) {
                            $data[$key][Customer::NGAY_CAP_CMT] = \Carbon\Carbon::createFromFormat('d/m/Y', $row['ngay_cap']);
                        } else {
                            $data[$key][Customer::NGAY_CAP_CMT] = null;
                        }

                        $objCustomer = Customer::create($data[$key]);
                        if ($project_id && isset($row['quan_he']) == false) {
                            $item['project_id']  = $project_id;
                            $item['customer_id'] = $objCustomer->id;
                            $item['level']       = ($type) ? $type->id : Customer::TYPE_CUSTOMER_NORMAL;
                            $item['created_by']  = $sale->id;
                            ProjectCustomer::create($item);
                        }
                    }
                }
                return redirect()->route('list.index')->with('message', trans('messages.import_success'))->with('list-fail', $listFail);
            });

            return redirect()->route('list.index')->with('message', trans('messages.import_success'));
        }
        return redirect()->route('list.index')->with('message', 'Không tồn tại file import');
    }

    public function getProjectBySaleId($id) {
        $listProject = Project::where(Project::CREATED_BY, $id)->pluck('name', 'id')->toArray();
        return response()->json([
            'data' => $listProject,
        ]);
    }

    public function getShareCustomerByLeader() {
        return $this->serviceCustomer->getShareCustomerByLeader();
    }

    public function postShareCustomerByLeader() {
        return $this->serviceCustomer->postShareCustomerByLeader();
    }

    public function postImportStore(Request $request) {
        $listFail = [];
        if ($request->hasFile('import_file')) {

            $validator = Validator::make($request->all(), [
                //or this
                'import_file' => 'required|max:50000|mimetypes:application/csv,application/excel,' .
                'application/vnd.ms-excel, application/vnd.msexcel,' .
                'text/csv, text/anytext, text/plain, text/x-c,' .
                'text/comma-separated-values,' .
                'inode/x-empty,' .
                'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            ]);
            if ($validator->fails()) {
                $listFail[] = 'File import phải là file Excel';
                return redirect()->back()->with('error', trans('Lỗi import dữ liệu'))
                    ->with('list-fail', $listFail);
            }

            $check_success = false;
            Excel::load($request->file('import_file')->getRealPath(), function ($reader) use (&$listFail, &$check_success) {
                $data = [];
                if (!isset($reader->toArray()[0][0])) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }
                $row_check = $reader->toArray()[0][0];
                if (!array_key_exists('ho_ten_khach_hang', $row_check) ||
                    !array_key_exists('so_dien_thoai', $row_check) ||
                    !array_key_exists('email', $row_check) ||
                    !array_key_exists('dia_chi_lien_he', $row_check) ||
                    !array_key_exists('loai_du_lieu', $row_check)
                ) {
                    $listFail[] = 'File import không đúng mẫu';
                    return false;
                }
                foreach ($reader->toArray() as $sheet) {
                    foreach ($sheet as $key => $row) {
                        if (!isset($row['so_dien_thoai'])) {
                            continue;
                        }
                        $check = Customer::where('phone', $row['so_dien_thoai'])->first();
                        if ($check) {
                            $listFail[] = 'Đã tồn tại số điện thoại: ' . $row['so_dien_thoai'];
                            continue;
                        }

                        if (isset($row['tinh_thanh_pho'])) {
                            $city = \App\Models\Projects\Province::where('name', 'like', '%' . $row['tinh_thanh_pho'] . '%')
                                ->orderBy('id', 'desc')
                                ->first();
                        }

                        if (isset($row['quan_huyen'])) {
                            $district = \App\Models\Projects\District::where('name', 'like', '%' . $row['quan_huyen'] . '%')
                                ->orderBy('id', 'desc')
                                ->first();
                        }
                        if (isset($row['loai_du_lieu'])) {
                            $type_cus = TypeCustomer::where('name', 'like', '%' . $row['loai_du_lieu'] . '%')
                                ->orderBy('id', 'desc')
                                ->first();
                        }

                        $city_id     = isset($city->id) ? $city->id : 0;
                        $district_id = isset($district->id) ? $district->id : 0;
                        $type_cus_id = isset($type_cus->id) ? $type_cus->id : 0;

                        $full_name     = trim($row['ho_ten_khach_hang']);
                        $arr_full_name = explode(' ', $full_name);
                        $last_name     = implode(' ', array_slice($arr_full_name, 0, count($arr_full_name) - 1));
                        $first_name    = end($arr_full_name);

                        $data[$key][Customer::FULLNAME]   = $full_name;
                        $data[$key][Customer::LAST_NAME]  = $last_name;
                        $data[$key][Customer::FIRST_NAME] = $first_name;
                        $data[$key][Customer::CITY_ID]    = $city_id;

                        $data[$key][Customer::DISTRICT_ID] = $district_id;
                        $data[$key][Customer::PHONE]       = $row['so_dien_thoai'];

                        $data[$key][Customer::EMAIL]   = $row['email'];
                        $data[$key][Customer::ADDRESS] = $row['dia_chi_lien_he'];
                        $data[$key][Customer::TYPE]    = $type_cus_id;
                        $data[$key][Customer::COMMENT] = $row['ghi_chu'];
                        $objCustomer                   = Customer::create($data[$key]);
                    }

                }
                $check_success = true;
                return redirect()->route('customer_stores.index')->with('message', trans('messages.import_success'))
                    ->with('list-fail', $listFail);
            });
            if (count($listFail) && !$check_success) {
                return redirect()->back()->with('error', trans('Lỗi import'))->with('list-fail', $listFail);
            } else {
                return redirect()->route('customer_stores.index')->with('message', trans('messages.import_success'));
            }
        }
        return redirect()->back()->with('error', trans('Không tồn tại file import'));
    }

    public function downloadFileImport() {

        $arr_province = Province::all(['name'])->toArray();
        $arr_district = District::all(['name'])->toArray();
        //$arr_project = ProjectManager::all(['name'])->toArray();
        $arr_customer_type = TypeCustomer::all(['name'])->sortBy('name')->toArray();

        $cus_store_default = [
            'ho_ten_khach_hang' => 'Nguyễn Văn A',
            'so_dien_thoai'     => '0982633825',
            'email'             => 'nguyenvana@gmail.com',
            'dia_chi_lien_he'   => 'Đống Đa, Hà Nội...',
            'loai_du_lieu'      => "dự án D'cap",
            'ghi_chu'           => "đã sự dụng chạy thử",
            'tinh_thanh_pho'    => 'Hà Nội',
            'quan_huyen'        => 'Đống Đa',
        ];

        Excel::create('file_import_kh_kho', function ($excel) use ($cus_store_default, $arr_district, $arr_province, $arr_customer_type) {
            $excel->sheet('danh_sach', function ($sheet) use ($cus_store_default, $arr_district, $arr_province, $arr_customer_type) {
                $count_province      = count($arr_province) + 1;
                $count_district      = count($arr_district) + 1;
                $count_customer_type = count($arr_customer_type) + 1;

                $sheet->fromArray($cus_store_default, null, 'A2', true);
                $sheet->row(1, array(
                    'Họ tên khách hàng', 'Số điện thoại', 'Email', 'Đia chỉ liên hệ', 'Loại dữ liệu',
                    'Ghi chú', 'Tỉnh thành phố', 'Quận huyện',
                ));

                $this->laravelExcel($sheet, 1);

                // Freeze first row
                $sheet->freezeFirstRow();

                $sheet->cell('A1:H1', function ($cell) {

                    // Set font
                    $cell->setFont(array(
                        'name' => 'Times New Roman',
                        'size' => '14',
                        'bold' => true,
                    ));

                });

                $style = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    ),
                    'borders'   => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                        'outline'    => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                        ),
                    ),
                );

                $sheet->getStyle('A1:H1')->applyFromArray($style);

                $this->setCellFormula1($sheet, 'E', 500, 'type!$A$2:$A$' . $count_customer_type);
                $this->setCellFormula1($sheet, 'G', 500, 'province!$A$2:$A$' . $count_province);
                $this->setCellFormula1($sheet, 'H', 500, 'district!$A$2:$A$' . $count_district);

            });

            $excel->sheet('district', function ($sheet) use ($arr_district) {

                $sheet->fromArray($arr_district, null, 'A1', true);
                $sheet->getProtection()->setSheet(true);
                $sheet->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);

            });
            $excel->sheet('province', function ($sheet) use ($arr_province) {

                $sheet->fromArray($arr_province, null, 'A1', true);
                $sheet->getProtection()->setSheet(true);
                $sheet->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);

            });
            $excel->sheet('type', function ($sheet) use ($arr_customer_type) {

                $sheet->fromArray($arr_customer_type, null, 'A1', true);
                $sheet->getProtection()->setSheet(true);
                $sheet->setSheetState(PHPExcel_Worksheet::SHEETSTATE_VERYHIDDEN);

            });

        })->download('xlsx');

    }

    /**
     * @param null $sheet
     * @param null $intRowNumber
     */

    public function laravelExcel($sheet = null, $intRowNumber = null) {
        $arrColumn = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];
        try {
            foreach ($arrColumn as $key => $value) {
                $sheet->setSize($value . $intRowNumber, 30, 30);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    //$cellFormula1 vd:  'project!$B$2:$A$2'
    public function setCellFormula1($sheet, $cellName, $numberCell, $cellFormula1) {
        for ($i = 1; $i <= $numberCell; $i++) {
            $objValidation = $sheet->getCell($cellName . $i)->getDataValidation();
            $objValidation->setType(\PHPExcel_Cell_DataValidation::TYPE_LIST);
            $objValidation->setErrorStyle(\PHPExcel_Cell_DataValidation::STYLE_INFORMATION);
            $objValidation->setAllowBlank(false);
            $objValidation->setShowInputMessage(true);
            $objValidation->setShowErrorMessage(true);
            $objValidation->setShowDropDown(true);
            $objValidation->setErrorTitle('Input error');
            $objValidation->setError('Value is not in list.');
            $objValidation->setPromptTitle('Pick from list');
            $objValidation->setPrompt('Please pick a value from the drop-down list.');
            $objValidation->setFormula1($cellFormula1); //note this!
        }
    }

    public function getTotalCustomerByShare($id) {
        return $this->serviceCustomer->getTotalCustomerByShare($id);
    }

    public function collectionCustomer($id) {
        $data = $this->repository->find($id);
        return view($this->partView . '.collection-customer', compact('data'));
    }

    public function updateCollectionCustomer($id, Request $request) {
        $this->serviceCustomer->deactiveCutomer($id);
        return redirect()->route('list.index')->with('message', trans('messages.collection_sharing_customer'));

    }

    public function getUpcomingRecover(Request $request) {
        // Get customer not cared in week
        $arrUser = [];
        if ($request->input('user_id')) {
            array_push($arrUser, $request->input('user_id'));
        } else {
            $user    = new User();
            $arrUser = $user->getAllChildUser(\Auth::user()->id);
            array_push($arrUser, \Auth::user()->id);
        }
        $input   = $request->all();
        $expDate = Carbon::now()->subDays(7);

        $model = $this->model;
        $total = $model::count();
        $model = $model->whereIn(Customer::CREATED_BY, $arrUser)
            ->where(Customer::HISTORY_CARE_AT, '<=', $expDate)
            ->where(Customer::SOURCE_ID, Customer::SOURCE_CUSTOMER_COMPANY)
            ->whereNotNull(Customer::SHARE_ID)->take($input['pagination']['perpage'])
            ->skip($input['pagination']['perpage'] * ($input['pagination']['page'] - 1))->get();
        $data = [];
        if (count($model) > 0) {
            foreach ($model as $key => $value) {
                $data[$key]                      = new \stdClass();
                $data[$key]                      = $value;
                $data[$key]->history_care_at_str = Carbon::createFromFormat('Y-m-d H:i:s', $value->history_care_at)->format('d-m-Y H:i:s');
                $data[$key]->name_user           = $value->agent->fullname;
            }
        }
        $response['data'] = $data;
        $meta             = new \stdClass();
        $meta->page       = $input['pagination']['page'];
        $meta->perpage    = $input['pagination']['perpage'];
        $meta->total      = $total;
        $response['meta'] = $meta;
        return response()->json($response);

    }
}
