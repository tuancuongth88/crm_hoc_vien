<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customers\HistoriesCreateRequest;
use App\Http\Requests\Customers\HistoriesUpdateRequest;
use App\Models\Customers\Customer;
use App\Models\Customers\Histories;
use App\Models\Customers\Level;
use App\Models\Customers\ProjectCustomer;
use App\Models\Projects\Project;
use App\Models\Users\User;

//use App\Http\Requests;
use App\Repositories\Customers\HistoriesRepository;
use App\Repositories\Customers\ServiceCustomerRepository;
use App\Services\AuthService;
use App\Validators\Customers\HistoriesValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class HistoriesController.
 *
 * @package namespace App\Http\Controllers\Customers;
 */
class HistoriesController extends Controller {
    private $auth;
    private $request;
    /**
     * @var HistoriesRepository
     */
    protected $serviceCustomerRepository;
    protected $repository;

    /**
     * @var HistoriesValidator
     */
    protected $validator;

    private $partView;
    /**
     * HistoriesController constructor.
     *
     * @param HistoriesRepository $repository
     * @param HistoriesValidator $validator
     */
    public function __construct(AuthService $auth, Request $request, HistoriesRepository $repository, HistoriesValidator $validator, ServiceCustomerRepository $serviceCustomerRepository) {
        $this->auth       = $auth;
        $this->request    = $request;
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->serviceCustomerRepository = $serviceCustomerRepository;
        $this->partView   = 'administrator.customers.histories';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $histories = $this->repository->paginate();
        if (request()->wantsJson()) {
            return response()->json([
                'data' => $histories,
            ]);
        }

        return view($this->partView . '.index', ['data' => $histories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  HistoriesCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(HistoriesCreateRequest $request) {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            //delete all project customer
            $relationProject               = ProjectCustomer::where('customer_id', $request['customer_id'])->delete();
            $CustomerCare                  = Customer::find($request['customer_id']);
            $CustomerCare->history_care_at = date('Y-m-d H:i:s');
            $CustomerCare->status          = $request['status'];
            $CustomerCare->save();
            //add relation
            $project = $request->project;
            if (isset($project)) {
                foreach ($project as $key => $value) {
                    $data                = $value;
                    $data['customer_id'] = $request['customer_id'];
                    ProjectCustomer::create($data);
                }
            }

            // save data
            $input = $request->except('project');
            if ($input['time_call_back']) {
                $input['time_call_back'] = \Carbon\Carbon::parse($input['time_call_back']);
            }
            $input['user_id']    = \Auth::user()->id;
            $input['created_by'] = \Auth::user()->id;
            $history             = $this->repository->create($input);

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $history->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }
            return redirect()->route('history.show', $input['customer_id'])->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {

        $customer = Customer::find($id);
        // danh sach sale
        $listCustomerProject = ProjectCustomer::where('customer_id', $id);
        $levels              = Level::pluck('name', 'id')->toArray();
        $listProject         = Project::pluck('name', 'id')->toArray();
        $listHistory         = Histories::where(Histories::CUSTOMER_ID, $id)->get();
        if (request()->wantsJson()) {
            $viewHistory = view("administrator.customers.customer.listHistory", compact('listHistory'))->render();
            return response()->json([
                'customer'        => $customer,
                'viewlistHistory' => $viewHistory,
            ]);
        }
        return view($this->partView . '.index', compact('customer', 'listCustomerProject', 'levels', 'listProject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {

        $history = $this->repository->find($id);

        return view('histories.edit', compact('history'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  HistoriesUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(HistoriesUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $input               = $request->all();
            $input['updated_by'] = \Auth::user()->id;
            $history             = $this->repository->update($input, $id);

            $response = [
                'message' => 'Histories updated.',
                'data'    => $history->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->repository->delete($id);
        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }
        return redirect('history.index')->with('message', trans('messages.delete_success'));
    }

    public function saveHistory(Request $request) {
        $input                         = $request->all();
        $CustomerCare                  = Customer::find($input['customer_id']);
        $CustomerCare->history_care_at = date('Y-m-d H:i:s');
        $CustomerCare->status          = $input['status'];
        $CustomerCare->save();
        if ($input['time_call_back']) {
            $input['time_call_back'] = \Carbon\Carbon::parse($input['time_call_back']);
        }
        $input['user_id']    = \Auth::user()->id;
        $input['created_by'] = \Auth::user()->id;
        $history             = $this->repository->create($input);

        if($input['status'] == Customer::DEACTIVE && $CustomerCare->share_id != null){

            $this->serviceCustomerRepository->deactiveCutomer($input['customer_id'], 1);
        }
        if ($history) {
            return response()->json([
                'message' => trans('messages.create_success'),
            ]);
        }
    }

    public function getTodayCare(Request $request, $type = 1) {
        // $userId = (isset($request->input['user_id'])) ? $request->input['user_id'] : \Auth::user()->id;

        $arrUser = [];
        if ($request->input('user_id')) {
            array_push($arrUser, $request->input('user_id'));
        } else {
            $user    = new User();
            $arrUser = $user->getAllChildUser(\Auth::user()->id);
            array_push($arrUser, \Auth::user()->id);
        }
        $input = $request->all();
        $model = new Histories();
        $total = $model::count();
        $model = $model->whereIn('user_id', $arrUser);
        if ($type == 1) {
            $model = $model->whereBetween('created_at', [\Carbon\Carbon::today(), \Carbon\Carbon::now()->endOfDay()]);
        } else {
            $model = $model->whereBetween('time_call_back', [\Carbon\Carbon::today(), \Carbon\Carbon::now()->endOfDay()]);
        }

        $model = $model->take($input['pagination']['perpage'])
            ->skip($input['pagination']['perpage'] * ($input['pagination']['page'] - 1));
        $listData = $model->with(['agent', 'customer'])->get()->toArray();
        $data     = [];
        if (count($listData) > 0) {
            foreach ($listData as $key => $value) {
                $data[$key] = new \stdClass();
                $data[$key] = $value;
                if ($value['customer']['created_by'] != Auth::user()->id) {
                    $data[$key]['phone'] = '***' . substr($value['phone'], 6);
                }
            }
        }
        $response['data'] = $data;
        $meta             = new \stdClass();
        $meta->page       = $input['pagination']['page'];
        $meta->perpage    = $input['pagination']['perpage'];
        $meta->total      = $total;
        $response['meta'] = $meta;
        return response()->json($response);
    }

}
