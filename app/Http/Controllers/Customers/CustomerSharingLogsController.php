<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customers\CustomerSharingLogCreateRequest;
use App\Http\Requests\Customers\CustomerSharingLogUpdateRequest;
use App\Repositories\Customers\CustomerSharingLogRepository;
use App\Validators\Customers\CustomerSharingLogValidator;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;


/**
 * Class CustomerSharingLogsController.
 *
 * @package namespace App\Http\Controllers;
 */
class CustomerSharingLogsController extends Controller
{
    /**
     * @var CustomerSharingLogRepository
     */
    protected $repository;

    /**
     * @var CustomerSharingLogValidator
     */
    protected $validator;

    /**
     * CustomerSharingLogsController constructor.
     *
     * @param CustomerSharingLogRepository $repository
     * @param CustomerSharingLogValidator $validator
     */
    public function __construct(CustomerSharingLogRepository $repository, CustomerSharingLogValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $customerSharingLogs = $this->repository->all();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $customerSharingLogs,
            ]);
        }

        return view('customerSharingLogs.index', compact('customerSharingLogs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CustomerSharingLogCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CustomerSharingLogCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $customerSharingLog = $this->repository->create($request->all());

            $response = [
                'message' => 'CustomerSharingLog created.',
                'data' => $customerSharingLog->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $customerSharingLog = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $customerSharingLog,
            ]);
        }

        return view('customerSharingLogs.show', compact('customerSharingLog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $customerSharingLog = $this->repository->find($id);

        return view('customerSharingLogs.edit', compact('customerSharingLog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CustomerSharingLogUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CustomerSharingLogUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $customerSharingLog = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'CustomerSharingLog updated.',
                'data' => $customerSharingLog->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'CustomerSharingLog deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'CustomerSharingLog deleted.');
    }
}
