<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\Customers\TypeCustomerCreateRequest;
use App\Http\Requests\Customers\TypeCustomerUpdateRequest;
use App\Repositories\Customers\TypeCustomerRepository;
use App\Validators\Customers\TypeCustomerValidator;

/**
 * Class TypeCustomersController.
 *
 * @package namespace App\Http\Controllers\Customers;
 */
class TypeCustomersController extends Controller
{
    /**
     * @var TypeCustomerRepository
     */
    protected $repository;

    /**
     * @var TypeCustomerValidator
     */
    protected $validator;

    private $partView;
    /**
     * TypeCustomersController constructor.
     *
     * @param TypeCustomerRepository $repository
     * @param TypeCustomerValidator $validator
     */
    public function __construct(TypeCustomerRepository $repository, TypeCustomerValidator $validator)
    {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->partView = 'administrator.customers.customer_type';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $typeCustomers = $this->repository->orderBy('name')->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $typeCustomers,
            ]);
        }

        return view($this->partView.'.index', ['data' =>$typeCustomers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TypeCustomerCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(TypeCustomerCreateRequest $request)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $typeCustomer = $this->repository->create($request->all());

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $typeCustomer->toArray(),
            ];

            if ($request->wantsJson()) {
                return response()->json($response);
            }
            return redirect()->route('customer_type.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $typeCustomer = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $typeCustomer,
            ]);
        }

        return view('typeCustomers.show', compact('typeCustomer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $typeCustomer = $this->repository->find($id);

        return view($this->partView.'.edit', ['data' => $typeCustomer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  TypeCustomerUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(TypeCustomerUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $typeCustomer = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $typeCustomer->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return redirect()->route('customer_type.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag()
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'TypeCustomer deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'TypeCustomer deleted.');
    }

    public function create(){
        return view($this->partView.'.create');
    }
}
