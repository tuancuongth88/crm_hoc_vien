<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customers\CustomerSharingUserCreateRequest;
use App\Http\Requests\Customers\CustomerSharingUserUpdateRequest;
use App\Models\Customers\Customer;
use App\Models\Customers\CustomerSharing;
use App\Models\Customers\CustomerSharingUser;
use App\Models\Users\User;
use App\Repositories\Customers\CustomerSharingUserRepository;
use App\Validators\Customers\CustomerSharingUserValidator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class CustomerSharingUsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class CustomerSharingUsersController extends Controller {
    /**
     * @var CustomerSharingUserRepository
     */
    protected $repository;

    /**
     * @var CustomerSharingUserValidator
     */
    protected $validator;
    protected $partView;
    private $model;

    /**
     * CustomerSharingUsersController constructor.
     *
     * @param CustomerSharingUserRepository $repository
     * @param CustomerSharingUserValidator $validator
     */
    public function __construct(CustomerSharingUserRepository $repository, CustomerSharingUserValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->partView   = 'administrator.customers.sharing_user';
        $this->model      = new CustomerSharingUser();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $listSharing = CustomerSharing::all()->pluck('name', 'id')->toArray();

        $customerSharingUsers = $this->model;
        if(\Auth::user()->can('is-manager')){
            $customerSharingUsers = $customerSharingUsers->where('user_id', \Auth::user()->id);
        }

        if (isset($request['search']) && $request['search'] != '' && \Auth::user()->can('is-admin-customer')) {
            $list_user_id         = User::where('fullname', 'like', '%' . $request['search'] . '%')->pluck('id')->toArray();
            $customerSharingUsers = $customerSharingUsers->whereIn('user_id', $list_user_id);
        }

        if ($request['sharing_id']) {
            $customerSharingUsers = $customerSharingUsers->where('cus_sharing_id', $request['sharing_id']);
        }

        $data = $customerSharingUsers->whereNull('parent_id')->orderBy('id', 'desc')->paginate();
        return view($this->partView . '.index', compact('data', 'listSharing'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CustomerSharingUserCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CustomerSharingUserCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $customerSharingUser = $this->repository->create($request->all());

            $response = [
                'message' => 'CustomerSharingUser created.',
                'data'    => $customerSharingUser->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = CustomerSharingUser::where('parent_id', $id)->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->partView . '.show', compact('data'));
    }

    public function listShare($id) {
        $dataCusSharingUser = CustomerSharingUser::where('id', $id)->first();

        $data = [];
        if ($dataCusSharingUser->params) {
            foreach ($dataCusSharingUser->params as $key => $value) {
                $data[$key] = new \stdClass();
                $objcus     = Customer::where('id', $value['customer_id'])->get();
                foreach ($objcus as $key1 => $value1) {
                    $data[$key]                    = $value1;
                    $data[$key]->sharing_assign_at = $value['assign_at'];
                }
            }

        }

        return view($this->partView . '.detail', compact('data'));
    }

    public function listRecovery($id) {
        $dataCusSharingUser = CustomerSharingUser::where('id', $id)->first();
        $data               = [];
        if ($dataCusSharingUser->recover_list) {
            foreach ($dataCusSharingUser->recover_list as $key => $value) {
                $data[$key] = new \stdClass();
                $objcus     = Customer::where('id', $value['customer_id'])->get();
                foreach ($objcus as $key1 => $value1) {
                    $data[$key]                     = $value1;
                    $data[$key]->sharing_recover_at = $value['created_at'];
                    $data[$key]->recover_message    = $value['message'];
                }
                foreach ($dataCusSharingUser->params as $key_p => $value_p) {
                    if ($value_p['customer_id'] == $value['customer_id']) {
                        $data[$key]->sharing_assign_at = $value_p['assign_at'];
                    }
                }
            }

        }
        $is_list_recover = true;

        return view($this->partView . '.detail', compact('data', 'is_list_recover'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $customerSharingUser = $this->repository->find($id);

        return view('customerSharingUsers.edit', compact('customerSharingUser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CustomerSharingUserUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CustomerSharingUserUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $customerSharingUser = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'CustomerSharingUser updated.',
                'data'    => $customerSharingUser->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {
            return response()->json([
                'message' => 'CustomerSharingUser deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'CustomerSharingUser deleted.');
    }

    public function getCollectionCustomer(Request $request) {
        $listSharing = CustomerSharing::all()
            ->where('created_by', \Auth::user()->id)
            ->pluck('name', 'id')->toArray();
        $customerSharingUsers = $this->model
            ->where('user_id', \Auth::user()->id)
            ->whereNotNULL('number_recover');
        if ($request['share_id']) {
            $customerSharingUsers = $customerSharingUsers->where('cus_sharing_id', $request['share_id']);
        }
        $customerSharingUsers = $customerSharingUsers->paginate();
        $data                 = [];
        foreach ($customerSharingUsers as $key => $value) {
            foreach ($value->recover_list as $key1 => $value1) {
                if ($value1 == null) {
                    continue;
                }
                $customer                    = Customer::find($value1['customer_id']);
                $care_by                     = User::find($value->user_id);
                $created_by                  = User::find($value1['created_by']);
                $data[$key1]                 = new \stdClass();
                $data[$key1]                 = $value1;
                $data[$key1]['fullname']     = $customer->fullname;
                $data[$key1]['email']        = $customer->email;
                $data[$key1]['phone']        = $customer->phone;
                $data[$key1]['care_by']      = $care_by->fullname;
                $data[$key1]['created_by']   = $created_by->fullname;
                $data[$key1]['sharing_name'] = $value->cusSharing->name;
            }
        }
        return view($this->partView . '.list-collection-customer', compact('data', 'listSharing'));
    }
}
