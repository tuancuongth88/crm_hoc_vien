<?php

namespace App\Http\Controllers\Customers;

use App\Http\Controllers\Controller;
use App\Http\Requests\Customers\CustomerSharingCreateRequest;
use App\Http\Requests\Customers\CustomerSharingUpdateRequest;
use App\Jobs\SendEmail;
use App\Models\Customers\Customer;
use App\Models\Customers\CustomerSharing;
use App\Models\Customers\CustomerSharingLog;
use App\Models\Customers\CustomerSharingUser;
use App\Models\Customers\TypeCustomer;
use App\Models\Permissions\Permission;
use App\Models\Permissions\Role;
use App\Models\Projects\District;
use App\Models\Projects\ProjectManager;
use App\Models\Projects\Province;
use App\Models\Systems\Department;
use App\Models\Systems\Position;
use App\Models\Users\User;
use App\Notifications\CustomerNotifications;
use App\Repositories\Customers\CustomerSharingRepository;
use App\Validators\Customers\CustomerSharingUserValidator;
use App\Validators\Customers\CustomerSharingValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class CustomerSharingsController.
 *
 * @package namespace App\Http\Controllers;
 */
class CustomerSharingsController extends Controller {
    /**
     * @var CustomerSharingRepository
     */
    protected $repository;

    /**
     * @var CustomerSharingValidator
     */
    protected $validator;

    protected $validatorSharingUser;

    private $partView;

    private $user;

    private $model;

    private $mCustomer;

    private $mCustomerSharingUser;

    private $mSharingLog;

    /**
     * CustomerSharingsController constructor.
     *
     * @param CustomerSharingRepository $repository
     * @param CustomerSharingValidator $validator
     */
    public function __construct(
        CustomerSharingRepository $repository,
        CustomerSharingValidator $validator,
        CustomerSharingUserValidator $validatorSharingUser) {
        $this->repository           = $repository;
        $this->validator            = $validator;
        $this->validatorSharingUser = $validatorSharingUser;
        $this->partView             = 'administrator.customers.sharing';
        $this->user                 = new User();
        $this->model                = new CustomerSharing();
        $this->mCustomer            = new Customer();
        $this->mCustomerSharingUser = new CustomerSharingUser();
        $this->mSharingLog          = new CustomerSharingLog();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        //$customerSharings = $this->repository->all();

        $projectManager = ProjectManager::all()->pluck('name', 'id')->toArray();
        $listProvince   = Province::all()->pluck('name', 'city_id')->toArray();
        $listDistrict   = District::all()->pluck('name', 'district_id')->toArray();
        $listRoles      = Role::all()->pluck('display_name', 'id')->toArray();
        $listCusType    = TypeCustomer::orderBy('name')->pluck('name', 'id')->toArray();
        $listCusGroup   = CustomerSharing::$listCusGroup;

        $customers = $this->mCustomer->whereNull('created_by');

        if (isset($request['city_id'])) {
            $customers = $customers->where('city_id', $request['city_id']);
        }
        if (isset($request['district_id'])) {
            $customers = $customers->where('district_id', $request['district_id']);
        }
        if (isset($request['type']) && $request['type'] != 0) {
            $customers = $customers->where('type', $request['type']);
        }

        $totalRecord = $customers->count();
        return view($this->partView . '.index',
            compact(
                'projectManager',
                'listProvince',
                'listDistrict',
                'totalRecord',
                'listRoles',
                'listCusType',
                'listCusGroup'
            ));
    }

    public function list(Request $request) {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $field_sort = 'name';
        $sort_type  = 'asc';
        if ($request->has('sort')) {
            $sort_input = $request['sort'];
            $field_sort = $sort_input['field'];
            $field_sort = ($field_sort == 'created_at_str') ? 'created_at' : 'name';
            $sort_type  = $sort_input['sort'];
        }

        $customerSharings = $this->model->orderBy($field_sort, $sort_type)->get();
        $data             = [];
        foreach ($customerSharings as $key => $value) {
            $data[$key]                 = new \stdClass();
            $data[$key]                 = $value;
            $data[$key]->created_name   = $value->createdBy->fullname;
            $data[$key]->created_at_str = $value->created_at->format('d-m-Y H:i:s');
            $count                      = 0;
            $count_recover              = 0;
            foreach ($value->sharingUser as $key_u => $val_u) {

                $count += $val_u->number_share;
                $count_recover += $val_u->number_recover;
            }
            $data[$key]->total_number_share   = $count;
            $data[$key]->total_number_recover = $count_recover;
        }

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->partView . '.list',
            compact(
                'response'
            ));
    }

    public function getSharingUser($id) {

        $sharingUser = CustomerSharingUser::where(CustomerSharingUser::CUS_SHARING_ID, $id)->orderBy('id', 'desc')->get();
        $data        = [];
        foreach ($sharingUser as $key => $value) {
            $data[$key]           = new \stdClass();
            $data[$key]           = $value;
            $data[$key]->fullname = $value->user->fullname;
        }
        return response()->json([
            'data' => $data,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CustomerSharingCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(CustomerSharingCreateRequest $request) {
        $name_sharing = $request['name'];
        //check data input
        if (isset($request['list_user_sharing'])) {
            $listFail             = [];
            $total_number_sharing = 0;
            $user_id_arr          = [];
            foreach ($request['list_user_sharing'] as $key => $value) {
                $number_share = isset($value['number_share']) ? $value['number_share'] : null;
                if (!$number_share || (int) $number_share < 1) {
                    $listFail[] = 'Số lượng không được trống và lớn hơn 0';
                } else {
                    $total_number_sharing += $number_share;
                }
                $user_id = isset($value['user_id']) ? (int) $value['user_id'] : null;
                if (!$user_id) {
                    $listFail[] = 'Người dùng không được trống';
                }
                if (in_array($user_id, $user_id_arr)) {
                    $listFail[] = 'Danh sách người dùng không được trùng nhau';
                } else {
                    $user_id_arr[] = $user_id;
                }

                if (count($listFail) > 0) {
                    return redirect()->back()->with('error', trans('Lỗi chia khách hàng'))
                        ->with('list-fail', $listFail);
                }
            }

            $customers = $this->mCustomer->whereNull('created_by');
            if (isset($request['type']) && $request['type'] != 0) {
                $customers = $customers->where(Customer::TYPE, $request['type']);
            }
            if (isset($request['group']) && $request['group'] != '' && $request['group'] != null) {
                switch ($request['group']) {
                case CustomerSharing::GROUP_NEW:
                    $customers = $customers->whereNull(Customer::TOTAL_ASSIGN);
                    break;
                case CustomerSharing::GROUP_SHARED:
                    $customers = $customers->whereNotNull(Customer::TOTAL_ASSIGN);
                    $customers =  $customers->where(
                        function ($query) {
                            $query->where(Customer::STATUS, '<>',Customer::DEACTIVE);
                            $query->orWhereNull(Customer::STATUS);
                        });
                    break;
                case CustomerSharing::GROUP_NO_NEED:
                    $customers = $customers->where(Customer::STATUS, Customer::DEACTIVE);
                    break;
                }
            }

            $customers_check = $customers->count();
            if ($customers_check < $total_number_sharing) {
                $listFail[] = 'Số lượng chia không được lớn hơn tổng số khách hàng hiện có';
            }

            if (!$name_sharing && $name_sharing == '') {
                $listFail[] = 'Tên chiến dịch không được trống';
            }

            if (count($listFail) > 0) {
                return redirect()->back()->with('error', trans('Lỗi chia khách hàng'))
                    ->with('list-fail', $listFail);
            }
        }

        //create sharing
        $objSharing = $this->repository->create(
            [
                CustomerSharing::NAME       => $name_sharing,
                CustomerSharing::CREATED_BY => Auth::user()->id,
            ]
        );
        if ($objSharing->count() > 0) {
            foreach ($request['list_user_sharing'] as $key => $value) {
                $number_share       = isset($value['number_share']) ? $value['number_share'] : null;
                $input_sharing_user = [
                    CustomerSharingUser::NUMBER_SHARE   => $number_share,
                    CustomerSharingUser::USER_ID        => $value['user_id'],
                    CustomerSharingUser::CUS_SHARING_ID => $objSharing->id,
                    CustomerSharingUser::CREATED_BY     => Auth::user()->id,
                ];
                $obj_customerSharingUser = $this->mCustomerSharingUser->create($input_sharing_user);
                //send mail thông báo
                $user = User::find($value['user_id']);
                $user->notify(new CustomerNotifications(
                    $user->id,
                    'Hệ thống quản lý công việc',
                    'Bạn vừa được bàn giao ' . $number_share . 'KH từ chiến dịch: ' . $objSharing->name
                ));
                //get customer

                $customers_ran = $this->mCustomer->whereNull('created_by');
                if (isset($request['type']) && $request['type'] != 0) {
                    $customers_ran = $customers_ran->where(Customer::TYPE, $request['type']);
                }
                if (isset($request['group']) && $request['group'] != '' && $request['group'] != null) {
                    switch ($request['group']) {
                    case CustomerSharing::GROUP_NEW:
                        $customers_ran = $customers_ran->whereNull(Customer::TOTAL_ASSIGN);
                        break;
                    case CustomerSharing::GROUP_SHARED:
                        $customers_ran = $customers_ran->whereNotNull(Customer::TOTAL_ASSIGN);
                        $customers_ran =  $customers_ran->where(
                            function ($query) {
                                $query->where(Customer::STATUS, '<>',Customer::DEACTIVE);
                                $query->orWhereNull(Customer::STATUS);
                            });
                        break;
                    case CustomerSharing::GROUP_NO_NEED:
                        $customers_ran = $customers_ran->where(Customer::STATUS, Customer::DEACTIVE);
                        break;
                    }
                }

                $customers_ran    = $customers_ran->inRandomOrder()->take($number_share)->get();
                $obj_params_user  = new \stdClass();
                $obj_sharing_user = new \stdClass();
                foreach ($customers_ran as $key_cus => $val_cus) {
                    $obj_params  = new \stdClass();
                    $string1     = "1";
                    $obj_sharing = new \stdClass();

                    $sharing_params             = new \stdClass();
                    $sharing_params->assign_by  = Auth::user()->id;
                    $sharing_params->sharing_id = (int) $objSharing->id;
                    $sharing_params->sale_id    = (int) $value['user_id'];
                    $sharing_params->comment    = 'Admin bàn giao cho quản lý';
                    $sharing_params->created_at = date("d-m-Y H:i:s");

                    //check sharing log
                    $sharing_logs = CustomerSharingLog::where('customer_id', $val_cus->id)->first();
                    if ($sharing_logs) {
                        //update param
                        $count_item_params               = count($sharing_logs->params) + 1;
                        $obj_sharing->$count_item_params = $sharing_params;
                        $obj_params                      = $obj_sharing;
                        $str_sql_update                  = "UPDATE customer_sharing_logs SET params = JSON_MERGE(params, '" . json_encode($obj_params) . "') where id = " . $sharing_logs->id;
                        DB::unprepared(DB::raw($str_sql_update));
                        CustomerSharingLog::where('id', $sharing_logs)->update(array(CustomerSharingLog::UPDATED_BY => Auth::user()->id));
                    } else {
                        //insert new
                        $obj_sharing->$string1 = $sharing_params;
                        $obj_params            = $obj_sharing;
                        $data_sharing_log      = [
                            CustomerSharingLog::CUSTOMER_ID => $val_cus->id,
                            CustomerSharingLog::PARAMS      => $obj_params,
                            CustomerSharingLog::CREATED_BY  => Auth::user()->id,
                        ];
                        CustomerSharingLog::create($data_sharing_log);
                    }

                    $total_assign = 1;
                    if ($val_cus->total_assign != null && $val_cus->total_assign != '') {
                        $total_assign = $total_assign + (int) $val_cus->total_assign;
                    }

                    $this->mCustomer->where('id', $val_cus->id)
                        ->update(
                            array(
                                Customer::CREATED_BY      => $value['user_id'],
                                Customer::USER_ID         => $value['user_id'],
                                Customer::SHARE_ID        => $objSharing->id,
                                Customer::SOURCE_ID       => Customer::SOURCE_CUSTOMER_COMPANY,
                                Customer::LEVEL           => 1,
                                Customer::TOTAL_ASSIGN    => $total_assign,
                                Customer::HISTORY_CARE_AT => Carbon::now()->toDateTimeString(),
                                Customer::ASSIGN_AT       => Carbon::now()->toDateTimeString(),
                            ));

                    $obj_sharing_params_user              = new \stdClass();
                    $obj_sharing_params_user->customer_id = (int) $val_cus->id;
                    $obj_sharing_params_user->assign_at   = date("d-m-Y H:i:s");
                    $key_sharing_user                     = $key_cus + 1;
                    $obj_sharing_user->$key_sharing_user  = $obj_sharing_params_user;
                }
                $obj_params_user = $obj_sharing_user;
                $this->mCustomerSharingUser->where('id', $obj_customerSharingUser->id)
                    ->update(
                        array(
                            CustomerSharingUser::PARAMS => json_encode($obj_params_user),
                        ));
            }
            return redirect()->route('sharing.index')->with('message', trans('Chia dữ liệu thành công'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $customerSharing = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $customerSharing,
            ]);
        }

        return view('customerSharings.show', compact('customerSharing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $customerSharing = $this->repository->find($id);

        return view('customerSharings.edit', compact('customerSharing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CustomerSharingUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(CustomerSharingUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $customerSharing = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'CustomerSharing updated.',
                'data'    => $customerSharing->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {

        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'CustomerSharing deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'CustomerSharing deleted.');
    }

    public function getUserByRole($id) {
        $check_role = Role::find($id);
        $html       = '';
        if ($check_role->count() > 0) {
            $listUser = DB::table('users AS a')
                ->join('role_user as b', 'a.id', '=', 'b.user_id')
                ->where([['b.role_id', $id]])->orderBy('department_id')->get();
            $html = $this->innerHtml($listUser);
        }

        return response()->json([
            'data' => $html,
        ]);

    }

    public function innerHtml($data) {
        $html = '';
        foreach ($data as $value) {
            $department = Department::find($value->department_id);
            $position = Position::find($value->position_id);
            $html .= '<option value="' . $value->id . '">' .  $department->name .' | '.$position->name. ' | '. $value->fullname  .'</option>';
        }
        return $html;
    }

    public function countCustomer(Request $request) {
        $customers = $this->mCustomer->whereNull('created_by');

        if (isset($request['cus_type']) && $request['cus_type'] != 0) {
            $customers = $customers->where(Customer::TYPE, $request['cus_type']);
        }
        if (isset($request['cus_group']) && $request['cus_group'] != '') {
            switch ($request['cus_group']) {
            case CustomerSharing::GROUP_NEW:
                $customers = $customers->whereNull(Customer::TOTAL_ASSIGN);
                break;
            case CustomerSharing::GROUP_SHARED:
                $customers = $customers->whereNotNull(Customer::TOTAL_ASSIGN);
                $customers =  $customers->where(
                    function ($query) {
                        $query->where(Customer::STATUS, '<>',Customer::DEACTIVE);
                        $query->orWhereNull(Customer::STATUS);
                    });
                break;
            case CustomerSharing::GROUP_NO_NEED:
                $customers = $customers->where(Customer::STATUS, Customer::DEACTIVE);
                break;
            }

        }
        $totalRecord   = $customers->count();
        return response()->json([
            'data' => $totalRecord,
        ]);
    }
}
