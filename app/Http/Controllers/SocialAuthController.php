<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Users\User;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Socialite;
use URL;

class SocialAuthController extends Controller {

    protected $facebook;

    public function __construct(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $facebook) {
        $this->facebook = $facebook;
    }

    /**
     * Chuyển hướng người dùng sang OAuth Provider.
     *
     * @return Response
     */
    public function redirectToProvider($provider) {
        // if (!Session::has('pre_url')) {
        //     Session::put('pre_url', URL::previous());
        // } else {
        //     if (URL::previous() != URL::to('login')) {
        //         Session::put('pre_url', URL::previous());
        //     }

        // }
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Lấy thông tin từ Provider, kiểm tra nếu người dùng đã tồn tại trong CSDL
     * thì đăng nhập, ngược lại nếu chưa thì tạo người dùng mới trong SCDL.
     *
     * @return Response
     */
    public function handleProviderCallback($provider) {
        if ($provider == 'facebook') {
            $authUser = self::handleFacebook();
        } else {
            $user     = Socialite::driver($provider)->user();
            $authUser = $this->findOrCreateUser($user, $provider);
        }
        Auth::login($authUser, true);
        return Redirect::to(route('login'));
    }

    /**
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */
    public function findOrCreateUser($user, $provider) {
        $authUser = User::where('provider_id', $user->id)->first();
        if ($authUser) {
            return $authUser;
        }
        return User::create([
            'username'    => $user->name,
            'fullname'    => $user->name,
            'email'       => $user->email,
            'provider'    => $provider,
            'provider_id' => $user->id,
            'active'      => 1,
        ]);
    }

    public function handleFacebook() {
        try {
            $token = $this->facebook->getAccessTokenFromRedirect();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        // Access token will be null if the user denied the request
        // or if someone just hit this URL outside of the OAuth flow.
        if (!$token) {
            // Get the redirect helper
            $helper = $this->facebook->getRedirectLoginHelper();

            if (!$helper->getError()) {
                abort(403, 'Unauthorized action.');
            }

            // User denied the request
            dd(
                $helper->getError(),
                $helper->getErrorCode(),
                $helper->getErrorReason(),
                $helper->getErrorDescription()
            );
        }

        if (!$token->isLongLived()) {
            // OAuth 2.0 client handler
            $oauth_client = $this->facebook->getOAuth2Client();

            // Extend the access token.
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                dd($e->getMessage());
            }
        }

        $this->facebook->setDefaultAccessToken($token);
        // Save for later
        Session::put('fb_user_access_token', (string) $token);
        // Get basic info on the user from Facebook.
        try {
            $response = $this->facebook->get('/me?fields=id,name,email');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }

        // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
        $facebook_user = $response->getGraphUser();
        // Create the user if it does not exist or update the existing entry.
        // This will only work if you've added the SyncableGraphNodeTrait to your User model.
        return $user = User::createOrUpdateGraphNode($facebook_user);
    }
}
