<?php

namespace App\Http\Controllers\Educations;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Educations\ProgramCreateRequest;
use App\Http\Requests\Educations\ProgramUpdateRequest;
use App\Models\Education\Area;
use App\Models\Education\Program;
use App\Models\Education\Subject;
use App\Repositories\Education\ProgramRepository;
use App\Validators\Education\ProgramValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\Gate;

/**
 * Class ProgramsController.
 *
 * @package namespace App\Http\Controllers\Education;
 */
class ProgramsController extends Controller {
    /**
     * @var ProgramRepository
     */
    protected $repository;

    /**
     * @var ProgramValidator
     */
    protected $validator;

    /**
     * ProgramsController constructor.
     *
     * @param ProgramRepository $repository
     * @param ProgramValidator $validator
     */
    public function __construct(ProgramRepository $repository, ProgramValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->view       = 'administrator.educations.program';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::denies('program.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->view . '.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ProgramCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ProgramCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            //xử lý upload ảnh
            if($request->image_file){
                $file_image                           = $request->image_file;
                $request['image_url'] = uploadFile($file_image, IMAGE_PROGRAM);
            }
            if($request->icon_file){
                $file_icon                           = $request->icon_file;
                $request['icon_url'] = uploadFile($file_icon, IMAGE_PROGRAM);
            }
            $program = $this->repository->create($request->all());

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $program->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('program.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $program = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $program,
            ]);
        }

        return view($this->view . '.show', compact('program'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('program.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data     = $this->repository->find($id);
        $listArea = Area::pluck('name', 'id');
        return view($this->view . '.edit', compact('data', 'listArea'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ProgramUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ProgramUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            //xử lý upload ảnh
            if($request->image_file){
                $file_image                           = $request->image_file;
                $request['image_url'] = uploadFile($file_image, IMAGE_PROGRAM);
            }
            if($request->icon_file){
                $file_icon                           = $request->icon_file;
                $request['icon_url'] = uploadFile($file_icon, IMAGE_PROGRAM);
            }
            $program = $this->repository->update($request->all(), $id);

            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $program->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('program.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('program.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
            $program = $this->repository->find($id);
            if($program->subject->count() > 0){
                return redirect()->back()->with('error', 'Lĩnh vực này đã tồn tại '.$program->subject->count().' chương trình nên không thể xóa được');
            }
            if($program->delete()) {
                return redirect()->back()->with('message', trans('messages.delete_success'));
            } else {
                return redirect()->back()->with('error', trans('message.delete_failed'));
            }
            if (request()->wantsJson()) {

                return response()->json([
                    'message' => trans('messages.delete_success'),
                    'deleted' => $program,
                ]);
            }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('program.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $listArea = Area::pluck('name', 'id');
        return view($this->view . '.create', compact('listArea'));
    }

    public function getUser() {
        $listSubjectByUser = Subject::where('user_id', 2);
        $arrSubject        = $listSubjectByUser->pluck('id')->toArray();
        $listSubjectByUser = $listSubjectByUser->get()->toArray();
        $listArea = Area::pluck('name', 'id');
        return view($this->view . '.user', compact('listArea'));
    }

    public function getSubjectByProgram($id) {
        $listSubject = Subject::where('program_id', $id)->pluck('name', 'id')->toArray();
        return response()->json($listSubject);
    }
}