<?php

namespace App\Http\Controllers\Educations;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Educations\LessonCreateRequest;
use App\Http\Requests\Educations\LessonUpdateRequest;
use App\Models\Education\Document;
use App\Models\Education\Lesson;
use App\Models\Education\Program;
use App\Models\Education\Subject;
use App\Repositories\Educations\LessonRepository;
use App\Validators\Educations\LessonValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\Gate;

/**
 * Class LessonsController.
 *
 * @package namespace App\Http\Controllers\Educations;
 */
class LessonsController extends Controller
{
    /**
     * @var LessonRepository
     */
    protected $repository;

    /**
     * @var LessonValidator
     */
    protected $validator;

    /**
     * LessonsController constructor.
     *
     * @param LessonRepository $repository
     * @param LessonValidator $validator
     */
    public $partView;
    private $listImage;
    private $model;

    public function __construct(LessonRepository $repository, LessonValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->partView = 'administrator.educations.lesson';
        $this->listImage = array();
        $this->model = new Lesson();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('lesson.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $lessons = $this->model->where(Subject::CREATED_BY, '>', 0);
        if (isset($request['search'])) {
            $lessons->where(Lesson::NAME, 'like', '%' . $request['search'] . '%');
        }

        $lessons = $lessons->paginate(20);
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $lessons,
            ]);
        }
        $listProgram = Program::pluck('name', 'id')->toArray();
        $listSubject = Subject::pluck('name', 'id')->toArray();
        return view($this->partView . '.index', ['data' => $lessons, 'listProgram' => $listProgram, 'listSubject' => $listSubject]);
    }

    public function getSearch(Request $request)
    {
        $lessons = $this->repository->findWhere(['name' => $request->name]);
        return view($this->partView . '.index', ['data' => $lessons]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LessonCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(LessonCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request['created_by'] = Auth::user()->id;
            $lesson = $this->repository->create($request->all());
            $listFile = explode(',', $request->id_file);
            if ($listFile) {
                $document = Document::where('model', 'Lesson')->whereIn('id', $listFile);
                if ($document->count() > 0) {
                    $a[Document::ITEM_ID] = $lesson->id;
                    $document->update($a);
                }
            }
            $response = [
                'message' => trans('messages.create_success'),
                'data' => $lesson->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('lesson.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lesson = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $lesson,
            ]);
        }

        return view('lessons.show', compact('lesson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('lesson.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $lesson = $this->repository->find($id);
        $listParent = Lesson::whereNull(Lesson::PARENT)->pluck('name', 'id')->toArray();
        return view($this->partView . '.edit', ['data' => $lesson, 'listParent' => $listParent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  LessonUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(LessonUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $request['updated_by'] = Auth::user()->id;
            $lesson = $this->repository->update($request->all(), $id);
            $listFile = explode(',', $request->id_file);
            if ($listFile) {
                $document = Document::where('model', 'Lesson')->whereIn('id', $listFile);
                if ($document->count() > 0) {
                    $a[Document::ITEM_ID] = $lesson->id;
                    $document->update($a);
                }
            }
            $response = [
                'message' => trans('messages.update_success'),
                'data' => $lesson->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return redirect()->route('lesson.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('lesson.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $lesson = $this->repository->find($id);
        if($lesson->question->count() > 0){
            return redirect()->back()->with('error', 'Khóa học này thuộc  '.$lesson->question->count().' ngân hàng câu hỏi nên không thể xóa được.');
        }
        if($lesson->subject_lesson->count() > 0) {
            return redirect()->back()->with('error', 'Khóa học này thuộc  '.$lesson->subject_lesson->count().' chương trình nên không thể xóa được.');
        }
        if($lesson->delete()) {
            return redirect()->back()->with('message', trans('messages.delete_success'));
        } else {
            return redirect()->back()->with('error', trans('message.delete_failed'));
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'Lesson deleted.',
                'deleted' => $lesson,
            ]);
        }

        return redirect()->back()->with('message', 'Lesson deleted.');
    }

    public function create()
    {
        if (Gate::denies('lesson.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $listParent = Lesson::whereNull(Lesson::PARENT)->pluck('name', 'id')->toArray();
        return view($this->partView . '.create', compact('listParent'));
    }

    public function postUploadAttachment(Request $request)
    {
        $file = $request->file;
        $attachment[Document::FILE_URL] = uploadFile($file, EDUCATION_FILE_LESSON);
        $attachment[Document::MODEL] = 'Lesson';
        $attachment[Document::NAME] = $file->getClientOriginalName();

        $obj = Document::create($attachment);
        $this->listImage[] = $obj->id;

        return response()->json([
            'message' => trans('messages.upload_success'),
            'data' => $obj,
        ]);
    }

    public function postDeleteAttachment(Request $request)
    {
        $file = $request->all();
        $data = Document::find($file['id']);
        if ($data) {
            $data->delete();
            unlink(public_path() . $file['url']);
        };
        return response()->json([
            'message' => trans('messages.delete_success'),
            'status' => true,
        ]);
    }

    public function ajaxGetAllLessonSubject($id)
    {
        $lst_lessons = DB::table('education_lessons AS a')
            ->join('education_subject_lesson as b', 'a.id', '=', 'b.lesson_id')
            ->join('education_subjects as c', 'c.id', '=', 'b.subject_id')
            ->where([['c.id', $id]])->get();

        if ($lst_lessons->count() > 0) {
            $html = $this->innerHtml($lst_lessons);
        } else {
            $html = '';
        }

        return response()->json([
            'status' => true,
            'data' => $html
        ]);
    }

    public function innerHtml($data)
    {
        $html = '';
        foreach ($data as $key => $value) {
            $html .= '<tr class="d-flex">';
            $html .= '<td class="col-1 text-center" scope="row">' . ($key + 1) . '</td>';
            $html .= '<td class="col-sm-7 text-center">' . $value->name . '</td>';
            $html .= '<td class="col-sm-4 text-center">' . $value->open_time . '</td>';
            $html .= '</tr>';
        }
        return $html;
    }
}
