<?php

namespace App\Http\Controllers\Educations;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Educations\ClassRoomCreateRequest;
use App\Http\Requests\Educations\ClassRoomUpdateRequest;
use App\Repositories\Education\ClassRoomRepository;
use App\Validators\Education\ClassRoomValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class ClassRoomsController.
 *
 * @package namespace App\Http\Controllers\Education;
 */
class ClassRoomsController extends Controller {
    /**
     * @var ClassRoomRepository
     */
    protected $repository;

    /**
     * @var ClassRoomValidator
     */
    protected $validator;

    private $partView;
    /**
     * ClassRoomsController constructor.
     *
     * @param ClassRoomRepository $repository
     * @param ClassRoomValidator $validator
     */
    public function __construct(ClassRoomRepository $repository, ClassRoomValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->partView   = 'administrator.educations.classroom';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->all();
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }
        return view($this->partView.'.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ClassRoomCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ClassRoomCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $classRoom = $this->repository->create($request->all());

            $response = [
                'message' => 'ClassRoom created.',
                'data'    => $classRoom->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $classRoom = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $classRoom,
            ]);
        }

        return view('classRooms.show', compact('classRoom'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $classRoom = $this->repository->find($id);

        return view('classRooms.edit', compact('classRoom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ClassRoomUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ClassRoomUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);

            $classRoom = $this->repository->update($request->all(), $id);

            $response = [
                'message' => 'ClassRoom updated.',
                'data'    => $classRoom->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->back()->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $deleted = $this->repository->delete($id);

        if (request()->wantsJson()) {

            return response()->json([
                'message' => 'ClassRoom deleted.',
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', 'ClassRoom deleted.');
    }
}
