<?php

namespace App\Http\Controllers\Educations;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\Educations\SubjectCreateRequest;
use App\Http\Requests\Educations\SubjectUpdateRequest;
use App\Models\Education\Document;
use App\Models\Education\Exam;
use App\Models\Education\ExamResult;
use App\Models\Education\Lesson;
use App\Models\Education\Program;
use App\Models\Education\Subject;
use App\Models\Education\SubjectLesson;
use App\Models\Education\UserRegProgram;
use App\Models\Users\Type;
use App\Models\Users\User;
use App\Repositories\Education\SubjectRepository;
use App\Validators\Education\SubjectValidator;
use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;

/**
 * Class SubjectsController.
 *
 * @package namespace App\Http\Controllers\Educations;
 */
class SubjectsController extends Controller
{
    /**
     * @var SubjectRepository
     */
    protected $repository;

    /**
     * @var SubjectValidator
     */
    protected $validator;

    /**
     * SubjectsController constructor.
     *
     * @param SubjectRepository $repository
     * @param SubjectValidator $validator
     */

    private $partView;

    public function __construct(SubjectRepository $repository, SubjectValidator $validator)
    {
        $this->repository = $repository;
        $this->validator = $validator;
        $this->partView = 'administrator.educations.subject';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('subject.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        if (\Auth::user()->isStudent()) {
            $userId = Auth::user()->id;
            $listSubjectId = UserRegProgram::where(UserRegProgram::USER_ID, $userId)->pluck(UserRegProgram::SUBJECT_ID);
            $subjects = Subject::whereIn('id', $listSubjectId->toArray())->paginate(PAGINATE);
        } else {
            $subjects = $this->repository->paginate();
        }

        if (request()->wantsJson()) {
            return response()->json([
                'data' => $subjects,
            ]);
        }

        return view('administrator.educations.subject.index', ['data' => $subjects]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  SubjectCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(SubjectCreateRequest $request)
    {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $request['status'] = isset($request['status']) ? STATUS_ONE : STATUS_ZERO;
            $request['is_hot'] = isset($request['is_hot']) ? ISHOT_ONE : ISHOT_ZERO;
            $request['created_by'] = Auth::user()->id;
            $request[Subject::OPEN_TIME] = Carbon::parse($request['open_time']);
            $request[Subject::CLOSE_TIME] = Carbon::parse($request['close_time']);
            $subject = $this->repository->create($request->all());
            $listFile = explode(',', $request->id_file);

            if ($request[Subject::OPEN_TIME] > $request[Subject::CLOSE_TIME]) {
                return redirect()->back()->withErrors('Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc')->withInput();
            }

            if ($listFile) {
                $document = Document::where('model', 'Subject')->whereIn('id', $listFile);
                if ($document->count() > 0) {
                    $a['item_id'] = $subject->id;
                    $document->update($a);
                }
            }

            $response = [
                'message' => trans('messages.create_success'),
                'data' => $subject->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }
            return redirect()->route('subject.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = $this->repository->find($id);

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $subject,
            ]);
        }

        return view('subjects.show', compact('subject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('subject.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $subject = $this->repository->find($id);
        $listTypeUser = Type::pluck('name', 'id')->toArray();
        $listProgram = Program::pluck('name', 'id')->toArray();
        return view($this->partView . '.edit', ['data' => $subject, 'listTypeUser' => $listTypeUser, 'listProgram' => $listProgram]);
    }

    /**
     * Update the specified resource in storage.
     * @param  SubjectUpdateRequest $request
     * @param  string $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(SubjectUpdateRequest $request, $id)
    {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $request['status'] = isset($request['status']) ? STATUS_ONE : STATUS_ZERO;
            $request['is_hot'] = isset($request['is_hot']) ? ISHOT_ONE : ISHOT_ZERO;
            $request['created_by'] = Auth::user()->id;
            $request[Subject::OPEN_TIME] = Carbon::parse($request['open_time']);
            $request[Subject::CLOSE_TIME] = Carbon::parse($request['close_time']);
            $subject = $this->repository->update($request->all(), $id);

            if ($request[Subject::OPEN_TIME] > $request[Subject::CLOSE_TIME]) {
                return redirect()->back()->withErrors('Thời gian bắt đầu phải nhỏ hơn thời gian kết thúc')->withInput();
            }

            $listFile = explode(',', $request->id_file);
            if ($listFile) {
                $document = Document::where('model', 'Subject')->whereIn('id', $listFile);
                if ($document->count() > 0) {
                    $a['item_id'] = $subject->id;
                    $document->update($a);
                }
            }

            $response = [
                'message' => trans('messages.update_success'),
                'data' => $subject->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('subject.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error' => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('subject.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $subject = $this->repository->find($id);
        if($subject->lesson->count() > 0){
            return redirect()->back()->with('error', 'Chương trình này thuộc  '.$subject->lesson->count().' khóa học nên không thể xóa được');
        }
        if($subject->delete()) {
            return redirect()->back()->with('message', trans('messages.delete_success'));
        } else {
            return redirect()->back()->with('error', trans('message.delete_failed'));
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'subject' => $subject
            ]);
        }
        return redirect()->back()->with('message', 'Subject deleted.');
    }

    public function create()
    {
        if (Gate::denies('subject.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $listTypeUser = Type::pluck('name', 'id')->toArray();
        $listProgram = Program::pluck('name', 'id')->toArray();
        return view($this->partView . '.create', compact('subject', 'listTypeUser', 'listProgram'));
    }

    public function getUserByType($type)
    {
        $user = User::where('type', $type)->pluck('fullname', 'id')->toArray();
        return response()->json($user);
    }

    public function getLessonBySubject($id)
    {
        if (Gate::denies('subject.list.lesson.by_subject')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $subject = $this->repository->find($id);
        $listTypeUser = Type::pluck('name', 'id')->toArray();
        $listProgram = Program::pluck('name', 'id')->toArray();
        $listLessonBySubject = SubjectLesson::where(SubjectLesson::SUBJECT_ID, $id)->get();

        if (request()->wantsJson()) {
            foreach ($listLessonBySubject as $key => $value) {
                $listLessonBySubject[$key]['lesson_name'] = $value->lesson->name;
                $listLessonBySubject[$key]['fullname'] = $value->teacher->fullname;
                $listLessonBySubject[$key]['action_delete'] = route('subject.lesson.delete', $value->id);
            }
            return response()->json([
                'data' => $listLessonBySubject->toArray()
            ]);
        }

        return view(
            $this->partView . '.detail_lesson_by_subject',
            compact('subject', 'listTypeUser', 'listProgram', 'listLessonBySubject')
        );
    }

    public function addLessonToSubject($id)
    {
        if (Gate::denies('subject.add.lesson.to_subject')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $lessons = Lesson::paginate(PAGINATE);
        $user = Auth::user();
        $listUser = $user->getListUserByRole('is-teacher')->pluck('fullname', 'id')->toArray();
        return view($this->partView . '.add_lesson_to_subject', ['data' => $lessons, 'listUser' => $listUser, 'id' => $id]);
    }

    public function storeAddLessonToSubject(Request $request)
    {
        $checkLessonOnSubject = SubjectLesson::where($request->all())->count();
        if ($checkLessonOnSubject > 0) {
            return response()->json(['code' => '01', 'message' => 'Bài giảng thuộc giáo viên này đã tồn tại trong khóa học']);
        }
        if (SubjectLesson::create($request->all())) {
            return response()->json(['code' => '00', 'message' => 'Thêm mới thành công.']);
        }
        return response()->json(['code' => '01', 'message' => 'Thêm mới thất bại.']);
    }

    public function searchLesson(Request $request)
    {
        $lessons = Lesson::where(Lesson::CREATED_BY, '>', 0);
        if (isset($request['search'])) {
            $lessons->where(Lesson::NAME, 'like', '%' . $request['search'] . '%');
        }
        $lessons = $lessons->paginate(PAGINATE);

        $user = Auth::user();
        $listUser = $user->getListUserByRole('is-teacher')->pluck('fullname', 'id')->toArray();
        return view($this->partView . '.add_lesson_to_subject', ['data' => $lessons, 'listUser' => $listUser, 'id' => $request['subject_id']]);
    }

    public function deleteLessonOnSubject($id)
    {
        if (Gate::denies('subject.remove.lesson.to_subject')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $subjectLesson = SubjectLesson::find($id);
        if ($subjectLesson->delete()) {
            return redirect()->back()->with('message', trans('messages.delete_success'));
        }
        return redirect()->back()->with('message', trans('messages.delete_failed'));
    }

    public function postUploadAttachment(Request $request)
    {
        $file = $request->file;
        $attachment[Document::FILE_URL] = uploadFile($file, EDUCATION_FILE_SUBJECT);
        $attachment[Document::MODEL] = 'Subject';
        $attachment[Document::NAME] = $file->getClientOriginalName();

        $obj = Document::create($attachment);
        $this->listImage[] = $obj->id;

        return response()->json([
            'message' => trans('messages.upload_success'),
            'data' => $obj,
        ]);
    }

    public function postUploadAttachmentByTeacher(Request $request)
    {
        $file = $request->file;
        $attachment[Document::FILE_URL] = uploadFile($file, EDUCATION_FILE_SUBJECT);
        $attachment[Document::MODEL] = 'SubjectLesson';
        $attachment[Document::NAME] = $file->getClientOriginalName();
        $attachment[Document::ITEM_ID] = $request->item_id;

        $obj = Document::create($attachment);
        $this->listImage[] = $obj->id;
        return response()->json([
            'message'       => trans('messages.upload_success'),
            'data'          => $obj,
        ]);
    }

    public function postDeleteAttachment(Request $request)
    {
        $file = $request->all();
        $data = Document::find($file['id']);
        if ($data) {
            $data->delete();
            unlink(public_path() . $file['url']);
        };
        return response()->json([
            'message' => trans('messages.delete_success'),
            'status' => true,
        ]);
    }

    public function getAllUserBySubject($id)
    {
        if (Gate::denies('subject.list.user.reg.subject')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $user = Auth::user();
        $subject = Subject::find($id);
        if ($user->isStudent()) {
            $listUserReg = UserRegProgram::where('subject_id', $id)->where(UserRegProgram::USER_ID, $user->id)->paginate(PAGINATE);
        } else {
            $listUserReg = UserRegProgram::where('subject_id', $id)->paginate(PAGINATE);
        }
        $listStatusPass = UserRegProgram::statusPass();
        return view($this->partView . '.list_user_reg_subject', compact('subject', 'listUserReg', 'listStatusPass'));
    }

    public function getExamHistory($id, $user_id)
    {
        $subject = Subject::find($id);
//        $lst_exam = Exam::where($id)->examResult()->where(ExamResult::CREATED_BY, $user_id)->get();
        $lst_exam = Exam::join('education_exam_results', ExamResult::EXAM_ID, '=', 'education_exams.id')
            ->where('subject_id', $id)
            ->where('education_exam_results.created_by', $user_id)->get();
        return view(
            $this->partView . '.exam_history',
            compact('lst_exam', 'subject')
        );
    }

    public function userAssessment(Request $request)
    {
        if (Gate::denies('subject.set.user.assessment')) {
            return response()->json([
                'status' => false,
                'message' => "Bạn không có quyền đánh giá học viên.",
            ]);
        }
        $userReg = UserRegProgram::find($request->id);
        if ($userReg) {
            if ($userReg->update($request->all())) {
                return response()->json([
                    'status' => true,
                    'message' => trans('messages.update_success'),
                ]);
            }
        }
        return response()->json([
            'status' => false,
            'message' => trans('messages.update_failed'),
        ]);

    }

    public function getDataByItemId(Request $request)
    {
        $id = $request->has('id') ? $request->input('id') : null;
        $model_name = $request->has('model') ? $request->input('model') : null;
        $document = Document::where('item_id', $id)->where('model', $model_name)->get();

        $data = [];
        foreach ($document as $key => $value) {
            $data[$key] = $value;
        }
        return response()->json([
            'data' => $data,
        ]);
    }


    public function destroyDoc($id)
    {

        $doc = Document::find($id);
        if ($doc->count() > 0 && file_exists(public_path() . $doc->file_url)) {
            unlink(public_path() . $doc->file_url);
        }
        $deleted = $doc->delete();

        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function downloadFile($id)
    {
        $doc = Document::find($id);
        if ($doc->count() > 0 && file_exists(public_path() . $doc->file_url)) {


            header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
            header("Cache-Control: public"); // needed for internet explorer
            header("Content-Type: application/octet-stream");
            header("Content-Transfer-Encoding: Binary");
            header("Content-Length:" . filesize(public_path() . $doc->file_url));
            header("Content-Disposition: attachment; filename=" . $doc->name);
            readfile(public_path() . $doc->file_url);
            die();
        } else {
        }

    }
}