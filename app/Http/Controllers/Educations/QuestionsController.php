<?php

namespace App\Http\Controllers\Educations;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Educations\QuestionCreateRequest;
use App\Http\Requests\Educations\QuestionUpdateRequest;
use App\Models\Education\Exam;
use App\Models\Education\Question;
use App\Models\Education\Subject;
use App\Models\Education\Lesson;
use App\Repositories\Education\QuestionRepository;
use App\Validators\Education\QuestionValidator;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Illuminate\Support\Facades\Gate;

/**
 * Class QuestionsController.
 *
 * @package namespace App\Http\Controllers\Education;
 */
class QuestionsController extends Controller {
    /**
     * @var QuestionRepository
     */
    protected $repository;

    /**
     * @var QuestionValidator
     */
    protected $validator;

    /**
     * QuestionsController constructor.
     *
     * @param QuestionRepository $repository
     * @param QuestionValidator $validator
     */
    public function __construct(QuestionRepository $repository, QuestionValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->view       = 'administrator.educations.question';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::denies('question.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $data = $this->repository->paginate();
//        $listSubject = Subject::where(Subject::STATUS)
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->view . '.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  QuestionCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(QuestionCreateRequest $request) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);

            $model                     = $this->repository->model();
            $input                     = $request->all();
            $input[$model::CREATED_BY] = \Auth::user()->id;
            $question                  = $this->repository->create($input);
            $model::saveRelateAnswer($input['list_answer'], $question);

            $response = [
                'message' => trans('messages.create_success'),
                'data'    => $question->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('question.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $data = $this->repository->find($id);
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }
        return view($this->view . '.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('question.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data = $this->repository->find($id);
        $listSubject = Subject::pluck('name', 'id')->toArray();
        $listLesson = Lesson::pluck('name', 'id')->toArray();
        return view($this->view . '.edit', compact('data', 'listSubject', 'listLesson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  QuestionUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(QuestionUpdateRequest $request, $id) {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $input    = $request->all();
            $question = $this->repository->update($input, $id);
            $model    = $this->repository->model();
            $question->hasAnswers()->delete();
            $model::saveRelateAnswer($input['list_answer'], $question);
            $response = [
                'message' => trans('messages.update_success'),
                'data'    => $question->toArray(),
            ];

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('question.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('question.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $deleted = $this->repository->delete($id);
        if($deleted) {
            return redirect()->back()->with('message', trans('messages.delete_success'));
        } else {
            return redirect()->back()->with('error', trans('message.delete_failed'));
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'delete' => $deleted
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('question.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $listSubject = Subject::pluck('name', 'id')->toArray();
        $listLesson = Lesson::pluck('name', 'id')->toArray();
        return view($this->view . '.create', compact('listSubject', 'listLesson'));
    }

    public function getList(Request $request) {
        $input = $request->all();
        $model = new Question();
        $total = $model::count();
        if (isset($input['query'])) {
            foreach ($input['query'] as $key => $value) {
                if ($value == strval(ZERO) || $value == '') {
                    unset($input['query'][$key]);
                }
            }
            if (isset($input['query']['name'])) {
                $model = $model->where('name', 'LIKE', '%' . $input['query']['name'] . '%');
                unset($input['query']['name']);
            }
            $model = $model->where($input['query']);
        } else {
            $model = $model->take($input['pagination']['perpage'])
                ->skip($input['pagination']['perpage'] * ($input['pagination']['page'] - 1));
        }
        // $arrId = $model->pluck('id');
        $arrSelected = [];
        if ($request->input('id')) {
            $obj         = Exam::find($request->input('id'));
            $arrSelected = explode(',', $obj->questions);
        }
        $listData = $model->get();
        $data     = [];
        foreach ($listData as $key => $value) {
            $data[$key] = new \stdClass();
            $data[$key] = $value;

            // $data[$key]->RecordID = $key + 1;
            $data[$key]->level   = Question::$listLevel[$value->level];
            if(isset($value->subject)) {
                $data[$key]->subject_name = $value->subject->name;
            } else {
                continue;
            }

            if (in_array($value->id, $arrSelected)) {
                $data[$key]->checked = true;
            }
        }

        $response['data'] = $data;
        $meta             = new \stdClass();
        $meta->page       = $input['pagination']['page'];
        $meta->perpage    = $input['pagination']['perpage'];
        $meta->total      = $total;
        // $meta->rowIds     = $arrId;
        // $meta->field      = 'RecordID';
        $response['meta'] = $meta;
        // $response         = [];
        return response()->json($response);
    }

    public function getListById(Request $request) {
        $response = Question::whereIn('id', $request->all())->pluck('name', 'id');
        return response()->json($response);
    }

}