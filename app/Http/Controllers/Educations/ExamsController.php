<?php

namespace App\Http\Controllers\Educations;

use App\Http\Controllers\Controller as Controller;
use App\Http\Requests;
use App\Http\Requests\Educations\ExamCreateRequest;
use App\Http\Requests\Educations\ExamUpdateRequest;
use App\Models\Education\Answer;
use App\Models\Education\Exam;
use App\Models\Education\ExamResult;
use App\Models\Education\Lesson as Lession;
use App\Models\Education\Question;
use App\Models\Education\Subject;
use App\Models\Education\UserRegProgram;
use App\Models\Projects\ProjectManager;
use App\Models\Users\User;
use App\Repositories\Education\ExamRepository;
use App\Validators\Education\ExamValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\Gate;

/**
 * Class ExamsController.
 *
 * @package namespace App\Http\Controllers\Education;
 */
class ExamsController extends Controller {
    /**
     * @var ExamRepository
     */
    protected $repository;

    /**
     * @var ExamValidator
     */
    protected $validator;

    /**
     * ExamsController constructor.
     *
     * @param ExamRepository $repository
     * @param ExamValidator $validator
     */
    public function __construct(ExamRepository $repository, ExamValidator $validator) {
        $this->repository = $repository;
        $this->validator  = $validator;
        $this->view       = 'administrator.educations.exam';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (Gate::denies('exam.view')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        if(Auth::user()->isStudent()){
            $userId = Auth::user()->id;
            $listSubjectId = UserRegProgram::where(UserRegProgram::USER_ID, $userId)->pluck(UserRegProgram::SUBJECT_ID);
            $data = Exam::whereIn(Exam::SUBJECT_ID, $listSubjectId->toArray())->paginate(PAGINATE);
        }else{
            $data = $this->repository->paginate();
        }
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }

        return view($this->view . '.index', compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ExamCreateRequest $request
     *
     * @return \Illuminate\Http\Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function store(ExamCreateRequest $request) {
        try {
            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_CREATE);
            $model = $this->repository->model();
            $input = $request->all();
            if (\Carbon\Carbon::parse($input['start_time']) > \Carbon\Carbon::parse($input['finish_time'])) {
                $response = [
                    'status'  => false,
                    'message' => trans('messages.start_time_is_not_bigger_than_finish_time'),
                ];
                return response()->json($response);
            }
            if (\Carbon\Carbon::parse($input['start_time']) == \Carbon\Carbon::parse($input['finish_time'])) {
                $response = [
                    'status'  => false,
                    'message' => trans('messages.start_time_is_not_equal_finish_time'),
                ];
                return response()->json($response);
            }
            $input[$model::CREATED_BY] = \Auth::user()->id;
            $input[$model::QUESTIONS]  = (isset($input[$model::QUESTIONS])) ? implode(',', $input[$model::QUESTIONS]) : '';

            $exam     = $this->repository->create($input);
            $response = [
                'status' => false,
            ];
            if ($exam) {
                $response = [
                    'message' => trans('messages.create_success'),
                    'data'    => $exam->toArray(),
                    'status'  => true,
                ];
            }

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('exam.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {
            if ($request->wantsJson()) {
                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request) {
        $check = ExamResult::where('exam_id', $id)->where('created_by', \Auth::user()->id)->first();
        if ($check) {
            return redirect()->back()->with('error', trans('messages.you_had_done_this'));
        }

        $data = $this->repository->find($id);
        if (Carbon::now() >= Carbon::parse($data->finish_time)) {
            return redirect()->back()->with('error', trans('messages.exam_were_finished'));
        }
        if (Carbon::now() <= Carbon::parse($data->start_time)) {
            return redirect()->back()->with('error', trans('messages.exam_has_not_begun'));
        }

        if ($request->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }
        $listQuestion = Question::whereIn('id', explode(',', $data->questions))->get();
        return view($this->view . '.show', compact('data', 'listQuestion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        if (Gate::denies('exam.update')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $data        = $this->repository->find($id);
        $listLevel   = Question::$listLevel;
        $user = Auth::user();
        $listTeacher = $user->getListUserByRole('is-teacher')->pluck('fullname', 'id');
        $listSubject = Subject::pluck('name', 'id')->toArray();
        $listTypeExam = Exam::$typeExam;
        return view($this->view . '.edit', compact('listLevel', 'listSubject', 'listTeacher', 'data', 'listTypeExam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ExamUpdateRequest $request
     * @param  string            $id
     *
     * @return Response
     *
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function update(ExamUpdateRequest $request, $id) {
        try {

            $this->validator->with($request->all())->passesOrFail(ValidatorInterface::RULE_UPDATE);
            $model                     = $this->repository->model();
            $input                     = $request->all();
            $input[$model::UPDATED_BY] = \Auth::user()->id;
            $input[$model::QUESTIONS]  = implode(',', $input[$model::QUESTIONS]);
            $exam                      = $this->repository->update($input, $id);

            $response = [
                'status' => false,
            ];
            if ($exam) {
                $response = [
                    'message' => trans('messages.update_success'),
                    'data'    => $exam->toArray(),
                    'status'  => true,
                ];
            }

            if ($request->wantsJson()) {

                return response()->json($response);
            }

            return redirect()->route('exam.index')->with('message', $response['message']);
        } catch (ValidatorException $e) {

            if ($request->wantsJson()) {

                return response()->json([
                    'error'   => true,
                    'message' => $e->getMessageBag(),
                ]);
            }

            return redirect()->back()->withErrors($e->getMessageBag())->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Gate::denies('exam.delete')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $listExamsResult = ExamResult::where(ExamResult::EXAM_ID, '=', $id)->pluck('exam_id')->toArray();
        $listCreatedExam = ExamResult::whereDate(ExamResult::CREATED_BY, '<=', Carbon::now())->pluck('created_at')->toArray();
        $deleted = false;
        $delete_false = array();

        if(count($listExamsResult) > 0) {
            $delete_false['message'] = trans('messages.education_exam_not_delete');
        } else if (count($listCreatedExam) > 0) {
            $delete_false['message'] = trans('messages.education_exam_not_delete_time');
        } else {
            $deleted = $this->repository->delete($id);
        }
        if (request()->wantsJson()) {

            return response()->json([
                'message' => trans('messages.delete_success'),
                'deleted' => $deleted,
                'delete_false' => $delete_false,
            ]);
        }

        return redirect()->back()->with('message', trans('messages.delete_success'));
    }

    public function create() {
        if (Gate::denies('exam.create')) {
            abort(505, trans('messages.you_do_not_have_permission'));
        }
        $listLevel   = Question::$listLevel;
        $user = new User();
        $listTeacher = $user->getListUserByRole('is-teacher')->pluck('fullname', 'id');
        $listSubject = Subject::pluck('name', 'id')->toArray();
        $listTypeExam = Exam::$typeExam;
        return view($this->view . '.create', compact('listLevel', 'listSubject', 'listTeacher', 'listTypeExam'));
    }

    public function putExam($id, Request $request) {
        $userId = \Auth::user()->id;
        $check  = ExamResult::where('exam_id', $id)->where('created_by', $userId)->first();
        if ($check) {
            abort(505, trans('messages.you_had_done_this'));
        }
        $listAnswer = Answer::whereIn('id', $request->input('answer'))
            ->where('is_correct', ONE)
            ->pluck('id')
            ->toArray();
        $listCorrect = array_intersect($request->input('answer'), $listAnswer);
        // $listWrong   = array_diff($request->input('answer'), $listAnswer);
        $point               = count($listCorrect) / count($request->input('answer'));
        $point               = number_format($point, 1, '.', '') * EDUCATION_EXAM_WIDTH_POINT;
        $input['exam_id']    = $id;
        $input['point']      = $point;
        $input['answers']    = implode(',', $request->input('answer'));
        $input['created_by'] = $userId;
        $obj                 = ExamResult::create($input);
        $response            = [
            'status' => false,
        ];
        if ($obj) {
            $response = [
                'message' => trans('messages.sent_success'),
                'data'    => $obj->toArray(),
                'status'  => true,
            ];
        }

        return redirect()->route('exam.index')->with('message', $response['message']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function getDetail($id, Request $request) {
        $data = $this->repository->find($id);
        if (request()->wantsJson()) {

            return response()->json([
                'data' => $data,
            ]);
        }
        
        $listQuestion = Question::whereIn('id', explode(',', $data->questions))->get();
        $obj          = ExamResult::where('created_by', $request->input('user_id'))
            ->where('exam_id', $id)
            ->orderBy('id', 'DESC')
            ->first();

        if (!$obj) {
            return redirect()->back()->with('message', trans('messages.data_is_empty'));
        }
        $arrQuestion = $listQuestion->pluck('id')->toArray();
        // $listAnswer  = Answer::whereIn('question_id', $arrQuestion)
        //     ->pluck('id')
        //     ->toArray();
        $listAnswerCorrect = Answer::whereIn('question_id', $arrQuestion)->where('is_correct', ONE)
            ->pluck('id')
            ->toArray();
        // $listAnswerWrong = Answer::whereIn('question_id', $arrQuestion)->where('is_correct', '!=', ONE)
        //     ->pluck('id')
        //     ->toArray();
        $hisAnswer   = explode(',', $obj->answers);
        $listCorrect = array_intersect($hisAnswer, $listAnswerCorrect);
        // $listWrong      = array_diff($hisAnswer, $listAnswerCorrect);
        // $percentWrong   = count($listWrong) / count($listAnswerWrong) * 100;
        $percentCorrect = round(count($listCorrect) / count($listAnswerCorrect) * 100);
        // $percentWrong   = number_format($percentWrong, 1, '.', '');
        $percentWrong   = 100 - $percentCorrect;
        $percentCorrect = number_format($percentCorrect, 0, '.', '');
        $listUser       = User::paginate(10);
        $listUser->withPath($id . '?user_id=' . $request->input('user_id'));
        $arrUser = $listUser->pluck('id')
            ->toArray();
        $listExamResult = ExamResult::where('exam_id', $id)
            ->whereIn('created_by', $arrUser)
            ->pluck('point', 'created_by')
            ->toArray();

        return view($this->view . '.detail', compact(
            'data',
            'listQuestion',
            'obj',
            'percentWrong',
            'percentCorrect',
            'listUser',
            'listExamResult'
        ));
    }
        /**
     * Export file excel for exam.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function export($id)
    {
        $results = DB::table('education_user_reg_program')
                    ->join('education_subjects', 'education_user_reg_program.subject_id', '=', 'education_subjects.id')
                    ->join('education_exams', 'education_user_reg_program.subject_id', '=', 'education_exams.subject_id')
                    ->join('users', 'education_user_reg_program.user_id', '=', 'users.id')
                    ->select('education_exams.id as exam_id', 'education_exams.name as exam_name', 'education_subjects.name as subject_name', 'education_exams.start_time as exam_start_time', 'education_exams.finish_time as exam_finish_time', 'users.fullname as user_fullname', 'users.username as username', 'users.email as email')
                    ->where('education_exams.id', '=', $id)->get()->toArray();
        $data = array();
        foreach ($results as $result) {
            $data[] = (array)$result;
        }
        if($data !== []) {
            \Excel::create('Danh sách kỳ thi', function ($excel) use ($data) {
                $excel->sheet('Danh sách kỳ thi', function ($sheet) use ($data) {
                    // $sheet->fromArray($data);
                    if (!empty($data)) {
                        foreach ($data as $key => $value) {
                            $i = $key + 7;
                            $number = $key + 1;

                            $sheet->cell('C2', $value['subject_name']);
                            $sheet->cell('C3', $value['exam_name']);
                            $sheet->cell('B' . $i, $number);
                            $sheet->cell('E3', $value['exam_finish_time']);
                            $sheet->cell('E4', $value['exam_start_time']);
                            $sheet->cell('C4', $value['exam_id']);
                            $sheet->cell('C' . $i, $value['email']);
                            $sheet->cell('D' . $i, $value['user_fullname']);
                        }
                        $sheet->cell('B2', function ($cell) {
                            $cell->setValue('Chương trình');
                        });
                        $sheet->cell('B3', function ($cell) {
                            $cell->setValue('Tên bài kiểm tra');
                        });
                        $sheet->cell('D4', function ($cell) {
                            $cell->setValue('Thời gian kết thúc');
                        });
                        $sheet->cell('D3', function ($cell) {
                            $cell->setValue('Thời gian bắt đầu');
                        });
                        $sheet->cell('B4', function ($cell) {
                            $cell->setValue('Mã đề');
                        });
                        $sheet->cell('B6', function ($cell) {
                            $cell->setValue('STT');
                        });
                        $sheet->cell('C6', function ($cell) {
                            $cell->setValue('Email học viên');
                        });
                        $sheet->cell('D6', function ($cell) {
                            $cell->setValue('Tên học viên');
                        });
                        $sheet->cell('E6', function ($cell) {
                            $cell->setValue('Điểm số');
                        });
                        $sheet->getStyle('B2')->applyFromArray(array(
                            'font' => array(
                                'bold' => true,
                            )
                        ));
                        $sheet->getStyle('B3')->applyFromArray(array(
                            'font' => array(
                                'bold' => true,
                            )
                        ));
                        $sheet->getStyle('B4')->applyFromArray(array(
                            'font' => array(
                                'bold' => true,
                            )
                        ));
                        $sheet->getStyle('D3')->applyFromArray(array(
                            'font' => array(
                                'bold' => true,
                            )
                        ));
                        $sheet->getStyle('D4')->applyFromArray(array(
                            'font' => array(
                                'bold' => true,
                            )
                        ));
                        $sheet->getStyle('B6')->applyFromArray(array(
                            'font' => array(
                                'bold' => true,
                            )
                        ));
                        $sheet->getStyle('C6')->applyFromArray(array(
                            'font' => array(
                                'bold' => true,
                            )
                        ));
                        $sheet->getStyle('D6')->applyFromArray(array(
                            'font' => array(
                                'bold' => true,
                            )
                        ));
                        $sheet->getStyle('E6')->applyFromArray(array(
                            'font' => array(
                                'bold' => true,
                            )
                        ));
                        $sheet->setStyle(array(
                            'font' => array(
                                'name' => 'Times New Roman',
                                'size' => 12,
                                'horizontal' => 'center'
                            )
                        ));
                    }
                });
            })->download('xls');
        } else {
            return redirect()->back()->with('error', trans('messages.you_had_register_exam'));
        }
    }

    /**
     * Import file excel for exam.
     *
     * @param
     *
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        if (Gate::denies('exam.import.point')) {
            return response()->json([
                'status'  => false,
                'message' => trans('messages.you_do_not_have_permission'),
            ]);
        }
        if ($request->hasFile('sample_file')) {
            $allowed = array('xls, xlsx');
            $path = $_FILES['sample_file']['name'];
            $ext = pathinfo($path, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
                return response()->json([
                    'message' => trans('messages.import_file_extension'),
                ]);
            } else {
                $path = $request->file('sample_file')->getRealPath();
            }
            $data = [];
            \Excel::load($path, function ($reader) use (&$data) {

                $objExcel = $reader->getExcel();
                $sheet = $objExcel->getSheet(0);

                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                // Loop through each row of the worksheet in turn

                for ($row = 1; $row <= $highestRow; $row++) {
                    //  Read a row of data into an array
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                        NULL, TRUE, FALSE);

                    $data[] = $rowData[0];
                }
            });
            $exam_id = $data[3][2];
            $listUserSuccess = array();
            $listUserFalse   = array();
            $data_total = 0;
            // check bai thi ton tai khong
            $exam = Exam::find($exam_id);
            if(!$exam){
                return response()->json([
                    'status'  => false,
                    'message' => trans('messages.import_file_not_idExam'),
                ]);
            }
            for ($i = 6; $i < count($data); $i++) {
                $data_total ++;
                $data_sheet = $data[$i];
                $created_by = $data_sheet[2];
                $point      = $data_sheet[4] ? $data_sheet[4] : null;
                if(!$point){
                    $listUserFalse[$i]['email'] = $created_by;
                    $listUserFalse[$i]['message'] = trans('messages.import_file_not_point');
                    continue;
                }
                // check user exit
                $user = User::where('email', $created_by)->first();
                if(!$user){
                    $listUserFalse[$i]['email'] = $created_by;
                    $listUserFalse[$i]['message'] = trans('messages.import_file_not_have_user');
                    continue;
                }
                //check user có đăng ký chương trình hay không
                $checkUserBySubject = UserRegProgram::where(UserRegProgram::USER_ID, $user->id)
                                ->where(UserRegProgram::SUBJECT_ID, $exam->subject_id)->get();
                if(!$checkUserBySubject){
                    $listUserFalse[$i]['email'] = $created_by;
                    $listUserFalse[$i]['message'] = trans('messages.import_file_not_reg');
                    continue;
                }
                //check user by exam
                $checkUserExamResult = ExamResult::where(ExamResult::CREATED_BY, $user->id)->where(ExamResult::EXAM_ID, $exam_id)->count();
                if($checkUserExamResult > 0 ){
                    $listUserFalse[$i]['email'] = $created_by;
                    $listUserFalse[$i]['message'] = trans('messages.import_file_existed');
                    continue;
                }
                if(!$exam || !$user || !$checkUserBySubject) {
                    $listUserFalse[$i]['message'] = trans('messages.import_file_not_structure');
                    continue;
                }
                //import file into examResult
                if ( $checkUserExamResult == 0 ) {
                    $arr = array(
                        'exam_id'    => $exam_id,
                        'point'      => $point,
                        'created_by' => $user->id,
                    );
                    ExamResult::create($arr);
                    $listUserSuccess[$i]['email'] = $created_by;
                }
            }
            return response()->json([
                'status'        => true,
                'message'       => trans('messages.import_success'),
                'data_false'    => $listUserFalse,
                'data_total'    => $data_total,
            ]);
        }
    }
}