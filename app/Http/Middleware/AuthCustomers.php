<?php

namespace App\Http\Middleware;

use App\Models\Customers\Accounts;
use App\Services\ResponseService;
use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Illuminate\Support\Facades\Config;
class AuthCustomers extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = new ResponseService();
        if (!$token = $this->auth->setRequest($request)->getToken()) {
            return $response->json(CODE_UNAUTHORIZED, '', 'token not provided');
        }
        try {
            $user = Auth::guard('api')->user();
        } catch (TokenExpiredException $e) {
            return $response->json(CODE_UNAUTHORIZED, '', 'token xpire');
        } catch (JWTException $e) {
            return $response->json(CODE_UNAUTHORIZED, $e->getMessage(), 'Exception');
        }

        if (!$user) {
            return $response->json(CODE_NOT_FOUND, '', 'user not found');
        }
        return $next($request);



    }
}
