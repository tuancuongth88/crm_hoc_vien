<?php

namespace App\Http\Requests\Customers;

use App\Models\Customers\Histories;
use Illuminate\Foundation\Http\FormRequest;

class HistoriesCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
//            Histories::PHONE,
//            Histories::NOTE,
        ];
    }

    public function messages() {
//        return [
//            'required' => trans('validation.required'),
//            'max:191'  => trans('validation.max.string'),
//        ];
    }

    public function attributes() {
//        return Histories::getFieldVietnamese();
    }
}
