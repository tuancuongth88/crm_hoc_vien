<?php

namespace App\Http\Requests\Customers;

use App\Models\Customers\CustomerSharingUser;
use Illuminate\Foundation\Http\FormRequest;

class CustomerSharingUserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            CustomerSharingUser::NUMBER_SHARE => 'required|numeric',
            CustomerSharingUser::USER_ID => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => trans('validation.required'),
            'max:191' => trans('validation.max.string'),
        ];
    }

    public function attributes()
    {
        return CustomerSharingUser::getFieldVietnamese();
    }
}
