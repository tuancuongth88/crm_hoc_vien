<?php

namespace App\Http\Requests\Customers;

use App\Models\Customers\Customer;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CustomerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Customer::LAST_NAME => 'required',
            Customer::FIRST_NAME => 'required',
            Customer::PHONE => 'required|unique:customers,phone,'. $this->id,
            Customer::CITY_ID => 'required',
            Customer::DISTRICT_ID => 'required',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required' => trans('validation.required'),
            'max:191'  => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return Customer::getFieldVietnamese();
    }
}
