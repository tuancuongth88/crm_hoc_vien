<?php

namespace App\Http\Requests\AppHelpDesk;

use App\Models\Customers\Accounts;
use Illuminate\Foundation\Http\FormRequest;

class AccountsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Accounts::FULLNAME => 'required',
            Accounts::PHONE => 'digits_between:9,12|numeric|regex:/^[0-9]+$/|required|unique:accounts',
//            Accounts::EMAIL => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required' => trans('validation.required'),
            'max:191'  => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return Accounts::getFieldVietnamese();
    }
}
