<?php

namespace App\Http\Requests\Systems;

use App\Models\Systems\Company;
use Illuminate\Foundation\Http\FormRequest;

class CompanyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Company::NAME       => 'required|max:250',
            Company::TAX  => 'required',
            Company::PHONE=> 'required',
            Company::ADDRESS=> 'required',
        ];
    }

    public function messages() {
        return [
            'required'       => trans('validation.required'),
            'max:191'        => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return Company::getFieldVietnamese();
    }
}
