<?php

namespace App\Http\Requests\Systems;

use App\Models\Systems\Department;
use Illuminate\Foundation\Http\FormRequest;

class DepartmentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Department::NAME       => 'required|max:250',
            Department::BRANCH_ID  => 'required|numeric',
            Department::COMPANY_ID => 'required|numeric',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required'       => trans('validation.required'),
            'numeric'       => trans('validation.numeric'),
            'max:191'        => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return Department::getFieldVietnamese();
    }
}
