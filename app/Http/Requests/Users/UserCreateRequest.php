<?php

namespace App\Http\Requests\Users;

use App\Models\Users\User;
use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            User::EMAIL         => 'required|email|max:255|unique:' . User::TABLE,
            User::FULLNAME      => 'required|max:60',
            User::PHONE         => 'min:8|max:15|regex:/^[0-9]+$/',
            User::GENDER        => 'required',
            User::DEPARTMENT_ID => 'required',
            User::BIRTHDAY      => 'date|date_format:"d-m-Y"',
            User::POSITION_ID   => 'required',
            User::BRANCH_ID     => 'required',
            User::PASSWORD      => 'min:6|required|confirmed',
            User::BRANCH_ID     => 'required',
            User::IDENTITY      => 'digits_between:9,12|numeric',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required'       => trans('validation.required'),
            'max:191'        => trans('validation.max.string'),
            'confirmed'      => trans('validation.confirmed'),
            'date_format'    => trans('validation.date_format'),
            'regex'          => trans('validation.regex'),
            'digits_between' => trans('validation.digits_between'),
        ];
    }

    public function attributes() {
        return User::getFieldVietnamese();
    }
}