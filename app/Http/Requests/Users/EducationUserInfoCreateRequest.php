<?php

namespace App\Http\Requests\Users;

use App\Models\Users\EducationUserInfo;
use Illuminate\Foundation\Http\FormRequest;

class EducationUserInfoCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            EducationUserInfo::HOBBY => 'required',
            EducationUserInfo::TARGET => 'required',
            EducationUserInfo::TARGET => 'required',
            EducationUserInfo::ADVANTAGE => 'required',
            EducationUserInfo::DEFECT => 'required',
            EducationUserInfo::MARRIAGE => 'required',
            EducationUserInfo::IDENTITY_DATE => 'required',
            EducationUserInfo::IDENTITY_PLACE_RELEASE => 'required',
            EducationUserInfo::SKYPE => 'required',
            EducationUserInfo::ZALO => 'required',
            EducationUserInfo::VIBER => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'required' => trans('validation.required'),
            'max:191' => trans('validation.max.string'),
        ];
    }

    public function attributes()
    {
        return EducationUserInfo::getFieldVietnamese();
    }
}
