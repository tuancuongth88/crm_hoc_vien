<?php

namespace App\Http\Requests\Users;

use App\Models\Users\User;
use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            User::PHONE => 'digits_between:9,12|numeric|regex:/^[0-9]+$/',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'numeric'        => trans('validation.numeric'),
            'regex'          => trans('validation.regex'),
            'digits_between' => trans('validation.digits_between'),
        ];
    }

    public function attributes() {
        return User::getFieldVietnamese();
    }
}
