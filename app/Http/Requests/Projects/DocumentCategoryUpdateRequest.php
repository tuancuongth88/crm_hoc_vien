<?php

namespace App\Http\Requests\Projects;

use App\Models\Projects\DocumentCategory;
use Illuminate\Foundation\Http\FormRequest;

class DocumentCategoryUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            DocumentCategory::NAME => 'required|max:191',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required' => trans('validation.required'),
            'max:191'  => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return DocumentCategory::getFieldVietnamese();
    }
}
