<?php

namespace App\Http\Requests\Projects;

use App\Models\Projects\News;
use Illuminate\Foundation\Http\FormRequest;

class NewsUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            News::TITLE       => 'required|max:191',
            News::DESCRIPTION => 'required|max:250',
            News::IS_COMMENT  => 'numeric',
            News::CATEGORY_ID => 'required|numeric',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required'    => trans('validation.required'),
            'max:191'     => trans('validation.max.string'),
            'date'        => trans('validation.date'),
            'date_format' => trans('validation.date_format'),
            'numeric'     => trans('validation.numeric'),
        ];
    }

    public function attributes() {
        return News::getFieldVietnamese();
    }
}
