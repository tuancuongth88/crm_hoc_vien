<?php

namespace App\Http\Requests\Projects;

use App\Models\Projects\ProjectManager;
use Illuminate\Foundation\Http\FormRequest;

class ProjectManagerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ProjectManager::NAME   => 'required|max:191',
        ];
    }
}
