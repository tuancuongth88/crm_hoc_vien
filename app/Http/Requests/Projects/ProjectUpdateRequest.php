<?php

namespace App\Http\Requests\Projects;

use App\Models\Projects\Project;
use Illuminate\Foundation\Http\FormRequest;

class ProjectUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            Project::NAME    => 'required|max:191',
            // Project::CITY_ID     => 'numeric',
            // Project::DISTRICT_ID => 'numeric',
            Project::ADDRESS => 'required|max:191',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'numeric'  => trans('validation.numeric'),
            'required' => trans('validation.required'),
            'max:191'  => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return Project::getFieldVietnamese();
    }
}
