<?php

namespace App\Http\Requests\Educations;

use App\Models\Education\Program;
use Illuminate\Foundation\Http\FormRequest;

class ProgramUpdateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            Program::NAME      => 'required|max:191',
            Program::IMAGE_URL => 'dimensions:max_width=100',
            Program::ICON_URL  => 'dimensions:max_width=100',
            Program::DESCRIPTION => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required' => trans('validation.required'),
            'max:191'  => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return Program::getFieldVietnamese();
    }
}
