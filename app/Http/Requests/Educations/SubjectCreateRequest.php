<?php

namespace App\Http\Requests\Educations;

use App\Models\Education\Subject;
use Illuminate\Foundation\Http\FormRequest;

class SubjectCreateRequest extends FormRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            Subject::NAME       => 'required|max:191',
            Subject::PROGRAM_ID => 'required',
            Subject::OPEN_TIME  => 'required',
            Subject::CLOSE_TIME => 'required',
            Subject::SHORT_NAME => 'required',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages() {
        return [
            'required' => trans('validation.required'),
            'max:191'  => trans('validation.max.string'),
        ];
    }

    public function attributes() {
        return Subject::getFieldVietnamese();
    }


}
