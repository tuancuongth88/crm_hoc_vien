<?php

namespace App\Presenters\Systems;

use App\Transformers\Systems\DepartmentTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class DepartmentPresenter.
 *
 * @package namespace App\Presenters\Systems;
 */
class DepartmentPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new DepartmentTransformer();
    }
}
