<?php

namespace App\Presenters\Systems;

use App\Transformers\Systems\PositionTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class PositionPresenter.
 *
 * @package namespace App\Presenters\Systems;
 */
class PositionPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new PositionTransformer();
    }
}
