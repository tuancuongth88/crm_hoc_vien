<?php

namespace App\Presenters\Jobs;

use App\Transformers\Jobs\TargetTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TargetPresenter.
 *
 * @package namespace App\Presenters\Jobs;
 */
class TargetPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TargetTransformer();
    }
}
