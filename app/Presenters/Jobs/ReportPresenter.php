<?php

namespace App\Presenters\Jobs;

use App\Transformers\Jobs\ReportTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ReportPresenter.
 *
 * @package namespace App\Presenters\Jobs;
 */
class ReportPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ReportTransformer();
    }
}
