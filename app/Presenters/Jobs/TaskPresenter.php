<?php

namespace App\Presenters\Jobs;

use App\Transformers\Jobs\TaskTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TaskPresenter.
 *
 * @package namespace App\Presenters\Jobs;
 */
class TaskPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TaskTransformer();
    }
}
