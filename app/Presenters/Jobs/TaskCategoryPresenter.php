<?php

namespace App\Presenters\Jobs;

use App\Transformers\Jobs\TaskCategoryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TaskCategoryPresenter.
 *
 * @package namespace App\Presenters\Jobs;
 */
class TaskCategoryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TaskCategoryTransformer();
    }
}
