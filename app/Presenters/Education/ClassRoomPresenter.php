<?php

namespace App\Presenters\Education;

use App\Transformers\Education\ClassRoomTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ClassRoomPresenter.
 *
 * @package namespace App\Presenters\Education;
 */
class ClassRoomPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ClassRoomTransformer();
    }


}
