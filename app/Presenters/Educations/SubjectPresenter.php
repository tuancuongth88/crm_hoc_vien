<?php

namespace App\Presenters\Educations;

use App\Transformers\Educations\SubjectTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class SubjectPresenter.
 *
 * @package namespace App\Presenters\Educations;
 */
class SubjectPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new SubjectTransformer();
    }
}
