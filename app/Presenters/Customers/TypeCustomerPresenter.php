<?php

namespace App\Presenters\Customers;

use App\Transformers\Customers\TypeCustomerTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TypeCustomerPresenter.
 *
 * @package namespace App\Presenters\Customers;
 */
class TypeCustomerPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TypeCustomerTransformer();
    }
}
