<?php

namespace App\Presenters\Customers;

use App\Transformers\Customers\AccountsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class AccountsPresenter.
 *
 * @package namespace App\Presenters\Customers;
 */
class AccountsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new AccountsTransformer();
    }
}
