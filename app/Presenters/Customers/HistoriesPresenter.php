<?php

namespace App\Presenters\Customers;

use App\Transformers\Customers\HistoriesTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class HistoriesPresenter.
 *
 * @package namespace App\Presenters\Customers;
 */
class HistoriesPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new HistoriesTransformer();
    }
}
