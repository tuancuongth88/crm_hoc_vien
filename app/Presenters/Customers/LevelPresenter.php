<?php

namespace App\Presenters\Customers;

use App\Transformers\Customers\LevelTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class LevelPresenter.
 *
 * @package namespace App\Presenters\Customers;
 */
class LevelPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new LevelTransformer();
    }
}
