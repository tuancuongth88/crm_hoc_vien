<?php

namespace App\Presenters\Projects;

use App\Transformers\Projects\TypeTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class TypePresenter.
 *
 * @package namespace App\Presenters\Projects;
 */
class TypePresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new TypeTransformer();
    }
}
