<?php

namespace App\Presenters\Projects;

use App\Transformers\Projects\NewsTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class NewsPresenter.
 *
 * @package namespace App\Presenters\Projects;
 */
class NewsPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new NewsTransformer();
    }
}
