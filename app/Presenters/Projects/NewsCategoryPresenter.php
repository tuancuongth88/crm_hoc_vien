<?php

namespace App\Presenters\Projects;

use App\Transformers\Projects\NewsCategoryTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class NewsCategoryPresenter.
 *
 * @package namespace App\Presenters\Projects;
 */
class NewsCategoryPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new NewsCategoryTransformer();
    }
}
