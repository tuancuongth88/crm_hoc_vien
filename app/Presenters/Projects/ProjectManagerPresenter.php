<?php

namespace App\Presenters;

use App\Transformers\ProjectManagerTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

/**
 * Class ProjectManagerPresenter.
 *
 * @package namespace App\Presenters;
 */
class ProjectManagerPresenter extends FractalPresenter
{
    /**
     * Transformer
     *
     * @return \League\Fractal\TransformerAbstract
     */
    public function getTransformer()
    {
        return new ProjectManagerTransformer();
    }
}
