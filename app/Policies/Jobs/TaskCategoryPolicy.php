<?php

namespace App\Policies\Jobs;

use App\Models\Jobs\TaskCategory;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskCategoryPolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the task category.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Jobs\TaskCategory  $taskCategory
     * @return mixed
     */
    public function view(User $user, TaskCategory $taskCategory = null) {
        if (\Auth::user()->can('task-category.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create task categories.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user) {
        if (\Auth::user()->can('task-category.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the task category.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Jobs\TaskCategory  $taskCategory
     * @return mixed
     */
    public function update(User $user, TaskCategory $taskCategory = null) {
        if (\Auth::user()->can('task-category.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the task category.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Jobs\TaskCategory  $taskCategory
     * @return mixed
     */
    public function delete(User $user, TaskCategory $taskCategory = null) {
        if (\Auth::user()->can('task-category.delete')) {
            return true;
        }
        return false;
    }
}
