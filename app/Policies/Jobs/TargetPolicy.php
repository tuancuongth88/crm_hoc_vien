<?php

namespace App\Policies\Jobs;

use App\Models\Jobs\Target;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TargetPolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the target.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Jobs\Target  $target
     * @return mixed
     */
    public function view(User $user, Target $target = null) {
        if (\Auth::user()->can('target.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create targets.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user) {
        if (\Auth::user()->can('target.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the target.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Jobs\Target  $target
     * @return mixed
     */
    public function update(User $user, Target $target = null) {
        if (\Auth::user()->can('target.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the target.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Jobs\Target  $target
     * @return mixed
     */
    public function delete(User $user, Target $target = null) {
        if (\Auth::user()->can('target.delete')) {
            return true;
        }
        return false;
    }
}
