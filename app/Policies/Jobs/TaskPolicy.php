<?php

namespace App\Policies\Jobs;

use App\Models\Jobs\Task;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TaskPolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the task.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Jobs\Task  $task
     * @return mixed
     */
    public function view(User $user, Task $task) {
        //
    }

    /**
     * Determine whether the user can create tasks.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user) {
        //
    }

    /**
     * Determine whether the user can update the task.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Jobs\Task  $task
     * @return mixed
     */
    public function update(User $user, Task $task) {
        //
    }

    /**
     * Determine whether the user can delete the task.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Jobs\Task  $task
     * @return mixed
     */
    public function delete(User $user, Task $task) {
        //
    }

    public function rate(User $user, Report $report = null) {
        if (\Auth::user()->can('task.rate')) {
            return true;
        }
        return false;
    }
}
