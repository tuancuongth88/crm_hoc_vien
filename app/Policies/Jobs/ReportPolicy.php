<?php

namespace App\Policies\Jobs;

use App\Models\Jobs\Report;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReportPolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the report.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Report  $report
     * @return mixed
     */
    public function list(User $user, Report $report = null) {
        if (\Auth::user()->can('report.list')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can view the report.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Report  $report
     * @return mixed
     */
    public function view(User $user, Report $report = null) {
        if (\Auth::user()->can('report.view') && $user->id === $report->created_by) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create reports.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user) {
        if (\Auth::user()->can('report.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the report.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Report  $report
     * @return mixed
     */
    public function update(User $user, Report $report = null) {
        // if (\Auth::user()->can('report.update')) {
        if (\Auth::user()->can('report.update') && $user->id === $report->created_by) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the report.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Report  $report
     * @return mixed
     */
    public function delete(User $user, Report $report = null) {
        if (\Auth::user()->can('report.delete') && $user->id === $report->created_by) {
            return true;
        }
        return false;
    }

    public function approve(User $user, Report $report = null) {
        if (\Auth::user()->can('report.approve')) {
            return true;
        }
        return false;
    }
}
