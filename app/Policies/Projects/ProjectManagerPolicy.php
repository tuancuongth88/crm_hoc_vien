<?php

namespace App\Policies\Projects;

use App\Models\Projects\ProjectManager;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectManagerPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the project manager.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\App\Models\Projects\ProjectManager  $projectManager
     * @return mixed
     */
    public function view(User $user, ProjectManager $projectManager = null)
    {
        if (\Auth::user()->can('project-manager.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create project managers.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('project-manager.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the project manager.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\App\Models\Projects\ProjectManager  $projectManager
     * @return mixed
     */
    public function update(User $user, ProjectManager $projectManager  = null)
    {
        if (\Auth::user()->can('project-manager.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the project manager.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\App\Models\Projects\ProjectManager  $projectManager
     * @return mixed
     */
    public function delete(User $user, ProjectManager $projectManager  = null)
    {
        if (\Auth::user()->can('project-manager.delete')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can restore the project manager.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\App\Models\Projects\ProjectManager  $projectManager
     * @return mixed
     */
    public function restore(User $user, ProjectManager $projectManager  = null)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the project manager.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\App\Models\Projects\ProjectManager  $projectManager
     * @return mixed
     */
    public function forceDelete(User $user, ProjectManager $projectManager = null)
    {
        //
    }
}
