<?php

namespace App\Policies\Projects;

use App\Models\Projects\Project;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProjectPolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the project.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Projects\Project  $project
     * @return mixed
     */
    public function view(User $user, Project $project = null) {
        if (\Auth::user()->can('project.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create projects.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user) {
        if (\Auth::user()->can('project.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the project.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Projects\Project  $project
     * @return mixed
     */
    public function update(User $user, Project $project = null) {
        if (\Auth::user()->can('project.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the project.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Projects\Project  $project
     * @return mixed
     */
    public function delete(User $user, Project $project = null) {
        if (\Auth::user()->can('project.delete')) {
            return true;
        }
        return false;
    }
}
