<?php

namespace App\Policies\Projects;

use App\Models\Projects\NewsCategory;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsCategoryPolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the news category policy.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Projects\NewsCategoryPolicy  $newsCategoryPolicy
     * @return mixed
     */
    public function view(User $user, NewsCategoryPolicy $newsCategoryPolicy = null) {
        if (\Auth::user()->can('news-category.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create news category policies.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user) {
        if (\Auth::user()->can('news-category.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the news category policy.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Projects\NewsCategoryPolicy  $newsCategoryPolicy
     * @return mixed
     */
    public function update(User $user, NewsCategoryPolicy $newsCategoryPolicy = null) {
        if (\Auth::user()->can('news-category.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the news category policy.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Projects\NewsCategoryPolicy  $newsCategoryPolicy
     * @return mixed
     */
    public function delete(User $user, NewsCategoryPolicy $newsCategoryPolicy = null) {
        if (\Auth::user()->can('news-category.delete')) {
            return true;
        }
        return false;
    }
}
