<?php

namespace App\Policies\Projects;

use App\Models\Projects\News;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the news.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\News  $news
     * @return mixed
     */
    public function view(User $user, News $news = null) {
        if (\Auth::user()->can('news.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create news.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user) {
        if (\Auth::user()->can('news.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the news.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\News  $news
     * @return mixed
     */
    public function update(User $user, News $news = null) {
        if (\Auth::user()->can('news.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the news.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\News  $news
     * @return mixed
     */
    public function delete(User $user, News $news = null) {
        if (\Auth::user()->can('news.delete')) {
            return true;
        }
        return false;
    }

    public function approve(User $user, News $news = null) {
        if (\Auth::user()->can('news.approve')) {
            return true;
        }
        return false;
        // return $user->id === $news->created_by;
    }
}
