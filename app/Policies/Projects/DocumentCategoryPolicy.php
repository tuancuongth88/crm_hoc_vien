<?php

namespace App\Policies\Projects;

use App\Models\Projects\DocumentCategory;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class DocumentCategoryPolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the document category.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Projects\DocumentCategory  $documentCategory
     * @return mixed
     */
    public function view(User $user, DocumentCategory $documentCategory = null) {
        if (\Auth::user()->can('document-category.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create document categories.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user) {
        if (\Auth::user()->can('document-category.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the document category.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Projects\DocumentCategory  $documentCategory
     * @return mixed
     */
    public function update(User $user, DocumentCategory $documentCategory = null) {
        if (\Auth::user()->can('document-category.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the document category.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Projects\DocumentCategory  $documentCategory
     * @return mixed
     */
    public function delete(User $user, DocumentCategory $documentCategory = null) {
        if (\Auth::user()->can('document-category.delete')) {
            return true;
        }
        return false;
    }
}
