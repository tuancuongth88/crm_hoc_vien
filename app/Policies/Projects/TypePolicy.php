<?php

namespace App\Policies\Projects;

use App\Models\Projects\Type;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TypePolicy {
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the type.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Type  $type
     * @return mixed
     */
    public function view(User $user) {
        if (\Auth::user()->can('type.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create types.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user) {
        if (\Auth::user()->can('type.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the type.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Type  $type
     * @return mixed
     */
    public function update(User $user, Type $type = null) {
        if (\Auth::user()->can('type.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the type.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Type  $type
     * @return mixed
     */
    public function delete(User $user, Type $type = null) {
        if (\Auth::user()->can('type.delete')) {
            return true;
        }
        return false;
    }
}
