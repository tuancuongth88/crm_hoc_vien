<?php

namespace App\Policies\Educations;

use App\Models\Users\User;
use App\Models\Education\Lesson;
use Illuminate\Auth\Access\HandlesAuthorization;

class LessonPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the lesson.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\App\Models\Education\Lesson  $lesson
     * @return mixed
     */
    public function view(User $user, Lesson $lesson = null)
    {
        if (\Auth::user()->can('lesson.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create lessons.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('lesson.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the lesson.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\App\Models\Education\Lesson  $lesson
     * @return mixed
     */
    public function update(User $user, Lesson $lesson = null)
    {
        if (\Auth::user()->can('lesson.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the lesson.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\App\Models\Education\Lesson  $lesson
     * @return mixed
     */
    public function delete(User $user, Lesson $lesson = null)
    {
        if (\Auth::user()->can('lesson.delete')) {
            return true;
        }
        return false;
    }
}
