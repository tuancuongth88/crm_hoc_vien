<?php

namespace App\Policies\Educations;

use App\Models\Users\User;
use App\Models\Education\Exam;
use Illuminate\Auth\Access\HandlesAuthorization;

class ExamPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the exam.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Education\Exam  $exam
     * @return mixed
     */
    public function view(User $user, Exam $exam = null)
    {
        if (\Auth::user()->can('exam.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create exams.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('exam.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the exam.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Education\Exam  $exam
     * @return mixed
     */
    public function update(User $user, Exam $exam = null)
    {
        if (\Auth::user()->can('exam.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the exam.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Education\Exam  $exam
     * @return mixed
     */
    public function delete(User $user, Exam $exam = null)
    {
        if (\Auth::user()->can('exam.delete')) {
            return true;
        }
        return false;
    }

    public function importPoint(User $user, Exam $exam = null)
    {
        if (\Auth::user()->can('exam.import.point')) {
            return true;
        }
        return false;
    }
}
