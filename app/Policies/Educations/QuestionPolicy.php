<?php

namespace App\Policies\Educations;

use App\Models\Users\User;
use App\Models\Education\Question;
use Illuminate\Auth\Access\HandlesAuthorization;

class QuestionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the question.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Education\Question  $question
     * @return mixed
     */
    public function view(User $user, Question $question = null)
    {
        if (\Auth::user()->can('question.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create questions.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('question.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the question.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Education\Question  $question
     * @return mixed
     */
    public function update(User $user, Question $question = null)
    {
        if (\Auth::user()->can('question.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the question.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Models\Education\Question  $question
     * @return mixed
     */
    public function delete(User $user, Question $question = null)
    {
        if (\Auth::user()->can('question.delete')) {
            return true;
        }
        return false;
    }
}
