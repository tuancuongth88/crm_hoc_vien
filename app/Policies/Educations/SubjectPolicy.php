<?php

namespace App\Policies\Educations;

use App\Models\Education\Subject;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SubjectPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the subject.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Subject  $subject
     * @return mixed
     */
    public function view(User $user, Subject $subject = null)
    {
        if (\Auth::user()->can('subject.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create subjects.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('subject.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the subject.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Subject  $subject
     * @return mixed
     */
    public function update(User $user, Subject $subject = null)
    {
        if (\Auth::user()->can('subject.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the subject.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Subject  $subject
     * @return mixed
     */
    public function delete(User $user, Subject $subject = null)
    {
        if (\Auth::user()->can('subject.delete')) {
            return true;
        }
        return false;
    }

    public function getListUserRegBySubject(User $user, Subject $subject = null){
        if (\Auth::user()->can('subject.user.reg')) {
            return true;
        }
        return false;
    }

    public function setAssessment(User $user, Subject $subject = null){
        if (\Auth::user()->can('subject.user.assessment')) {
            return true;
        }
        return false;
    }

    public function getListLessonBySubject(User $user, Subject $subject = null){
        if (\Auth::user()->can('subject.view-lesson')) {
            return true;
        }
        return false;
    }

    public function addLessonToSubject(User $user, Subject $subject = null){
        if (\Auth::user()->can('subject.add-lesson-to-subject')) {
            return true;
        }
        return false;
    }

    public function removeLessonToSubject(User $user, Subject $subject = null){
        if (\Auth::user()->can('subject.remove-lesson-to-subject')) {
            return true;
        }
        return false;
    }
}
