<?php

namespace App\Policies\Educations;

use App\Models\Education\Program;
use App\Models\Users\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProgramPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the program.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Program  $program
     * @return mixed
     */
    public function view(User $user, Program $program = null)
    {
        if (\Auth::user()->can('program.view')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can create programs.
     *
     * @param  \App\Models\Users\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        if (\Auth::user()->can('program.create')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can update the program.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Program  $program
     * @return mixed
     */
    public function update(User $user, Program $program = null)
    {
        if (\Auth::user()->can('program.update')) {
            return true;
        }
        return false;
    }

    /**
     * Determine whether the user can delete the program.
     *
     * @param  \App\Models\Users\User  $user
     * @param  \App\Program  $program
     * @return mixed
     */
    public function delete(User $user, Program $program = null)
    {
        if (\Auth::user()->can('program.delete')) {
            return true;
        }
        return false;
    }
}
