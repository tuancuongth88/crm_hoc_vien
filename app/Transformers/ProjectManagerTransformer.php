<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Models\ProjectManager;

/**
 * Class ProjectManagerTransformer.
 *
 * @package namespace App\Transformers;
 */
class ProjectManagerTransformer extends TransformerAbstract
{
    /**
     * Transform the ProjectManager entity.
     *
     * @param \App\Models\ProjectManager $model
     *
     * @return array
     */
    public function transform(ProjectManager $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
