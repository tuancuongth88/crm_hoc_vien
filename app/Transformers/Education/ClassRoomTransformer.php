<?php

namespace App\Transformers\Education;

use League\Fractal\TransformerAbstract;
use App\Models\Education\ClassRoom;

/**
 * Class ClassRoomTransformer.
 *
 * @package namespace App\Transformers\Education;
 */
class ClassRoomTransformer extends TransformerAbstract
{
    /**
     * Transform the ClassRoom entity.
     *
     * @param \App\Models\Education\ClassRoom $model
     *
     * @return array
     */
    public function transform(ClassRoom $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
