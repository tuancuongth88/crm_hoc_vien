<?php

namespace App\Transformers\Jobs;

use League\Fractal\TransformerAbstract;
use App\Models\Jobs\Target;

/**
 * Class TargetTransformer.
 *
 * @package namespace App\Transformers\Jobs;
 */
class TargetTransformer extends TransformerAbstract
{
    /**
     * Transform the Target entity.
     *
     * @param \App\Models\Jobs\Target $model
     *
     * @return array
     */
    public function transform(Target $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
