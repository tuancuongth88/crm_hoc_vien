<?php

namespace App\Transformers\Jobs;

use League\Fractal\TransformerAbstract;
use App\Models\Jobs\Task;

/**
 * Class TaskTransformer.
 *
 * @package namespace App\Transformers\Jobs;
 */
class TaskTransformer extends TransformerAbstract
{
    /**
     * Transform the Task entity.
     *
     * @param \App\Models\Jobs\Task $model
     *
     * @return array
     */
    public function transform(Task $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
