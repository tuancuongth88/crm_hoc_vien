<?php

namespace App\Transformers\Jobs;

use League\Fractal\TransformerAbstract;
use App\Models\Jobs\Report;

/**
 * Class ReportTransformer.
 *
 * @package namespace App\Transformers\Jobs;
 */
class ReportTransformer extends TransformerAbstract
{
    /**
     * Transform the Report entity.
     *
     * @param \App\Models\Jobs\Report $model
     *
     * @return array
     */
    public function transform(Report $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
