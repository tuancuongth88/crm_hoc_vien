<?php

namespace App\Transformers\Jobs;

use League\Fractal\TransformerAbstract;
use App\Models\Jobs\TaskCategory;

/**
 * Class TaskCategoryTransformer.
 *
 * @package namespace App\Transformers\Jobs;
 */
class TaskCategoryTransformer extends TransformerAbstract
{
    /**
     * Transform the TaskCategory entity.
     *
     * @param \App\Models\Jobs\TaskCategory $model
     *
     * @return array
     */
    public function transform(TaskCategory $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
