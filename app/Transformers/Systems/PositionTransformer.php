<?php

namespace App\Transformers\Systems;

use League\Fractal\TransformerAbstract;
use App\Models\Systems\Position;

/**
 * Class PositionTransformer.
 *
 * @package namespace App\Transformers\Systems;
 */
class PositionTransformer extends TransformerAbstract
{
    /**
     * Transform the Position entity.
     *
     * @param \App\Models\Systems\Position $model
     *
     * @return array
     */
    public function transform(Position $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
