<?php

namespace App\Transformers\Systems;

use League\Fractal\TransformerAbstract;
use App\Models\Systems\Company;

/**
 * Class CompanyTransformer.
 *
 * @package namespace App\Transformers\Systems;
 */
class CompanyTransformer extends TransformerAbstract
{
    /**
     * Transform the Company entity.
     *
     * @param \App\Models\Systems\Company $model
     *
     * @return array
     */
    public function transform(Company $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
