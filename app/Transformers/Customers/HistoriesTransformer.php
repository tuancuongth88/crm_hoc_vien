<?php

namespace App\Transformers\Customers;

use League\Fractal\TransformerAbstract;
use App\Models\Customers\Histories;

/**
 * Class HistoriesTransformer.
 *
 * @package namespace App\Transformers\Customers;
 */
class HistoriesTransformer extends TransformerAbstract
{
    /**
     * Transform the Histories entity.
     *
     * @param \App\Models\Customers\Histories $model
     *
     * @return array
     */
    public function transform(Histories $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
