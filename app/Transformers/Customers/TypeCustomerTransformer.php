<?php

namespace App\Transformers\Customers;

use League\Fractal\TransformerAbstract;
use App\Models\Customers\TypeCustomer;

/**
 * Class TypeCustomerTransformer.
 *
 * @package namespace App\Transformers\Customers;
 */
class TypeCustomerTransformer extends TransformerAbstract
{
    /**
     * Transform the TypeCustomer entity.
     *
     * @param \App\Models\Customers\TypeCustomer $model
     *
     * @return array
     */
    public function transform(TypeCustomer $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
