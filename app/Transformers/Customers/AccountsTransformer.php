<?php

namespace App\Transformers\Customers;

use League\Fractal\TransformerAbstract;
use App\Models\Customers\Accounts;

/**
 * Class AccountsTransformer.
 *
 * @package namespace App\Transformers\Customers;
 */
class AccountsTransformer extends TransformerAbstract
{
    /**
     * Transform the Accounts entity.
     *
     * @param \App\Models\Customers\Accounts $model
     *
     * @return array
     */
    public function transform(Accounts $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
