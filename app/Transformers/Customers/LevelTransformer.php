<?php

namespace App\Transformers\Customers;

use League\Fractal\TransformerAbstract;
use App\Models\Customers\Level;

/**
 * Class LevelTransformer.
 *
 * @package namespace App\Transformers\Customers;
 */
class LevelTransformer extends TransformerAbstract
{
    /**
     * Transform the Level entity.
     *
     * @param \App\Models\Customers\Level $model
     *
     * @return array
     */
    public function transform(Level $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
