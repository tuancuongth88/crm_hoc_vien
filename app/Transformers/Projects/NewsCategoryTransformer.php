<?php

namespace App\Transformers\Projects;

use League\Fractal\TransformerAbstract;
use App\Models\Projects\NewsCategory;

/**
 * Class NewsCategoryTransformer.
 *
 * @package namespace App\Transformers\Projects;
 */
class NewsCategoryTransformer extends TransformerAbstract
{
    /**
     * Transform the NewsCategory entity.
     *
     * @param \App\Models\Projects\NewsCategory $model
     *
     * @return array
     */
    public function transform(NewsCategory $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
