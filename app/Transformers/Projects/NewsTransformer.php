<?php

namespace App\Transformers\Projects;

use League\Fractal\TransformerAbstract;
use App\Models\Projects\News;

/**
 * Class NewsTransformer.
 *
 * @package namespace App\Transformers\Projects;
 */
class NewsTransformer extends TransformerAbstract
{
    /**
     * Transform the News entity.
     *
     * @param \App\Models\Projects\News $model
     *
     * @return array
     */
    public function transform(News $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
