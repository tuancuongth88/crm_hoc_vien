<?php

namespace App\Transformers\Projects;

use League\Fractal\TransformerAbstract;
use App\Models\Projects\Project;

/**
 * Class ProjectTransformer.
 *
 * @package namespace App\Transformers\Projects;
 */
class ProjectTransformer extends TransformerAbstract
{
    /**
     * Transform the Project entity.
     *
     * @param \App\Models\Projects\Project $model
     *
     * @return array
     */
    public function transform(Project $model)
    {
        return [
            'id'         => (int) $model->id,

            /* place your other model properties here */

            'created_at' => $model->created_at,
            'updated_at' => $model->updated_at
        ];
    }
}
