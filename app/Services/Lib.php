<?php
/**
 * Created by PhpStorm.
 * User: tuancuong
 * Date: 1/30/2019
 * Time: 2:56 PM
 */

namespace App\Services;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use TechAPI\Api\SendBrandnameOtp;
use TechAPI\Exception as TechException;
use TechAPI\Auth\AccessToken;
use Maatwebsite\Excel\Facades\Excel;
class Lib
{
    public static function sendSMSOTP($phone, $content, $branchName = 'HAIPHATLAND'){
        $arrMessage = array(
            'Phone'      => $phone,
            'BrandName'  => $branchName,
            'Message'    => $content
        );
        // Khởi tạo đối tượng API với các tham số phía trên.
        $apiSendBrandname = new SendBrandnameOtp($arrMessage);
        try
        {
            // Lấy đối tượng Authorization để thực thi API
            $oGrantType      = getTechAuthorization();
            // Thực thi API
            $arrResponse     = $oGrantType->execute($apiSendBrandname);
            // kiểm tra kết quả trả về có lỗi hay không
            if (! empty($arrResponse['error']))
            {
                // Xóa cache access token khi có lỗi xảy ra từ phía server
                AccessToken::getInstance()->clear();

                $result['sms_code'] = $arrResponse['error'];
                $result['sms_message'] =$arrResponse['error_description'];
                return $result;
            }
            $result['sms_code'] = Response::HTTP_OK;
            $result['sms_message'] = json_encode($arrResponse) ;
            return $result;
        }
        catch (\Exception $ex)
        {
            $result['sms_code'] = $ex->getCode();
            $result['sms_message'] = $ex->getMessage();
            return $result;
        }
    }

    public static function exportExcel($title, $nameFile, $nameSheet, $arrTitle, $data, $arrayWith = array()){
        Excel::create($nameFile, function ($excel) use ($title, $nameSheet, $arrTitle, $data, $arrayWith) {
            $excel->sheet($nameSheet, function ($sheet) use ($title, $arrTitle, $data, $arrayWith) {
                $sheet->row(1,  [$title]);
                $sheet->row(1, function($row) {
                    $row->setFont([
                        'family'     => 'Calibri',
                        'size'       => '16',
                        'bold'       =>  true
                    ]);
                });
                $sheet->row(3, $arrTitle);
                $sheet->row(3, function($row) {
                    $row->setBackground('#23b7e5');
                });

                if($arrayWith){
                    foreach ($arrayWith as $key => $value){
                        $sheet->setWidth($key, $value);
                    }
                }
                $sheet->fromArray($data, null, 'A4', null, false);
            });
        })->download('xlsx');
    }
}