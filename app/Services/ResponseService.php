<?php
namespace App\Services;

class ResponseService {

    public function json($status, $data, $message) {
        return response()->json(
            [
                'status'  => $status,
                'data'    => $data,
                'message' => $message,
            ]
        );
    }
    public function api_json($status, $data, $message, $code) {
        return response()->json(
            [
                'status'  => $status,
                'data'    => $data,
                'message' => $message,
                'code'    => $code
            ]
        );
    }
}