$(document).ready(function() {
    $('#send_at').datetimepicker({
        todayHighlight: true,
        autoclose: true,
        format: 'dd-mm-yyyy hh:ii',
        todayBtn: true,
    });
    $('#summernote_edit').summernote({
        callbacks: {
            onImageUpload: function(image) {
                uploadImage(image[0]);
            }
        },
        height: 350,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['table', ['table']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['picture', ['picture']]
        ]
    });
    $("#author").select2({
        placeholder: "Tìm kiếm tài khoản",
        allowClear: true,
        ajax: {
            url: "/administrator/system/user",
            dataType: 'json',
            delay: 250,
            data: function(params) {
                return {
                    search: params.term, // search term
                    page: params.page
                };
            },
            processResults: function(response, params) {
                // parse the results into the format expected by Select2
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data, except to indicate that infinite
                // scrolling can be used
                params.page = params.page || 1;

                return {
                    results: response.data.data,
                    pagination: {
                        more: (params.page * 30) < response.total_count
                    }
                };
            },
            cache: true
        },
        escapeMarkup: function(markup) {
            return markup;
        }, // let our custom formatter work
        minimumInputLength: 1,
        templateResult: formatRepo, // omitted for brevity, see the source of this page
        templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
    });

    $('#description').change(counter);
    $('#description').keydown(counter);
    $('#description').keypress(counter);
    $('#description').keyup(counter);
    $('#description').blur(counter);
    $('#description').focus(counter);

    $("#create-news").validate({
        highlight: function (input) {
            $(input).parents('.form-group').addClass('has-danger');
        },
        unhighlight: function (input) {
            $(input).parents('.form-group').removeClass('has-danger');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error.addClass('form-message').addClass('validation-error'));
        },
        rules: {
            title: {
                required: true,
                maxlength: 191
            },
            description: {
                required: true,
                maxlength: 250
            },
            summernote: {
                required: true,
            },
            // author: {
            //     required: true,
            // },
            // send_at: {
            //     required: true,
            //     date: true
            // },
            // title_meta: {
            //     maxlength: 191
            // },
            // description_meta: {
            //     maxlength: 191
            // },
            // keyword_meta: {
            //     maxlength: 191
            // }
        },
    });
});
counter = function() {
    var value = $('#description').val();
    if (value.length == 0) {
        $('#totalChars').html(0);
        return;
    }
    var totalChars = value.length;
    $('#totalChars').html(totalChars);
};