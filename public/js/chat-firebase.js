var config = {
    apiKey: "AIzaSyBE4HsdHgrM9ZLOO_5kxEgtcloiYGpZZ-4",
    authDomain: "crm-land.firebaseapp.com",
    databaseURL: "https://crm-land.firebaseio.com",
    projectId: "crm-land",
    storageBucket: "crm-land.appspot.com",
    messagingSenderId: "505543396837"
};
firebase.initializeApp(config);


var database = firebase.database().ref('/accounts');
var html = '';
var accountSection = $('.inbox_chat');
database.orderByChild("last_send_time").on("child_added", function(data) {
    loadAccount(data);
});

database.orderByChild("last_send_time").on("child_changed", function(data) {
    html = '';
    notifyMe(data.val().fullname, "Vừa nhắn tin");
    database.orderByChild("last_send_time").on("child_added", function(data) {
        loadAccount(data);
    });

});

var phone_number, avatar, id , phoneOld;
var db_ref,listener = null;
$('.inbox_chat').on('click', '.chat_list', function() {
    clearContentChat();
    phoneOld = this.phone_number;
    phone_number  =  $(this).attr('data-phone');
    avatar =    $(this).attr('data-img');
    id     =    $(this).attr('data-id')
    $('.chat_list').removeClass('active_chat');
    $(this).addClass('active_chat');
    getData(phone_number);
});

function loadAccount(data) {
    var result = data.val();
    var avatar = '/no-avatar.ico';
    var totalMsgNotView = '';
    if(result.avatar != null){
        avatar = result.avatar;
    }
    if(result.count_notification > 0){
        totalMsgNotView = result.count_notification;
    }
    var activeSection = '';
    if(result.phone == phone_number){
        activeSection = 'active_chat';
    }

    html += '<div class="chat_list '+activeSection+'" data-phone="'+result.phone+'" data-id="'+result.id+'" data-img="'+avatar+'" style="cursor: pointer;">\n' +
        '                        <div class="chat_people" >\n' +
        '                            <div class="chat_img">\n' +
        '                                <div class="m-card-profile__pic">\n' +
        '                                    <div class="m-card-profile__pic-wrapper" >\n' +
        '                                            <img src="'+avatar+'" alt="" id="avatar"/>\n' +
        '                                    </div>\n' +
        '                                </div>\n' +
        '                            </div>\n' +
        '                            <div class="chat_ib">\n' +
        '                                <h5>'+result.fullname+' <span class="chat_date">' ;
    if(result.count_notification > 0) {
        html += '<a href="#" class="btn btn-outline-danger m-btn btn-sm  m-btn--icon m-btn--pill">';
        html += '<span><i class="la la-comments"></i><span>' + totalMsgNotView + '</span></span></a>';
    }
    html +='                         </span></h5>\n' +
        '                                <p>'+result.phone+'</p>\n' +
        '                            </div>\n' +
        '                        </div>\n' +
        '                    </div>';
    accountSection.html(html);
}
function getData(phone_number) {
    var key = {};
    //hien thi noi dung chat
    var html = '';
    if(this.db_ref != null){
        this.db_ref.off('child_added',this.listener);
    }
    db_ref = firebase.database().ref('messages/'+phone_number);
     listener = db_ref.on('child_added', function(data) {
        var result = data.val();
        //check dung dung tai khoan dang o do khong
            clearContentChat();
            var date = new Date(result.timestamp * 1000);
                var dateFormart = js_yyyy_mm_dd_hh_mm_ss(date);
                if(result.type == "admin"){
                    html += '<div class="outgoing_msg">';
                    html += '<div class="sent_msg">'
                    html += result.fullname+'<p>'+ result.text +'</p>';
                    html += '<span class="time_date"> '+dateFormart+'</span> </div></div>';
                }
                else{
                    html += '<div class="incoming_msg">';
                    html += '<div class="incoming_msg_img">';
                    html += '<img src="'+avatar+'" alt="sunil"> </div>'
                    html += '<div class="received_msg">';
                    html += '<div class="received_withd_msg">';
                    html += result.fullname+'<p>'+result.text;
                    html += '<span class="time_date">'+dateFormart+'</span></div></div></div>';
                    key[data.key] = {'id': data.key};
                    updateCountNotification();
                    updateStatusRead(data.key);
                }
                $('.msg_history').append(html)
                $('.write_msg').val("");
                $(".msg_history").animate({ scrollTop: $(".msg_history")[0].scrollHeight }, 0);
    });
}

function updateCountNotification() {
    var db_ref2 = firebase.database().ref('accounts/'+id);
    var updates = {};
    if(db_ref2){
        updates['count_notification'] = 0;
        db_ref2.update(updates);
    }

}
function updateStatusRead(key){
        firebase.database().ref('messages/'+phone_number+'/'+key).update({notviewed: true});
}
clearContentChat();
function clearContentChat() {
    $('.msg_history').empty();
}

function newMessage() {
    var message = $(".write_msg").val();
    if($.trim(message) == '') {
        return false;
    }
    if(phone_number != null){
        writeUserData(message, phone_number);
    }else{
        swal({
            "title": "Thông báo!",
            text: "Bạn phải chọn tài khoản khách hàng để nhắn tin",
            type: 'error',
            showCancelButton: false,
            confirmButtonText: 'Đóng'
        });
    }

};


$('.msg_send_btn').click(function() {
    newMessage();
});
$(window).on('keydown', function(e) {
    if (e.which == 13) {
        newMessage();
        return false;
    }
});

function writeUserData(message) {
    var db_ref = firebase.database().ref('messages/'+phone_number);
    var db_ref2 = firebase.database().ref('accounts/'+id);
    var fullnameAdmin = $('#admin_full_name').val();
    db_ref.push({
        fullname: fullnameAdmin,
        type: "admin",
        text: message,
        phone: phone_number,
        notviewed: false,
        timestamp: Math.floor(Date.now() / 1000),
    });
    var updates = {};
    updates['count_notification'] = 0;
    updates['last_send_time'] = 0 - Math.floor(Date.now() / 1000);
    db_ref2.update(updates);
}

function js_yyyy_mm_dd_hh_mm_ss (now) {
    year = "" + now.getFullYear();
    month = "" + (now.getMonth() + 1); if (month.length == 1) { month = "0" + month; }
    day = "" + now.getDate(); if (day.length == 1) { day = "0" + day; }
    hour = "" + now.getHours(); if (hour.length == 1) { hour = "0" + hour; }
    minute = "" + now.getMinutes(); if (minute.length == 1) { minute = "0" + minute; }
    second = "" + now.getSeconds(); if (second.length == 1) { second = "0" + second; }
    return year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
}

function notifyMe(title, content) {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification(title, {
            icon: location.origin+"/logo.png",
            body: content,
        });

        notification.onclick = function () {
            window.open(location.hostname+"/system-app/chat");
        };

    }
}



