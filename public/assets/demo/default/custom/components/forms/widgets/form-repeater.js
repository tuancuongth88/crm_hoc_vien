//== Class definition
var FormRepeater = function () {

    //== Private functions
    var demo1 = function () {
        $('#m_repeater_1').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };

    var demo2 = function () {
        $('#m_repeater_2').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });
    };


    var demo3 = function () {
        $('#m_repeater_3').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                if (confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });
    };

    var demo4 = function () {
        $('#m_repeater_4').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };

    var demo5 = function () {
        $('#m_repeater_5').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };

    var demo6 = function () {
        $('#m_repeater_6').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };

    var demo7 = function () {
        $('.m_repeater_7').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                name_from = $(this).find('.literacy_param_from').attr('name');
                name_to = $(this).find('.literacy_param_to').attr('name');
                initDate("input[name='" + name_from + "']");
                initDate("input[name='" + name_to + "']");
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };
    var demo8 = function () {
        $('.m_repeater_8').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                name_from = $(this).find('.experienced_param_from').attr('name');
                name_to = $(this).find('.experienced_param_to').attr('name');
                initDate("input[name='" + name_from + "']");
                initDate("input[name='" + name_to + "']");
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };

    var repeater_customer_sharing = function () {
        $('.repeater_customer_sharing').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                initGetUserByRole();
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    };

    return {
        // public functions
        init: function () {
            demo1();
            demo2();
            demo3();
            demo4();
            demo5();
            demo6();
            demo7();
            demo8();
            repeater_customer_sharing();
        }
    };
}();

jQuery(document).ready(function () {
    FormRepeater.init();
});

    