@extends('administrator.app')
@section('title','Mục tiêu')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Mục tiêu
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('administrator.errors.errors-validate')
    <!-- END: Subheader -->
    <div class="m-content">
        <div style="margin-bottom: 20px">
            <a href="{{ route('target.index') }}" class="btn btn-success">Danh sách mục tiêu</a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Cập nhật mục tiêu
                                </h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => array('target.update', $data->id), 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="type">
                                Công việc
                                </label>
                                <select class="form-control m-input" name="task">
                                    @foreach ($task as $key => $val)
                                        <option value="{{ $key }}" {{ ($key == $data->task) ? 'selected' : '' }}>
                                            {{ $val['name'] }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Mục tiêu
                                </label>
                                <input type="number" class="form-control m-input" name="target" value="{{ $data->target }}">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Ngày
                                </label>
                                <div class="input-group date" >
                                    <input type="text" name="day" class="form-control m-input m_datepicker" readonly required="" value="{{ $data->day }}" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('target.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    {{-- </form> --}}
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.m_datepicker').datepicker({
            todayBtn: "linked",
            clearBtn: true,
            todayHighlight: true,
            autoclose: true,
            format: 'dd-mm-yyyy',
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
    });

</script>
@stop