@extends('administrator.app')
@section('title','Báo cáo công việc')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Báo cáo công việc
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Section-->
            <div class="m-section__content">
                @include('administrator.jobs.report-trial.search')
            </div>
            @include('administrator.errors.info-search')
            <div class="m-section">
                @include('administrator.errors.messages')

                <div class="m-section__content">
                    <table class="table m-table m-table--head-bg-warning">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Tên nhân viên
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Số điện thoại
                            </th>
                            <th>
                                Chức vụ
                            </th>
                            <th>
                                Phòng ban
                            </th>
                            <th>
                                Ngày tạo
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr>
                                <th scope="row">
                                    {{ $key + 1 }}
                                </th>
                                <td>
                                    {{ $value->fullname }}
                                </td>
                                <td>
                                    {{ $value->email }}
                                </td>
                                <td>
                                    {{ $value->phone }}
                                </td>
                                <td>
                                    {{ $value->position->name or 'Không xác định'}}
                                </td>
                                <td>
                                    {{ $value->department->name or 'Không xác định' }}
                                </td>
                                <td>
                                    {{ isset($value->created_at) ? $value->created_at->format('d-m-Y H:i:s') : '' }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->appends([
                'name' => @$input['name'],
                'phone'=> @$input['phone'],
                'email'=> @$input['email']
                ])->links()  }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
            <!--end::Section-->
        </div>
    </div>
    <script>
        jQuery(document).ready(function () {
            $('.m-select2').select2();
            $('#search-datepicker-report').datepicker({
                format: 'm-yyyy',
                viewMode: "months", //this
                minViewMode: "months",//and this
            });

            $('.action-search-report').click(function () {
                var action = $(this).data('action');
                $('#search_form_report').attr('action', action);
                $('#search_form_report').attr('target', '');
                $('#search_form_report').submit();
            });

            $('.action-export-report').click(function () {
                var action = $(this).data('action');
                $('#search_form_report').attr('action', action);
                $('#search_form_report').attr('target', '_blank');
                $('#search_form_report').submit();
            });

        });
    </script>
@stop