{{ Form::open(array('route' => 'report-trial.index', 'method' => 'GET', 'id' =>'search_form_report')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tên nhân viên</label>
        <input type="text" class="form-control m-input" placeholder="Tên nhân viên" name="name"
               value="{{isset($input['name'])? $input['name']:'' }}">
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Email</label>
        <input type="text" class="form-control m-input" placeholder="email" name="email"
               value="{{isset($input['email']) ? $input['email']:'' }}">
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Điện thoại</label>
        <input type="text" class="form-control m-input" placeholder="Số điện thoại" name="phone"
               value="{{isset($input['phone']) ? $input['phone']:'' }}">
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="button" value="Tìm kiếm" class="btn btn-primary form-control action-search-report"
               data-action="{{route('report-trial.index')}}"

        />
    </div>
</div>
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tháng báo cáo</label>
        <input type="text" class="form-control m-input"
               placeholder="Tháng báo cáo" name="month_report"
               value="{{Carbon\Carbon::now()->month.'-'.Carbon\Carbon::now()->year}}"
               id="search-datepicker-report">
    </div>
    <div class="col-md-3 col-sm-6 mb-3 mb-md-0">
        <label>Xem chi tiết và in báo cáo tháng</label>
        <input type="button" class="btn btn-primary form-control action-export-report"
               data-action="{{route('report.export',['type'=>'month'])}}" value="Xem">
    </div>
</div>
</form>
