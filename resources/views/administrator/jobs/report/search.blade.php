{{ Form::open(array('route' => 'report.index', 'method' => 'GET', 'id' =>'search_form_customer')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Nhân viên</label>
        {{ Form::select('user_id',  ['' => 'Lựa chọn'] + $listSale, old('user_id'), ['class' => 'form-control m-select2', 'id' => 'user_id']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Tìm kiếm" class="btn btn-primary form-control"/>
    </div>
</div>
</form>
