@extends('administrator.app')
@section('title','Báo cáo công việc')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Báo cáo công việc tháng {{ date('m-Y') }}
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        <div class="m-section">
            <div class="m-section__content">
                <!--begin:: Widgets/Stats-->
                <div class="m-portlet ">
                    <div class="m-portlet__body  m-portlet__body--no-padding">
                        <div class="row m-row--no-padding m-row--col-separator-xl">
                            @foreach ($taskStability as $idTask => $quantity)
                            <div class="col-md-12 col-lg-6 col-xl-3">
                                <!--begin::Total Profit-->
                                <div class="m-widget24">
                                    <div class="m-widget24__item">
                                        <h4 class="m-widget24__title">
                                            {{ (isset($listTask[$idTask])) ? $listTask[$idTask] : '' }}
                                        </h4>
                                        <br>
                                        <span class="m-widget24__desc">
                                            Số lượng
                                        </span>
                                        <span class="m-widget24__stats m--font-brand">
                                            +{{ $quantity }}
                                        </span>
                                        <div class="m--space-10"></div>
                                        <div class="progress m-progress--sm">
                                            @if (isset($targetMonth[$idTask]))
                                                <div class="progress-bar  {{ (($quantity/$targetMonth[$idTask]) * 100 < 50) ? 'm--bg-danger' : 'm--bg-success'}}" role="progressbar" style="width: {{ ($quantity/$targetMonth[$idTask]) * 100 }}%;" aria-valuenow="{{ ($quantity/$targetMonth[$idTask]) * 100 }}" aria-valuemin="0" aria-valuemax="100"></div>
                                            @endif
                                        </div>
                                        <span class="m-widget24__change">
                                            Mục tiêu
                                        </span>
                                        <span class="m-widget24__number">
                                            {{ isset($targetMonth[$idTask]) ? $targetMonth[$idTask] : '0' }}
                                        </span>
                                    </div>


                                </div>
                                <!--end::Total Profit-->
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Stats-->
                <!--Begin::Portlet-->
                <div class="m-portlet  m-portlet--full-height ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Công việc phát sinh trong tháng
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <b>Báo cáo của : {{ \Auth::user()->fullname }}</b>
                        {{-- <div class="m-scrollable mCustomScrollbar _mCS_5 mCS-autoHide" data-scrollbar-shown="true" data-scrollable="true" data-max-height="380" style="overflow: visible; height: 380px; max-height: 380px; position: relative;">
                            <!--Begin::Timeline 2 -->
                            <div class="m-timeline-2">
                                <div class="m-timeline-2__items  m--padding-top-25 m--padding-bottom-30">
                                    @foreach ($listTaskSuddenly as $taskSuddenly)
                                        <div class="m-timeline-2__item m--margin-top-30">
                                            <span class="m-timeline-2__item-time">
                                                {{ date('d-m', strtotime($taskSuddenly->created_at)) }}
                                            </span>
                                            <div class="m-timeline-2__item-cricle">
                                                <i class="fa fa-genderless m--font-success"></i>
                                            </div>
                                            <div class="m-timeline-2__item-text  m--padding-top-5">
                                                {{ $taskSuddenly->task }} - {{ $taskSuddenly->location }} - {{ $taskSuddenly->duration }}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <!--End::Timeline 2 -->
                        </div> --}}
                        <div class="m_datatable" id="auto_column_hide"></div>
                    </div>
                </div>
                <!--End::Portlet-->
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
    $(document).ready(function() {
        $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/administrator/job/report/task-suddenly/month/1',
                        method: 'GET',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                    },
                },
                pageSize: 10,
                saveState: false,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // column sorting
            sortable: false,

            pagination: false,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#generalSearch'),
            },

            rows: {
                // auto hide columns, if rows overflow
                autoHide: true,
            },

            // columns definition
            columns: [{
                field: 'created_at',
                title: 'Ngày',
            }, {
                field: 'task',
                title: 'Công việc',
                width: 150,
            }, {
                field: 'Latitude',
                title: 'Đánh giá',
                // callback function support for column rendering
                template: function(row) {
                    var status = {
                        'unrate': {
                            'title': 'Chưa đánh giá',
                            'class': 'm-badge--warning'
                        },
                        'good': {
                            'title': 'Tốt',
                            'class': 'm-badge--success'
                        },
                        'medium': {
                            'title': 'Trung bình',
                            'class': ' m-badge--primary'
                        },
                        'weak': {
                            'title': 'Yếu',
                            'class': ' m-badge--danger'
                        },
                    };
                    return '<span class="m-badge ' + status[row.rate].class + ' m-badge--wide">' + status[row.rate].title + '</span>';
                },
            }, {
                field: 'location',
                title: 'Địa điểm',
                width: 350,
            }, {
                field: 'duration',
                title: 'Lượng thời gian',
            }, {
                field: 'Tài liệu đính kèm',
                title: 'Tài liệu đính kèm',
                width: 750,
                // callback function support for column rendering
                template: function(row) {
                    var listFile = '';
                    row.has_files.forEach(function(file) {
                        listFile += '<i class="fa fa-paperclip"></i><a href="' + file.file + '" download="' + file.file + '">' + file.file + '</a><br>';
                        // return 'file.file';
                    });
                    return listFile;
                },
            }],
        });
    });

</script>
@stop