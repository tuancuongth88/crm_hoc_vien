@extends('administrator.app')
@section('title','Báo cáo')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Báo cáo
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--Begin::Main Portlet-->
        <div class="m-portlet m-portlet--full-height">
            <!--begin: Portlet Head-->
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Báo cáo công việc ngày {{ date('d/m/Y') }}
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="#" data-toggle="m-tooltip" class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left" data-width="auto" title="Get help with filling up this form">
                                <i class="flaticon-info m--icon-font-size-lg3"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--end: Portlet Head-->
            <!--begin: Portlet Body-->
            <div class="m-portlet__body m-portlet__body--no-padding">
                <!--begin: Form Wizard-->
                <div class="m-wizard m-wizard--3 m-wizard--success" id="m_wizard">
                    <!--begin: Message container -->
                    <div class="m-portlet__padding-x">
                        <!-- Here you can put a message or alert -->
                    </div>
                    <!--end: Message container -->
                    <div class="row m-row--no-padding">
                        <div class="col-xl-4 col-lg-12">
                            <!--begin: Form Wizard Head -->
                            <div class="m-wizard__head">
                                <!--begin: Form Wizard Progress -->
                                <div class="m-wizard__progress">
                                    <div class="progress">
                                        <div class="progress-bar"  role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Progress -->
                                <!--begin: Form Wizard Nav -->
                                <div class="m-wizard__nav">
                                    <div class="m-wizard__steps">
                                        <div class="m-wizard__step m-wizard__step--current" data-wizard-target="#m_wizard_form_step_1">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                    <span>
                                                        <span>
                                                            1
                                                        </span>
                                                    </span>
                                                </a>
                                                <div class="m-wizard__step-line">
                                                    <span></span>
                                                </div>
                                                <div class="m-wizard__step-label">
                                                    Công việc thường xuyên - Triển khai tại công ty
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_2">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                    <span>
                                                        <span>
                                                            2
                                                        </span>
                                                    </span>
                                                </a>
                                                <div class="m-wizard__step-line">
                                                    <span></span>
                                                </div>
                                                <div class="m-wizard__step-label">
                                                    Công việc bất thường - triển khai ngoài hiện trường
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_3">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                    <span>
                                                        <span>
                                                            3
                                                        </span>
                                                    </span>
                                                </a>
                                                <div class="m-wizard__step-line">
                                                    <span></span>
                                                </div>
                                                <div class="m-wizard__step-label">
                                                    Hoạt động truyền thông công ty
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_4">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                    <span>
                                                        <span>
                                                            4
                                                        </span>
                                                    </span>
                                                </a>
                                                <div class="m-wizard__step-line">
                                                    <span></span>
                                                </div>
                                                <div class="m-wizard__step-label">
                                                    Phát triển nhân sự
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Nav -->
                            </div>
                            <!--end: Form Wizard Head -->
                        </div>
                        <div class="col-xl-8 col-lg-12">
                            <!--begin: Form Wizard Form-->
                            <div class="m-wizard__form">
                                <!--
                                    1) Use m-form--label-align-left class to alight the form input lables to the right
                                    2) Use m-form--state class to highlight input control borders on form validation
                                -->
                                <form class="m-form m-form--label-align-left- m-form--state-" id="m_form" enctype="multipart/form-data">
                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                    <!--begin: Form Body -->
                                    <div class="m-portlet__body m-portlet__body--no-padding">
                                        <!--begin: Form Wizard Step 1-->
                                        <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                            <div class="m-form__section m-form__section--first">
                                                <div class="m-form__heading">
                                                    <h3 class="m-form__heading-title">
                                                        Đăng tin dự án
                                                    </h3>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">
                                                        * Website cá nhân:
                                                    </label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số tin" aria-describedby="basic-addon1" name="post_website">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-5 col-lg-5"><input type="file" name="post_website_file" multiple="" id="post_website" style="padding-top: 7px;"></div>
                                                    <div class="col-xl-12 col-lg-12" style="padding-top:15px">
                                                            <textarea class="form-control m-input m-input--air" name="post_website_detail" rows="5" placeholder="Link tin đăng, mỗi link một dòng"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">
                                                        * Facebook - Fanpage:
                                                    </label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số tin" aria-describedby="basic-addon1" name="post_facebook">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-5 col-lg-5"><input type="file" name="post_facebook_file" multiple="" style="padding-top: 7px;"></div>
                                                    <div class="col-xl-12 col-lg-12" style="padding-top:15px">
                                                            <textarea class="form-control m-input m-input--air" name="post_facebook_detail" rows="5" placeholder="Link tin đăng, mỗi link một dòng"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">
                                                        * Trang tin bất động sản:
                                                    </label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số tin" aria-describedby="basic-addon1" name="post_news">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-5 col-lg-5"><input type="file" name="post_news_file" multiple="" style="padding-top: 7px;"></div>
                                                    <div class="col-xl-12 col-lg-12" style="padding-top:15px">
                                                            <textarea class="form-control m-input m-input--air" rows="5" placeholder="Link tin đăng, mỗi link một dòng" name="post_news_detail"></textarea>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-3 col-lg-3 col-form-label">
                                                        * Forum:
                                                    </label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số tin" aria-describedby="basic-addon1" name="post_forum">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-5 col-lg-5"><input type="file" name="post_forum_file" multiple="" style="padding-top: 7px;"></div>
                                                    <div class="col-xl-12 col-lg-12" style="padding-top:15px">
                                                            <textarea class="form-control m-input m-input--air" rows="5" placeholder="Link tin đăng, mỗi link một dòng" name="post_forum_detail"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                            <div class="m-form__section">
                                                <div class="m-form__heading">
                                                    <h3 class="m-form__heading-title">
                                                        Cập nhật thông tin
                                                    </h3>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">
                                                        * Cập nhật thông tin thị trường:
                                                    </label>
                                                    <div class="col-xl-8 col-lg-8">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lần" aria-describedby="basic-addon1" name="update_info_market">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                     batdongsan.com.vn / cafebiz.vn ...
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 col-form-label">
                                                        * Cập nhật thông tin dự án:
                                                    </label>
                                                    <div class="col-xl-8 col-lg-8">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lần" aria-describedby="basic-addon1" name="update_info_project">
                                                            <div class="input-group-append">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    Email sàn hoặc trao đổi với quản lý
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 1-->
                                        <!--begin: Form Wizard Step 2-->
                                        <div class="m-wizard__form-step" id="m_wizard_form_step_2">
                                            <div class="m-form__section m-form__section--first">
                                                <div class="m-form__heading">
                                                    <h3 class="m-form__heading-title">
                                                        Công việc ngoài hiện trường
                                                    </h3>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Phát tờ rơi cá nhân:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text" id="basic-addon1">
                                                                                                #
                                                                                            </span>
                                                                                        </div>
                                                                                        <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="flypaper_personal">
                                                                                    </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text" id="basic-addon1">
                                                                                                <i class="la la-unlock-alt"></i>
                                                                                            </span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="flypaper_personal_time">
                                                                                    </div>

                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="flypaper_personal_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Phát tờ rơi & treo phướn tập thể:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text" id="basic-addon1">
                                                                                                #
                                                                                            </span>
                                                                                        </div>
                                                                                        <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="flypaper_group">
                                                                                    </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                                                        <div class="input-group-prepend">
                                                                                            <span class="input-group-text" id="basic-addon1">
                                                                                                <i class="la la-unlock-alt"></i>
                                                                                            </span>
                                                                                        </div>
                                                                                        <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="flypaper_group_time">
                                                                                    </div>

                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="flypaper_group_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Thực hiện Questionair tìm kiếm khách hàng tiềm năng cá nhân:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="questionair">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="la la-unlock-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="questionair_time">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="questionair_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Trực dự án và nhà mẫu trong giờ hành chính:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="guard_project_in_hour">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="la la-unlock-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="guard_project_in_hour_time">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="guard_project_in_hour_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Trực dự án và nhà mẫu ngoài giờ hành chính:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="guard_project_out_hour">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="la la-unlock-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="guard_project_out_hour_time">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="guard_project_out_hour_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Tham gia sự kiện mở bán và giới thiệu dự án phân phối:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="open_sale">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="la la-unlock-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="open_sale_time">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="open_sale_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Tham gia các hoạt động Roadshow, quảng cáo :</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="roadshow">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="la la-unlock-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="roadshow_time">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="roadshow_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Tham gia sự kiện, event, hội thảo bên ngoài:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="event">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="la la-unlock-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="event_time">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="event_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Tìm kiếm khách hàng tại các địa điểm công cộng:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="find_public">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="la la-unlock-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="find_public_time">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="find_public_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Thiết lập cuộc hẹn tư vấn khách hàng:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="setup_appointment">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="la la-unlock-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="setup_appointment_time">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="setup_appointment_file" multiple="">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-4 col-lg-4 text-left align-middle col-form-label">Tổng kết, đánh giá công việc:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="total">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    <i class="la la-unlock-alt"></i>
                                                                </span>
                                                            </div>
                                                            <input type="text" class="form-control m-input" placeholder="Thời gian" aria-describedby="basic-addon1" name="total_time">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-12 col-lg-12" style="margin-top:15px">
                                                         <input type="file" name="total_file" multiple="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 2-->
                                        <!--begin: Form Wizard Step 3-->
                                        <div class="m-wizard__form-step" id="m_wizard_form_step_3">
                                            <div class="m-form__section m-form__section--first">
                                                <div class="m-form__heading">
                                                    <h3 class="m-form__heading-title">
                                                        Hoạt động truyền thông công ty
                                                    </h3>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-3 col-lg-3 text-left align-middle col-form-label">Like bài viết:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="like">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-5 col-lg-5">
                                                         <input type="file" name="like_file" multiple="" style="padding-top: 7px;">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-3 col-lg-3 text-left align-middle col-form-label">Comment bài viết:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="comment">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-5 col-lg-5">
                                                         <input type="file" name="comment_file" multiple="" style="padding-top: 7px;">
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed"></div>
                                                <div class="form-group m-form__group row">
                                                    <label class="col-xl-3 col-lg-3 text-left align-middle col-form-label">Share bài viết:</label>
                                                    <div class="col-xl-4 col-lg-4">
                                                        <div class="input-group m-input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text" id="basic-addon1">
                                                                    #
                                                                </span>
                                                            </div>
                                                            <input type="number" class="form-control m-input" placeholder="Số lượng" aria-describedby="basic-addon1" name="share">
                                                        </div>
                                                    </div>
                                                    <div class="col-xl-5 col-lg-5">
                                                         <input type="file" name="share_file" multiple="" style="padding-top: 7px;">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 3-->
                                        <!--begin: Form Wizard Step 4-->
                                        <div class="m-wizard__form-step" id="m_wizard_form_step_4">
                                            <!--begin::Section-->
                                            <div class="m-form__section m-form__section--first">
                                                <div class="m-form__heading">
                                                    <h3 class="m-form__heading-title">
                                                        Phát triển nhân sự
                                                    </h3>
                                                </div>
                                                <div class="form-group m-form__group row">
                                                    <div class="alert m-alert m-alert--default" role="alert" style="font-weight: 100 !important">
                                                        Nếu trong ngày bạn có giới thiệu được nhân sự kinh doanh tham gia hệ thống vui lòng nhập thông tin của nhân sự được bạn giới thiệu xuống bên dưới. (có thể + thêm nếu giới thiệu dược nhiều hơn 1 người), Nếu không hãy bỏ trống mục này
                                                    </div>
                                                    <div id="m_repeater_1">
                                                        <div class="form-group  m-form__group row" id="m_repeater_1">
                                                            <div data-repeater-list="info" class="col-lg-12">
                                                                <div data-repeater-item class="form-group m-form__group row align-items-center">
                                                                    <div class="col-md-9">
                                                                        <div class="m-form__group m-form__group--inline contact-form">
                                                                                <input name="name" type="text" class="form-control" placeholder=" Họ tên" size="50" style="margin-bottom: 13px;">
                                                                                <input name="phone" type="text" class="form-control" placeholder=" Điện thoại" size="50" style="margin-bottom: 13px;">
                                                                                <input name="email" type="text" class="form-control" placeholder=" Email" size="50" style="margin-bottom: 13px;">
                                                                        </div>
                                                                        <div class="d-md-none m--margin-bottom-10"></div>
                                                                    </div>

                                                                    <div class="col-md-3">
                                                                        <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
                                                                            <span>
                                                                                <i class="la la-trash-o"></i>
                                                                                <span>
                                                                                    Xóa
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="m-form__group form-group row button-contact">
                                                            <label class="col-lg-2 col-form-label"></label>
                                                            <div class="col-lg-4">
                                                                <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                                                                    <span>
                                                                        <i class="la la-plus"></i>
                                                                        <span>
                                                                            Thêm mới
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <!--end::Section-->
                                        </div>
                                        <!--end: Form Wizard Step 4-->
                                    </div>
                                    <!--end: Form Body -->
                                    <!--begin: Form Actions -->
                                    <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                                        <div class="m-form__actions">
                                            <div class="row">
                                                <div class="col-lg-6 m--align-left">
                                                    <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                                        <span>
                                                            <i class="la la-arrow-left"></i>
                                                            &nbsp;&nbsp;
                                                            <span>
                                                                Back
                                                            </span>
                                                        </span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-6 m--align-right">
                                                    <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
                                                        <span>
                                                            <i class="la la-check"></i>
                                                            &nbsp;&nbsp;
                                                            <span>
                                                                Submit
                                                            </span>
                                                        </span>
                                                    </a>
                                                    <a href="#" class="btn btn-success m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                                        <span>
                                                            <span>
                                                                Save & Continue
                                                            </span>
                                                            &nbsp;&nbsp;
                                                            <i class="la la-arrow-right"></i>
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Actions -->
                                </form>
                            </div>
                            <!--end: Form Wizard Form-->
                        </div>
                    </div>
                </div>
                <!--end: Form Wizard-->
            </div>
            <!--end: Portlet Body-->
        </div>
        <!--End::Main Portlet-->
    </div>
</div>
<input type="hidden" id="api_file" name="api_file" value="{{ env('API_FILE') }}">
<script type="text/javascript">
    $(document).ready(function() {
        //== Class definition
        var WizardDemo = function () {
            //== Base elements
            var wizardEl = $('#m_wizard');
            var formEl = $('#m_form');
            var validator;
            var wizard;

            //== Private functions
            var initWizard = function () {
                //== Initialize form wizard
                wizard = wizardEl.mWizard({
                    startStep: 1
                });

                //== Validation before going to next page
                wizard.on('beforeNext', function(wizard) {
                    if (validator.form() !== true) {
                        return false;  // don't go to the next step
                    }
                })

                //== Change event
                wizard.on('change', function(wizard) {
                    mApp.scrollTop();
                });
            }

            var initValidation = function() {
                validator = formEl.validate({
                    //== Validate only visible fields
                    ignore: ":hidden",

                    //== Validation rules
                    // rules: {
                    //     //=== Client Information(step 1)
                    //     //== Client details
                    //     name: {
                    //         required: true
                    //     },
                    //     email: {
                    //         required: true,
                    //         email: true
                    //     },
                    //     phone: {
                    //         required: true,
                    //         phoneUS: true
                    //     },

                    //     //== Mailing address
                    //     address1: {
                    //         required: true
                    //     },
                    //     city: {
                    //         required: true
                    //     },
                    //     state: {
                    //         required: true
                    //     },
                    //     city: {
                    //         required: true
                    //     },
                    //     country: {
                    //         required: true
                    //     },

                    //     //=== Client Information(step 2)
                    //     //== Account Details
                    //     account_url: {
                    //         required: true,
                    //         url: true
                    //     },
                    //     account_username: {
                    //         required: true,
                    //         minlength: 4
                    //     },
                    //     account_password: {
                    //         required: true,
                    //         minlength: 6
                    //     },

                    //     //== Client Settings
                    //     account_group: {
                    //          required: true
                    //     },
                    //     'account_communication[]': {
                    //         required: true
                    //     },

                    //     //=== Client Information(step 3)
                    //     //== Billing Information
                    //     billing_card_name: {
                    //         required: true
                    //     },
                    //     billing_card_number: {
                    //         required: true,
                    //         creditcard: true
                    //     },
                    //     billing_card_exp_month: {
                    //         required: true
                    //     },
                    //     billing_card_exp_year: {
                    //         required: true
                    //     },
                    //     billing_card_cvv: {
                    //         required: true,
                    //         minlength: 2,
                    //         maxlength: 3
                    //     },

                    //     //== Billing Address
                    //     billing_address_1: {
                    //         required: true
                    //     },
                    //     billing_address_2: {

                    //     },
                    //     billing_city: {
                    //         required: true
                    //     },
                    //     billing_state: {
                    //         required: true
                    //     },
                    //     billing_zip: {
                    //         required: true,
                    //         number: true
                    //     },
                    //     billing_delivery: {
                    //         required: true
                    //     },

                    //     //=== Confirmation(step 4)
                    //     accept: {
                    //         required: true
                    //     }
                    // },

                    //== Validation messages
                    messages: {
                        'account_communication[]': {
                            required: 'You must select at least one communication option'
                        },
                        accept: {
                            required: "You must accept the Terms and Conditions agreement!"
                        }
                    },

                    //== Display error
                    invalidHandler: function(event, validator) {
                        mApp.scrollTop();

                        swal({
                            "title": "",
                            "text": "There are some errors in your submission. Please correct them.",
                            "type": "error",
                            "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                        });
                    },

                    //== Submit valid form
                    submitHandler: function (form) {

                    }
                });
            }

            var initSubmit = function() {
                var btn = formEl.find('[data-wizard-action="submit"]');

                btn.on('click', function(e) {
                    e.preventDefault();

                    if (validator.form()) {

                        $.ajaxSetup({
                          headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                          }
                        });
                        var data = new FormData(document.getElementById('m_form'));
                        data.append("list_attachment", JSON.stringify(listAttachment));
                        mApp.progress(btn);
                        mApp.block(formEl);
                        $.ajax({
                            url: '/administrator/job/report',
                            cache: false,
                            data: data,
                            type: "post",
                            processData: false,
                            contentType: false,
                            success: function(response) {
                                mApp.unprogress(btn);
                                mApp.unblock(formEl);

                                swal({
                                    "title": "Thông báo!",
                                    text: response.message,
                                    type: 'warning',
                                    showCancelButton: false,
                                    confirmButtonText: 'Xác nhận!'
                                }).then(function(result) {
                                    if (result.value) {
                                        window.location.href = "/administrator/job/report";
                                    }
                                });
                            },
                            error: function(data) {

                            }
                        });
                    }
                });
            }

            return {
                // public functions
                init: function() {
                    wizardEl = $('#m_wizard');
                    formEl = $('#m_form');

                    initWizard();
                    initValidation();
                    initSubmit();
                }
            };
        }();

        jQuery(document).ready(function() {
            WizardDemo.init();
        });
        var listAttachment = [];
        $('input[type=file]').change(function(e){
            var inputName = $(this).attr('name');
            // listinputName] = [];
            $(e.target.files).each(function( index, file ) {
                var data = new FormData();
                data.append("file", file);
                // $.ajaxSetup({
                //     headers: {
                //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //     }
                // });
                $.ajax({
                    url: $('input[name=api_file]').val() + '/api/file',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(response) {
                        response.data.type = inputName;
                        listAttachment.push(response.data);
                    },
                    error: function(data) {

                    }
                });
            });


            return false;

        });
    });
</script>
@stop