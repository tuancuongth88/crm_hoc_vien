@extends('administrator.app')
@section('title','Công việc')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Công việc
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        <div class="col-xs-12" style="margin-bottom: 20px">
            <a href="{{ route('report.create') }}" class="btn btn-primary m-btn m-btn--icon">
                <span>
                    <i class="fa flaticon-plus"></i>
                    <span>
                        Thêm mới
                    </span>
                </span>
            </a>
        </div>
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-calendar-3"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    {{ $report->name }}<br>
                                </h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => array('report.evaluate', $report->id),'enctype' => 'multipart/form-data')) }}
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                    <div class="m-portlet__body">
                        <i class="fa fa-user"></i> Người tạo : {{ $report->getCreatedBy->fullname }}<br>
                        {{ $report->name }}
                        <table class="table m-table m-table--head-bg-warning">
                            <thead>
                                <tr>
                                    <th>Công việc cố định</th>
                                    <th>Số lượng</th>
                                    <th>Mục tiêu</th>
                                    <th>Tỉ lệ</th>
                                    <th width="15%">Đánh giá</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($report->hasStabilityTasks as $stabilityTask)
                                    <tr>
                                        <td>{{ $taskStability[$stabilityTask->task]['name'] }}</td>
                                        <td>{{ $stabilityTask->quantity }}</td>
                                        <td>
                                            @if ($target =  App\Models\Jobs\Target::where('task', $stabilityTask->task)
                                                    ->where('day', \Carbon\Carbon::now()->startOfDay())
                                                    ->first())
                                                {{ $target->target }}
                                            @endif
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{ ($target) ? ($stabilityTask->quantity / $target->target) * 100 : 0 }}%" aria-valuenow="{{ ($target) ? ($stabilityTask->quantity / $target->target) * 100 : 0 }}" aria-valuemin="0" aria-valuemax="100">
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <select class="form-control m-input m-input--solid rate-stability" name="rate" data-id="{{ $stabilityTask->id }}">
                                                @foreach (App\Models\Jobs\Report::$rateLevel as $level => $name)
                                                    <option value="{{ $level }}" {{ ($stabilityTask->rate == $level) ? 'selected' : '' }}>
                                                        {{ $name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <table class="table m-table m-table--head-bg-warning">
                            <thead>
                                <tr>
                                    <th>Công việc phát sinh</th>
                                    <th>Khoảng thời gian</th>
                                    <th>Vị trí</th>
                                    <th>Tài liệu</th>
                                    <th>Đánh giá</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($report->hasSuddenlyTasks as $suddenlyTask)
                                    <tr>
                                        <td>{{ $suddenlyTask->task }}</td>
                                        <td>{{ $suddenlyTask->duration }}</td>
                                        <td>{{ $suddenlyTask->location }}</td>
                                        <td>
                                            @foreach ($suddenlyTask->hasFiles as $file)
                                                <i class="fa fa-paperclip"></i><a href="{{ $file->file }}" download="{{ $file->url }}">{{ $file->file }}</a><br>
                                            @endforeach
                                        </td>
                                        <td>
                                            <select class="form-control m-input m-input--solid rate-suddenly" data-id="{{ $suddenlyTask->id }}">
                                                @foreach (App\Models\Jobs\Report::$rateLevel as $level => $name)
                                                    <option value="{{ $level }}" {{ ($suddenlyTask->rate == $level) ? 'selected' : '' }}>
                                                        {{ $name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="m-portlet__foot">
                        <div class="row align-items-center">

                            @if ($report->evaluate)
                                <div class="col-lg-6">
                                    <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
                                        <div class="m-alert__icon">
                                            <i class="la la-check-circle"></i>
                                        </div>
                                        <div class="m-alert__text">
                                            <strong>
                                                Đánh giá của {{ ($report->updated_by) ? $report->getUpdatedBy->fullname : '' }}:
                                            </strong>
                                            {!! $report->evaluate !!}
                                        </div>
                                    </div>
                                </div>
                            @else
                                <div class="col-lg-12" style="margin-bottom: 20px">
                                    <textarea class="summernote" id="summernote" name="evaluate">{!! $report->evaluate !!}</textarea>
                                </div>
                                <div class="col-lg-6 m--valign-middle">
                                    Đánh giá bản báo cáo này:
                                </div>
                            @endif
                            <div class="col-lg-6 m--align-right">
                                @if (is_null($report->evaluate))
                                <button type="submit" class="btn btn-outline-primary m-btn m-btn--icon">
                                    <span>
                                        <i class="fa fa-send-o"></i>
                                        <span>
                                            Gửi đánh giá
                                        </span>
                                    </span>
                                </button>
                                @endif
                                {{-- <a class="btn btn-outline-success m-btn m-btn--icon" data-id="{{ $report->id }}">
                                    <span>
                                        <i class="fa fa-thumbs-up"></i>
                                        <span id="labelapprove">
                                            {{ ($report->is_approve == ZERO) ? 'Duyệt' : 'Bỏ duyệt' }}
                                        </span>
                                    </span>
                                </a> --}}
                                <span class="m--margin-left-10">
                                    {{-- hoặc --}}
                                    <a href="{{ route('report.index') }}" class="m-link m--font-bold">
                                        Về danh sách
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#summernote').summernote({
            callbacks: {
                onImageUpload: function(image) {
                    uploadImage(image[0]);
                }
            },
            height: 150,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
              ]
        });
        $('.rate-stability').change(function() {
            var id = $(this).attr('data-id');
            var url = '/administrator/job/task/stability-rate/' + id + '?rate=' + $(this).val();
            postRateTask(url);
        });
        $('.rate-suddenly').change(function() {
            var id = $(this).attr('data-id');
            var url = '/administrator/job/task/suddenly-rate/' + id + '?rate=' + $(this).val();
            postRateTask(url);
        });
        $('#approve').click(function() {
            var id = $(this).attr('data-id');

            var url = '/administrator/job/report/approve/' + id;

            postRateTask(url);
        });
    });
    function postRateTask(url) {
        mApp.blockPage({
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            size: 'lg'
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: url,
            contentType: "application/json",
            type: "post",
            success: function(response) {
                if (response.redirect) {
                    window.location.href = response.redirect;
                }
                var type = 'danger';
                if (response.status) {
                    type = 'success';
                }
                var content = {};
                content.message = response.message;
                content.icon = 'icon fa fa-check';
                var notify = $.notify(content, {
                    type: type,
                    allow_dismiss: 'true',
                    timer: '2000',
                    placement: {
                        from: 'bottom',
                        align: 'right'
                    },
                    offset: {
                        x: '30',
                        y: '30'
                    },
                    delay: '1000',
                    z_index: '100000',
                    animate: {
                        enter: 'animated bounceIn',
                        exit: 'animated bounceIn'
                    }
                });
                mApp.unblockPage();
            },
            error: function(data) {

            }
        });
    }
</script>
@stop