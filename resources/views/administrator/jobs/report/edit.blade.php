@extends('administrator.app')
@section('title','Báo cáo')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Báo cáo
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <a href="{{ route('report.index') }}" class="btn btn-success">Danh sách báo cáo</a>
            </div>
            <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Báo cáo công việc ngày {{ date('d/m/Y') }}
                                </h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => array('report.update', $report->id), 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Mô tả
                                </label>
                                <textarea class="form-control m-input" name="name" >{{ $report->name }}</textarea>
                            </div>
                            <div id="m_repeater_4">
                                <div class="form-group m-form__group">
                                    <label>
                                        Công việc cố định:
                                    </label>
                                    <div data-repeater-list="stability" class="">
                                        @if (count($report->hasStabilityTasks) == 0)
                                            <div data-repeater-item class="row m--margin-bottom-10">
                                                <div class="col-lg-5">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-file-text"></i>
                                                            </span>
                                                        </div>
                                                        <select class="form-control m-input" name="task">
                                                            @foreach ($taskStability as $key => $value)
                                                                <option value="{{ $key }}">
                                                                    {{ $value['name'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-indent"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control m-input" placeholder="Số lượng" name="quantity" required="" value="{{ old('title') }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                        <i class="la la-remove"></i>
                                                    </label>
                                                </div>
                                            </div>
                                        @endif
                                        @foreach ($report->hasStabilityTasks as $stabilityTask)
                                            <div data-repeater-item class="row m--margin-bottom-10">
                                                <div class="col-lg-5">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-file-text"></i>
                                                            </span>
                                                        </div>
                                                        <select class="form-control m-input" name="task">
                                                            @foreach ($taskStability as $key => $value)
                                                                <option value="{{ $key }}" {{ ($key == $stabilityTask->task) ? 'selected' : ''}}>
                                                                    {{ $value['name'] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-indent"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control m-input" placeholder="Số lượng" name="quantity" required="" value="{{ $stabilityTask->quantity }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <label data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                        <i class="la la-remove"></i>
                                                    </label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col">
                                        <div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>
                                                    Thêm công việc cố định
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="m_repeater_3">
                                <div class="form-group  m-form__group">
                                    <label>
                                        Công việc phát sinh:
                                    </label>
                                    <div data-repeater-list="suddenly" class="">
                                        @if (count($report->hasSuddenlyTasks) == 0)
                                        <div data-repeater-item class="row m--margin-bottom-10">
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-file-text"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control m-input" placeholder="Tên công việc" name="task" value="{{ old('title') }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-map-marker"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control m-input" placeholder="Địa điểm" name="location" value="{{ old('location') }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-2">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="la la-calendar-check-o"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" class="form-control m-input" placeholder="Khoảng thời gian" name="duration" value="{{ old('duration') }}">
                                                </div>
                                            </div>
                                            <div class="col-lg-3">
                                                <select class="form-control m-input m-input--solid" id="category_id" name="category_id">
                                                    <option value="0">CÔNG VIỆC BẤT THƯỜNG</option>
                                                    @foreach ($category as $element)
                                                        <option value="{{ $element['id'] }}" {{ (old('category_id') == $element['id']) ? 'selected' : '' }}>
                                                            {{ $element['name'] }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-lg-3 m--margin-top-30">
                                                Tài liệu đính kèm
                                                <input type="file" name="attachment" multiple="">
                                                {{-- <div class="custom-file">
                                                <input type="file" class="custom-file-input" name="attachment" multiple="">
                                                    <label class="custom-file-label" for="customFile">
                                                        Tài liệu đính kèm
                                                    </label>
                                                </div> --}}
                                            </div>
                                            <div class="col-lg-1 m--margin-top-30">
                                                <label data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                    <i class="la la-remove"></i>
                                                </label>
                                            </div>
                                        </div>
                                        @endif
                                        @foreach ($report->hasSuddenlyTasks as $suddenlyTask)
                                        <div class="block">
                                            <div data-repeater-item class="row m--margin-bottom-10">
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-file-text"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control m-input" placeholder="Tên công việc" name="task" value="{{ $suddenlyTask->task }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-map-marker"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control m-input" placeholder="Địa điểm" name="location" value="{{ $suddenlyTask->location }}">
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-calendar-check-o"></i>
                                                            </span>
                                                        </div>
                                                        <input type="text" class="form-control m-input" placeholder="Khoảng thời gian" name="duration" value="{{ $suddenlyTask->duration }}">
                                                    </div>
                                                </div>
                                                <input type="hidden" name="task_suddenly_id" value="{{ $suddenlyTask->id }}">
                                                <div class="col-lg-3">
                                                    <select class="form-control m-input m-input--solid" id="category_id" name="category_id">
                                                        <option value="0">CÔNG VIỆC BẤT THƯỜNG</option>
                                                        @foreach ($category as $element)
                                                            <option value="{{ $element['id'] }}" {{ ($suddenlyTask->category_id == $element['id']) ? 'selected' : '' }}>
                                                                {{ $element['name'] }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col-lg-3 m--margin-top-30">
                                                    Tài liệu đính kèm
                                                    <input type="file" name="attachment" multiple="">
                                                    {{-- <div class="custom-file">
                                                    <input type="file" class="custom-file-input" name="attachment" multiple="">
                                                        <label class="custom-file-label" for="customFile">
                                                            Tài liệu đính kèm
                                                        </label>
                                                    </div> --}}
                                                </div>
                                                <div class="col-lg-1 m--margin-top-30">
                                                    <label data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only delete-element">
                                                        <i class="la la-remove"></i>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 m--margin-bottom-10 list-file">
                                                <!--begin::m-widget4-->
                                                <div class="m-widget4">
                                                    @foreach ($suddenlyTask->hasFiles as $file)
                                                    <input type="hidden" name="file_id[]" value="{{ $file->id }}">
                                                    <div class="m-widget4__item">
                                                        <div class="m-widget4__img m-widget4__img--icon">
                                                            @if (in_array($file->extendsion, ['jpg', 'jpeg']))
                                                                <img src="{{ URL::asset('assets/app/media/img/files/jpg.svg') }}" alt="">
                                                            @endif
                                                            @if (in_array($file->extendsion, ['docx', 'doc']))
                                                                <img src="{{ URL::asset('assets/app/media/img/files/doc.svg') }}" alt="">
                                                            @endif
                                                            @if (in_array($file->extendsion, ['pdf']))
                                                                <img src="{{ URL::asset('assets/app/media/img/files/pdf.svg') }}" alt="">
                                                            @endif
                                                            @if (in_array($file->extendsion, ['xls', 'xlsx']))
                                                                <img src="{{ URL::asset('assets/app/media/img/files/xls.png') }}" alt="">
                                                            @endif
                                                        </div>
                                                        <div class="m-widget4__info">
                                                            <span class="m-widget4__text">
                                                                <a href="{{ $file->url }}">{{ $file->file }}</a> (<i class="fa fa-database"></i>{{ formatSizeUnits($file->size) }} KB)
                                                            </span>
                                                            <span class="m-widget4__text">
                                                                <a href="http://view.officeapps.live.com/op/view.aspx?src={!! url($file->url)  !!}" class="m-widget4__icon" target="_blank">
                                                                    <i class="fa fa-eye"></i>
                                                                </a>
                                                            </span>
                                                            <a href="{{ $file->url }}" download="{{ $file->name }}" class="m-widget4__icon">
                                                                <i class="la la-download"></i>
                                                            </a>
                                                        </div>
                                                        <div class="m-widget4__ext">

                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                                <!--end::Widget 9-->
                                            </div>
                                        </div>

                                        @endforeach
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col">
                                        <div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>
                                                    Thêm công việc phát sinh
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('report.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    {{-- </form> --}}
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $( "#m_repeater_3" ).show();
        $( "#m_repeater_4" ).show();
        $('body').on('click', '.delete-element', function() {
            $(this).closest('.block').find('.list-file').remove();
        });
    });
</script>
@stop