@extends('administrator.app')
@section('title','Công việc')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    {{-- <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Công việc
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div> --}}
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        <!--begin:: Widgets/Best Sellers-->
        <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Báo cáo
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget5_tab1_content" role="tab">
                                Công việc thường xuyên
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget5_tab2_content" role="tab">
                                Công việc bất thường
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget5_tab3_content" role="tab">
                                Hoạt động truyền thông công ty
                            </a>
                        </li>
                        <li class="nav-item m-tabs__item">
                            <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget5_tab4_content" role="tab">
                                Phát triển nhân sự
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin::Content-->
                <div class="tab-content">
                    <div class="tab-pane active" id="m_widget5_tab1_content" aria-expanded="true">
                        <!--begin::m-widget5-->
                        <div class="m-widget5">
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Đăng tin website
                                    </h4>
                                    <span class="m-widget5__desc">
                                        {!! $report->post_website_detail !!}
                                    </span>
                                    <div class="m-widget5__info">
                                        @foreach ($report->hasAttachment->where('field', 'post_website_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->post_website }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['post_website']) ? ($target->data['post_website']['number']).'/'.$target->data['post_website']['time']: '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Đăng tin Facebook - Fanpage
                                    </h4>
                                    <span class="m-widget5__desc">
                                        {!! $report->post_facebook_detail !!}
                                    </span>
                                    <div class="m-widget5__info">
                                        @foreach ($report->hasAttachment->where('field', 'post_facebook_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->post_facebook }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['post_facebook']) ? ($target->data['post_facebook']['number']).'/'. $target->data['post_facebook']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Đăng tin bất động sản
                                    </h4>
                                    <span class="m-widget5__desc">
                                        {!! $report->post_news_detail !!}
                                    </span>
                                    <div class="m-widget5__info">
                                        @foreach ($report->hasAttachment->where('field', 'post_news_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->post_news }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['post_news']) ? ($target->data['post_news']['number']).'/'.$target->data['post_news']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Đăng tin Forum
                                    </h4>
                                    <span class="m-widget5__desc">
                                        {!! $report->post_forum_detail !!}
                                    </span>
                                    <div class="m-widget5__info">
                                        @foreach ($report->hasAttachment->where('field', 'post_forum_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->post_forum }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['post_forum']) ? ($target->data['post_forum']['number']).'/'.$target->data['post_forum']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Cập nhật thông tin thị trường
                                    </h4>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->update_info_market }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['update_info_market']) ? ($target->data['update_info_market']['number']).'/'.$target->data['update_info_market']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Cập nhật thông tin dự án
                                    </h4>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->update_info_project }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['update_info_project']) ? ($target->data['update_info_project']['number']).'/'.$target->data['update_info_project']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!--end::m-widget5-->
                    </div>
                    <div class="tab-pane" id="m_widget5_tab2_content" aria-expanded="false">
                        <!--begin::m-widget5-->
                        <div class="m-widget5">
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Phát tờ rơi cá nhân
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->flypaper_personal_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'flypaper_personal_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->flypaper_personal }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['flypaper_personal']) ? ($target->data['flypaper_personal']['number']).'/'.$target->data['flypaper_personal']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Phát tờ rơi & treo phướn tập thể
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->flypaper_group_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'flypaper_group_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->flypaper_group }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['flypaper_group']) ? ($target->data['flypaper_group']['number']).'/'.$target->data['flypaper_group']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Thực hiện Questionair tìm kiếm khách hàng tiềm năng
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->questionair_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'questionair_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->questionair }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['questionair']) ? ($target->data['questionair']['number']).'/'.$target->data['questionair']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Trực dự án và nhà mẫu trong giờ hành chính
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->guard_project_in_hour_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'guard_project_in_hour_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->guard_project_in_hour }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['guard_project_in_hour']) ? ($target->data['guard_project_in_hour']['number']).'/'.$target->data['guard_project_in_hour']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Trực dự án và nhà mẫu trong giờ hành chính
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->guard_project_out_hour_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'guard_project_out_hour_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->guard_project_out_hour }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['guard_project_out_hour']) ? ($target->data['guard_project_out_hour']['number']).'/'.$target->data['guard_project_out_hour']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Tham gia sự kiện mở bán và giới thiệu dự án phân phối
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->open_sale_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'open_sale_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->open_sale }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['open_sale']) ? ($target->data['open_sale']['number']).'/'.$target->data['open_sale']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Tham gia các hoạt động Roadshow, quảng cáo
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->roadshow_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'roadshow_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->roadshow }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['roadshow']) ? ($target->data['roadshow']['number']).'/'.$target->data['roadshow']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Tham gia sự kiện, event, hội thảo bên ngoài
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->event_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'event_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->event }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['event']) ? ($target->data['event']['number']).'/'.$target->data['event']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Tìm kiếm khách hàng tại các địa điểm công cộng
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->find_public_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'find_public_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->find_public }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['find_public']) ? ($target->data['find_public']['number']).'/'.$target->data['find_public']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Thiết lập cuộc hẹn tư vấn khách hàng
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->setup_appointment_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'setup_appointment_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->setup_appointment }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['setup_appointment']) ? ($target->data['setup_appointment']['number']).'/'.$target->data['setup_appointment']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Tổng kết, đánh giá công việc
                                    </h4>
                                    <div class="m-widget5__info">
                                        <span class="m-widget5__info-label">
                                            Thời gian:
                                        </span>
                                        <span class="m-widget5__info-date m--font-info">
                                            {{ $report->total_time }}
                                        </span>
                                        <br>
                                        @foreach ($report->hasAttachment->where('field', 'total_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->total }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        {{ isset($target->data['total']) ? ($target->data['total']['number']).'/'.$target->data['total']['time'] : '' }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!--end::m-widget5-->
                    </div>
                    <div class="tab-pane" id="m_widget5_tab3_content" aria-expanded="false">
                        <!--begin::m-widget5-->
                        <div class="m-widget5">
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Like bài viết
                                    </h4>
                                    <div class="m-widget5__info">
                                        @foreach ($report->hasAttachment->where('field', 'like_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->like }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        ?
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Comment bài viết
                                    </h4>
                                    <div class="m-widget5__info">
                                        @foreach ($report->hasAttachment->where('field', 'comment_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->comment }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        ?
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                            <div class="m-widget5__item">
                                <div class="m-widget5__content">
                                    <h4 class="m-widget5__title">
                                        Share bài viết
                                    </h4>
                                    <div class="m-widget5__info">
                                        @foreach ($report->hasAttachment->where('field', 'share_file') as $file)
                                            <i class="fa fa-paperclip"></i><a target="_blank" href="{{ $file->url }}" download="{{ $file->url }}">{{ $file->name }}</a>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="m-widget5__stats1">
                                    <span class="m-widget5__number">
                                        {{ $report->share }}
                                    </span>
                                    <br>
                                    <span class="m-widget5__sales">
                                        Số lượng
                                    </span>
                                </div>
                                <div class="m-widget5__stats2">
                                    <span class="m-widget5__number">
                                        ?
                                    </span>
                                    <br>
                                    <span class="m-widget5__votes">
                                        Mục tiêu
                                    </span>
                                </div>
                            </div>
                        </div>
                        <!--end::m-widget5-->
                    </div>
                    <div class="tab-pane" id="m_widget5_tab4_content" aria-expanded="false">
                        <!--begin::Widget 14-->
                        <div class="m-widget4">
                            <!--begin::Widget 14 Item-->
                            @foreach ($report->hasInfoManager as $info)
                            <div class="m-widget4__item">
                                <div class="m-widget4__img m-widget4__img--pic">
                                    <img src="/no-avatar.ico" alt="">
                                </div>
                                <div class="m-widget4__info">
                                    <span class="m-widget4__title">
                                        {{ $info->name }}
                                    </span>
                                    <br>
                                    <span class="m-widget4__sub">
                                        {{ $info->phone }}
                                    </span>
                                    <br>
                                    <span class="m-widget4__sub">
                                        {{ $info->email }}
                                    </span>
                                </div>
                            </div>
                            @endforeach
                            <!--end::Widget 14 Item-->

                        </div>
                        <!--end::Widget 14-->
                    </div>
                </div>
                <!--end::Content-->
            </div>
            {{ Form::open(array('route' => array('report.evaluate', $report->id),'enctype' => 'multipart/form-data')) }}
            <div class="m-portlet__foot">
                <div class="row align-items-center">
                    @if ($report->evaluate)
                        <div class="col-lg-6">
                            <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show" role="alert">
                                <div class="m-alert__icon">
                                    <i class="la la-check-circle"></i>
                                </div>
                                <div class="m-alert__text">
                                    <strong>
                                        Đánh giá của {{ ($report->updated_by) ? $report->getUpdatedBy->fullname : '' }}:
                                    </strong>
                                    {!! $report->evaluate !!}
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="col-lg-12" style="margin-bottom: 20px">
                            <textarea class="summernote" id="summernote" name="evaluate">{!! $report->evaluate !!}</textarea>
                        </div>
                        <div class="col-lg-6 m--valign-middle">
                            Đánh giá của bạn
                        </div>
                    @endif
                    <div class="col-lg-6 m--align-right">
                        @if (is_null($report->evaluate))
                        <button type="submit" class="btn btn-outline-primary m-btn m-btn--icon">
                            <span>
                                <i class="fa fa-send-o"></i>
                                <span>
                                    Gửi đánh giá
                                </span>
                            </span>
                        </button>
                        @endif
                        {{-- <a class="btn btn-outline-success m-btn m-btn--icon" data-id="{{ $report->id }}">
                            <span>
                                <i class="fa fa-thumbs-up"></i>
                                <span id="labelapprove">
                                    {{ ($report->is_approve == ZERO) ? 'Duyệt' : 'Bỏ duyệt' }}
                                </span>
                            </span>
                        </a> --}}
                        <span class="m--margin-left-10">
                            {{-- hoặc --}}
                            <a href="{{ route('report.index') }}" class="m-link m--font-bold">
                                Về danh sách
                            </a>
                        </span>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
        <!--end:: Widgets/Best Sellers-->
        <!--end::Section-->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#summernote').summernote({
            callbacks: {
                onImageUpload: function(image) {
                    uploadImage(image[0]);
                }
            },
            height: 150,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
              ]
        });
    });
</script>
@stop