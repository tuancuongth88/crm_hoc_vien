<?php
$data     = (isset($data)) ? $data : null;
$workList = (isset($workList)) ? $workList : App\Repositories\Jobs\PlanRepository::modelList;
?>

@extends('administrator.app')
@section('title','Kế hoạch triển khai công việc')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Mục tiêu</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <a href="{{ route('target.index') }}" class="btn btn-success">Danh sách mục tiêu</a>
            </div>
            <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide"><i class="la la-gear"></i></span>
                                <h3 class="m-portlet__head-text">Thêm mới mục tiêu</h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => empty($data['id']) ? ['target.store'] : ['target.update', $data->id], 'method' => empty($data['id']) ? 'POST' : 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group clearfix">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">#</span>
                                    </div>
                                    {{ Form::text('title', getObject($data, 'title'), ['class' => 'form-control', 'placeholder' => 'Tiêu đề', 'required']) }}
                                </div>
                            </div>
                            <div class="form-group m-form__group clearfix">
                                {{ Form::textarea('description', getObject($data, 'description'), ['class' => 'form-control', 'placeholder' => 'ghi chú', 'rows' => 3]) }}
                            </div>
                            <div class="form-group m-form__group clearfix">
                                {{ Form::text('month_of_year', getObject($data, 'month_of_year') , ['class' => 'form-control date-picker', 'placeholder' => 'Chọn tháng năm', 'required']) }}
                            </div>
                            <div class="form-group m-form__group clearfix">
                                <div class="m-form__heading">
                                    <h3 class="m-form__heading-title">Chi tiết công việc</h3>
                                </div>
                                <table class="table table-striped">
                                    @foreach($workList as $key => $value)
                                        <?php $dataJson = !empty(getObject($data, 'data')[$key]) ? getObject($data, 'data')[$key] : ['number' => '', 'time' => ''];
                                        ?>
                                        <tr>
                                            <td><label class="py-2">{{ $value }}</label></td>
                                            <td style="width: 250px" class="align-middle">
                                                <div class="pull-right">
                                                    {{ Form::hidden('target_title[]', $key) }}
                                                    <div class="pull-left">
                                                        {{ Form::number('target_number[]', (isset($data->data[$key])) ? $data->data[$key]['number'] : '' , ['class' => 'form-control m-input', 'required', 'style' => 'width: 100px', 'placeholder' => 'Số lượng', 'min' => 0]) }}
                                                    </div>
                                                    <span class="pull-left py-2"> / </span>
                                                    <div class="pull-left">
                                                        {{ Form::select('time_unit[]', ['ngày' => 'ngày', 'tuần' => 'tuần', 'tháng' => 'tháng'], $dataJson['time'], ['class' => 'form-control', 'required']) }}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">Lưu</button>
                                <a class="btn btn-secondary" href="{{ route('target.index') }}">Trở về danh sách</a>
                            </div>
                        </div>
                    {{-- </form> --}}
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>

    <script>
        $(document).ready(function() {
            $('.date-picker').datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                format: 'mm-yyyy',
                onClose: function (dateText, inst) {
                    $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
                }
            });
        });
    </script>
@stop