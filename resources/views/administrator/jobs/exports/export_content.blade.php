@foreach($data as $key=>$value)
    <table class="c14">
        <tbody>
        <tr class="c33">
            <td class="c15" colspan="1" rowspan="1">
                <p class="c22"><span class="c0">
                    <img height="55px" src="{{URL::asset('image1.png')}}">
                </span></p>
            </td>
            <td class="c34" colspan="5" rowspan="1">
                <p class="c22">
                <span class="c43">
                    BÁO CÁO KẾT QUẢ CÔNG VIỆC / THÁNG {{$month_report}}
                </span><span class="c0">
                    <br>(Kết quả lấy từ hệ thống báo cáo công việc  dành cho cấp nhân viên kinh doanh thử việc) </span>
                </p>
                <p class="c22"><span class="c0">
                    Hải Phát Land - Ngày: {{\Carbon\Carbon::now()->format('d-m-Y') }}<br>
                    @if($value->created_at != null)
                            Ngày kích hoạt tài khoản: {{ \Carbon\Carbon::parse($value->created_at)->format('d-m-Y') }}
                        @endif
                </span></p>
            </td>
        </tr>
        <tr class="c42">
            <td class="c40" colspan="6" rowspan="1">
                <p class="c5"><span class="c0"></span></p>
            </td>
        </tr>
        <tr class="c23">
            <td class="c15" colspan="1" rowspan="1">
                <p class="c10"><span class="c9">Họ tên</span></p>
            </td>
            <td class="c34" colspan="5" rowspan="1">
                <p class="c10"><span class="c0">{{$value->fullname .' - '.$value->email}}</span></p>
            </td>
        </tr>
        <tr class="c37">
            <td class="c15" colspan="1" rowspan="1">
                <p class="c10"><span class="c9">Chức vụ</span></p>
            </td>
            <td class="c34" colspan="5" rowspan="1">
                <p class="c10"><span class="c0">{{$value->position_name}}</span></p>
            </td>
        </tr>
        <tr class="c35">
            <td class="c15" colspan="1" rowspan="1">
                <p class="c10"><span class="c9">Phòng ban</span></p>
            </td>
            <td class="c34" colspan="5" rowspan="1">
                <p class="c10"><span class="c0">{{$value->department_name}}</span></p>
            </td>
        </tr>
        <tr class="c39">
            <td class="c17 c32" colspan="1" rowspan="1">
                <p class="c6"><span class="c28">Stt</span></p>
            </td>
            <td class="c7" colspan="1" rowspan="1">
                <p class="c6"><span class="c28">Nội dung công việc</span></p>
            </td>
            <td class="c11 c32" colspan="1" rowspan="1">
                <p class="c6"><span class="c28">Kết quả</span></p>
            </td>
            <td class="c3 c32" colspan="1" rowspan="1">
                <p class="c6"><span class="c28">Chỉ tiêu</span></p>
            </td>
            <td class="c20 c32" colspan="1" rowspan="1">
                <p class="c6"><span class="c28">Hoàn thành</span></p>
            </td>
            <td class="c20 c32" colspan="1" rowspan="1">
                <p class="c6"><span class="c28">Ghi chú</span></p>
            </td>
        </tr>
        <tr class="c41">
            <td class="c29" colspan="3" rowspan="1">
                <p class="c1"><span class="c28">Triển khai công việc tại VP Công ty</span>
                </p>
            </td>
            <td class="c21" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">10%</span>
                </p>
            </td>
            <td class="c18" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">
                        {{number_format($value->rating_work_company_office,2)}}%
                    </span>
                </p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">
                    </span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">1</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Gọi điện</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ checkNumberNegative($value->total_call) }}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_call_phone }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ number_format($value->rate_call,2) }}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">cuộc/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">2</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Đăng tin website</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_post_website)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_post_website }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_post_website,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">tin/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">3</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Đăng tin Fanpage</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ checkNumberNegative($value->total_post_facebook)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_post_facebook }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ number_format($value->rate_post_facebook,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">tin/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">4</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Đăng tin trang BĐS + Forum</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ checkNumberNegative($value->total_post_news_and_forum) }}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_post_news_and_forum }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ number_format($value->rate_post_news_and_forum,2) }}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">tin/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">5</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Gửi email</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_send_email)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_send_email }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_send_email,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">mail/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">6</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Cập nhật thông tin dự án</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ checkNumberNegative($value->total_info_project) }}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_update_info_project }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ number_format($value->rate_info_project,2) }}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Dự án/ tháng</span></p>
            </td>
        </tr>

        <tr class="c41">
            <td class="c29" colspan="3" rowspan="1">
                <p class="c1"><span class="c28">Triển khai công việc ngoài hiện trường</span>
                </p>
            </td>
            <td class="c21" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">10%</span>
                </p>
            </td>
            <td class="c18" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">
                        {{number_format($value->rating_complete_job_chart,2)}}%
                    </span>
                </p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">
                    </span></p>
            </td>
        </tr>

        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">1</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Phát tờ rơi cá nhân</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_flypaper_personal)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_flypaper_personal}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_flypaper_personal,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">tờ/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">2</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Phát tờ rơi và treo phướn tập thể</span>
                </p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_flypaper_group)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_flypaper_group}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_flypaper_group,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">lần/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">3</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Thực hiện Questionair</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_questionair)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_questionair}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_questionair,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">thông tin KH</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">4</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Trực dự án và nhà mẫu</span>
                </p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_guard_project)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_guard_project}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_guard_project,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">lần/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">5</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1">
                    <span class="c0">Tham gia sự kiện mở bán, giới thiệu dự án</span>
                </p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_open_sale)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_open_sale}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_open_sale,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">lần/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">6</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1">
                    <span class="c0">Tham gia Roadshow, quảng bá, xúc tiến</span>
                </p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_roadshow)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_roadshow}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_roadshow,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">lần/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">7</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1">
                    <span class="c0">Tham gia sự kiện event hội thảo bên ngoài</span>
                </p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_event)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_event}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_event,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">lần/ tháng</span></p>
            </td>
        </tr>
        {{--<tr class="c13">--}}
        {{--<td class="c17" colspan="1" rowspan="1">--}}
        {{--<p class="c6"><span class="c12">18</span></p>--}}
        {{--</td>--}}
        {{--<td class="c16" colspan="1" rowspan="1">--}}
        {{--<p class="c1"><span class="c0">Chủ động tìm kiếm khách hàng tại các địa điểm công cộng</span>--}}
        {{--</p>--}}
        {{--</td>--}}
        {{--<td class="c11" colspan="1" rowspan="1">--}}
        {{--<p class="c2"><span class="c0">{{$value->total_find_public}}</span></p>--}}
        {{--</td>--}}
        {{--<td class="c3" colspan="1" rowspan="1">--}}
        {{--<p class="c2"><span class="c0">{{$value->conf_find_public}}</span></p>--}}
        {{--</td>--}}
        {{--<td class="c20" colspan="1" rowspan="1">--}}
        {{--<p class="c2"><span class="c0">{{number_format($value->rate_find_public,2)}}%</span></p>--}}
        {{--</td>--}}
        {{--<td class="c17" colspan="1" rowspan="1">--}}
        {{--<p class="c2"><span class="c0"></span></p>--}}
        {{--</td>--}}
        {{--</tr>--}}

        <tr class="c41">
            <td class="c29" colspan="3" rowspan="1">
                <p class="c1"><span class="c28">Chỉ tiêu khách hàng</span>
                </p>
            </td>
            <td class="c21" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">40%</span>
                </p>
            </td>
            <td class="c18" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">
                        {{number_format($value->rating_complete_search_customer,2)}}%
                    </span>
                </p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">
                    </span></p>
            </td>
        </tr>

        <tr class="c35">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">1</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Khách hàng quan tâm</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_levels_1)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ $value->conf_kpi_levels_1  }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_levels_1,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Khách hàng</span></p>
            </td>
        </tr>

        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">2</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Cuộc hẹn với khách hàng</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_setup_appointment)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_setup_appointment}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_setup_appointment,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Cuộc hẹn</span></p>
            </td>
        </tr>

        <tr class="c37">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">3</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Khách hàng gặp tư vấn trực tiếp</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_levels_2)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ $value->conf_kpi_levels_2  }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_levels_2,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">khách hàng</span></p>
            </td>
        </tr>
        <tr class="c30">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">4</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Khách hàng chốt giao dịch</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_levels_3)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{ $value->conf_kpi_levels_3  }}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_levels_3,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">khách hàng</span></p>
            </td>
        </tr>

        <tr class="c13">
            <td class="c29" colspan="3" rowspan="1">
                <p class="c1">
                    <span class="c28">Hoạt động truyền thông nội bộ</span>
                </p>
            </td>
            <td class="c21" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">10%</span>
                </p>
            </td>
            <td class="c18" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rating_complete_brand_work,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">1</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Like</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_like)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_like}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_like,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">lượt/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">2</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Share</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_share)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_share}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_share,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">lượt/ tháng</span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">3</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Comment</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{checkNumberNegative($value->total_comment)}}</span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{$value->conf_comment}}</span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0">{{number_format($value->rate_comment,2)}}%</span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">lượt/ tháng</span></p>
            </td>
        </tr>

        <tr class="c13">
            <td class="c29" colspan="3" rowspan="1">
                <p class="c1">
                    <span class="c28">Tuân thủ Văn hóa doanh nghiệp</span>
                </p>
            </td>
            <td class="c21" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">20%</span>
                </p>
            </td>
            <td class="c18" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">1</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Đồng phục</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">2</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Tham gia các hoạt động, phong trào tập thể</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
        </tr>

        <tr class="c13">
            <td class="c29" colspan="3" rowspan="1">
                <p class="c1">
                    <span class="c28">Hoàn thành khóa đào tạo</span>
                </p>
            </td>
            <td class="c21" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">10%</span>
                </p>
            </td>
            <td class="c18" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c17" colspan="1" rowspan="1">
                <p class="c6"><span class="c12">1</span></p>
            </td>
            <td class="c16" colspan="1" rowspan="1">
                <p class="c1"><span class="c0">Khóa đào tạo hội nhập</span></p>
            </td>
            <td class="c11" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c3" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c20" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
        </tr>
        <tr class="c13">
            <td class="c29" colspan="3" rowspan="1">
                <p class="c1">
                    <span class="c28">Tổng điểm</span>
                </p>
            </td>
            <td class="c21" colspan="1" rowspan="1">
                <p class="c2">
                    <span class="c0">100%</span>
                </p>
            </td>
            <td class="c18" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
            <td class="c17" colspan="1" rowspan="1">
                <p class="c2"><span class="c0"></span></p>
            </td>
        </tr>
        <tr class="c42">
            <td class="c19" colspan="6" rowspan="1">
                <p class="c6"><span class="c31">
                    Ghi chú: Thông tin trên được lấy từ phần mềm quản lý công việc (Report.haiphatland.com.vn)
                </span>
                </p>
            </td>
        </tr>
        </tbody>
    </table>
    @if(count($data)== $key +1)
        <div class="page-break end">
        </div>
    @else
        <div class="page-break">
        </div>
        <p class="tag-disable-print" style="margin: 10px;padding: 10px;height: 10px;width: 100%">&nbsp;</p>
    @endif
@endforeach
