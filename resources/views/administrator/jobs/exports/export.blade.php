<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style type="text/css">
        @media all {
            .page-break {
                display: none;
            }
        }

        @media print {
            .page-break {
                display: block;
                page-break-before: always;
            }

            .page-break.end {
                display: none !important;
                page-break-before: always;
            }

            .c28 {
                color: black !important;
            }

            .tag-disable-print {
                display: none;
            }
        }

        ol {
            margin: 0;
            padding: 0
        }

        table td, table th {
            padding: 0
        }

        .c7 {
            border-right-style: solid;
            padding: 3pt 2pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            background-color: #7f7f7f;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 268.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c18 {
            border-right-style: solid;
            padding: 3pt 3pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            background-color: #ffc000;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 89.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c29 {
            border-right-style: solid;
            padding: 3pt 2pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            background-color: #c55a11;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 449.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c20 {
            border-right-style: solid;
            padding: 3pt 2pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 89.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c11 {
            border-right-style: solid;
            padding: 3pt 2pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 57pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c16 {
            border-right-style: solid;
            padding: 3pt 2pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 268.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c15 {
            border-right-style: solid;
            padding: 3pt 3pt 3pt 3pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c24 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 57pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c40 {
            border-right-style: solid;
            padding: 0;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 538.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c3 {
            border-right-style: solid;
            padding: 3pt 2pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 47.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c17 {
            border-right-style: solid;
            padding: 3pt 2pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 60.7pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c4 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 47.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c34 {
            border-right-style: solid;
            padding: 3pt 3pt 3pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 462pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c21 {
            border-right-style: solid;
            padding: 3pt 2pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 40.7pt;
            border-top-color: #000000;
            border-bottom-style: solid;
            background-color: #c55a11;
        }

        .c19 {
            border-right-style: solid;
            padding: 3pt 2pt 3pt 2pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 538.5pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c26 {
            border-right-style: solid;
            padding: 5pt 5pt 5pt 5pt;
            border-bottom-color: #000000;
            border-top-width: 1pt;
            border-right-width: 1pt;
            border-left-color: #000000;
            vertical-align: top;
            border-right-color: #000000;
            border-left-width: 1pt;
            border-top-style: solid;
            border-left-style: solid;
            border-bottom-width: 1pt;
            width: 89.2pt;
            border-top-color: #000000;
            border-bottom-style: solid
        }

        .c0 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c28 {
            color: #ffffff;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c9 {
            color: #000000;
            font-weight: 700;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c12 {
            color: #000000;
            font-weight: 400;
            text-decoration: none;
            vertical-align: baseline;
            font-size: 11pt;
            font-family: "Times New Roman";
            font-style: normal
        }

        .c2 {
            margin-left: 7pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            text-align: right;
            margin-right: 7pt
        }

        .c6 {
            margin-left: 2pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            text-align: center;
            margin-right: 2pt
        }

        .c1 {
            margin-left: 7pt;
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            text-align: left;
            margin-right: 7pt
        }

        .c38 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.15;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .c25 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: right;
            margin-right: 7pt
        }

        .c31 {
            background-color: #ffffff;
            font-size: 11pt;
            font-family: "Times New Roman";
            font-style: italic;
            font-weight: 700
        }

        .c5 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left;
            height: 11pt
        }

        .c14 {
            border-spacing: 0;
            border-collapse: collapse;
            margin-right: auto
        }

        .c10 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: left
        }

        .c22 {
            padding-top: 0pt;
            padding-bottom: 0pt;
            line-height: 1.0;
            text-align: center
        }

        .c36 {
            background-color: #ffffff;
            /*max-width: 538.6pt;*/
            /*padding: 0pt 56.7pt 0pt 0pt*/
        }

        .c43 {
            font-size: 12pt;
            font-family: "Times New Roman";
            font-weight: 700
        }

        .c23 {
            /*height: 19pt*/
        }

        .c42 {
            /*height: 23pt*/
        }

        .c39 {
            /*height: 34pt*/
        }

        .c33 {
            /*height: 57pt*/
        }

        .c27 {
            margin-right: 7pt
        }

        .c13 {
            height: 0pt
        }

        .c32 {
            background-color: #7f7f7f
        }

        .c41 {
            height: 22pt
        }

        .c37 {
            /*height: 21pt*/
        }

        .c35 {
            height: 15pt
        }

        .c30 {
            /*height: 24pt*/
        }

        .c8 {
            height: 11pt
        }

        .title {
            padding-top: 0pt;
            color: #000000;
            font-size: 26pt;
            padding-bottom: 3pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        .subtitle {
            padding-top: 0pt;
            color: #666666;
            font-size: 15pt;
            padding-bottom: 16pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        li {
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        p {
            margin: 0;
            color: #000000;
            font-size: 11pt;
            font-family: "Arial"
        }

        h1 {
            padding-top: 20pt;
            color: #000000;
            font-size: 20pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h2 {
            padding-top: 18pt;
            color: #000000;
            font-size: 16pt;
            padding-bottom: 6pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h3 {
            padding-top: 16pt;
            color: #434343;
            font-size: 14pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h4 {
            padding-top: 14pt;
            color: #666666;
            font-size: 12pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h5 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            orphans: 2;
            widows: 2;
            text-align: left
        }

        h6 {
            padding-top: 12pt;
            color: #666666;
            font-size: 11pt;
            padding-bottom: 4pt;
            font-family: "Arial";
            line-height: 1.15;
            page-break-after: avoid;
            font-style: italic;
            orphans: 2;
            widows: 2;
            text-align: left
        }
    </style>
</head>
<body class="c36">
@include('administrator.errors.info-search')
<div class="m-section ">
    @include('administrator.errors.messages')
</div>
@if(!$data)
    <div style="margin: 5%;color: black;font-family: bold; font-size: 16pt ">
        Không có danh sách báo cáo nào được tìm thấy. <a href="http://report.haiphatland.com.vn">
            Về trang chủ
        </a>
    </div>
@endif
@include('administrator.jobs.exports.export_content')
</body>
</html>
