@extends('administrator.app')
@section('title','Vai trò')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Vai trò
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    @include('administrator.errors.errors-validate')
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <a href="{{ route('role.index') }}" class="btn btn-success">Danh sách</a>
            </div>
            <div class="col-md-12">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Thêm mới
                                </h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => array('role.put-role'),'method' => 'PUT',  'class' => 'm-form m-form--fit m-form--label-align-right')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label class="col-form-label">
                                        Tài khoản
                                    </label>
                                    <div class="">
                                        <select class="form-control m-select2" name="user_id" id="user_id">
                                        {{-- <select class="form-control m-bootstrap-select m_selectpicker" name="user_id" id="user_id" data-live-search="true"> --}}
                                            <option>Chọn tài khoản</option>
                                            @foreach ($listUser as $user)
                                                <option value="{{ $user->id }}" data-role-id="{{ ($user->roles()->first()) ? $user->roles()->first()->id : '' }}">
                                                    {{ $user->email }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <label class="col-form-label">
                                        Vai trò
                                    </label>
                                    <div class="">
                                        <select class="form-control m-bootstrap-select m_selectpicker" name="role_id" id="role_id">
                                            <option>Chọn vai trò</option>
                                            @foreach ($listRole as $role)
                                                <option value="{{ $role->id }}">
                                                    {{ $role->display_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('role.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>
<script src="{{ URL::asset('assets/demo/default/custom/components/forms/widgets/bootstrap-switch.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function (argument) {
        $('.m-select2').select2({
            placeholder: "Select a value",
        });
        $('#user_id').change(function() {
            var roleId = $('#user_id').find(":selected").attr('data-role-id');
            $("#role_id").val(roleId);
            // $('select[name=selValue]').val(1);
            $('.m_selectpicker').selectpicker('refresh');
        });
    });



</script>
@stop
