@extends('administrator.app')
@section('title','Vai trò')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Vai trò
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <a href="{{ route('role.index') }}" class="btn btn-success">Danh sách</a>
            </div>
            <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Thêm mới
                                </h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => array('role.put-permission'),'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
                        <div class="m-portlet__body">
                            <!--begin: Search Form -->
                            <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                <div class="row align-items-center">
                                    <div class="col-xl-4 order-2 order-xl-1">
                                        <div class="form-group m-form__group row align-items-center">
                                            {{ Form::select('role_id', [0 => 'Chọn vai trò'] + $listRole, null, ['class' => 'form-control m-select2', 'id' => 'role_id']) }}
                                        </div>
                                    </div>
                                    <div class="col-xl-2 order-2 order-xl-1">
                                        <label class="m-checkbox m-checkbox--state-success">
                                            <input type="checkbox" onclick="toggle(this);" id="select-all"/>
                                            Chọn tất cả
                                            <span></span>
                                        </label>
                                    </div>
                                    <div class="col-xl-4 order-1 order-xl-2">
                                        <button type="submit" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                            <span>
                                                <i class="la la-save"></i>
                                                <span>
                                                    Lưu
                                                </span>
                                            </span>
                                        </button>
                                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Search Form -->
                            <div class="col-md-12 m--margin-top-30">
                                <div class="m_datatable" id="ajax_data"></div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                        </div>
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>
@section('script')
<script src="{{ URL::asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
<script>
    $(document).ready(function () {
        $('.m-select2').select2();
        $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: '/administrator/system/permission-by-role',
                    },
                },
                pageSize: 10,
                saveState: false,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // column sorting
            sortable: false,

            pagination: false,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            // search: {
            //     input: $('#generalSearch'),
            // },
            // columns definition
            columns: [
            {
                field: "id",
                title: "#",
                width: 50,
                sortable: false,
                textAlign: 'center',
                // selector: {class: 'm-checkbox--solid m-checkbox--brand', test: 'ok'},
                template: function(row) {
                    if (row.checked) {
                        return '<input type="checkbox" class="checkbox-permission" checked name="permission[' + row.id + ']">';
                    }
                    return '<input type="checkbox" class="checkbox-permission" name="permission[' + row.id + ']">';
                },
            }
            // ,{
            //     field: 'name',
            //     title: 'Tên',
            // }
            ,{
                field: 'display_name',
                title: 'Quyền',
            },{
                field: 'description',
                title: 'Mô tả',
            }],
        });
        $('body').on('change', '#role_id', function() {
            $('#select-all').prop('checked', false);
            var id = $(this).val();
            // $( '#list-permission').empty();
            // $.ajax({
            //     url: '/administrator/system/permission-by-role/' + id,
            //     type : "get",
            //     success: function (result){
            //         $('#list-permission').append(result);
            //     }
            // });
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'role_id');
        });
    });

    function toggle(source) {
        var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i] != source)
                checkboxes[i].checked = source.checked;
        }
    }
</script>
@stop
@stop