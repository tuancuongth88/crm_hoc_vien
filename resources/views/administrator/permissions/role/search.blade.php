{{ Form::open(array('route' => 'role.config-role-user', 'method' => 'GET', 'id' =>'search_form')  ) }}
<div class="row">
    <div class="col-md-5 m--margin-bottom-10">
        <label>Tên hoặc email</label>
        <input type="text" class="form-control m-input search_name" placeholder="Tên hoặc email" name="search_name"
               value="{{isset($input['search_name'])? $input['search_name']:'' }}">
    </div>
    <div class="col-md-5 col-sm-6 mb-3 mb-md-0">
        <label>Vai trò hiện tại</label>
        {{ Form::select('search_role_id',[0 => '-- Chọn vai trò --']+$listRole,@$input['search_role_id'], ['required','class' => 'form-control m-input search_role_id']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="button" value="Tìm kiếm" class="btn btn-primary form-control config-role-user-search"/>
    </div>
</div>
</form>
