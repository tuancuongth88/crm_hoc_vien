@extends('administrator.app')
@section('title','Vai trò')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Cấu hình vai trò người dùng
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            {{ Form::open(array('route' => 'role.cf-role-user-store', 'method' => 'POST', 'id' =>'config-role-user')  ) }}
            <div class="row">
                <div class="col-xl-6">
                    <!--begin::Section-->
                    <div class="m-section__content">
                        @include('administrator.permissions.role.search')
                    </div>
                    @include('administrator.errors.info-search')
                    <div class="m-section">
                        @include('administrator.errors.messages')

                        <div class="m-section__content">
                            <table class="table m-table m-table--head-bg-warning">
                                <thead>
                                <tr>
                                    <th>
                                        <input type="checkbox" name="select-all" id="select-all"/>
                                    </th>
                                    <th>
                                        Tên nhân viên
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Vai trò hiện tại
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $value)
                                    <tr>
                                        <th scope="row">
                                            <?php
                                            ?>
                                            <input type="checkbox"
                                                   name="cf_role_u[user_id][]"
                                                   data-user_id="{{$value->id}}"
                                                   value="{{$value->id}}"
                                            />
                                        </th>
                                        <td>
                                            {{ $value->fullname }}
                                        </td>
                                        <td>
                                            {{ $value->email }}
                                        </td>
                                        <td>
                                            @if(isset($value->roles[0]))
                                                {{ data_get($value->roles[0],'display_name') }}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $data->appends(@$input)->links()  }}
                        Tổng số {{ $data->total() }} bản ghi
                    </div>
                    <!--end::Section-->
                </div>
                <div class="col-xl-6">
                    <div class="row m-4">
                        <div class="col-md-6 col-sm-6 mb-3 mb-md-0">
                            <label>Vai trò mới</label>
                            {{ Form::select('cf_role_u[role_id]',['' => '-- Chọn vai trò --']+$listRole,null, ['required','class' => 'form-control m-input']) }}
                        </div>
                        <div class="col-md-4 col-sm-6 mb-3 mb-md-0">
                            <label>Lưu vai trò mới</label>
                            <input type="submit" value="Lưu lại" class="btn btn-primary form-control action-save"/>
                        </div>
                    </div>
                </div>
            </div>
            {{Form::close()}}
        </div>
    </div>
    <script>
        jQuery(document).ready(function () {
            $('.m-select2').select2();
        });

        $('#select-all').click(function (event) {
            if (this.checked) {
                // Iterate each checkbox
                $(':checkbox').each(function () {
                    this.checked = true;
                });
            } else {
                $(':checkbox').each(function () {
                    this.checked = false;
                });
            }
        });

        $('.config-role-user-search').click(function (e) {
            var search_role_id = $('.search_role_id').val();
            var search_name = $('.search_name').val();
            var url = '{{ route("role.config-role-user", ["search_role_id"=>"rp_search_role_id","search_name"=>"rp_search_name"]) }}';
            url = url.replace('rp_search_role_id', search_role_id);
            url = url.replace('rp_search_name', search_name);
            url = url.replace('&amp;', '&');
            window.location.href = url;
        })

    </script>
@stop
