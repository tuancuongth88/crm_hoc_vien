@extends('administrator.app')
@section('title','Vai trò')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Vai trò
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        <div class="col-xs-12" style="margin-bottom: 20px">
            {{ Form::open(array('enctype' => 'multipart/form-data', 'method'=>'POST', 'route' => 'role.import', 'id' => 'import-form')) }}
                <a href="{{ route('role.create') }}" class="btn btn-primary m-btn m-btn--icon">
                    <span>
                        <i class="fa flaticon-plus"></i>
                        <span>
                            Thêm mới
                        </span>
                    </span>
                </a>
                <a href="{{ route('role.config-user') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="fa flaticon-user-ok"></i>
                        <span>
                            User
                        </span>
                    </span>
                </a>
                <a href="{{ route('role.config-role-user') }}" class="btn btn-success m-btn m-btn--icon">
                    <span>
                        <i class="fa flaticon-settings-1"></i>
                        <span>
                           Gán vai trò cho users
                        </span>
                    </span>
                </a>
                <a href="{{ route('role.config-role') }}" class="btn btn-info m-btn m-btn--icon">
                    <span>
                        <i class="fa flaticon-puzzle"></i>
                        <span>
                            Quyền
                        </span>
                    </span>
                </a>
                <div class="custom-file col-md-3 m--padding-top-5">
                    <input type="file" class="custom-file-input" id="customFile" name="import_file">
                    <label class="custom-file-label" for="customFile">
                        Chọn file import
                    </label>
                </div>
            <button type="submit" class="btn btn-success" id="import_file"><i class="fa fa-file-excel-o"></i> Import</button>
            {{ Form::close() }}<br>
            <a href="{{ url('/role.xlsx') }}">Mẫu import File</a>
        </div>
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <table class="table m-table m-table--head-bg-warning">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Tên
                            </th>
                            <th>
                                Tùy chọn
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <th scope="row">
                                {{ $key + 1 }}
                            </th>
                            <td>
                                {{ $value['display_name'] }}
                            </td>
                            <td>
                                <a href="{{ route('role.edit', $value->id) }}" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                {{ Form::open(array('method'=>'DELETE', 'route' => array('role.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                    <i class="fa fa-close"></i>
                                </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
@stop
