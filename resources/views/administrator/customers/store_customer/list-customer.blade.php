<div class="m-section">
    <div class="m-section__content">
        <table class="table m-table m-table--head-bg-warning" id = data_customer>
            <thead>
            <tr>
                <th>
                    #
                </th>
                <th>
                    Tên khách hàng
                </th>
                <th>
                    Số điện thoại
                </th>
                <th>
                    Email
                </th>
                <th>
                    Địa chỉ
                </th>
                <th>
                    Loại dữ liệu
                </th>
                <th>
                    Ghi chú
                </th>
            </tr>
            </thead>
            <tbody>
            @foreach($totalCustomer as $key => $value)
                <tr>
                    <th scope="row">
                        {{ $key + 1 }}
                    </th>
                    <td>
                        {{ $value['fullname'] }}
                    </td>
                    <td>
                        {{ $value['phone'] }}
                    </td>
                    <td>
                        {{ $value['email'] }}
                    </td>
                    <td>
                        {{ $value['address'] }}
                    </td>
                    <td>
                        {{@$value->customerType->name }}
                    </td>
                    <td>
                        {{@$value['comment'] }}
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{ $totalCustomer->links() }}
    Tổng số {{ $totalCustomer->total() }} bản ghi
</div>
<script>
    $(window).on('hashchange',function(){
        page = window.location.hash.replace('#','');
    });
    $("#table_customer").off().on('click','.pagination a', function(e) {
        e.preventDefault();
        //to get what page, when you click paging
        var page = $(this).attr('href').split('page=')[1];
        getTransaction(page);
        location.hash = page;
    });
    function getTransaction(page) {
        console.log(page);
        callAjax(id, strPage+page);
    }
</script>
