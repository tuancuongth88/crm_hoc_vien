@extends('administrator.app')
@section('title','Phân chia khách hàng')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet col-xl-8 offset-xl-2">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Giao khách hàng cho sale trong phòng
                            </h3>
                        </div>
                    </div>
                </div>
            <!--begin::Form-->
                {{ Form::open(array('route' => 'customer.post-share-customer-by-leader', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
                <div class="m-portlet__body">
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">

                        </h3>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">
                            Chọn chiến dịch
                        </label>

                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <div class="custom-file">
                                {{ Form::select('share_id', ['' => 'Lựa chọn'] + \App\Models\Customers\CustomerSharing::getAllSharingByUser(), old('share_id'), ['class' => 'form-control', 'id' => 'share_id', 'required']) }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">
                            Số khách hàng:
                        </label>

                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <div class="custom-file" style="margin-top: 9px; font-weight: bold;">
                                <input type="hidden" name="total_customer" class="total_customer">
                                <span id="total_customer" style="color: red;">0 </span> Khách hàng
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">
                            Chọn nhân viên:
                        </label>

                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <div class="custom-file" style=" font-weight: bold;">
                                <input type="hidden" name="total_customer" class="total_customer">
                                <span style="color: red;" id="total_sale">{{ count($listSale) }} &nbsp</span> Nhân viên
                                &nbsp&nbsp&nbsp
                                <a href="#" class="btn btn-outline-brand m-btn m-btn--icon m-btn--icon-only m-btn--outline-2x m-btn--pill m-btn--air"   data-toggle="modal" data-target="#m_modal_6">
                                    <i class="flaticon-user-add"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">
                            Nhập số lượng giao:
                        </label>

                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <div class="custom-file">
                                <input type="number"  min="0" name="number_customer_input" value="{{ old('number_customer_input') }}" class="form-control">
                            </div>
                        </div>
                    </div>
                    @include('administrator.errors.messages')
                    <div class="modal fade" id="m_modal_6" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">
                                        Danh sách nhân viên
                                    </h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">
                                            &times;
                                        </span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <p>
                                        <div class="m-section" style="overflow: scroll; height: 400px;">
                                            <div class="m-section__content">
                                                <table class="table table-bordered m-table m-table--border-brand m-table--head-bg-brand" >
                                                    <thead>
                                                        <tr >
                                                            <th>
                                                                Chọn
                                                                <label class="m-checkbox m-checkbox--state-success">
                                                                    <input type="checkbox" id="select_all">
                                                                    <span></span>
                                                                </label>
                                                            </th>
                                                            <th>
                                                                Tên nhân viên
                                                            </th>
                                                            <th>
                                                                Email
                                                            </th>
                                                            <th>
                                                                Chức vụ
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($lstUser as $key => $value)
                                                        <tr>
                                                            <th scope="row">
                                                                <label class="m-checkbox m-checkbox--state-success">
                                                                    <input type="checkbox" name="sales[]" value="{{ $value->id }}" class="checkbox_sale">
                                                                    <span></span>
                                                                </label>
                                                            </th>
                                                            <td>
                                                                {{ $value->fullname }}
                                                            </td>
                                                            <td>
                                                                {{ $value->email }}
                                                            </td>
                                                            <td>
                                                                {{ @$value->position->name }}
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                        Đóng
                                    </button>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="selected_sale">
                                        Chọn
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <div class="row">
                            <div class="col-lg-8 offset-lg-4">
                                <button class="btn btn-primary">
                                    Đổ khách hàng cho sale
                                </button>
                                <input type="reset" value="XÓA DỮ LIỆU" class="btn btn-danger">
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
            <!--end::Form-->
                <div id="table_customer">

                </div>

            </div>
            <!--end::Portlet-->

        </div>
    </div>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        var id = 0;
        var strPage = '?page=';
        jQuery(document).ready(function() {

            $('.m-select2').select2();
            $('#share_id').change(function (e) {
                e.preventDefault();
                id = $(this).val();
                if(id > 0){
                    callAjax(id, strPage+'1');
                }else{
                    $('#total_customer').empty();
                    $('#table_customer').empty();
                    $('.total_customer').val('');

                }
            });
            $("input[type='checkbox']").attr('checked', true);
             // Select all
            $("#select_all").off().on('click', function(e) {
                console.log(1);
                var checkbox = $('#select_all').prop('checked');
                if(checkbox == true){
                    $("input[type='checkbox']").attr('checked', true);
                }
                else{
                    $("input[type='checkbox']").attr('checked', false);
                }
                return true;
            });

            $("#select_all").change(function(){  //"select all" change
                $('#total_sale').empty();
                var status = this.checked; // "select all" checked status
                $('.checkbox_sale').each(function(){ //iterate all listed checkbox items
                    this.checked = status; //change ".checkbox" checked status
                });
                $('#total_sale').append($('.checkbox_sale:checked').length);
            });

            $('.checkbox_sale').change(function(){ //".checkbox" change
                $('#total_sale').empty();
                //uncheck "select all", if one of the listed checkbox item is unchecked
                if(this.checked == false){ //if this item is unchecked
                    $("#select_all")[0].checked = false; //change "select all" checked status to false
                }

                //check "select all" if all checkbox items are checked
                if ($('.checkbox_sale:checked').length == $('.checkbox_sale').length ){
                    $("#select_all")[0].checked = true; //change "select all" checked status to true
                }
                $('#total_sale').append($('.checkbox_sale:checked').length);
            });

            // Select none
        });
        function callAjax(id, strPage) {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'GET',
                url: 'get-total-customer-by-share/'+id+strPage,
                dataType: "json",
                success: function (result) {
                    $('#total_customer').empty();
                    $('.total_customer').val('');
                    $('#table_customer').empty();
                    $('#total_customer').append(result.data.total);
                    $('.total_customer').val(result.data.total);
                    $('#table_customer').append(result.data.html);
                }
            });
        }
    </script>

@stop