@extends('administrator.app')
@section('title','Nhập dữ liệu khách hàng vào kho')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet col-xl-8 offset-xl-2">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Nhập dữ liệu khách hàng vào kho
                                <small>
                                    <a href="{{ route('customer_stores.download')}}">Mẫu import File</a>
                                </small>
                            </h3>
                        </div>
                    </div>
                </div>
                @include('administrator.errors.errors-validate')
                @if(session('list-fail'))
                    <div class="alert alert-danger alert-notification">
                        @foreach (session('list-fail') as $element)
                            {{ $element }}<br>
                        @endforeach
                    </div>
                @endif
                @include('administrator.errors.messages')
            <!--begin::Form-->
                {{ Form::open(array('route' => 'customer_stores.post-import', 'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
                <div class="m-portlet__body">
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">

                        </h3>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-form-label col-lg-3 col-sm-12">
                            Chọn tệp
                        </label>
                        <div class="col-lg-4 col-md-9 col-sm-12">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="inputGroupFile01" name="import_file"
                                       accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                                <label class="custom-file-label" for="inputGroupFile01">
                                    Chọn file
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions m-form__actions">
                        <div class="row">
                            <div class="col-lg-8 offset-lg-4">
                                <button class="btn btn-primary">
                                    LƯU LẠI
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            {{ Form::close() }}
            <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop