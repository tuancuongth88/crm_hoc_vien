@foreach($listHistory as $key => $value)
    <tr>
        <th scope="row">
            {{ $key }}
        </th>
        <td>
            {{ $value->customer->fullname }}
        </td>
        <td>
            {{ $value->phone }}
        </td>
        <td>
            {{ $value->time_call_back }}
        </td>
        <td>
            {{ isset($value->channel) ?  \App\Models\Customers\Histories::$listChannels[$value->channel] : '' }}
        </td>
        <td>
            {{ $value->note }}
        </td>
    </tr>
@endforeach