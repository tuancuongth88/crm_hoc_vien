{{ Form::open(array('route' => 'customer_stores.index', 'method' => 'GET', 'id' =>'search_form_customer')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tên khách hàng</label>
        <input type="text" name="search" id="search" class="form-control" placeholder="Tên khách hàng"/>
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Loại khách hàng</label>
        {{ Form::select('type', [0 => 'Lựa chọn'] + $listCusType, old('type'), ['class' => 'form-control', 'id' => 'type']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Search" class="btn btn-primary form-control"/>
    </div>

    <input type="hidden" name="searchJoin" value="and">
</div>
</form>
