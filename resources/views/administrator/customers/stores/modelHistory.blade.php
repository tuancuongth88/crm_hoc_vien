<div class="modal fade show" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     style="padding-right: 17px; display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Thêm mới lịch sử chăm sóc
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        ×
                    </span>
                </button>
            </div>
            {{ Form::open(array('route' => 'history.store', 'class' => 'm-form m-form--fit m-form--label-align-right', 'id' => 'submit_form_history', 'enctype' => 'multipart/form-data')) }}
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <div class="modal-body">
                <div class="form-group m-form__group row ">
                    <div class="col-lg-12">
                        Số điện thoại
                        <input type="text" class="form-control m-input" id="phone" placeholder="Nhập số điện thoại" name="phone" value="{{ old('phone') }}">
                    </div>
                </div>
                <div class="form-group m-form__group row ">
                    <div class="col-lg-12">
                        Thời gian cảnh báo
                        <input type="text" class="form-control m-input" id="callback" readonly="readonly" placeholder="Thời gian cảnh báo" name="time_call_back" value="{{ isset($value->time_call_back) ? $value->time_call_back : '' }}">
                    </div>
                </div>
                <div class="form-group m-form__group row ">
                    <div class="col-lg-12">
                        Kênh chăm sóc
                        {{ Form::select('channel', ['' => '---Chọn kênh chăm sóc---'] + \App\Models\Customers\Histories::$listChannels, old('channel'), ['class' => 'form-control m-input', 'id' => 'channel']) }}
                    </div>
                </div>
                <div class="form-group m-form__group row ">
                    <div class="col-lg-12">
                        Nội dung trao đổi
                        {{ Form::textarea('note', null, ['class' => 'form-control m-input', 'id' => 'note', 'placeholder' => 'Nội dung trao đổi', 'required' => true]) }}
                    </div>
                </div>
            </div>
            <input type="hidden" name="customer_id" id="customer_id" />
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">
                    Lưu
                </button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>