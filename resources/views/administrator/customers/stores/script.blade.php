<script>
    // update sale
    $('.agent-selected').change(function () {
        var id = $(this).children(":selected").attr("id");
        var customer_id = $(this).children(":selected").attr("customer_id");
        $.ajax({
            url: '',
            data: {sale: id, customer_id: customer_id},
            success: function (result) {

            },
            error: function (data) {

            }
        })
    });

    //  load index
    jQuery(document).ready(function () {
        DatatableRemoteAjaxDemo.init(window.location.href);
    });

    var datatable;

    function index(url) {
        datatable = $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        dataType: 'json',
                        url: url,
                        cache: false,
                    },
                },
                pageSize: 20, // display 20 records per page
                serverPaging: true,
                serverFiltering: false,
                serverSorting: true,
            },

            // layout definition
            layout: {
                theme: 'default',
                scroll: false,
                height: null,
                footer: false,
            },

            sortable: true,

            filterable: false,

            pagination: true,

            detail: {
                title: 'Xem lịch sửa chăm sóc',
                content: childTable,
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '',
                    sortable: false,
                    width: 20,
                    textAlign: 'center' // left|right|center,
                }, {
                    field: 'fullname',
                    title: 'Họ và Tên',
                }, {
                    field: 'phone',
                    title: 'Số điện thoại',
                }, {
                    field: 'email',
                    title: 'Email',
                }, {
                    field: 'address',
                    title: 'Địa chỉ',
                }, {
                    field: 'type_customer',
                    title: 'Loại khách hàng',

                }, {
                    field: 'project_name',
                    title: 'Dự án',
                }
            ],
        });
        return datatable;
    }

    var DatatableRemoteAjaxDemo = function () {
        var demo = function (url) {
            index(url)
            $('#source_id, #level, #project_id').selectpicker();
        }

        return {
            // public functions
            init: function (url) {
                demo(url);
            },
        };
    }();


    // load child
    function childTable(dataChild) {
        $.ajax({
            url: 'history/list/' + dataChild.data.id,
            type: 'GET',
            success: function (response) {
                $('<div/>').attr('id', 'child_data_local_' + dataChild.data.id).appendTo(dataChild.detailCell).mDatatable({
                    data: {
                        type: 'local',
                        source: response.data,
                        pageSize: 10
                    },
                    // layout definition
                    layout: {
                        theme: 'default',
                        scroll: true,
                        height: 300,
                        footer: false,

                        // enable/disable datatable spinner.
                        spinner: {
                            type: 1,
                            theme: 'default',
                        },
                    },

                    sortable: true,

                    // columns definition
                    columns: [
                        {
                            field: 'sale',
                            title: 'Tư vấn viên',
                        }, {
                            field: 'phone',
                            title: 'Số điện thoại',
                        }, {
                            field: 'time_call_back',
                            title: 'Thời gian cảnh báo',
                        }, {
                            field: 'channel',
                            title: 'Kênh chăm sóc',
                        }
                        , {
                            field: 'note',
                            title: 'Ghi chú',
                        }
                    ],
                });
            }
        });
    }
</script>
