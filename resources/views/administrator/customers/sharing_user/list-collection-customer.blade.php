@extends('administrator.app')
@section('title','Danh sách khách hàng')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Danh sách khách hàng bị thu hồi
                            </h3>
                        </div>
                    </div>
                </div>
                <!-- END: Subheader -->
                <div class="m-content">
                    @include('administrator.customers.sharing_user.collection-search')
                    @include('administrator.errors.messages')
                    <div class="m-section__content m-portlet m-portlet--mobile">
                        <table class="table m-table m-table--head-bg-brand">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Tên chiến dịch
                                </th>
                                <th>
                                    Tên khách hàng
                                </th>
                                <th>
                                    Số điện thoại
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Nhân viên chăm sóc
                                </th>
                                <th>
                                    Được thu bởi
                                </th>
                                <th>
                                    Lý do thu hồi
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $key => $value)
                                <tr>
                                    <th scope="row">
                                        {{ $key }}
                                    </th>
                                    <td>
                                        {{ $value['sharing_name'] }}
                                    </td>
                                    <td>
                                        {{ $value['fullname'] }}
                                    </td>
                                    <td>
                                        {{ $value['phone'] }}
                                    </td>
                                    <td>
                                        {{ $value['email'] }}
                                    </td>
                                    <td>
                                        {{ $value['care_by'] }}
                                    </td>
                                    <td>
                                        {{ $value['created_by'] }}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#m_modal_collection" style="color: #fff; background: #f48120">
                                            Chi tiết
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('administrator.customers.customer.modelHistory')
    @include('administrator.customers.customer.script')
    <!--begin::Modal-->
    <div class="modal fade" id="m_modal_collection" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        Lý do thu hồi
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            &times;
                        </span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! @$value['message']; !!}
                    @if(@$value['file'] != NULL)
                        <a href="{{ @$value['file'] }}" class="btn btn-outline-success m-btn m-btn--icon m-btn--outline-2x">
                            <span>
                                <i class="la la-calendar-check-o"></i>
                                <span>
                                    Tải file
                                </span>
                            </span>
                        </a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <!--end::Modal-->
@stop
