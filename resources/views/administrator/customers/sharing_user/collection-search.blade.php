{{ Form::open(array('route' => 'customer.list-collection-customer', 'method' => 'GET', 'id' =>'search_form_collection')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Số điện thoại</label>
        <input type="text" name="search" id="search" class="form-control" placeholder="Số điện thoại"/>
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0 bold">
        <label>Chiến dịch</label>
        {{ Form::select('share_id', [0 => 'Lựa chọn'] + $listSharing, old('share_id'), ['class' => 'form-control', 'id' => 'share_id']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Search" class="btn btn-primary form-control"/>
    </div>
</div>
</div>
</form>