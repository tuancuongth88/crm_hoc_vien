<script>
    // update sale
    $('.agent-selected').change(function () {
        var id = $(this).children(":selected").attr("id");
        var customer_id = $(this).children(":selected").attr("customer_id");
        $.ajax({
            url: '',
            data: {sale: id, customer_id: customer_id},
            success: function (result) {

            },
            error: function (data) {

            }
        })
    });

    //  load index
    jQuery(document).ready(function () {
        DatatableRemoteAjaxDemo.init(window.location.href);
    });

    var datatable;

    function index(url) {
        datatable = $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        dataType: 'json',
                        url: url,
                    },
                },
                pageSize: 20, // display 20 records per page
                serverPaging: true,
                serverFiltering: false,
                serverSorting: true,
            },

            // layout definition
            layout: {
                theme: 'default',
                scroll: false,
                height: null,
                footer: false,
            },

            sortable: true,

            filterable: false,

            pagination: true,

            detail: {
                title: 'Xem lịch sửa chăm sóc',
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '',
                    sortable: false,
                    width: 20,
                    textAlign: 'center' // left|right|center,
                }, {
                    field: 'fullname',
                    sortable: false,
                    title: 'Người nhận',
                },
                {
                    field: 'number_share',
                    title: 'SL khách hàng nhận',
                }, {
                    field: 'number_recover',
                    title: 'SL khách hàng bị thu hồi',
                },
                {
                    field: 'sharing_name',
                    sortable: false,
                    title: 'Chiến dịch',
                },
                {
                    field: 'created_name',
                    sortable: false,
                    title: 'Người tạo',
                },
                {
                    field: 'created_at_str',
                    title: 'Ngày tạo',
                }
            ],
        });
        return datatable;
    }

    var DatatableRemoteAjaxDemo = function () {
        var demo = function (url) {
            index(url)
            $('#source_id, #level, #project_id').selectpicker();
        }

        return {
            // public functions
            init: function (url) {
                demo(url);
            },
        };
    }();

</script>
