@extends('administrator.app')
@section('title','Danh sách quản lý được chia')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Chi tiết khách hàng được chia từ quản lý
                            </h3>
                        </div>
                    </div>
                </div>
                <!-- END: Subheader -->
                <div class="m-content">
                    <!--begin::Section-->
                    <div class="m-section__content">
                        <table class="table m-table m-table--head-bg-warning">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Tên chiến dịch
                                </th>
                                <th>
                                    Tài khoản
                                </th>
                                <th>
                                    Số lượng nhận
                                </th>
                                <th>
                                    Số lượng bị thu
                                </th>
                                <th>
                                    Thời gian giao
                                </th>
                                <th>
                                    Action
                                </th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $value)
                                <tr>
                                    <th scope="row">
                                        {{ $key + 1 }}
                                    </th>
                                    <td>
                                        {{ @$value->cusSharing->name }}
                                    </td>
                                    <td>
                                        {{ @$value->user->fullname }}
                                    </td>
                                    <td>
                                        {{ @$value->number_share }}
                                    </td>
                                    <td>
                                        {{ @$value->number_recover }}
                                    </td>
                                    <td>
                                        {{ @$value->created_at }}
                                    </td>
                                    <td>
                                        <a href="{{ route('sharing-user.listShare', [$value->id]) }}"
                                           class="btn btn-primary" data-original-title="Danh sách được chia"
                                           data-toggle="m-tooltip">
                                            <i class="fa fa-list"></i>
                                        </a>
                                        <a href="{{ route('sharing-user.listRecovery', [$value->id]) }}"
                                           class="btn btn-primary" data-original-title="Danh sách thu hồi"
                                           data-toggle="m-tooltip">
                                            <i class="fa fa-recycle"></i>
                                        </a>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    {{ $data->links() }}
                    Tổng số {{ $data->total() }} bản ghi
                    <!--end::Section-->
                </div>
            </div>
        </div>
    </div>
    @include('administrator.customers.sharing_user.script')
@stop