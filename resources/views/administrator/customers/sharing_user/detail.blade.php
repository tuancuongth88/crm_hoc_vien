@extends('administrator.app')
@section('title','Danh sách khách hàng')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Danh sách khách hàng
                            </h3>
                        </div>
                    </div>
                </div>
                <!-- END: Subheader -->
                <div class="m-content">
                    <!--begin::Section-->
                    <div class="m-section__content">
                        <table class="table m-table m-table--head-bg-warning">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Tên khách hàng
                                </th>
                                <th>
                                    Số điện thoại
                                </th>
                                <th>
                                    Email
                                </th>
                                <th>
                                    Thời gian giao
                                </th>
                                @if(isset($is_list_recover))
                                    <th>
                                        Thời gian thu
                                    </th>
                                    <th>
                                        Lý do thu
                                    </th>
                                    <th>

                                    </th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 0?>
                            @foreach($data as $key => $value)
                                <tr>
                                    <th scope="row">
                                        {{ $i += 1 }}
                                    </th>
                                    <td>
                                        {{ @$value->fullname }}
                                    </td>
                                    <td>
                                        {{ @$value->phone }}
                                    </td>
                                    <td>
                                        {{ @$value->email }}
                                    </td>
                                    <td>
                                        {{ @$value->sharing_assign_at }}
                                    </td>
                                    @if(isset($is_list_recover))
                                        <td>
                                            {{ @$value->sharing_recover_at }}
                                        </td>
                                        <td>
                                            {!! @$value->recover_message !!}
                                        </td>
                                        <td>
                                            <a href="{{ @$value->file }}">Tải file</a>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('administrator.customers.sharing_user.script')
@stop