{{ Form::open(array('route' => 'sharing-user.index', 'method' => 'GET', 'id' =>'search_form_customer')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tên người nhận</label>
        <input type="text" name="search" id="search" class="form-control" placeholder="Tên người nhận"/>
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0 bold">
        <label>Chiến dịch</label>
        {{ Form::select('sharing_id',
         [0 => 'Lựa chọn'] + $listSharing, old('sharing_id'),
          ['class' => 'form-control', 'id' => 'sharing_id'])
          }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Tìm kiếm" class="btn btn-primary form-control"/>
    </div>
</div>
</form>
