<!-- The Modal -->

<div class="modal fade show" id="m_modal_4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     style="padding-right: 17px; display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Thêm mới lịch sử chăm sóc
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">
                            ×
                        </span>
                </button>
            </div>
            {{ Form::open(array('route' => 'history.store', 'class' => 'm-form m-form--fit m-form--label-align-right', 'id' => 'submit_form', 'enctype' => 'multipart/form-data')) }}
                <div class="modal-body">
                    <div class="form-group m-form__group row ">
                        <div class="col-lg-12">
                            Thêm dự án
                        </div>
                    </div>
                    <div class="form-group m-form__group row ">
                        <div class="col-lg-12">
                            <div id="m_repeater_3">
                                <div class="form-group  m-form__group row">
                                    <div data-repeater-list="project" class="col-lg-12">
                                        @if($customer->projectCustomer->count() > 0)
                                            @foreach($customer->projectCustomer as $key => $value)
                                            <div data-repeater-item class="row m--margin-bottom-10">
                                                <div class="col-lg-5">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-archive ">Dự án</i>
                                                            </span>
                                                        </div>
                                                        {!! Form::select('project_id',  [0 => 'Lựa chọn'] + $listProject, isset($value->project_id) ? $value->project_id : null, ['class' => 'form-control m-input project_id', 'id' => 'project_id', 'required' => 'required' ]) !!}

                                                    </div>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-level-up">Cấp độ </i>
                                                            </span>
                                                        </div>
                                                        {!! Form::select('level',  [0 => 'Lựa chọn'] + $levels, isset($value->level) ? $value->level : null, ['class' => 'form-control m-input']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                        <i class="la la-remove"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            @endforeach
                                        @else
                                            <div data-repeater-item class="row m--margin-bottom-10">
                                                <div class="col-lg-5">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-archive ">Dự án</i>
                                                            </span>
                                                        </div>
                                                        {!! Form::select('project_id',  [0 => 'Lựa chọn'] + $listProject, null, ['class' => 'form-control m-input project_id', 'id' => 'project_id']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-lg-5">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">
                                                                <i class="la la-level-up">Cấp độ </i>
                                                            </span>
                                                        </div>
                                                        {!! Form::select('level',  [0 => 'Lựa chọn'] + $levels, null, ['class' => 'form-control m-input']) !!}
                                                    </div>
                                                </div>
                                                <div class="col-lg-2">
                                                    <a href="#" data-repeater-delete="" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                        <i class="la la-remove"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col">
                                        <div data-repeater-create="" class="btn btn btn-primary m-btn m-btn--icon">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span> Add </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row ">
                        <div class="col-lg-12">
                            Thời gian gọi lại
                            <input type="text" class="form-control m-input" id="callback" readonly="readonly" placeholder="Thời gian gọi lại" name="time_call_back" value="{{ isset($value->time_call_back) ? $value->time_call_back : '' }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row ">
                        <div class="col-lg-12">
                            Nội dung trao đổi
                            {{ Form::textarea('note', null, ['class' => 'form-control m-input', 'placeholder' => 'Nội dung trao đổi', 'required' => true]) }}
                        </div>
                    </div>
                </div>
            <input type="hidden" name="customer_id" value="{{ $customer->id }}" />
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
                <button type="submit" class="btn btn-primary">
                    Lưu
                </button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('body').on('change', '.project_id', function() {
            if ($('select option[value="' + $(this).val() + '"]:selected').length > 1) {
                $(this).val('-1').change();
                alert('Đã tồn tại dự án')
            }
        });


        $('#callback').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
    });
</script>