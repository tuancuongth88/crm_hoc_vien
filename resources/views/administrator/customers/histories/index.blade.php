@extends('administrator.app')
@section('title','Danh sách lịch sử chăm sóc')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Danh sách lịch sử chăm sóc
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        <div class="header_history">
            <div class="col-lo-3">
                <label style="font-weight: bold;">Thông tin khách hàng</label>
            </div>
            <div class="col-lo-9">
                <div class="m-section row" style="margin-bottom: 20px">
                    <div class="col-lg-6">
                        <label style="font-weight: bold;">Họ tên: </label>
                        <label style="color: #ff0000;">{{ $customer->fullname }}</label>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: bold;">Số điện thoại: </label>
                        <label style="color: #ff0000;">{{ $customer->phone }}</label>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: bold;">Email: </label>
                        <label style="color: #ff0000;">{{ $customer->email }}</label>
                    </div>
                    <div class="col-lg-6">
                        <label style="font-weight: bold;">Địa chỉ: </label>
                        <label style="color: #ff0000;">{{ $customer->address }}</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12" style="margin-bottom: 20px; padding-top: 20px;">
            <a href="{{ route('list.index') }}" class="btn btn-primary"> <i class="fa flaticon-list"></i>Danh sách khách hàng</a>
            <button type="button" class="btn btn-brand" data-toggle="modal" data-target="#m_modal_4">
                <span>
                <i class="fa flaticon-plus"></i>
                <span>
                    Thêm mới
                </span>
            </span>
            </button>
        </div>
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <table class="table m-table m-table--head-bg-warning">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Tư vấn viên
                            </th>
                            <th>
                                Dự án
                            </th>
                            <th>
                                Số điện thoại
                            </th>
                            <th>Thời gian hẹn gọi lại</th>
                            <th>
                                Node
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($customer->histories as $key => $value)
                        <tr>
                            <th scope="row">
                                {{ $key + 1 }}
                            </th>
                            <td>
                                {{ $value->agent->username }}
                            </td>
                            <td>
                                {{ $value->agent->username }}
                            </td>
                            <td>
                            {{ $value->customer->phone }}
                            </td>
                            <td>
                                {{ $value->time_call_back }}
                            </td>
                            <td>
                                {{ $value->note }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!--end::Section-->
    </div>
    </div>
    @include('administrator.customers.histories.show')
@stop