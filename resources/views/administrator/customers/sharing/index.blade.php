@extends('administrator.app')
@section('title','Danh sách khách hàng kho')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-content">
            <div class="m-portlet">

                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Chia khách hàng
                            </h3>
                        </div>
                    </div>
                </div>
                <!-- END: Subheader -->
                <div class="m-content">
                    <!--begin::Section-->
                    @include('administrator.errors.info-search')
                    <div class="m-section ">
                        @if(session('list-fail'))
                            <div class="alert alert-danger alert-notification">
                                @foreach (session('list-fail') as $element)
                                    {{ $element }}<br>
                                @endforeach
                            </div>
                        @endif
                        @include('administrator.errors.messages')
                        @include('administrator.customers.sharing.search')
                        @include('administrator.customers.sharing.add_sharing')
                    </div>
                    <!--end::Section-->
                </div>
            </div>
        </div>
    </div>
@stop