<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="row form-group m-2">
    <div class="col-md-6 col-lg-3">
        <label class="col-form-label">
            Tên chiến dịch
        </label>
    </div>
    <div class="col-md-6">
        <input type="text"
               name="name"
               class="form-control m-input"
               placeholder="Tên chiến dịch"
               value="{{old('name')}}"
        />
    </div>
</div>
<div class="m-wizard__form-step  repeater_customer_sharing"
     id="m_wizard_form_step_2">
    <div class="" data-repeater-list="list_user_sharing">
        <div class="row form-group border p-3 class-repeater-item m-0" data-repeater-item>
            <div class="col-md-3 align-self-center">
                <div class="row form-group">
                    <div class="col-md-6 col-lg-3">
                        <label class="col-form-label">
                            Số lượng
                        </label>
                    </div>
                    <div class="col-md-6">
                        <input type="number"
                               name="number_share"
                               class="form-control m-input"
                               id="number_share"
                               placeholder="Số lượng"
                        />
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="row form-group">
                    <div class="col-md-6 col-lg-3">
                        <label class="col-form-label">
                            Nhóm quyền
                        </label>
                    </div>
                    <div class="col-md-6 col-lg-8">
                        <select class="form-control m-input drl_roles"
                                name="role_id">
                            <option value="0">Tất cả</option>
                            @foreach ($listRoles as $key_l => $value_l)
                                <option value="{{ $key_l }}">
                                    {{ $value_l }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="row form-group">
                    <div class="col-md-6 col-lg-3">
                        <label class="col-form-label">
                            User
                        </label>
                    </div>
                    <div class="col-md-6 col-lg-8">
                        <select class="form-control m-input slt_lst_user" name="user_id">
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-lg-2 align-self-center">
                <button class="btn btn-primary" type="button"
                        data-repeater-delete
                        style="background-color:rgb(255,15,0);">
                    Xóa
                </button>
            </div>
        </div>
    </div>
    <div class="row d-inline-block float-right m-2">
        <div class="col">
            <button data-repeater-create class="btn btn-primary "
                    type="button">
                Thêm dòng
            </button>
        </div>
    </div>
</div>
<div class="row d-block m-2" style="clear: both">
    <div class="col-md-2">
        <input type="submit" value="Chia dữ liệu" class="btn btn-primary form-control"/>
    </div>
</div>
</form>

<script>
    $(document).ready(function () {
        initGetUserByRole();
        $('#customer_type').on('change', function () {
            initCountCustomer();
        });
        $('#customer_group').on('change', function () {
            initCountCustomer();
        });
    });

    function initGetUserByRole() {
        $('.drl_roles').on('change', function () {
            var that = this;
            var role_id = $(that).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: './sharing/user-by-role/' + role_id,
                cache: false,
                type: "post",
                processData: false,
                contentType: false,
                success: function (response) {
                    $(that).closest('.class-repeater-item').find('.slt_lst_user').empty();
                    $(that).closest('.class-repeater-item').find('.slt_lst_user').empty().append(response.data);
                },
                error: function (data) {

                }
            });

        })
    }

    function initCountCustomer() {
        var customer_type = $('#customer_type').val();
        var customer_group = $('#customer_group').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: './sharing/count-customer',
            cache: false,
            type: "post",
            data: {cus_type: customer_type, cus_group: customer_group},
            success: function (response) {
                $('.totalRecord').text(response.data)
            },
            error: function (data) {

            }
        });
    }

</script>