<script>
    // update sale
    $('.agent-selected').change(function () {
        var id = $(this).children(":selected").attr("id");
        var customer_id = $(this).children(":selected").attr("customer_id");
        $.ajax({
            url: '',
            data: {sale: id, customer_id: customer_id},
            success: function (result) {

            },
            error: function (data) {

            }
        })
    });

    //  load index
    jQuery(document).ready(function () {
        DatatableRemoteAjaxDemo.init(window.location.href);
    });

    var datatable;

    function index(url) {
        datatable = $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        dataType: 'json',
                        url: url,
                    },
                },
                pageSize: 20, // display 20 records per page
                serverPaging: true,
                serverFiltering: false,
                serverSorting: true,
            },

            // layout definition
            layout: {
                theme: 'default',
                scroll: false,
                height: null,
                footer: false,
            },
            sortable: true,

            filterable: false,

            pagination: true,

            detail: {
                title: 'Xem lịch sửa chăm sóc',
                content: childTable,
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '',
                    sortable: false,
                    width: 20,
                    textAlign: 'center' // left|right|center,
                }, {
                    field: 'name',
                    title: 'Tên chiến dịch',
                    sortable: true,
                }, {
                    field: 'created_by.fullname',
                    sortable: false,
                    title: 'Người tạo',
                },
                {
                    field: 'total_number_share',
                    title: 'Tổng số lượng chia',
                    sortable: false,
                }, {
                    field: 'total_number_recover',
                    sortable: false,
                    title: 'Tổng số thu hồi',
                },
                {
                    field: 'created_at_str',
                    title: 'Ngày tạo',
                }
            ],
        });
        return datatable;
    }

    var DatatableRemoteAjaxDemo = function () {
        var demo = function (url) {
            index(url)
            $('#source_id, #level, #project_id').selectpicker();
        }

        return {
            // public functions
            init: function (url) {
                demo(url);
            },
        };
    }();


    // load child
    function childTable(dataChild) {
        $.ajax({
            url: 'sharing-user/' + dataChild.data.id,
            type: 'GET',
            success: function (response) {
                $('<div/>').attr('id', 'child_data_local_' + dataChild.data.id).appendTo(dataChild.detailCell).mDatatable({
                    data: {
                        type: 'local',
                        source: response.data,
                        pageSize: 10
                    },
                    // layout definition
                    layout: {
                        theme: 'default',
                        scroll: true,
                        height: 300,
                        footer: false,

                        // enable/disable datatable spinner.
                        spinner: {
                            type: 1,
                            theme: 'default',
                        },
                    },

                    sortable: true,

                    // columns definition
                    columns: [
                        {
                            field: 'fullname',
                            title: 'Người nhận',
                        }, {
                            field: 'number_share',
                            title: 'SL khách hàng đã nhận',
                        }, {
                            field: 'number_recover',
                            title: 'SL khách hàng bị thu',
                        }
                    ],
                });
            }
        });
    }

    // add history
    $('body').on('click', '.add_row_child', function () {
        $('#phone').val("");
        $('#callback').val("");
        $('#note').val("");
        $('#channel').val(0);
        var id = $(this).attr('id');
        $('#customer_id').val(id);
        $.ajax({
            url: 'history/' + id,
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                console.log(data);
                $('#m_modal_4').modal('toggle');
                $('#phone').val(data.customer.phone);
            }

        })
    });

    jQuery(document).ready(function () {
        $('#callback').datetimepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy h:i:s',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

        $('#submit_form_history').submit(function (e) {
            e.preventDefault();
            var time_call_back = $('#callback').val();
            var phone = $('#phone').val();
            var note = $('#note').val();
            var customer_id = $('#customer_id').val();
            var channel = $('#channel').val();
            var dataForm = {
                time_call_back: time_call_back,
                phone: phone,
                note: note,
                customer_id: customer_id,
                channel: channel
            };
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ route('customer.history.save') }} ',
                type: 'POST',
                data: dataForm,
                success: function (data) {
                    alert(data.message);
                    $('#m_modal_4').modal('hide');
                    datatable.reload();
                }
            });
        });

        $('body').on('click', '.delete_history_id', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var check_confirm = confirm("bạn muốn xóa khách hàng này khỏi hệ thống?");
            if (check_confirm) {
                var id = $(this).attr('id');
                $.ajax({
                    url: '/administrator/customer/history/' + id,
                    type: 'DELETE',
                    dataType: "json",
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function (result) {
                        alert(result.message);
                        console.log(result);
                        index();
                        $('#child_data_local').mDatatable('reload');
                    }
                });
            }
        });

        $('body').on('click', '.delete_customer_id', function (e) {
            e.preventDefault();
            var check_confirm = confirm("bạn muốn xóa khách hàng này khỏi hệ thống?");
            if (check_confirm) {
                var id = $(this).attr('id');
                $.ajax({
                    url: 'list/' + id,
                    type: 'DELETE',
                    dataType: "json",
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function (result) {
                        alert(result.message);
                        console.log(result);
                        index();
                        $('#child_data_local').mDatatable('reload');
                    }
                });
            }
        });
    });
</script>
