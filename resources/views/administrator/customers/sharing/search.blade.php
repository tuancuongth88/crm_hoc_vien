{{ Form::open(array('route' => 'sharing.store', 'method' => 'POST', 'id' =>'customer-sharing')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-3 col-sm-6 mb-3 mb-md-0 bold">
        <label>Nhóm khách hàng</label>
        {{ Form::select('group',
         ['' => 'Tất cả'] + $listCusGroup, old('group'),
          ['class' => 'form-control', 'id' => 'customer_group'])
          }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0 bold">
        <label>Loại khách hàng</label>
        {{ Form::select('type',
         [0 => 'Tất cả'] + $listCusType, old('type'),
          ['class' => 'form-control ', 'id' => 'customer_type'])
          }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Danh sách chiến dịch</label>
        <a class="btn btn-primary" href="sharing/list">Danh sách chiến dịch</a>
    </div>
</div>
<div class="font-weight-bold">
    Tổng số khách hàng chưa chia: <span class="totalRecord">{{$totalRecord}}</span>
</div>