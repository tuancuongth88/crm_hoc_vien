<script>
    // update sale
    $('.agent-selected').change(function () {
        var id = $(this).children(":selected").attr("id");
        var customer_id = $(this).children(":selected").attr("customer_id");
        $.ajax({
            url: '',
            data: {sale: id, customer_id: customer_id},
            success: function (result) {

            },
            error: function (data) {

            }
        })
    });

    //  load index
    jQuery(document).ready(function() {
        DatatableRemoteAjaxDemo.init(window.location.href );
    });

    var datatable;
    function index(url) {
        datatable = $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        dataType: 'json',
                        url: url,
                    },
                },
                pageSize: 20, // display 20 records per page
                serverPaging: true,
                serverFiltering: false,
                serverSorting: true,
            },

            // layout definition
            layout: {
                theme: 'default',
                scroll: false,
                height: null,
                footer: false,
            },

            sortable: true,

            filterable: false,

            pagination: true,

            detail: {
                title: 'Xem lịch sửa chăm sóc',
                content: childTable,
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '',
                    sortable: false,
                    width: 20,
                    textAlign: 'center' // left|right|center,
                }, {
                    field: 'fullname',
                    title: 'Tên khách hàng',
                }, {
                    field: 'phone',
                    title: 'Số điện thoại',
                }, {
                    field: 'email',
                    title: 'Email',
                },
                {
                    field: 'address',
                    title: 'Địa chỉ',
                },
                {
                    field: 'level',
                    title: 'Loại khách hàng',

                },
                {
                    field: 'type',
                    title: 'Loại dữ liệu',
                },
                {
                    field: 'Actions',
                    width: 110,
                    title: 'Actions',
                    sortable: false,
                    overflow: 'visible',
                    template: function (row, index, datatable) {
                        var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                        var html = "";
                        if(row.delete_edit_add_history == 1){
                            html += '<div class="dropdown ' + dropup + '">\
                                        <a  class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill add_row_child" id="'+row.id+'" title="Thêm lịch sử chăm sóc">\
                                            <i class="fa flaticon-plus"></i>\
                                        </a>\
                                    </div>\
                                    <a href="{{ url()->current() }}/'+ row.id +'/edit" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Sửa khách hàng">\
                                        <i class="la la-edit"></i>\
                                    </a>\
                                   <a class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill delete_customer_id" id="'+row.id+'" title="Xóa khách hàng" >\
                                        <i class="la la-trash"></i>\
                                    </a>';
                        }
                        @if (\Auth::user()->can('is-telesale'))
                            if(row.share_id != null){
                                html += '<a href="{{ url()->current() }}/'+ row.id +'/collection-customer" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Thu lại khách hàng">' +
                                '<i class="la la-lock"></i> + </a>';
                            }
                        @endif
                        return html;
                    },
                }],
        });
        return datatable;
    }
    var DatatableRemoteAjaxDemo = function() {
        var demo = function(url) {
            index(url)
            $('#source_id, #level, #project_id').selectpicker();
        }

        return {
            // public functions
            init: function(url) {
                demo(url);
            },
        };
    }();


    // load child
    function childTable(dataChild) {
        $.ajax({
           url:  'history/list/'+dataChild.data.id,
           type: 'GET',
           success: function (response) {
                $('<div/>').attr('id', 'child_data_local_' + dataChild.data.id).appendTo(dataChild.detailCell).mDatatable({
                    data: {
                        type: 'local',
                        source: response.data,
                        pageSize: 10
                    },
                    // layout definition
                    layout: {
                        theme: 'default',
                        scroll: true,
                        height: 300,
                        footer: false,

                        // enable/disable datatable spinner.
                        spinner: {
                            type: 1,
                            theme: 'default',
                        },
                    },

                    sortable: true,

                    // columns definition
                    columns: [
                         {
                            field: 'sale',
                            title: 'Tư vấn viên',
                        }, {
                            field: 'phone',
                            title: 'Số điện thoại',
                        }, {
                            field: 'time_call_back',
                            title: 'Thời gian cảnh báo',
                        }, {
                            field: 'channel',
                            title: 'Kênh chăm sóc',
                        }
                        , {
                            field: 'note',
                            title: 'Ghi chú',
                        }
                        , {
                            field: 'created_at',
                            title: 'Ngày tạo',
                        }, {
                            field: 'Actions',
                            width: 110,
                            title: 'Actions',
                            sortable: false,
                            overflow: 'visible',
                            template: function (row, index, datatable) {
                                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                                if(row.delete_edit_add_history == 1){
                                    return '<a href="{{ url()->current() }}/'+ row.id +'/edit" id="'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill delete_history_id" title="Xóa">\
                                        <i class="la la-trash"></i></a>';
                                }
                            },
                        }
                    ],
                });
           }
        });
    }

    // add history
    $('body').on('click', '.add_row_child', function() {
        $('#phone').val("");
        $('#callback').val("");
        $('#note').val("");
        $('#channel').val(0);
        $('#status').val(0);
        var id = $(this).attr('id');
        $('#customer_id').val(id);
        $.ajax({
            url: 'history/'+id,
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                console.log(data);
                $('#m_modal_4').modal('toggle');
                $('#phone').val(data.customer.phone);
            }

        })
    });

    jQuery(document).ready(function() {
        $('.m-select2').select2();
        $('#callback').datetimepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy h:i:s',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

        $('#submit_form_history').submit(function (e) {
            e.preventDefault();
            var time_call_back = $('#callback').val();
            var phone = $('#phone').val();
            var note = $('#note').val();
            var customer_id = $('#customer_id').val();
            var channel = $('#channel').val();
            var status = $('#status').val();
            var dataForm = { time_call_back: time_call_back, phone: phone, note: note, customer_id: customer_id, channel: channel, status: status };
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ route('customer.history.save') }} ',
                type: 'POST',
                data: dataForm,
                success: function (data) {
                    alert(data.message);
                    $('#m_modal_4').modal('hide');
                    datatable.reload();
                }
            });
        });

        $('body').on('click', '.delete_history_id', function(e) {
           e.preventDefault();
           var id = $(this).attr('id');
            var check_confirm = confirm("bạn muốn xóa khách hàng này khỏi hệ thống?");
            if(check_confirm){
                var id = $(this).attr('id');
                $.ajax({
                    url: '/administrator/customer/history/'+id,
                    type: 'DELETE',
                    dataType: "json",
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function (result) {
                        alert(result.message);
                        console.log(result);
                        index();
                        $('#child_data_local').mDatatable('reload');
                    }
                });
            }
        });

        $('body').on('click', '.delete_customer_id', function(e) {
            e.preventDefault();
            var check_confirm = confirm("bạn muốn xóa khách hàng này khỏi hệ thống?");
            if(check_confirm){
                var id = $(this).attr('id');
                $.ajax({
                    url: 'list/'+id,
                    type: 'DELETE',
                    dataType: "json",
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function (result) {
                        alert(result.message);
                        console.log(result);
                        index();
                        $('#child_data_local').mDatatable('reload');
                    }
                });
            }
        });
    });

    function disableselect(e){
        return false
    }
    function reEnable(){
        return true
    }
    //if IE4+
    document.onselectstart=new Function ("return false")
    //if NS6
    if (window.sidebar){
        document.onmousedown=disableselect
        document.onclick=reEnable
    }
</script>
