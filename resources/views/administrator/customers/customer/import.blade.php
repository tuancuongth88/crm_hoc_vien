@extends('administrator.app')
@section('title','Thêm mới khách hàng')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet col-xl-8 offset-xl-2">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Import dữ liệu khách hàng
                                <small>
                                    <a href="{{ url('/file_import_khach_hang.xlsx') }}">Mẫu import File</a>
                                </small>
                            </h3>
                        </div>
                    </div>
                </div>
                @include('administrator.errors.errors-validate')
                    <!--begin::Form-->
                {{ Form::open(array('route' => 'customer.post-import', 'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
                    <div class="m-portlet__body">
                        <div class="m-form__heading">
                            <h3 class="m-form__heading-title">

                            </h3>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12">
                                Chọn tệp
                            </label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="inputGroupFile01" name="import_file" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                                    <label class="custom-file-label" for="inputGroupFile01">Chọn file</label>
                                  </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12">
                                Chọn nhân viên
                            </label>
                            <div class="col-lg-4 classol-md-9 col-sm-12">
                                {!!Form::select('sale_id',  ['' => '---Chọn sale---', ] + $listUser , old('param'), ['class' => 'form-control list_sale m-select2'])!!}
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <label class="col-form-label col-lg-3 col-sm-12">
                                Chọn dự án
                            </label>
                            <div class="col-lg-4 col-md-9 col-sm-12">
                                <select class="form-control m-select2" name="project_id" id="select-project">
                                    <option value="">---Lựa chọn---</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-8 offset-lg-4">
                                    <button class="btn btn-primary">
                                        LƯU LẠI
                                    </button>
                                    <input type="reset" value="XÓA DỮ LIỆU" class="btn btn-danger">
                                </div>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
                    <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        jQuery(document).ready(function() {
            $('.m-select2').select2();
            $('.list_sale').change(function (e) {
                e.preventDefault();
                $('#select-project').empty();
                var id = $(this).val();
                if(id > 0){
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        type: 'GET',
                        url: 'project-by-sale/'+id,
                        dataType: "json",
                        success: function (result) {
                            $('#select-project').append('<option value="">---Lựa chọn---</option>');
                            $.each(result.data, function (i, item) {
                                $('#select-project').append($('<option>', {
                                    value: i,
                                    text : item
                                }));
                            });
                        }

                    })
                }else{
                    $('#select-project').append('<option value="">---Lựa chọn---</option>');
                }
            });
        });
    </script>
@stop