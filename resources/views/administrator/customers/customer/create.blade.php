@extends('administrator.app')
@section('title','Thêm mới khách hàng')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Thêm mới khách hàng
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <a href="{{ route('list.index') }}" class="btn btn-success">Danh sách khách hàng</a>
                <a href="{{ route('list.create') }}" class="btn btn-primary">Thêm khách hàng</a>
            </div>
            <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Thêm mới khách hàng
                                </h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => 'list.store', 'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
                    <div class=" form-group m-form__group row ">
                        <div class="col-lg-4">
                            <label for="name">
                                Tên <span style="color: red;">(*)</span>
                            </label>
                            <input type="text" class="form-control m-input" id="fullname" placeholder="Tên khách hàng" name="fullname" value="{{ old('fullname') }}">
                        </div>
                        <div class=" col-lg-4">
                            <label for="name">
                                Hòm thư <span style="color: red;">(*)</span>
                            </label>
                            <input type="email" class="form-control m-input" id="phone" placeholder="Email" name="email" value="{{ old('email') }}">
                        </div>
                    </div>
                    <div class=" form-group m-form__group row ">
                        <div class="col-lg-4">
                            <label for="name">
                                Số điện thoại <span style="color: red;">(*)</span>
                            </label>
                            <input type="text" class="form-control m-input" id="phone" placeholder="Số điện thoại" name="phone" value="{{ old('phone') }}">
                        </div>
                        <div class="col-lg-4">
                            <label for="name">
                                Ngày sinh
                            </label>
                            <input type="text" class="form-control m-input" id="birthday" readonly="readonly" placeholder="Ngày sinh" name="birthday" value="{{ old('birthday') }}">
                        </div>
                    </div>
                    <div class="form-group m-form__group row ">
                        <div class="col-lg-4">
                            <label for="name">
                                Đối tượng khách hàng <span style="color: red;">(*)</span>
                            </label>
                            <select class="form-control m-input" id="type" name="type">
                                @foreach($type as $item)
                                    <option value="{{ $item['id'] }}">
                                        {{ $item['name'] }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="name">
                                Địa chỉ
                            </label>
                            <input type="text" class="form-control m-input" id="address" placeholder="Địa chỉ khách hàng" name="address" value="{{ old('address') }}">
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="name">
                                Ảnh đại diện
                            </label>
                            <input type="file" class="form-control m-input" id="avatar" placeholder="Địa chỉ khách hàng" name="avatar" value="{{ old('avatar') }}">
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions">
                            <button class="btn btn-primary">
                                Lưu
                            </button>
                            <a class="btn btn-secondary" href="{{ route('list.index') }}">
                                Trở về danh sách
                            </a>
                        </div>
                    </div>
                    {{-- </form> --}}
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#birthday').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
    });
</script>
@stop