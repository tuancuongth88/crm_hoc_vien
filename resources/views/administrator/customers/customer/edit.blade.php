@extends('administrator.app')
@section('title','Cập nhật khách hàng')

@section('content')
    <style type="text/css">
        input[required] + span {
            color: #999;
            font-family: Arial;
            font-size: 1em;
            position: relative;
            top: -28px;
            padding-left: 10px;
        }

        input[required] + span:after {
            content:'(*)';
            color: red;
        }

        /* show the placeholder when input has no content (no content = invalid) */
        input[required]:invalid + span {
            display: inline-block;
        }

        /* hide the placeholder when input has some text typed in */
        input[required]:valid + span {
            display: none;
        }
        .no_bottom {
            padding-bottom: 0px !important;
        }
    </style>
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<div class="m-content">
			<!--begin::Portlet-->
			<div class="m-portlet col-xl-8 offset-xl-2">
				<div class="m-portlet__head">
					<div class="m-portlet__head-caption">
						<div class="m-portlet__head-title">
							<h3 class="m-portlet__head-text">
								Cập nhật khách hàng
							</h3>
						</div>
					</div>
				</div>
			@include('administrator.errors.errors-validate')
			<!--begin::Form-->
				{{ Form::open(array('route' => array('list.update', 'id' => $data->id), 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
                <input type="hidden" name="id" value="{{ $data->id }}">
				<meta name="csrf-token" content="{{ csrf_token() }}">
				<div class="m-portlet__body">
					<div class="m-form__heading">
						<h3 class="m-form__heading-title">
							Thông tin cơ bản của khách hàng
						</h3>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-xl-5 col-lg-5">
                                <input type="text" name="last_name" class="form-control m-input" value="{{ $data->last_name }}" id="last_name">
                            <span class="placeName">Họ:</span>
						</div>
						<div class="col-xl-4 col-lg-4">
                            <input type="text" name="first_name" class="form-control m-input" value="{{ $data->first_name }}" id="first_name">
                            <span class="placeName">Tên:</span>
						</div>
						<div class="col-xl-3 col-lg-3">
							{{ Form::select('sex', \App\Models\Customers\Customer::$listSex, $data->sex, ['class' => 'form-control m-input']) }}
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-xl-5 col-lg-5">
							<input type="text" name="cmt" class="form-control m-input" placeholder="Số CMND/Hộ chiếu" value="{{ $data->cmt }}">
						</div>
                        <div class="col-xl-4 col-lg-4">
                            <input type="text" name="noi_cap_cmt" class="form-control m-input" placeholder="Nơi cấp" value="{{ $data->noi_cap_cmt }}" id="noi_cap_cmt">
                        </div>
                        <div class="col-xl-3 col-lg-3">
                            <input type="text" name="ngay_cap_cmt" id="ngay_cap_cmt" class="form-control m-input" placeholder="Ngày cấp" value="{{ $data->ngay_cap_cmt }}">
                        </div>
					</div>
					<div class="form-group m-form__group row">
                        <div class="col-xl-5 col-lg-5">
                            <input type="text" name="phone" class="form-control m-input" value="{{ $data->phone }}" id="phone">
                            <span class="placeName">Điện thoại liên hệ:</span>
                        </div>
                        <div class="col-xl-4 col-lg-4">
                            <input type="text" name="email" placeholder="Email khách hàng:" class="form-control m-input" value="{{ $data->email }}" id="email">

                        </div>
                        <div class="col-xl-3 col-lg-3">
                            <input type="text" name="birthday" class="form-control m-input" placeholder="Ngày sinh" value="{{ $data->birthday }}" id="birthday">
                        </div>
					</div>
					<div class="form-group m-form__group row">
					    <div class="col-xl-5 col-lg-5">
					        <label class="text-left align-middle mt-3">Tỉnh/thành phố: <span class="required">(*)</span></label>
					        {{ Form::select('city_id', ['' => 'Lựa chọn'] + $listCity, $data->city_id, ['class' => 'form-control m-input', 'id' => 'city_id']) }}
					    </div>
					    <div class="col-xl-7 col-lg-7">
					        <label class="text-right align-middle mt-3">Quận/Huyện: <span class="required">(*)</span></label>
					        {{ Form::select('district_id', $listDistrict, $data->district_id, ['class' => 'form-control m-input', 'id' => 'district_id']) }}
					    </div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-xl-12 col-lg-12">
							<input type="text" name="address" class="form-control m-input" placeholder="Địa chỉ" value="{{ $data->address }}">
						</div>
					</div>
					<div class="form-group m-form__group row">
                            <div class="col-xl-12 col-lg-12">
                                <label class="text-left align-middle mt-3">Khoảng tài chính: </label>
                                <input type="hidden" id="finance" name="finance" />
                            </div>
                        </div>
                    <div class="form-group m-form__group row">
                        <div class="col-xl-12 col-lg-12">
                             <label class="text-left align-middle mt-3">Sở thích: </label>
                            <textarea class="summernote" name="info_hobby" id="content">{{ $data->info['so_thich'] }}</textarea>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-xl-12 col-lg-12">
                             <label class="text-left align-middle mt-3">Nhu cầu: </label>
                            <textarea class="summernote" name="info_need" id="content">{{ $data->info['nhu_cau'] }}</textarea>
                        </div>
                    </div>
					<div class="m-form__heading" style="margin-top:20px">
						<h3 class="m-form__heading-title">
							Phân loại khách hàng
						</h3>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-xl-6 col-lg-6">
							<label style="margin-bottom:10px">Phân loại khách hàng</label>
							{{ Form::select('level', $levels, $projectByCustomer->count() > 0 ? $projectByCustomer->first()->level : null, ['class' => 'form-control m-input']) }}
						</div>
						<div class="col-xl-6 col-lg-6">
							<label style="margin-bottom:10px">Khách hàng của dự án</label>
							<div class="m-checkbox-list">
								@foreach($listProject as $key => $value)
									<label class="m-checkbox">
										<input id="project_id" name="project_id[]" {{ in_array($key, $arrProject) ? 'checked' : '' }}  type="checkbox" value="{{ $key }}"  />
										{{ $value }}
										<span></span>
									</label>
								@endforeach
							</div>
						</div>
					</div>
					<div class="form-group m-form__group row">
						<div class="col-xl-6 col-lg-6">
							<label style="margin-bottom:10px">Nguồn khách từ kênh nào</label>
							<div class="m-radio-list" id="toastTypeGroup">
								@foreach(\App\Models\Customers\Customer::$sourceCustomer as $key => $value)
									<label class="m-radio">
										<input type="radio" name="source_id" checked value="{{ $key }}"  />
										{{ $value }}
										<span></span>
									</label>
								@endforeach
							</div>
						</div>
						<div class="col-xl-6 col-lg-6">
							<label style="margin-bottom:10px">Kênh marketing</label>
							<input type="text" name="channel" class="form-control m-input" placeholder="Nhập kênh marketing ra data khách này" value="{{ $data->channel }}">
						</div>
					</div>
					<div class="m-form__heading" style="margin-top:20px">
                            <h3 class="m-form__heading-title">
                                Người thân
                            </h3>
                            <div id="strong">
                                <div class="form-group m-form__group row">
                                    <div data-repeater-list="director" class="col-lg-12">
                                		@if ($data->children->count() == 0)
                                        <div data-repeater-item class="form-group m-form__group row align-items-center">
                                            <div class="col-md-10 ">
                                               <div class="row">
                                                   <div class="col-md-6">
                                                       {{ Form::select('relation', ['' => 'Lựa chọn'] + App\Models\Customers\Customer::$listRelation, null, ['class' => 'form-control m-input', 'style' => 'margin-bottom: 7px;']) }}
                                                   </div>
                                                   <div class="col-md-6">
                                                        {{ Form::select('sex_child', \App\Models\Customers\Customer::$listSex, null, ['class' => 'form-control m-input']) }}

                                                   </div>
                                               </div>
                                               <div class="row">
                                                   <div class="col-md-6">
                                                        <input type="text" class="form-control m-input" placeholder="Họ" size="50" name="last_name_child" style="margin-bottom: 7px">

                                                   </div>
                                                   <div class="col-md-6">
                                                       <input type="text" class="form-control m-input" placeholder="Tên" size="50" name="first_name_child" style="margin-bottom: 7px">
                                                   </div>
                                               </div>
                                               <div class="row">
                                                   <div class="col-md-6">
                                                        <input type="email" class="form-control m-input" placeholder="Email" size="50" name="email_child" style="margin-bottom: 7px">
                                                   </div>
                                                   <div class="col-md-6">
                                                        <input type="number" class="form-control m-input" placeholder="Điện thoại" size="50" name="phone_child" style="margin-bottom: 7px">
                                                   </div>
                                               </div>
                                               <div class="row">
                                                   <div class="col-md-6">
                                                        <input type="text" name="cmt_child" class="form-control m-input" placeholder="Số CMND/Hộ chiếu" value="">
                                                   </div>
                                                   <div class="col-md-6">
                                                       <div class="input-group date">
                                                            <input type="text" class="form-control m-input datetime-picker birthday_child" placeholder="Ngày sinh" readonly name="birthday_child" id="birthday_child" />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar glyphicon-th"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                   </div>
                                               </div>

                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>

                                            <div class="col-md-2">
                                                <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
                                                    <span>
                                                        <i class="la la-trash-o"></i>
                                                        <span>
                                                            Xóa
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    	@endif
                                    	@foreach ($data->children as $director)
                                    	<div data-repeater-item class="form-group m-form__group row align-items-center">
                                            <div class="col-md-10 ">
                                               <div class="row">
                                                   <div class="col-md-6">
                                                       {{ Form::select('relation', ['' => 'Lựa chọn'] + App\Models\Customers\Customer::$listRelation, $director->relation, ['class' => 'form-control m-input', 'style' => 'margin-bottom: 7px;']) }}
                                                   </div>
                                                   <div class="col-md-6">
                                                        {{ Form::select('sex_child', \App\Models\Customers\Customer::$listSex, $director->sex_child , ['class' => 'form-control m-input']) }}

                                                   </div>
                                               </div>
                                               <div class="row">
                                                   <div class="col-md-6">
                                                        <input type="text" class="form-control m-input" placeholder="Họ" size="50" name="last_name_child" value="{{ $director->last_name }}" style="margin-bottom: 7px">
                                                   </div>
                                                   <div class="col-md-6">
                                                       <input type="text" class="form-control m-input" placeholder="Tên" size="50" name="first_name_child" value="{{ $director->first_name }}" style="margin-bottom: 7px">
                                                   </div>
                                               </div>
                                               <div class="row">
                                                   <div class="col-md-6">
                                                        <input type="email" class="form-control m-input" placeholder="Email" size="50" name="email_child" value="{{ $director->email }}" style="margin-bottom: 7px">
                                                   </div>
                                                   <div class="col-md-6">
                                                        <input type="number" class="form-control m-input" placeholder="Điện thoại" size="50" name="phone_child" value="{{ $director->phone }}" style="margin-bottom: 7px">
                                                   </div>
                                               </div>
                                               <div class="row">
                                                   <div class="col-md-6">
                                                        <input type="text" name="cmt_child" class="form-control m-input" value="{{ $director->cmt }}" placeholder="Số CMND/Hộ chiếu" value="">
                                                   </div>
                                                   <div class="col-md-6">
                                                       <div class="input-group date">
                                                            <input type="text" class="form-control m-input datetime-picker birthday_child" placeholder="Ngày sinh" readonly value="{{  date('d-m-Y', strtotime($director->birthday)) }}" name="birthday_child" id="birthday_child" />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">
                                                                    <i class="la la-calendar glyphicon-th"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                   </div>
                                               </div>

                                                <div class="d-md-none m--margin-bottom-10"></div>
                                            </div>

                                            <div class="col-md-2">
                                                <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
                                                    <span>
                                                        <i class="la la-trash-o"></i>
                                                        <span>
                                                            Xóa
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    	@endforeach

                                    </div>

                                </div>
                                <div class="m-form__group form-group row">
                                    <label class="col-lg-2 col-form-label"></label>
                                    <div class="col-lg-4">
                                        <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                                            <span>
                                                <i class="la la-plus"></i>
                                                <span>
                                                    Thêm mới
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
				</div>
				<div class="m-portlet__foot m-portlet__foot--fit">
					<div class="m-form__actions m-form__actions">
						<div class="row">
							<div class="col-lg-8 offset-lg-4">
								<button class="btn btn-primary">
									LƯU LẠI
								</button>
								<input type="reset" value="XÓA DỮ LIỆU" class="btn btn-danger">
							</div>
						</div>
					</div>
				</div>
			{{ Form::close() }}
			<!--end::Form-->
			</div>
			<!--end::Portlet-->
		</div>
	</div>
	<script>
        jQuery(document).ready(function() {
            function  getDistrictByCity(id) {
                $.ajax({
                    url: '/administrator/customer/district/'+id,
                    type: 'GET',
                    contentType: "application/json",
                    success: function (result) {
                        $('select[name="district_id"]').empty();
                        $.each(result.data, function(key, value) {
                            $('select[name="district_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });
                    }
                })
            }

            $('#city_id').change(function (e) {
                var id = $(this).val();
                if(id == ""){
                    $('select[name="district_id"]').empty();
                    $('select[name="district_id"]').append('<option value="">---Lựa chọn---</option>');
                }else{
                    getDistrictByCity(id);
                }
            });

            $('#ngay_cap_cmt').datepicker({
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

            $('#finance').ionRangeSlider({
                type: "double",
                min: 0,
                max: 20000000000,
                step: 1000000000,
                from: '{{ ($data->finance_min) ? $data->finance_min : 0 }}',
            	to: '{{ ($data->finance_max) ? $data->finance_max : 0 }}',
                // prefix: ": ",
                decorate_both: true
            });

            $('#birthday').datepicker({
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

            $('#birthday_child').datepicker({
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

            $('#strong').repeater({
                initEmpty: false,
                show: function () {
                    $(this).slideDown();
                    $(this).find('input.datetime-picker').datepicker({
                        todayHighlight: false,
                        format: 'dd-mm-yyyy',
                        autoclose: true,
                        orientation: "bottom left",
                        templates: {
                            leftArrow: '<i class="la la-angle-left"></i>',
                            rightArrow: '<i class="la la-angle-right"></i>'
                        }
                    });
                    console.log($(this));
                },

                hide: function(deleteElement) {
                    if(confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                }
            });

            $('.summernote').summernote({
                    callbacks: {
                        onImageUpload: function(image) {
                            uploadImageProject(image[0], $(this).attr('id'));
                        }
                    },
                    height: 150,
                    toolbar: [
                        // [groupName, [list of button]]
                        ['style', ['bold', 'italic', 'underline', 'clear']],
                        ['font', ['strikethrough']],
                        ['fontsize', ['fontsize']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['insert', ['picture']],
                        ['table', ['table']],
                        ['view', ['fullscreen']],
                        ['help', ['help']]
                    ]
                });

            $("#last_name").attr("required", "true");
            $("#first_name").attr("required", "true");
            $("#phone").attr("required", "true");
            $('.input-placeholder input').siblings('.placeholder').hide();
            $('.input-placeholder input')
                .on( 'focus', function () { $(this).siblings('.placeholder').hide(); } )
                .on( 'blur',  function () { if ( !$(this).val()) $(this).siblings('.placeholder').show(); } );


            $('.summernote').summernote({
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImageProject(image[0], $(this).attr('id'));
                    }
                },
                height: 150,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['picture']],
                    ['table', ['table']],
                    ['view', ['fullscreen']],
                    ['help', ['help']]
                ]
            });

            });
        function uploadImageProject(image, el) {

                var data = new FormData();
                data.append("image", image);
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
                $.ajax({
                    url: '/administrator/upload',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(response) {
                        if (response.status) {
                            var image = $('<img width="100%">').attr('src', response.data);
                            $('#' + el).summernote("insertNode", image[0]);
                        }
                    },
                    error: function(data) {

                    }
                });
            }
	</script>
@stop