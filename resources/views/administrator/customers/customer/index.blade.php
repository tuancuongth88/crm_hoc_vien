@extends('administrator.app')
@section('title','Danh sách khách hàng')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-content">
        <div class="m-portlet">

            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Danh sách khách hàng
                        </h3>
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->
            <div class="m-content">
            @include('administrator.customers.customer.search')
            <!--begin::Section-->
                <div class="col-xs-12" style="margin-bottom: 20px">
                    <a href="{{ route('list.create') }}" class="btn btn-primary m-btn m-btn--icon">
                <span>
                    <span>
                        Thêm mới
                    </span>
                </span>
                    </a>
                    <a href="{{ route('customer.historyCus') }}" class="btn btn-info m-btn m-btn--icon">
                <span>
                    <span>
                        Lịch sử khách hàng
                    </span>
                </span>

                    </a>
                    <a href="{{ route('customer.get-import') }}" class="btn btn-danger m-btn m-btn--icon">
                <span>
                    <span>
                        Import khách hàng
                    </span>
                </span>
                    </a>
                </div>
                @include('administrator.errors.info-search')
                <div class="m-section ">
                    @if(session('list-fail'))
                        <div class="alert alert-danger alert-notification">
                            @foreach (session('list-fail') as $element)
                                {{ $element }}<br>
                            @endforeach
                        </div>
                    @endif
                    @include('administrator.errors.messages')
                    <div class="m-section__content m-portlet m-portlet--mobile">
                        <div class="m_datatable" id="child_data_local"></div>
                    </div>
                </div>
                <!--end::Section-->
            </div>
        </div>
    </div>
</div>
    @include('administrator.customers.customer.modelHistory')
    @include('administrator.customers.customer.script')
@stop