@extends('administrator.app')
@section('title','Thu lại khách hàng')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet col-xl-8 offset-xl-2">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Thu lại khách hàng
                            </h3>
                        </div>
                    </div>
                </div>
                @include('administrator.errors.messages')
                {{ Form::open(array('route' => array('customer.update-collection-customer', 'id' => $data->id), 'method' => 'POST', 'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
                <input type="hidden" name="id" value="{{ $data->id }}">
                <input type="hidden" name="created_by" value="{{ $data->created_by }}">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <div class="m-portlet__body">
                    <div class="m-form__heading">
                        <h3 class="m-form__heading-title">
                            Lý do thu lại khách hàng
                        </h3>
                    </div>
                    <div class="form-group m-form__group row">
                        <div class="col-xl-12 col-lg-12">
                            <label>Họ tên khách hàng: {{ $data->fullname }}</label>
                        </div>
                        <div class="col-xs-12 col-lg-12">
                            <label>
                                Mô tả:
                            </label>
                            <div class="input-group m-input-group m-input-group--square">
                                <textarea class="summernote" name="description" id="description"></textarea>
                            </div>
                            <span class="m-form__help">
                                Mô tả lý do thu khách hàng
                            </span>
                        </div>
                    </div>
                    <div class="col-xl-12 col-lg-12">
                        <input type="file" class="form-control-file" data-name="file_collection">
                        <input type="hidden" name="file_collection" id="file_collection">
                    </div>
                </div>
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <button class="btn btn-primary">
                            <i class="fa fa-save"></i>
                            Lưu
                        </button>
                        <a class="btn btn-secondary" href="{{ route('list.index') }}">
                            <i class="la la-arrow-circle-o-left"></i>
                            Trở về danh sách
                        </a>
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
    <input type="hidden" id="api_file" name="api_file" value="{{ env('API_FILE') }}">
    <script>
        $('document').ready(function(){
            $('.summernote').summernote({
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImageProject(image[0], $(this).attr('id'));
                    }
                },
                height: 250,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['picture']],
                    ['table', ['table']],
                    ['view', ['fullscreen']],
                    ['help', ['help']]
                ]
            });
            function uploadImageProject(image, el) {

                var data = new FormData();
                data.append("image", image);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '/administrator/upload',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(response) {
                        console.log(response);
                        if (response.status) {
                            var image = $('<img width="100%">').attr('src', response.data);
                            $('#' + el).summernote("insertNode", image[0]);
                        }

                    },
                    error: function(data) {

                    }
                });
            }

            $('input[type=file]').change(function(e) {
                $(e.target.files).each(function (index, file) {
                    var data = new FormData();
                    data.append("file", file);
                    // $.ajaxSetup({
                    //     headers: {
                    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    //     }
                    // });
                    $.ajax({
                        url: $('input[name=api_file]').val() + '/api/file',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data,
                        type: "post",
                        success: function (response) {
                            $("#file_collection").val(response.data.url);
                        },
                        error: function (data) {

                        }
                    });
                });
            });
        });
    </script>
@stop