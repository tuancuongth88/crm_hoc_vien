@extends('administrator.app')
@section('title','Lịch sử khách hàng')

@section('content')
    <div class="m-content" style="margin-bottom: 20px;">
        <div class="row">
            <div class="col-xl-4">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Chọn thông tin khách hàng
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    {{ Form::open(array('route' => array('customer.showHistoryCus') ,'method' => 'POST','class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'id' => 'create_historyCus')) }}
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row">
                            <div class="col-xl-12 col-lg-12 mt-2">
                                <label>
                                    Chọn tên khách hàng:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">
                                    {!!Form::select('customer_id',  ['' => 'Chọn tên khách hàng', ] + $listCustomer , old('customer_id'), ['class' => 'form-control listCustomer m-select2', 'id' => 'customer_id', 'required' => true])!!}
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 mt-2">
                                <input type="text" name="phone" class="form-control m-input" placeholder="Số điện thoại" value="" id="phone">
                            </div>
                            <div class="col-xl-12 col-lg-12 mt-2">
                                <input type="text" name="email" class="form-control m-input" placeholder="Email" value="" id="email">
                            </div>
                            <div class="col-xl-12 col-lg-12 mt-2">
                                <div class="input-group date">
                                    <input type="text" class="form-control m-input datetime-picker" placeholder="Thời gian cảnh báo" readonly name="time_call_back" id="time_call_back" />
                                    <div class="input-group-append">
                                        <span class="input-group-text">
                                            <i class="la la-calendar glyphicon-th"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 mt-2">
                                {{ Form::select('channel', ['' => '---Chọn kênh chăm sóc---'] + \App\Models\Customers\Histories::$listChannels, old('channel'), ['class' => 'form-control m-input', 'id' => 'channel', 'required']) }}
                            </div>
                            <div class="col-xl-12 col-lg-12 mt-2">
                                {{ Form::select('status', \App\Models\Customers\Customer::$listStatus, old('status'), ['class' => 'form-control m-input', 'id' => 'status']) }}
                            </div>
                            <div class="col-xl-12 col-lg-12 mt-2">
                                {{ Form::textarea('note', null, ['class' => 'form-control m-input', 'id' => 'note', 'placeholder' => 'Nội dung trao đổi', 'required' => true]) }}
                            </div>
                            <div>
                                <input type="hidden" name="employer_id" placeholder="Id người tư vấn" value="" id="employer_id">
                                <input type="hidden" name="employer_name" placeholder="Tên người tư vấn" value="" id="employer_name">
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit" style="padding: 10px; padding-left: 30px;">
                        <div class="m-form__actions m-form__actions">
                            <button class="btn btn-primary" id="save_history">
                                <i class="fa fa-save"></i>
                                Lưu
                            </button>
                        </div>
                    </div>
                    {{ Form::close() }}
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
            <div class="col-xl-8">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Lịch sử khách hàng
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">

                        <!--begin::Section-->
                        <div class="m-section">
                            <div class="m-section__content">
                                <table class="table m-table m-table--head-bg-brand">
                                    <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Tư vấn viên
                                        </th>
                                        <th>
                                            Số điện thoại
                                        </th>
                                        <th>
                                            Thời gian cảnh báo
                                        </th>
                                        <th>
                                            Kênh chăm sóc
                                        </th>
                                        <th>
                                            Ghi chú
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody id="showCustomerHistories">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Section-->
                    </div>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datetime-picker').datetimepicker({
                todayHighlight: true,
                autoclose: true,
                pickerPosition: 'top-left',
                todayBtn: true,
                format: 'dd-mm-yyyy h:i'
            });
            $('.m-select2').select2();
            $('#subject_id').on('change', function() {
                $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'customer_id');
            });


            //action select customer]
            $('#customer_id').on('change', function(e) {
                var id = $(this).val();
                resetForm();
                getHistory(id);
            });

            //action add history
            $('#create_historyCus').submit(function (e) {
                e.preventDefault();
                var id = $('#customer_id option:selected').val();
                var time_call_back = $('#time_call_back').val();
                var phone = $('#phone').val();
                var note = $('#note').val();
                var customer_id = id;
                var channel = $('#channel').val();
                var status = $('#status').val();
                var dataForm = { time_call_back: time_call_back, phone: phone, note: note, customer_id: customer_id, channel: channel, status : status };
                console.log(dataForm);
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ route('customer.history.save') }} ',
                    type: 'POST',
                    data: dataForm,
                    success: function (data) {
                        getHistory(id);
                    }
                });
            });

            function getHistory(id) {
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: 'history/'+id,
                    type: 'GET',
                    dataType: "json",
                    contentType: "application/json;charset=utf-8",
                    success: function (data) {
                        resetForm();
                        $('#phone').val(data.customer.phone);
                        $('#email').val(data.customer.email);
                        $('#showCustomerHistories').append(data.viewlistHistory);
                    }
                })
            }
            $('#note').keypress(function (e) {
                if (e.which == 13) {
                    $('#create_historyCus').submit();
                    return false;    //<---- Add this line
                }
            });

            function resetForm() {
                $('#showCustomerHistories').empty();
                $('#phone').val("");
                $('#email').val("");
                $('#time_call_back').val("");
                $('#chanel').val("");
                $('#note').val("");
                $('#status').val("");
            }

        });
    </script>
@stop