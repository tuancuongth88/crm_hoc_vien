{{ Form::open(array('route' => 'list.index', 'method' => 'GET', 'id' =>'search_form_customer')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tên khách hàng</label>
        <input type="text" name="fullname" id="search" class="form-control" value="{{ @$input['fullname'] }}" placeholder="Tên khách hàng" />
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Điện thoại</label>
        <input type="text" name="phone" id="search" class="form-control" value="{{ @$input['phone'] }}" placeholder="Số điện thoại" />
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Loại khách hàng</label>
        {{ Form::select('level', ['' => 'Lựa chọn'] + $listLevel, @$input['level'], ['class' => 'form-control', 'id' => 'level']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Nhân viên</label>
        {{ Form::select('user_id',  ['' => 'Lựa chọn'] + $listSale, @$input['user_id'], ['class' => 'form-control m-select2', 'id' => 'user_id']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Dự án</label>
        {{ Form::select('project_id', ['' => 'Lựa chọn'] + $listProject, @$input['project_id'], ['class' => 'form-control', 'id' => 'project_id']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Nguồn khách hàng</label>
        {{ Form::select('source_id', ['' => 'Lựa chọn'] + \App\Models\Customers\Customer::$sourceCustomer, @$input['source_id'], ['class' => 'form-control', 'id' => 'source_id']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Chiến dịch </label>
        {{ Form::select('share_id', ['' => 'Lựa chọn'] + \App\Models\Customers\CustomerSharing::getAllSharingByUser(), @$input['share_id'], ['class' => 'form-control', 'id' => 'share_id']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Loại dữ liệu khách hàng </label>
        {{ Form::select('type', ['' => 'Lựa chọn'] + \App\Models\Customers\TypeCustomer::getAllTypeCustomerByUser(), @$input['type'], ['class' => 'form-control', 'id' => 'share_id']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
            <label>Tìm kiếm</label>
            <input type="submit" value="Tìm kiếm" class="btn btn-primary form-control" />
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
            <label>Hủy tìm</label>
            <a href="{{ route('list.index') }}" class="btn btn-danger form-control">Hủy tìm</a>
    </div>
</div>
</form>
