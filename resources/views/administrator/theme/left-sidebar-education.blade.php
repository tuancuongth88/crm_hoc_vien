<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
     data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
            <a href="{{ route('report.statistic') }}" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-line-graph"></i>
                <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Dashboard
                            </span>
                        </span>
                </span>
            </a>
        </li>
        <li class="m-menu__section">
            <h4 class="m-menu__section-text">
                Đào tạo
            </h4>
            <i class="m-menu__section-icon flaticon-more-v3"></i>
        </li>
        <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item  m-menu__item--submenu
        @if(Request::segment(2) == 'education') m-menu__item--open m-menu__item--expanded @endif ">
            <a href="#" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-open-box"></i>
                <span class="m-menu__link-text">Đào tạo</span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    @if (\Auth::user()->can('program.view'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('program.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                                    Lĩnh vực
                                                </span>
                            </a>
                        </li>
                    @endif
                    @if (\Auth::user()->can('subject.view'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('subject.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                                    Chương trình
                                                </span>
                            </a>
                        </li>
                    @endif
                    @if (\Auth::user()->can('lesson.view'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('lesson.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                                    Khóa học
                                                </span>
                            </a>
                        </li>
                    @endif
                    @if (\Auth::user()->can('question.view'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('question.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                                    Ngân hàng câu hỏi
                                                </span>
                            </a>
                        </li>
                    @endif
                    @if (\Auth::user()->can('exam.view'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('exam.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                                    Kỳ thi
                                                </span>
                            </a>
                        </li>
                    @endif

                    @if (\Auth::user()->can('program.view'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('user.student') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                                    Danh sách học viên
                                                </span>
                            </a>
                        </li>
                    @endif

                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('user.teacher') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                    Danh sách giảng viên
                                </span>
                        </a>
                    </li>
                    @if(\Auth::user()->can('is-teacher'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('user.teacher.edit') }}" class="m-menu__link">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Cập nhật thông tin giảng viên
                                </span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </li>
        <li class="m-menu__section">
            <h4 class="m-menu__section-text">
                Báo cáo công việc
            </h4>
            <i class="m-menu__section-icon flaticon-more-v3"></i>
        </li>
        <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item  m-menu__item--submenu
        @if(Request::segment(2) == 'info') m-menu__item--open m-menu__item--expanded @endif ">
            <a href="#" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-time-3"></i>
                <span class="m-menu__link-text">
                    Dự án
                </span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    @if (\Auth::user()->can('is-admin'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('type.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Loại dự án
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('segment.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                Phân khúc dự án
                            </span>
                            </a>
                        </li>
                    @endif
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('project.create') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Thêm dự án
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('project.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Danh sách
                            </span>
                        </a>
                    </li>
                    @if (\Auth::user()->can('project-manager.view'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('project-manager.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                Quản lý dự án
                            </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('project-manager.create') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                            Thêm mới dự án quản lý
                        </span>
                            </a>
                        </li>
                    @endif
                </ul>
            </div>
        </li>
        <li class="m-menu__item  m-menu__item--submenu @if(Request::segment(2) == 'customer') m-menu__item--open m-menu__item--expanded @endif " aria-haspopup="true" data-menu-submenu-toggle="hover">
            <a href="#" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-user-ok"></i>
                <span class="m-menu__link-text">
                        Khách hàng
                    </span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    @if(\Auth::user()->can('is-manager') || \Auth::user()->can('is-admin-customer'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('sharing-user.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                Báo cáo chia khách hàng
                             </span>
                            </a>
                        </li>
                    @endif
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('list.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Danh sách
                             </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('list.create') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Thêm mới
                             </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('customer.historyCus') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Lịch sử chăm sóc
                             </span>
                        </a>
                    </li>
                    @if(\Auth::user()->can('is-admin-customer'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('customer_stores.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                Kho khách hàng
                             </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('customer_type.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                Loại dữ liệu
                             </span>
                            </a>
                        </li>
                    @endif
                    @if(\Auth::user()->can('is-manager'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('customer.get-share-customer-by-leader') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                Giao khách hàng cho sale
                             </span>
                            </a>
                        </li>

                    @endif
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('customer.list-collection-customer') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                            Danh sách khách hàng bị thu hồi
                         </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item  m-menu__item--submenu
        @if(Request::segment(2) == 'job') m-menu__item--open m-menu__item--expanded @endif ">
            <a href="#" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-list"></i>
                <span class="m-menu__link-text">
                    Báo cáo
                </span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    @if (\Auth::user()->can('is-admin'))
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('task-category.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Danh mục nhóm công việc
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('target.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Mục tiêu
                                </span>
                            </a>
                        </li>
                    @endif

                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('report.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                Báo cáo
                            </span>
                            </a>
                        </li>

                    @if (\Auth::user()->can('is-hanh-chinh') || \Auth::user()->type == App\Models\Users\User::TYPE_TRIAL_PERIOD)
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('report-trial.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                            Chuyên viên thử việc
                        </span>
                        </a>
                    </li>
                    @endif
                </ul>
            </div>
        </li>
        @if (\Auth::user()->can('is-admin'))
            <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item
            @if(Request::segment(3) == 'user') m-menu__item--open m-menu__item--expanded @endif ">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-text">
                        User
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('user.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Danh sách
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item @if(in_array(Request::segment(3), ['department', 'branch', 'company', 'position', 'role'])) m-menu__item--open m-menu__item--expanded @endif m-menu__item--submenu">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-settings-1"></i>
                    <span class="m-menu__link-text">
                        System
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('department.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Phòng ban
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('branch.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Chi nhánh
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('company.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    công ty
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('position.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Chức vụ
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item " aria-haspopup="true">
                            <a href="{{ route('role.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Quản lý quyền
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        @endif
        @if(\Auth::user()->can('is-admin-customer'))
        <li class="m-menu__section">
            <h4 class="m-menu__section-text">
                Ứng dụng CS khách hàng
            </h4>
            <i class="m-menu__section-icon flaticon-more-v3"></i>
        </li>
        <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item
            @if(Request::segment(3) == 'account' || Request::segment(3) == 'chat') m-menu__item--open m-menu__item--expanded @endif ">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-users"></i>
                    <span class="m-menu__link-text">
                        Tài khoản
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item @if(Request::segment(3) == 'account') m-menu__item--active @endif" aria-haspopup="true">
                            <a href="{{ route('account.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Danh sách tài khoản
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item @if(Request::segment(3) == 'chat') m-menu__item--active @endif" aria-haspopup="true">
                            <a href="{{ route('account.chat') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Livechat với khách hàng
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item @if(Request::segment(3) == 'chart') m-menu__item--active @endif" aria-haspopup="true">
                            <a href="{{ route('account.transaction.chart') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Thống kê giao dịch
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item
            @if(Request::segment(2) == 'news') m-menu__item--open m-menu__item--expanded @endif ">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon fa 	fa fa-newspaper-o"></i>
                    <span class="m-menu__link-text">
                        Quản lý tin tức
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item @if(Request::segment(3) == 'promotion') m-menu__item--active @endif" aria-haspopup="true">
                            <a href="{{ route('news.promotion') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Chính sách ưu đãi
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item @if(Request::segment(3) == 'news') m-menu__item--active @endif" aria-haspopup="true">
                            <a href="{{ route('news.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Tin dự án
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="m-menu__item  @if(Request::segment(3) == 'feedback') m-menu__item--active @endif" aria-haspopup="true">
                <a href="{{ route('feedback.index') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon la 	la-bullhorn"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quản lý phản hồi
                            </span>
                        </span>
                </span>
                </a>
            </li>
            <li class="m-menu__item  @if(Request::segment(2) == 'consignments') m-menu__item--active @endif" aria-haspopup="true">
                <a href="{{ route('consignments.index') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon la 	la-cart-plus"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Quản lý ký gửi nhà đất
                            </span>
                        </span>
                </span>
                </a>
            </li>
            <li class="m-menu__item  @if(Request::segment(3) == 'list-booking') m-menu__item--active @endif" aria-haspopup="true">
                <a href="{{ route('list-booking.list') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon la 	la-calendar-check-o"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Đặt lịch xem nhà mẫu
                            </span>
                        </span>
                </span>
                </a>
            </li>

            <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item
            @if(Request::segment(4) == 'sms') m-menu__item--open m-menu__item--expanded @endif ">
                <a href="#" class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon fa fa fa-wechat"></i>
                    <span class="m-menu__link-text">
                        Quản lý SMS
                    </span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item @if(Request::segment(4) == 'sms') m-menu__item--active @endif" aria-haspopup="true">
                            <a href="{{ route('sms.index') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Template tin nhắn
                                </span>
                            </a>
                        </li>
                        <li class="m-menu__item @if(Request::segment(5) == 'promotion') m-menu__item--active @endif" aria-haspopup="true">
                            <a href="{{ route('sms.promotion') }}" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                    <span></span>
                                </i>
                                <span class="m-menu__link-text">
                                    Gửi tin nhắn
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>

            <li class="m-menu__item  @if(Request::segment(3) == 'list-customer-promotion') m-menu__item--active @endif" aria-haspopup="true">
                <a href="{{ route('app.customer.tmp') }}" class="m-menu__link ">
                    <i class="m-menu__link-icon la 	la-calendar-check-o"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Khách hàng khuyến mại
                            </span>
                        </span>
                </span>
                </a>
            </li>
        @endif
    </ul>
</div>
