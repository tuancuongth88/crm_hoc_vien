<!--begin::Base Styles -->
<!--begin::Page Vendors -->
{{-- <link href="{{ URL::asset('assets/vendors/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" /> --}}
<link href="{{ URL::asset('plugins/tagsinput/tagsinput.css') }}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors -->
<link href="{{ URL::asset('assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('css/custom.css') }}" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />


<!--end::Base Styles -->