<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " data-menu-vertical="true" data-menu-scrollable="false" data-menu-dropdown-timeout="500">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
            <a href="{{ route('report.statistic') }}" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-line-graph"></i>
                        <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">
                                Dashboard
                            </span>
                        </span>
                        </span>
                    </a>
        </li>
        <li class="m-menu__section">
            <h4 class="m-menu__section-text">
                Module
            </h4>
            <i class="m-menu__section-icon flaticon-more-v3"></i>
        </li>
        <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item  m-menu__item--submenu
        @if(Request::segment(2) == 'info') m-menu__item--open m-menu__item--expanded @endif ">
            <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-time-3"></i>
                        <span class="m-menu__link-text">
                                        Dự án
                                    </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    @if (\Auth::user()->can('is-manager'))
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('type.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Loại dự án
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('segment.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Phân khúc dự án
                            </span>
                        </a>
                    </li>
                    @endif
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('project.create') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                            <span></span>
                                        </i>
                            <span class="m-menu__link-text">
                                Thêm dự án
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('project.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Danh sách
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('project-manager.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                            Quản lý dự án
                        </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('project-manager.create') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Thêm mới dự án quản lý
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" data-menu-submenu-toggle="hover">
            <a href="#" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-icon flaticon-user-ok"></i>
                    <span class="m-menu__link-text">
                        Khách hàng
                    </span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('list.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Danh sách
                             </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('list.create') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Thêm mới
                             </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
        <li aria-haspopup="true" data-menu-submenu-toggle="hover" class="m-menu__item  m-menu__item--submenu
        @if(Request::segment(2) == 'job') m-menu__item--open m-menu__item--expanded @endif ">
            <a href="#" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-list"></i>
                        <span class="m-menu__link-text">
                            Báo cáo
                        </span>
                        <i class="m-menu__ver-arrow la la-angle-right"></i>
                    </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    @if (\Auth::user()->can('is-manager'))
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('task-category.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Danh mục nhóm công việc
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('target.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Mục tiêu
                            </span>
                        </a>
                    </li>
                    @endif
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('report.index') }}" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">
                                Báo cáo
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</div>