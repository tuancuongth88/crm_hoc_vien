<div class="tab-content">
    <!--begin::Widget 14-->
    <div class="m-widget4">
        @foreach ($data as $user)
            <!--begin::Widget 14 Item-->
            <div class="m-widget4__item">
                <div class="m-widget4__img m-widget4__img--pic">
                    <img src="{{ $user->avatar }}" alt="">
                </div>
                <div class="m-widget4__info">
                    <span class="m-widget4__title">
                        {{ $user->fullname }}
                    </span>
                    <br>
                    <span class="m-widget4__sub">
                        {{ $user->phone }}
                    </span>
                </div>
                <div class="m-widget4__ext">
                    <a href="#"  class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary">
                        Thống kê
                    </a>
                </div>
            </div>
            <!--end::Widget 14 Item-->
        @endforeach
    </div>
    <!--end::Widget 14-->
    <label for="" class="m--margin-top-30">
        {{ $data->links() }}
        {{-- <nav aria-label="Page navigation example">
          <ul class="pagination">
            <li class="page-item"><a class="page-link" href="{{ $data->previousPageUrl() }}">Trước</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">Next</a></li>
          </ul>
        </nav> --}}
        {{-- {{ $paginator->links('administrator.append.paginate', ['data' => $data]) }} --}}
    </label>
</div>