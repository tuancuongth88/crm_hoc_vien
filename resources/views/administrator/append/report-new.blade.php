<style type="text/css">
    .m-widget5 .m-widget5__item{
        border-bottom: none !important;
    }
</style>
<div class="row">
    <div class="col-xl-4">
        <!--begin:: Widgets/Authors Profit-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            CÔNG VIỆC THƯỜNG XUYÊN
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body" style="margin-top: 20px;">
                <!-- start khach hang -->
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Tổng số cuộc gọi
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu:
                                <b>{{ isset($plan->data['call_customer_potential']) ? ($plan->data['call_customer_potential']['number']).'/'.$plan->data['call_customer_potential']['time'] : '' }}</b>
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ isset($history[\App\Models\Customers\Histories::CAll_PHONE]) ? $history[\App\Models\Customers\Histories::CAll_PHONE] : 0 }}
                            </span>
                        </span>
                    </div>
                </div>
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Tổng số Tin nhắn
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu ?
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ isset($history[\App\Models\Customers\Histories::SEND_SMS]) ? $history[\App\Models\Customers\Histories::SEND_SMS] : 0 }}
                            </span>
                        </span>
                    </div>
                </div>
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Tổng số Email đã gửi
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['send_email']) ? ($plan->data['send_email']['number']).'/'.$plan->data['send_email']['time'] : '' }}</b>
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ isset($history[\App\Models\Customers\Histories::SEND_EMAIL]) ? $history[\App\Models\Customers\Histories::SEND_EMAIL] : 0 }}
                            </span>
                        </span>
                    </div>
                </div>
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Tổng số tương tác Social
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['total_social']) ? ($plan->data['total_social']['number']).'/'.$plan->data['total_social']['time'] : '' }}</b>
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ isset($history[\App\Models\Customers\Histories::SOCIAL_NETWORK]) ? $history[\App\Models\Customers\Histories::SOCIAL_NETWORK] : 0 }}
                            </span>
                        </span>
                    </div>
                </div>
                <!-- End Khach hang -->
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Đăng tin website
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['post_website']) ? ($plan->data['post_website']['number']).'/'.$plan->data['post_website']['time'] : '' }}</b>
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ $report->pluck('post_website')->sum() }}
                            </span>
                        </span>
                    </div>
                </div>
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Đăng tin facebook, fanpage
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['post_facebook']) ? ($plan->data['post_facebook']['number']).'/'.$plan->data['post_facebook']['time'] : '' }}</b>
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ $report->pluck('post_facebook')->sum() }}
                            </span>
                        </span>
                    </div>
                </div>
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Đăng tin bất động sản
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['post_news']) ? ($plan->data['post_news']['number']).'/'.$plan->data['post_news']['time'] : '' }}</b>
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ $report->pluck('post_news')->sum() }}
                            </span>
                        </span>
                    </div>
                </div>
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Đăng tin forum
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['post_forum']) ? ($plan->data['post_forum']['number']).'/'.$plan->data['post_forum']['time'] : '' }}</b>
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ $report->pluck('post_forum')->sum() }}
                            </span>
                        </span>
                    </div>
                </div>
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Cập nhật thông tin thị trường
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['update_info_market']) ? ($plan->data['update_info_market']['number']).'/'.$plan->data['update_info_market']['time'] : '' }}</b>
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ $report->pluck('update_info_market')->sum() }}
                            </span>
                        </span>
                    </div>
                </div>
                <div class="m-widget4">
                    <div class="m-widget4__item">
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                Cập nhật thông tin dự án
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['update_info_project']) ? ($plan->data['update_info_project']['number']).'/'.$plan->data['update_info_project']['time'] : '' }}</b>
                            </span>
                        </div>
                        <span class="m-widget4__ext">
                            <span class="m-widget4__number m--font-brand">
                                {{ $report->pluck('update_info_project')->sum() }}
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Authors Profit-->
    </div>
    <div class="col-xl-4">
        <!--begin:: Widgets/Authors Profit-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            CÔNG VIỆC BẤT THƯỜNG
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body" style="margin-top: 20px;">
                <!-- start khach hang -->
                <div class="m-widget5">
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">

                            <span class="m-widget5__title">
                                 Phát tờ rơi cá nhân
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['flypaper_personal']) ? ($plan->data['flypaper_personal']['number']).'/'.$plan->data['flypaper_personal']['time'] : '' }}</b>
                            </span>
                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('flypaper_personal')->sum() }}
                                    </span>
                            <br>

                        </div>
                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Phát tờ rơi & treo phướn tập thể
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['flypaper_group']) ? ($plan->data['flypaper_group']['number']).'/'.$plan->data['flypaper_group']['time'] : '' }}</b>
                            </span>

                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('flypaper_group')->sum() }}
                                    </span>
                            <br>

                        </div>
                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Thực hiện Questionair tìm kiếm khách hàng tiềm năng
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['questionair']) ? ($plan->data['questionair']['number']).'/'.$plan->data['questionair']['time'] : '' }}</b>
                            </span>
                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('questionair')->sum() }}
                                    </span>
                            <br>

                        </div>
                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Trực dự án và nhà mẫu trong giờ hành chính
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['guard_project_in_hour']) ? ($plan->data['guard_project_in_hour']['number']).'/'.$plan->data['guard_project_in_hour']['time'] : '' }}</b>
                            </span>

                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('guard_project_in_hour')->sum() }}
                                    </span>
                            <br>

                        </div>
                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Trực dự án và nhà mẫu ngoài giờ hành chính
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['guard_project_out_hour']) ? ($plan->data['guard_project_out_hour']['number']).'/'.$plan->data['guard_project_out_hour']['time'] : '' }}</b>
                            </span>
                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('guard_project_out_hour')->sum() }}
                                    </span>
                            <br>

                        </div>
                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Tham gia sự kiện mở bán và giới thiệu dự án phân phối
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['open_sale']) ? ($plan->data['open_sale']['number']).'/'.$plan->data['open_sale']['time'] : '' }}</b>
                            </span>

                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('open_sale')->sum() }}
                                    </span>
                            <br>

                        </div>
                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Tham gia các hoạt động Roadshow, quảng cáo
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['roadshow']) ? ($plan->data['roadshow']['number']).'/'.$plan->data['roadshow']['time'] : '' }}</b>
                            </span>
                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('roadshow')->sum() }}
                                    </span>
                            <br>

                        </div>
                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Tham gia sự kiện, event, hội thảo bên ngoài
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['event']) ? ($plan->data['event']['number']).'/'.$plan->data['event']['time'] : '' }}</b>
                            </span>

                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('event')->sum() }}
                                    </span>
                            <br>

                        </div>
                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Tìm kiếm khách hàng tại các địa điểm công cộng
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['find_public']) ? ($plan->data['find_public']['number']).'/'.$plan->data['find_public']['time'] : '' }}</b>
                            </span>

                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('find_public')->sum() }}
                                    </span>
                            <br>

                        </div>

                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Thiết lập cuộc hẹn tư vấn khách hàng
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['setup_appointment']) ? ($plan->data['setup_appointment']['number']).'/'.$plan->data['setup_appointment']['time'] : '' }}</b>
                            </span>

                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('setup_appointment')->sum() }}
                                    </span>
                            <br>

                        </div>

                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Tổng kết, đánh giá công việc
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['total']) ? ($plan->data['total']['number']).'/'.$plan->data['total']['time'] : '' }}</b>
                            </span>

                        </div>
                        <div class="m-widget5__stats1">
                                    <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                        {{ $report->pluck('total')->sum() }}
                                    </span>
                            <br>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Authors Profit-->
    </div>
    <div class="col-xl-4">
        <!--begin:: Widgets/Authors Profit-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            CÔNG VIỆC TRUYỀN THÔNG CÔNG TY
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body" style="margin-top: 20px;">
                <!-- start khach hang -->
                <div class="m-widget5">
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Like bài viết
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                 <b>{{ isset($plan->data['like']) ? ($plan->data['like']['number']).'/'.$plan->data['like']['time'] : '' }}</b>
                            </span>

                        </div>
                        <div class="m-widget5__stats1">
                                        <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                            {{ $report->pluck('like')->sum() }}
                                        </span>
                            <br>

                        </div>

                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Comment bài viết
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu
                                <b>{{ isset($plan->data['comment']) ? ($plan->data['comment']['number']).'/'.$plan->data['comment']['time'] : '' }}</b>
                            </span>

                        </div>
                        <div class="m-widget5__stats1">
                                <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                    {{ $report->pluck('comment')->sum() }}
                                </span>
                            <br>
                        </div>
                    </div>
                    <div class="m-widget5__item" style="border-bottom: none !important;">
                        <div class="m-widget5__content">
                            <span class="m-widget5__title">
                                Share bài viết
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                Yêu cầu <b>{{ isset($plan->data['share']) ? ($plan->data['share']['number']).'/'.$plan->data['share']['time'] : '' }}</b>
                            </span>
                        </div>
                        <div class="m-widget5__stats1">
                                <span class="m-widget4__number m--font-brand"  style="font-weight: bold;font-size: 20px;">
                                    {{ $report->pluck('share')->sum() }}
                                </span>
                            <br>

                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Authors Profit-->
    </div>
</div>



<input type="hidden" name="duration_type" id="duration_type" value="{{ $durationType }}">
<script type="text/javascript">
    $(document).ready(function() {
        var str = '';
        if (parseInt($('#user_id').val()) > 0) {
            str = '?user_id=' + $('#user_id').val();
        }
        var durationType = $('#duration_type').val();
        $('#m_datatable_latest_orders').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/administrator/job/report/task-suddenly/' + durationType + str,
                        method: 'GET',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        map: function(raw) {
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                          }
                          return dataSet;
                        },
                    },
                },


                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // column sorting
            sortable: false,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },


            rows: {
                // auto hide columns, if rows overflow
                autoHide: true,
            },

            // columns definition
            columns: [{
                field: 'created_at',
                title: 'Ngày',
                template: function(row) {
                    if (row.report) {
                        return row.report.created_at;
                    }
                    return '';
                },
            }, {
                field: 'task',
                title: 'Công việc',
                width: 150,
            }, {
                field: 'Latitude',
                title: 'Đánh giá',
                // callback function support for column rendering
                template: function(row) {
                    var status = {
                        'unrate': {
                            'title': 'Chưa đánh giá',
                            'class': 'm-badge--warning'
                        },
                        'good': {
                            'title': 'Tốt',
                            'class': 'm-badge--success'
                        },
                        'medium': {
                            'title': 'Trung bình',
                            'class': ' m-badge--primary'
                        },
                        'weak': {
                            'title': 'Yếu',
                            'class': ' m-badge--danger'
                        },
                    };
                    return '<span class="m-badge ' + status[row.rate].class + ' m-badge--wide">' + status[row.rate].title + '</span>';
                },
            }, {
                field: 'location',
                title: 'Địa điểm',
                width: 350,
            }, {
                field: 'duration',
                title: 'Khoảng thời gian',
            }, {
                field: 'Tài liệu đính kèm',
                title: 'Tài liệu đính kèm',
                width: 350,
                // callback function support for column rendering
                template: function(row) {
                    var listFile = '';
                    row.has_files.forEach(function(file) {
                        listFile += '<i class="fa fa-paperclip"></i><a href="' + file.file + '" download="' + file.file + '">' + file.file + '</a><br>';
                        // return 'file.file';
                    });
                    return listFile;
                },
            }],
        });

        $('#summernote').summernote({
            callbacks: {
                onImageUpload: function(image) {
                    uploadImage(image[0]);
                }
            },
            height: 150,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
              ]
        });

        $('#m_portlet_tools_5').mPortlet();
        $('#m_portlet_tools_5').on('afterFullscreenOn', function(portlet) {
            toastr.info('After fullscreen on event fired!');
            mApp.initScroller(scrollable, {});
        });

        $('#m_portlet_tools_5').on('afterFullscreenOff', function(portlet) {
            toastr.warning('After fullscreen off event fired!');
        });
        $('#m_portlet_tools_5').on('beforeRemove', function(portlet) {
            toastr.info('Before remove event fired!');

            return confirm('Are you sure to remove this portlet ?');  // remove portlet after user confirmation
        });

        $('#m_portlet_tools_5').on('afterRemove', function(portlet) {
            setTimeout(function() {
                toastr.warning('After remove event fired!');
            }, 2000);
        });
        $('#m_portlet_tools_5').on('beforeCollapse', function(portlet) {
            setTimeout(function() {
                toastr.info('Before collapse event fired!');
            }, 100);
        });

        $('#m_portlet_tools_5').on('afterCollapse', function(portlet) {
            setTimeout(function() {
                toastr.warning('Before collapse event fired!');
            }, 2000);
        });

        $('#m_portlet_tools_5').on('beforeExpand', function(portlet) {
            setTimeout(function() {
                toastr.info('Before expand event fired!');
            }, 100);
        });

        $('#m_portlet_tools_5').on('afterExpand', function(portlet) {
            setTimeout(function() {
                toastr.warning('After expand event fired!');
            }, 2000);
        });
    });

</script>