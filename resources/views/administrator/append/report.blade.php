<div class="row">
    <div class="col-xl-4">
        <!--begin:: Widgets/Authors Profit-->
        <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            CÔNG VIỆC THƯỜNG XUYÊN
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="m-widget4">
                    @foreach ($taskFixed as $key => $task)
                        <div class="m-widget4__item">
                            <div class="m-widget4__img m-widget4__img--logo">
                                <i class="fa {{ $task['icon'] }}" style="font-size: 3rem;"></i>
                            </div>
                            <div class="m-widget4__info">
                                <span class="m-widget4__title">
                                    {{ $task['name'] }}
                                </span>
                                <br>
                                <span class="m-widget4__sub">
                                    Yêu cầu {{ isset($targetMonth[$key]) ? $targetMonth[$key] : '0' }}
                                </span>
                            </div>
                            <span class="m-widget4__ext">
                                <span class="m-widget4__number m--font-brand">
                                    {{ isset($taskStability[$key]) ? $taskStability[$key] : '0' }}
                                </span>
                            </span>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <!--end:: Widgets/Authors Profit-->
    </div>
    <div class="col-xl-8">
        <div class="m-portlet m-portlet--mobile ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            CÔNG VIỆC BẤT THƯỜNG
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <!--begin: Datatable -->
                <div class="m_datatable" id="m_datatable_latest_orders"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
<div class="row">
    @foreach ($listTaskCategory as $taskCategory)
    <div class="col-xl-4">
    <!--begin:: Widgets/Tasks -->
        <div class="m-portlet m-portlet--full-height ">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            {{ $taskCategory['category']['name'] }}
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane active" id="m_widget2_tab1_content">
                        <div class="m-widget2">
                            @foreach ($taskCategory['task'] as $task)
                                <div class="m-widget2__item m-widget2__item--primary">
                                    <div class="m-widget2__checkbox">
                                        {{-- <label class="m-checkbox m-checkbox--solid m-checkbox--single m-checkbox--brand">
                                            <input type="checkbox">
                                            <span></span>
                                        </label> --}}
                                    </div>
                                    <div class="m-widget2__desc">
                                        <span class="m-widget2__text">
                                            {{ $task['task'] }}
                                        </span>
                                        <br>
                                        <span class="m-widget2__user-name">
                                            <a href="#" class="m-widget2__link">
                                                {{ $task['created_at'] }}
                                            </a>
                                        </span>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <div class="tab-pane" id="m_widget2_tab2_content"></div>
                    <div class="tab-pane" id="m_widget2_tab3_content"></div>
                </div>
            </div>
        </div>
        <!--end:: Widgets/Tasks -->
    </div>
    @endforeach
</div>
{{-- @if (Gate::allows('report.approve') && Request::input('user_id'))
    <div class="row">
        <div class="col-md-12">
            <!--begin::Portlet-->
            <div class="m-portlet m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_5">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <span class="m-portlet__head-icon">
                                <i class="flaticon-placeholder-2"></i>
                            </span>
                            <h3 class="m-portlet__head-text">
                                Đánh giá
                            </h3>
                        </div>
                    </div>
                    <div class="m-portlet__head-tools">
                        <ul class="m-portlet__nav">
                            <li class="m-portlet__nav-item">
                                <a href="#" data-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                    <i class="la la-power-off"></i>
                                </a>
                            </li>
                            <li class="m-portlet__nav-item">
                                <a href="#"  data-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                    <i class="la la-expand"></i>
                                </a>
                            </li>
                            <li class="m-portlet__nav-item">
                                <a href=""  data-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon">
                                    <i class="la la-angle-down"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!--begin::Form-->
                <form class="m-form m-form--fit m-form--label-align-right">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label for="exampleTextarea">
                                Nhận xét của bạn:
                            </label>
                            <textarea class="summernote" id="summernote" name="evaluate"></textarea>
                        </div>
                        <div class="m-form__actions">
                            <button type="submit" class="btn btn-outline-primary m-btn m-btn--icon">
                                <span>
                                    <i class="fa fa-send-o"></i>
                                    <span>
                                        Gửi đánh giá
                                    </span>
                                </span>
                            </button>

                        </div>
                    </div>
                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>

    </div>
@endif --}}

<input type="hidden" name="duration_type" id="duration_type" value="{{ $durationType }}">
<script type="text/javascript">
    $(document).ready(function() {
        var str = '';
        if (parseInt($('#user_id').val()) > 0) {
            str = '?user_id=' + $('#user_id').val();
        }
        var durationType = $('#duration_type').val();
        $('#m_datatable_latest_orders').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/administrator/job/report/task-suddenly/' + durationType + str,
                        method: 'GET',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        map: function(raw) {
                          // sample data mapping
                          var dataSet = raw;
                          if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                          }
                          return dataSet;
                        },
                    },
                },


                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // column sorting
            sortable: false,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },


            rows: {
                // auto hide columns, if rows overflow
                autoHide: true,
            },

            // columns definition
            columns: [{
                field: 'created_at',
                title: 'Ngày',
                template: function(row) {
                    if (row.report) {
                        return row.report.created_at;
                    }
                    return '';
                },
            }, {
                field: 'task',
                title: 'Công việc',
                width: 150,
            }, {
                field: 'Latitude',
                title: 'Đánh giá',
                // callback function support for column rendering
                template: function(row) {
                    var status = {
                        'unrate': {
                            'title': 'Chưa đánh giá',
                            'class': 'm-badge--warning'
                        },
                        'good': {
                            'title': 'Tốt',
                            'class': 'm-badge--success'
                        },
                        'medium': {
                            'title': 'Trung bình',
                            'class': ' m-badge--primary'
                        },
                        'weak': {
                            'title': 'Yếu',
                            'class': ' m-badge--danger'
                        },
                    };
                    return '<span class="m-badge ' + status[row.rate].class + ' m-badge--wide">' + status[row.rate].title + '</span>';
                },
            }, {
                field: 'location',
                title: 'Địa điểm',
                width: 350,
            }, {
                field: 'duration',
                title: 'Khoảng thời gian',
            }, {
                field: 'Tài liệu đính kèm',
                title: 'Tài liệu đính kèm',
                width: 350,
                // callback function support for column rendering
                template: function(row) {
                    var listFile = '';
                    row.has_files.forEach(function(file) {
                        listFile += '<i class="fa fa-paperclip"></i><a href="' + file.file + '" download="' + file.file + '">' + file.file + '</a><br>';
                        // return 'file.file';
                    });
                    return listFile;
                },
            }],
        });

        $('#summernote').summernote({
            callbacks: {
                onImageUpload: function(image) {
                    uploadImage(image[0]);
                }
            },
            height: 150,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
              ]
        });

        $('#m_portlet_tools_5').mPortlet();
        $('#m_portlet_tools_5').on('afterFullscreenOn', function(portlet) {
            toastr.info('After fullscreen on event fired!');
            mApp.initScroller(scrollable, {});
        });

        $('#m_portlet_tools_5').on('afterFullscreenOff', function(portlet) {
            toastr.warning('After fullscreen off event fired!');
        });
        $('#m_portlet_tools_5').on('beforeRemove', function(portlet) {
            toastr.info('Before remove event fired!');

            return confirm('Are you sure to remove this portlet ?');  // remove portlet after user confirmation
        });

        $('#m_portlet_tools_5').on('afterRemove', function(portlet) {
            setTimeout(function() {
                toastr.warning('After remove event fired!');
            }, 2000);
        });
        $('#m_portlet_tools_5').on('beforeCollapse', function(portlet) {
            setTimeout(function() {
                toastr.info('Before collapse event fired!');
            }, 100);
        });

        $('#m_portlet_tools_5').on('afterCollapse', function(portlet) {
            setTimeout(function() {
                toastr.warning('Before collapse event fired!');
            }, 2000);
        });

        $('#m_portlet_tools_5').on('beforeExpand', function(portlet) {
            setTimeout(function() {
                toastr.info('Before expand event fired!');
            }, 100);
        });

        $('#m_portlet_tools_5').on('afterExpand', function(portlet) {
            setTimeout(function() {
                toastr.warning('After expand event fired!');
            }, 2000);
        });
    });

</script>