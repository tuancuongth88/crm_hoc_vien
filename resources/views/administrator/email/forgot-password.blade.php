<!DOCTYPE html>
<html>
<head>
	<title>Quên mật khẩu</title>
</head>
<body>
	<p>
		Xin chào !
		Có vẻ như bạn đã gửi yêu cầu khôi phục mật khẩu tại {{ url("/") }}<br>
		Đây là đường dẫn để khôi phục mật khẩu của bạn: <a href="{{ $data }}">{{ $data }}</a>.<br>
		Đường dẫn này sẽ hết hạn sau 1 ngày nữa.<br>
		Cảm ơn.
		</br>

	</p>
</body>
</html>