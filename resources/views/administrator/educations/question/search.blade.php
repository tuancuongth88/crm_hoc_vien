{{ Form::open(array('route' => 'question.index', 'method' => 'GET')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tên Câu hỏi</label>
        <input type="text" name="search" id="search" class="form-control" placeholder="Tên Câu hỏi" />
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Chương trình</label>
        <select name="subject_id" class="form-control">
{{--        @foreach($data as $key => $value)--}}
            {{--<option value="{{$value->id}}">{{$value->subject->name}}</option>--}}
        {{--@endforeach--}}
        </select>
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Tìm kiếm" class="btn btn-primary form-control" />
    </div>

    <input type="hidden" name="searchJoin" value="and">
</div>
</form>