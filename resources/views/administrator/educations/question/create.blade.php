@extends('administrator.app')
@section('title','câu hỏi')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <a href="{{ route('question.index') }}" class="btn btn-primary m-btn m-btn--icon">
                    <span>
                        <i class="fa flaticon-list-3"></i>
                        <span>
                            Danh sách câu hỏi
                        </span>
                    </span>
                </a>
            </div>
            <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Thêm mới câu hỏi
                                </h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => 'question.store', 'class' => 'm-form')) }}
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Câu hỏi:
                                    </label>
                                    <div class="col-lg-4">
                                        <div class="form-group m-form__group">
                                            <textarea class="form-control m-input" name="name" rows="3" cols="8"></textarea>
                                        </div>
                                        <span class="m-form__help">
                                            Nội dung câu hỏi
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Chương trình:
                                    </label>
                                    <div class="col-lg-4">
                                        <div class="form-group m-form__group">
                                            {!!Form::select('subject_id', [0 => 'Chọn Chương trình'] + $listSubject, null, ['class' => 'form-control'])!!}
                                        </div>
                                        <span class="m-form__help">
                                            Lĩnh vực câu hỏi
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Khóa học:
                                    </label>
                                    <div class="col-lg-4">
                                        <div class="form-group m-form__group">
                                            {!!Form::select('lesson_id', [0 => 'Chọn Khóa học'] + $listLesson, null, ['class' => 'form-control'])!!}
                                        </div>
                                        <span class="m-form__help">
                                            Lĩnh vực câu hỏi
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-lg-2 col-form-label">
                                        Mức độ câu hỏi:
                                    </label>
                                    <div class="col-lg-4">
                                        <div class="m-radio-list">
                                            <label class="m-radio m-radio--check-bold m-radio--state-success">
                                                {{ Form::radio('level', \App\Models\Education\Question::EASY, false) }}
                                                Dễ
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--check-bold m-radio--state-brand">
                                                {{ Form::radio('level', \App\Models\Education\Question::MEDIUM, false) }}
                                                Trung bình
                                                <span></span>
                                            </label>
                                            <label class="m-radio m-radio--check-bold m-radio--state-danger">
                                                {{ Form::radio('level', \App\Models\Education\Question::HARD, false) }}
                                                Khó
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-form__seperator m-form__seperator--dashed  m-form__seperator--space m--margin-bottom-40"></div>
                                <div id="m_repeater_1">
                                    <div class="form-group  m-form__group row" id="m_repeater_1">
                                        <label  class="col-lg-2 col-form-label">
                                            Danh sách câu trả lời:
                                        </label>
                                        <div data-repeater-list="list_answer" class="col-lg-10">
                                            <div data-repeater-item class="form-group m-form__group row align-items-center">
                                                <div class="col-md-6">
                                                    <div class="m-form__group--inline">

                                                        <div class="m-form__control">
                                                            <input type="text" class="form-control m-input" placeholder="Nhập nội dung câu trả lời" name="answer">
                                                        </div>
                                                    </div>
                                                    <div class="d-md-none m--margin-bottom-10"></div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="m-radio-inline">
                                                        <label class="m-checkbox m-checkbox--state-success">
                                                            <input type="checkbox" name="is_correct" value="1">
                                                            Đáp áp đúng
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
                                                        <span>
                                                            <i class="la la-trash-o"></i>
                                                            <span>
                                                                Xóa
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-form__group form-group row">
                                        <label class="col-lg-2 col-form-label"></label>
                                        <div class="col-lg-4">
                                            <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                                                <span>
                                                    <i class="la la-plus"></i>
                                                    <span>
                                                        Thêm
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('question.index') }}">
                                    <i class="la la-arrow-circle-o-left"></i>
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    {{-- </form> --}}
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('#list-answer').repeater({
            initEmpty: false,

            defaultValues: {
                'text-input': 'foo'
            },

            show: function () {
                $(this).slideDown();
            },

            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });
    });
</script>
@stop