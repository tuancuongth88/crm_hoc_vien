@extends('administrator.app')
@section('title','Ngân hàng câu hỏi')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        @include('administrator.educations.question.search')
        <!--begin::Section-->
        @if (\Auth::user()->can('question.create'))
        <div class="col-xs-12" style="margin-bottom: 20px">
            <a href="{{ route('question.create') }}" class="btn btn-primary m-btn m-btn--icon">
                <span>
                    <i class="fa flaticon-plus"></i>
                    <span>
                        Thêm mới
                    </span>
                </span>
            </a>
        </div>
        @endif
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <table class="table m-table m-table--head-bg-warning">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Tên câu hỏi
                            </th>
                            <th>
                                Chương trình
                            </th>
                            <th>
                                Khóa học
                            </th>
                            <th>
                                Tùy chọn
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <th scope="row">
                                {{ $key + 1 }}
                            </th>
                            <td>
                                {{ $value['name'] }}
                            </td>
                            <td>
                                {{ @$value->subject->name }}
                            </td>
                            <td>
                                {{ @$value->lesson->name }}
                            </td>
                            <td>
                                @if (\Auth::user()->can('question.update'))
                                <a href="{{ route('question.edit', $value->id) }}" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                @endif
                                @if (\Auth::user()->can('question.delete'))
                                {{ Form::open(array('method'=>'DELETE', 'route' => array('question.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                    <i class="fa fa-close"></i>
                                </button>
                                {{ Form::close() }}
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            {{ $data->links() }}
            Tổng số {{ $data->total() }} bản ghi
        </div>
        <!--end::Section-->
    </div>
</div>
@stop