@extends('administrator.app')
@section('title','Tạo lĩnh vực')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 20px">
				<a href="{{ route('program.index') }}" class="btn btn-primary m-btn m-btn--icon">
					<span>
						<i class="fa flaticon-list-3"></i>
						<span>
							Danh sách lĩnh vực
						</span>
					</span>
				</a>
			</div>
	        <div class="col-md-12">
				@include('administrator.errors.errors-validate')
	            <!--begin::Portlet-->
	            <div class="m-portlet m-portlet--tab">
	                <div class="m-portlet__head">
	                    <div class="m-portlet__head-caption">
	                        <div class="m-portlet__head-title">
	                            <span class="m-portlet__head-icon m--hide">
	                                <i class="la la-gear"></i>
	                            </span>
	                            <h3 class="m-portlet__head-text">
	                                Thêm mới lĩnh vực
	                            </h3>
	                        </div>
	                    </div>
	                </div>
	                {{ Form::open(array('route' => 'program.store', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype' => 'multipart/form-data')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label>
                                        Tên lĩnh vực: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="Nhập tên" name="name" value="{{ old('name') }}">
                                    </div>
                                    <span class="m-form__help">
                                        Tên hiển thị Lĩnh vực
                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Vị trí hiển thị
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="number" name="position" class="form-control m-input">
                                    </div>
                                    <span class="m-form__help">
                                        đánh số vị trí hiển thị
                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Chọn ảnh:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="file" name="image_file" id="file_image">
                                    </div>
                                    <span class="m-form__help">
                                        Upload ảnh hiển thị lĩnh vực
                                    </span>
                                </div>

                                <div class="col-lg-6">
                                    <label>
                                        Chọn icon:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="file" name="icon_file" id="file_icon">
                                    </div>
                                    <span class="m-form__help">
                                        Upload ảnh icon cho lĩnh vực
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <label>
                                        Mô tả: <span class="required">*</span>
                                    </label>
                                    <textarea class="summernote" name="description" id="description">{{ old('description') }}</textarea>
                                    <span class="m-form__help">
                                        Mô tả Lĩnh vực
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('program.index') }}">
                                    <i class="la la-arrow-circle-o-left"></i>
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    {{-- </form> --}}
                    {{ Form::close() }}
	            </div>
	            <!--end::Portlet-->
	        </div>
	        <!--end::Form-->
	    </div>
	</div>
</div>
<script>
    $('document').ready(function(){
        $('.summernote').summernote({
            callbacks: {
                onImageUpload: function(image) {
                    uploadImageProject(image[0], $(this).attr('id'));
                }
            },
            height: 150,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture']],
                ['table', ['table']],
                ['view', ['fullscreen']],
                ['help', ['help']]
            ]
        });

        function uploadImageProject(image, el) {

            var data = new FormData();
            data.append("image", image);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/administrator/upload',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "post",
                success: function(response) {
                    if (response.status) {
                        var image = $('<img width="100%">').attr('src', response.data);
                        $('#' + el).summernote("insertNode", image[0]);
                    }

                },
                error: function(data) {

                }
            });
        }
        $('#file_image').on('change', function() {
            return ValidateFileUpload('file_image');
        });
        $('#file_icon').on('change', function() {
            return ValidateFileUpload('file_icon');
        });
        function ValidateFileUpload(id_file) {
            var fuData = document.getElementById(id_file);
            var FileUploadPath = fuData.value;

            if (FileUploadPath == '') {
                alert("Please upload an image");
            } else {
                var Extension = FileUploadPath.substring(
                    FileUploadPath.lastIndexOf('.') + 1).toLowerCase();

                if (Extension == "gif" || Extension == "png" || Extension == "bmp"
                    || Extension == "jpeg" || Extension == "jpg") {
                    if (fuData.files && fuData.files[0]) {
                        var reader = new FileReader();

                        reader.onload = function(e) {
                            $('#blah').attr('src', e.target.result);
                        }
                        reader.readAsDataURL(fuData.files[0]);
                    }

                } else {
                    alert("Chỉ cho phép định dạng GIF, PNG, JPG, JPEG and BMP. ");
                    $("input[type=file]").val('');
                }
            }
        }
    });
</script>
@stop