@extends('administrator.app')
@section('title','Danh mục Linh vực')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Danh sách Lĩnh vực đào tạo
                    </h3>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Section-->
            @if (\Auth::user()->can('program.create'))
                <div class="col-xs-12" style="margin-bottom: 20px">
                    <a href="{{ route('program.create') }}" class="btn btn-primary m-btn m-btn--icon">
                <span>
                    <i class="fa flaticon-plus"></i>
                    <span>
                        Thêm mới
                    </span>
                </span>
                    </a>
                </div>
            @endif
            @include('administrator.errors.info-search')
            <div class="m-section">
                @include('administrator.errors.messages')
                <div class="m-section__content">
                    <table class="table m-table m-table--head-bg-warning">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Tên Lĩnh vực
                            </th>
                            <th>
                                Chương trình
                            </th>
                            <th>
                                Mức hiển thị ưu tiên
                            </th>
                            <th>
                                Tùy chọn
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr>
                                <th scope="row">
                                    {{ $key + 1 }}
                                </th>
                                <td>
                                    {{ $value['name'] }}
                                </td>
                                <td>
                                    {{ $value->subject->count() }}
                                </td>
                                <td>
                                    {{ $value['position'] }}
                                </td>
                                <td>
                                    @if (\Auth::user()->can('program.update'))
                                        <a href="{{ route('program.edit', $value->id) }}"
                                           class="btn btn-accent m-btn m-btn--icon m-btn--icon-only tooltips"
                                           data-original-title="Sửa lĩnh vực"
                                           data-toggle="m-tooltip"
                                        >
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    @endif
                                    @if (\Auth::user()->can('program.delete'))
                                        {{ Form::open(array('method'=>'DELETE',
                                        'route' => array('program.destroy', $value->id),
                                        'style' => 'display: inline-block;')) }}
                                        <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                                class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
                                                data-original-title="Xóa lĩnh vực"
                                                data-toggle="m-tooltip"
                                        >
                                            <i class="fa fa-close"></i>
                                        </button>
                                        {{ Form::close() }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->links() }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
            <!--end::Section-->
        </div>
    </div>
@stop