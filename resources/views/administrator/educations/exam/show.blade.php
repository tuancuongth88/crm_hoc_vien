@extends('administrator.app')
@section('title','Danh sách')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--begin::Section-->
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--responsive-mobile m-portlet--warning m-portlet--head-solid-bg m-portlet--bordered" id="show-exam">
                    {{ Form::open(array('route' => ['exam.submit', $data->id], 'method' => 'PUT')) }}
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-technology"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    {{ $data->name }}
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                                <button type="submit" class="m-btn btn btn-secondary" onclick="return confirm('Bạn có chắc chắn muốn nộp bài?');">
                                    <i class="la la-floppy-o"></i>Nộp bài
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="margin-bottom-30">
                            <i>{{ $data->description }}</i><br>
                            <b>Thời gian:</b> {{ $data->start_time }} đến {{ $data->finish_time }}<br>
                            <b>Danh sách câu hỏi</b> <i>(Chọn câu trả lời đúng)</i>
                        </div>

                        <!--begin::Preview-->
                        <div class="m-list-timeline">
                            <div class="m-list-timeline__items">
                                @foreach ($listQuestion as $question)
                                <div class="m-list-timeline__item">
                                    <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                    <span class="m-list-timeline__icon flaticon-info"></span>
                                    <span class="m-list-timeline__text">
                                        <b>Câu hỏi:</b> {{ $question->name }} ?
                                        {{-- <span class="m-badge m-badge--success m-badge--wide">
                                            completed
                                        </span> --}}
                                    <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                                        @foreach ($question->hasAnswers as $answer)
                                            @if ($answer->question_id == $question->id)
                                                <div class="m-stack__item">
                                                    <label class="m-radio m-radio--check-bold m-radio--state-success">
                                                        {{ Form::radio('answer[' . $question->id . ']', $answer->id, false) }}
                                                        {{ $answer->name }}
                                                        <span></span>
                                                    </label>
                                                </div>
                                            @endif
                                        @endforeach

                                    </div>
                                    </span>
                                    {{-- <span class="m-list-timeline__time">
                                        Just now
                                    </span> --}}
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <!--end::Preview-->
                    </div>
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
@stop