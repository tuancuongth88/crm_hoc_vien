@extends('administrator.app')
@section('title','kỳ thi')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <a href="{{ route('exam.index') }}" class="btn btn-primary m-btn m-btn--icon">
                    <span>
                        <i class="fa flaticon-list-3"></i>
                        <span>
                            Danh sách kỳ thi
                        </span>
                    </span>
                </a>
            </div>
            <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Cập nhật kỳ thi
                                </h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => ['exam.update', $data->id], 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'id' => 'create-exam-form')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-8">
                                    <label>
                                        Tên kỳ thi: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="Nhập tên" name="name" value="{{ $data->name }}">
                                    </div>
                                    <span class="m-form__help">
                                        Tên hiển thị kỳ thi học
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-8">
                                    <label>
                                        Mô tả:
                                    </label>
                                    <textarea class="form-control m-input" name="description" rows="3">{{ $data->description }}</textarea>
                                    <span class="m-form__help">
                                        Tóm tắt về kỳ thi
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Chương trình: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        {!!Form::select('subject_id', $listSubject, $data->subject_id, ['class' => 'form-control'])!!}
                                    </div>
                                    <span class="m-form__help">
                                        Kỳ thi theo khóa học
                                    </span>
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Thời gian bắt đầu:
                                    </label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control m-input datetime-picker" readonly name="start_time" value="{{ $data->start_time }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help">
                                        Chọn thời gian bắt đầu kỳ thi
                                    </span>
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Thời gian kết thúc:
                                    </label>
                                    <div class="input-group date">
                                        <input type="text" class="form-control m-input datetime-picker" readonly name="finish_time" value="{{ $data->finish_time }}"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar glyphicon-th"></i>
                                            </span>
                                        </div>
                                    </div>
                                    <span class="m-form__help">
                                        Chọn thời gian kết thúc kỳ thi
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Giáo viên:  <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        {!!Form::select('teacher', $listTeacher, null, ['class' => 'form-control'])!!}
                                    </div>
                                    <span class="m-form__help">
                                        Giáo viên chấm bài thi
                                    </span>
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Chọn hình thức thi:  <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        {{ Form::select('status', ['' => 'Chọn hình thức thi'] + $listTypeExam, $data->status, ['class' => 'form-control', 'id' => 'exam_online_offline']) }}
                                    </div>
                                    <span class="m-form__help">
                                        Hình thức thi
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row" id="row_online_offline">
                                <div class="col-lg-12">
                                    <label>
                                        Lựa chọn câu hỏi cho kỳ thi:
                                    </label>
                                    <!--begin: Search Form -->
                                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                                        <div class="row align-items-center">
                                            <div class="col-xl-12 order-2 order-xl-1">
                                                <div class="form-group m-form__group row align-items-center">
                                                    <div class="col-md-3">
                                                        <div class="m-input-icon m-input-icon--left">
                                                            <input type="text" class="form-control m-input" placeholder="Câu hỏi..." id="name">
                                                            <span class="m-input-icon__icon m-input-icon__icon--left">
                                                                <span>
                                                                    <i class="la la-search"></i>
                                                                </span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        {{ Form::select('level', [0 => 'Chọn mức độ'] + $listLevel , null, ['class' => 'form-control m-select2', 'id' => 'level']) }}
                                                    </div>
                                                    <div class="col-md-3">
                                                        {{ Form::select('subject_id', [0 => 'Chọn Chương trình'] + $listSubject, null, ['class' => 'form-control m-select2', 'id' => 'subject_id']) }}
                                                    </div>
                                                    <div class="col-md-3">
                                                        <label class="m-checkbox m-checkbox--state-success">
                                                            <input type="checkbox" onclick="toggle(this);" id="select-all"/>
                                                            Chọn tất cả
                                                            <span></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Search Form -->
                                    <!--begin: Selected Rows Group Action Form -->
                                    <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30 collapse" id="m_datatable_group_action_form1">
                                        <div class="row align-items-center">
                                            <div class="col-xl-12">
                                                <div class="m-form__group m-form__group--inline">
                                                    <div class="m-form__label m-form__label-no-wrap">
                                                        <label class="m--font-bold m--font-danger-">
                                                            Đã chọn
                                                            <span id="m_datatable_selected_number1"></span>
                                                            câu hỏi:
                                                        </label>
                                                    </div>
                                                    <div class="m-form__control">
                                                        <div class="btn-toolbar">
                                                            &nbsp;&nbsp;&nbsp;
                                                            <button class="btn btn-sm btn-success" type="button" data-toggle="modal" data-target="#m_modal_fetch_id_server">
                                                                Danh sách câu hỏi đã chọn
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Selected Rows Group Action Form -->
                                    <!--begin: Datatable -->
                                    <div class="m_datatable" id="list-question"></div>
                                    <!--end: Datatable -->
                                    <!--begin::Modal-->
                                    <div class="modal fade" id="m_modal_fetch_id_server" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">
                                                        Các câu hỏi được chọn
                                                    </h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">
                                                            &times;
                                                        </span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="m-scrollable" data-scrollbar-shown="true" data-scrollable="true" data-max-height="200">
                                                        <ul class="m_datatable_selected_ids"></ul>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                        Đóng
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Modal-->
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('exam.index') }}">
                                    <i class="la la-arrow-circle-o-left"></i>
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    {{-- </form> --}}
                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
    $(document).ready(function () {
        $('.datetime-picker').datetimepicker({
            todayHighlight: true,
            autoclose: true,
            pickerPosition: 'top-left',
            todayBtn: true,
            format: 'dd-mm-yyyy h:i'
        });
        var id = {{ Request::segment(4) }};
        var options = {
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        // url: 'https://keenthemes.com/metronic/preview/inc/api/datatables/demos/default.php',
                        url: '/administrator/education/question/list',
                        method: 'get',
                        params: {
                            id: id
                        },
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: true, // enable/disable datatable scroll both horizontal and vertical when needed.
                height: 550, // datatable's body's fixed height
                footer: false, // display/hide footer
                spinner: {
                    overlayColor: '#000000',
                    opacity: 0,
                    type: 'loader',
                    state: 'brand',
                    message: true
                },
            },

            // column sorting
            sortable: true,

            pagination: true,
            translate: {
                records: {
                    processing: 'Vui lòng chờ...',
                    noRecords: 'Không có dữ liệu'
                },
                toolbar: {
                    pagination: {
                        items: {
                            default: {
                                first: 'Đầu',
                                prev: 'Trước',
                                next: 'Tiếp',
                                last: 'Cuối',
                                more: 'Thêm trang',
                                input: 'Số trang',
                                select: 'Chọn trang'
                            },
                        }
                    }
                }
            },
            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '#',
                    sortable: false,
                    width: 40,
                    textAlign: 'center',
                    template: function(row) {
                        if (row.checked) {
                            return '<input type="checkbox" class="checkbox-question" checked name="question[' + row.id + ']" value="' + row.id + '">';
                        }
                        return '<input type="checkbox" class="checkbox-question" name="question[' + row.id + ']" value="' + row.id + '">';
                    },
                    // selector: {class: 'm-checkbox--solid m-checkbox--brand'},
                }, {
                    field: 'name',
                    title: 'Câu hỏi',
                }, {
                    field: 'level',
                    title: 'Mức độ',
                }, {
                    field: 'subject_name',
                    title: 'Chương trình',
                }],
        };
        options.extensions = {
            checkbox: {},
        };
        options.search = {
            input: $('#generalSearch1'),
        };

        var datatable = $('#list-question').mDatatable(options);
        $('#m_datatable_group_action_form1').collapse('show');
        /*datatable.on('m-datatable--on-click-checkbox m-datatable--on-layout-updated', function(e) {
            // datatable.checkbox() access to extension methods
            var ids = datatable.checkbox().getSelectedId();
            console.log("datatable.checkbox()", datatable.checkbox());
            console.log("ids", ids);
            var count = ids.length;
            $('#m_datatable_selected_number1').html(count);
            if (count > 0) {
                $('#m_datatable_group_action_form1').collapse('show');
            } else {
                $('#m_datatable_group_action_form1').collapse('hide');
            }
        });*/

        $('#m_modal_fetch_id_server').on('show.bs.modal', function(e) {
            var selected = [];
            $("input[type='checkbox']:checked").each ( function() {
                selected.push($(this).val());
                console.log("$(this).val()", $(this).val());
            });
            // var ids = datatable.checkbox().getSelectedId();
            var arrId = JSON.stringify(selected);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/administrator/education/question/list-id',
                method: 'patch',
                data: arrId,
                contentType: 'application/json',
                success: function(response) {
                    var arrQuestion = response;
                    var c = document.createDocumentFragment();
                    for (var i = 0; i < selected.length; i++) {
                        if (selected[i] !== "on") {
                            var li = document.createElement('li');
                            li.setAttribute('data-id', selected[i]);
                            li.innerHTML = '<label class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill remove-question"><i class="fa fa-trash-o"></i></label> Câu hỏi: ' + arrQuestion[selected[i]].substring(0,20) + '...';
                            c.appendChild(li);
                        }
                    }
                    $(e.target).find('.m_datatable_selected_ids').append(c);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }).on('hide.bs.modal', function(e) {
            $(e.target).find('.m_datatable_selected_ids').empty();
        });
        // process the form
        $('#create-exam-form').submit(function(event) {
            var selected = [];
            $("input[type='checkbox']:checked").each ( function() {
                if ($(this).val() != 'on') {
                    selected.push($(this).val());
                }
            });
            // get the form data
            // there are many ways to get this data using jQuery (you can use the class or id also)
            var formData = {
                'name'         : $('input[name=name]').val(),
                'subject_id'   : $('select[name=subject_id]').val(),
                'teacher'      : $('select[name=teacher]').val(),
                'status'       : $('select[name=status]').val(),
                'description'  : $('textarea[name=description]').val(),
                'start_time'   : $('input[name=start_time]').val(),
                'finish_time'  : $('input[name=finish_time]').val(),
                'questions'    : selected
            };
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            // process the form
            $.ajax({
                type        : 'PUT', // define the type of HTTP verb we want to use (POST for our form)
                url         : $(this).attr('action'), // the url where we want to POST
                data        : formData, // our data object
                dataType    : 'json', // what type of data do we expect back from the server
                encode          : true
            })
            // using the done promise callback
            .done(function(response) {
                if (response.status) {
                    swal({
                        "title": "Thông báo!",
                        text: response.message,
                        type: 'warning',
                        showCancelButton: false,
                        confirmButtonText: '<i class="la la-check-circle"></i> Xác nhận!'
                    }).then(function(result) {
                        console.log("result", result);
                        if (result.value) {
                            window.location.href = "/administrator/education/exam";
                        }
                    });
                } else {
                    swal({
                        "title": "Thông báo!",
                        text: response.message,
                        type: 'warning',
                        showCancelButton: false,
                        confirmButtonText: 'Xác nhận!'
                    });
                }
                // log data to the console so we can see

                // here we will handle errors and validation messages
            });

            // stop the form from submitting the normal way and refreshing the page
            event.preventDefault();
        });
        $('.m-select2').select2();
        $('#subject_id').on('change', function() {
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'subject_id');
        });
        $('#level').on('change', function() {
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'level');
        });
        $('body').on('click', '.remove-question', function() {
            var ids = datatable.checkbox().getSelectedId();
            var item = $(this).parent().attr('data-id');
            ids.splice(ids.indexOf(item),1);
            $(this).parent().remove();
            $("input[type='checkbox']:checked").each ( function() {
                if (item == $(this).val()) {
                    $(this).prop('checked', false);
                }
            });
        });
        if($('#exam_online_offline').val() === '{{constant('App\Models\Education\Exam::OFFLINE_EXAM')}}') {
            $('#row_online_offline').hide();
        } else {
            $('#row_online_offline').show();
        }
    });
    function toggle(source) {
        $('#m_datatable_group_action_form1').collapse('show');
        var checkboxes = document.querySelectorAll('input[type="checkbox"]');
        for (var i = 0; i < checkboxes.length; i++) {
            if (checkboxes[i] != source)
                checkboxes[i].checked = source.checked;
        }
    }
</script>
@stop