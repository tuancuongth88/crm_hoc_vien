@extends('administrator.app')
@section('title','Danh sách')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Kỳ thi
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Section-->
            <div class="col-xs-12" style="margin-bottom: 20px">
                @if (\Auth::user()->can('exam.create'))
                    <a href="{{ route('exam.create') }}" class="btn btn-primary m-btn m-btn--icon">
		                <span>
		                    <i class="fa flaticon-plus"></i>
		                    <span>
		                        Thêm mới
		                    </span>
		                </span>
                    </a>
                @endif
                @if (\Auth::user()->can('exam.import.point'))
                    <a href="#" class="btn btn-accent" data-toggle="modal" data-target="#m_modal_import"
                       id="import_upload">
                        <i class="fa fa-upload"></i>
                        <span>Upload Điểm học viên</span>
                    </a>
                @endif
            </div>

            @include('administrator.errors.info-search')
            <div class="m-section">
                @include('administrator.errors.messages')
                <div class="m-section__content">
                    <table class="table m-table m-table--head-bg-warning">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Tên
                            </th>
                            <th>
                                Chương trình
                            </th>
                            <th>
                                Thời gian
                            </th>
                            <th>
                                Tùy chọn
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr>
                                <th scope="row">
                                    {{ $key + 1 }}
                                </th>
                                <td>
                                    {{ $value['name'] }}
                                </td>
                                <td>
                                    {{ $value->subject->name }}
                                </td>
                                <td>
                                    {{ $value->start_time }} <code>đến</code> {{ $value->finish_time }}
                                </td>
                                <td>
                                    @if($value->status > constant('App\Models\Education\Exam::ONLINE_EXAM'))
                                        <a href="{{ route('exam.export', [$value->id]) }}"
                                           class="btn btn-accent m-btn m-btn--icon m-btn--icon-only exportExam"
                                           data-original-title="Tải danh sách kỳ thi" data-toggle="m-tooltip">
                                            <i class="fa fa-file-excel-o"></i>
                                        </a>
                                    @endif
                                    @if (\Auth::user()->can('exam.update'))
                                        <a href="{{ route('exam.edit', $value->id) }}"
                                           class="btn btn-accent m-btn m-btn--icon m-btn--icon-only"
                                           data-original-title="Sửa kỳ thi"
                                           data-toggle="m-tooltip">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    @endif
                                    @if($value->status != constant('App\Models\Education\Exam::OFFLINE_EXAM'))
                                    <a href="{{ route('exam.show', $value->id) }}"
                                       class="btn btn-accent m-btn m-btn--icon m-btn--icon-only"
                                       data-original-title="Làm bài thi" data-toggle="m-tooltip">
                                        <i class="fa fa-leanpub"></i>
                                    </a>
                                    <a href="{{ route('exam.detail', [ $value->id, 'user_id' => \Auth::user()->id]) }}"
                                       class="btn btn-accent m-btn m-btn--icon m-btn--icon-only"
                                       data-original-title="Chi tiết bài thi" data-toggle="m-tooltip">
                                        <i class="fa fa-line-chart"></i>
                                    </a>
                                    @endif
                                    @if (\Auth::user()->can('exam.delete'))
                                        {{ Form::open(array('method'=>'1111',
                                        'route' => array('exam.destroy', $value->id),
                                        'style' => 'display: inline-block;',
                                         'class' => 'delete_exam', 'data-id' => $value->id)) }}
                                        <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                                class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
                                                data-original-title="Xóa kỳ thi" data-toggle="m-tooltip">
                                            <i class="fa fa-close"></i>
                                        </button>
                                        {{ Form::close() }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->links() }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
            <!--begin::Modal-->
            <div class="modal fade" id="m_modal_import" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">
                                Nhập kết quả thi
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                            </button>
                        </div>
                        <div class="modal-body" id="modal-container">
                            {!! Form::open(array('route' => 'exam.import','method'=>'POST','files'=>'true', 'id' => 'import_file')) !!}
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-12">
                                    <div class="form-group">
                                        {!! Form::label('Kết quả kỳ thi','',['class'=>'col-md-4']) !!}
                                        <div class="col-md-12">
                                            {!! Form::file('sample_file', array('class' => 'form-control', 'id' => 'sample_file')) !!}
                                            {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                    {!! Form::submit('Upload',['class'=>'btn btn-primary']) !!}
                                </div>
                            </div>
                            {!! Form::close() !!}
                            <div id="append_total"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Modal-->
            <!--end::Section-->
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#import_file').submit(function (e) {
                e.preventDefault();
                var formData = new FormData();
                var totalFiles = document.getElementById("sample_file").files.length;
                if (totalFiles > 0) {
                    var file = document.getElementById("sample_file").files[0];
                    formData.append("sample_file", file);
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '{{ route('exam.import') }}',
                        type: 'POST',
                        data: formData,
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            if (response.data_false === undefined) {
                                alert(response.message);
                                $('#m_modal_import').modal('hide');
                            } else {
                                var total = 0;
                                var items = [];
                                $.each(response.data_false, function (i) {
                                    items.push('<li style="display: none">' + response.data_false[i].email + ' | ' + '<span>' + response.data_false[i].message + '</span>' + '</li>');
                                    total++;
                                });
                                if (total > 0) {
                                    $("#append_total").append('<span> Số lượng học viên không thành công: ' + '<span style="font-weight: bold">' + total + '/' + response.data_total + '</span>' + '</span><div><i class="fa fa-caret-right" style="color: #f48120;"></i><a href="#" id="detail_append" style="font-weight: bold;">Xem chi tiết</a><ol id="append_d">\n' +
                                        '</ol></div>');
                                    $("#detail_append").click(function () {
                                        $("li", "#append_d").toggle();
                                    });
                                    $("#append_d").append(items.join(''));
                                } else {
                                    $("#append_total").append('');
                                    $('#m_modal_import').modal('hide');
                                }
                            }
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                } else {
                    alert('Bạn phải chọn file');
                }
            });
            $('body').on('click', '.close', function () {
                $('#append_total').empty();
            });
            $('.delete_exam').submit(function (e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var $tr = $(this).closest('tr');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: 'exam' + '/' + id,
                    type: 'DELETE',
                    dataType: 'json',
                    data: {
                        'id': id
                    },
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        if (response.delete_false == '') {
                            $tr.find('td').fadeOut(1000, function () {
                                $tr.remove();
                            });
                            alert(response.message);
                        } else {
                            $.each(response.delete_false, function (i) {
                                alert(response.delete_false[i]);
                            });
                        }
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
            });
        });
    </script>
@stop