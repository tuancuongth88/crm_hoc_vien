@extends('administrator.app')
@section('title','Danh sách')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--begin::Section-->
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--responsive-mobile m-portlet--warning m-portlet--head-solid-bg m-portlet--bordered m-portlet m-portlet--tabs" id="show-exam">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon">
                                    <i class="flaticon-technology"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    {{ $data->name }}
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--right m-tabs-line-danger" role="tablist">
                                @if (\Auth::user()->can('is-manager'))
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_portlet_tab_1_1" role="tab">
                                        Nhân viên
                                    </a>
                                </li>

                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link
                                    @if (\Auth::user()->can('is-manager')) active @endif" data-toggle="tab" href="#m_portlet_tab_1_2" role="tab">
                                        Chi tiết
                                    </a>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane" id="m_portlet_tab_1_1">
                                <div class="m-widget4 m-widget4--progress">
                                    @foreach ($listUser as $user)
                                    <div class="m-widget4__item">
                                        <div class="m-widget4__img m-widget4__img--pic">
                                            <img src="{{ $user->avatar }}" alt="">
                                        </div>
                                        <div class="m-widget4__info">
                                            <span class="m-widget4__title">
                                                {{ $user->fullname }}
                                            </span>
                                            <br>
                                            <span class="m-widget4__sub">
                                                {{ $user->phone }}
                                            </span>
                                        </div>
                                        <div class="m-widget4__progress">
                                            <div class="m-widget4__progress-wrapper">
                                                <span class="m-widget17__progress-number">
                                                    {{ isset($listExamResult[$user->id]) ? $listExamResult[$user->id] . ' điểm' : 'Chưa làm bài' }}
                                                </span>
                                                <span class="m-widget17__progress-label">
                                                    @if (isset($listExamResult[$user->id]))
                                                        <span class="m-badge m-badge--{{ getClassByRatePoint($listExamResult[$user->id]) }} m-badge--wide">
                                                        {{ rateByPoint($listExamResult[$user->id]) }}
                                                        </span>
                                                    @endif
                                                </span>
                                                <div class="progress" style="display: grid;">
                                                    <div class="progress-bar progress-bar-striped progress-bar-animated  bg-{{ isset($listExamResult[$user->id]) ? getClassByRatePoint($listExamResult[$user->id]) : 'metal' }}" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100" style="width: {{ isset($listExamResult[$user->id]) ? ($listExamResult[$user->id] / 10) * 100 : 0 }}%"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-widget4__ext">
                                            <a href="{{ route('exam.detail', Request::segment(5)). '?user_id=' .$user->id }}"  class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary show-result">
                                                Xem
                                            </a>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>

                                <label for="" class="m--margin-top-30">
                                    {{ $listUser->links() }}
                                </label>
                            </div>
                            <div class="tab-pane
                            @if (\Auth::user()->can('is-manager')) active @endif " id="m_portlet_tab_1_2">
                                <div class="col-xl-12">
                                    <!--begin:: Widgets/Profit Share-->
                                    <div class="m-widget14">
                                        <div class="m-widget14__header">
                                            <h3 class="m-widget14__title">
                                                Điểm số
                                            </h3>
                                            <span class="m-widget14__desc">
                                                Bạn đã đạt được trong kỳ thi
                                            </span>
                                        </div>
                                        <div class="row  align-items-center">
                                            <div class="col">
                                                <div id="m_chart_profit_share" class="m-widget14__chart" style="height: 160px">
                                                    <div class="m-widget14__stat">
                                                        {{ $obj->point }} điểm
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="m-widget14__legends">
                                                    <div class="m-widget14__legend">
                                                        <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                                        <span class="m-widget14__legend-text">
                                                            {{ $percentCorrect }} % Đúng
                                                        </span>
                                                    </div>
                                                    <div class="m-widget14__legend">
                                                        <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                                        <span class="m-widget14__legend-text">
                                                            {{ $percentWrong }} % Sai
                                                        </span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end:: Widgets/Profit Share-->
                                </div>
                                <div class="margin-bottom-30">
                                    <i>{{ $data->description }}</i><br>
                                    <i class="la la-user btn-outline-success"></i>
                                    <b>Người thực hiện:</b> {{ \App\Models\Users\User::find(Auth::user()->id)->fullname }}<br>
                                    <i class="la la-clock-o btn-outline-success"></i>
                                    <b>Thời gian:</b> {{ $data->start_time }} đến {{ $data->finish_time }}<br>
                                    <i class="la la-clock-o btn-outline-success"></i>
                                    <b>Thời gian nộp bài:</b> {{ $obj->created_at }}<br>
                                    <b>Danh sách câu hỏi</b> <i>(Chọn câu trả lời đúng)</i>
                                </div>

                                <!--begin::Preview-->
                                <div class="m-list-timeline">
                                    <div class="m-list-timeline__items">
                                        @foreach ($listQuestion as $key => $question)
                                        <div class="m-list-timeline__item">
                                            <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                            <span class="m-list-timeline__icon flaticon-info"></span>
                                            <span class="m-list-timeline__text">
                                                <b>Câu số {{ $key + 1 }}:</b> {{ $question->name }} ?
                                                {{-- <span class="m-badge m-badge--success m-badge--wide">
                                                    Đáp án đúng:
                                                </span> --}}
                                            <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                                                @foreach ($question->hasAnswers as $answer)
                                                    @if ($answer->question_id == $question->id)
                                                        <div class="m-stack__item">
                                                            <label class="m-checkbox m-checkbox--state-success">
                                                                {{ Form::checkbox('answer', ONE, (in_array($answer->id, explode(',', $obj->answers) )) ? true : false, ['disabled' => true]) }}
                                                                {{ $answer->name }}
                                                                @if ($answer->is_correct == ONE)
                                                                    <i class="la la-check btn-outline-danger"></i>
                                                                @endif
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    @endif
                                                @endforeach

                                                {{-- <span class="m-list-timeline__time">
                                                    Đáp án đúng: {{ $answer->name }}
                                                </span> --}}
                                            </div>
                                            </span>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                <!--end::Preview-->
                            </div>
                        </div>

                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function (argument) {
        var profitShare = function() {
            if ($('#m_chart_profit_share').length == 0) {
                return;
            }

            var chart = new Chartist.Pie('#m_chart_profit_share', {
                series: [{
                        value: {{ $percentCorrect }},
                        className: 'custom',
                        meta: {
                            color: mUtil.getColor('accent')
                        }
                    },
                    {
                        value: {{ $percentWrong }},
                        className: 'custom',
                        meta: {
                            color: mUtil.getColor('warning')
                        }
                    },
                ],
                labels: [1, 2, 3]
            }, {
                donut: true,
                donutWidth: 17,
                showLabel: false
            });

            chart.on('draw', function(data) {
                if (data.type === 'slice') {
                    // Get the total path length in order to use for dash array animation
                    var pathLength = data.element._node.getTotalLength();

                    // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                    data.element.attr({
                        'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                    });

                    // Create animation definition while also assigning an ID to the animation for later sync usage
                    var animationDefinition = {
                        'stroke-dashoffset': {
                            id: 'anim' + data.index,
                            dur: 1000,
                            from: -pathLength + 'px',
                            to: '0px',
                            easing: Chartist.Svg.Easing.easeOutQuint,
                            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                            fill: 'freeze',
                            'stroke': data.meta.color
                        }
                    };

                    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                    if (data.index !== 0) {
                        animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                    }

                    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

                    data.element.attr({
                        'stroke-dashoffset': -pathLength + 'px',
                        'stroke': data.meta.color
                    });

                    // We can't use guided mode as the animations need to rely on setting begin manually
                    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                    data.element.animate(animationDefinition, false);
                }
            });

            // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
            chart.on('created', function() {
                if (window.__anim21278907124) {
                    clearTimeout(window.__anim21278907124);
                    window.__anim21278907124 = null;
                }
                window.__anim21278907124 = setTimeout(chart.update.bind(chart), 15000);
            });
        };
        profitShare();
    });
</script>
@stop