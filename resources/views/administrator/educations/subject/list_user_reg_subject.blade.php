@extends('administrator.app')
@section('title','Danh sách học viên đăng ký chương trình')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Danh sách học viên đăng ký chương trình
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px">
                    <a href="{{ route('subject.index') }}" class="btn btn-success">Danh sách Chương trình</a>
                </div>
                <div class="col-md-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="row" style="font-size: 24px;">
                                <div class="col-md-6">
                                    Lĩnh vực: <span style="color: red;"> {{ $subject->program->name }}</span>
                                </div>
                                <div class="col-md-6">
                                    Chương trình: <span style="color: red;"> {{ $subject->name }}</span>
                                </div>
                                <div class="col-md-6">Thời gian khai giảng: <span style="color: red;"
                                                                                  style="margin-top: 30px;"> {{ $subject->open_time }}</span>
                                </div>
                                <div class="col-md-6">Thời gian kết thúc: <span
                                            style="color: red;"> {{ $subject->close_time }}</span></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="row" style="font-size: 18px; padding-top: 10px; padding-bottom: 10px;">
                                <div class="col-md-12">
                                    Danh sách học viên
                                </div>
                            </div>
                            @include('administrator.errors.messages')
                            <div class="m-section__content">
                                <table class="table m-table m-table--head-bg-warning">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th> Tên học viên</th>
                                        <th>Tổng điểm Chương trình</th>
                                        <th>Điểm trung binh Chương trình</th>
                                        <th>
                                            Thời gian đăng ký
                                        </th>
                                        <th>
                                            Đánh giá học viên
                                        </th>
                                        <th>
                                            Action
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($listUserReg as $key => $value)
                                        <tr>
                                            <th scope="row">
                                                {{ $key + 1 }}
                                            </th>
                                            <td>
                                                {{ $value->user->fullname }}
                                            </td>
                                            <td>
                                                {{ $value->total_point }}
                                            </td>
                                            <td>
                                                {{ $value->avg_point }}
                                            </td>
                                            <td>
                                                {{ $value->created_at }}
                                            </td>
                                            <td>
                                                @if (\Auth::user()->can('subject.user.assessment'))
                                                    {{ Form::select('pass', [0 => 'Lựa chọn'] + $listStatusPass, $value->pass, ['class' => 'form-control m-input change_pass_user', 'data-id' => $value->id , 'data-user-id' => $value->user_id]) }}
                                                @else
                                                    {{ Form::select('pass', [0 => 'Lựa chọn'] + $listStatusPass, $value->pass, ['class' => 'form-control m-input', 'disabled' => 'disabled']) }}
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ route('subject.user.exam-history', [$value->subject_id,$value->user_id]) }}"
                                                   class="btn btn-primary"
                                                   data-original-title="Lịch sử kỳ thi"
                                                   data-toggle="m-tooltip"
                                                >
                                                    <i class="fa fa-list"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script type="text/javascript">
        $(document).ready(function (argument) {
            $('.change_pass_user').change(function () {
                var status = $(this).val();
                var textSelect = $(this).find("option:selected").text();
                if (status > 0) {
                    var checkConfirm = confirm("Bạn muốn đánh giá học viên này " + textSelect + "?");
                    if (checkConfirm == true) {
                        var id = $(this).attr('data-id');
                        var dataForm = {id: id, pass: status};
                    }
                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: '/administrator/education/subject/user-reg/user-assessment/',
                        type: 'POST',
                        data: dataForm,
                        dataType: "json",
                        success: function (data) {
                            alert(data.message);
                        }
                    })
                }
            });
        });
    </script>
@stop
