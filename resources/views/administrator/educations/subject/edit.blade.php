@extends('administrator.app')
@section('title','Sửa Chương trình')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader">
	    <div class="d-flex align-items-center">
	        <div class="mr-auto">
	            <h3 class="m-subheader__title m-subheader__title--separator">
					Sửa Chương trình
	            </h3>
	            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
	                <li class="m-nav__item m-nav__item--home">
	                    <a href="#" class="m-nav__link m-nav__link--icon">
	                        <i class="m-nav__link-icon la la-home"></i>
	                    </a>
	                </li>
	            </ul>
	        </div>
	    </div>
	</div>
	<!-- END: Subheader -->
	<div class="m-content">
		<div style="margin-bottom: 20px">
			<a href="{{ route('subject.index') }}" class="btn btn-success">Danh sách Chương trình</a>
        	<a href="{{ route('subject.create') }}" class="btn btn-primary">Thêm Chương trình</a>
		</div>
		<div class="row">
	        <div class="col-md-12">
	            <!--begin::Portlet-->
				@include('administrator.errors.errors-validate')
	            <div class="m-portlet m-portlet--tab">
	                <div class="m-portlet__head">
	                    <div class="m-portlet__head-caption">
	                        <div class="m-portlet__head-title">
	                            <span class="m-portlet__head-icon m--hide">
	                                <i class="la la-gear"></i>
	                            </span>
	                            <h3 class="m-portlet__head-text">
	                                Cập nhật Chương trình
	                            </h3>
	                        </div>
	                    </div>
	                </div>
	                {{ Form::open(array('route' => array('subject.update', 'id' => $data->id), 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
	                	<meta name="csrf-token" content="{{ csrf_token() }}">
	                    <div class="m-portlet__body row">
                            <div class="col-lg-6">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Tên Chương trình<span style="color: red;">(*)</span>
                                    </label>
                                    <input type="text" class="form-control m-input" id="name" placeholder="Tên Chương trình" name="name" value="{{ $data->name }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group">
                                    <label for="name">
                                        Tên viết tắt<span style="color: red;">(*)</span>
                                    </label>
                                    <input type="text" class="form-control m-input" id="short_name" placeholder="Tên viết tắt" name="short_name" value="{{ $data->short_name }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label for="name">
                                        Thuộc Lĩnh vực <span style="color: red;">(*)</span>
                                    </label>
                                {{ Form::select('program_id', [ '' => '---Lựa chọn---'] + $listProgram, $data->program_id, ['class' => 'form-control', 'id' => 'program_id', 'required']) }}
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label for="name">
                                        Vị trí hiển thị ưu tiên <span style="color: red;"></span>
                                    </label>
                                    <input type="number" class="form-control m-input" id="position" placeholder="Vị trí hiển thị ưu tiên" name="position" value="{{ $data->position }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label for="name">
                                        Thời gian bắt đầu Chương trình <span style="color: red;">(*)</span>
                                    </label>
                                    <input type="text" name="open_time" id="open_time" class="form-control m-input" placeholder="Ngày khai giảng" value="{{ $data->open_time }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label for="name">
                                        Thời gian kết thúc Chương trình <span style="color: red;">(*)</span>
                                    </label>
                                    <input type="text" name="close_time" id="close_time" class="form-control m-input" placeholder="Thời gian kết thúc khoá học" value="{{ $data->close_time }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label for="name">
                                        Lịch học<span style="color: red;"></span>
                                    </label>
                                    <input type="text" name="schedule" id="schedule" class="form-control m-input" placeholder="Nhập lịch học VD: thứ 2 - thứ 7" value="{{ $data->schedule }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label for="name">
                                        Khung giờ học<span style="color: red;"></span>
                                    </label>
                                    <input type="text" name="school_time" id="school_time" class="form-control m-input" placeholder="Nhập khung giờ học VD: 8h - 12h" value="{{ $data->school_time }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label for="name">
                                        Thời lượng<span style="color: red;"></span>
                                    </label>
                                    <input type="text" name="total_time" id="total_time" class="form-control m-input" placeholder="Thời lượng VD: 30 ngày" value="{{ $data->total_time }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label for="name">
                                        Phí tham dự<span style="color: red;"></span>
                                    </label>
                                    <input type="number" name="total_fee" id="total_fee" class="form-control m-input" placeholder="Nhập phí tham dự" value="{{ $data->total_fee }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group m-form__group ">
                                    <label for="name">
                                        Phí ưu đãi<span style="color: red;"></span>
                                    </label>
                                    <input type="number" name="fee_discount" id="fee_discount" class="form-control m-input" placeholder="Nhập % phí ưu đãi" value="{{ $data->fee_discount }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="m-form__group form-group ">
                                    <label class="col-form-label">Xuất bản :</label>
                                    <span class="m-switch m-switch--icon m-switch--warning" style="width: 100%;">
                                        <label>
                                            <input type="checkbox" name="status" {{ $data->status == 1 ? 'checked' : '' }}>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="m-form__group form-group ">
                                    <label class="col-form-label">Nổi bật :</label>
                                    <span class="m-switch m-switch--icon m-switch--warning" style="width: 100%;">
                                        <label>
                                            <input type="checkbox" name="is_hot" {{ $data->status == 1 ? 'checked' : '' }}>
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Mô tả:
                                </label>
                                <textarea type="text" class=" summernote" id="description" placeholder="Mô tả ngắn" name="description">{{ $data->description }}</textarea>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group ">
                                <label for="name"> Mục tiêu:
                                </label>
                                <textarea class="summernote" name="target" id="target">{{ $data->target }}</textarea>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group ">
                                <label for="name">
                                    Thông điệp:
                                </label>
                                <textarea class="summernote" name="message" id="message">{{ $data->message }}</textarea>
                            </div>
                        </div>
						<div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Upload File
                                </label>
                                <div  class="m-dropzone dropzone m-dropzone--primary" id="attachment">
                                    <div class="m-dropzone__msg dz-message needsclick">
                                        <h3 class="m-dropzone__msg-title">
                                            Kéo thả file vào đây để upload
                                        </h3>
                                        <span class="m-dropzone__msg-desc">
                                            Tối đa 10 File kích thước 5MB/file
                                        </span>
                                    </div>
                                </div >
								@foreach($data->documents->toArray() as $item)
									<a href="{{$item['file_url'] }}">{{ $item['name'] }}</a><br>
								@endforeach
                            </div>
                        </div>
                        <input type="hidden" name="id_file" id="id_file" >
	                    <div class="m-portlet__foot m-portlet__foot--fit">
	                        <div class="m-form__actions">
	                            <button class="btn btn-primary">
	                                Lưu
	                            </button>
	                            <a class="btn btn-secondary" href="{{ route('subject.index') }}">
	                                Trở về danh sách
	                            </a>
	                        </div>
	                    </div>
	                {{-- </form> --}}
	                {{ Form::close() }}
	            </div>
	            <!--end::Portlet-->
	        </div>
	        <!--end::Form-->
	    </div>
	</div>
</div>
<input type="hidden" name="attach" id="attach" value="{{ json_encode($data->documents->toArray()) }}">
<script src="{{ URL::asset('assets/demo/default/custom/components/forms/widgets/bootstrap-switch.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('assets/demo/default/custom/components/forms/widgets/bootstrap-select.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function (argument) {


        $('.m-select2').select2({
            placeholder: "Select a value",
        });

        $('#open_time').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        $('#close_time').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });

		var listAttachment = JSON.parse($('#attach').val());
        $("#attachment").dropzone({
            addRemoveLinks: true,
            url: '{{ route('subject.upload-attachment') }}',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            paramName: "file",
            dictRemoveFile: "Xóa",
            dictRemoveFileConfirmation: "Bạn có chắc muốn xóa tệp này",
            acceptedFiles: "image/*,application/pdf,.doc,.docx,.xls,.xlsx,.csv,.tsv,.ppt,.pptx,.pages,.odt,.rtf",
            dictInvalidFileType: "Định dạng file không phải dạng cho phép",
            dictFileTooBig: "Dung lượng file quá lớn vượt giới hạn cho phép",
            maxFilesize: 10,
            removedfile: function(file, test) {
                var fileRemove = findObjectByKey(listAttachment, 'name', file.name);
                $.ajax({
                    type: 'DELETE',
                    url: '{{ route('subject.delete-attachment') }}',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        id: fileRemove.id,
                        url: fileRemove.file_url,
                    },
                    sucess: function(data) {

                    }
                });
                listAttachment = $.grep(listAttachment, function(e){
                    return e.id != fileRemove.id;
                });
                var nameArray = listAttachment.map(function (el) { return el.id; });
                $('#id_file').val(nameArray);
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            init: function() {
            	thisDropzone = this;
            	$.each(listAttachment, function (key, value) {
                    thisDropzone.emit("addedfile", value);
                    thisDropzone.options.thumbnail.call(thisDropzone, value, value.file_url);
                    thisDropzone.emit("complete", value);
                    var nameArray = listAttachment.map(function (el) { return el.id; });
                    $('#id_file').val(nameArray);
                });
                this.on("success", function(file, response) {
                    listAttachment.push(response.data);
                    var nameArray = listAttachment.map(function (el) { return el.id; });
                    $('#id_file').val(nameArray);
                    alert(response.message);
                });
                // Handle errors
                this.on('error', function(file, response) {
                    alert('File tải lên không phù hợp');
                });
            }
        });

        $('.summernote').summernote({
            callbacks: {
                onImageUpload: function(image) {
                    uploadImageProject(image[0], $(this).attr('id'));
                }
            },
            height: 150,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture']],
                ['table', ['table']],
                ['view', ['fullscreen']],
                ['help', ['help']]
            ]
        });

        function uploadImageProject(image, el) {

            var data = new FormData();
            data.append("image", image);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/administrator/upload',
                cache: false,
                contentType: false,
                processData: false,
                data: data,
                type: "post",
                success: function(response) {
                    if (response.status) {
                        var image = $('<img width="100%">').attr('src', response.data);
                        $('#' + el).summernote("insertNode", image[0]);
                    }

                },
                error: function(data) {

                }
            });
        }
    });
</script>
@stop