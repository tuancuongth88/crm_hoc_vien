@extends('administrator.app')
@section('title','Thư viện khóa học')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Danh sách thư viện khóa học
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
        @include('administrator.educations.subject.search-lesson')
        <!--begin::Section-->

            @include('administrator.errors.info-search')
            <div class="m-section">
                @include('administrator.errors.messages')
                <div class="m-section__content">
                    <table class="table m-table m-table--head-bg-warning">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Tên Khóa học</th>
                            <th>Giáo viên</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr>
                                <th scope="row">
                                    {{ $key + 1 }}
                                </th>

                                <td>
                                    <a href="">{{ $value['name'] }}</a>
                                </td>
                                <td>
                                    {{ Form::select('user_id', ['' => 'Chọn tài khoản'] + $listUser , old('user_id'), ['class' => 'form-control select_teacher_action', 'lesson-id' => $value->id]) }}
                                </td>
                                <td>
                                    <a id='add_new_lessson_{{ $value->id }}' data-lesson="{{ $value->id }}" data-user-id="0" class="btn btn-primary add_new_lessson">Add</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->links() }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
            <!--end::Section-->
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <input type="hidden" name="subject_id" id="subject_id" value="{{ $id }}">
<script>
    $(document).ready(function () {
        $('.select_teacher_action').change(function () {
            var user_id = $(this).val();
            var lesson_id = $(this).attr('lesson-id');
            $("#add_new_lessson_"+lesson_id).attr('data-user-id', user_id);
        });

        $('.add_new_lessson').click(function (e) {
            e.preventDefault();
            var comfirm = confirm("Bạn muốn thêm khóa học này vào Chương trình? ");
            if(comfirm == true){
                var lesson_id = $(this).attr('data-lesson');
                var user_id = $(this).attr('data-user-id');
                var subject_id = $('#subject_id').val();
                if(user_id < 1){
                    alert("Bạn phải chọn giáo viên");
                    return;
                }
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: '{{ route('subject.store-add-lesson-to-subject') }}',
                    type: 'POST',
                    data: {lesson_id: lesson_id, user_id: user_id, subject_id: subject_id},
                    dataType: 'json',
                    success: function (data) {
                        alert(data.message);
                        if(data.code == '00'){
                            window.location.href = '/administrator/education/subject/show-lesson-by-subject/'+ subject_id;
                        }
                    }
                })
            }
        });
    });
</script>
@stop