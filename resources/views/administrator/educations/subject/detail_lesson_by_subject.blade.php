@extends('administrator.app')
@section('title','Danh sách khóa học thuộc chương trình')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Danh sách khóa học thuộc chương trình
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px">
                    <a href="{{ route('subject.index') }}" class="btn btn-success">Danh sách Chương trình</a>
                </div>
                <div class="col-md-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="row" style="font-size: 24px;">
                                <div class="col-md-6">
                                    Lĩnh vực: <span style="color: red;"> {{ $subject->program->name }}</span>
                                </div>
                                <div class="col-md-6">
                                    Chương trình: <span style="color: red;"> {{ $subject->name }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Form-->
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            @if(\Auth::user()->can('subject.add-lesson-to-subject'))
                                <div class="row" style="font-size: 18px; padding-top: 10px; padding-bottom: 10px;">
                                    <div class="col-md-12">
                                        <a href="{{ route('subject.add-lesson-to-subject', $subject->id) }}"
                                           id="add_new_lessson" class="btn btn-primary">Thêm khóa học</a>
                                    </div>
                                </div>
                            @endif
                            <div class="row" style="font-size: 18px; padding-top: 10px; padding-bottom: 10px;">
                                <div class="col-md-12">
                                    Danh sách Khóa học
                                </div>
                            </div>
                            @include('administrator.errors.messages')
                            <div class="m-section__content m-portlet m-portlet--mobile">
                                <div class="m_datatable" id="child_data_local"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
    @include('administrator.educations.subject.script')
    <script type="text/javascript">
        $(document).ready(function (argument) {
            $('.m-select2').select2({
                placeholder: "Select a value",
            });
            $('#type_user').change(function () {
                $('#user_id').empty();
                $('#user_id').append("<option value=''>Lựa chọn</option>");
                var type = $("#type_user").val();
                $.ajax({
                    url: '/administrator/education/subject/list-user/' + type,
                    type: 'GET',
                    success: function (data) {
                        $.each(data, function (i, item) {
                            $('#user_id').append("<option value=" + i + ">" + item + "</option>");
                        })
                    }
                })
            });
        });

        function CallUploadFile(id) {
            $("#uploadFile.uploadFile" + id).click();

        }


    </script>
@stop
