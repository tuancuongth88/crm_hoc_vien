<script>
    // update sale
    $('.agent-selected').change(function () {
        var id = $(this).children(":selected").attr("id");
        var customer_id = $(this).children(":selected").attr("customer_id");
        $.ajax({
            url: '',
            data: {sale: id, customer_id: customer_id},
            success: function (result) {

            },
            error: function (data) {

            }
        })
    });

    //  load index
    jQuery(document).ready(function () {
        DatatableRemoteAjaxDemo.init(window.location.href);
    });

    var datatable;

    function index(url) {
        datatable = $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        dataType: 'json',
                        url: url,
                    },
                },
                pageSize: 20, // display 20 records per page
                serverPaging: true,
                serverFiltering: false,
                serverSorting: true,
            },

            // layout definition
            layout: {
                theme: 'default',
                scroll: false,
                height: null,
                footer: false,
            },

            sortable: true,

            filterable: false,

            pagination: true,

            detail: {
                title: 'Xem lịch sửa chăm sóc',
                content: childTable,
            },

            search: {
                input: $('#generalSearch'),
            },

            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '',
                    sortable: false,
                    width: 20,
                    textAlign: 'center' // left|right|center,
                }, {
                    field: 'lesson_name',
                    title: 'Khóa học',
                }, {
                    field: 'fullname',
                    title: 'Giáo viên',
                },
                {
                    field: 'Actions',
                    width: 110,
                    title: 'Actions',
                    sortable: false,
                    overflow: 'visible',
                    template: function (row, index, datatable) {
                        var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                        var user_id = {{\Auth::user()->id}}
                            html = '';
                        @if(\Auth::user()->can('subject.remove-lesson-to-subject'))
                            html += '<form method="POST" action="' + row.action_delete + '" accept-charset="UTF-8" style="display: inline-block;"><input name="_method" type="hidden" value="DELETE">';
                            html += ' <input name="_token" type="hidden" value="{{ csrf_token() }}">';
                            html += '<button onclick="' + "return confirm('Bạn có chắc chắn muốn xóa?');" + '" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">';
                            html += '<i class="fa fa-close"></i>';
                            html += '</button></form>';
                        @endif
                        if (user_id == row.user_id) {
                            html += '<button class="profilecard__img wf-80 wf-md-126 mr-md-5 mr-3" id="imgAvatar"  onclick="CallUploadFile(' + row.id + ')" title="Thêm tài liệu">';
                            html += '<i class="fa fa-upload"></i></button>';
                            html += '<input type="file" name="uploadFile" id="uploadFile" multiple="" data-item-id="' + row.id + '"style="display: none;" class="uploadFile' + row.id + '" >';
                        }
                        return html;
                    },
                }],
        });
        return datatable;
    }

    var DatatableRemoteAjaxDemo = function () {
        var demo = function (url) {
            index(url);
        };

        return {
            // public functions
            init: function (url) {
                demo(url);
            },
        };
    }();


    // load child
    function childTable(dataChild) {
        var user_id =
        {{\Auth::user()->id}}
        if (dataChild.user_id !== user_id) {
            return ;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: '../document/lesson-subject/',
            type: 'POST',
            data: {id: dataChild.data.id, model: 'SubjectLesson'},
            success: function (response) {
                $('<div/>').attr('id', 'child_data_local_' + dataChild.data.id).appendTo(dataChild.detailCell).mDatatable({
                    data: {
                        type: 'local',
                        source: response.data,
                        pageSize: 10
                    },
                    // layout definition
                    layout: {
                        theme: 'default',
                        scroll: true,
                        height: 300,
                        footer: false,

                        // enable/disable datatable spinner.
                        spinner: {
                            type: 1,
                            theme: 'default',
                        },
                    },

                    sortable: true,

                    // columns definition
                    columns: [
                        {
                            field: 'name',
                            title: 'Tên tài liệu',
                        }, {
                            field: 'created_at',
                            title: 'Ngày tạo',
                        }, {
                            field: 'Actions',
                            width: 110,
                            title: 'Actions',
                            sortable: false,
                            overflow: 'visible',
                            template: function (row, index, datatable) {
                                var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                                return '<a href="{{ url()->current() }}/' + row.id + '/edit" id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill delete_document_id" title="Xóa">\
                                        <i class="la la-trash"></i></a>\
                                        <a href="../document/download-file/' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Tải file">\\\n' +
                                    '                                        <i class="la la-download"></i></a>';
                            },
                        }
                    ],
                });
            }
        });
    }

    // add history
    $('body').on('click', '.add_row_child', function () {
        $('#phone').val("");
        $('#callback').val("");
        $('#note').val("");
        $('#channel').val(0);
        var id = $(this).attr('id');
        $('#customer_id').val(id);
        $.ajax({
            url: 'history/' + id,
            type: 'GET',
            dataType: "json",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                console.log(data);
                $('#m_modal_4').modal('toggle');
                $('#phone').val(data.customer.phone);
            }

        })
    });

    jQuery(document).ready(function () {


        $('#submit_form_history').submit(function (e) {
            e.preventDefault();
            var time_call_back = $('#callback').val();
            var phone = $('#phone').val();
            var note = $('#note').val();
            var customer_id = $('#customer_id').val();
            var channel = $('#channel').val();
            var dataForm = {
                time_call_back: time_call_back,
                phone: phone,
                note: note,
                customer_id: customer_id,
                channel: channel
            };
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ route('customer.history.save') }} ',
                type: 'POST',
                data: dataForm,
                success: function (data) {
                    alert(data.message);
                    $('#m_modal_4').modal('hide');
                    $('#child_data_local_' + customer_id).mDatatable('reload');
                }
            });
        });

        $('body').on('click', '.delete_document_id', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var check_confirm = confirm("bạn muốn xóa tài liệu này không?");
            if (check_confirm) {
                var id = $(this).attr('id');
                $.ajax({
                    url: '../document/delete-doc/' + id,
                    type: 'DELETE',
                    dataType: "json",
                    data: {
                        '_token': $('input[name=_token]').val(),
                    },
                    success: function (result) {
                        alert('Xóa tài liệu thành công');
                        datatable.reload();
                    }
                });
            }
        });

        // $('body').on('click', '.delete_customer_id', function (e) {
        //     e.preventDefault();
        //     var check_confirm = confirm("bạn muốn xóa khách hàng này khỏi hệ thống?");
        //     if (check_confirm) {
        //         var id = $(this).attr('id');
        //         $.ajax({
        //             url: 'list/' + id,
        //             type: 'DELETE',
        //             dataType: "json",
        //             data: {
        //                 '_token': $('input[name=_token]').val(),
        //             },
        //             success: function (result) {
        //                 alert(result.message);
        //                 console.log(result);
        //                 index();
        //                 $('#child_data_local').mDatatable('reload');
        //             }
        //         });
        //     }
        // });

        $('body').on('change', '#uploadFile', function (e) {
            e.preventDefault();
            var formData = new FormData();
            var totalFiles = $(this).get(0).files.length;
            var customer_id = $('#customer_id').val();
            var item_id = $(this).data('item-id');
            formData.append("item_id", item_id);
            for (var i = 0; i < totalFiles; i++) {
                var file = $(this).get(0).files[i];
                formData.append("file", file);
            }
            $.ajax({
                type: "POST",
                url: '{{route('subject.upload-file-teacher')}}',
                data: formData,
                dataType: 'json',
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data) {
                        //$("#imgAvatar").attr("src", data.Result);
                        alert("Thêm tài liệu thành công");
                        datatable.reload();
                        //DisplaySuccess('Ảnh đại diện của bạn đã cập nhật xong');
                    } else {
                        DisplayError('Ảnh đại diện của bạn chưa cập nhật được lên hệ thống');
                    }
                }
            });
        });
    });
</script>
