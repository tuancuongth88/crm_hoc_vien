{{ Form::open(array('route' => 'subject.index', 'method' => 'GET')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tên chương trình</label>
        <input type="text" name="search" id="search" class="form-control" placeholder="Tên chương trình"/>
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Tìm kiếm" class="btn btn-primary form-control"/>
    </div>

    <input type="hidden" name="searchJoin" value="and">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>&nbsp;</label>
        <a href="{{route('subject.index')}}" class="btn btn-primary form-control">Hủy tìm kiếm</a>
    </div>
</div>
</form>
