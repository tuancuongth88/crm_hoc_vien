@extends('administrator.app')
@section('title','Danh sách chương trình')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Danh sách chương trình
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
        @include('administrator.educations.subject.search-subject')
        <!--begin::Section-->
            @if (\Auth::user()->can('subject.create'))
                <div class="col-xs-12" style="margin-bottom: 20px">
                    <a href="{{ route('subject.create') }}" class="btn btn-primary m-btn m-btn--icon">
                <span>
                    <i class="fa flaticon-plus"></i>
                    <span>
                        Tạo chương trình
                    </span>
                </span>
                    </a>
                </div>
            @endif
            @include('administrator.errors.info-search')
            <div class="m-section">
                @include('administrator.errors.messages')
                <div class="m-section__content">
                    <table class="table m-table m-table--head-bg-warning">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Lĩnh vực
                            </th>
                            <th>
                                Tên chương trình
                            </th>
                            <th>
                                Trạng thái xuất bản
                            </th>
                            <th>
                                Ngày khai giảng
                            </th>
                            <th>
                                Ngày kết thúc
                            </th>

                            <th>
                                Tùy chọn
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr>
                                <th scope="row">
                                    {{ $key + 1 }}
                                </th>
                                <td>
                                    {{ $value->program->name }}
                                </td>
                                <td>
                                    {{ $value['name'] }}
                                </td>
                                <td>
                                    {{ $value->getStatus() }}
                                </td>
                                <td>
                                    {{ $value['open_time'] }}
                                </td>
                                <td>
                                    {{ $value['close_time'] }}
                                </td>
                                <td>
                                    @if(\Auth::user()->can('subject.user.reg'))
                                        <a href="{{ route('subject.user.reg', $value->id) }}" class="btn btn-primary"
                                           data-original-title="Danh sách học viên"
                                           data-toggle="m-tooltip"
                                        >
                                            <i class="fa fa-user"></i>
                                        </a>
                                    @endif
                                    @if(\Auth::user()->can('subject.view-lesson'))
                                        <a href="{{ route('subject.view-lesson', $value->id) }}"
                                           class="btn btn-accent m-btn m-btn--icon m-btn--icon-only"
                                           data-original-title="Danh sách khóa học"
                                           data-toggle="m-tooltip"
                                        >
                                            <i class="fa fa-leanpub"></i>
                                        </a>
                                    @endif
                                    @if(\Auth::user()->can('subject.update'))
                                        <a href="{{ route('subject.edit', $value->id) }}"
                                           class="btn btn-accent m-btn m-btn--icon m-btn--icon-only"
                                           data-original-title="Sửa trương chình"
                                           data-toggle="m-tooltip"
                                        >
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    @endif
                                    @if(\Auth::user()->can('subject.delete'))
                                        {{ Form::open(array('method'=>'DELETE',
                                         'route' => array('subject.destroy', $value->id),
                                          'style' => 'display: inline-block;')) }}
                                        <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                                class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
                                                data-original-title="Xóa chương trình"
                                                data-toggle="m-tooltip"
                                        >
                                            <i class="fa fa-close"></i>
                                        </button>
                                        {{ Form::close() }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->links() }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
            <!--end::Section-->
        </div>
    </div>
@stop