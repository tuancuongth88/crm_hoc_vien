{{ Form::open(array('route' => 'subject.search.lesson', 'method' => 'GET')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tên Khóa học</label>
        <input type="text" name="search" id="search" class="form-control" placeholder="Tên khóa học"/>
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Search" class="btn btn-primary form-control"/>
    </div>

    <input type="hidden" name="searchJoin" value="and">
</div>
<input type="hidden" name="subject_id" id="subject_id" value="{{ $id }}">
</form>
