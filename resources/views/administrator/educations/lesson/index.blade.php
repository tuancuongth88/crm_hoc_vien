@extends('administrator.app')
@section('title','Thư viện Khóa học')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Danh sách thư viện Khóa học
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
        @include('administrator.educations.lesson.search')
        <!--begin::Section-->
            @if (\Auth::user()->can('lesson.create'))
                <div class="col-xs-12" style="margin-bottom: 20px">
                    <a href="{{ route('lesson.create') }}" class="btn btn-primary m-btn m-btn--icon">
				<span>
					<i class="fa flaticon-plus"></i>
					<span>
						Thêm mới
					</span>
				</span>
                    </a>
                </div>
            @endif
            @include('administrator.errors.info-search')
            <div class="m-section">
                @include('administrator.errors.messages')
                <div class="m-section__content">
                    <table class="table m-table m-table--head-bg-warning">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Khóa học
                            </th>
                            <th>
                                Số buổi
                            </th>
                            <th>
                                Số giờ
                            </th>
                            <th>
                                Tùy chọn
                            </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($data as $key => $value)
                            <tr>
                                <th scope="row">
                                    {{ $key + 1 }}
                                </th>
                                <td>
                                    {{ $value['name'] }}
                                </td>
                                <td>
                                    {{ $value['number_session'] }}
                                </td>
                                <td>
                                    {{ $value['number_hours'] }}
                                </td>
                                <td>
                                    @if (\Auth::user()->can('lesson.update'))
                                        <a href="{{ route('lesson.edit', $value->id) }}"
                                           class="btn btn-accent m-btn m-btn--icon m-btn--icon-only"
                                           data-original-title="Sửa khóa học"
                                           data-toggle="m-tooltip"
                                        >
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                    @endif
                                    @if (\Auth::user()->can('lesson.delete'))
                                        {{ Form::open(array('method'=>'DELETE',
                                         'route' => array('lesson.destroy', $value->id),
                                          'style' => 'display: inline-block;')) }}
                                        <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                                class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
                                                data-original-title="Xóa khóa học"
                                                data-toggle="m-tooltip"
                                        >
                                            <i class="fa fa-close"></i>
                                        </button>
                                        {{ Form::close() }}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $data->links() }}
                Tổng số {{ $data->total() }} bản ghi
            </div>
            <!--end::Section-->
        </div>

    </div>
@stop