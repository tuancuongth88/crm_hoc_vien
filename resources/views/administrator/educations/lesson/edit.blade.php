@extends('administrator.app')
@section('title','Khóa học')

@section('content')
	<link href="{{ URL::asset('plugins/jquery-file-upload/jquery.fileupload.css') }}" rel="stylesheet" type="text/css" />
	<div class="m-grid__item m-grid__item--fluid m-wrapper">
		<!-- BEGIN: Subheader -->
		<div class="m-subheader">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="m-subheader__title m-subheader__title--separator">
						Khóa học
					</h3>
					<ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
						<li class="m-nav__item m-nav__item--home">
							<a href="#" class="m-nav__link m-nav__link--icon">
								<i class="m-nav__link-icon la la-home"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!-- END: Subheader -->
		<div class="m-content">
			<div class="row">
				<div class="col-md-12" style="margin-bottom: 20px">
					<a href="{{ route('lesson.index') }}" class="btn btn-success">Danh sách Khóa học</a>
					<a href="{{ route('lesson.create') }}" class="btn btn-primary">Thêm Khóa học</a>
				</div>
				<div class="col-md-12">
				@include('administrator.errors.errors-validate')
				<!--begin::Portlet-->
					<div class="m-portlet m-portlet--tab">
						<div class="m-portlet__head">
							<div class="m-portlet__head-caption">
								<div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
									<h3 class="m-portlet__head-text">
										Thêm mới Khóa học
									</h3>
								</div>
							</div>
						</div>
						{{ Form::open(array('route' => array('lesson.update', 'id' => $data->id), 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
						<meta name="csrf-token" content="{{ csrf_token() }}">
						<div class="m-portlet__body">
							<div class="form-group m-form__group">
								<label for="name">
									Tên: <span class="required">*</span>
								</label>
								<input type="text" class="form-control m-input" id="name" placeholder="Tên Khóa học" name="name" value="{{ $data->name }}">
							</div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="name">
                                        Loại cha khóa học:
                                    </label>
                                    {!!Form::select('parent', ['' => 'Chọn loại cha'] + $listParent, null, ['class' => 'form-control'])!!}
                                </div>
                                <div class="col-lg-6">

                                </div>
                            </div>
                        </div>
						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
								<div class="col-lg-6">
									<label>
										Số buổi
									</label>
									<div class="input-group m-input-group m-input-group--square">
										<div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon-calendar-3"></i>
                                            </span>
										</div>
										<input type="number" class="form-control m-input" placeholder="Số buổi" name="number_session" value="{{ $data->number_session }}">
									</div>
									<span class="m-form__help">
                                        Nhập số buổi học
                                    </span>
								</div>
								<div class="col-lg-6">
									<label>
										Số giờ học
									</label>
									<div class="input-group m-input-group m-input-group--square">
										<div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="flaticon-stopwatch"></i>
                                            </span>
										</div>
										<input type="number" class="form-control m-input" placeholder="Số giờ" name="number_hours" value="{{ $data->number_hours }}">
									</div>
									<span class="m-form__help">
                                        Nhập số giờ học
                                    </span>
								</div>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="form-group m-form__group">
								<label for="name">
									Mô tả
								</label>
								<textarea class="summernote" name="description" id="description">{{ $data->description }}</textarea>
							</div>
						</div>
						<div class="m-portlet__body">
							<div class="form-group m-form__group">
								<label for="name">
									Upload File
								</label>
								<div  class="m-dropzone dropzone m-dropzone--primary" id="attachment">
									<div class="m-dropzone__msg dz-message needsclick">
										<h3 class="m-dropzone__msg-title">
											Kéo thả file vào đây để upload
										</h3>
										<span class="m-dropzone__msg-desc">
                                            Tối đa 10 File kích thước 5MB/file
                                        </span>
									</div>
								</div >
							</div>
						</div>
						<input type="hidden" name="id_file" id="id_file" >
						<div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions">
								<button class="btn btn-primary">
									Lưu
								</button>
								<a class="btn btn-secondary" href="{{ route('lesson.index') }}">
									Trở về danh sách
								</a>
							</div>
						</div>
						{{ Form::close() }}
					</div>
				</div>
				<!--end::Form-->
			</div>
		</div>
	</div>
	<input type="hidden" name="attach" id="attach" value="{{ json_encode($data->documents->toArray()) }}">
	<script>
        $(document).ready(function() {
            var formEl = $('#lesson_create');
            var listAttachment = JSON.parse($('#attach').val());
            $("#attachment").dropzone({
                addRemoveLinks: true,
                url: '{{ route('lesson.upload-attachment') }}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                paramName: "file",
                dictRemoveFile: "Xóa",
                dictRemoveFileConfirmation: "Bạn có chắc muốn xóa tệp này",
                acceptedFiles: "image/*,application/pdf,.doc,.docx,.xls,.xlsx,.csv,.tsv,.ppt,.pptx,.pages,.odt,.rtf",
                dictInvalidFileType: "Định dạng file không phải dạng cho phép",
                dictFileTooBig: "Dung lượng file quá lớn vượt giới hạn cho phép",
                maxFilesize: 10,
                removedfile: function(file, test) {
                    var fileRemove = findObjectByKey(listAttachment, 'name', file.name);
                    $.ajax({
                        type: 'DELETE',
                        url: '{{ route('lesson.delete-attachment') }}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: fileRemove.id,
                            url: fileRemove.file_url,
                        },
                        sucess: function(data) {

                        }
                    });
                    listAttachment = $.grep(listAttachment, function(e){
                        return e.id != fileRemove.id;
                    });
                    var nameArray = listAttachment.map(function (el) { return el.id; });
                    $('#id_file').val(nameArray);
                    var _ref;
                    return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                },
                init: function() {
                    thisDropzone = this;
                    $.each(listAttachment, function (key, value) {
                        thisDropzone.emit("addedfile", value);
                        thisDropzone.options.thumbnail.call(thisDropzone, value, value.file_url);
                        thisDropzone.emit("complete", value);
                        var nameArray = listAttachment.map(function (el) { return el.id; });
                        $('#id_file').val(nameArray);
                    });
                    this.on("success", function(file, response) {
                        listAttachment.push(response.data);
                        var nameArray = listAttachment.map(function (el) { return el.id; });
                        $('#id_file').val(nameArray);
                        alert(response.message);
                    });
                    // Handle errors
                    this.on('error', function(file, response) {
                        alert('File tải lên không phù hợp');
                    });
                }
            });
            $('.summernote').summernote({
                callbacks: {
                    onImageUpload: function(image) {
                        uploadImageProject(image[0], $(this).attr('id'));
                    }
                },
                height: 150,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['picture']],
                    ['table', ['table']],
                    ['view', ['fullscreen']],
                    ['help', ['help']]
                ]
            });

            function uploadImageProject(image, el) {

                var data = new FormData();
                data.append("image", image);
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '/administrator/upload',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(response) {
                        if (response.status) {
                            var image = $('<img width="100%">').attr('src', response.data);
                            $('#' + el).summernote("insertNode", image[0]);
                        }

                    },
                    error: function(data) {

                    }
                });
            }
        });
	</script>
@stop