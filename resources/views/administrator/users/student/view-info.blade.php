@extends('administrator.app')
@section('title','Thông tin học viên')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dự án
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px">
                    <!--Begin::Main Portlet-->
                    <div class="m-portlet">
                        <!--begin: Portlet Head-->
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Cập nhật thông tin người dùng
                                        <small>
                                            Bạn thêm thông tin người dùng theo form dưới đây
                                        </small>
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <a href="#" data-toggle="m-tooltip"
                                           class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left"
                                           data-width="auto" title="Get help with filling up this form">
                                            <i class="flaticon-info m--icon-font-size-lg3"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end: Portlet Head-->
                        <!--begin: Form Wizard-->
                        <div class="m-wizard m-wizard--1 m-wizard--success" id="block-project">
                            <!--begin: Message container -->
                            <div class="m-portlet__padding-x">
                                <!-- Here you can put a message or alert -->
                                @include('administrator.errors.messages')
                                @include('administrator.errors.errors-validate')
                            </div>
                            <!--end: Message container -->
                            <!--begin: Form Wizard Head -->
                            <div class="m-wizard__head m-portlet__padding-x">
                                <!--begin: Form Wizard Progress -->
                                <div class="m-wizard__progress">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="100"
                                             aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Progress -->
                                <!--begin: Form Wizard Nav -->
                                <div class="m-wizard__nav">
                                    <div class="m-wizard__steps">
                                        <div class="m-wizard__step m-wizard__step--current"
                                             data-wizard-target="#m_wizard_form_step_1">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        1
                                                    </span>
                                                </span>
                                                </a>
                                                <div class="m-wizard__step-label">
                                                    Thông tin cơ bản
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_2">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        2
                                                    </span>
                                                </span>
                                                </a>
                                                <div class="m-wizard__step-label">
                                                    Trình độ học vấn
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_3">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        3
                                                    </span>
                                                </span>
                                                </a>
                                                <div class="m-wizard__step-label">
                                                    Kinh nghiệm làm việc
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_4">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        4
                                                    </span>
                                                </span>
                                                </a>
                                                <div class="m-wizard__step-label">
                                                    Học viện
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Nav -->
                            </div>
                            <!--end: Form Wizard Head -->
                            <!--begin: Form Wizard Form-->
                            <div class="m-wizard__form">
                                <form class="m-form m-form--label-align-left- m-form--state-"
                                      id="education-user-info-form"
                                      enctype="multipart/form-data">
                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                    <input type="hidden" name="user_info_id"
                                           value="{{(isset($userInfo[0]) && isset($userInfo[0]->EducationUserInfo->id))
                                                         ? $userInfo[0]->EducationUserInfo->id:0
                                                  }}"
                                    />
                                    <!--begin: Form Body -->
                                    <div class="m-portlet__body">
                                        <!--begin: Form Wizard Step 1-->
                                        <div class="m-wizard__form-step m-wizard__form-step--current m-option"
                                             id="m_wizard_form_step_1">
                                            <fieldset class="border p-2">
                                                <legend class="w-auto">Thông tin cơ bản</legend>
                                                <div class="row form-group">
                                                    <div class="col-lg-3 align-self-center text-center">
                                                        <img class="profilecard__img wf-80 wf-md-126 mr-md-5 mr-3"
                                                             style="cursor: pointer; width: 200px;"
                                                             src="{{isset($userInfo[0])? $userInfo[0]->avatar :'' }}"
                                                             alt="avatar">
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Họ tên:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{isset($userInfo[0])? $userInfo[0]->fullname :'' }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Email:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{isset($userInfo[0]) ? $userInfo[0]->email:''}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Số điện thoại:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{isset($userInfo[0]) ? $userInfo[0]->phone:''}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Số CMND:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{isset($userInfo[0]) ? $userInfo[0]->identity:''}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Nơi cấp:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{
                                                                   (isset($userInfo[0]) && isset($userInfo[0]->EducationUserInfo->identity_place_release))
                                                                   ? $userInfo[0]->EducationUserInfo->identity_place_release:''
                                                                   }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Facebook:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{
                                                                      isset($userInfo[0]) ? $userInfo[0]->facebook:''
                                                                       }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Viber

                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{(isset($userInfo[0]) && isset($userInfo[0]->EducationUserInfo->viber))
                                                                    ? $userInfo[0]->EducationUserInfo->viber:''}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Mục tiêu

                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{
                                                                   (isset($userInfo[0]) && isset($userInfo[0]->EducationUserInfo->target))
                                                                   ? $userInfo[0]->EducationUserInfo->target:''
                                                                   }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Ưu điểm

                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{
                                                                   (isset($userInfo[0]) && isset($userInfo[0]->EducationUserInfo->advantage))
                                                                   ? $userInfo[0]->EducationUserInfo->advantage:''
                                                                   }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Giới tính:
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-5 align-self-center">
                                                                <div class="m-form__group form-group">
                                                                    <label class="col-form-label">
                                                                        {{
                                                                        isset(\App\Models\Users\User::$listGender[$userInfo[0]->gender]) ?
                                                                        \App\Models\Users\User::$listGender[$userInfo[0]->gender]:''
                                                                       }}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Ngày sinh:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{isset($userInfo[0]) ? convertDate('d/m/Y',$userInfo[0]->birthday):''}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Địa chỉ:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{isset($userInfo[0]) ? $userInfo[0]->address:''}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Ngày cấp:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{
                                                                   (isset($userInfo[0]) &&  isset($userInfo[0]->EducationUserInfo->identity_date))
                                                                   ? convertDate('d/m/Y',$userInfo[0]->EducationUserInfo->identity_date):''
                                                                   }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Skype:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{
                                                                   (isset($userInfo[0]) && isset($userInfo[0]->EducationUserInfo->skype))
                                                                   ? $userInfo[0]->EducationUserInfo->skype:''
                                                                   }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Zalo:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{
                                                                   (isset($userInfo[0]) && isset($userInfo[0]->EducationUserInfo->zalo))
                                                                   ? $userInfo[0]->EducationUserInfo->zalo:''
                                                                   }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    TT hôn nhân:
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-5 align-self-center">
                                                                <div class="m-form__group form-group">
                                                                    <label class="col-form-label">
                                                                        {{isset($userInfo[0]->EducationUserInfo->marriage) ?
                                                                        \App\Models\Users\EducationUserInfo::$listMarriage[$userInfo[0]->EducationUserInfo->marriage]:''
                                                                        }}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Sở thích:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{
                                                                   (isset($userInfo[0]) &&  isset($userInfo[0]->EducationUserInfo->hobby))
                                                                   ? $userInfo[0]->EducationUserInfo->hobby:''
                                                                   }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row form-group">
                                                            <div class="col-lg-3">
                                                                <label class="col-form-label font-weight-normal">
                                                                    Nhược điểm:
                                                                </label>
                                                            </div>
                                                            <div class="col">
                                                                <label class="col-form-label">
                                                                    {{
                                                                    (isset($userInfo[0]) &&  isset($userInfo[0]->EducationUserInfo->defect))
                                                                    ? $userInfo[0]->EducationUserInfo->defect:''
                                                                    }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </div>
                                        <!--end: Form Wizard Step 1-->
                                        <!--begin: Form Wizard Step 2-->
                                        <div class="m-wizard__form-step  m_repeater_7"
                                             id="m_wizard_form_step_2">
                                            <div class="" data-repeater-list="literacy_param">
                                                @if(isset($userInfo_literacy_param) && $userInfo_literacy_param)
                                                    @foreach($userInfo_literacy_param as $key=>$value)
                                                        <div class="row form-group border p-3" data-repeater-item>
                                                            <div class="col-md-4 align-self-center">
                                                                <div class="row form-group">
                                                                    <div class="col-md-3">
                                                                        <label class="col-form-label">
                                                                            {{$value->from}}
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-2 col-lg-2">
                                                                        <div>Đến</div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <label class="col-form-label">
                                                                            {{$value->to}}
                                                                        </label>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-lg-6">
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label font-weight-normal">
                                                                            Tên trường
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <label class="col-form-label">
                                                                            {{$value->school_name}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label font-weight-normal">
                                                                            Địa chỉ
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <label class="col-form-label">
                                                                            {{$value->school_address}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label font-weight-normal">
                                                                            Loại tốt nghiệp
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <label class="col-form-label">
                                                                            {{\App\Models\Users\EducationUserInfo::$listLiteracy[$value->str_literacy]}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <div class="row form-group border p-3" data-repeater-item>
                                                        Chưa cập nhật thông tin học viên
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 2-->
                                        <!--begin: Form Wizard Step 3-->
                                        <div class="m-wizard__form-step m_repeater_8" id="m_wizard_form_step_3">
                                            <div class="" data-repeater-list="experienced_param">
                                                @if(isset($userInfo_experienced_param) && $userInfo_experienced_param)
                                                    @foreach($userInfo_experienced_param as $key=>$value)
                                                        <div class="row form-group border p-3" data-repeater-item>
                                                            <div class="col-md-4 align-self-center">
                                                                <div class="row form-group">
                                                                    <div class="col-md-3">
                                                                        <label class="col-form-label">
                                                                            {{$value->from}}
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-2 col-lg-2">
                                                                        <div>Đến</div>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <label class="col-form-label">
                                                                            {{$value->to}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-lg-6">
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label font-weight-normal">
                                                                            Tên công ty
                                                                            <br/>
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <label class="col-form-label">
                                                                            {{$value->company_name}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label font-weight-normal">
                                                                            Chức vụ
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <label class="col-form-label">
                                                                            {{$value->position}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label font-weight-normal">
                                                                            Mức lương
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <label class="col-form-label">
                                                                            {{$value->wage}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label font-weight-normal">
                                                                            Địa chỉ
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <label class="col-form-label">
                                                                            {{$value->company_address}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label
                                                                                class="col-form-label font-weight-normal">
                                                                            Mô tả
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <label class="col-form-label">
                                                                            {{$value->description}}
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <div class="row form-group border p-3" data-repeater-item>
                                                        Thông tin chưa được cập nhật
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 3-->
                                        <!--begin: Form Wizard Step 4-->
                                        <div class="m-wizard__form-step" id="m_wizard_form_step_4">
                                            <table class="table table-bordered">
                                                <thead>
                                                <tr class="d-flex thead-dark">
                                                    <th class="col-1 text-center" scope="col">STT</th>
                                                    <th class="col-sm-7 text-center" scope="col">Chương trình</th>
                                                    <th class="col-sm-3 text-center" scope="col">Đánh giá</th>
                                                    <th class="col-sm-1 text-center" scope="col">#</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @if($user_reg_program->count() > 0)
                                                    @foreach($user_reg_program as $key=>$value)
                                                        <tr class="d-flex">
                                                            <td class="col-1 text-center" scope="row">
                                                                {{$key+1}}
                                                            </td>
                                                            <td class="col-sm-7 text-center">
                                                                {{isset($value->subject->name) ? $value->subject->name:''}}
                                                            </td>
                                                            <td class="col-sm-3 text-center">
                                                                {{isset($value->pass)
                                                                ? (($value->pass == 1) ? 'Đạt':'Không đạt'):''}}
                                                            </td>
                                                            <td class="col-sm-1 text-center">
                                                                <button type="button"
                                                                        class="btn btn-primary btn-view-detail-lesson"
                                                                        data-toggle="modal"
                                                                        data-subject-id="{{$value->subject->id}}"
                                                                        data-target="#modalDetailLesson">
                                                                    View
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        <!--end: Form Wizard Step 4-->
                                    </div>
                                    <!--end: Form Body -->
                                    <!--begin: Form Actions -->
                                    <!--end: Form Actions -->
                                </form>
                            </div>
                            <!--end: Form Wizard Form-->
                        </div>
                        <!--end: Form Wizard-->
                    </div>
                    <!--End::Main Portlet-->
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="modalDetailLesson" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Các môn học</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="d-flex">
                            <th class="col-1 text-center" scope="col">STT</th>
                            <th class="col-sm-7 text-center" scope="col">Tên môn học</th>
                            <th class="col-sm-4 text-center" scope="col">Thời gian</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            var wizardEl = $('#block-project');
            var wizard;
            wizard = wizardEl.mWizard({
                startStep: 1
            });

            //== Validation before going to next page
            wizard.on('beforeNext', function (wizard) {

            })

            //== Change event
            wizard.on('change', function (wizard) {
                mApp.scrollTop();
            });


            $('.btn-view-detail-lesson').on('click', function () {
                var subject_id = $(this).data('subject-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '/administrator/education/lesson/ajaxget-alllesson/' + subject_id,
                    cache: false,
                    type: "post",
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $('#modalDetailLesson .modal-body table tbody').empty();
                        $('#modalDetailLesson .modal-body table tbody').append(response.data)
                    },
                    error: function (data) {

                    }
                });

            })

        });

    </script>
@stop
