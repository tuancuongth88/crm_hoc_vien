@extends('administrator.app')
@section('title','Danh sách user')

@section('content')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <!--begin::Section-->
            @include('administrator.errors.info-search')
            <div class="m-section">
                @include('administrator.errors.messages')
                <div class="m-section__content">
                    <div class="m-portlet m-portlet--mobile">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Người dùng
                                        <small>
                                            danh sách user
                                        </small>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <!--begin: Search Form -->
                            <div class="m-form m-form--label-align-right ">
                                <div class="row align-items-center">
                                    <div class="col-xl-8 order-2 order-xl-1">
                                        {{ Form::open(array('enctype' => 'multipart/form-data',
                                         'method'=>'GET',
                                         'route' => 'user.student',
                                          'id' => 'search-form')) }}
                                        <div class="form-group m-form__group row align-items-center">
                                            <div class="col-md-4">
                                                <div class="m-input-icon m-input-icon--left">
                                                    <input type="text" class="form-control m-input"
                                                           placeholder="Tên hoặc email"
                                                           id="email"
                                                           name="txt_search"
                                                           value="{{$txt_search}}"
                                                    >
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <input type="submit" value="Tìm kiếm"
                                                       class="btn btn-primary form-control">
                                            </div>
                                            <div class="col-md-3">
                                                <a href="{{route('user.student')}}"
                                                   class="btn btn-primary form-control">Hủy tìm kiếm</a>
                                            </div>
                                        </div>
                                        {{ Form::close() }}<br>
                                    </div>
                                    <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                        <div class="m-separator m-separator--dashed d-xl-none"></div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Search Form -->
                            <!--begin: Datatable -->
                            <div class="m_datatable" id="ajax_data">
                                <table class="table m-table m-table--head-bg-warning">
                                    <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Tên
                                        </th>
                                        <th>
                                            Điện thoại
                                        </th>
                                        <th>
                                            Tùy chọn
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $key => $value)
                                        <tr>
                                            <th scope="row">
                                                {{ $key + 1 }}
                                            </th>
                                            <td>
                                                {{ $value['email'] }}
                                            </td>
                                            <td>
                                                {{ $value['fullname'] }}
                                            </td>
                                            <td>
                                                {{ $value['phone'] }}
                                            </td>
                                            <td>
                                                <a href="{{ route('user.student.view', $value->id) }}"
                                                   class="btn btn-accent m-btn m-btn--icon m-btn--icon-only"
                                                   data-original-title="Xem học viên"
                                                   data-toggle="m-tooltip"
                                                >
                                                    <i class="flaticon-visible"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {{ $data->links() }}
                            Tổng số {{ $data->total() }} bản ghi
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop