@extends('administrator.app')
@section('title','Danh sách user')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    {{-- <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Danh sách tài khoản
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div> --}}
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        <div class="col-xs-12" style="margin-bottom: 20px">
            {{ Form::open(array('enctype' => 'multipart/form-data', 'method'=>'POST', 'route' => 'user.import', 'id' => 'import-form')) }}
                <a href="{{ route('user.create') }}" class="btn btn-primary m-btn m-btn--icon">
                    <span>
                        <i class="fa flaticon-plus"></i>
                        <span>
                            Thêm mới tài khoản
                        </span>
                    </span>
                </a>
                <div class="custom-file col-md-3 m--padding-top-5">
                    <input type="file" class="custom-file-input" id="customFile" name="import_file">
                    <label class="custom-file-label" for="customFile">
                        Chọn file import
                    </label>
                </div>
            <button type="submit" class="btn btn-success" id="import_file"><i class="fa fa-file-excel-o"></i> Import</button>
            {{ Form::close() }}<br>
            <a href="{{ url('/user.xlsx') }}" style="font-weight: bold; font-size: 17px; color: #000;">Mẫu File Import</a>
        </div>
        @if(session('listUserFail'))
            <div class="m-alert m-alert--outline alert alert-warning alert-dismissible fade show" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                @foreach (session('listUserFail') as $element)
                    Email {{ $element }} đã có trong cơ sở dữ liệu<br>
                @endforeach
            </div>
        @endif
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                {{-- <table class="table m-table m-table--head-bg-warning">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Tên
                            </th>
                            <th>
                                Điện thoại
                            </th>
                            <th>
                                Tùy chọn
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <th scope="row">
                                {{ $key + 1 }}
                            </th>
                            <td>
                                {{ $value['email'] }}
                            </td>
                            <td>
                                {{ $value['fullname'] }}
                            </td>
                            <td>
                                {{ $value['phone'] }}
                            </td>
                            <td>
                                <a href="{{ route('user.edit', $value->id) }}" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                {{ Form::open(array('method'=>'DELETE', 'route' => array('user.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                    <i class="fa fa-close"></i>
                                </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table> --}}
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Người dùng
                                    <small>
                                        danh sách user
                                    </small>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-3">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input" placeholder="Tên ..." id="email">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span>
                                                        <i class="la la-search"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::select('position_id', [0 => 'Chọn chức vụ'] + $listPosition, null, ['class' => 'form-control m-select2', 'id' => 'position_id']) }}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::select('department_id', [0 => 'Chọn phòng ban'] + $listDepartment, null, ['class' => 'form-control m-select2', 'id' => 'department_id']) }}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::select('branch', [0 => 'Chọn chi nhánh'] + $listBranch, null, ['class' => 'form-control m-select2', 'id' => 'branch']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <a href="{{ route('user.create') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                        <span>
                                            <i class="la la-plus-circle"></i>
                                            <span>
                                                Thêm mới
                                            </span>
                                        </span>
                                    </a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->
<!--begin: Datatable -->
                        <div class="m_datatable" id="ajax_data"></div>
                        <!--end: Datatable -->
                    </div>
                </div>
            </div>
            {{-- {{ $data->links() }}
            Tổng số {{ $data->total() }} bản ghi --}}
        </div>

        <!--end::Section-->
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
    $(document).ready(function (argument) {
        // $('#import-form').submit(function() {
        //     var data = $("#login_form :input").serializeArray();
        //     alert('Handler for .submit() called.');
        // });
        $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/administrator/system/user/list',
                        method: 'GET',
                    },
                },

                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // column sorting
            sortable: false,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#email'),
            },

            rows: {
                // auto hide columns, if rows overflow
                autoHide: true,
            },

            // columns definition
            columns: [
            {
                field: 'avatar',
                title: 'Ảnh',
                template: function(row) {
                    return '<img width="25" src="' + row.avatar + '">';
                },
            },
            {
                field: 'fullname',
                title: 'Tên',
            }, {
                field: 'email',
                title: 'Email',
            }, {
                field: 'phone',
                title: 'Điện thoại',
            }, {
                field: 'position',
                title: 'Chức vụ',
                template: function(row) {
                    if (row.position) {
                        return row.position.name;
                    }
                    return '';
                },
            }, {
                field: 'birthday',
                title: 'Ngày sinh',
            }, {
                field: 'Tùy chọn',
                title: 'Tùy chọn',
                sortable: false,
                overflow: 'visible',
                template: function (row, index, datatable) {
                    var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                    return '<a href="/administrator/system/user/' + row.id + '/edit" class="m-portlet__nav-link btn   m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Sửa">\
                                  <i class="la la-edit"></i>\
                            </a>\
                            <button data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger   m-btn--icon m-btn--icon-only m-btn--pill delete-user" title="Xóa">\
                                  <i class="la la-trash"></i>\
                            </button>\
                            ';
                },
            }, {
                field: 'department',
                title: 'Phòng ban',
                template: function(row) {
                    if (row.department) {
                        return row.department.name;
                    }
                    return '';
                },
            }, {
                field: 'branch',
                title: 'Chi nhánh',
                template: function(row) {
                    if (row.branch) {
                        return row.branch.name;
                    }
                    return '';
                },
            }, {
                field: 'company',
                title: 'Công ty',
                template: function(row) {
                    if (row.company) {
                        return row.company.name;
                    }
                    return '';
                },
            }, {
                field: 'address',
                title: 'Địa chỉ',
            },
            {
                field: 'created_at',
                title: 'Ngày tạo',
            }],
        });
        $('body').on('click', '.delete-user', function() {
            var id = $(this).attr('data-id');
            swal({
                title: 'Bạn có chắc?',
                text: "muốn xóa người dùng này!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có!'
            }).then(function(result) {
                if (result.value) {
                    if (id > 0) {
                        mApp.block($('#ajax_data'));
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: '/administrator/system/user/' + id,
                            type: 'POST',
                            contentType: 'application/json',
                            success: function(response) {
                                mApp.unblock($('#ajax_data'));
                                swal({
                                    "title": "Thông báo",
                                    "text": response.message,
                                    "type": "success",
                                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                });
                                if (response.status) {
                                    $('.m_datatable').mDatatable('reload');
                                }
                            },
                            error: function(data) {
                                console.log(data);
                            }
                        });
                    }
                }
            });

        });
        $('.m-select2').select2();
        $('#position_id').on('change', function() {
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'position_id');
        });
        $('#department_id').on('change', function() {
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'department_id');
        });
        $('#branch').on('change', function() {
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'branch');
        });
    });

</script>
@stop