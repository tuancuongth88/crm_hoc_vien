@extends('administrator.app')
@section('title','Danh sách user')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Thông tin cá nhân
                </h3>
                {{-- <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul> --}}
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <div class="m-portlet m-portlet--full-height  ">
                    <div class="m-portlet__body">
                        <div class="m-card-profile">
                            <div class="m-card-profile__title m--hide">
                                Your Profile
                            </div>
                            <div class="m-card-profile__pic">
                                <div class="m-card-profile__pic-wrapper">
                                    <img src="{{ $user->avatar }}" alt=""/>
                                </div>
                            </div>
                            <div class="m-card-profile__details">
                                <span class="m-card-profile__name">
                                    {{ $user->fullname }}
                                </span>
                                <a href="" class="m-card-profile__email m-link">
                                    {{ $user->phone }}
                                </a>
                            </div>
                        </div>
                        <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                            <li class="m-nav__separator m-nav__separator--fit"></li>
                            <li class="m-nav__section m--hide">
                                <span class="m-nav__section-text">
                                    Section
                                </span>
                            </li>
                            <li class="m-nav__item">
                                <a href="{{ route('user.edit', \Auth::user()->id) }}" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                    <span class="m-nav__link-title">
                                        <span class="m-nav__link-wrap">
                                            <span class="m-nav__link-text">
                                                Thông tin cá nhân
                                            </span>
                                            {{-- <span class="m-nav__link-badge">
                                                <span class="m-badge m-badge--success">
                                                    2
                                                </span>
                                            </span> --}}
                                        </span>
                                    </span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="{{ route('user.get-change-password') }}" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-share"></i>
                                    <span class="m-nav__link-text">
                                        Thay đổi mật khẩu
                                    </span>
                                </a>
                            </li>
                            {{--
                            <li class="m-nav__item">
                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-chat-1"></i>
                                    <span class="m-nav__link-text">
                                        Messages
                                    </span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-graphic-2"></i>
                                    <span class="m-nav__link-text">
                                        Sales
                                    </span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-time-3"></i>
                                    <span class="m-nav__link-text">
                                        Events
                                    </span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="../header/profile&amp;demo=default.html" class="m-nav__link">
                                    <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                    <span class="m-nav__link-text">
                                        Support
                                    </span>
                                </a>
                            </li> --}}
                        </ul>
                        <div class="m-portlet__body-separator"></div>
                        {{-- <div class="m-widget1 m-widget1--paddingless">
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            Member Profit
                                        </h3>
                                        <span class="m-widget1__desc">
                                            Awerage Weekly Profit
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-brand">
                                            +$17,800
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            Orders
                                        </h3>
                                        <span class="m-widget1__desc">
                                            Weekly Customer Orders
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-danger">
                                            +1,800
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            Issue Reports
                                        </h3>
                                        <span class="m-widget1__desc">
                                            System bugs and issues
                                        </span>
                                    </div>
                                    <div class="col m--align-right">
                                        <span class="m-widget1__number m--font-success">
                                            -27,49%
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                                <li class="nav-item m-tabs__item">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                        <i class="flaticon-share m--hide"></i>
                                        Cập nhật thông tin tài khoản
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @include('administrator.errors.errors-validate')
                    <div class="tab-content">
                        <div class="tab-pane active" id="m_user_profile_tab_1">
                            {{ Form::open(array('route' => array('user.update', $user->id), 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
                                <div class="m-portlet__body">

                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">
                                                1. Thông tin cơ bản
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Họ tên
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" name="fullname" type="text" value="{{ $user->fullname }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Email
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="email" value="{{ $user->email }}"" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Ngày sinh
                                        </label>
                                        <div class="col-7">
                                            <input type="text" class="form-control" id="birthday" placeholder="Chọn ngày" name="birthday" value="{{ $user->birthday }}"">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            CMT
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" type="text" value="{{ $user->identity }}" name="identity">
                                            <span class="m-form__help">
                                                Số chứng minh nhân dân hoặc hộ chiếu
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Điện thoại
                                        </label>
                                        <div class="col-7">
                                            <input name="phone" class="form-control m-input" type="text" value="{{ $user->phone }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Giới tính
                                        </label>
                                        <div class="col-7">
                                            <select class="form-control m-input" id="gender" name="gender">
                                                <option value="1" {{ ($user['gender'] == 1) ? 'selected' : ''}}>
                                                    Nam
                                                </option>
                                                <option value="2" {{ ($user['gender'] == 2) ? 'selected' : ''}}>
                                                    Nữ
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">
                                                2. Công tác
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Công ty
                                        </label>
                                        <div class="col-7">
                                            <select class="form-control m-input" id="company_id" name="company_id">
                                                @foreach ($data['companies'] as $element)
                                                    <option value="{{ $element['id'] }}" {{ ($user['company_id'] == $element['id']) ? 'selected' : ''}}>
                                                        {{ $element['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Chi nhánh
                                        </label>
                                        <div class="col-7">
                                            <select class="form-control m-input" id="branch_id" name="branch_id">
                                                @foreach ($data['branchs'] as $element)
                                                    <option value="{{ $element['id'] }}" {{ ($user['branch_id'] == $element['id']) ? 'selected' : ''}}>
                                                        {{ $element['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Phòng ban
                                        </label>
                                        <div class="col-7">
                                            <select class="form-control m-input" name="department_id">
                                                <option value="0">--Lựa chọn--</option>
                                                @foreach ($data['departments'] as $element)
                                                    <option value="{{ $element['id'] }}" {{ ($user['department_id'] == $element['id']) ? 'selected' : ''}}>
                                                        {{ $element['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Người quản lý
                                        </label>
                                        <div class="col-7">
                                            {{ Form::select('parent_id', [0 => '--- Lựa chọn---'] + $listUserByDepartment->pluck('email', 'id')->toArray(0), $user->parent_id, ['class' => 'form-control m-input m-select2', 'id' => 'parent_id']) }}

                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Chức vụ
                                        </label>
                                        <div class="col-7">
                                            <select class="form-control m-input" id="position" name="position_id">
                                                @foreach ($data['positions'] as $element)
                                                    <option value="{{ $element['id'] }}" {{ ($user['position_id'] == $element['id']) ? 'selected' : ''}}>
                                                        {{ $element['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">
                                                3. Mạng xã hội
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Website
                                        </label>
                                        <div class="col-7">
                                            <input type='text' class="form-control input-website" type="text" name="website" />
                                            <span class="m-form__help">
                                                Website cá nhân:
                                                <code>
                                                http://...
                                            </code>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Facebook
                                        </label>
                                        <div class="col-7">
                                            <input type='text' class="form-control input-website" type="text" name="facebook" />
                                            <span class="m-form__help">
                                                Facebook cá nhân:
                                                <code>
                                                http://...
                                            </code>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Twitter
                                        </label>
                                        <div class="col-7">
                                            <input type='text' class="form-control input-website" type="text" name="twitter" />
                                            <span class="m-form__help">
                                                Twitter cá nhân:
                                                <code>
                                                http://...
                                            </code>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Instagram
                                        </label>
                                        <div class="col-7">
                                            <input type='text' class="form-control input-website" type="text" name="instagram" />
                                            <span class="m-form__help">
                                                Instagram cá nhân:
                                                <code>
                                                http://...
                                            </code>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Google+
                                        </label>
                                        <div class="col-7">
                                            <input type='text' class="form-control input-website" type="text" name="google" />
                                            <span class="m-form__help">
                                                Google+ cá nhân:
                                                <code>
                                                http://...
                                            </code>
                                        </div>
                                    </div>
                                    {{-- <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Loại tài khoản
                                        </label>
                                        <div class="col-7">
                                            <select class="form-control m-input" id="type" name="type">
                                                @foreach ($data['type'] as $element)
                                                    <option value="{{ $element['id'] }}">
                                                        {{ $element['name'] }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}
                                    <div class="m-form__group form-group row">
                                        <label class="col-2 col-form-label">
                                            Kích hoạt
                                        </label>
                                        <div class="col-7">
                                            <span class="m-switch m-switch--icon m-switch--success">
                                                <label>
                                                    <input type="checkbox" {{ ($user['active'] == 1) ? 'checked' : ''}} name="active" value="1">
                                                    <span></span>
                                                </label>
                                            </span>
                                        </div>
                                    </div>
                                    @if (\Auth::user()->can('is-admin'))
                                        <div class="m-form__group form-group row">
                                            <label class="col-2 col-form-label">
                                                Trạng thái nhân viên
                                            </label>
                                            <div class="col-7">
                                            <span class="m-switch m-switch--icon m-switch--success">
                                                {{ Form::select('type', $listType, $user['type'], ['class' => 'form-control']) }}
                                            </span>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                    Lưu
                                                </button>
                                                &nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Hủy
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {{ Form::close() }}
                        </div>
                        <div class="tab-pane " id="m_user_profile_tab_2"></div>
                        <div class="tab-pane " id="m_user_profile_tab_3"></div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#birthday').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
    });
</script>
@include('administrator.users.script')
@stop
