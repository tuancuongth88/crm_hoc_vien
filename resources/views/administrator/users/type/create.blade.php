@extends('administrator.app')
@section('title','loại người dùng')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<div class="m-content">
		<div class="row">
			<div class="col-md-12" style="margin-bottom: 20px">
				<a href="{{ route('user-type.index') }}" class="btn btn-primary m-btn m-btn--icon">
					<span>
						<i class="fa flaticon-list-3"></i>
						<span>
							Danh sách loại người dùng
						</span>
					</span>
				</a>
			</div>
	        <div class="col-md-12">
				@include('administrator.errors.errors-validate')
	            <!--begin::Portlet-->
	            <div class="m-portlet m-portlet--tab">
	                <div class="m-portlet__head">
	                    <div class="m-portlet__head-caption">
	                        <div class="m-portlet__head-title">
	                            <span class="m-portlet__head-icon m--hide">
	                                <i class="la la-gear"></i>
	                            </span>
	                            <h3 class="m-portlet__head-text">
	                                Thêm mới loại người dùng
	                            </h3>
	                        </div>
	                    </div>
	                </div>
	                {{ Form::open(array('route' => 'user-type.store', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-8">
                                    <label>
                                        Tên loại người dùng:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="Nhập tên" name="name">
                                    </div>
                                    <span class="m-form__help">
                                        Tên hiển thị loại người dùng
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('role.index') }}">
                                    <i class="la la-arrow-circle-o-left"></i>
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                    {{-- </form> --}}
                    {{ Form::close() }}
	            </div>
	            <!--end::Portlet-->
	        </div>
	        <!--end::Form-->
	    </div>
	</div>
</div>
@stop