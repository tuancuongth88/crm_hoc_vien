<script>
    $(document).ready(function() {
        $('.m-select2').select2();
        $('#department_id').change(function () {
            $('#parent_id').empty();
            var department = $('#department_id').val();
            $.ajax({
                url : '/administrator/system/user/user-department/'+ department,
                type: 'GET',
                success: function (data) {
                    $('#parent_id').append($('<option>',
                        {
                            value: 0,
                            text :'-- Lựa chọn --'
                        }))
                    $.each(data.data, function (key, data) {
                        $('#parent_id').append($('<option>',
                            {
                                value: data.id,
                                text : data.email
                            }))
                    })
                }
            });
        });
    });
</script>