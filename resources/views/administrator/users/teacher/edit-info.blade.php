@extends('administrator.app')
@section('title','Sửa thông tin user')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Thông tin giảng viên
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px">
                    <!--Begin::Main Portlet-->
                    <div class="m-portlet">
                        <!--begin: Portlet Head-->
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Cập nhật thông tin người dùng
                                        <small>
                                            Bạn thêm thông tin người dùng theo form dưới đây
                                        </small>
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <a href="#" data-toggle="m-tooltip"
                                           class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left"
                                           data-width="auto" title="Get help with filling up this form">
                                            <i class="flaticon-info m--icon-font-size-lg3"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!--end: Portlet Head-->
                        <!--begin: Form Wizard-->
                        <div class="m-wizard m-wizard--1 m-wizard--success" id="block-project">
                            <!--begin: Message container -->
                            <div class="m-portlet__padding-x">
                                <!-- Here you can put a message or alert -->
                                @include('administrator.errors.messages')
                                @include('administrator.errors.errors-validate')
                            </div>
                            <!--end: Message container -->
                            <!--begin: Form Wizard Head -->
                            <div class="m-wizard__head m-portlet__padding-x">
                                <!--begin: Form Wizard Progress -->
                                <div class="m-wizard__progress">
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" aria-valuenow="100"
                                             aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Progress -->
                                <!--begin: Form Wizard Nav -->
                                <div class="m-wizard__nav">
                                    <div class="m-wizard__steps">
                                        <div class="m-wizard__step m-wizard__step--current"
                                             data-wizard-target="#m_wizard_form_step_1">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        1
                                                    </span>
                                                </span>
                                                </a>
                                                <div class="m-wizard__step-label">
                                                    Thông tin cơ bản
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_2">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        2
                                                    </span>
                                                </span>
                                                </a>
                                                <div class="m-wizard__step-label">
                                                    Trình độ học vấn
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_3">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        3
                                                    </span>
                                                </span>
                                                </a>
                                                <div class="m-wizard__step-label">
                                                    Kinh nghiệm làm việc
                                                </div>
                                            </div>
                                        </div>
                                        <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_4">
                                            <div class="m-wizard__step-info">
                                                <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        4
                                                    </span>
                                                </span>
                                                </a>
                                                <div class="m-wizard__step-label">
                                                    Kinh nghiệm đào tạo
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Wizard Nav -->
                            </div>
                            <!--end: Form Wizard Head-->
                            <!--begin: Form Wizard Form-->
                            <div class="m-wizard__form">
                                <form class="m-form m-form--label-align-left- m-form--state-" id="education-user-info-form" enctype="multipart/form-data">
                                    <meta name="csrf-token" content="{{ csrf_token() }}">
                                    <input type="hidden" name="user_info_id"
                                           value="{{(isset($teacherInfo[0]) && isset($teacherInfo[0]->EducationUserInfo->id))
                                                         ? $teacherInfo[0]->EducationUserInfo->id:0 }}"
                                    />
                                    <!--begin: Form Body -->
                                    <div class="m-portlet__body">
                                        <!--begin: Form Wizard Step 1-->
                                        <div class="m-wizard__form-step m-wizard__form-step--current m-option" id="m_wizard_form_step_1">
                                            <div class="row form-group">
                                                <div class="col-lg-3 align-self-center text-center">
                                                    <img class="profilecard__img wf-80 wf-md-126 mr-md-5 mr-3"
                                                         style="cursor: pointer; width: 200px;"
                                                         src="{{isset($teacherInfo[0])? $teacherInfo[0]->avatar :'' }}"
                                                         alt="avatar" id="imgAvatar"
                                                         onclick="CallUploadFile()"
                                                         title="Đổi ảnh đại diện khác">
                                                    <input type="file" name="uploadAvatar" id="uploadAvatar" multiple=""
                                                           style="display: none;"
                                                           accept="image/*">
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label ">
                                                                Họ tên
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="fullname"
                                                                   placeholder="Họ và tên" class="form-control m-input"
                                                                   value="{{isset($teacherInfo[0])? $teacherInfo[0]->fullname :'' }}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Email
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{isset($teacherInfo[0]) ? $teacherInfo[0]->email:'' }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Số điện thoại
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="phone" class="form-control m-input"
                                                                   placeholder="Số điện thoại"
                                                                   value="{{isset($teacherInfo[0]) ? $teacherInfo[0]->phone:''}}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Số CMND
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="identity"
                                                                   class="form-control m-input"
                                                                   placeholder="Số chứng minh thư"
                                                                   value="{{isset($teacherInfo[0]) ? $teacherInfo[0]->identity:''}}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Nơi cấp
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="identity_place_release"
                                                                   class="form-control m-input"
                                                                   placeholder="Nơi cấp CMND"
                                                                   value="{{
                                                                       (isset($teacherInfo[0]) && $teacherInfo[0]->EducationUserInfo)
                                                                       ? $teacherInfo[0]->EducationUserInfo->identity_place_release:''
                                                                       }}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Facebook
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="facebook"
                                                                   class="form-control m-input"
                                                                   placeholder="Địa chỉ facebook"
                                                                   value="{{isset($teacherInfo[0]) ? $teacherInfo[0]->facebook:''}}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Viber
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="viber" class="form-control m-input"
                                                                   placeholder="Địa chỉ viber"
                                                                   value="{{
                                                                       (isset($teacherInfo[0]) && $teacherInfo[0]->EducationUserInfo)
                                                                        ? $teacherInfo[0]->EducationUserInfo->viber:''
                                                                        }}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Mục tiêu
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="target"
                                                                   class="form-control m-input"
                                                                   value="{{
                                                                       (isset($teacherInfo[0]) && $teacherInfo[0]->EducationUserInfo)
                                                                       ? $teacherInfo[0]->EducationUserInfo->target:''
                                                                       }}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Ưu điểm
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="advantage"
                                                                   class="form-control m-input"
                                                                   placeholder="Ưu điểm của bản thân"
                                                                   value="{{
                                                                       (isset($teacherInfo[0]) && $teacherInfo[0]->EducationUserInfo)
                                                                       ? $teacherInfo[0]->EducationUserInfo->advantage:''
                                                                       }}"
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Giới tính
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-5 align-self-center">
                                                            <div class="m-form__group form-group">
                                                                <select class="form-control m-input" id="gender"
                                                                        name="gender">
                                                                    @foreach (\App\Models\Users\User::$listGender as $key => $value)
                                                                        <option value="{{ $key }}" {{($teacherInfo[0]->gender == $key ) ? 'selected':'' }} >
                                                                            {{ $value }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Ngày sinh
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="birthday"
                                                                   class="form-control m-input"
                                                                   id="m_datepicker_1"
                                                                   data-date-format="dd/mm/yyyy"
                                                                   placeholder="Ngày sinh"
                                                                   value="{{isset($teacherInfo[0]) ? convertDate('d/m/Y',$teacherInfo[0]->birthday):''}}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Địa chỉ
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="address"
                                                                   placeholder="Địa chỉ hiện tại"
                                                                   class="form-control m-input"
                                                                   value="{{isset($teacherInfo[0]) ? $teacherInfo[0]->address:''}}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Ngày cấp
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="identity_date"
                                                                   class="form-control m-input"
                                                                   placeholder="Ngày cấp CMND"
                                                                   id="m_datepicker_2"
                                                                   data-date-format="dd/mm/yyyy"
                                                                   value="{{
                                                                       (isset($teacherInfo[0]) && $teacherInfo[0]->EducationUserInfo)
                                                                       ? convertDate('d/m/Y',$teacherInfo[0]->EducationUserInfo->identity_date):''
                                                                       }}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Skype
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="skype" class="form-control m-input"
                                                                   placeholder="Địa chỉ skype"
                                                                   value="{{
                                                                       (isset($teacherInfo[0])&& $teacherInfo[0]->EducationUserInfo)
                                                                       ? $teacherInfo[0]->EducationUserInfo->skype:''
                                                                       }}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Zalo
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="zalo" class="form-control m-input"
                                                                   placeholder="Địa chỉ zalo"
                                                                   value="{{
                                                                       (isset($teacherInfo[0]) && $teacherInfo[0]->EducationUserInfo)
                                                                       ? $teacherInfo[0]->EducationUserInfo->zalo:''
                                                                       }}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                TT hôn nhân
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col-lg-5 align-self-center">
                                                            <div class="m-form__group form-group">
                                                                <select class="form-control m-input" id="marriage"
                                                                        name="marriage">
                                                                    @foreach (\App\Models\Users\EducationUserInfo::$listMarriage as $key => $value)
                                                                        <option value="{{ $key }}" {{($teacherInfo[0]->EducationUserInfo &&  $teacherInfo[0]->EducationUserInfo->marriage == $key ) ? 'selected':'' }} >
                                                                            {{ $value }}
                                                                        </option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Sở thích
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="hobby" class="form-control m-input"
                                                                   placeholder="Sở thích"
                                                                   value="{{
                                                                       (isset($teacherInfo[0]) && $teacherInfo[0]->EducationUserInfo)
                                                                       ? $teacherInfo[0]->EducationUserInfo->hobby:''
                                                                       }}"
                                                            />
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-4">
                                                            <label class="col-form-label">
                                                                Nhược điểm
                                                                <span class="required" aria-required="true">*</span>
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <input type="text" name="defect"
                                                                   class="form-control m-input"
                                                                   placeholder="Nhược điểm"
                                                                   value="{{
                                                                       (isset($teacherInfo[0]) && $teacherInfo[0]->EducationUserInfo)
                                                                       ? $teacherInfo[0]->EducationUserInfo->defect:''
                                                                       }}"
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 1-->
                                        <!--begin: Form Wizard Step 2-->
                                        <div class="m-wizard__form-step  m_repeater_7"
                                             id="m_wizard_form_step_2">
                                            <div class="" data-repeater-list="literacy_param">
                                                @if(isset($teacherInfo_literacy_param) && $teacherInfo_literacy_param)
                                                    @foreach($teacherInfo_literacy_param as $key=>$value)
                                                        <div class="row form-group border p-3" data-repeater-item>
                                                            <div class="col-md-4 align-self-center">
                                                                <div class="row form-group">
                                                                    <div class="col-md-5">
                                                                        <input type="text"
                                                                               name="literacy_param[{{$key}}][from]"
                                                                               class="form-control m-input literacy_param_from"
                                                                               id="literacy_from_{{$key}}"
                                                                               data-date-format="dd/mm/yyyy"
                                                                               placeholder="Từ ngày"
                                                                               value="{{$value->from}}"
                                                                        />
                                                                    </div>
                                                                    <div class="col-md-2 col-lg-2">
                                                                        <div>Đến</div>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <input type="text"
                                                                               name="literacy_param[{{$key}}][to]"
                                                                               class="form-control m-input literacy_param_to"
                                                                               id="literacy_to_{{$key}}"
                                                                               data-date-format="dd/mm/yyyy"
                                                                               placeholder="Đến ngày"
                                                                               value="{{$value->to}}"
                                                                        />
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-lg-6">
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label">
                                                                            Tên trường
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <input type="text"
                                                                               name="literacy_param[{{$key}}][school_name]"
                                                                               placeholder="Tên trường đã học"
                                                                               style="width:306px;"
                                                                               class="form-control m-input"
                                                                               value="{{$value->school_name}}"
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label">
                                                                            Địa chỉ
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <input type="text"
                                                                               name="literacy_param[{{$key}}][school_address]"
                                                                               placeholder="Địa chỉ"
                                                                               class="form-control m-input"
                                                                               style="width:305px;"
                                                                               value="{{$value->school_address}}"
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label">
                                                                            Loại tốt nghiệp
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <select class="form-control m-input"
                                                                                id="marriage"
                                                                                name="str_literacy">
                                                                            @foreach (\App\Models\Users\EducationUserInfo::$listLiteracy as $key_l => $value_l)
                                                                                <option value="{{ $key_l }}" {{($value->str_literacy ==$key_l) ? 'selected':'' }} >
                                                                                    {{ $value_l }}
                                                                                </option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-lg-2 align-self-center">
                                                                <button class="btn btn-primary" type="button"
                                                                        data-repeater-delete
                                                                        style="background-color:rgb(255,15,0);">
                                                                    Xóa
                                                                </button>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <div class="row form-group border p-3" data-repeater-item>
                                                        <div class="col-md-4 align-self-center">
                                                            <div class="row form-group">
                                                                <div class="col-md-5">
                                                                    <input type="text"
                                                                           name="from"
                                                                           class="form-control m-input literacy_param_from"
                                                                           id="m_datepicker_5"
                                                                           data-date-format="dd/mm/yyyy"
                                                                           placeholder="Từ ngày"
                                                                    />
                                                                </div>
                                                                <div class="col-md-2 col-lg-2">
                                                                    <div>Đến</div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input type="text"
                                                                           name="to"
                                                                           class="form-control m-input literacy_param_to"
                                                                           id="m_datepicker_6"
                                                                           data-date-format="dd/mm/yyyy"
                                                                           placeholder="Đến ngày"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-lg-6">
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label">
                                                                        Tên trường
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <input type="text"
                                                                           name="school_name"
                                                                           placeholder="Tên trường đã học"
                                                                           style="width:306px;"
                                                                           class="form-control m-input"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label">
                                                                        Địa chỉ
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <input type="text"
                                                                           name="school_address"
                                                                           placeholder="Địa chỉ"
                                                                           class="form-control m-input"
                                                                           style="width:305px;"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label">
                                                                        Loại tốt nghiệp
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <select class="form-control m-input"
                                                                            id="marriage"
                                                                            name="str_literacy">
                                                                        @foreach (\App\Models\Users\EducationUserInfo::$listLiteracy as $key => $value)
                                                                            <option value="{{ $key }}">
                                                                                {{ $value }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-lg-2 align-self-center">
                                                            <button class="btn btn-primary" type="button"
                                                                    data-repeater-delete
                                                                    style="background-color:rgb(255,15,0);">
                                                                Xóa
                                                            </button>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="col">
                                                <button data-repeater-create class="btn btn-primary float-right"
                                                        type="button">
                                                    Thêm dòng
                                                </button>
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 2-->
                                        <!--begin: Form Wizard Step 3-->
                                        <div class="m-wizard__form-step m_repeater_8" id="m_wizard_form_step_3">
                                            <div class="" data-repeater-list="experienced_param">
                                                @if(isset($teacherInfo_experienced_param) && $teacherInfo_experienced_param)
                                                    @foreach($teacherInfo_experienced_param as $key=>$value)
                                                        <div class="row form-group border p-3" data-repeater-item>
                                                            <div class="col-md-4 align-self-center">
                                                                <div class="row form-group">
                                                                    <div class="col-md-5">
                                                                        <input type="text"
                                                                               name="experienced_param[{{$key}}][from]"
                                                                               class="form-control m-input experienced_param_from"
                                                                               id="experienced_from_{{$key}}"
                                                                               data-date-format="dd/mm/yyyy"
                                                                               placeholder="Từ ngày"
                                                                               value="{{$value->from}}"
                                                                        />
                                                                    </div>
                                                                    <div class="col-md-2 col-lg-2">
                                                                        <div>Đến</div>
                                                                    </div>
                                                                    <div class="col-md-5">
                                                                        <input type="text"
                                                                               name="experienced_param[{{$key}}][to]"
                                                                               class="form-control m-input experienced_param_to"
                                                                               id="experienced_to_{{$key}}"
                                                                               data-date-format="dd/mm/yyyy"
                                                                               placeholder="Đến ngày"
                                                                               value="{{$value->to}}"
                                                                        />
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-lg-6">
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label">
                                                                            Tên công ty
                                                                            <br/>
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <input type="text" class="form-control m-input"
                                                                               name="company_name"
                                                                               placeholder="Tên công ty"
                                                                               style="width:306px;"
                                                                               value="{{$value->company_name}}"
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label">
                                                                            Chức vụ
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <input type="text" class="form-control m-input"
                                                                               name="position"
                                                                               placeholder="Chức vụ"
                                                                               style="width:305px;"
                                                                               value="{{$value->position}}"
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label">
                                                                            Mức lương
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <input type="text" class="form-control m-input"
                                                                               name="wage"
                                                                               placeholder="Mức lương"
                                                                               style="width:305px;"
                                                                               value="{{$value->wage}}"
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label class="col-form-label">
                                                                            Địa chỉ
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                        <input type="text" class="form-control m-input"
                                                                               name="company_address"
                                                                               placeholder="Địa chỉ công ty"
                                                                               style="width:305px;"
                                                                               value="{{$value->company_address}}"
                                                                        />
                                                                    </div>
                                                                </div>
                                                                <div class="row form-group">
                                                                    <div class="col-md-6 col-lg-3">
                                                                        <label
                                                                                class="col-form-label">
                                                                            Lĩnh vực kinh doanh
                                                                        </label>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-8">
                                                                <textarea name="description"
                                                                          class="form-control m-input"
                                                                          style="width:304px;">{{$value->description}}</textarea>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4 col-lg-2 align-self-center">
                                                                <button class="btn btn-primary" type="button"
                                                                        data-repeater-delete
                                                                        style="background-color:rgb(255,15,0);">Xóa
                                                                </button>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <div class="row form-group border p-3" data-repeater-item>
                                                        <div class="col-md-4 align-self-center">
                                                            <div class="row form-group">
                                                                <div class="col-md-5">
                                                                    <input type="text"
                                                                           name="from"
                                                                           class="form-control m-input experienced_param_from"
                                                                           id="m_datepicker_7"
                                                                           data-date-format="dd/mm/yyyy"
                                                                           placeholder="Từ ngày"
                                                                    />
                                                                </div>
                                                                <div class="col-md-2 col-lg-2">
                                                                    <div>Đến</div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <input type="text"
                                                                           name="to"
                                                                           class="form-control m-input experienced_param_to"
                                                                           id="m_datepicker_8"
                                                                           data-date-format="dd/mm/yyyy"
                                                                           placeholder="Đến ngày"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-lg-6">
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label">
                                                                        Tên công ty
                                                                        <br/>
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <input type="text" class="form-control m-input"
                                                                           name="company_name"
                                                                           placeholder="Tên công ty"
                                                                           style="width:306px;"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label">
                                                                        Chức vụ
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <input type="text" class="form-control m-input"
                                                                           name="position"
                                                                           placeholder="Chức vụ"
                                                                           style="width:305px;"
                                                                    />
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label">
                                                                        Mức lương
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <input type="text" class="form-control m-input"
                                                                           name="wage"
                                                                           placeholder="Mức lương"
                                                                           style="width:305px;"/>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label">
                                                                        Địa chỉ
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <input type="text" class="form-control m-input"
                                                                           name="company_address"
                                                                           placeholder="Địa chỉ công ty"
                                                                           style="width:305px;"/>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label
                                                                            class="col-form-label">
                                                                        Lĩnh vực kinh doanh
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                <textarea name="description"
                                                                          class="form-control m-input"
                                                                          style="width:304px;">

                                                                </textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-lg-2 align-self-center">
                                                            <button class="btn btn-primary" type="button"
                                                                    data-repeater-delete
                                                                    style="background-color:rgb(255,15,0);">Xóa
                                                            </button>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="row form-group">
                                                <div class="col"></div>
                                                <div class="col-lg-2 align-self-center">
                                                    <button data-repeater-create class="btn btn-primary" type="button">
                                                        Thêm dòng
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 3-->
                                        <!--begin: Form Wizard Step 4-->
                                        <div class="m-wizard__form-step" id="m_wizard_form_step_4">
                                            <div class="row form-group border p-3">
                                                <div class="col-md-12 align-self-center">
                                                    <div class="row form-group">
                                                        <div class="col-md-6 col-lg-3">
                                                            <label class="col-form-label font-weight-normal">
                                                                Kỹ năng đào tạo
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6 col-lg-8">
                                                            <textarea name="skill_teacher"
                                                                      class="form-control m-input"
                                                                      style="width:600px; height: 250px;">{{
                                                                       (isset($teacherInfo[0]) && $teacherInfo[0]->EducationUserInfo)
                                                                        ? $teacherInfo[0]->EducationUserInfo->skill_teacher:''
                                                                        }}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end: Form Wizard Step 4-->
                                    </div>
                                    <!--end: Form Body -->
                                    <!--begin: Form Actions -->
                                    <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                                        <div class="m-form__actions m-form__actions">
                                            <div class="row form-group">
                                                <div class="col-lg-2"></div>
                                                <div class="col-lg-4 m--align-left">
                                                    <a href="#"
                                                       class="btn btn-secondary m-btn m-btn--custom m-btn--icon"
                                                       data-wizard-action="prev">
                                                    <span>
                                                        <i class="la la-arrow-left"></i>
                                                        &nbsp;&nbsp;
                                                        <span>
                                                            Quay Lại
                                                        </span>
                                                    </span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-4 m--align-right">
                                                    <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon"
                                                       data-wizard-action="submit">
                                                    <span>
                                                        <i class="la la-check"></i>
                                                        &nbsp;&nbsp;
                                                        <span>
                                                            Lưu lại
                                                        </span>
                                                    </span>
                                                    </a>
                                                    <a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon"
                                                       data-wizard-action="next">
                                                    <span>
                                                        <span>
                                                            Lưu và tiếp tục
                                                        </span>
                                                        &nbsp;&nbsp;
                                                        <i class="la la-arrow-right"></i>
                                                    </span>
                                                    </a>
                                                </div>
                                                <div class="col-lg-2"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Actions -->
                                </form>
                            </div>
                            <!--end: Form Wizard Form-->
                        </div>
                        <!--end: Form Wizard-->
                    </div>
                    <!--End::Main Portlet-->
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade bd-example-modal-lg" id="modalDetailLesson" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Các môn học</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr class="d-flex">
                            <th class="col-1 text-center" scope="col">STT</th>
                            <th class="col-sm-7 text-center" scope="col">Tên môn học</th>
                            <th class="col-sm-4 text-center" scope="col">Thời gian</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Đóng</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            jQuery.validator.setDefaults({
                // This will ignore all hidden elements alongside `contenteditable` elements
                // that have no `name` attribute
                ignore: ":hidden, [contenteditable='true']:not([name])"
            });

            $.validator.addMethod(
                "customerDate",
                function (value, element) {
                    // put your own logic here, this is just a (crappy) example
                    return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
                },
                "Nhập lại theo đúng định dạng dd/mm/yyyy."
            );

            var wizardEl = $('#block-project');
            var formEl = $('#education-user-info-form');
            var validator;
            var wizard;
            wizard = wizardEl.mWizard({
                startStep: 1
            });

            //== Validation before going to next page
            wizard.on('beforeNext', function (wizard) {
                if (validator.form() !== true) {
                    return false;  // don't go to the next step
                }
            })

            //== Change event
            wizard.on('change', function (wizard) {
                mApp.scrollTop();
            });
            validator = formEl.validate({
                //== Validate only visible fields
                ignore: ":hidden",

                //== Validation rules
                rules: {
                    //=== Client Information(step 1)
                    //== Client details
                    fullname: {
                        required: true,
                        maxlength: 191
                    },

                    //== Mailing address
                    address: {
                        required: true,
                        maxlength: 191
                    },
                    birthday: {
                        required: true,
                        customerDate: true

                    },
                    phone: {
                        required: true,
                        number: true,
                        minlength: 10,
                        maxlength: 11
                    },
                    identity: {
                        required: true,
                        number: true
                    },
                    identity_date: {
                        required: true,
                        dateITA: true,
                    },
                    identity_place_release: {
                        required: true,
                        maxlength: 191
                    },
                    facebook: {
                        required: true,
                        maxlength: 191
                    },
                    skype: {
                        required: true,
                        maxlength: 191
                    },
                    zalo: {
                        required: true,
                        maxlength: 191
                    },
                    viber: {
                        required: true,
                        maxlength: 191
                    },
                    target: {
                        required: true,
                    },
                    hobby: {
                        required: true,
                    },
                    defect: {
                        required: true,
                    },
                    advantage: {
                        required: true,
                    },

                },


                //== Display error
                invalidHandler: function (event, validator) {
                    mApp.scrollTop();

                    swal({
                        "title": "Thông báo",
                        "text": "Bạn chưa nhập 1 số thông tin cần thiết. Vui lòng kiểm tra lại!",
                        "type": "error",
                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                    });
                },

                //== Submit valid form
                submitHandler: function (form) {

                },

                // Called when the element is invalid:
                highlight: function (element) {
                    $(element).css('border', '1px solid #f48120');
                },

                // Called when the element is valid:
                unhighlight: function (element) {
                    $(element).css('border', '1px solid #ced4da');
                }

            });
            var btn = formEl.find('[data-wizard-action="submit"]');

            btn.on('click', function (e) {
                e.preventDefault();

                if (validator.form()) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    var data = new FormData(document.getElementById('education-user-info-form'));

                    mApp.progress(btn);
                    mApp.block(formEl);
                    $.ajax({
                        url: '/administrator/system/user/education-info/store',
                        cache: false,
                        data: data,
                        type: "post",
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        success: function (response) {
                            mApp.unprogress(btn);
                            mApp.unblock(formEl);
                            swal({
                                "title": "Thêm mới thành công",
                                text: response.message,
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonText: 'Xác nhận!'
                            }).then(function (result) {
                                if (result.value) {
                                    window.location.href = "/administrator/system/user/teacher";
                                }
                            });
                        },
                        error: function (xhr, textStatus, error) {
                            swal({
                                "title": "Thêm mới không thành công",
                                text: JSON.stringify(xhr.responseJSON.errors),
                                type: 'warning',
                                showCancelButton: false,
                                confirmButtonText: 'Xác nhận!'
                            }).then(function (result) {
                                if (result.value) {
                                    window.location.href = "/administrator/system/user/teacher";
                                }
                            });
                        }
                    });
                }
            });
            //init date literacy
            @if(isset($teacherInfo_experienced_param) && $teacherInfo_experienced_param)
            @foreach($teacherInfo_literacy_param as $key=>$value)
            initDate('#literacy_from_{{$key}}');
            initDate('#literacy_to_{{$key}}');
            @endforeach
            @endif

            //init date experienced
            @if(isset($teacherInfo_experienced_param) && $teacherInfo_experienced_param)
            @foreach($teacherInfo_experienced_param as $key=>$value)
            initDate('#experienced_from_{{$key}}');
            initDate('#experienced_to_{{$key}}');
            @endforeach
            @else
            initDate('#m_datepicker_7');
            initDate('#m_datepicker_8');
            @endif

            $('.btn-view-detail-lesson').on('click', function () {
                var subject_id = $(this).data('subject-id');
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '/administrator/education/lesson/ajaxget-alllesson/' + subject_id,
                    cache: false,
                    type: "post",
                    processData: false,
                    contentType: false,
                    success: function (response) {
                        $('#modalDetailLesson .modal-body table tbody').empty();
                        $('#modalDetailLesson .modal-body table tbody').append(response.data)
                    },
                    error: function (data) {

                    }
                });

            })
            uploadAvatarInit();
        });

        function CallUploadFile() {
            $("#uploadAvatar").click();
        }

        function initDate(index) {
            $(index).datepicker({
                todayHighlight: true,
                format: 'dd/mm/yyyy',
                autoclose: true,
                orientation: "bottom left",
            });
        }

        function uploadAvatarInit() {
            $("#uploadAvatar").change(function () {
                var formData = new FormData();
                var totalFiles = document.getElementById("uploadAvatar").files.length;
                for (var i = 0; i < totalFiles; i++) {
                    console.log(1);
                    var file = document.getElementById("uploadAvatar").files[i];
                    formData.append("uploadAvatar", file);
                }
                $.ajax({
                    type: "POST",
                    url: '/administrator/system/user/education-info/uploadavatar',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        if (data) {
                            $("#imgAvatar").attr("src", data.Result);
                            //DisplaySuccess('Ảnh đại diện của bạn đã cập nhật xong');
                        } else {
                            DisplayError('Ảnh đại diện của bạn chưa cập nhật được lên hệ thống');
                        }
                    }
                });
            });
        }

    </script>
@stop