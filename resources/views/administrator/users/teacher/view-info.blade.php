@extends('administrator.app')
@section('title','Thông tin giảng viên')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Danh sách giảng viên
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <!--Begin::Main Portlet-->
                <div class="m-portlet">
                    <!--begin: Portlet Head-->
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Cập nhật thông tin giảng viên
                                    <small>
                                        Bạn thêm thông tin giảng viên theo form dưới đây
                                    </small>
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-toggle="m-tooltip"
                                       class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left"
                                       data-width="auto" title="Get help with filling up this form">
                                        <i class="flaticon-info m--icon-font-size-lg3"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end: Portlet Head-->
                    <!--begin: Form Wizard-->
                    <div class="m-wizard m-wizard--1 m-wizard--success" id="block-project">
                        <!--begin: Message container -->
                        <div class="m-portlet__padding-x">
                        </div>
                        <!--end: Message container -->
                        <!--begin: Form Wizard Head -->
                        <div class="m-wizard__head m-portlet__padding-x">
                            <!--begin: Form Wizard Progress -->
                            <div class="m-wizard__progress">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="100"
                                         aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!--end: Form Wizard Progress -->
                            <!--begin: Form Wizard Nav -->
                            <div class="m-wizard__nav">
                                <div class="m-wizard__steps">
                                    <div class="m-wizard__step m-wizard__step--current"
                                         data-wizard-target="#m_wizard_form_step_1">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    1
                                                </span>
                                            </span>
                                            </a>
                                            <div class="m-wizard__step-label">
                                                Thông tin cơ bản
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_2">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    2
                                                </span>
                                            </span>
                                            </a>
                                            <div class="m-wizard__step-label">
                                                Trình độ học vấn
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_3">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    3
                                                </span>
                                            </span>
                                            </a>
                                            <div class="m-wizard__step-label">
                                                Lĩnh vực kinh doanh
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_4">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                            <span>
                                                <span>
                                                    4
                                                </span>
                                            </span>
                                            </a>
                                            <div class="m-wizard__step-label">
                                                Kỹ năng đào tạo
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Nav -->
                        </div>
                        <!--end: Form Wizard Head -->
                        <!--begin: Form Wizard Form-->
                        <div class="m-wizard__form">
                            <form class="m-form m-form--label-align-left- m-form--state-" id="education-user-info-form" enctype="multipart/form-data">
                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                <input type="hidden" value="{{(isset($teacherInfo[0]) && isset($teacherInfo[0]->EducationUserInfo->id))
                                                         ? $teacherInfo[0]->EducationUserInfo->id:0 }}" />
                                <!--begin: Form Body -->
                                <div class="m-portlet__body">
                                    <!--begin: Form Wizard Step 1-->
                                    <div class="m-wizard__form-step m-wizard__form-step--current m-option" id="m_wizard_form_step_1">
                                        <fieldset class="border p-2">
                                            <legend class="w-auto">Thông tin cơ bản</legend>
                                            <div class="row form-group">
                                                <div class="col-lg-3 align-self-center text-center">
                                                    <img class="profilecard__img wf-80 wf-md-126 mr-md-5 mr-3"
                                                         style="cursor: pointer; width: 200px;" src="{{isset($teacherInfo[0])? $teacherInfo[0]->avatar :''}}" alt="avatar">
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Họ tên:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{isset($teacherInfo[0])? $teacherInfo[0]->fullname :'' }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Email:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{isset($teacherInfo[0])? $teacherInfo[0]->email :'' }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Số điện thoại:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{isset($teacherInfo[0])? $teacherInfo[0]->phone :'' }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Số CMND:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{isset($teacherInfo[0])? $teacherInfo[0]->identity :'' }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">
                                                                Nơi cấp:
                                                            </label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{(isset($teacherInfo[0]) && isset($teacherInfo[0]->EducationUserInfo->identity_place_release))
                                                                  ? $teacherInfo[0]->EducationUserInfo->identity_place_release:''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Facebook:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{isset($teacherInfo[0]) ? $teacherInfo[0]->facebook:''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Viber:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{(isset($teacherInfo[0]) && isset($teacherInfo[0]->EducationUserInfo->viber))
                                                                    ? $teacherInfo[0]->EducationUserInfo->viber:''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Mục tiêu:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{(isset($teacherInfo[0]) && isset($teacherInfo[0]->EducationUserInfo->target))
                                                                  ? $teacherInfo[0]->EducationUserInfo->target:''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Ưu điểm:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{(isset($teacherInfo[0]) && isset($teacherInfo[0]->EducationUserInfo->advantage))
                                                                   ? $teacherInfo[0]->EducationUserInfo->advantage:''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Giới tính:</label>
                                                        </div>
                                                        <div class="col-lg-5 align-self-center">
                                                            <div class="m-form__group form-group">
                                                                <label class="col-form-label">
                                                                    {{isset(\App\Models\Users\User::$listGender[$teacherInfo[0]->gender]) ?
                                                                        \App\Models\Users\User::$listGender[$teacherInfo[0]->gender]:''}}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Ngày sinh:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{isset($teacherInfo[0]) ? convertDate('d/m/Y',$teacherInfo[0]->birthday):''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Địa chỉ:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{isset($teacherInfo[0]) ? $teacherInfo[0]->address:''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Ngày cấp:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{(isset($teacherInfo[0]) &&  isset($teacherInfo[0]->EducationUserInfo->identity_date))
                                                                   ? convertDate('d/m/Y',$teacherInfo[0]->EducationUserInfo->identity_date):''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Skype:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{
                                                                   (isset($teacherInfo[0]) && isset($teacherInfo[0]->EducationUserInfo->skype))
                                                                   ? $teacherInfo[0]->EducationUserInfo->skype:''
                                                                   }}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Zalo:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{(isset($teacherInfo[0]) && isset($teacherInfo[0]->EducationUserInfo->zalo))
                                                                   ? $teacherInfo[0]->EducationUserInfo->zalo:''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">TT hôn nhân:</label>
                                                        </div>
                                                        <div class="col-lg-5 align-self-center">
                                                            <div class="m-form__group form-group">
                                                                <label class="col-form-label">
                                                                    {{isset($teacherInfo[0]->EducationUserInfo->marriage) ?
                                                                        \App\Models\Users\EducationUserInfo::$listMarriage[$teacherInfo[0]->EducationUserInfo->marriage]:''
                                                                        }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Sở thích:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{(isset($teacherInfo[0]) &&  isset($teacherInfo[0]->EducationUserInfo->hobby))
                                                                   ? $teacherInfo[0]->EducationUserInfo->hobby:''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <div class="row form-group">
                                                        <div class="col-lg-3">
                                                            <label class="col-form-label font-weight-normal">Nhược điểm:</label>
                                                        </div>
                                                        <div class="col">
                                                            <label class="col-form-label">
                                                                {{(isset($teacherInfo[0]) &&  isset($teacherInfo[0]->EducationUserInfo->defect))
                                                                    ? $teacherInfo[0]->EducationUserInfo->defect:''}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                    <!--end: Form Wizard Step 1-->
                                    <!--begin: Form Wizard Step 2-->
                                    <div class="m-wizard__form-step  m_repeater_7" id="m_wizard_form_step_2">
                                        <div class="" data-repeater-list="literacy_param">
                                            @if(isset($teacherInfo_literacy_param) && $teacherInfo_literacy_param)
                                                @foreach($teacherInfo_literacy_param as $key=>$value)
                                                    <div class="row form-group border p-3" data-repeater-item>
                                                        <div class="col-md-4 align-self-center">
                                                            <div class="row form-group">
                                                                <div class="col-md-3">
                                                                    <label class="col-form-label">
                                                                        {{$value->from}}
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-2 col-lg-2">
                                                                    <div>Đến</div>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label class="col-form-label">
                                                                        {{$value->to}}
                                                                    </label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-lg-6">
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label font-weight-normal">Tên trường:</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <label class="col-form-label">
                                                                        {{$value->school_name}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label font-weight-normal">Địa chỉ:</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <label class="col-form-label">
                                                                        {{$value->school_address}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label font-weight-normal">Loại tốt nghiệp:</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <label class="col-form-label">
                                                                        {{\App\Models\Users\EducationUserInfo::$listLiteracy[$value->str_literacy]}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="row form-group border p-3" data-repeater-item>
                                                    Thông tin giảng viên chưa được cập nhật
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 2-->
                                    <!--begin: Form Wizard Step 3-->
                                    <div class="m-wizard__form-step m_repeater_8" id="m_wizard_form_step_3">
                                        <div class="" data-repeater-list="experienced_param">
                                            @if(isset($teacherInfo_literacy_param) && $teacherInfo_experienced_param)
                                                @foreach($teacherInfo_experienced_param as $key=>$value)
                                                    <div class="row form-group border p-3" data-repeater-item>
                                                        <div class="col-md-4 align-self-center">
                                                            <div class="row form-group">
                                                                <div class="col-md-3">
                                                                    <label class="col-form-label">
                                                                        {{$value->from}}
                                                                    </label>
                                                                </div>
                                                                <div class="col-md-2 col-lg-2">
                                                                    <div>Đến</div>
                                                                </div>
                                                                <div class="col-md-5">
                                                                    <label class="col-form-label">
                                                                        {{$value->from}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-lg-6">
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label font-weight-normal">Tên công ty:<br/></label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <label class="col-form-label">
                                                                        {{$value->company_name}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label font-weight-normal">Chức vụ:</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <label class="col-form-label">
                                                                        {{$value->position}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label font-weight-normal">Mức lương:</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <label class="col-form-label">
                                                                        {{$value->wage}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label font-weight-normal">Địa chỉ:</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <label class="col-form-label">
                                                                        {{$value->company_address}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="row form-group">
                                                                <div class="col-md-6 col-lg-3">
                                                                    <label class="col-form-label font-weight-normal">Mô tả:</label>
                                                                </div>
                                                                <div class="col-md-6 col-lg-8">
                                                                    <label class="col-form-label">
                                                                        {{$value->description}}
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="row form-group border p-3" data-repeater-item>
                                                    Thông tin giảng viên chưa được cập nhật
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 3-->
                                    <!--begin: Form Wizard Step 4-->
                                    <div class="m-wizard__form-step" id="m_wizard_form_step_4">
                                        @if(isset($teacherInfo[0]) && isset($teacherInfo[0]->EducationUserInfo->skill_teacher))
                                            <div class="row form-group border p-3">
                                                <div class="col-md-12 align-self-center">
                                                    <div class="row form-group">
                                                        <div class="col-md-6 col-lg-3">
                                                            <label class="col-form-label font-weight-normal">
                                                                Kỹ năng đào tạo
                                                            </label>
                                                        </div>
                                                        <div class="col-md-6 col-lg-8">
                                                            <label class="col-form-label">{{$teacherInfo[0]->EducationUserInfo->skill_teacher}}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @else
                                        <div class="row form-group border p-3" data-repeater-item>
                                            Thông tin giảng viên chưa được cập nhật
                                        </div>
                                        @endif
                                    </div>
                                    <!--end: Form Wizard Step 4-->
                                </div>
                                <!--end: Form Body -->
                                <!--begin: Form Actions -->
                                <!--end: Form Actions -->
                            </form>
                        </div>
                        <!--end: Form Wizard Form-->
                        <!--end: Form Wizard Form-->
                    </div>
                    <!--end: Form Wizard-->
                </div>
                <!--End::Main Portlet-->
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {

        var wizardEl = $('#block-project');
        var wizard;
        wizard = wizardEl.mWizard({
            startStep: 1
        });

        //== Validation before going to next page
        wizard.on('beforeNext', function (wizard) {

        })

        //== Change event
        wizard.on('change', function (wizard) {
            mApp.scrollTop();
        });

    });

</script>
@stop