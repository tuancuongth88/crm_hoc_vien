@extends('administrator.app')
@section('title',' Quản lý tin')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Tin tức<h1 data-localize="greeting"></h1>
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <a href="{{ route('news.index') }}" class="btn btn-success">Danh sách tin</a>
            </div>
            <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Thêm mới
                                </h3>
                            </div>
                        </div>
                    </div>
                    {{ Form::open(array('route' => 'news.store', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed m-form--state', 'id' => 'create-news', 'enctype' => 'multipart/form-data')) }}
                        <div class="m-portlet__body">
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Tiêu đề
                                </label>
                                <input type="text" class="form-control m-input m-input--solid" id="title" placeholder="Tiêu đề tin" name="title" value="{{ old('title') }}">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Mô tả
                                </label>
                                <textarea class="form-control m-input m-input--solid" id="description" rows="3" name="description">{{ old('description') }}</textarea>
                                Tổng số ký tự: <span id="totalChars">0</span><br/>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="col-form-label">
                                    Nội dung
                                </label>
                                <textarea class="summernote" id="summernote" name="content">{!! old('content') !!}</textarea>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="category_id">
                                    Danh mục
                                    </label>
                                    <select class="form-control m-input m-input--solid" id="category_id" name="category_id" required>
                                        @foreach ($category as $element)
                                            <option value="{{ $element['id'] }}" {{ (old('category_id') == $element['id']) ? 'selected' : '' }}>
                                                {{ $element['name'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-6">
                                    <label for="project_id">
                                    Dự án
                                    </label>
                                    <select class="form-control m-input m-input--solid" id="project_id" name="project_id" required>
                                        @foreach ($project as $element)
                                            <option value="{{ $element['id'] }}" {{ (old('project_id') == $element['id']) ? 'selected' : '' }}>
                                                {{ $element['name'] }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label class="">
                                        Ảnh đại diện
                                    </label>
                                    <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="customFile" name="image_url">
                                        <label class="custom-file-label" for="customFile">
                                            Chọn ảnh
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">

                                <div class="col-lg-4">
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('news.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>

                    {{ Form::close() }}
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>
@section('script')
<script type="text/javascript">
    $('#summernote').summernote({
        callbacks: {
            onImageUpload: function(image) {
                uploadImage(image[0]);
            }
        },
        height: 350,
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['style']],
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['table', ['table']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['picture', ['picture']]
        ]
    });
    function uploadImage(image) {
        var data = new FormData();
        data.append("image", image);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
            url: '/administrator/upload',
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "post",
            success: function(response) {
                if (response.status) {
                    var image = $('<img width="100%">').attr('src', response.data);
                    $('#summernote').summernote("insertNode", image[0]);
                }

            },
            error: function(data) {
                console.log(data);
            }
        });
    }
</script>
<script src="{{ URL::asset('js/news/script.js') }}" type="text/javascript"></script>
@stop
@stop
