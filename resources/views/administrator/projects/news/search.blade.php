{{ Form::open(array('route' => 'news.search', 'method' => 'GET', 'id' =>'search_form')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tiêu đề</label>
        <input type="text" name="name" id="name" class="form-control" value="{{ @$input['name'] }}" placeholder="Tiêu đề bài viết" />
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Danh mục tin</label>
        {{ Form::select('category_id', ['' => 'Chọn danh mục'] +  $listCategory , @$input['category_id'], ['class' => 'form-control']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Dự án</label>
        {{ Form::select('project_id', ['' => 'Chọn dự án'] +  getOptionByModel(\App\Models\Projects\ProjectManager::class, 'id', 'name') , @$input['project_id'], ['class' => 'form-control']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Tìm kiếm" class="btn btn-primary form-control" />
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Hủy tìm</label>
        <a href="{{ route('news.index') }}" class="btn btn-danger form-control">Hủy tìm</a>
    </div>
</div>
</form>
