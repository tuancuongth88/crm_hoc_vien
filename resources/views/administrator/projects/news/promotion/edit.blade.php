@extends('administrator.app')
@section('title','Sửa tin ưu đãi')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Tin tức
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px">
                    <a href="{{ route('news.promotion') }}" class="btn btn-success">Danh sách tin ưu đãi</a>
                </div>
                <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                    <h3 class="m-portlet__head-text">
                                        Cập nhật tin
                                    </h3>
                                </div>
                            </div>
                        </div>
                        {{ Form::open(array('route' => array('news.promotion.update', $data->id), 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype' => 'multipart/form-data')) }}
                        <div class="m-portlet__body">
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Tiêu đề
                                </label>
                                <input type="text" class="form-control m-input m-input--solid" id="title" placeholder="Tiêu đề tin" name="title" value="{{ $data->title }}">
                            </div>
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Mô tả
                                </label>
                                <textarea class="form-control m-input m-input--solid" id="description" rows="3" name="description">{{ $data->description }}</textarea>
                                Tổng số ký tự: <span id="totalChars">0</span><br/>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="col-form-label">
                                    Nội dung
                                </label>
                                <textarea class="" id="summernote_edit" name="content">{{ $data->content }}</textarea>
                            </div>

                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label class="">
                                        Ảnh đại diện
                                    </label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="customFile" name="image_url">
                                        <label class="custom-file-label" for="customFile">
                                            Chọn ảnh
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label for="name">
                                        Ảnh đã chọn
                                    </label></br>
                                    <img src="{{ $data->image_url }}" alt="" height="75px">
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('news.promotion') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>

                        {{ Form::close() }}

                    </div>
                    <!--end::Portlet-->
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
@section('script')
    <script src="{{ URL::asset('js/news/script.js') }}" type="text/javascript"></script>
@stop
@stop
