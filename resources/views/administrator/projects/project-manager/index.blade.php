@extends('administrator.app')
@section('title','Danh sách')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--begin::Section-->
        @include('administrator.errors.info-search')
        <div class="m-section">
            <div class="m-section__content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Dự án
                                    <small>
                                        Quản lý dự án
                                    </small>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="m-content">
                            <!--begin::Section-->
                            @if (\Auth::user()->can('project-manager.create'))
                                <div class="col-xs-12" style="margin-bottom: 20px">
                                    <a href="{{ route('project-manager.create') }}" class="btn btn-primary m-btn m-btn--icon">
                                        <span>
                                            <i class="fa flaticon-plus"></i>
                                            <span>
                                                Thêm mới
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            @endif
                            @include('administrator.errors.info-search')
                            <div class="m-section">
                                @include('administrator.errors.messages')
                                <div class="m-section__content">
                                    <table class="table m-table m-table--head-bg-warning">
                                        <thead>
                                        <tr>
                                            <th>
                                                #
                                            </th>
                                            <th>
                                                Tên dự án
                                            </th>
                                            <th>
                                                Người tạo
                                            </th>
                                            <th>
                                                Thời gian tạo
                                            </th>
                                            <th>
                                                Thời gian cập nhật
                                            </th>
                                            <th>
                                                Tùy chọn
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $key => $value)
                                        <tr>
                                            <th scope="row">
                                                {{ $key + 1 }}
                                            </th>
                                            <td>
                                                {{ $value['name'] }}
                                            </td>
                                            <td>
                                                {{ \App\Models\Users\User::find(Auth::user()->id)->fullname }}
                                            </td>
                                            <td>
                                                {{ $value->created_at }}
                                            </td>
                                            <td>
                                                {{ $value->updated_at }}
                                            </td>
                                            <td>
                                                @if (\Auth::user()->can('project-manager.update'))
                                                    <a href="{{ route('project-manager.edit', $value->id) }}"
                                                       class="btn btn-accent m-btn m-btn--icon m-btn--icon-only tooltips"
                                                       data-original-title="Sửa dự án"
                                                       data-toggle="m-tooltip">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                @endif
                                                @if (\Auth::user()->can('project-manager.delete'))
                                                    {{ Form::open(array('method'=>'DELETE', 'route' => array('project-manager.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                                    <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');"
                                                            class="btn btn-danger m-btn m-btn--icon m-btn--icon-only"
                                                            data-original-title="Xóa dự án"
                                                            data-toggle="m-tooltip">
                                                        <i class="fa fa-close"></i>
                                                    </button>
                                                    {{ Form::close() }}
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!--end::Section-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop