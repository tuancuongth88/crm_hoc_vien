@extends('administrator.app')
@section('title',' dự án')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px">
                    <a href="{{ route('project-manager.index') }}" class="btn btn-primary m-btn m-btn--icon">
					<span>
						<i class="fa flaticon-list-3"></i>
						<span>
							Danh sách dự án
						</span>
					</span>
                    </a>
                </div>
                <div class="col-md-12">
                    @include('administrator.errors.errors-validate')
                    <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        Thêm mới dự án
                                    </h3>
                                </div>
                            </div>
                        </div>
                        {{ Form::open(array('route' => 'project-manager.store', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype' => 'multipart/form-data')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label>
                                        Tên dự án: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="Nhập tên dự án" name="name" value="{{ old('name') }}">
                                    </div>
                                    <span class="m-form__help">
                                        Tên hiển thị Dự án
                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Email: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="email" class="form-control m-input" placeholder="Nhập Email hỗ trợ" name="email" value="{{ old('email') }}">
                                    </div>
                                    <span class="m-form__help">
                                        Nhập Email Hỗ trợ dự án
                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Hot Line chăm sóc: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="Nhập Hotline hỗ trợ" name="hotline" value="{{ old('hotline') }}">
                                    </div>
                                    <span class="m-form__help">
                                        Nhập HotLine Chăm Sóc
                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Ảnh dự án: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="hidden" name="image_url" value="" id="image_url">
                                        <input type="file" class="form-control m-input" name="file" value="{{ old('file') }}">
                                    </div>
                                    <span class="m-form__help">
                                        Ảnh Dự án
                                    </span>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <label>
                                        Mô tả:
                                    </label>
                                    <textarea class="summernote" name="description" id="description">{{ old('description') }}</textarea>
                                    <span class="m-form__help">
                                        Mô tả Dự án
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('project-manager.index') }}">
                                    <i class="la la-arrow-circle-o-left"></i>
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                    <!--end::Portlet-->
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
    <input type="hidden" id="api_file" name="api_file" value="{{ env('API_FILE') }}">
    <script>
        jQuery(document).ready(function() {
            $('.summernote').summernote({
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });

            $('input[type=file]').change(function(e){
                var inputName = $(this).attr('name');
                $(e.target.files).each(function( index, file ) {
                    var data = new FormData();
                    data.append("file", file);
                    $.ajax({
                        url: $('input[name=api_file]').val() + '/api/file',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data,
                        type: "post",
                        success: function(response) {
                            $('#image_url').val(response.data.url);
                        },
                        error: function(data) {

                        }
                    });
                });


                return false;

            });
        });
    </script>
@stop