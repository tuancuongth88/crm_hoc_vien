@extends('administrator.app')
@section('title','Danh sách')
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <!--begin::Section-->
            @include('administrator.errors.info-search')
            <div class="m-section">
                <div class="m-section__content">
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <span class="m-portlet__head-icon m--hide">
                                        <i class="la la-gear"></i>
                                    </span>
                                    <h3 class="m-portlet__head-text">
                                        Sửa dự án
                                    </h3>
                                </div>
                            </div>
                        </div>
                        {{ Form::open(array('route' => array('project-manager.update', $data->id), 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right','enctype' => 'multipart/form-data')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-6">
                                    <label>
                                        Tên dự án: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="Nhập tên dự án" name="name" value="{{ $data->name }}">
                                    </div>
                                    <span class="m-form__help">
                                        Tên hiển thị Dự án
                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Email: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="email" class="form-control m-input" placeholder="Nhập Email hỗ trợ" name="email" value="{{ $data->email }}">
                                    </div>
                                    <span class="m-form__help">
                                        Nhập Email Hỗ trợ dự án
                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <label>
                                        Hot Line chăm sóc: <span class="required">*</span>
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="Nhập Hotline hỗ trợ" name="hotline" value="{{ $data->hotline }}">
                                    </div>
                                    <span class="m-form__help">
                                        Nhập Hot Line Chăm Sóc
                                    </span>
                                </div>
                                <div class="col-lg-6">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <label>
                                                Ảnh dự án: <span class="required">*</span>
                                            </label>
                                            <div class="input-group m-input-group m-input-group--square">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <i class="la la-slack"></i>
                                                    </span>
                                                </div>
                                                <img src="">
                                                <input type="hidden" name="image_url" value="{{ $data->image_url }}" id="image_url">
                                                <input type="file" class="form-control m-input" placeholder="Nhập Email hỗ trợ" name="file" value="{{ old('email') }}">
                                            </div>
                                            <span class="m-form__help">
                                                Ảnh Dự án
                                            </span>
                                        </div>
                                        <div class="col-lg-6">
                                            <img src="{{ $data->image_url }}" width="200px">
                                        </div>        
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">
                                    <label>
                                        Mô tả:
                                    </label>
                                    <textarea class="summernote" name="description" id="description">{{ $data->description }}</textarea>
                                    <span class="m-form__help">
                                        Mô tả Dự án
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('project-manager.index') }}">
                                    <i class="la la-arrow-circle-o-left"></i>
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" id="api_file" name="api_file" value="{{ env('API_FILE') }}">
    <script>
        jQuery(document).ready(function() {
            $('.summernote').summernote({
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']]
                ]
            });
            $('input[type=file]').change(function(e){
                var inputName = $(this).attr('name');
                $(e.target.files).each(function( index, file ) {
                    var data = new FormData();
                    data.append("file", file);
                    $.ajax({
                        url: $('input[name=api_file]').val() + '/api/file',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data,
                        type: "post",
                        success: function(response) {
                            $('#image_url').val(response.data.url);
                        },
                        error: function(data) {

                        }
                    });
                });


                return false;

            });
        });
    </script>
@stop