@extends('administrator.app')
@section('title',' dự án')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Dự án
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <!--Begin::Main Portlet-->
                <div class="m-portlet">
                    <!--begin: Portlet Head-->
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Thêm mới dự án
                                    <small>
                                        Bạn thêm thông tin dự án bạn đang bán theo form dưới đây
                                    </small>
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="m-portlet__nav">
                                <li class="m-portlet__nav-item">
                                    <a href="#" data-toggle="m-tooltip" class="m-portlet__nav-link m-portlet__nav-link--icon" data-direction="left" data-width="auto" title="Get help with filling up this form">
                                        <i class="flaticon-info m--icon-font-size-lg3"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--end: Portlet Head-->
<!--begin: Form Wizard-->
                    <div class="m-wizard m-wizard--1 m-wizard--success" id="block-project">
                        <!--begin: Message container -->
                        <div class="m-portlet__padding-x">
                            <!-- Here you can put a message or alert -->
                        </div>
                        <!--end: Message container -->
<!--begin: Form Wizard Head -->
                        <div class="m-wizard__head m-portlet__padding-x">
                            <!--begin: Form Wizard Progress -->
                            <div class="m-wizard__progress">
                                <div class="progress">
                                    <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                            <!--end: Form Wizard Progress -->
    <!--begin: Form Wizard Nav -->
                            <div class="m-wizard__nav">
                                <div class="m-wizard__steps">
                                    <div class="m-wizard__step m-wizard__step--current" data-wizard-target="#m_wizard_form_step_1">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        1
                                                    </span>
                                                </span>
                                            </a>
                                            <div class="m-wizard__step-line">
                                                <span></span>
                                            </div>
                                            <div class="m-wizard__step-label">
                                                Thông tin cơ bản
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_2">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        2
                                                    </span>
                                                </span>
                                            </a>
                                            <div class="m-wizard__step-line">
                                                <span></span>
                                            </div>
                                            <div class="m-wizard__step-label">
                                                Thông tin chi tiết
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_3">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        3
                                                    </span>
                                                </span>
                                            </a>
                                            <div class="m-wizard__step-line">
                                                <span></span>
                                            </div>
                                            <div class="m-wizard__step-label">
                                                Thông tin quản lý dự án
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-wizard__step" data-wizard-target="#m_wizard_form_step_4">
                                        <div class="m-wizard__step-info">
                                            <a href="#" class="m-wizard__step-number">
                                                <span>
                                                    <span>
                                                        4
                                                    </span>
                                                </span>
                                            </a>
                                            <div class="m-wizard__step-line">
                                                <span></span>
                                            </div>
                                            <div class="m-wizard__step-label">
                                                Chân dung khách hàng tiềm năng
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end: Form Wizard Nav -->
                        </div>
                        <!--end: Form Wizard Head -->
                        <!--begin: Form Wizard Form-->
                        <div class="m-wizard__form">
                            <form class="m-form m-form--label-align-left- m-form--state-" id="project-create-form" enctype="multipart/form-data">
                                <meta name="csrf-token" content="{{ csrf_token() }}">
                                <!--begin: Form Body -->
                                <div class="m-portlet__body">
                                    <!--begin: Form Wizard Step 1-->
                                    <div class="m-wizard__form-step m-wizard__form-step--current" id="m_wizard_form_step_1">
                                        <div class="row">
                                            <div class="col-xl-8 offset-xl-2">
                                                <div class="m-form__section m-form__section--first">
                                                    <div class="m-form__heading">
                                                        <h3 class="m-form__heading-title">
                                                            Thông tin cơ bản về dự án
                                                        </h3>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            <span style="color: red;">(*)</span> Nhóm dự án:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            {!!Form::select('project_manager_id', ['' => 'Chọn nhóm dự án'] + $listProjectManager, null, ['class' => 'form-control m-input', 'id' => 'project_manager_id'])!!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            <span style="color: red;">(*)</span> Tên dự án:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <input type="text" name="name" class="form-control m-input" placeholder="" maxlength="191">
                                                            <span class="m-form__help">
                                                                Nhập tên đầy đủ của dự án vào đây
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            <span style="color: red;">(*)</span> Chủ đầu tư:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <input type="text" name="investor" class="form-control m-input" placeholder="" maxlength="191" id="investor_id">
                                                            <span class="m-form__help">
                                                                Nhập tên công ty chủ đầu tư vào đây
                                                            </span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            <span style="color: red;">(*)</span> Loại dự án:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <select class="form-control m-input" id="type" name="type">
                                                                @foreach ($type as $element)
                                                                    <option value="{{ $element['id'] }}">
                                                                        {{ $element['name'] }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            <span style="color: red;">(*)</span> Phân khúc dự án:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <div class="m-radio-list" id="toastTypeGroup">
                                                                @foreach ($segments as $element)
                                                                <label class="m-radio m-radio--check-bold m-radio--state-success">
                                                                    {{ Form::radio('segment', $element['id'] , false) }}
                                                                    {{ $element['name'] }}
                                                                    <span></span>
                                                                </label>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-separator m-separator--dashed m-separator--lg"></div>
                                                <div class="m-form__section">
                                                    <div class="m-form__heading">
                                                        <h3 class="m-form__heading-title">
                                                            Địa chỉ dự án
                                                            <i data-toggle="m-tooltip" data-width="auto" class="m-form__heading-help-icon flaticon-info"></i>
                                                        </h3>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            <span style="color: red;">(*)</span> Tỉnh/ Thành phố
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <select class="form-control m-input" id="city_id" name="city_id">
                                                                <option>Chọn tỉnh/thành</option>
                                                                @foreach ($listCity as $key => $city)
                                                                    <option value="{{ $key }}">
                                                                        {{ $city }}
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            <span style="color: red;">(*)</span> Quận / Huyện:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <select class="form-control m-input" id="district_id" name="district_id">
                                                                <option value="">Chọn quận/huyện</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-xl-3 col-lg-3 col-form-label">
                                                            <span style="color: red;">(*)</span> Địa chỉ dự án:
                                                        </label>
                                                        <div class="col-xl-9 col-lg-9">
                                                            <input type="text" name="address" class="form-control m-input" maxlength="191">
                                                            {{-- <span class="m-form__help">
                                                                Tố Hữu, Hà Đông, Hà Nội
                                                            </span> --}}
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 1-->
            <!--begin: Form Wizard Step 2-->
                                    <div class="m-wizard__form-step" id="m_wizard_form_step_2">

                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-2 col-sm-12">
                                                <strong>Tổng quan dự án</strong>
                                            </label>
                                            <div class="col-lg-10 col-md-9 col-sm-12">
                                                <textarea class="summernote" name="content" id="content"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-2 col-sm-12">
                                                <strong>Vị trí dự án</strong>
                                            </label>
                                            <div class="col-lg-10 col-md-9 col-sm-12">
                                                <textarea class="summernote" name="area_position" id="area_position"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-2 col-sm-12">
                                                <strong>Tiện ích nội khu</strong>
                                            </label>
                                            <div class="col-lg-10 col-md-9 col-sm-12">
                                                <textarea class="summernote" name="utilitie_local" id="utilitie_local"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-2 col-sm-12">
                                                <strong>Kết nối vùng</strong>
                                            </label>
                                            <div class="col-lg-10 col-md-9 col-sm-12">
                                                <textarea class="summernote" name="area_connect" id="area_connect"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-2 col-sm-12">
                                                <strong>Mặt bằng căn hộ</strong>
                                            </label>
                                            <div class="col-lg-10 col-md-9 col-sm-12">
                                                <textarea class="summernote" name="area_ground" id="area_ground"></textarea>
                                            </div>
                                        </div>
                                       {{-- <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">
                                                <strong>Hồ sơ pháp lý</strong>
                                            </label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <input type="file" class="form-control-file" data-name="profile">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">
                                                <strong>Salekit dự án</strong>
                                            </label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <input type="file" class="form-control-file" data-name="salekit">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">
                                                <strong>Bộ Q&A dự án</strong>
                                            </label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <input type="file" class="form-control-file" data-name="qa">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">
                                                <strong>Form mẫu - hợp đồng</strong>
                                            </label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <input type="file" class="form-control-file" data-name="contract">
                                            </div>
                                        </div>
                                        <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">
                                                <strong>Chính sách bán hàng</strong>
                                            </label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <input type="file" class="form-control-file" data-name="policy">
                                            </div>
                                        </div>--}}
                                        {{-- <div class="form-group m-form__group row">
                                            <label class="col-form-label col-lg-3 col-sm-12">
                                                Tải lên các tài liệu dự án
                                            </label>
                                            <div class="col-lg-4 col-md-9 col-sm-12">
                                                <div action="/administrator/info/project/attachment" class="m-dropzone dropzone m-dropzone--primary" id="attachment">
                                                    <div class="m-dropzone__msg dz-message needsclick">
                                                        <h3 class="m-dropzone__msg-title">
                                                            Kéo thả file vào đây để upload
                                                        </h3>
                                                        <span class="m-dropzone__msg-desc">
                                                            Tối đa 10 File kích thước 5MB/file
                                                        </span>
                                                    </div>
                                                </div >
                                            </div>
                                        </div> --}}
                                    </div>
                                    <!--end: Form Wizard Step 2-->
            <!--begin: Form Wizard Step 3-->
                                    <div class="m-wizard__form-step" id="m_wizard_form_step_3">
                                        <div class="row">
                                            <div class="col-xl-8 offset-xl-2">
                                                <div class="m-form__section m-form__section--first">
                                                    <div id="strong">
                                                        <div class="form-group  m-form__group row">
                                                            <label style="margin-bottom:20px;">
                                                                <strong>Giám đốc dự án</strong>
                                                            </label>
                                                            <div data-repeater-list="director" class="col-lg-12">
                                                                <div data-repeater-item class="form-group m-form__group row align-items-center">
                                                                    <div class="col-md-8">
                                                                        <div class="m-form__group m-form__group--inline">
                                                                            <input type="text" class="form-control m-input" placeholder=" Họ tên" size="50" name="name" style="margin-bottom: 7px">
                                                                            <input type="number" class="form-control m-input" placeholder=" Điện thoại" size="50" name="phone" style="margin-bottom: 7px">
                                                                            <input type="email" class="form-control m-input" placeholder=" Email" size="50" name="email" style="margin-bottom: 7px">
                                                                        </div>
                                                                        <div class="d-md-none m--margin-bottom-10"></div>
                                                                    </div>

                                                                    <div class="col-md-4">
                                                                        <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
                                                                            <span>
                                                                                <i class="la la-trash-o"></i>
                                                                                <span>
                                                                                    Xóa
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="m-form__group form-group row">
                                                            <label class="col-lg-2 col-form-label"></label>
                                                            <div class="col-lg-4">
                                                                <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                                                                    <span>
                                                                        <i class="la la-plus"></i>
                                                                        <span>
                                                                            Thêm mới
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-form__section m-form__section--second">
                                                    <div id="weak">
                                                        <div class="form-group  m-form__group row">
                                                            <label style="margin-bottom:20px;">
                                                                <strong>Admin dự án</strong>
                                                            </label>
                                                            <div data-repeater-list="admin" class="col-lg-12">
                                                                <div data-repeater-item class="form-group m-form__group row align-items-center">
                                                                    <div class="col-md-8">
                                                                        <div class="m-form__group m-form__group--inline">
                                                                            <input type="text" class="form-control" placeholder=" Họ tên" size="50" name="name" style="margin-bottom: 7px">
                                                                            <input type="number" class="form-control" placeholder=" Điện thoại" size="50" name="phone" style="margin-bottom: 7px">
                                                                            <input type="email" class="form-control" placeholder=" Email" size="50" name="email" style="margin-bottom: 7px">
                                                                        </div>
                                                                        <div class="d-md-none m--margin-bottom-10"></div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div data-repeater-delete="" class="btn-sm btn btn-danger m-btn m-btn--icon m-btn--pill">
                                                                            <span>
                                                                                <i class="la la-trash-o"></i>
                                                                                <span>
                                                                                    Xóa
                                                                                </span>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="m-form__group form-group row">
                                                            <label class="col-lg-2 col-form-label"></label>
                                                            <div class="col-lg-4">
                                                                <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                                                                    <span>
                                                                        <i class="la la-plus"></i>
                                                                        <span>
                                                                            Thêm mới
                                                                        </span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end: Form Wizard Step 3-->
                                    <!--begin: Form Wizard Step 4-->
                                    <div class="m-wizard__form-step" id="m_wizard_form_step_4">
                                        <div class="row">
                                            <div class="col-xl-8 offset-xl-2">
                                                <!--begin::Section-->
                                                        <div class="m-form__group form-group">
                                                        <label for="">
                                                            Giới tính
                                                        </label>
                                                        <div class="m-checkbox-inline">
                                                            <label class="m-checkbox">
                                                                <input type="checkbox" name="male" value="1">
                                                                Nam
                                                                <span></span>
                                                            </label>
                                                            <label class="m-checkbox">
                                                                <input type="checkbox" name="female" value="1">
                                                                Nữ
                                                                <span></span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                    <div class="m-form__group form-group">
                                                            <label for="">
                                                            Độ tuổi
                                                        </label>
                                                        <div class="m-ion-range-slider">
                                                            <input type="hidden" id="customer_age" name="customer_age" />
                                                        </div>
                                                    </div>
                                                    <div class="m-form__group form-group">
                                                            <label for="">
                                                            Tầm tài chính
                                                        </label>
                                                        <input type="text" class="form-control m-input m--margin-bottom-10" placeholder="Khoảng 3 - 5 tỷ" name="finance">
                                                    </div>
                                                    <div class="m-form__group form-group">
                                                            <label for="">
                                                            Nhu cầu
                                                        </label>
                                                        <textarea class="summernote" name="customer_require" id="customer_require"></textarea>
                                                    </div>
                                                    <div class="m-form__group form-group">
                                                            <label for="">
                                                            Địa lý
                                                        </label>
                                                        <input type="text" class="form-control m-input m--margin-bottom-10" maxlength="190" name="customer_area">
                                                    </div>
                                                    <div class="m-form__group form-group">
                                                            <label for="">
                                                            Sở thích
                                                        </label>
                                                        <textarea class="summernote" name="customer_hobby" id="customer_hobby"></textarea>
                                                    </div>
                                                    <div class="m-form__group form-group">
                                                            <label for="">
                                                            Trình độ
                                                        </label>
                                                        <input type="text" class="form-control m-input m--margin-bottom-10" name="customer_level">
                                                    </div>
                                                    <div class="m-form__group form-group">
                                                            <label for="">
                                                            Sở hữu nhà
                                                        </label>
                                                        <div class="m-ion-range-slider">
                                                            <input type="hidden" id="has_house" name="has_house" />
                                                        </div>
                                                    </div>
                                                    <div class="m-form__group form-group">
                                                            <label for="">
                                                            Nghề nghiệp xã hội
                                                        </label>
                                                        <textarea class="summernote" name="customer_job" id="customer_job"></textarea>
                                                    </div>
                                                    <div class="form-group m-form__group">
                                                        <label>
                                                            Miêu tả khách hàng tiềm năng một cách khái quát
                                                        </label>
                                                        <div>
                                                            <textarea class="summernote" name="customer_description" id="customer_description"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <!--end::Section-->
                                    </div>
                                    <!--end: Form Wizard Step 4-->
                                </div>
                                <!--end: Form Body -->
        <!--begin: Form Actions -->
                                <div class="m-portlet__foot m-portlet__foot--fit m--margin-top-40">
                                    <div class="m-form__actions m-form__actions">
                                        <div class="row">
                                            <div class="col-lg-2"></div>
                                            <div class="col-lg-4 m--align-left">
                                                <a href="#" class="btn btn-secondary m-btn m-btn--custom m-btn--icon" data-wizard-action="prev">
                                                    <span>
                                                        <i class="la la-arrow-left"></i>
                                                        &nbsp;&nbsp;
                                                        <span>
                                                            Quay Lại
                                                        </span>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="col-lg-4 m--align-right">
                                                <a href="#" class="btn btn-primary m-btn m-btn--custom m-btn--icon" data-wizard-action="submit">
                                                    <span>
                                                        <i class="la la-check"></i>
                                                        &nbsp;&nbsp;
                                                        <span>
                                                            Lưu lại
                                                        </span>
                                                    </span>
                                                </a>
                                                <a href="#" class="btn btn-warning m-btn m-btn--custom m-btn--icon" data-wizard-action="next">
                                                    <span>
                                                        <span>
                                                            Lưu và tiếp tục
                                                        </span>
                                                        &nbsp;&nbsp;
                                                        <i class="la la-arrow-right"></i>
                                                    </span>
                                                </a>
                                            </div>
                                            <div class="col-lg-2"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--end: Form Actions -->
                            </form>
                        </div>
                        <!--end: Form Wizard Form-->
                    </div>
                    <!--end: Form Wizard-->
                </div>
                <!--End::Main Portlet-->
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="api_file" name="api_file" value="{{ env('API_FILE') }}">
<script type="text/javascript">
    $(document).ready(function() {
        jQuery.validator.setDefaults({
          // This will ignore all hidden elements alongside `contenteditable` elements
          // that have no `name` attribute
          ignore: ":hidden, [contenteditable='true']:not([name])"
        });
        $('#strong').repeater({
            initEmpty: false,
            show: function () {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });

        $('#weak').repeater({
            initEmpty: false,
            show: function() {
                $(this).slideDown();
            },

            hide: function(deleteElement) {
                if(confirm('Are you sure you want to delete this element?')) {
                    $(this).slideUp(deleteElement);
                }
            }
        });

        $('#customer_age').ionRangeSlider({
            type: "double",
            min: 18,
            max: 80,
            prefix: "Tuổi: ",
            decorate_both: true
        });

        $('#has_house').ionRangeSlider({
            min: 0,
            max: 10,
            prefix: "Căn: ",
        });

        $( "#city_id" ).change(function() {
            $('#district_id').empty().append('<option>Chọn quận/huyện</option>');
            var id = parseInt($(this).val());
            if (id > 0) {
                $.ajax({
                    url: '/administrator/info/project/city/' + id,
                    type: 'GET',
                    success: function(response) {
                        $('#district_id').empty();
                        $.each(response, function (i, item) {
                            $('#district_id').append($('<option>', {
                                value: item.district_id,
                                text : item.name
                            }));
                        });
                    },
                    error: function(data) {

                    }
                });
            }

        });

        var wizardEl = $('#block-project');
        var formEl = $('#project-create-form');
        var validator;
        var wizard;
        wizard = wizardEl.mWizard({
            startStep: 1
        });

        //== Validation before going to next page
        wizard.on('beforeNext', function(wizard) {
            if (validator.form() !== true) {
                return false;  // don't go to the next step
            }
        })

        //== Change event
        wizard.on('change', function(wizard) {
            mApp.scrollTop();
        });
        validator = formEl.validate({
            //== Validate only visible fields
            ignore: ":hidden",

            //== Validation rules
            rules: {
                //=== Client Information(step 1)
                //== Client details
                name: {
                    required: true,
                    maxlength: 191
                },
                project_manager_id: {
                    required: true
                },
                //== Mailing address
                address: {
                    required: true,
                    maxlength: 191
                },
                city_id: {
                    required: true
                },
                district_id: {
                    required: true
                },
                investor: {
                    required: true
                }

            },

            //== Validation messages
            // messages: {
            //     'account_communication[]': {
            //         required: 'Bạn chưa nhập trường yêu cầu'
            //     },
            //     accept: {
            //         required: "You must accept the Terms and Conditions agreement!"
            //     }
            // },

            //== Display error
            invalidHandler: function(event, validator) {
                mApp.scrollTop();

                swal({
                    "title": "Thông báo",
                    "text": "Bạn chưa nhập 1 số thông tin cần thiết. Vui lòng kiểm tra lại!",
                    "type": "error",
                    "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                });
            },

            //== Submit valid form
            submitHandler: function (form) {

            }
        });
        var btn = formEl.find('[data-wizard-action="submit"]');

        btn.on('click', function(e) {
            e.preventDefault();

            if (validator.form()) {
                $.ajaxSetup({
                  headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
                });
                var data = new FormData(document.getElementById('project-create-form'));
                for (var item in listUrl ) {
                    data.append(item, listUrl[item]);
                }
                mApp.progress(btn);
                mApp.block(formEl);
                $.ajax({
                    url: '/administrator/info/project',
                    cache: false,
                    data: data,
                    type: "post",
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        console.log(response);
                        mApp.unprogress(btn);
                        mApp.unblock(formEl);

                        swal({
                            "title": "Thông báo!",
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Xác nhận!'
                        }).then(function(result) {
                            if (result.value) {
                                window.location.href = "/administrator/info/project";
                            }
                        });
                    },
                    error: function(data) {
                        mApp.unblock(formEl);
                        swal({
                            "title": "Thông báo!",
                            text: response.message,
                            type: 'warning',
                            showCancelButton: false,
                            confirmButtonText: 'Xác nhận!'
                        }).then(function(result) {
                            if (result.value) {
                                window.location.href = "/administrator/info/project";
                            }
                        });
                    }
                });
            }
        });

        /*var listAttachment = [];
        $("#attachment").dropzone({
            addRemoveLinks: true,
            url: "/administrator/info/project/attachment",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            paramName: "file",
            dictRemoveFile: "Xóa",
            dictRemoveFileConfirmation: "Bạn có chắc muốn xóa tệp này",
            removedfile: function(file, test) {
                var fileRemove = findObjectByKey(listAttachment, 'name', file.name, 'size', file.size);

                $.ajax({
                    type: 'DELETE',
                    url: '/administrator/info/project/attachment',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        id: fileRemove.id,
                        url: fileRemove.url,
                    },
                    sucess: function(data) {

                    }
                });
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            init: function() {
                this.on("success", function(file, response) {
                    listAttachment.push(response.data);
                });
            }
        });*/
        $('.summernote').summernote({
            callbacks: {
                onImageUpload: function(image) {
                    uploadImageProject(image[0], $(this).attr('id'));
                }
            },
            height: 150,
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['insert', ['picture']],
                ['table', ['table']],
                ['view', ['fullscreen']],
                ['help', ['help']]
            ]
        });

        var listUrl = {};
        $('input[type=file]').change(function(e){
            var inputName = $(this).attr('data-name');
            $(e.target.files).each(function( index, file ) {
                var data = new FormData();
                data.append("file", file);
                // $.ajaxSetup({
                //     headers: {
                //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                //     }
                // });
                $.ajax({
                    url: $('input[name=api_file]').val() + '/api/file',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data,
                    type: "post",
                    success: function(response) {
                        listUrl[inputName] = response.data.url;
                    },
                    error: function(data) {

                    }
                });
            });


            return false;

        });

    });



    function uploadImageProject(image, el) {

        var data = new FormData();
        data.append("image", image);
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
            url: '/administrator/upload',
            cache: false,
            contentType: false,
            processData: false,
            data: data,
            type: "post",
            success: function(response) {
                if (response.status) {
                    var image = $('<img width="100%">').attr('src', response.data);
                    $('#' + el).summernote("insertNode", image[0]);
                }

            },
            error: function(data) {

            }
        });
    }
</script>
@stop
