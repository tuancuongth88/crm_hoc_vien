@extends('administrator.app')
@section('title','Danh sách')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    {{-- <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Dự án
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div> --}}
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <div class="m-portlet m-portlet--mobile">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Dự án
                                    <small>
                                        danh sách dự án
                                    </small>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <!--begin: Search Form -->
                        <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                            <div class="row align-items-center">
                                <div class="col-xl-8 order-2 order-xl-1">
                                    <div class="form-group m-form__group row align-items-center">
                                        <div class="col-md-3">
                                            <div class="m-input-icon m-input-icon--left">
                                                <input type="text" class="form-control m-input" placeholder="Tên dự án..." id="name">
                                                <span class="m-input-icon__icon m-input-icon__icon--left">
                                                    <span>
                                                        <i class="la la-search"></i>
                                                    </span>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::select('city_id', [0 => 'Chọn tỉnh/thành'] + $listCity, null, ['class' => 'form-control m-select2', 'id' => 'city_id']) }}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::select('district_id', [0 => 'Chọn quận/huyện'], null, ['class' => 'form-control m-select2', 'id' => 'district_id']) }}
                                        </div>
                                        <div class="col-md-3">
                                            {{ Form::select('created_by', [0 => 'Chọn nhân viên'] + $listUser, null, ['class' => 'form-control m-select2', 'id' => 'created_by']) }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xl-4 order-1 order-xl-2 m--align-right">
                                    <a href="{{ route('project.create') }}" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                                        <span>
                                            <i class="la la-cart-plus"></i>
                                            <span>
                                                Thêm mới
                                            </span>
                                        </span>
                                    </a>
                                    <div class="m-separator m-separator--dashed d-xl-none"></div>
                                </div>
                            </div>
                        </div>
                        <!--end: Search Form -->
<!--begin: Datatable -->
                        <div class="m_datatable" id="ajax_data"></div>
                        <!--end: Datatable -->
                    </div>
                </div>
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
<meta name="csrf-token" content="{{ csrf_token() }}">
<input name="user_id" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}" id="user_id" type="hidden">
<script type="text/javascript">
    $(document).ready(function() {
        $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/administrator/info/project/list',
                        method: 'GET',
                    },
                },

                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
            },
            // column sorting
            sortable: false,

            pagination: true,

            toolbar: {
                // toolbar items
                items: {
                    // pagination
                    pagination: {
                        // page size select
                        pageSizeSelect: [10, 20, 30, 50, 100],
                    },
                },
            },

            search: {
                input: $('#name'),
            },

            rows: {
                // auto hide columns, if rows overflow
                autoHide: true,
            },

            // columns definition
            columns: [
            // {
            //     field: 'image_url',
            //     title: 'Ảnh',
            //     template: function(row) {
            //         return '<img width="150" src="' + row.image_url + '">';
            //     },
            // },
            {
                field: 'name',
                title: 'Tên dự án',
            }, {
                field: 'type',
                title: 'Loại dự án',
                // callback function support for column rendering
                template: function(row) {
                    if (row.type_project) {
                        return row.type_project.name;
                    }
                    return '';
                },
            }, {
                field: 'address',
                title: 'Địa điểm',
            }, {
                field: 'district',
                title: 'Quận/huyện',
                template: function(row) {
                    if (row.district) {
                        return row.district.name;
                    }
                    return '';
                },
            }, {
                field: 'province',
                title: 'Tỉnh/thành',
                template: function(row) {
                    if (row.province) {
                        return row.province.name;
                    }
                    return '';
                },
            }, {
                field: 'investor',
                title: 'Chủ đầu tư',

            }, {
                field: 'Tùy chọn',
                title: 'Tùy chọn',
                sortable: false,
                overflow: 'visible',
                template: function (row, index, datatable) {
                    var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                    var userId = $('#user_id').val();
                    var adminID = '{{ADMIN_ID}}';
                    if(row.created_by == userId || userId == adminID){
                        return '<a href="/administrator/info/project/' + row.id + '/edit" class="m-portlet__nav-link btn  m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pil edit_project_class" data-id="'+row.id+'" title="Sửa">\
                                  <i class="la la-edit"></i>\
                            </a>\
                            <button data-id="' + row.id + '" class="m-portlet__nav-link btn m-btn m-btn--hover-danger   m-btn--icon m-btn--icon-only m-btn--pill delete-project" title="Xóa">\
                                  <i class="la la-trash"></i>\
                            </button>\
                            ';
                    }
                },
            },
            {
                field: 'Keypoint',
                title: 'Giám đốc',
                width: 750,
                template: function(row) {
                    str = "<div class=\"rTable\">\n" +
                        "<div class=\"rTableRow\">\n" +
                        "<div class=\"rTableHead\"><strong>Tên </strong></div>\n" +
                        "<div class=\"rTableHead\">Số điện thoại</div>" +
                        "<div class=\"rTableHead\"><span style=\"font-weight: bold;\">Email</span></div>\n" +
                        "</div>";
                    $.each(row.has_info_manager, function (key, value) {
                        if(value.type == 2){
                           str += "<div class=\"rTableRow\">";
                           str +="<div class=\"rTableCell\">"+value.name+"</div>\n" +
                               "<div class=\"rTableCell\"><a href=\"tel:"+value.phone+"\">"+value.phone+"</a></div>\n" +
                               "<div class=\"rTableCell\">"+value.email+"</div>";
                           str +="</div>"
                        }
//                        str += 'Keypoint: ' + value.keypoint + '<br>';
                    });
                    str +='<div>';
                    return str;
                },
            },{
                field: 'Weakness',
                title: 'Admin',
                width: 750,
                template: function(row) {
                    str = "<div class=\"rTable\">\n" +
                        "<div class=\"rTableRow\">\n" +
                        "<div class=\"rTableHead\"><strong>Tên </strong></div>\n" +
                        "<div class=\"rTableHead\">Số điện thoại</div>" +
                        "<div class=\"rTableHead\"><span style=\"font-weight: bold;\">Email</span></div>\n" +
                        "</div>";
                    $.each(row.has_info_manager, function (key, value) {
                        if(value.type == 1){
                            str += "<div class=\"rTableRow\">";
                            str +="<div class=\"rTableCell\">"+value.name+"</div>\n" +
                                "<div class=\"rTableCell\"><a href=\"tel:"+value.phone+"\">"+value.phone+"</a></div>\n" +
                                "<div class=\"rTableCell\">"+value.email+"</div>";
                            str +="</div>"
                        }
//                        str += 'Keypoint: ' + value.keypoint + '<br>';
                    });
                    str +='<div>';
                    return str;
                },
            },
            ],
        });
        $('body').on('click', '.delete-project', function() {
            var id = $(this).attr('data-id');
            swal({
                title: 'Bạn có chắc?',
                text: "muốn xóa dự án này!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Có!'
            }).then(function(result) {
                if (result.value) {
                    if (id > 0) {
                        mApp.block($('#ajax_data'));
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: '/administrator/info/project/' + id,
                            type: 'DELETE',
                            contentType: 'application/json',
                            success: function(response) {
                                mApp.unblock($('#ajax_data'));
                                if(response.status == false){
                                    swal({
                                        "title": "Thông báo",
                                        "text": response.message,
                                        "type": "error",
                                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                    });
                                }else{
                                    swal({
                                        "title": "Thông báo",
                                        "text": response.message,
                                        "type": "success",
                                        "confirmButtonClass": "btn btn-secondary m-btn m-btn--wide"
                                    });
                                    if (response.status) {
                                        $('.m_datatable').mDatatable('reload');
                                    }
                                }

                            },
                            error: function(data) {
                                console.log(data);
                            }
                        });
                    }
                }
            });

        });
        $('.m-select2').select2();
        $( "#city_id" ).change(function() {
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'city_id');
            $('#district_id').empty().append('<option>Chọn quận/huyện</option>');
            var id = parseInt($(this).val());
            if (id > 0) {
                $.ajax({
                    url: '/administrator/info/project/city/' + id,
                    type: 'GET',
                    success: function(response) {
                        $.each(response, function (i, item) {
                            $('#district_id').append($('<option>', {
                                value: item.district_id,
                                text : item.name
                            }));
                        });
                    },
                    error: function(data) {

                    }
                });
            }

        });
        $('#district_id').on('change', function() {
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'district_id');
        });
        $('#created_by').on('change', function() {
            $('.m_datatable').mDatatable().search($(this).val().toLowerCase(), 'created_by');
        });

    });

</script>
@stop