@extends('administrator.app')
@section('title','Thư mục tài liệu')

@section('content')
@section('css')
<link href="{{ URL::asset('plugins/easyui/easyui.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('plugins/easyui/icon.css') }}" rel="stylesheet" type="text/css" />
@endsection

<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Thư mục tài liệu
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Quản lý tài liệu
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="row">
                            <div style="margin:20px 0;" class="col-md-6">
                                <a href="javascript:void(0)" class="btn btn-success m-btn m-btn--icon m-btn--icon-only" onclick="collapseAll()"><i class="fa fa-angle-double-up"></i></a>
                                <a href="javascript:void(0)" class="btn btn-info m-btn m-btn--icon m-btn--icon-only" onclick="expandAll()"><i class="fa fa-angle-double-down"></i></a>
                                <button data-toggle="modal" data-target="#modal-create-folder" class="btn btn-warning m-btn m-btn--icon m-btn--icon-only" title="Thêm thư mục"><i class="fa fa-folder"></i></button>
                                <a href="javascript:void(0)" class="btn btn-brand m-btn m-btn--icon m-btn--icon-only" onclick="goToUpload()" title="Chọn 1 thư mục để tải lên"><i class="fa fa-upload"></i></a>
                                <a href="javascript:void(0)" class="btn btn-primary m-btn m-btn--icon m-btn--icon-only" onclick="loadFile()" title="Chọn 1 thư mục để xem tài liệu"><i class="fa fa-eye"></i></a>
                                <a href="javascript:void(0)" class="btn btn-primary m-btn m-btn--icon m-btn--icon-only" onclick="edit()" title="Sửa"><i class="fa fa-pencil"></i></a>
                                <a href="javascript:void(0)" class="btn btn-primary m-btn m-btn--icon m-btn--icon-only" onclick="save()" title="Lưu"><i class="fa fa-save"></i></a>
                                <a href="javascript:void(0)" class="btn btn-primary m-btn m-btn--icon m-btn--icon-only" onclick="cancel()" title="Hủy"><i class="fa fa-undo"></i></a>
                            </div>
                            <div style="margin:20px 0;" class="col-md-6 text-right">
                                <div class="input-group">
                                    <input type="text" class="form-control" name="search" placeholder="Tìm kiếm tài liệu..." id="search-input">
                                    <div class="input-group-append">
                                        <button class="btn btn-secondary" type="button" id="search">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <table id="tg" class="easyui-treegrid col-md-3" title="Danh sách thư mục" style="width:400px;height:550px"
                                    data-options="
                                        iconCls: 'icon-ok',
                                        rownumbers: true,
                                        animate: true,
                                        collapsible: true,
                                        fitColumns: true,
                                        url: '/administrator/info/document/tree-data',
                                        method: 'get',
                                        idField: 'id',
                                        treeField: 'name',
                                        onContextMenu: onContextMenu
                                    ">
                                    <thead>
                                        <tr>
                                            <th data-options="field:'name',width:180,editor:'text'">Thư mục</th>
                                            <th data-options="field:'created_at',width:80">Ngày khởi tạo</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="col-md-7">
                                <div class="panel datagrid panel-htop">
                                    <div class="panel-header">
                                        Danh sách tài liệu
                                    </div>
                                    <div class="panel-body" id="list-file">
                                    </div>
                                </div>

                                <!--begin:: Widgets/Download Files-->
                                {{-- <div class="m-portlet m-portlet--full-height ">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h3 class="m-portlet__head-text">
                                                    Danh sách tài liệu
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="m-portlet__head-tools">
                                            <ul class="m-portlet__nav">
                                                <li class="m-portlet__nav-item m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover">
                                                    <a href="#" class="m-portlet__nav-link m-dropdown__toggle dropdown-toggle btn btn--sm m-btn--pill btn-secondary m-btn m-btn--label-brand">
                                                        Today
                                                    </a>
                                                    <div class="m-dropdown__wrapper">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">
                                                                                Quick Actions
                                                                            </span>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            <a href="" class="m-nav__link">
                                                                                <i class="m-nav__link-icon flaticon-share"></i>
                                                                                <span class="m-nav__link-text">
                                                                                    Activity
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            <a href="" class="m-nav__link">
                                                                                <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                                                <span class="m-nav__link-text">
                                                                                    Messages
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            <a href="" class="m-nav__link">
                                                                                <i class="m-nav__link-icon flaticon-info"></i>
                                                                                <span class="m-nav__link-text">
                                                                                    FAQ
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            <a href="" class="m-nav__link">
                                                                                <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                                <span class="m-nav__link-text">
                                                                                    Support
                                                                                </span>
                                                                            </a>
                                                                        </li>
                                                                        <li class="m-nav__separator m-nav__separator--fit"></li>
                                                                        <li class="m-nav__item">
                                                                            <a href="#" class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">
                                                                                Cancel
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="m-portlet__body" id="list-file">

                                    </div>
                                </div> --}}
                                <!--end:: Widgets/Download Files-->

                            </div>
                            <meta name="csrf-token" content="{{ csrf_token() }}">
                            <div id="mm" class="easyui-menu" style="width:120px;">
                                <div onclick="append()" data-options="iconCls:'icon-add'">Tạo thư mục con</div>
                                <div onclick="edit()" data-options="iconCls:'fa fa-pencil'">Đổi tên</div>
                                <div onclick="goToUpload()" data-options="iconCls:'fa fa-upload'">Tải lên</div>
                                <div onclick="loadFile()" data-options="iconCls:'fa fa-eye'">Xem chi tiết</div>
                                <div onclick="removeIt()" data-options="iconCls:'icon-remove'">Xóa</div>
                                <div class="menu-sep"></div>
                                <div onclick="collapse()" data-options="iconCls:'fa fa-angle-double-up'">Thu gọn</div>
                                <div onclick="expand()" data-options="iconCls:'fa fa-angle-double-down'">Xem thêm</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="modal-create-folder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Tạo thư mục
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <form id="create-folder" action="{{ route('document-category.store') }}">
            <div class="modal-body">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <input type="hidden" name="parent_id" id="parent_id">
                <div class="form-group">
                    <label for="recipient-name" class="form-control-label">
                        Tên thư mục:
                    </label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Hủy
                </button>
                <button type="submit" class="btn btn-primary">
                    Lưu
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->
@section('script')
<script src="{{ URL::asset('plugins/easyui/jquery.easyui.min.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('body').on( "click", "#search", function() {
            loadFile($('#search-input').val());
        });
        $("#search-input").keypress(function(e) {
            if(e.which == 13) {
                loadFile($('#search-input').val());
            }
        });
        $('#tg').treegrid({
            onDblClickCell: function (field, row) {
                loadFile();
            },
        });
    });
    function collapseAll(){
        $('#tg').treegrid('collapseAll');
    }
    function expandAll(){
        $('#tg').treegrid('expandAll');
    }
    function goToUpload(){
        var node = $('#tg').treegrid('getSelected');
        window.location.href = "/administrator/info/document/upload/" + node.id;
    }
    function loadFile($keyword){
        var search = '';
        if ($keyword) {
            search = ';name:' + $keyword;
        }
        var node = $('#tg').treegrid('getSelected');
        if (!node) {
            alert('Bạn chưa chọn thư mục');
            return false;
        }
        $('#list-file').empty();
        $.ajax({
            url: '/administrator/info/document?searchJoin=and&search=category_id:' + node.id + search,
            type : "get",
            success: function (result){
                $('#list-file').append(result);
            },
            error: function (data) {
                console.log("data", data);
            }
        });
    }

    function onContextMenu(e,row){
        if (row){
            e.preventDefault();
            $(this).treegrid('select', row.id);
            $('#mm').menu('show',{
                left: e.pageX,
                top: e.pageY
            });
        }
    }
    function append(){
        var node = $('#tg').treegrid('getSelected');
        $('#modal-create-folder').modal('show');
        $('#parent_id').val(node.id);
    }
    function removeIt(){
        var node = $('#tg').treegrid('getSelected');
        if (node){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/administrator/info/document-category/' + node.id,
                type: 'DELETE',
                success: function(response) {
                    if (response.status) {
                        $('#tg').treegrid('remove', node.id);
                    }
                    alert(response.message);
                },
                error: function(data) {
                    console.log(data);
                }
            });
        }
    }
    function collapse(){
        var node = $('#tg').treegrid('getSelected');
        if (node){
            $('#tg').treegrid('collapse', node.id);
        }
    }
    function expand(){
        var node = $('#tg').treegrid('getSelected');
        if (node){
            $('#tg').treegrid('expand', node.id);
        }
    }
    var editingId;
    function edit(){
        if (editingId != undefined){
            $('#tg').treegrid('select', editingId);
            return;
        }
        var row = $('#tg').treegrid('getSelected');
        if (row){
            editingId = row.id;
            $('#tg').treegrid('beginEdit', editingId);
        }
    }
    function save(){
        if (editingId != undefined){
            var t = $('#tg');
            t.treegrid('endEdit', editingId);
            var row = $('#tg').treegrid('getSelected');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '/administrator/info/document-category/' + row.id,
                type: 'PUT',
                data: row,
                success: function(response) {
                    console.log("response", response);
                },
                error: function(data) {
                    console.log(data);
                }
            });
            editingId = undefined;
        }
    }
    function cancel(){
        if (editingId != undefined){
            $('#tg').treegrid('cancelEdit', editingId);
            editingId = undefined;
        }
    }
    $("#create-folder").validate({
        highlight: function (input) {
            $(input).parents('.form-group').addClass('has-error').addClass('has-error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-group').removeClass('has-error').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error.addClass('form-message').addClass('validation-error'));
        },
        rules: {
            name: "required",
        },
        submitHandler: function(form){
            event.preventDefault();
            var data = $('#create-folder').serialize();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: $('#create-folder').attr('action'),
                type: 'POST',
                data: data,
                success: function(response) {
                    var node = $('#tg').treegrid('getSelected');
                    if (node) {
                        $('#tg').treegrid('append',{
                            parent: node.id,
                            data: [response.data]
                        });
                    } else {
                        $('#tg').treegrid('append',{
                            data: [response.data]
                        });
                    }
                    $('#name').val();
                    $('#modal-create-folder').modal('hide');
                },
                error: function(data) {
                    console.log(data);
                }
            });
        },
    });
</script>
@stop
@stop