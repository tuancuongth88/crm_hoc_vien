@extends('administrator.app')
@section('title','Thư mục')

@section('content')
@section('css')
{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> --}}
<link href="{{ URL::asset('plugins/jquery-file-upload/jquery.fileupload.css') }}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .fade {
        opacity: 100!important;
    }

    .fade-0 {
        opacity: 0!important;
    }
</style>
@stop
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Thư mục
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 20px">
                <a href="{{ route('document-category.index') }}" class="btn btn-success">Danh sách thư mục</a>
            </div>
            <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                <div class="m-portlet m-portlet--tab">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <span class="m-portlet__head-icon m--hide">
                                    <i class="la la-gear"></i>
                                </span>
                                <h3 class="m-portlet__head-text">
                                    Upload tài liệu
                                </h3>
                            </div>
                        </div>
                    </div>
                        <div class="m-portlet__body">
                            <blockquote>
                                <strong>Lưu ý:</strong>
                                <p>Định dạng cho phép: jpg, jpeg, doc, docx, xls, xlsx, ppt, pptx, png, pdf.<br>
                                Dung lượng cho phép: dưới 30 Mb.<br>
                                Vui lòng chỉ tải các định dạng tệp trên.Ngoài ra hệ thống không hỗ trợ</p>
                            </blockquote><br>
                            <form id="fileupload" method="POST" action="/administrator/info/document" enctype="multipart/form-data">
                                <meta id="token" name="csrf-token" content="{{ csrf_token() }}">
                                <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
                                <div class="row fileupload-buttonbar">

                                    <div class="col-lg-12">
                                        <input type="hidden" name="category_id" id="category_id" value="{{ Request::segment(5) }}">
                                        <!-- The fileinput-button span is used to style the file input field as button -->
                                        <span class="btn btn-success fileinput-button">
                                            <i class="glyphicon glyphicon-plus"></i>
                                            <i class="fa fa-plus-circle"></i><span> Thêm tệp...</span>
                                            <input type="file" name="files[]" multiple>
                                        </span>
                                        <button type="submit" class="btn btn-primary start">
                                            <i class="glyphicon glyphicon-upload"></i>
                                            <i class="fa fa-upload"></i><span> Tải lên</span>
                                        </button>
                                        {{-- <button type="reset" class="btn btn-warning cancel">
                                            <i class="glyphicon glyphicon-ban-circle"></i>
                                            <span>Hủy</span>
                                        </button>
                                        <button type="button" class="btn btn-danger delete">
                                            <i class="glyphicon glyphicon-trash"></i>
                                            <span>Xóa</span>
                                        </button>
                                        <input type="checkbox" class="toggle"> --}}
                                        <!-- The global file processing state -->
                                        <span class="fileupload-process"></span>
                                    </div>
                                    <!-- The global progress state -->
                                    <div class="col-lg-12 fileupload-progress fade-0">
                                        <!-- The global progress bar -->
                                        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                                            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                                        </div>
                                        <!-- The extended global progress state -->
                                        <div class="progress-extended">&nbsp;</div>
                                    </div>
                                </div>
                                <!-- The table listing the files available for upload/download -->
                                <table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
                            </form>
                        </div>
                        <div class="m-portlet__foot">
                            <div class="m-form__actions">
                                <a class="btn btn-secondary" href="{{ route('document-category.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                </div>
                <!--end::Portlet-->
            </div>
            <!--end::Form-->
        </div>
    </div>
</div>
@section('script')

<!-- The template to display files available for upload -->
<script id="template-upload" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-upload fade">
        <td>
            <span class="preview"></span>
        </td>
        <td>
            <p class="name">{%=file.name%}</p>
            <strong class="error text-danger"></strong>
        </td>
        <td>
            <p class="size">Processing...</p>
            <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="progress-bar progress-bar-success" style="width:0%;"></div></div>
        </td>
        <td>
            {% if (!i && !o.options.autoUpload) { %}
                <button class="btn btn-primary start" disabled>
                    <i class="glyphicon glyphicon-upload"></i>
                    <span>Tải lên</span>
                </button>
            {% } %}
            {% if (!i) { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Hủy</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery>
                        {% if (file.extendsion == 'jpg') { %}
                            <img src="{%=file.url%}" width="75">
                        {% } %}
                        {% if (file.extendsion == 'xlsx') { %}
                        <span><i class="fa fa-file-excel-o"></i></span>
                        {% } %}
                        {% if (file.extendsion == 'docx') { %}
                        <span><i class="fa fa-file-word-o"></i></span>
                        {% } %}
                    </a>
                {% } %}
            </span>
        </td>
        <td>
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.url?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger">Lỗi<br></span> Không thể tải lên</div>
                {{-- <div><span class="label label-danger">Lỗi<br></span> {%=file.error%}</div> --}}
            {% } %}
        </td>
        <td>
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td>
            {% if (file.deleteUrl) { %}
                {{-- <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span>Xóa</span>
                </button>
                <input type="checkbox" name="delete" value="1" class="toggle"> --}}
            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span>Hủy</span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}
</script>

<script src="{{ URL::asset('plugins/jquery-ui-widget/jquery.ui.widget.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/javascript-template.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('js/javascript-load-image.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/jquery-file-upload/jquery.fileupload.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/jquery-file-upload/jquery.fileupload-process.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/jquery-file-upload/jquery.fileupload-image.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('plugins/jquery-file-upload/jquery.fileupload-ui.js') }}" type="text/javascript"></script>
<script>
    $( document ).ready(function() {
        var token = $('#token').attr('content');
        $('#fileupload').fileupload({
            url: '/administrator/info/document',
            formData: {
                '_token': token,
                'category_id': $('#category_id').val()
            },
        });
    });
</script>
@stop
@stop
