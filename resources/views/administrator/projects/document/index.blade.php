{{-- <table>
    <thead>
        <th width="30%">Tên tài liệu</th>
        <th width="15%">Định dạng</th>
        <th width="20%">Dung lượng</th>
        <th width="20%">Tùy chọn</th>
    </thead>
    @foreach ($data as $file)
        <tr>
            <td>
                <i class="fa fa-paperclip"></i> <a href="{{ $file->url }}">{{ $file->name }}</a>
            </td>
            <td>
                @if (in_array($file->extendsion, ['jpg', 'jpeg']))
                    <i class="fa fa-file-image-o"></i>
                @endif
                @if (in_array($file->extendsion, ['docx', 'doc']))
                    <i class="fa fa-file-word-o"></i>
                @endif
                @if (in_array($file->extendsion, ['xls', 'xlsx']))
                    <i class="fa fa-file-excel-o"></i>
                @endif
                {{ $file->extendsion }}
            </td>
            <td><i class="fa fa-database"></i>{{ formatSizeUnits($file->size) }} KB</td>
            <td>
                <a href="http://view.officeapps.live.com/op/view.aspx?src={!! url($file->url)  !!}" class="btn btn-success m-btn m-btn--icon m-btn--icon-only" target="_blank">
                    <i class="fa fa-eye"></i>
                </a>
                <a href="{{ $file->url }}" download="{{ $file->name }}" class="btn btn-info m-btn m-btn--icon m-btn--icon-only">
                    <i class="fa fa-download"></i>
                </a>
                {{ Form::open(array('method'=>'DELETE', 'route' => array('document.destroy', $file->id), 'style' => 'display: inline-block;')) }}
                <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                    <i class="fa fa-close"></i>
                </button>
                {{ Form::close() }}
            </td>
        </tr>
    @endforeach
</table> --}}
<!--begin::m-widget4-->
<div class="m-widget4">
    @foreach ($data as $file)
    <div class="m-widget4__item">
        <div class="m-widget4__img m-widget4__img--icon">
            @if (in_array($file->extendsion, ['jpg', 'jpeg']))
                <img src="{{ URL::asset('assets/app/media/img/files/jpg.svg') }}" alt="">
            @endif
            @if (in_array($file->extendsion, ['docx', 'doc']))
                <img src="{{ URL::asset('assets/app/media/img/files/doc.svg') }}" alt="">
            @endif
            @if (in_array($file->extendsion, ['pdf']))
                <img src="{{ URL::asset('assets/app/media/img/files/pdf.svg') }}" alt="">
            @endif
            @if (in_array($file->extendsion, ['xls', 'xlsx']))
                <img src="{{ URL::asset('assets/app/media/img/files/xls.png') }}" alt="">
            @endif

        </div>
        <div class="m-widget4__info">
            <span class="m-widget4__text">
                <a href="{{ $file->url }}">{{ $file->name }}</a> (<i class="fa fa-database"></i>{{ formatSizeUnits($file->size) }} KB)
            </span>
        </div>
        <div class="m-widget4__info">
            <span class="m-widget4__text">
                <a href="http://view.officeapps.live.com/op/view.aspx?src={!! url($file->url)  !!}" class="m-widget4__icon" target="_blank">
                    <i class="fa fa-eye"></i>
                </a>
            </span>
        </div>
        <div class="m-widget4__info">
            <a href="{{ $file->url }}" download="{{ $file->name }}" class="m-widget4__icon">
                <i class="la la-download"></i>
            </a>
        </div>
        <div class="m-widget4__ext">
            <span class="m-widget4__text">
                {{ Form::open(array('method'=>'DELETE', 'route' => array('document.destroy', $file->id), 'style' => 'display: inline-block;')) }}
                <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="m-widget4__icon">
                    <i class="fa fa-close"></i>
                </button>
                {{ Form::close() }}
            </span>
        </div>
    </div>
    @endforeach
</div>
<!--end::Widget 9-->