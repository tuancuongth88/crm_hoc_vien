@extends('administrator.app')
@section('title','HỆ THỐNG BÁO CÁO CÔNG VIỆC - HẢI PHÁT LAND')

@section('content')
    <div class="m-content" style="overflow: hidden">
        <div class="row">
            @if (\Auth::user()->can('is-manager'))
                <div class="col-md-4">
                    <!--begin:: Widgets/New Users-->
                    <div class="m-portlet m-portlet--full-height ">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Nhân viên
                                    </h3>
                                </div>
                            </div>
                            <div class="m-portlet__head-tools">
                                <select class="form-control m-select2" id="search-user" name="param">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                        <div class="m-portlet__body" id="list-user">
                            <div class="tab-content">
                                <!--begin::Widget 14-->
                                <div class="m-widget4">
                                @foreach ($listUser as $user)
                                    <!--begin::Widget 14 Item-->
                                        <div class="m-widget4__item">
                                            <div class="m-widget4__img m-widget4__img--pic">
                                                <img src="{{ @$user->avatar }}" alt="">
                                            </div>
                                            <div class="m-widget4__info">
                                            <span class="m-widget4__title">
                                                {{ @$user->fullname }}
                                            </span>
                                                <br>
                                                <span class="m-widget4__sub">
                                                {{ @$user->position->name }}
                                            </span>
                                            </div>
                                            <div class="m-widget4__ext">
                                                <a href="{{ route('report.statistic'). '?user_id=' .$user->id }}"
                                                   class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary">
                                                    Thống kê
                                                </a>
                                            </div>
                                        </div>
                                        <!--end::Widget 14 Item-->
                                    @endforeach
                                </div>
                                <!--end::Widget 14-->
                                <label for="" class="m--margin-top-30">
                                    {{ $listUser->links() }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <!--end:: Widgets/New Users-->
                </div>

            @else
                <div class="col-md-4">
                    <div class="m-portlet m-portlet--bordered-semi m-portlet--full-height ">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption" style="border-bottom:1px solid #eee">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">
                                        Lịch chăm khách trong ngày
                                    </h3>
                                </div>
                            </div>

                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget4" id="calendar_call_customer">

                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-md-8">
                <!--begin::Portlet-->

                <div class="m-portlet m-portlet--tabs">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-tabs m-tabs-line m-tabs-line--primary m-tabs-line--2x" role="tablist">
                                <li class="nav-item m-tabs__item m--margin-right-10">
                                    <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_tabs_6_1"
                                       role="tab">
                                        <i class="la la-cog"></i>
                                        Lịch sử chăm sóc hôm nay
                                    </a>
                                </li>
                                @if (\Auth::user()->can('is-manager'))
                                    <li class="nav-item m-tabs__item m--margin-right-10">
                                        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_2"
                                           role="tab">
                                            <i class="la la-briefcase"></i>
                                            Công việc cần thực hiện
                                        </a>
                                    </li>
                                @endif
                                <li class="nav-item m-tabs__item m--margin-right-10">
                                    <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_tabs_6_3"
                                       role="tab">
                                        <i class="la la-cog"></i>
                                        Khách hàng sắp bị thu hồi
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <div class="m-portlet__body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="m_tabs_6_1" role="tabpanel">
                                <div class="m_datatable" id="ajax_data"></div>
                            </div>
                            <div class="tab-pane" id="m_tabs_6_2" role="tabpanel">
                                <div class="m_datatable" id="list_history_call_onday"></div>
                            </div>
                            <div class="tab-pane" id="m_tabs_6_3" role="tabpanel">
                                <div class="m_datatable" id="customer_upcoming_recover"></div>
                            </div>
                        </div>
                    </div>
                    <!--end::Form-->
                </div>


            </div>

        </div>
        <!--Begin::Section-->
        <div class="m-portlet">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-xl-4">
                        <!--begin:: Widgets/Stats2-1 -->
                        <div class="m-widget1">
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            Tổng số dự án
                                        </h3>
                                        <span class="m-widget1__desc">
                                        Số dự án bạn đã cập nhật
                                    </span>
                                    </div>
                                    <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-brand">
                                        {{ $totalProject }}
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            Tổng số khách hàng
                                        </h3>
                                        <span class="m-widget1__desc">
                                        Data khách hàng bạn có
                                    </span>
                                    </div>
                                    <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-danger">
                                        {{ $totalCustomer }}
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            Khách hàng tiềm năng
                                        </h3>
                                        <span class="m-widget1__desc">
                                        Khách hàng tiềm năng bạn có
                                    </span>
                                    </div>
                                    <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-success">
                                        {{ $totalCustomerLevel1 }}
                                    </span>
                                    </div>
                                </div>
                            </div>
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            Khách hàng mục tiêu
                                        </h3>
                                        <span class="m-widget1__desc">
                                        Khách hàng mục tiêu bạn có
                                    </span>
                                    </div>
                                    <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-success">
                                        {{ $totalCustomerLevel2 }}
                                    </span>
                                    </div>
                                </div>
                            </div>                            
                            <div class="m-widget1__item">
                                <div class="row m-row--no-padding align-items-center">
                                    <div class="col">
                                        <h3 class="m-widget1__title">
                                            Khách hàng nét
                                        </h3>
                                        <span class="m-widget1__desc">
                                        Khách hàng nét bạn có
                                    </span>
                                    </div>
                                    <div class="col m--align-right">
                                    <span class="m-widget1__number m--font-success">
                                        {{ $totalCustomerLevel3 }}
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end:: Widgets/Stats2-1 -->
                    </div>
                    <div class="col-xl-4">
                        <!--begin:: Widgets/Daily Sales-->
                        <div class="m-widget14">
                            <div class="m-widget14__header m--margin-bottom-30">
                                <h3 class="m-widget14__title">
                                    Biến động data khách hàng
                                </h3>
                                <span class="m-widget14__desc">
                                Thay đổi số khách hàng trong tuần qua của bạn
                            </span>
                            </div>
                            <div class="m-widget14__chart" style="height:120px;">
                                <canvas id="m_chart_daily_sales"></canvas>
                            </div>
                        </div>
                        <!--end:: Widgets/Daily Sales-->
                    </div>
                    <div class="col-xl-4">
                        <!--begin:: Widgets/Profit Share-->
                        <div class="m-widget14">
                            <div class="m-widget14__header">
                                <h3 class="m-widget14__title">
                                    Cơ cấu khách hàng tiềm năng
                                </h3>
                                <span class="m-widget14__desc">
                                Tỷ lệ khách hàng tiềm năng phân bố theo dự án
                            </span>
                            </div>
                            <div class="row  align-items-center">
                                <div class="col">
                                    <div id="m_chart_profit_share" class="m-widget14__chart" style="height: 160px">
                                        <div class="m-widget14__stat">
                                            100
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="m-widget14__legends" id="percer_customer_potential">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end:: Widgets/Profit Share-->
                    </div>
                </div>
            </div>
        </div>
        <!--End::Section-->

        <div class="row">
            <div class="col-xl-12">
                <div class="m-portlet m-portlet--full-height ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    THỐNG KÊ CÔNG VIỆC ĐÃ THỰC HIỆN
                                </h3>
                            </div>
                        </div>
                        <div class="m-portlet__head-tools">
                            <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm"
                                role="tablist">
                                {{-- <li class="nav-item m-tabs__item">
                                    <div class='input-group pull-right' id='date-select'>
                                        <input type='text' class="form-control m-input" readonly  placeholder="Chọn ngày"/>
                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <i class="la la-calendar-check-o"></i>
                                            </span>
                                        </div>
                                    </div>
                                </li> --}}
                                <li class="nav-item m-tabs__item">
                                    <button class="nav-link m-tabs__link active" id="day" data-toggle="tab" role="tab">
                                        Trong ngày
                                    </button>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <button class="nav-link m-tabs__link" id="week" data-toggle="tab" role="tab">
                                        Trong tuần
                                    </button>
                                </li>
                                <li class="nav-item m-tabs__item">
                                    <button class="nav-link m-tabs__link" id="month" data-toggle="tab" role="tab">
                                        Trong tháng
                                    </button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--begin:: Widgets/Stats-->
        <!-- <div class="m-portlet ">
            <div class="m-portlet__body  m-portlet__body--no-padding">
                <div class="row m-row--no-padding m-row--col-separator-xl">
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    Công việc thường xuyên
                                </h4>
                                <br>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-brand" role="progressbar" style="width: 50%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Mức độ hoàn thành
                                </span>
                                <span class="m-widget24__number">
                                    78%
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    Cập nhật thông tin khách hàng
                                </h4>
                                <br>
                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-info" role="progressbar" style="width: 84%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Mức độ hoàn thành
                                </span>
                                <span class="m-widget24__number">
                                    84%
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    Cập nhật khách hàng tiềm năng
                                </h4>
                                <br>

                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-danger" role="progressbar" style="width: 69%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Mức độ hoàn thành
                                </span>
                                <span class="m-widget24__number">
                                    69%
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6 col-xl-3">
                        <div class="m-widget24">
                            <div class="m-widget24__item">
                                <h4 class="m-widget24__title">
                                    Đào tạo - Văn Hóa - Thương Hiệu
                                </h4>
                                <br>

                                <div class="m--space-10"></div>
                                <div class="progress m-progress--sm">
                                    <div class="progress-bar m--bg-success" role="progressbar" style="width: 90%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="m-widget24__change">
                                    Mức độ hoàn thành
                                </span>
                                <span class="m-widget24__number">
                                    90%
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <!--end:: Widgets/Stats-->
        <any id="fill-data-jobs"></any>

        <input type="hidden" name="user_id" id="user_id" value="{{ Request::input('user_id') }}">

        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/chartist@0.11.0/dist/chartist.min.js"></script>
        <script src="https://www.gstatic.com/firebasejs/5.9.0/firebase.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                var str = '';
                if (parseInt($('#user_id').val()) > 0) {
                    str = '?user_id=' + $('#user_id').val();
                }
                $.ajax({
                    url: '/administrator/job/report-statistic/day' + str,
                    type: 'GET',
                    success: function (response) {
                        $('#fill-data-jobs').append(response);
                    },
                    error: function (data) {
                        console.log(data);
                    }
                });
                $("#day").click(function () {
                    $('#fill-data-jobs').empty();
                    $.ajax({
                        url: '/administrator/job/report-statistic/day' + str,
                        type: 'GET',
                        success: function (response) {
                            $('#fill-data-jobs').append(response);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                });
                $("#week").click(function () {
                    $('#fill-data-jobs').empty();
                    $.ajax({
                        url: '/administrator/job/report-statistic/week' + str,
                        type: 'GET',
                        success: function (response) {
                            $('#fill-data-jobs').append(response);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                });
                $("#month").click(function () {
                    $('#fill-data-jobs').empty();
                    $.ajax({
                        url: '/administrator/job/report-statistic/month' + str,
                        type: 'GET',
                        success: function (response) {
                            $('#fill-data-jobs').append(response);
                        },
                        error: function (data) {
                            console.log(data);
                        }
                    });
                });

                getCustomerByWeek();
                getCutomerByPotential();
                listCustomerTimeCallBackOnday();

                function getCustomerByWeek() {
                    $.ajax({
                        url: '/administrator/job/report/customer-by-week' + str,
                        type: 'GET',
                        dataType: 'json',
                        success: function (results) {

                            var objChar = {};
                            objChar.labels = [];
                            objChar.dataNomal = [];
                            objChar.dataPotential = [];
                            if (results.length > 0) {
                                $.each(results, function (i, item) {
                                    objChar.labels.push(item.day);
                                    objChar.dataNomal.push(item.totalCustomer)
                                    objChar.dataPotential.push(item.customer_potential)

                                })
                            } else {
                                objChar.labels.push('0');
                                objChar.dataNomal.push('0')
                                objChar.dataPotential.push('0')
                            }

                            var chartContainer = $('#m_chart_daily_sales');

                            if (chartContainer.length == 0) {
                                return;
                            }

                            var chartData = {
                                labels: objChar.labels,
                                datasets: [{
                                    //label: 'Dataset 1',
                                    backgroundColor: '#34bfa3',
                                    data: objChar.dataPotential
                                }, {
                                    //label: 'Dataset 2',
                                    backgroundColor: '#f3f3fb',
                                    data: objChar.dataNomal
                                }]
                            };

                            var chart = new Chart(chartContainer, {
                                type: 'bar',
                                data: chartData,
                                options: {
                                    title: {
                                        display: false,
                                    },
                                    tooltips: {
                                        intersect: false,
                                        mode: 'nearest',
                                        xPadding: 10,
                                        yPadding: 10,
                                        caretPadding: 10
                                    },
                                    legend: {
                                        display: false
                                    },
                                    responsive: true,
                                    maintainAspectRatio: false,
                                    barRadius: 4,
                                    scales: {
                                        xAxes: [{
                                            display: false,
                                            gridLines: false,
                                            stacked: true
                                        }],
                                        yAxes: [{
                                            display: false,
                                            stacked: true,
                                            gridLines: false
                                        }]
                                    },
                                    layout: {
                                        padding: {
                                            left: 0,
                                            right: 0,
                                            top: 0,
                                            bottom: 0
                                        }
                                    }
                                }
                            });
                        }
                    })
                }

                function getCutomerByPotential() {
                    $.ajax({
                        url: '/administrator/job/report/customer-potential-by-project' + str,
                        type: 'GET',
                        dataType: 'json',
                        success: function (result) {
                            var seriesChar = [];
                            if (result.length > 0) {
                                $.each(result, function (i, item) {
                                    var color = getRandomColor();
                                    var element = {};
                                    element.value = item.percent;
                                    element.className = 'custom';
                                    element.meta = {color: color};
                                    seriesChar.push(element);

                                    // appent percer to text
                                    $('#percer_customer_potential').append('<div class="m-widget14__legend">\n' +
                                        '<span class="m-widget14__legend-bullet" style="background-color: ' + color + '"></span>\n' +
                                        '<span class="m-widget14__legend-text">\n' +
                                        item.percent + '%' + item.project_name +
                                        '</span>\n' +
                                        '</div>');
                                })
                            } else {
                                var color = getRandomColor();
                                var element = {};
                                element.value = 100;
                                element.className = 'custom';
                                element.meta = {color: color};
                                seriesChar.push(element);
                            }
                            if ($('#m_chart_profit_share').length == 0) {
                                return;
                            }

                            var chart = new Chartist.Pie('#m_chart_profit_share', {
                                series: seriesChar,
                                labels: [1, 2, 3]
                            }, {
                                donut: true,
                                donutWidth: 17,
                                showLabel: false
                            });

                            chart.on('draw', function (data) {
                                if (data.type === 'slice') {
                                    // Get the total path length in order to use for dash array animation
                                    var pathLength = data.element._node.getTotalLength();

                                    // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                                    data.element.attr({
                                        'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                                    });

                                    // Create animation definition while also assigning an ID to the animation for later sync usage
                                    var animationDefinition = {
                                        'stroke-dashoffset': {
                                            id: 'anim' + data.index,
                                            dur: 1000,
                                            from: -pathLength + 'px',
                                            to: '0px',
                                            easing: Chartist.Svg.Easing.easeOutQuint,
                                            // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                                            fill: 'freeze',
                                            'stroke': data.meta.color
                                        }
                                    };

                                    // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                                    if (data.index !== 0) {
                                        animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                                    }

                                    // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

                                    data.element.attr({
                                        'stroke-dashoffset': -pathLength + 'px',
                                        'stroke': data.meta.color
                                    });

                                    // We can't use guided mode as the animations need to rely on setting begin manually
                                    // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                                    data.element.animate(animationDefinition, false);
                                }
                            });

                            // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
                            chart.on('created', function () {
                                if (window.__anim21278907124) {
                                    clearTimeout(window.__anim21278907124);
                                    window.__anim21278907124 = null;
                                }
                                //window.__anim21278907124 = setTimeout(chart.update.bind(chart), 15000);
                            });
                        }
                    });

                }

                function listCustomerTimeCallBackOnday() {
                    $.ajax({
                        url: '/administrator/customer/notification/call_back' + str,
                        type: 'GET',
                        dataType: 'json',
                        success: function (result) {
                            if (result.data.length > 0) {
                                $.each(result.data, function (i, item) {
//                            console.log(123);
                                    $('#calendar_call_customer').append('<div class="m-widget4__item">\n' +
                                        '                                <div class="m-widget4__info">\n' +
                                        '                                <span class="m-widget4__title">\n' + item.fullname +
                                        '                            </span>\n' +
                                        '                            <br>\n' +
                                        '                            <span class="m-widget4__sub">\n' + item.note +
                                        '                            </span>\n' +
                                        '                            </div>\n' +
                                        '                            <span class="m-widget4__ext">\n' +
                                        '                                <span class="m-widget4__number m--font-brand">\n' + item.time_call +
                                        '                            </span>\n' +
                                        '                            </span>\n' +
                                        '                            </div>');
                                })
                            }
                        }
                    });
                }

                function getRandomColor() {
                    var letters = '0123456789ABCDEF';
                    var color = '#';
                    for (var i = 0; i < 6; i++) {
                        color += letters[Math.floor(Math.random() * 16)];
                    }
                    return color;
                }

                $('#date-select').daterangepicker({
                    buttonClasses: 'm-btn btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary',

                    ranges: {
                        'Hôm nay': [moment(), moment()],
                        'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Tuần này': [moment().startOf('isoWeek'), moment().endOf('isoWeek')],
                        'Tuần trước': [moment().subtract(1, 'weeks').startOf('isoWeek'), moment().subtract(1, 'weeks').endOf('isoWeek')],
                        '7 Ngày trước': [moment().subtract(6, 'days'), moment()],
                        '30 Ngày trước': [moment().subtract(29, 'days'), moment()],
                        'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                        'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    locale: {
                        applyLabel: 'Chọn',
                        cancelLabel: "Hủy",
                        fromLabel: 'Từ ngày',
                        toLabel: 'Đến ngày',
                        customRangeLabel: 'Tùy chọn',
                        daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
                        monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                        firstDay: 1
                    }
                }, function (start, end, label) {
                    $('#date-select .form-control').val(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
                });
                $('#ajax_data').mDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                method: 'GET',
                                url: '/administrator/customer/history/today/1' + str,
                                map: function (raw) {
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: false,
                        footer: false
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    toolbar: {
                        // toolbar items
                        items: {
                            // pagination
                            pagination: {
                                // page size select
                                pageSizeSelect: [10, 20, 30, 50, 100],
                            },
                        },
                    },

                    search: {
                        input: $('#generalSearch'),
                    },
                    rows: {
                        // auto hide columns, if rows overflow
                        autoHide: true,
                    },
                    columns: [
                        {
                            field: 'user_id',
                            title: 'Nhân viên',
                            template: function (row) {
                                if (row.agent) {
                                    return row.agent.fullname;
                                }
                                return '';
                            },
                        }
                        , {
                            field: 'customer_id',
                            title: 'Khách hàng',
                            template: function (row) {
                                if (row.customer) {
                                    return row.customer.fullname;
                                }
                                return '';
                            },
                        }
                        , {
                            field: 'phone',
                            title: 'Điện thoại',
                        }, {
                            field: 'note',
                            title: 'Ghi chú',
                        }, {
                            field: 'time_call_back',
                            title: 'Thời gian gọi lại',
                        }],
                });

                $('#list_history_call_onday').mDatatable({
                    // datasource definition
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                // sample GET method
                                method: 'GET',
                                url: '/administrator/customer/history/today/2' + str,
                                map: function (raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: false,
                        footer: false
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    toolbar: {
                        // toolbar items
                        items: {
                            // pagination
                            pagination: {
                                // page size select
                                pageSizeSelect: [10, 20, 30, 50, 100],
                            },
                        },
                    },

                    search: {
                        input: $('#generalSearch'),
                    },
                    rows: {
                        // auto hide columns, if rows overflow
                        autoHide: true,
                    },
                    // columns definition
                    columns: [
                        {
                            field: 'user_id',
                            title: 'Nhân viên',
                            template: function (row) {
                                if (row.agent) {
                                    return row.agent.fullname;
                                }
                                return '';
                            },
                        }
                        , {
                            field: 'customer_id',
                            title: 'Khách hàng',
                            template: function (row) {
                                if (row.customer) {
                                    return row.customer.fullname;
                                }
                                return '';
                            },
                        }
                        , {
                            field: 'phone',
                            title: 'Điện thoại',
                        }, {
                            field: 'note',
                            title: 'Ghi chú',
                        }, {
                            field: 'time_call_back',
                            title: 'Thời gian gọi lại',
                        }],
                });

                $('#customer_upcoming_recover').mDatatable({
                    // datasource definition
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                // sample GET method
                                method: 'GET',
                                url: '/administrator/customer/upcoming/recover' + str,
                                map: function (raw) {
                                    // sample data mapping
                                    var dataSet = raw;
                                    if (typeof raw.data !== 'undefined') {
                                        dataSet = raw.data;
                                    }
                                    return dataSet;
                                },
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                    },

                    // layout definition
                    layout: {
                        scroll: false,
                        footer: false
                    },

                    // column sorting
                    sortable: true,

                    pagination: true,

                    toolbar: {
                        // toolbar items
                        items: {
                            // pagination
                            pagination: {
                                // page size select
                                pageSizeSelect: [10, 20, 30, 50, 100],
                            },
                        },
                    },

                    search: {
                        input: $('#generalSearch'),
                    },
                    rows: {
                        // auto hide columns, if rows overflow
                        autoHide: true,
                    },
                    // columns definition
                    columns: [
                        {
                            field: 'name_user',
                            title: 'Nhân viên',
                        }
                        , {
                            field: 'fullname',
                            title: 'Khách hàng',
                        }
                        , {
                            field: 'phone',
                            title: 'Điện thoại',
                        }, {
                            field: 'email',
                            title: 'Email',
                        }, {
                            field: 'history_care_at_str',
                            title: 'Thời gian chăm sóc',
                        }],
                });


                $("#search-user").select2({
                    placeholder: "Tìm kiếm..",
                    allowClear: true,
                    width: '240',
                    language: {
                        // You can find all of the options in the language files provided in the
                        // build. They all must be functions that return the string that should be
                        // displayed.
                        inputTooShort: function () {
                            return "Nhập ít nhất 1 ký tự...";
                        }
                    },
                    ajax: {
                        url: "/administrator/system/user/search",
                        dataType: 'json',
                        delay: 250,
                        data: function (params) {
                            return {
                                keyword: params.term, // search term
                                page: params.page
                            };
                        },
                        processResults: function (data, params) {
                            // parse the results into the format expected by Select2
                            // since we are using custom formatting functions we do not need to
                            // alter the remote JSON data, except to indicate that infinite
                            // scrolling can be used
                            params.page = params.page || 1;

                            return {
                                results: data,
                                pagination: {
                                    more: (params.page * 30) < data.total_count
                                }
                            };
                        },
                        cache: true
                    },
                    escapeMarkup: function (markup) {
                        return markup;
                    }, // let our custom formatter work
                    minimumInputLength: 1,
                    templateResult: formatRepo, // omitted for brevity, see the source of this page
                    templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
                });

                function formatRepo(repo) {
                    if (repo.loading) return repo.text;
                    var markup = "<div class='select2-result-repository clearfix'>" +
                        "<div class='select2-result-repository__meta'>" +
                        "<div class='select2-result-repository__title'><a class='test' href='/administrator/job/dashboard?user_id=" + repo.id + "'>" + repo.fullname + "</a></div>";
                    if (repo.description) {
                        markup += "<div class='select2-result-repository__description'>" + repo.fullname + "</div>";
                    }
                    markup += "<div class='select2-result-repository__statistics'>" +
                        "<div class='select2-result-repository__forks'><i class='fa fa-flash'></i> " + repo.email + "</div>" +
                        "<div class='select2-result-repository__watchers'><i class='fa fa-eye'></i> " + repo.phone + "</div>" +
                        "</div>" +
                        "</div></div>";
                    return markup;
                }

                function formatRepoSelection(repo) {
                    return repo.fullname || repo.text;
                }

                // $('#list-user').empty();
                // $.ajax({
                //     url: '/administrator/system/user/be-managed',
                //     type : "get",
                //     success: function (result){
                //         $('#list-user').append(result);
                //     },
                //     error: function (data) {
                //         console.log("data", data);
                //     }
                // });
                $('#search-user').on('select2:select', function (e) {
                    var user = $('#search-user').select2('data');

                    window.location.replace('/administrator/job/dashboard?user_id=' + user[0].id);
                });
            });
            if ('serviceWorker' in navigator) {
                // Register a service worker hosted at the root of the
                // site using the default scope.
                navigator.serviceWorker.register('/js/sw.js').then(function(handleNotification) {
                    console.log('Service worker registration succeeded:', handleNotification);
                }, /*catch*/ function(error) {
                    console.log('Service worker registration failed:', error);

                });
            } else {
                console.log('Service workers are not supported.');
            }
            // Initialize Firebase
            var config = {
                apiKey: "AIzaSyBE4HsdHgrM9ZLOO_5kxEgtcloiYGpZZ-4",
                authDomain: "crm-land.firebaseapp.com",
                databaseURL: "https://crm-land.firebaseio.com",
                projectId: "crm-land",
                storageBucket: "crm-land.appspot.com",
                messagingSenderId: "505543396837"
            };
            firebase.initializeApp(config);

            function handleNotification() {
                var database = firebase.database().ref('/accounts');
                database.orderByChild("last_send_time").on("child_changed", function(data) {
                    notifyMe(data.val().fullname, "Vừa nhắn tin");
                });
            }

            function notifyMe(title, content) {
                if (Notification.permission !== "granted")
                    Notification.requestPermission();
                else {
                    var notification = new Notification(title, {
                        icon: location.origin+"/logo.png",
                        body: content,
                    });

                    notification.onclick = function () {
                        window.open(location.hostname+"/system-app/chat");
                    };

                }
            }
        </script>
        <!--end:: Widgets/Stats-->
        <any id="fill-data-jobs"></any>
    </div>
@stop