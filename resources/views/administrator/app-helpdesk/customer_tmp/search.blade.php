{{ Form::open(array('route' => 'app.customer.tmp.search', 'method' => 'GET')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-3 col-sm-6 mb-3 mb-md-0">
        <label>Tên khách hàng</label>
        <input type="text" name="fullname"class="form-control" value="{{ @$request['fullname'] }}" placeholder="Họ tên, Số điện thoại" />
    </div>
    <div class="col-md-2 col-sm-3 mb-2 mb-md-0">
        <label>Mã số</label>
        <input type="text" name="maso" id="maso" class="form-control" value="{{ @$request['maso'] }}" placeholder="Mã số" />
    </div>
    <div class="col-md-2 col-sm-3 mb-2 mb-md-0">
        <label>Dự án</label>
        <input type="text" name="du_an" id="du_an" class="form-control" value="{{ @$request['du_an'] }}" placeholder="Tên dự án" />
    </div>
    <div class="col-md-2 col-sm-3 mb-2 mb-md-0">
        <label>Mã căn</label>
        <input type="text" name="ma_can" id="ma_can" class="form-control" value="{{ @$request['ma_can'] }}" placeholder="Mã căn" />
    </div>
    <div class="col-md-3 col-sm-6 mb-3 mb-md-0">
        <label>Trạng thái nhận</label>
        {{ Form::select('status', ['' => 'Tất cả', '0' => 'Chưa nhận', '1' => 'Đã nhận'], @$request['status'], ['class' => 'form-control']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Tìm kiếm" class="btn btn-primary form-control" />
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Hủy tìm</label>
        <a href="{{ route('app.customer.tmp') }}" class="btn btn-danger form-control">Hủy tìm</a>
    </div>
</div>
</form>
