@extends('administrator.app')
@section('title','Danh sách khách hàng')
<style>
    .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url("{{ URL::asset('assets/app/media/img/icons/fox-run.gif') }}") 50% 50% no-repeat rgb(249,249,249);
        opacity: 0.1;
    }
</style>
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Danh sách khách hàng nhận mã khuyến mại
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-content">
                @include('administrator.app-helpdesk.customer_tmp.search')
                <!--begin::Section-->

                    <div class="col-xs-12" style="margin-bottom: 20px">
                        {{ Form::open(array('enctype' => 'multipart/form-data', 'method'=>'POST', 'route' => 'app.customer.tmp.import', 'id' => 'import-form')) }}

                        <div class="custom-file col-md-3 m--padding-top-5">
                            <input type="file" class="custom-file-input" id="customFile" name="import_file">
                            <label class="custom-file-label" for="customFile">
                                Chọn file import
                            </label>
                        </div>
                        <button type="submit" class="btn btn-success" id="import_file"><i class="fa fa-file-excel-o"></i> Import</button>
                        {{ Form::close() }}<br>
                        <a href="/template_excel/customer_promotion_tmp.xlsx" style="font-weight: bold; font-size: 17px; color: #000;">Mẫu File Import</a>
                    </div>
                    @if(session('list-fail'))
                        <div class="alert alert-danger alert-notification">
                            @foreach (session('list-fail') as $element)
                                {{ $element }}<br>
                            @endforeach
                        </div>
                    @endif
                    @include('administrator.errors.info-search')
                    <div class="m-section">
                        @include('administrator.errors.messages')
                        <div class="m-section__content">
                            <table class="table m-table bg-white m-table--border-danger m-table--head-bg-success small">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Tên khách hàng
                                    </th>
                                    <th>
                                        Số điện thoại
                                    </th>
                                    <th>
                                        Mã căn
                                    </th>
                                    <th>
                                        Dự án
                                    </th>
                                    <th>
                                        Ngày vào hợp đồng
                                    </th>
                                    <th>
                                        Mã Số
                                    </th>
                                    <th>
                                        Số lượng mã
                                    </th>
                                    <th>
                                        Mã phụ
                                    </th>
                                    <th>
                                        Người phát
                                    </th>
                                    <th>
                                        Đã nhận
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $value)
                                    <tr>
                                        <th scope="row">
                                            {{ $key + 1 }}
                                        </th>
                                        <td>
                                            {{ $value->fullname }}
                                        </td>
                                        <td>
                                            {{ $value->phone }}
                                        </td>
                                        <td>
                                            {{ $value->ma_can }}
                                        </td>
                                        <td>
                                            {{ $value->du_an }}
                                        </td>
                                        <td>
                                            {{ $value->ngay_vao_hop_dong }}
                                        </td>
                                        <td style="color: #ff0000;">
                                            <b>
                                            {{ $value->maso }}
                                            </b>
                                        </td>
                                        <td>
                                            {{ $value->so_ma }}
                                        </td>
                                        <td style="color: #ff0000;">
                                            <b>{{ $value->ma_phu }}</b>
                                        </td>
                                        <td>
                                            @if($value->update_by > 0 && $value->status == ONE)
                                                {{ \App\Models\Users\User::find($value->update_by)->fullname  }}
                                            @endif
                                        </td>
                                        <td>
                                            {{ Form::open(array('method'=>'POST', 'route' => array('app.customer.tmp.update', $value->id), 'style' => 'display: inline-block;')) }}
                                            <span class="m-switch">
                                                <label>
                                                    <input type="checkbox" @if($value->status == ONE) checked="checked" @endif class="update-status" data-id="{{ $value->id }}" data-fullname="{{ $value->fullname }}" name="status">
                                                    <span></span>
                                                </label>
                                            </span>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if(isset($request))
                            {{ $data->appends(@$request->all())->links()  }}
                        @else
                            {{ $data->links()  }}
                        @endif
                        Tổng số {{ $data->total() }} bản ghi

                    </div>
                    <!--end::Section-->
                </div>
            </div>
        </div>
    </div>
    <div class="loader" style="display: none;"></div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        $(document).ready(function () {
            $('#m_modal_5').modal('toggle');

            $('.update-status').click(function () {
                var id = $(this).attr('data-id');
                var status = $(this).is(":checked");
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '/administrator/system-app/list-customer-promotion/update/'+id,
                    type: 'post',
                    data: {id: id, status: status},
                    beforeSend: function(){
                        $('.loader').css('display', 'block');
                    },
                    success: function(data) {
                        $('.loader').css('display', 'none');
                        swal({
                            title: "Thông báo!",
                            text: data.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Xác nhận!'
                        })
                        .then(function () {
                            location.reload();
                        });
                    },
                    error: function(){
                        $('.loader').css('display', 'none');
                        swal({
                            title: "Thông báo!",
                            text: "Có lỗi xảy ra",
                            type: 'error',
                            showCancelButton: false,
                            confirmButtonText: 'Xác nhận!'
                        }).then(function () {
                                location.reload();
                        });
                    },
                    timeout: 10000
                });
            });

        });
    </script>
@stop