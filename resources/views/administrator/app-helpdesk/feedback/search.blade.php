{{ Form::open(array('route' => 'feedback.search', 'method' => 'GET', 'id' =>'search_form')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
        <div class="col-md-4 col-sm-4 mb-3 mb-md-0">
            <label>Dự án</label>
            {{ Form::select('project_id', ['' => 'Chọn dự án'] +  getOptionByModel(\App\Models\Projects\ProjectManager::class, 'id', 'name') , @$input['project_id'], ['class' => 'form-control']) }}
        </div>
        <div class="col-md-4 col-sm-3 mb-3 mb-md-0">
            <label>Chọn nhân viên kinh doanh</label>
            {{ Form::select('sale_id', ['' => 'Chọn nhân viên'] + $listSale, @$input['sale_id'], ['class' => 'form-control', 'id' => 'user_id_select']) }}
        </div>
        <div class="col-md-4 col-sm-4 mb-3 mb-md-0">
            <label>Điểm đánh giá</label>
                {{ Form::select('rank_sale', ['' => 'Chọn điểm'] + $listRank, @$input['rank_sale'], ['class' => 'form-control']) }}
        </div>
        <div class="col-md-2 col-sm-2 mb-3 mb-md-0">
            <input type="submit" value="Tìm kiếm" class="btn btn-primary form-control" />
        </div>
        <div class="col-md-2 col-sm-2 mb-3 mb-md-0">
            <a href="{{ route('feedback.index') }}" class="btn btn-danger form-control">Hủy tìm</a>
        </div>
</div>
</form>
