@extends('administrator.app')
@section('title','Quản lý Phản hồi khách hàng')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Danh sách phản hồi của khách hàng về Sale
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Danh sách đánh giá
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-content">
                @include('administrator.app-helpdesk.feedback.search')
                <!--begin::Section-->

                @include('administrator.errors.info-search')
                <div class="m-section">
                        @include('administrator.errors.messages')
                        <div class="m-section__content">
                            <table class="table m-table m-table--head-bg-warning">
                                <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Tên Sale
                                        </th>
                                        <th>
                                            Dự án
                                        </th>
                                        <th>
                                            Điểm đánh giá
                                        </th>
                                        <th>
                                            Nội dung
                                        </th>
                                        <th>
                                            Thời gian
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key => $value)
                                    <tr>
                                        <th scope="row">
                                            {{ $key + 1 }}
                                        </th>
                                        <td>
                                            {{ @$value->sale->fullname }}
                                        </td>
                                        <td>
                                            {{ @$value->transaction->project->name }}
                                        </td>
                                        <td>
                                            @if($value->rank_sale < 3)
                                                <span style="color:#ff0303;">{{ $value->rank_sale }}</span>
                                            @else
                                                <span style="color:#14a719;">{{ $value->rank_sale }}</span>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $value->note }}
                                        </td>
                                        <td>
                                            {{ $value->created_at }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $data->links() }}
                        Tổng số {{ $data->total() }} bản ghi

                </div>
                <!--end::Section-->
            </div>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function () {
            $('#user_id_select').select2({width: '100%'});
        });
    </script>
@stop