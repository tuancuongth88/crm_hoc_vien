@extends('administrator.app')
@section('title','Quản lý sản phẩm ký gửi của khách hàng')

<style>
    #product-detail .kqchitiet .gia-title {
        font-size: 14px;
        float: none;
    }
    #product-detail .kqchitiet .gia-title strong {
        font-size: 14px;
        color: #319C00;
    }
    .m-portlet .m-portlet__head{
        height: 30px !important;
        color: #fff;
        font-weight: bold;
    }
    .product-detail{
        padding-top: 20px;
    }
</style>
<!-- Add fancyBox -->


@section('content')
    <div class="m-content">
        <div class="m-portlet">
                <div id="product-detail" class="m-portlet__body">
                    <div class="pm-title">
                        <h1 itemprop="name">
                            {{ $data->title }}
                        </h1>
                    </div>
                    <div class="kqchitiet">
                    <span class="diadiem-title mar-right-15">
                        <b>Địa chỉ:</b>
                        {{ $data->address }}
                        - {{ $data->district->name }} - {{ $data->city->name }}
                    </span>
                        <br>
                        <span style="display: inline-block;"><span class="gia-title mar-right-15">
                        <b> Giá:</b>
                        <strong >
                        {{ number_format($data->price).' ' .$data->priceType->name }}
                        </strong>
                    </span>
                    <span class="gia-title">
                        <b> Diện tích:</b>
                        <strong style="color: #319C00;"> {{ $data->area. $data->areaType->name }}</strong>
                    </span>
                    </span>
                    </div>
                    <div class="clear"></div>
                    <div class="pm-mota">
                        <h3>Thông tin mô tả</h3>
                    </div>
                    <div class="pm-content">
                        <div class="pm-desc">
                            {{ $data->description }}
                        </div>
                    </div>
                    <div class="row">
                        @foreach($listImages as $item)
                            <div class="col-sm-3">
                                <a data-fancybox="gallery" href="{{ $item }}" class="fancybox" title="Sample title">
                                    <img src="{{ $item }}" class="img-thumbnail" />
                                </a>
                            </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <span class=m-portlet__head-text">
                                                Đặc điểm bất động sản
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Loại tin rao
                                        </div>
                                        <div class="col-md-9">
                                            {{ @$data->landType->name }}
                                        </div>
                                        <div class="col-md-3">
                                            Diện tích
                                        </div>
                                        <div class="col-md-9">
                                            {{ @$data->area.$data->areaType->name }}
                                        </div>
                                        <div class="col-md-3">
                                            Địa chỉ
                                        </div>
                                        <div class="col-md-9">
                                            {{ @$data->address }}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="m-portlet m-portlet--brand m-portlet--head-solid-bg m-portlet--bordered" data-portlet="true" id="m_portlet_tools_1">
                                <div class="m-portlet__head">
                                    <div class="m-portlet__head-caption">
                                        <div class="m-portlet__head-title">
                                            <span class=m-portlet__head-text">
                                                Thông tin liên hệ
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__body">
                                    <div class="row">
                                        <div class="col-md-3">
                                            Tên liên lạc
                                        </div>
                                        <div class="col-md-9">
                                            {{ @$data->fullname }}
                                        </div>
                                        <div class="col-md-3">
                                            Số điện thoại
                                        </div>
                                        <div class="col-md-9">
                                            {{ @$data->phone }}
                                        </div>
                                        <div class="col-md-3">
                                            Email
                                        </div>
                                        <div class="col-md-9">
                                            {{ @$data->email }}
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".fancybox").fancybox();
        });
    </script>
@stop