@extends('administrator.app')
@section('title','Quản lý sản phẩm ký gửi của khách hàng')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Danh sách sản phẩm ký gửi khách hàng
                            </h3>
                        </div>
                    </div>
                </div>
                @include('administrator.app-helpdesk.consignment.search')
                <!--begin::Section-->
                    @include('administrator.errors.info-search')
                    <div class="m-section">
                        @include('administrator.errors.messages')
                        <a href="{{ route('consignments.export', $_REQUEST) }}" class="btn btn-success btn-sm m-btn	m-btn m-btn--icon m-btn--pill">
                            <span>
                                <i class="fa fa-file-excel-o"></i>
                                <span>
                                    Xuất File
                                </span>
                            </span>
                        </a>
                        <div class="m-section__content">
                            <table class="table tab table-bordered m-table m-table--head-bg-success bg-white small ">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Tài khoản
                                    </th>
                                    <th>
                                        Tiêu đề
                                    </th>
                                    <th>
                                        Hình thức
                                    </th>
                                    <th>
                                        Giá
                                    </th>
                                    <th>
                                        diện tích
                                    </th>
                                    <th>
                                        Tên đầy đủ
                                    </th>
                                    <th>
                                        Số điện thoại
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Ngày tiếp nhận
                                    </th>
                                    <th>
                                        Ngày hoàn thành
                                    </th>
                                    <th>
                                        Trạng thái
                                    </th>
                                    <th>
                                        Action
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $value)
                                    <tr>
                                        <th scope="row">
                                            {{ $key + 1 }}
                                        </th>
                                        <td>
                                            <a href="{{ route('account.edit', @$value->account_id) }}"> {{ @$value->account->fullname }}</a>
                                        </td>
                                        <td>
                                            {{ $value->title }}
                                        </td>
                                        <td>
                                            {{ @$value->productType->name }}
                                        </td>
                                        <td>
                                        {{ $value->price.' '. @$value->priceType->name }}
                                        </td>
                                        <td>
                                            {{ $value->area .' '. @$value->areaType->name }}
                                        </td>
                                        <td>
                                            {{ $value->fullname }}
                                        </td>
                                        <td>
                                            {{ $value->phone }}
                                        </td>
                                        <td>
                                            {{ $value->email }}
                                        </td>
                                        <td>
                                            {{ $value->created_at }}
                                        </td>
                                        <td>
                                            @if($value->status == \App\Models\Customers\Consignments::STATUS_HOAN_THANH)
                                                {{ $value->updated_at }}
                                            @endif
                                        </td>
                                        <td>
                                            @if($value->status == \App\Models\Customers\Consignments::STATUS_TIEP_NHAN)
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--up small" data-dropdown-toggle="click">
                                                    <a href="#" class="m-dropdown__toggle btn btn-info dropdown-toggle">
                                                        Chờ tiếp nhận
                                                    </a>
                                                    <div class="m-dropdown__wrapper small">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">
                                                                                Thay đổi trạng thái
                                                                            </span>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_TIEP_NHAN), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-info" style="width: 115px;">
                                                                                Chờ tiếp nhận
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_DA_NHAN), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-primary" style="width: 115px;">
                                                                                Đã chuyển Sale
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_HOAN_THANH), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-success" style="width: 115px;">
                                                                                Hoàn thành
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_HUY), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-danger" style="width: 115px;">
                                                                                Hủy
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($value->status == \App\Models\Customers\Consignments::STATUS_DA_NHAN)
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--up small" data-dropdown-toggle="click">
                                                    <a href="#" class="m-dropdown__toggle btn btn-primary dropdown-toggle">
                                                        Đã chuyển Sale
                                                    </a>
                                                    <div class="m-dropdown__wrapper small">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">
                                                                                Thay đổi trạng thái
                                                                            </span>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_TIEP_NHAN), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-info" style="width: 115px;">
                                                                                Chờ tiếp nhận
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_DA_NHAN), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-primary" style="width: 115px;">
                                                                                Đã chuyển Sale
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_HOAN_THANH), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-success" style="width: 115px;">
                                                                                Hoàn thành
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_HUY), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-danger" style="width: 115px;">
                                                                                Hủy
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($value->status == \App\Models\Customers\Consignments::STATUS_HOAN_THANH)
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--up small" data-dropdown-toggle="click">
                                                    <a href="#" class="m-dropdown__toggle btn btn btn-success dropdown-toggle">
                                                        Hoàn thành
                                                    </a>
                                                    <div class="m-dropdown__wrapper small">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">
                                                                                Thay đổi trạng thái
                                                                            </span>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_TIEP_NHAN), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-info" style="width: 115px;">
                                                                                Chờ tiếp nhận
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_DA_NHAN), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-primary" style="width: 115px;">
                                                                                Đã chuyển Sale
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_HOAN_THANH), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-success" style="width: 115px;">
                                                                                Hoàn thành
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_HUY), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-danger" style="width: 115px;">
                                                                                Hủy
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @else($value->status == \App\Models\Customers\Consignments::STATUS_HUY)
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left m-dropdown--up small" data-dropdown-toggle="click">
                                                    <a href="#" class="m-dropdown__toggle btn btn btn btn-danger dropdown-toggle">
                                                        Bị hủy
                                                    </a>
                                                    <div class="m-dropdown__wrapper small">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">
                                                                                Thay đổi trạng thái
                                                                            </span>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_TIEP_NHAN), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-info" style="width: 115px;">
                                                                                Chờ tiếp nhận
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_DA_NHAN), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-primary" style="width: 115px;">
                                                                                Đã chuyển NVKD
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_HOAN_THANH), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-success" style="width: 115px;">
                                                                                Hoàn thành
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('consignments.update-status', $value->id, App\Models\Customers\Consignments::STATUS_HUY), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-danger" style="width: 115px;">
                                                                                Hủy
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                        <td class="d-print-inline">
                                            <div class="dropdown ">
                                                <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">
                                                    <i class="la la-ellipsis-h"></i>
                                                </a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="{{ route('consignments.show', $value->id) }}">
                                                        <i class="fa fa-info-circle"></i>
                                                        Chi tiết
                                                    </a>
                                                    {{ Form::open(array('method'=>'DELETE', 'route' => array('consignments.delete', $value->id))) }}
                                                    <button class="dropdown-item" onclick="return confirm('Bạn có chắc chắn muốn xóa?');" href="{{ route('consignments.show', $value->id) }}">
                                                        <i class="fa fa-close"></i>
                                                        Xóa
                                                    </button>
                                                    {{ Form::close() }}
                                                </div>
                                            </div>
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $data->links() }}
                        Tổng số {{ $data->total() }} bản ghi
                    </div>

                    <!--end::Section-->
            </div>

        </div>
    </div>
    <script>
        $(document).ready(function () {

        });
    </script>
@stop