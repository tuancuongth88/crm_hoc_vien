@extends('administrator.app')
@section('title','Gửi tin nhắn chăm sóc khách hàng')
<link href="{{ URL::asset('css/chat.css') }}" rel="stylesheet" type="text/css" />
@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">

        <div class="messaging">
        <div class="inbox_msg">
            <div class="inbox_people">
                <div class="headind_srch">
                    <div class="recent_heading">
                        <h4>Tìm kiếm</h4>
                    </div>
                    <div class="srch_bar">
                        <div class="stylish-input-group">
                            <input type="text" class="search-bar" id="search-user"  placeholder="Nhập khách hàng" >
                        </div>
                    </div>
                </div>
                <div class="inbox_chat scroll">
                </div>
            </div>
            <div class="mesgs">
                <div class="msg_history">
                </div>
                <div class="type_msg">
                    <div class="input_msg_write">
                        <input type="text" class="write_msg" placeholder="Viết tin nhắn" />
                        <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <input type="hidden" name="admin_full_name" id="admin_full_name" value="{{ \Auth::user()->fullname }}">
    <script src="https://www.gstatic.com/firebasejs/5.8.3/firebase.js"></script>
    <script src="{{ URL::asset('js/chat-firebase.js') }}" ></script>
    <script>
        $(document).ready(function() {
            $("#search-user").keyup(function () {
                var input = $('#search-user').val();
                console.log(input);
                searchAccount(input);
            });

        });
        function searchAccount(str) {
            var data = {search : str};
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: '{{ route('chat.search.account') }}',
                type: 'POST',
                data: data,
                dataType: "html",
                success: function (data) {
                    $('.inbox_chat').html('');
                    $('.inbox_chat').html(data);
                }
            })
        }

    </script>
@stop