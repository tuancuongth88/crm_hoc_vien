@foreach($listAccount as $value)
    <div class="chat_list " data-phone="{{ $value->phone }}" data-id="{{ $value->id }}" data-img="{{ $value->avatar? $value->avatar : '/no-avatar.ico'   }}" style="cursor: pointer;">
        <div class="chat_people" >
            <div class="chat_img">
                <div class="m-card-profile__pic">
                    <div class="m-card-profile__pic-wrapper" >
                        @if($value->avatar)
                            <img src="{{ $value->avatar }}" alt="" id="avatar"/>
                        @else
                            <img src="/no-avatar.ico" alt="" id="avatar"/>
                        @endif
                    </div>
                </div>
            </div>
            <div class="chat_ib">
                <h5>{{ $value->fullname }} <span class="chat_date"></span></h5>
                <p> {{ $value->phone }}</p>
            </div>
        </div>
    </div>
@endforeach