<div class="m-section">
{{ Form::open(array('route' => 'list-booking.search', 'method' => 'GET', 'id' =>'search_form')  ) }}
    <div class="card-body pb-0">
        <div class="d-inline-block clearfix mr-3">
            <div class="form-group has-success">
                <label>Tên, số điện thoại hoặc email</label><br>
                {{ Form::text('search', @$input['search'], ['class' => 'form-control']) }}

            </div>
        </div>
        <div class="d-inline-block clearfix mr-3">
            <div class="form-group has-success">
                <label>Trạng thái</label><br>
                {{ Form::select('status', ['' => '--Trạng thái--'] + $listStatus, @$input['status'], ['class' => 'form-control']) }}
            </div>
        </div>
        <div class="d-inline-block clearfix mr-3">
            <div class="d-inline-block clearfix">
                <button type="submit" class="btn btn-info m-btn m-btn--icon m-btn--wide">
                    <i class="flaticon-search"></i> Tìm kiếm
                </button>
                <a href="{{ route('list-booking.list') }}" class="btn btn-warning">
                    <i class="flaticon-exclamation-1"></i> Hủy lọc</a>
            </div>
        </div>

    </div>
{{ Form::close() }}
</div>