@extends('administrator.app')
@section('title','Đặt lịch xem nhà mẫu')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- END: Subheader -->
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Danh sách khách hàng đặt lịch xem nhà mẫu
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-content">
                @include('administrator.app-helpdesk.booking-view.search')
                <!--begin::Section-->
                    @include('administrator.errors.info-search')
                    <div class="m-section">
                        @include('administrator.errors.messages')
                        <a href="{{ route('list-booking.export', $_REQUEST) }}" class="btn btn-success btn-sm m-btn	m-btn m-btn--icon m-btn--pill">
                            <span>
                                <i class="fa fa-file-excel-o"></i>
                                <span>
                                    Xuất File
                                </span>
                            </span>
                        </a>
                        <div class="m-section__content">
                            <table class="table m-table m-table--head-bg-warning">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Tài khoản
                                    </th>
                                    <th>
                                        Tên người đặt lịch
                                    </th>
                                    <th>
                                        Điện thoại
                                    </th>
                                    <th>
                                        Email
                                    </th>
                                    <th>
                                        Thời gian hẹn xem
                                    </th>
                                    <th>
                                        Nội dung
                                    </th>
                                    <th>
                                        Trạng thái
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $value)
                                    <tr>
                                        <th scope="row">
                                            {{ $key + 1 }}
                                        </th>
                                        <td>
                                            <a href="{{ route('account.edit', $value->account_id) }}">{{ @$value->account->fullname }}</a>
                                        </td>
                                        <td>
                                            {{ @$value->fullname }}
                                        </td>
                                        <td>
                                            {{ $value->phone }}
                                        </td>
                                        <td>
                                            {{ $value->email }}
                                        </td>
                                        <td>
                                            {{ $value->time_view }}
                                        </td>
                                        <td>
                                            {{ $value->note }}
                                        </td>
                                        <td>
                                            @if($value->status == \App\Models\Customers\BookingViewHouse::STATUS_NEW)
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left" data-dropdown-toggle="click">
                                                    <a href="#" class="m-dropdown__toggle btn btn-info dropdown-toggle">
                                                        Mới đăng
                                                    </a>
                                                    <div class="m-dropdown__wrapper">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">
                                                                                Thay đổi trạng thái
                                                                            </span>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('list-booking.update', $value->id, App\Models\Customers\BookingViewHouse::STATUS_NEW), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-info" style="width: 115px;">
                                                                                Mới đăng
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('list-booking.update', $value->id, App\Models\Customers\BookingViewHouse::STATUS_APPROVE), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-success" style="width: 115px;">
                                                                                Hoàn thành
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('list-booking.update', $value->id, App\Models\Customers\BookingViewHouse::STATUS_REJECT), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-danger" style="width: 115px;">
                                                                                Hủy
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($value->status == \App\Models\Customers\BookingViewHouse::STATUS_APPROVE)
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left" data-dropdown-toggle="click">
                                                    <a href="#" class="m-dropdown__toggle btn btn-success dropdown-toggle">
                                                        Hoàn thành
                                                    </a>
                                                    <div class="m-dropdown__wrapper">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">
                                                                                Thay đổi trạng thái
                                                                            </span>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('list-booking.update', $value->id, App\Models\Customers\BookingViewHouse::STATUS_NEW), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-info" style="width: 115px;">
                                                                                Mới đăng
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('list-booking.update', $value->id, App\Models\Customers\BookingViewHouse::STATUS_APPROVE), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-success" style="width: 115px;">
                                                                                Hoàn thành
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('list-booking.update', $value->id, App\Models\Customers\BookingViewHouse::STATUS_REJECT), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-danger" style="width: 115px;">
                                                                                Hủy
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @elseif($value->status == \App\Models\Customers\BookingViewHouse::STATUS_REJECT)
                                                <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-left" data-dropdown-toggle="click">
                                                    <a href="#" class="m-dropdown__toggle btn btn-danger dropdown-toggle">
                                                        Hủy
                                                    </a>
                                                    <div class="m-dropdown__wrapper">
                                                        <span class="m-dropdown__arrow m-dropdown__arrow--left"></span>
                                                        <div class="m-dropdown__inner">
                                                            <div class="m-dropdown__body">
                                                                <div class="m-dropdown__content">
                                                                    <ul class="m-nav">
                                                                        <li class="m-nav__section m-nav__section--first">
                                                                            <span class="m-nav__section-text">
                                                                                Thay đổi trạng thái
                                                                            </span>
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('list-booking.update', $value->id, App\Models\Customers\BookingViewHouse::STATUS_NEW), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-info" style="width: 115px;">
                                                                                Mới đăng
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('list-booking.update', $value->id, App\Models\Customers\BookingViewHouse::STATUS_APPROVE), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-success" style="width: 115px;">
                                                                                Hoàn thành
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                        <li class="m-nav__item">
                                                                            {{ Form::open(array('method'=>'post', 'route' => array('list-booking.update', $value->id, App\Models\Customers\BookingViewHouse::STATUS_REJECT), 'style' => 'display: inline-block;')) }}
                                                                            <button class="btn btn-danger" style="width: 115px;">
                                                                                Hủy
                                                                            </button>
                                                                            {{ Form::close() }}
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $data->links() }}
                        Tổng số {{ $data->total() }} bản ghi

                    </div>
                    <!--end::Section-->
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#user_id_select').select2({width: '100%'});
        });
    </script>
@stop