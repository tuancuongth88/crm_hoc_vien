@extends('administrator.app')
@section('title','Thêm mới tài khoản khách hàng')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Thêm mới tài khoản khách hàng
                    </h3>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Section-->
            <div class="row">
                <div class="col-xl-3 col-lg-4">
                    <div class="m-portlet m-portlet--full-height  ">
                        <div class="m-portlet__body">
                            <div class="m-card-profile">
                                <div class="m-card-profile__title m--hide">
                                    Your Profile
                                </div>
                                <div class="m-card-profile__pic">
                                    <div class="m-card-profile__pic-wrapper">
                                        <img src="/no-avatar.ico" alt="" id="avatar"/>
                                    </div>
                                </div>
                                <div class="m-card-profile__details">
                                    <b>Chọn ảnh đại diện</b>
                                </div>
                            </div>
                            <div class="m-portlet__body-separator"></div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-8">
                    <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-tools">
                                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                                    <li class="nav-item m-tabs__item">
                                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                            <i class="flaticon-share m--hide"></i>
                                           Thông tin tài khoản
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        @include('administrator.errors.errors-validate')
                        <div class="tab-content">
                            @include('administrator.errors.messages')
                            <div class="tab-pane active" id="m_user_profile_tab_1">
                                {{ Form::open(array('route' => array('account.store'), 'method' => 'POST', 'class' => 'm-form m-form--fit m-form--label-align-right', 'enctype' => 'multipart/form-data')) }}
                                <input type="file" id="fileavatar" value="" style="display: none;">
                                <input name="avatar" id="input-avatar" value="" type="hidden">
                                <div class="m-portlet__body">

                                    <div class="form-group m-form__group row">
                                        <div class="col-10 ml-auto">
                                            <h3 class="m-form__section">
                                                1. Thông tin cơ bản
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Họ tên <span style="color: red;">(*)</span>
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" name="fullname" type="text" placeholder="Nhập họ tên" value="{{ old('fullname') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Giới tính <span style="color: red;">(*)</span>
                                        </label>
                                        <div class="col-7">
                                            <select class="form-control m-input" id="gender" name="gender" required>
                                                <option value="1">
                                                    Nam
                                                </option>
                                                <option value="2">
                                                    Nữ
                                                </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Điện thoại <span style="color: red;">(*)</span>
                                        </label>
                                        <div class="col-7">
                                            <input name="phone" class="form-control m-input" placeholder="Nhập số điện thoại đăng nhập" type="text" value="{{ old('phone') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Email
                                        </label>
                                        <div class="col-7">
                                            <input class="form-control m-input" placeholder="Nhập email" type="email" name="email" value="{{ old('email') }}" >
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Ngày sinh
                                        </label>
                                        <div class="col-7">
                                            <input type="text" class="form-control" id="birthday" placeholder="Chọn ngày" name="birthday" value="{{ old('birthday') }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Địa chỉ
                                        </label>
                                        <div class="col-7">
                                            <input type="text" class="form-control" id="address" placeholder="Nhập địa chỉ" name="address" value="{{ old('address') }}">
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group row">
                                        <label for="example-text-input" class="col-2 col-form-label">
                                            Số CMND
                                        </label>
                                        <div class="col-7">
                                            <input type="text" class="form-control" id="identity" placeholder="Số CMND" name="identity" value="{{ old('identity') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="m-portlet__foot m-portlet__foot--fit">
                                    <div class="m-form__actions">
                                        <div class="row">
                                            <div class="col-2"></div>
                                            <div class="col-7">
                                                <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">
                                                    Lưu
                                                </button>
                                                &nbsp;&nbsp;
                                                <button type="reset" class="btn btn-secondary m-btn m-btn--air m-btn--custom">
                                                    Hủy
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                            <div class="tab-pane " id="m_user_profile_tab_2"></div>
                            <div class="tab-pane " id="m_user_profile_tab_3"></div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end::Section-->
        </div>
    </div>
    <input type="hidden" id="api_file" name="api_file" value="{{ env('API_FILE') }}">
    <script type="text/javascript">
        $(document).ready(function() {
            $('#birthday').datepicker({
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                autoclose: true,
                orientation: "bottom left",
                templates: {
                    leftArrow: '<i class="la la-angle-left"></i>',
                    rightArrow: '<i class="la la-angle-right"></i>'
                }
            });

            $('#avatar').hover(function(){
                $(this).css('cursor', 'pointer')
            });
            $('#avatar').click(function (e) {
                $('#fileavatar').trigger('click');
            });
            $('input[type=file]').change(function(e){
                var inputName = $(this).attr('name');
                $(e.target.files).each(function( index, file ) {
                    var data = new FormData();
                    data.append("file", file);
                    $.ajax({
                        url: $('input[name=api_file]').val() + '/api/file',
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: data,
                        type: "post",
                        success: function(response) {
                            console.log(response);
                            $("#avatar").attr("src", response.data.url);
                            $("#input-avatar").val(response.data.url);
                        },
                        error: function(data) {

                        }
                    });
                });


                return false;

            });
        });

    </script>
    @include('administrator.users.script')
@stop