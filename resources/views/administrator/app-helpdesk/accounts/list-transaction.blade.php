@extends('administrator.app')
@section('title',' Danh sách dự án của khách hàng')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-content">

        <div class="m-portlet">
            <div class="m-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="m-portlet m-portlet--tab row">
                            <div class="m-portlet__head">
                                <div class="row" style="font-size: 18px; padding-top: 10px; padding-bottom: 10px;">
                                    <div class="col-md-12">
                                        Thông tin khách hàng
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4" style="text-align: center;">
                                        <div class="row">
                                            <div class="col-md-6" style="text-align: right;">Tên khách hàng:</div>
                                            <div class="col-md-6" style="text-align: left;"><b>{{ $account->fullname }}</b></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4" style="text-align: center;">
                                        <div class="row">
                                            <div class="col-md-6" style="text-align: right;">Số điện thoại:</div>
                                            <div class="col-md-6" style="text-align: left;"><b>{{ $account->phone }}</b></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4" style="text-align: center;">
                                        <div class="row">
                                            <div class="col-md-6" style="text-align: right;">Email:</div>
                                            <div class="col-md-6" style="text-align: left;"><b>{{ $account->email }}</b></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4" style="text-align: center;">
                                        <div class="row">
                                            <div class="col-md-6" style="text-align: right;">Địa chỉ:</div>
                                            <div class="col-md-6" style="text-align: left;"><b>{{ $account->address }}</b></div>
                                        </div>
                                    </div>
                                    <div class="col-md-4"></div>
                                </div>

                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#m_modal_4" >
                            Thêm mới căn
                        </button>
                </div>
                </div>
                @include('administrator.errors.info-search')
                @include('administrator.errors.messages')
                <div class="m-section ">
                    <b><br>Danh sách căn hộ của khách hàng<br></b>
                    <div class="m-section__content m-portlet m-portlet--mobile">
                        <div class="m_datatable" id="child_data_local"></div>
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->
        </div>
    </div>
</div>

{{--Model them moi can--}}
<div class="modal fade" id="m_modal_4" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Thêm mới căn
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            {{ Form::open(array('method'=>'POST', 'route' => array('account.add-transaction'), 'style' => 'display: inline-block; padding-bottom: 20px;')) }}
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="recipient-name" class="form-control-label">
                            Dự án:
                        </label>
                        {{ Form::select('project_id', ['' => '---Chọn dự án---']+$listProject, old('project_id'), ['class' => 'form-control', 'required']) }}
                    </div>
                    <div class="col-md-12">
                            <label for="recipient-name" class="form-control-label">
                                Tên Căn:
                            </label>
                            <input type="text" name="name" class="form-control" id="recipient-name" required>
                    </div>
                    <div class="col-md-12">
                        <label for="recipient-name" class="form-control-label">
                            Giá tiền:
                        </label>
                        <input type="number" name="total_money" class="form-control" id="total-money" required>
                    </div>
                    <div class="col-md-12">
                        <label for="recipient-name" class="form-control-label">
                            Ngày vào hợp đồng:
                        </label>
                        <input type="text" name="date_payment" class="form-control" id="date_payment" required>
                    </div>
                    <div class="col-md-12">
                            <label for="message-text" class="form-control-label">
                                Nhân viên kinh doanh:
                            </label>
                        <select name="sale_id" class="form-control w-100" id="user_id_select" required>
                            {!! \App\Models\Users\User::innerOptionHtml(null, old('user_id')) !!}
                        </select>
                    </div>
                        <input type="hidden" name="account_id" value="{{ $accountId }}">
                </div>
            </div>
            <div class="c-section">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Hủy
                </button>
                <button type="submit" class="btn btn-danger">
                    Thêm mới
                </button>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
{{--Model them moi giao dich--}}
<div class="modal fade" id="m_modal_5" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <form id="transaction" method="post" action="{{ route('account.add-history-transaction') }}" class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Thêm mới lịch sử thanh toán
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <label for="recipient-name" class="form-control-label">
                            Tên giao dịch:
                        </label>
                        <input type="text" name="name" class="form-control" id="transaction-name" required>
                    </div>
                    <div class="col-md-12">
                        <label for="message-text" class="form-control-label">
                            Ngày thanh toán:
                        </label>
                        <input type="text" name="date_payment" class="form-control" id="transaction-date-payment" >
                    </div>
                    <div class="col-md-12">
                        <label for="message-text" class="form-control-label">
                            Trạng thái:
                        </label>
                        {{ Form::select('status', \App\Models\Users\AccountTransactions::$statusPayment, old('status'), ['class' => 'form-control', 'id' => 'transaction-status']) }}
                    </div>
                    <input type="hidden" name="account_id" id="transaction-account-id" value="{{ $accountId }}">
                    <input type="hidden" name="parent_id"  id="transaction-parent-id">
                </div>
            </div>
            <div class="c-section">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Đóng
                </button>
                <button type="submit" class="btn btn-danger" >
                    Thêm mới
                </button>
            </div>
        </div>
        </form>
    </div>
</div>

<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
    $(document).ready(function () {
        $('#user_id_select').select2({ width: '100%' });
        var projectSelectedId = 0;
        // add history
        $('body').on('click', '.add_row_child', function() {
            var id = $(this).attr('id');
            projectSelectedId = $(this).attr('data-project-id');
            $('#transaction-parent-id').val(id);
            $('#m_modal_5').modal('toggle');
        });

        // del house
        $('body').on('click', '.del_row_house', function() {
            var id = $(this).attr('id');
            var data = {id: id};
            swal({
                title: 'Bạn muốn xóa giao dịch này?',
                text: "Mọi lịch sử của giao dịch này sẽ bị xóa hết!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Xác nhận',
                cancelButtonText: 'Hủy bỏ',
                reverseButtons: true
            }).then(function (result) {
                if (result.value) {
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    })
                    $.ajax({
                        url: '{{ route('account.del-transaction') }}',
                        type: 'POST',
                        data: data,
                        success: function (response) {
                            swal({
                                "title": "Thông báo!",
                                text: response.message,
                                type: 'success',
                                showCancelButton: false,
                                confirmButtonText: 'Xác nhận!'
                            }).then(function () {
                                index();
                                $('#child_data_local').mDatatable('reload');
                            });
                        }
                    });
                }
            });



        });
        $('#transaction-date-payment').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        $('#date_payment').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
        //add transaction
        $('#transaction').submit(function (e) {
            e.preventDefault();
            var name            = $('#transaction-name').val();
            var datePayment     = $('#transaction-date-payment').val();
            var status          = $('#transaction-status').val();
            var tranAccountId   = $('#transaction-account-id').val();
            var parentId        = $('#transaction-parent-id').val();
            var data = {name: name,
                        date_payment: datePayment,
                        status: status,
                        account_id: tranAccountId,
                        project_id: projectSelectedId,
                        parent_id: parentId};
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $.ajax({
                url:  '{{ route('account.add-history-transaction') }}',
                type: 'POST',
                data: data,
                success: function (response) {
                    if(response.status == 200){
                        swal({
                            "title": "Thông báo!",
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Xác nhận!'
                        }).then(function() {
                            document.getElementById('transaction').reset();
                            $('#m_modal_5').modal('hide');
                            index();
                            $('#child_data_local').mDatatable('reload');
                        });
                    }else{
                        swal({
                            "title": "Thông báo!",
                            text: response.message,
                            type: 'warning',
                            showCancelButton: false,
                            confirmButtonText: 'Xác nhận!'
                        }).then(function(result) {
                            document.getElementById('transaction').reset();
                            $('#m_modal_5').modal('hide');
                            index();
                            $('#child_data_local').mDatatable('reload');
                        });
                    }
                }
            });
        });

        $('body').on('click', '.delete_history_id', function(e) {
            e.preventDefault();
            var id = $(this).attr('id');
            var check_confirm = confirm("bạn muốn xóa giao dịch này khỏi hệ thống?");
            if(check_confirm){
                var id = $(this).attr('id');
                var data = {id: id};
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '{{ route('account.del-history-transaction') }}',
                    type: 'post',
                    dataType: "json",
                    data: data,
                    success: function (response) {
                        swal({
                            "title": "Thông báo!",
                            text: response.message,
                            type: 'success',
                            showCancelButton: false,
                            confirmButtonText: 'Xác nhận!'}).then(function() {
                                index();
                                $('#child_data_local').mDatatable('reload');
                        });

                    }
                });
            }
        });
    });
    var datatable;
    jQuery(document).ready(function() {
        DatatableRemoteAjaxDemo.init(window.location.href );
    });
    function index(url) {
        datatable = $('.m_datatable').mDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        dataType: 'json',
                        url: url,
                    },
                },
                pageSize: 20, // display 20 records per page
                serverPaging: true,
                serverFiltering: false,
                serverSorting: true,
            },

            // layout definition
            layout: {
                theme: 'default',
                scroll: false,
                height: null,
                footer: false,
            },

            sortable: true,

            filterable: false,

            pagination: true,

            detail: {
                title: 'Xem lịch sửa thanh toán',
                content: childTable,
            },

            search: {
                input: $('#generalSearch'),
            },
            // columns definition
            columns: [
                {
                    field: 'id',
                    title: '',
                    sortable: false,
                    width: 20,
                    textAlign: 'center' // left|right|center,
                },
                {
                    field: 'project',
                    title: 'Dự án',
                    template: function(row) {
                        if (row.project) {
                            return row.project.name;
                        }
                        return '';
                    },
                },
                {
                    field: 'name',
                    title: 'Tên căn',

                },
                {
                    field: 'sale',
                    title: 'Sale',
                    template: function(row) {
                        if (row.sale) {
                            return row.sale.fullname;
                        }
                        return '';
                    },
                },
                {
                    field: 'total_money',
                    title: 'Tổng tiền',
                    template: function(row) {
                        if (row.sale) {
                            return addCommas(row.total_money);
                        }
                        return '';
                    },
                },
                {
                    field: '',
                    title: 'Tổng số giao dịch',
                    overflow: 'visible',
                    textAlign: 'center',
                    template: function(row) {
                        if (row.children.length > 0) {
                            return row.children.length;
                        }
                        return '-';
                    },
                },
                {
                    field: 'Actions',
                    width: 110,
                    title: 'Actions',
                    sortable: false,
                    overflow: 'visible',
                    template: function (row, index, datatable) {
                        var dropup = (datatable.getPageSize() - index) <= 4 ? 'dropup' : '';
                        return '<div class="dropdown ' + dropup + '">\
                                    <a class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill add_row_child" data-project-id="'+row.project.id+'" id="'+row.id+'" title="Thêm giao dịch">\
                                        <i class="fa flaticon-plus"></i>\
                                    </a>\
                                    <a class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill del_row_house" id="'+row.id+'" title="Xóa căn">\
                                        <i class="la la-trash"></i>\
                                    </a>\
                                </div>';
                    },
                }],
        });
        return datatable;
    }

    var DatatableRemoteAjaxDemo = function() {
        var demo = function(url) {
            index(url)
        }

        return {
            // public functions
            init: function(url) {
                demo(url);
            },
        };
    }();
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
    function childTable(dataChild) {
        var data = {id: dataChild.data.id, accountId: dataChild.data.account_id, projectId: dataChild.data.project_id};
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })
        $.ajax({
            url:  '{{ route('account.list-transaction-by-account') }}',
            type: 'POST',
            data: data,
            success: function (response) {
                $('<div/>').attr('id', 'child_data_local_' + dataChild.data.id).appendTo(dataChild.detailCell).mDatatable({
                    data: {
                        type: 'local',
                        source: response.data,
                        pageSize: 10
                    },
                    // layout definition
                    layout: {
                        theme: 'default',
                        scroll: true,
                        height: 300,
                        footer: false,

                        // enable/disable datatable spinner.
                        spinner: {
                            type: 1,
                            theme: 'default',
                        },
                    },

                    sortable: true,

                    // columns definition
                    columns: [
                        {
                            field: 'name',
                            title: 'Tên giao dịch',
                        }, {
                            field: 'date_payment',
                            title: 'Ngày giao dịch',
                            template: function (row) {
                                if(row.status != null){
                                    return row.date_payment;
                                }
                            }
                        }
                        // ,{
                        //     field: 'status',
                        //     title: 'Trạng thái',
                        //     textAlign: 'center',
                        //     template: function (row) {
                        //         if(row.status == 1){
                        //             return 'Đã thanh toán';
                        //         }
                        //         if(row.status == 0){
                        //             return 'Chưa thanh toán';
                        //         }
                        //         return '-';
                        //     }
                        // }
                        , {
                            field: 'Actions',
                            width: 110,
                            title: 'Actions',
                            sortable: false,
                            overflow: 'visible',
                            template: function (row, index, datatable) {
                                    return '<a href="{{ url()->current() }}/'+ row.id +'/edit" id="'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill delete_history_id" title="Xóa">\
                                                <i class="la la-trash"></i></a>';
                            },
                        }
                    ],
                });
            }
        });
    }
</script>
@stop