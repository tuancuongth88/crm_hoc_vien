@extends('administrator.app')
@section('title','Danh sách tài khoản khách hàng')
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="m-portlet">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Danh sách tài khoản khách hàng
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-content">
                @include('administrator.app-helpdesk.accounts.search')
                <!--begin::Section-->

                <div class="col-xs-12" style="margin-bottom: 20px">
                    {{ Form::open(array('enctype' => 'multipart/form-data', 'method'=>'POST', 'route' => 'account.import', 'id' => 'import-form')) }}
                    <a href="{{ route('account.create') }}" class="btn btn-primary m-btn m-btn--icon">
                        <span>
                            <i class="fa flaticon-plus"></i>
                            <span>
                                Thêm mới
                            </span>
                        </span>
                    </a>
                    <div class="custom-file col-md-3 m--padding-top-5">
                        <input type="file" class="custom-file-input" id="customFile" name="import_file">
                        <label class="custom-file-label" for="customFile">
                            Chọn file import
                        </label>
                    </div>
                    <button type="submit" class="btn btn-success" id="import_file"><i class="fa fa-file-excel-o"></i> Import</button>
                    {{ Form::close() }}<br>
                    <a href="{{ route('account.export-file') }}" style="font-weight: bold; font-size: 17px; color: #000;">Mẫu File Import</a>
                </div>
                @if(session('list-fail'))
                    <div class="alert alert-danger alert-notification">
                        @foreach (session('list-fail') as $element)
                            {{ $element }}<br>
                        @endforeach
                    </div>
                @endif
                @include('administrator.errors.info-search')
                <div class="m-section">
                        @include('administrator.errors.messages')
                        <div class="m-section__content">
                            <table class="table m-table m-table--head-bg-warning">
                                <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Tên khách hàng
                                        </th>
                                        <th>
                                            Tài khoản
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Ngày sinh
                                        </th>
                                        <th>
                                            Tổng số căn đã mua
                                        </th>
                                        <th>
                                            Tùy chọn
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key => $value)
                                    <tr>
                                        <th scope="row">
                                            {{ $key + 1 }}
                                        </th>
                                        <td>
                                            {{ $value->fullname }}
                                        </td>
                                        <td>
                                            {{ $value['phone'] }}
                                        </td>
                                        <td>
                                            {{ $value->email }}
                                        </td>
                                        <td>
                                            {{ $value->birthday }}
                                        </td>
                                        <td style="text-align: center;">
                                            <span style="color: red;  font-weight: bold;">{{ $value->transaction->where('parent_id', 0)->count() }}</span>
                                        </td>
                                        <td>
                                            <a href="{{ route('account.list-house', $value->id) }}"
                                               class="btn btn-primary ">
                                                Danh sách dự án
                                            </a>
                                            <a href="{{ route('account.edit', $value->id) }}" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            {{ Form::open(array('method'=>'DELETE', 'route' => array('account.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                            <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                <i class="fa fa-close"></i>
                                            </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $data->links() }}
                        Tổng số {{ $data->total() }} bản ghi

                </div>
                <!--end::Section-->
            </div>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function () {
            $('#m_modal_5').modal('toggle');
        });
    </script>
@stop