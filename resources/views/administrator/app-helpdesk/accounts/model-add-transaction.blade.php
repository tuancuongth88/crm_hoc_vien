{{--Model them moi giao dich--}}
<style>
    ul {
        list-style: none;
        padding: 0;
    }

    .inner {
        padding-left: 1em;
        overflow: hidden;
        display: none;
    }

    li {
        margin: .5em 0;

    a.toggle {
        width: 100%;
        display: block;
        background: rgba(0, 0, 0, 0.78);
        color: #fefefe;
        padding: .75em;
        border-radius: 0.15em;
        transition: background .3s ease;
    }
</style>
<div class="modal fade" id="m_modal_5" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <form id="transaction" method="post" action="{{ route('account.add-transaction-customer') }}"
              class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        THÊM MỚI DỰ ÁN ĐÃ MUA CHO KH NGUYỄN VĂN A
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="m_repeater_1">
                        <div class="form-group  m-form__group row" id="m_repeater_1">
                            <div data-repeater-list="info" class="col-xl-12">
                                <div data-repeater-item class="form-group m-form__group row align-items-center">
                                    <div class="col-xl-12">
                                        <div class="m-form__group m-form__group--inline contact-form">
                                            <div class="m-portlet m-portlet--head-sm" data-portlet="true" id="m_portlet_tools_3">
                                                <div class="m-portlet__head">
                                                    <div class="m-portlet__head-caption">
                                                        <div class="m-portlet__head-title">
                                                <span class="m-portlet__head-icon">
                                                    <i class="flaticon-coins"></i>
                                                </span>
                                                            <h3 class="m-portlet__head-text">
                                                                DỰ ÁN ROMAN
                                                            </h3>
                                                        </div>
                                                    </div>
                                                    <div class="m-portlet__head-tools">
                                                        <ul class="m-portlet__nav">
                                                            <li class="m-portlet__nav-item">
                                                                <a href="#" data-portlet-tool="toggle"
                                                                   class="m-portlet__nav-link m-portlet__nav-link--icon" title=""
                                                                   data-original-title="Collapse">
                                                                    <i class="la la-angle-down"></i>
                                                                </a>
                                                            </li>
                                                            <li class="m-portlet__nav-item">
                                                                <div data-repeater-delete="">
                                                                    <a href="#" data-portlet-tool="remove"
                                                                       class="m-portlet__nav-link m-portlet__nav-link--icon" title=""
                                                                       data-original-title="Remove">
                                                                        <i class="la la-close"></i>
                                                                    </a>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="m-portlet__body" style="display: none;">
                                                    <div class="m-accordion__item-content">
                                                        <div class="row">
                                                            <div class="col-xl-12">
                                                                <!--begin::Portlet-->
                                                                <div class="m-portlet__body">
                                                                    <!--begin::Section-->
                                                                    <div class="m-section">
                                                                        <div class="m-section__content">
                                                                            <a class="toggle" href="javascript:void(0);">Mã căn: A0502 -
                                                                                Trần Thanh Tùng</a>
                                                                            <ul class="inner">
                                                                                <li>
                                                                                    <table class="table m-table">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>
                                                                                                #
                                                                                            </th>
                                                                                            <th>
                                                                                                Mã căn
                                                                                            </th>
                                                                                            <th>
                                                                                                Nhân viên
                                                                                            </th>
                                                                                            <th>
                                                                                                Xóa mã căn
                                                                                            </th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <th scope="row">
                                                                                                1
                                                                                            </th>
                                                                                            <td>
                                                                                                A0502
                                                                                            </td>
                                                                                            <td>
                                                                                                Nguyễn Văn A
                                                                                            </td>
                                                                                            <td>
                                                                                                <button type="button"
                                                                                                        class="btn btn-danger">Xóa
                                                                                                </button>
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table class="table m-table">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>
                                                                                                Tên giao dịch
                                                                                            </th>
                                                                                            <th>
                                                                                                Ngày thanh toán
                                                                                            </th>
                                                                                            <th>
                                                                                                Trạng thái
                                                                                            </th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <th scope="row">
                                                                                                Đặt cọc
                                                                                            </th>
                                                                                            <td>
                                                                                                1/26/2019
                                                                                            </td>
                                                                                            <td>
                                                                                                Đã thanh toán
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Section-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-xl-12">
                                                                <!--begin::Portlet-->
                                                                <div class="m-portlet__body">
                                                                    <!--begin::Section-->
                                                                    <div class="m-section">
                                                                        <div class="m-section__content">
                                                                            <a class="toggle" href="javascript:void(0);">Mã căn: A0502 -
                                                                                Trần Thanh Tùng</a>
                                                                            <ul class="inner">
                                                                                <li>
                                                                                    <table class="table m-table">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>
                                                                                                #
                                                                                            </th>
                                                                                            <th>
                                                                                                Mã căn
                                                                                            </th>
                                                                                            <th>
                                                                                                Nhân viên
                                                                                            </th>
                                                                                            <th>
                                                                                                Xóa mã căn
                                                                                            </th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <th scope="row">
                                                                                                1
                                                                                            </th>
                                                                                            <td>
                                                                                                A0502
                                                                                            </td>
                                                                                            <td>
                                                                                                Nguyễn Văn A
                                                                                            </td>
                                                                                            <td>
                                                                                                <button type="button"
                                                                                                        class="btn btn-danger">Xóa
                                                                                                </button>
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                    <table class="table m-table">
                                                                                        <thead>
                                                                                        <tr>
                                                                                            <th>
                                                                                                Tên giao dịch
                                                                                            </th>
                                                                                            <th>
                                                                                                Ngày thanh toán
                                                                                            </th>
                                                                                            <th>
                                                                                                Trạng thái
                                                                                            </th>
                                                                                        </tr>
                                                                                        </thead>
                                                                                        <tbody>
                                                                                        <tr>
                                                                                            <th scope="row">
                                                                                                Đặt cọc
                                                                                            </th>
                                                                                            <td>
                                                                                                1/26/2019
                                                                                            </td>
                                                                                            <td>
                                                                                                Đã thanh toán
                                                                                            </td>
                                                                                        </tr>
                                                                                        </tbody>
                                                                                    </table>
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <!--end::Section-->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="d-md-none m--margin-bottom-10"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="m-form__group form-group row button-contact">
                            <label class="col-lg-2 col-form-label"></label>
                            <div class="col-lg-4">
                                <div data-repeater-create="" class="btn btn btn-sm btn-brand m-btn m-btn--icon m-btn--pill m-btn--wide">
                                    <span>
                                        <i class="la la-plus"></i>
                                        <span>
                                            Thêm mới
                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="c-section">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">
                        Đóng
                    </button>
                    <button type="submit" class="btn btn-danger">
                        Thêm mới
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $('.toggle').click(function (e) {
        e.preventDefault();

        var $this = $(this);

        if ($this.next().hasClass('show')) {
            $this.next().removeClass('show');
            $this.next().slideUp(350);
        } else {
            $this.parent().parent().find('li .inner').removeClass('show');
            $this.parent().parent().find('li .inner').slideUp(350);
            $this.next().toggleClass('show');
            $this.next().slideToggle(350);
        }
    });
</script>