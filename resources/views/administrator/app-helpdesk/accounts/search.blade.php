{{ Form::open(array('route' => 'account.index', 'method' => 'GET')  ) }}
<div class="row gutter-2 gutter-lg-3 mb-4">
    <div class="col-md-3 col-sm-6 mb-3 mb-md-0">
        <label>Tên khách hàng</label>
        <input type="text" name="search" id="search" class="form-control" value="{{ @$request['search'] }}" placeholder="Số điện thoại, Email, Tên" />
    </div>
    <div class="col-md-3 col-sm-6 mb-3 mb-md-0">
        <label>Sinh nhật trong tháng</label>
        {{ Form::select('birthday', ['' => 'Chọn tháng'] + $listMonth, @$request['birthday'], ['class' => 'form-control']) }}
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Tìm kiếm</label>
        <input type="submit" value="Tìm kiếm" class="btn btn-primary form-control" />
    </div>
    <div class="col-md-2 col-sm-6 mb-3 mb-md-0">
        <label>Hủy tìm</label>
        <a href="{{ route('account.index') }}" class="btn btn-danger form-control">Hủy tìm</a>
    </div>
</div>
</form>
