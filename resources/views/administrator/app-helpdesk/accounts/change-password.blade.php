@extends('administrator.app')
@section('title','Cập nhật')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
	<!-- BEGIN: Subheader -->
	<div class="m-subheader">
	    <div class="d-flex align-items-center">
	        <div class="mr-auto">
	            <h3 class="m-subheader__title m-subheader__title--separator">
	                Cập nhật tài khoản
	            </h3>
	            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
	                <li class="m-nav__item m-nav__item--home">
	                    <a href="#" class="m-nav__link m-nav__link--icon">
	                        <i class="m-nav__link-icon la la-home"></i>
	                    </a>
	                </li>
	            </ul>
	        </div>
	    </div>
	</div>
	@include('administrator.errors.errors-validate')
	<!-- END: Subheader -->
	<div class="m-content">
		<div style="margin-bottom: 20px">
		</div>
		<div class="row">
	        <div class="col-md-12">
            @include('administrator.errors.messages')
	            <!--begin::Portlet-->
	            <div class="m-portlet m-portlet--tab">
	                <div class="m-portlet__head">
	                    <div class="m-portlet__head-caption">
	                        <div class="m-portlet__head-title">
	                            <span class="m-portlet__head-icon m--hide">
	                                <i class="la la-gear"></i>
	                            </span>
	                            <h3 class="m-portlet__head-text">
	                                Cập nhật tài khoản Khách hàng
	                            </h3>
	                        </div>
	                    </div>
	                </div>

                    {{ Form::open(array('route' => array('account.post-change-password'), 'method' => 'POST', 'class' => 'm-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed', 'enctype' => 'multipart/form-data')) }}
						<input type="hidden" name="account_id" value="{{$id }}">
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <label>
                                    Mật khẩu hiện tại <span style="color: red;">(*)</span>:
                                </label>
                                <input type="password" class="form-control m-input" name="current_password" >
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="">
                                    Mật khẩu mới <span style="color: red;">(*)</span>:
                                </label>
                                <input type="password" class="form-control m-input" name="new_password" >
                            </div>
                            <div class="form-group m-form__group row">
                                <label>
                                    Xác nhận lại mật khẩu mới <span style="color: red;">(*)</span>:
                                </label>
                                {{-- <div class="input-group m-input-group m-input-group--square">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="la la-user"></i>
                                        </span>
                                    </div> --}}
                                    <input type="password" class="form-control m-input" name="new_password_confirmation">
                                {{-- </div> --}}
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Lưu
                                </button>
                            </div>
                        </div>
                    {{-- </form> --}}
                    {{ Form::close() }}


	            </div>
	            <!--end::Portlet-->
	        </div>
	        <!--end::Form-->
	    </div>
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#birthday').datepicker({
            todayHighlight: true,
            format: 'dd-mm-yyyy',
            autoclose: true,
            orientation: "bottom left",
            templates: {
                leftArrow: '<i class="la la-angle-left"></i>',
                rightArrow: '<i class="la la-angle-right"></i>'
            }
        });
    });
</script>
@stop