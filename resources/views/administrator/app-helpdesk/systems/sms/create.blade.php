@extends('administrator.app')
@section('title','Tạo mới mẫu')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Quản lý SMS
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">

            <div class="row">
                <div class="col-md-12" style="margin-bottom: 20px">
                    <a href="{{ route('sms.index') }}" class="btn btn-success">Danh sách</a>
                </div>
                <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
	                            <span class="m-portlet__head-icon m--hide">
	                                <i class="la la-gear"></i>
	                            </span>
                                    <h3 class="m-portlet__head-text">
                                        Thêm mới mẫu tin nhắn
                                    </h3>
                                </div>
                            </div>
                        </div>
                        @include('administrator.errors.errors-validate')
                        @include('administrator.errors.messages')
                        {{ Form::open(array('route' => 'sms.store', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Loại tin
                                </label>
                                {{ Form::select('type', $listType, old('type'), ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Tên mẫu
                                </label>
                                <input type="name" class="form-control m-input" id="name" placeholder="Tên mẫu" name="name" value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group m-form__group">
                                <label for="name">
                                    Nội dung
                                </label>
                                <div class="input-group m-input-group m-input-group--square">
                                    <textarea class="form-control" cols="100%" rows="4" name="message" id="message" maxlength="170" required>{{ old('message')}}</textarea>
                                </div>
                                <span class="m-form__help">
                                        Tổng số ký tự: <span id="totalChars">0</span><br/>
                                        <b>Lưu ý:</b><br>
                                        Nội dụng tin nhắn không được chứa các ký tự có dấu<br>
                                        <b>Trong đó:</b>
                                        <div class="row">
                                            <div class="col-md-3 " style="color: red;">{phone}:</div>
                                            <div class="col-md-9">Số điện thoại của tài khoản</div>
                                            <div class="col-md-3" style="color: red;">{fullname}:</div>
                                            <div class="col-md-9">Tên khách hàng</div>
                                        </div>
                                    </span>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('news-category.index') }}">
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                        {{-- </form> --}}
                        {{ Form::close() }}
                    </div>
                    <!--end::Portlet-->
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#message').keyup(function () {
                var text = $('#message').val();
                var text_create = text.replace(/à|á|ạ|ả`|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g,"o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g,"i");
                $('#message').val(text_create);

                var count = $("#message").val().length;
                $('#totalChars').html(count);
            })
        });
    </script>
@stop