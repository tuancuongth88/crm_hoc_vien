@extends('administrator.app')
@section('title','Cập nhật mẫu')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="row">
                <div class="col-md-12">
                @include('administrator.errors.errors-validate')
                @include('administrator.errors.messages')
                <!--begin::Portlet-->
                    <div class="m-portlet m-portlet--tab">
                        {{ Form::open(array('route' => array('sms.update', $data->id), 'method' => 'PUT', 'class' => 'm-form m-form--fit m-form--label-align-right')) }}
                        <div class="m-portlet__body">
                            <div class="form-group m-form__group row">
                                <div class="col-lg-8">
                                    <label>
                                        Mẫu:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="la la-slack"></i>
                                            </span>
                                        </div>
                                        <input type="text" class="form-control m-input" placeholder="Nhập tên" name="name"value="{{ $data->name }}">
                                    </div>

                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-lg-8">
                                    <label>
                                        Nội dung tin nhắn:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
                                        <textarea class="form-control" cols="100%" rows="4" name="message" id="message" maxlength="170">{{ $data->message }}</textarea>
                                    </div>
                                    <span class="m-form__help">
                                        Tổng số ký tự: <span id="totalChars">{{ strlen($data->message) }}</span><br/>
                                        <b>Lưu ý:</b><br>
                                        Nội dụng tin nhắn không được chứa các ký tự có dấu<br>
                                        <b>Trong đó:</b>
                                        <div class="row">
                                            <div class="col-md-3 " style="color: red;">{phone}:</div>
                                            <div class="col-md-9">Số điện thoại của tài khoản</div>
                                            @if($data->type == \App\Models\Systems\OptionSMS::TYPE_REG)
                                                <div class="col-md-3" style="color: red;">{password}:</div>
                                                <div class="col-md-9">Mật khẩu tài khoản</div>
                                                <div class="col-md-3" style="color: red;">{android}:</div>
                                                <div class="col-md-9">Link tải ứng dụng Android</div>
                                                <div class="col-md-3" style="color: red;">{ios}:</div>
                                                <div class="col-md-9">Link tải ứng dụng IOS</div>
                                            @endif
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions">
                                <button class="btn btn-primary">
                                    <i class="fa fa-save"></i>
                                    Lưu
                                </button>
                                <a class="btn btn-secondary" href="{{ route('sms.index') }}">
                                    <i class="la la-arrow-circle-o-left"></i>
                                    Trở về danh sách
                                </a>
                            </div>
                        </div>
                        {{-- </form> --}}
                        {{ Form::close() }}
                    </div>
                    <!--end::Portlet-->
                </div>
                <!--end::Form-->
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            $('#message').keyup(function () {
                var text = $('#message').val();
                var text_create = text.replace(/à|á|ạ|ả`|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a").replace(/\ /g, ' ').replace(/đ/g, "d").replace(/đ/g, "d").replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y").replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u").replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ.+/g,"o").replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ.+/g, "e").replace(/ì|í|ị|ỉ|ĩ/g,"i");
                $('#message').val(text_create);
                var count = $("#message").val().length;
                $('#totalChars').html(count);
            })
        });
    </script>
@stop