@extends('administrator.app')
@section('title','Danh sách Mẫu')

@section('content')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Danh sách mẫu tin nhắn
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-content">
                    <div class="col-xs-12" style="margin-bottom: 20px">
                        <a href="{{ route('sms.create') }}" class="btn btn-primary m-btn m-btn--icon">
                        <span>
                            <i class="fa flaticon-plus"></i>
                            <span>
                                Thêm mới
                            </span>
                        </span>
                        </a>
                    </div>
                    @if(session('list-fail'))
                        <div class="alert alert-danger alert-notification">
                            @foreach (session('list-fail') as $element)
                                {{ $element }}<br>
                            @endforeach
                        </div>
                    @endif
                    @include('administrator.errors.info-search')
                    <div class="m-section">
                        @include('administrator.errors.messages')
                        <div class="m-section__content">
                            <table class="table m-table m-table--head-bg-warning">
                                <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Mẫu
                                    </th>
                                    <th>
                                        Loại mẫu
                                    </th>
                                    <th>
                                        Nội dung
                                    </th>
                                    <th>
                                        Tùy chọn
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($data as $key => $value)
                                    <tr>
                                        <th scope="row">
                                            {{ $key + 1 }}
                                        </th>
                                        <td>
                                            {{ $value->name }}
                                        </td>
                                        <td>
                                            {{ @$listType[$value->type] }}
                                        </td>
                                        <td class="small">
                                            {{ $value->message }}
                                        </td>
                                        <td>
                                            <a href="{{ route('sms.edit', $value->id) }}" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                            {{ Form::open(array('method'=>'DELETE', 'route' => array('sms.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                            <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                                <i class="fa fa-close"></i>
                                            </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{ $data->links() }}
                        Tổng số {{ $data->total() }} bản ghi

                    </div>
                    <!--end::Section-->
                </div>
            </div>
        </div>
    </div>
@stop