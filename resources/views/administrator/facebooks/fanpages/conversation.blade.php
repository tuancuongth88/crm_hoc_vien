<div class="m-messenger m-messenger--message-arrow m-messenger--skin-light">
    <div class="m-messenger__messages">
        @foreach ($data as $value)
            <div class="m-messenger__wrapper">
                @if ($pageId != $value->from->id)
                    <div class="m-messenger__message m-messenger__message--in">
                        <div class="m-messenger__message-pic">
                            <img src="{{ URL::asset('assets/app/media/img/users/user3.jpg') }}" alt="" />
                        </div>
                        <div class="m-messenger__message-body">
                            <div class="m-messenger__message-arrow"></div>
                            <div class="m-messenger__message-content">
                                <div class="m-messenger__message-username">
                                    {{ $value->from->name }}
                                </div>
                                <div class="m-messenger__message-text">
                                    {{ $value->message }}
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="m-messenger__wrapper">
                        <div class="m-messenger__message m-messenger__message--out">
                            <div class="m-messenger__message-body">
                                <div class="m-messenger__message-arrow"></div>
                                <div class="m-messenger__message-content">
                                    <div class="m-messenger__message-text">
                                        {{ $value->message }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        @endforeach
        {{-- <div class="m-messenger__datetime">
            2:30PM
        </div> --}}
    </div>
    <div class="m-messenger__seperator"></div>
    <div class="m-messenger__form">
        <div class="m-messenger__form-controls">
            <input type="text" name="" placeholder="..." class="m-messenger__form-input">
        </div>
        <div class="m-messenger__form-tools">
            <a href="" class="m-messenger__form-attachment">
                <i class="fa fa-send-o"></i>
            </a>
        </div>
    </div>
</div>