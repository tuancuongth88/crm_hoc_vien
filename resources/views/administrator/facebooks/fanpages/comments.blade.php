
@extends('administrator.app')
@section('title','Chi nhánh')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Chi nhánh
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        From: {{ $data->from->name }}<br>
        Message: {{ $data->message }}<br>
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <div class="page-block p-x-4">
                    <ul style="list-style-type: none;">
                        @foreach($data->comments->data as $value)
                        <li style="list-style-type: none;">
                            <div>
                                <div class="widget-tree-comments-header">
                                    <span>&nbsp;&nbsp;{{ $value->created_time }}</span>
                                </div>
                                <div class="widget-tree-comments-text">
                                    <a href="{{ route('comment.child',  $value->id) }}">{{ $value->message }}</a>
                                </div>
                            </div>
                            {{-- @if($value->children->count() > 0)
                                <ul style="list-style-type: none;">
                                @foreach($value->children as $child)
                                    <li style="list-style-type: none;">
                                        <div>
                                            <img src="{{ $child->user->avatar }}" alt="" class="widget-tree-comments-avatar">
                                            <div class="widget-tree-comments-header">
                                                <a href="#" title="">{{ $child->user->email }}</a><span>&nbsp;&nbsp;{{ $child->created_at }}</span>
                                                <button data-commentid="{{ $child->id  }}" id="comment_{{ $value->id  }}"  class="{{ $child->is_approve == APPROVE ? 'btn btn-success': 'btn btn-danger' }} m-btn m-btn--icon m-btn--icon-only btn-approve">
                                                    <i id="comment_item_{{ $child->id  }}" class="{{ $child->is_approve == APPROVE ? 'fa fa-check': 'fa fa-close' }}"></i>
                                                </button>
                                            </div>
                                            <div class="widget-tree-comments-text">
                                                {{ $child->comment }}
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                            @endif --}}
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
@stop