@extends('administrator.app')
@section('title','Chi nhánh')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Chi nhánh
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        <div class="col-xs-12" style="margin-bottom: 20px">
            <a href="{{ route('branch.create') }}" class="btn btn-primary m-btn m-btn--icon">
                <span>
                    <i class="fa flaticon-plus"></i>
                    <span>
                        Thêm mới
                    </span>
                </span>
            </a>
        </div>
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <table class="table m-table m-table--head-bg-warning">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Tên
                            </th>
                            <th>
                                Danh mục
                            </th>
                            <th>
                                Tùy chọn
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <th scope="row">
                                {{ $key + 1 }}
                            </th>
                            <td>
                                <a href="{{ route('page.post', [ $value->id, 'access_token' => $value->access_token]) }}">{{ $value->name }}</a>
                            </td>
                            <td>
                                <a href="#">{{ $value->category }}</a>
                            </td>
                            <td>
                                <a href="{{ route('page.post', [ $value->id, 'access_token' => $value->access_token]) }}" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only">
                                    <i class="fa fa-list-alt"></i>
                                </a>
                                <a href="{{ route('page.inbox', [ $value->id, 'access_token' => $value->access_token]) }}" class="btn btn-accent m-btn m-btn--icon m-btn--icon-only">
                                    <i class="fa fa-commenting-o"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
@stop