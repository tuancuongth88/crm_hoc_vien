@extends('administrator.app')
@section('title','Chi nhánh')

@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <!--begin::Section-->
        <div class="col-xs-12" style="margin-bottom: 20px">
            <a href="{{ route('branch.create') }}" class="btn btn-primary m-btn m-btn--icon">
                <span>
                    <i class="fa flaticon-plus"></i>
                    <span>
                        Thêm mới
                    </span>
                </span>
            </a>
            <button id="show-inbox" class="btn btn-primary m-btn m-btn--icon" data-id="{{ $pageId }}" data-token="{{ $pageAccessToken }}">
                <span>
                    <i class="fa fa-comments"></i>
                    <span>
                        inbox
                    </span>
                </span>
            </button>
        </div>
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <!--begin:: Widgets/Support Tickets -->
                <div class="m-portlet m-portlet--full-height ">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    Danh sách bài viết
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="m-portlet__body">
                        <div class="m-widget3">
                            @foreach($data->posts->data as $value)
                            <div class="m-widget3__item">
                                <div class="m-widget3__header">
                                    <div class="m-widget3__user-img">
                                        <img class="m-widget3__img" src="{{ $data->picture->data->url }}" alt="">
                                    </div>
                                    <div class="m-widget3__info">
                                        <span class="m-widget3__username" data-id="{{ $data->id }}">
                                            {{ $data->name }}
                                        </span>
                                        <br>
                                        <span class="m-widget3__time">
                                            {{ date('d-m-Y H:i', strtotime($value->created_time))  }}
                                        </span>
                                    </div>

                                </div>
                                <div class="m-widget3__body">
                                    <p class="m-widget3__text">
                                        <a href="{{ route('page.post_comment', $value->id) }}">{{ $value->message or '' }}</a>
                                        <br>
                                        <img src="{{ $value->full_picture or '' }}" width="450">
                                    </p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!--end:: Widgets/Support Tickets -->
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>
<div id="m_quick_sidebar_haind" class="m-quick-sidebar m-quick-sidebar--tabbed m-quick-sidebar--skin-light">
    <div class="m-quick-sidebar__content m--hide" id="m-quick-sidebar__content">
    </div>
</div>
@section('script')
<script type="text/javascript">
    var quickSidebar = function() {
        var id = $('#show-inbox').attr('data-id');
        var token = $('#show-inbox').attr('data-token');
        var topbarAside = $('#m_quick_sidebar_haind');
        var topbarAsideToggle = $('#show-inbox');
        var topbarAsideContent = topbarAside.find('.m-quick-sidebar__content');

        var initOffcanvasTabs = function() {
        }

        var initOffcanvas = function() {
            topbarAside.mOffcanvas({
                class: 'm-quick-sidebar',
                overlay: true,
                // close: topbarAsideClose,
                toggle: topbarAsideToggle
            });

            topbarAside.mOffcanvas().on('afterShow', function() {
                $('#m-quick-sidebar__content').empty();
                topbarAsideContent.removeClass('m--hide');
                $.ajax({
                    url: '/administrator/fanpage/' + id + '/inbox?access_token=' + token,
                    type: 'GET',
                    success: function(response) {
                        $('#m-quick-sidebar__content').append(response);
                    },
                    error: function(data) {

                    }
                });
            });
        }

        return {
            init: function() {
                if (topbarAside.length === 0) {
                    return;
                }
                initOffcanvas();
            }
        };
    }();

    $(document).ready(function() {
        quickSidebar.init();
    });
</script>
@endsection
@stop