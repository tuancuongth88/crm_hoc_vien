@extends('administrator.app')
@section('title','Chi nhánh')

@section('content')
<link href="https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/7.0.3/jquery.mmenu.css" rel="stylesheet" type="text/css" />
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <!-- BEGIN: Subheader -->
    <div class="m-subheader">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">
                    Chi nhánh
                </h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                    <li class="m-nav__item m-nav__item--home">
                        <a href="#" class="m-nav__link m-nav__link--icon">
                            <i class="m-nav__link-icon la la-home"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Subheader -->
    <div class="m-content">
        <!--begin::Section-->
        <div class="col-xs-12" style="margin-bottom: 20px">
            <a href="{{ route('branch.create') }}" class="btn btn-primary m-btn m-btn--icon">
                <span>
                    <i class="fa flaticon-plus"></i>
                    <span>
                        Thêm mới
                    </span>
                </span>
            </a>
        </div>
        @include('administrator.errors.info-search')
        <div class="m-section">
            @include('administrator.errors.messages')
            <div class="m-section__content">
                <table class="table m-table m-table--head-bg-warning">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>
                                Tên
                            </th>
                            <th>
                                Email
                            </th>
                            <th>
                                Tổng số
                            </th>
                            <th>
                                Chưa đọc
                            </th>
                            <th>
                                Tùy chọn
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <th scope="row">
                                {{ $key + 1 }}
                            </th>
                            <td>
                                <a href="#">{{ $value->senders->data[0]->name }}</a>
                            </td>
                            <td>
                                <a href="#">{{ $value->senders->data[0]->email }}</a>
                            </td>
                            <td>
                                <a href="#">{{ $value->message_count }}</a>
                            </td>
                            <td>
                                <a href="#">{{ $value->unread_count }}</a>
                            </td>
                            <td>
                                {{-- {{ route('page.conversation', [ $value->id, 'access_token' => $pageAccessToken]) }} --}}
                                <button class="btn btn-accent m-btn m-btn--icon m-btn--icon-only" data-id="{{ $value->id }}" data-token="{{ $pageAccessToken }}">
                                    <i class="fa fa-pencil"></i>
                                </button>
                                {{ Form::open(array('method'=>'DELETE', 'route' => array('branch.destroy', $value->id), 'style' => 'display: inline-block;')) }}
                                <button onclick="return confirm('Bạn có chắc chắn muốn xóa?');" class="btn btn-danger m-btn m-btn--icon m-btn--icon-only">
                                    <i class="fa fa-close"></i>
                                </button>
                                {{ Form::close() }}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!--end::Section-->
    </div>
</div>

@stop