<span id="m_quick_sidebar_close" class="m-quick-sidebar__close">
            <i class="la la-close"></i>
        </span>
<ul id="m_quick_sidebar_tabs" class="nav nav-tabs m-tabs m-tabs-line m-tabs-line--brand" role="tablist">
    <li class="nav-item m-tabs__item">
        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_quick_sidebar_tabs_settings" role="tab">
            Danh sách
        </a>
    </li>
    <li class="nav-item m-tabs__item">
        <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_quick_sidebar_tabs_messenger" role="tab">
            Inbox
        </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active  m-scrollable" id="m_quick_sidebar_tabs_settings" role="tabpanel">
        <div class="tab-pane">
            <!--begin::Widget 14-->
            <div class="m-widget4">
                @foreach ($data as $value)
                    <!--begin::Widget 14 Item-->
                    <div class="m-widget4__item">
                        <div class="m-widget4__img m-widget4__img--pic">
                            <img src="{{ URL::asset('/facebook.png') }}" alt="">
                        </div>
                        <div class="m-widget4__info">
                            <span class="m-widget4__title">
                                <a class="tab-link show-inbox" href="#m_quick_sidebar_tabs_messenger" data-id="{{ $value->id }}"  data-token="{{ $pageAccessToken }}" data-page-id="{{ $pageId }}">{{ $value->senders->data[0]->name }}</a>
                            </span>
                            <br>
                            <span class="m-widget4__sub">
                                {{ $value->senders->data[0]->email }}
                            </span>
                        </div>
                        <div class="m-widget4__ext">
                            <a class="m-btn m-btn--pill m-btn--hover-brand btn btn-sm btn-secondary show-inbox"  href="#m_quick_sidebar_tabs_messenger"  data-id="{{ $value->id }}" data-token="{{ $pageAccessToken }}" data-page-id="{{ $pageId }}">
                                <i class="fa flaticon-chat-1"></i>
                            </a>
                        </div>
                    </div>
                    <!--end::Widget 14 Item-->
                @endforeach
            </div>
            <!--end::Widget 14-->
        </div>
    </div>
    <div class="tab-pane m-scrollable" id="m_quick_sidebar_tabs_messenger" role="tabpanel">

    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.show-inbox').on('click', function(event) {
            // Prevent url change
            event.preventDefault();
            $('#m_quick_sidebar_tabs_messenger').empty();
            var id = $(this).attr('data-id');
            var token = $(this).attr('data-token');
            var pageId = $(this).attr('data-page-id');
            $.ajax({
                url: '/administrator/fanpage/conversation/' + id + '?page_id=' + pageId + '&access_token=' + token,
                type: 'GET',
                success: function(response) {
                    $('#m_quick_sidebar_tabs_messenger').append(response);
                },
                error: function(data) {

                }
            });
            // `this` is the clicked <a> tag
            var target = $('[data-toggle="tab"][href="' + this.hash + '"]');
            // opening tab
            target.trigger('click');
            // scrolling into view
            target[0].scrollIntoView(true);
        });
    });
</script>