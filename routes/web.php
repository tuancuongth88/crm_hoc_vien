<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect()->route('login');
});
Route::get('administrator/login', ['uses' => 'Auth\AuthsController@getLogin'])->name('login');
Route::post('administrator/login', ['uses' => 'Auth\AuthsController@postLogin'])->name('login.post');
Route::post('administrator/register', ['uses' => 'Auth\AuthsController@postRegister'])->name('register');
Route::get('administrator/logout', ['uses' => 'Auth\AuthsController@getLogout'])->name('logout');
Route::get('user/activation/{token}', 'Auth\AuthsController@getActiveUser')->name('user.activate');
Route::post('user/forgot', 'Auth\AuthsController@postForgot')->name('user.forgot');
Route::get('/reset-password/{token}', 'Auth\AuthsController@getResetPassword')->name('user.reset');
Route::post('/reset-password', 'Auth\AuthsController@postResetPassword')->name('user.post-reset');

Route::group(['prefix' => 'administrator', 'middleware' => 'auth'], function () {
    Route::group(['prefix' => 'system'], function () {
        Route::resource('/dashboard', Administrators\Systems\DashboardController::class);
        Route::resource('branch', Systems\BranchesController::class);
        Route::resource('position', Systems\PositionsController::class);
        Route::get('user/user-department/{id}', 'Users\UsersController@getUserByDepartment')->name('user.list-user-by-department');
        Route::post('user/import', 'Users\UsersController@postImport')->name('user.import');
        Route::post('role/import', 'Permissions\RolesController@postImport')->name('role.import');
        Route::post('user/change-password', 'Users\UsersController@postChangePassword')->name('user.post-change-password');
        Route::get('/user/list', 'Users\UsersController@getList');
        Route::get('/user/change-password', 'Users\UsersController@getChangePassword')->name('user.get-change-password');
        Route::get('/user/be-managed', 'Users\UsersController@getBeManaged');
        Route::get('/user/search', 'Users\UsersController@getSearch');
        Route::get('user/student/edit-info', 'Users\EducationUserInfosController@index')->name('user.student.edit');
        Route::get('user/student/view-info/{id}', 'Users\EducationUserInfosController@viewInfo')->name('user.student.view');
        Route::get('user/student', 'Users\EducationUserController@index')->name('user.student');
        Route::get('user/teacher', 'Users\EducationUserController@getTeacher')->name('user.teacher');
        Route::get('user/teacher/view-info/{id}', 'Users\EducationUserInfosController@viewInfoTeacher')->name('user.teacher.view');
        Route::get('user/teacher/edit-info', 'Users\EducationUserInfosController@editInfoTeacher')->name('user.teacher.edit');
        Route::post('/user/{id}', 'Users\UsersController@destroy');
        Route::post('user/education-info/store', 'Users\EducationUserInfosController@store');
        Route::post('user/education-info/uploadavatar', 'Users\EducationUserInfosController@updateAvatar');
        Route::get('/user/read-notification/{id}', 'Users\UsersController@readNotification')->name('user.read.notification');
        Route::resource('user', Users\UsersController::class);
        Route::resource('department', Systems\DepartmentsController::class);
        Route::resource('company', Systems\CompaniesController::class);
        Route::resource('role', Permissions\RolesController::class);
        Route::resource('permission', Permissions\PermissionsController::class);
        Route::get('/config-role', 'Permissions\RolesController@getConfigRole')->name('role.config-role');
        Route::put('/put-permission', 'Permissions\RolesController@putPermissionIntoRole')->name('role.put-permission');
        Route::put('put-role', 'Permissions\RolesController@putRoleIntoUser')->name('role.put-role');
        Route::get('config-user', 'Permissions\RolesController@getConfigUser')->name('role.config-user');
        Route::get('config-role-user', 'Permissions\RolesController@getConfigRoleUser')->name('role.config-role-user');
        Route::post('config-role-user/store', 'Permissions\RolesController@postConfigRoleUser')->name('role.cf-role-user-store');
        Route::get('permission-by-role', 'Permissions\RolesController@getPermissionByRole');
        Route::resource('user-type', Users\TypesController::class);
    });
    Route::group(['prefix' => 'info'], function () {
        Route::post('/project/attachment', 'Projects\ProjectsController@postUploadAttachment')->name('project.upload-attachment');
        Route::get('/project/list', 'Projects\ProjectsController@getList');
        Route::post('/project/{id}', 'Projects\ProjectsController@update');
        Route::get('/project/delete/{id}', 'Projects\ProjectsController@destroy');
        Route::delete('/project/attachment', 'Projects\ProjectsController@postDeleteAttachment')->name('project.delete-attachment');
        Route::get('/project/city/{id}', 'Projects\ProjectsController@getDistrict');
        Route::resource('project-manager', Projects\ProjectManagersController::class);
        Route::resource('type', Projects\TypesController::class);
        Route::resource('segment', Projects\SegmentsController::class);
        Route::resource('project', Projects\ProjectsController::class);

        Route::resource('document-category', Projects\DocumentCategoriesController::class);
        Route::get('document/tree-data', 'Projects\DocumentsController@getTreeData')->name('document.tree-data');
        Route::get('document/upload/{id}', 'Projects\DocumentsController@getUpload')->name('document.upload');
        Route::resource('document', Projects\DocumentsController::class);
        Route::put('/news/accept/{id}', 'Projects\NewsController@putApprove')->name('news.approve');
    });
    Route::post('/upload', 'Administrators\Systems\UploadController@postUpload');

    Route::group(['prefix' => 'customer'], function () {
        Route::get('history/list/{id}', 'Customers\CustomersController@getHistoryToId')->name('customer.histories.list');
        Route::get('district/{id}', 'Customers\CustomersController@getDistrictOnCity')->name('city.district.list');
        Route::get('notification/call_back', 'Customers\CustomersController@listTimeCallBackCustomerOnday')->name('customer.notification.call_back');
        Route::get('project-by-sale/{id}', 'Customers\CustomersController@getProjectBySaleId')->name('customer.project.sale');
        // Thu hồi khách hàng
        Route::get('/list/{id}/collection-customer', 'Customers\CustomersController@collectionCustomer')->name('customer.collection-customer');
        Route::post('/list/{id}/update-collection-customer', 'Customers\CustomersController@updateCollectionCustomer')->name('customer.update-collection-customer');
        Route::get('/list/collection-customer', 'Customers\CustomerSharingUsersController@getCollectionCustomer')->name('customer.list-collection-customer');

        Route::get('upcoming/recover', 'Customers\CustomersController@getUpcomingRecover');
        Route::resource('list', Customers\CustomersController::class);
        Route::resource('level', Customers\LevelsController::class);
        Route::resource('customer_type', Customers\TypeCustomersController::class);
        Route::post('history/save', 'Customers\HistoriesController@saveHistory')->name('customer.history.save');
        Route::get('history/today/{type}', 'Customers\HistoriesController@getTodayCare');

        Route::get('/historyCus', 'Customers\CustomersController@historyCustomer')->name('customer.historyCus');
        Route::post('/getHistoryCus/{id}', 'Customers\CustomersController@getHistoryCustomer')->name('customer.getHistoryCus');
        Route::post('/showHistoryCus', 'Customers\CustomersController@showHistoryCustomer')->name('customer.showHistoryCus');

        Route::resource('history', Customers\HistoriesController::class);

        Route::get('/import', 'Customers\CustomersController@getImportForm')->name('customer.get-import');
        Route::post('/import', 'Customers\CustomersController@postImport')->name('customer.post-import');

        //giao thu contact
        Route::get('/share-customer-by-leader', 'Customers\CustomersController@getShareCustomerByLeader')->name('customer.get-share-customer-by-leader');
        Route::get('/get-total-customer-by-share/{id}', 'Customers\CustomersController@getTotalCustomerByShare')->name('customer.get-total-customer-by-share');
        Route::post('/share-customer-by-leader', 'Customers\CustomersController@postShareCustomerByLeader')->name('customer.post-share-customer-by-leader');


        Route::get('/import-store', 'Customers\CustomersController@getImportStoreForm')->name('customer_stores.get-import');
        Route::post('/import-store', 'Customers\CustomersController@postImportStore')->name('customer_stores.post-import');
        Route::get('/import-store/download-file', 'Customers\CustomersController@downloadFileImport')->name('customer_stores.download');
        Route::get('/stores', 'Customers\CustomersController@indexStores')->name('customer_stores.index');

        Route::post('/sharing/user-by-role/{id}', 'Customers\CustomerSharingsController@getUserByRole');
        Route::post('/sharing/count-customer', 'Customers\CustomerSharingsController@countCustomer');
        Route::get('/sharing/list', 'Customers\CustomerSharingsController@list')->name('sharing.list');
        Route::get('/sharing/sharing-user/{id}', 'Customers\CustomerSharingsController@getSharingUser')->name('sharing.list-user');
        Route::resource('sharing', Customers\CustomerSharingsController::class);
        Route::get('sharing-user/list-cus-share/{id}', 'Customers\CustomerSharingUsersController@listShare')->name('sharing-user.listShare');
        Route::get('sharing-user/list-cus-recovery/{id}', 'Customers\CustomerSharingUsersController@listRecovery')->name('sharing-user.listRecovery');
        Route::resource('sharing-user', Customers\CustomerSharingUsersController::class);


    });
    Route::get('fanpage', 'FacebooksController@getFanpage');
    Route::get('fanpage/post/{id}', 'FacebooksController@getPostByPage')->name('page.post');
    Route::get('fanpage/post/{id}/comments', 'FacebooksController@getCommentByPost')->name('page.post_comment');
    Route::get('fanpage/{pageId}/inbox', 'FacebooksController@getInboxByPage')->name('page.inbox');
    Route::get('fanpage/conversation/{conversationId}', 'FacebooksController@getConversationDetail')->name('page.conversation');

    Route::get('fanpage/find/{url}', 'FacebooksController@getPageByUrl');
    Route::get('fanpage/{pageId}/feed', 'FacebooksController@getFeedsByPage');
    Route::post('fanpage/{pageId}/feed', 'FacebooksController@postFeedToPage');
    Route::post('fanpage/post/{id}', 'FacebooksController@postCommentToPost');
    Route::get('fanpage/conversation/{conversationId}/reply', 'FacebooksController@postCommentToConversation');
    Route::get('fanpage/comments/{commentId}', 'FacebooksController@getCommentChild')->name('comment.child');
    Route::group(['prefix' => 'job'], function () {
        Route::post('report/evaluate/{id}', 'Jobs\ReportsController@postEvaluate')->name('report.evaluate');
        Route::post('task/stability-rate/{id}', 'Jobs\TasksController@postStabilityRate')->name('task.stability-rate');
        Route::post('task/suddenly-rate/{id}', 'Jobs\TasksController@postSuddenlyRate')->name('task.suddenly-rate');
        Route::post('report/approve/{id}', 'Jobs\ReportsController@postApprove')->name('report.approve');
        Route::get('dashboard', 'Jobs\ReportsController@getDashboard')->name('report.statistic');
        Route::get('report/task-suddenly/{type}', 'Jobs\ReportsController@getTaskSuddenly');

        Route::get('report/customer-by-week', 'Jobs\ReportsController@getReportCustomerByWeek')->name('report.customer-by-week');
        Route::get('report/customer-potential-by-project', 'Jobs\ReportsController@getReportCustomerPotentialByProject')->name('report.customer-potential-by-project');
        Route::resource('task', Jobs\TasksController::class);
        Route::resource('target', Jobs\PlansController::class);
        Route::post('/report/{id}', 'Jobs\ReportsController@update');
        Route::get('/export/{type}', 'Jobs\ReportsController@exportReport')->name('report.export');
        Route::get('/report-trial', 'Jobs\ReportsController@indexTrialPeriod')->name('report-trial.index');
        Route::resource('report', Jobs\ReportsController::class);
        Route::get('report-statistic/{type}', 'Jobs\ReportsController@getReport')->name('report.time');
        Route::resource('task-category', Jobs\TaskCategoriesController::class);
    });

    Route::group(['prefix' => 'education'], function () {
        Route::get('/question/list', 'Educations\QuestionsController@getList');
        Route::get('/exam/detail/{id}', 'Educations\ExamsController@getDetail')->name('exam.detail');
        Route::get('/exam/export/{id}', 'Educations\ExamsController@export')->name('exam.export');
        Route::post('/exam/import', 'Educations\ExamsController@import')->name('exam.import');
        Route::patch('/question/list-id', 'Educations\QuestionsController@getListById');
        Route::put('/exam/submit/{id}', 'Educations\ExamsController@putExam')->name('exam.submit');
        Route::get('/program/getsubject/{id}', 'Educations\ProgramsController@getSubjectByProgram');
        Route::resource('program', Educations\ProgramsController::class);
        Route::resource('question', Educations\QuestionsController::class);
        Route::resource('exam', Educations\ExamsController::class);

        Route::post('/subject/attachment', 'Educations\SubjectsController@postUploadAttachment')->name('subject.upload-attachment');
        Route::delete('/subject/attachment', 'Educations\SubjectsController@postDeleteAttachment')->name('subject.delete-attachment');
        Route::get('/subject/show-lesson-by-subject/{id}', 'Educations\SubjectsController@getLessonBySubject')->name('subject.view-lesson');
        Route::post('/subject/document/lesson-subject', 'Educations\SubjectsController@getDataByItemId')->name('edudocument.get-data-by-item');
        Route::delete('/subject/document/delete-doc/{id}', 'Educations\SubjectsController@destroyDoc')->name('edudocument.delete-doc');
        Route::get('/subject/document/download-file/{id}', 'Educations\SubjectsController@downloadFile');
        Route::post('/subject/lesson-subject/upload-file', 'Educations\SubjectsController@postUploadAttachmentByTeacher')->name('subject.upload-file-teacher');
        Route::get('/subject/add-lesson/{id}', 'Educations\SubjectsController@addLessonToSubject')->name('subject.add-lesson-to-subject');
        Route::post('/subject/add-lesson', 'Educations\SubjectsController@storeAddLessonToSubject')->name('subject.store-add-lesson-to-subject');
        Route::get('/subject/search-lesson', 'Educations\SubjectsController@searchLesson')->name('subject.search.lesson');
        Route::delete('/subject/lesson/delete/{id}', 'Educations\SubjectsController@deleteLessonOnSubject')->name('subject.lesson.delete');
        Route::get('/subject/user-reg/{id}', 'Educations\SubjectsController@getAllUserBySubject')->name('subject.user.reg');
        Route::get('/subject/user-reg/{id}/exam-history/{user_id}', 'Educations\SubjectsController@getExamHistory')->name('subject.user.exam-history');
        Route::post('/subject/user-reg/user-assessment', 'Educations\SubjectsController@userAssessment');
        Route::resource('subject', Educations\SubjectsController::class);

        Route::post('/lesson/attachment', 'Educations\LessonsController@postUploadAttachment')->name('lesson.upload-attachment');
        Route::post('/lesson/ajaxget-alllesson/{id}', 'Educations\LessonsController@ajaxGetAllLessonSubject');
        Route::delete('/lesson/attachment', 'Educations\LessonsController@postDeleteAttachment')->name('lesson.delete-attachment');
        Route::get('/lesson/search', 'Educations\LessonsController@getSearch')->name('lesson.search');
        Route::get('subject/list-user/{type}', 'Educations\SubjectsController@getUserByType')->name('subject.get-user-type');
        Route::resource('lesson', Educations\LessonsController::class);
        Route::resource('area', Educations\AreasController::class);
        Route::get('/user/program', 'Educations\ProgramsController@getUser');

        Route::resource('class-room', Educations\ClassRoomsController::class);
    });

    Route::group(['prefix' => 'system-app'], function () {
        Route::get('account/change-password/{id}', 'AppHelperDesk\AccountsController@getChangePassword')->name('account.get-change-password');
        Route::post('account/change-password', 'AppHelperDesk\AccountsController@postChangePassword')->name('account.post-change-password');

        //Transaction
        Route::get('account/list-house/account/{accountId}', 'AppHelperDesk\AccountsController@listHouse')->name('account.list-house');
        Route::post('account/list-transaction', 'AppHelperDesk\AccountsController@listTransaction')->name('account.list-transaction-by-account');
        Route::post('account/add-transaction', 'AppHelperDesk\AccountsController@addTransaction')->name('account.add-transaction');
        Route::get('account/edit-transaction', 'AppHelperDesk\AccountsController@editTransaction')->name('account.edit-transaction');
        Route::post('account/update-transaction', 'AppHelperDesk\AccountsController@updateTransaction')->name('account.update-transaction');
        Route::get('account/transaction/chart', 'AppHelperDesk\AccountsController@chart')->name('account.transaction.chart');

        Route::post('account/del-transaction-house', 'AppHelperDesk\AccountsController@delTransaction')->name('account.del-transaction');
        Route::post('account/add-history-transaction', 'AppHelperDesk\AccountsController@addHistoriesTransaction')->name('account.add-history-transaction');
        Route::post('account/del-histories-transaction', 'AppHelperDesk\AccountsController@delHistoriesTransaction')->name('account.del-history-transaction');
        Route::get('account/download-file', 'AppHelperDesk\AccountsController@downloadFileImport')->name('account.export-file');
        Route::post('account/import-file', 'AppHelperDesk\AccountsController@importAccount')->name('account.import');


        Route::resource('account', AppHelperDesk\AccountsController::class);
        Route::post('chat/search-account', 'AppHelperDesk\ChatController@search')->name('chat.search.account');
        Route::get('chat', 'AppHelperDesk\ChatController@index')->name('account.chat');

        Route::get('list-booking', 'AppHelperDesk\BookingViewHouseController@index')->name('list-booking.list');
        Route::get('list-booking/search', 'AppHelperDesk\BookingViewHouseController@search')->name('list-booking.search');
        Route::get('list-booking/search', 'AppHelperDesk\BookingViewHouseController@search')->name('list-booking.search');
        Route::post('list-booking/update/{id}/status/{status}', 'AppHelperDesk\BookingViewHouseController@updateStatus')->name('list-booking.update');
        Route::get('list-booking/export', 'AppHelperDesk\BookingViewHouseController@exportExcel')->name('list-booking.export');

        Route::get('system/sms/download-file', 'AppHelperDesk\SmsConfigController@exportFileImport')->name('sms.download.import');
        Route::get('system/sms/promotion', 'AppHelperDesk\SmsConfigController@promotion')->name('sms.promotion');
        Route::post('system/sms/promotion', 'AppHelperDesk\SmsConfigController@sendSMSFromFile')->name('sms.send.file');
        Route::resource('system/sms', AppHelperDesk\SmsConfigController::class);

        Route::get('list-customer-promotion', 'AppHelperDesk\CustomerTmpController@index')->name('app.customer.tmp');
        Route::get('list-customer-promotion/search', 'AppHelperDesk\CustomerTmpController@search')->name('app.customer.tmp.search');
        Route::post('list-customer-promotion/import', 'AppHelperDesk\CustomerTmpController@import')->name('app.customer.tmp.import');
        Route::post('list-customer-promotion/update/{id}', 'AppHelperDesk\CustomerTmpController@updateStatus')->name('app.customer.tmp.update');

    });

    Route::group(['prefix' => 'news'], function () {
        Route::resource('news-category', Projects\NewsCategoriesController::class);
        Route::get('promotion', 'Projects\NewsController@promotion')->name('news.promotion');
        Route::get('promotion/edit/{id}', 'Projects\NewsController@promotionEdit')->name('news.promotion.edit');
        Route::put('promotion/update/{id}', 'Projects\NewsController@promotionUpdate')->name('news.promotion.update');
        Route::get('promotion/create', 'Projects\NewsController@promotionCreate')->name('news.promotion.create');
        Route::post('promotion/store', 'Projects\NewsController@promotionStore')->name('news.promotion.store');
        Route::get('promotion/search', 'Projects\NewsController@promotionSearch')->name('news.promotion.search');
        Route::get('search', 'Projects\NewsController@search')->name('news.search');
        Route::resource('news', Projects\NewsController::class);
    });

    Route::group(['prefix' => 'feedback'], function () {
        Route::get('search', 'AppHelperDesk\FeedBackController@search')->name('feedback.search');
        Route::resource('feedback', AppHelperDesk\FeedBackController::class);

    });
    Route::group(['prefix' => 'consignments'], function () {
        Route::get('/', 'AppHelperDesk\ConsignmentController@index')->name('consignments.index');
        Route::get('/search', 'AppHelperDesk\ConsignmentController@search')->name('consignments.search');
        Route::get('/detail/{id}', 'AppHelperDesk\ConsignmentController@show')->name('consignments.show');
        Route::post('/update-status/{id}/status/{status}', 'AppHelperDesk\ConsignmentController@updateStatus')->name('consignments.update-status');
        Route::delete('/delete/{id}', 'AppHelperDesk\ConsignmentController@destroy')->name('consignments.delete');
        Route::get('/export', 'AppHelperDesk\ConsignmentController@exportExcel')->name('consignments.export');

    });

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});
Route::get('/auth/{provider}', 'SocialAuthController@redirectToProvider');
Route::get('/auth/{provide}/callback', 'SocialAuthController@handleProviderCallback');
