<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'user', 'middleware' => 'cors'], function (){
   Route::post('login', 'Apis\Auth\AuthController@postLogin');
   Route::post('forgot', 'Apis\Auth\AuthController@postForgot');
   Route::post('change-password', 'Apis\Customers\AccountsController@postChangePassword')->middleware('auth.customer');
   Route::post('update-profile', 'Apis\Customers\AccountsController@update')->middleware('auth.customer');
   Route::get('get-profile', 'Apis\Customers\AccountsController@getProfile')->middleware('auth.customer');
   Route::post('update-avatar', 'Apis\Customers\AccountsController@postChangeAvatar')->middleware('auth.customer');
   Route::post('logout', 'Apis\Auth\AuthController@logout')->middleware('auth.customer');
   Route::get('get-follower', 'Apis\Customers\AccountsController@getFollower')->middleware('auth.customer');
   Route::post('post-follower', 'Apis\Customers\AccountsController@postFollowNew')->middleware('auth.customer');

    Route::post('update-token-fire-base', 'Apis\Customers\AccountsController@addTokenFireBase')->middleware('auth.customer');
    Route::post('booking-view-house', 'Apis\Customers\BookingViewController@store')->middleware('auth.customer');

});
Route::group(['prefix' => 'consignment', 'middleware' => 'auth.customer'], function (){
    Route::get('get-option', 'Apis\Customers\ConsignmentController@optionConsignment');
    Route::get('list-consignment', 'Apis\Customers\ConsignmentController@index');
    Route::post('add-consignment', 'Apis\Customers\ConsignmentController@storage');
    Route::post('update-consignment', 'Apis\Customers\ConsignmentController@update');
    Route::post('destroy-consignment', 'Apis\Customers\ConsignmentController@updateStatus');
});


Route::group(['prefix' => 'project', 'middleware' => 'auth.customer'], function (){
    Route::get('list-project', 'Apis\Projects\ProjectsController@index');
    Route::post('post-rank-project', 'Apis\Projects\ProjectsController@postRankProject');
    Route::post('list-history-payment', 'Apis\Projects\ProjectsController@getHistoryPayment');

    Route::post('list-sale-project', 'Apis\Projects\ProjectsController@listSaleProject');
    Route::post('post-feedback', 'Apis\Projects\ProjectsController@postFeedBack');

});

Route::group(['prefix' => 'news', 'middleware' => 'auth.customer'], function (){
    Route::get('list-category', 'Apis\News\NewsController@index');
    Route::post('list-news', 'Apis\News\NewsController@getListNew');
    Route::post('view', 'Apis\News\NewsController@view');
});
Route::get('web-view/{slug}', 'Apis\News\NewsController@webView');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/customers/list', 'Apis\Customers\CustomersController@getIndex');
Route::group(['prefix' => 'educations'], function () {
    Route::get('/programs/list', 'Apis\Educations\ProgramsController@getIndex');
    Route::get('/subjects/detail', 'Apis\Educations\SubjectsController@getDetail');
    Route::get('/subjects/category', 'Apis\Educations\SubjectsController@getCategory');
    Route::get('/subjects/getLesson', 'Apis\Educations\SubjectsController@getLessons');
    Route::get('/subjects/list', 'Apis\Educations\SubjectsController@getSubjectHot');
});