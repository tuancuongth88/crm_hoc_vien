<?php

use Illuminate\Database\Seeder;

class OptionSMSSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('options_sms')->insert([
            [
                'name'      => 'Tin nhắn SMS Tạo tài khoản',
                'message'   => 'TAI KHOAN: {phone}. MAT KHAU: {password}. Link tai app android: {android}. Link tai app iOS: {ios}',
                'type'      => 1,
            ],
        ]);
    }
}
