<?php

use Illuminate\Database\Seeder;

class OptionConsignmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('option_consignment')->insert([
            [
                'name'      => 'Cho thuê',
                'parent'    => 0,
                'type'      => 1,
            ],
            [
                'name'      => 'Bán',
                'parent'    => 0,
                'type'      => 1,
            ],
            [
                'name'      => 'Bán căn hộ chung cư',
                'parent'    => 2,
                'type'      => 2,
            ],
            [
                'name'      => 'Bán nhà riêng',
                'parent'    => 2,
                'type'      => 2,
            ],
            [
                'name'      => 'Bán nhà biệt thự, liền kề',
                'parent'    => 2,
                'type'      => 2,
            ],
            [
                'name'      => 'Bán nhà mặt phố',
                'parent'    => 2,
                'type'      => 2,
            ],
            [
                'name'      => 'Bán đất nền dự án',
                'parent'    => 2,
                'type'      => 2,
            ],
            [
                'name'      => 'Bán đất',
                'parent'    => 2,
                'type'      => 2,
            ],
            [
                'name'      => 'Bán trang trại khu nghỉ dưỡng',
                'parent'    => 2,
                'type'      => 2,
            ],
            [
                'name'      => 'Bán kho, nhà xưởng',
                'parent'    => 2,
                'type'      => 2,
            ],
            [
                'name'      => 'Bán các loại bất động sản khác',
                'parent'    => 2,
                'type'      => 2,
            ],
            [
                'name'      => 'Cho thuê căn hộ chung cư',
                'parent'    => 1,
                'type'      => 2,
            ],
            [
                'name'      => 'Cho thuê nhà riêng',
                'parent'    => 1,
                'type'      => 2,
            ],
            [
                'name'      => 'Cho thuê nhà mặt phố',
                'parent'    => 1,
                'type'      => 2,
            ],
            [
                'name'      => 'Cho thuê nhà trọ, phòng trọ',
                'parent'    => 1,
                'type'      => 2,
            ],
            [
                'name'      => 'Cho thuê văn phòng',
                'parent'    => 1,
                'type'      => 2,
            ],
            [
                'name'      => 'Cho thuê cửa hàng, ki ốt',
                'parent'    => 1,
                'type'      => 2,
            ],
            [
                'name'      => 'Cho thuê kho, xưởng, nhà đất',
                'parent'    => 1,
                'type'      => 2,
            ],
            [
                'name'      => 'Cho thuê các loại bất động sản khác',
                'parent'    => 1,
                'type'      => 2,
            ],
            [
                'name'      => 'm2',
                'parent'    => 0,
                'type'      => 3,
            ],
            [
                'name'      => 'm2 Tim tường',
                'parent'    => 0,
                'type'      => 3,
            ],
            [
                'name'      => 'm2 thông thủy',
                'parent'    => 0,
                'type'      => 3,
            ],
            [
                'name'      => 'Triệu',
                'parent'    => 0,
                'type'      => 4,
            ],
            [
                'name'      => 'Tỷ',
                'parent'    => 0,
                'type'      => 4,
            ],
            [
                'name'      => 'Trăm nghìn/m2',
                'parent'    => 0,
                'type'      => 4,
            ],
            [
                'name'      => 'Triệu/m2',
                'parent'    => 0,
                'type'      => 4,
            ],
        ]);
    }
}
