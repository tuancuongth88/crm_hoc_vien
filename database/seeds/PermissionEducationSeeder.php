<?php

use Illuminate\Database\Seeder;

class PermissionEducationSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('permissions')->insert([
            [
                'name'         => 'program.create',
                'display_name' => 'Thêm lĩnh vực',
            ],
            [
                'name'         => 'program.update',
                'display_name' => 'Cập nhật lĩnh vực',
            ],
            [
                'name'         => 'program.delete',
                'display_name' => 'xóa lĩnh vực',
            ],
            [
                'name'         => 'program.view',
                'display_name' => 'Xem lĩnh vực',
            ],

            // Chương trình
            [
                'name'         => 'subject.create',
                'display_name' => 'Thêm chương trình',
            ],
            [
                'name'         => 'subject.update',
                'display_name' => 'Cập nhật chương trình',
            ],
            [
                'name'         => 'subject.delete',
                'display_name' => 'xóa chương trình',
            ],
            [
                'name'         => 'subject.view',
                'display_name' => 'Xem chương trình',
            ],
            [
                'name'         => 'subject.user.reg',
                'display_name' => 'Xem danh sách học viên đăng ký',
            ],
            [
                'name'         => 'subject.user.assessment',
                'display_name' => 'Đánh giá học lực học viên',
            ],
            [
                'name'         => 'subject.view-lesson',
                'display_name' => 'Xem danh sách khóa học theo chương trình',
            ],
            [
                'name'         => 'subject.add-lesson-to-subject',
                'display_name' => 'Thêm khóa học vào chương trình',
            ],
            [
                'name'         => 'subject.remove-lesson-to-subject',
                'display_name' => 'Xóa khóa học khỏi chương trình',
            ],
            //khóa học
            [
                'name'         => 'lesson.create',
                'display_name' => 'Thêm khóa học',
            ],
            [
                'name'         => 'lesson.update',
                'display_name' => 'Cập nhật khóa học',
            ],
            [
                'name'         => 'lesson.delete',
                'display_name' => 'Xóa khóa học',
            ],
            [
                'name'         => 'lesson.view',
                'display_name' => 'Xem khóa học',
            ],

            // Ngân hàng câu hỏi
            [
                'name'         => 'question.create',
                'display_name' => 'Thêm Câu hỏi',
            ],
            [
                'name'         => 'question.update',
                'display_name' => 'Cập nhật Câu hỏi',
            ],
            [
                'name'         => 'question.delete',
                'display_name' => 'Xóa Câu hỏi',
            ],
            [
                'name'         => 'question.view',
                'display_name' => 'Xem Câu hỏi',
            ],

            //Kỳ thi
            [
                'name'         => 'exam.create',
                'display_name' => 'Thêm kỳ thi',
            ],
            [
                'name'         => 'exam.update',
                'display_name' => 'Cập nhật kỳ thi',
            ],
            [
                'name'         => 'exam.delete',
                'display_name' => 'Xóa kỳ thi',
            ],
            [
                'name'         => 'exam.view',
                'display_name' => 'Xem kỳ thi',
            ],
            [
                'name'         => 'student.list',
                'display_name' => 'Xem danh sách học viên',
            ],
            [
                'name'         => 'exam.import.point',
                'display_name' => 'Import điểm học viên',

            ],
        ]);
    }
}