<?php

use Illuminate\Database\Seeder;

class NewsCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects_news_categories')->insert([
        [
            'name' => 'Tiến độ dự án',
            'slug' => 'tien-do-du-an',
        ],
        [
            'name' => 'Tin dự án',
            'slug' => 'tin-du-an',
        ],
        [
            'name' => 'Tin khuyến mại',
            'slug' => 'tin-khuyen-mai',
        ],
        [
            'name' => 'Thông báo',
            'slug' => 'thong-bao',
        ],
    ]);
    }
}
