<?php

use Illuminate\Database\Seeder;

class LevelSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('levels')->insert([
            [
                'name' => 'Khách hàng tiêm năng',
                'type' => 1,
            ],
            [
                'name' => 'Khách hàng mục tiêu',
                'type' => 1,
            ],
            [
                'name' => 'Khách hàng nét',
                'type' => 1,
            ],
        ]);
    }
}
