<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')->insert([
            'username'      => 'admin',
            'email'         => 'admin@nghetuvan.com',
            'fullname'      => 'Admin',
            'avatar'        => '/no-avatar.ico',
            // 'birthday'      => '01-01-2016',
            'password'      => \Hash::make(123456),
            'department_id' => 1,
            'branch_id'     => 1,
            'company_id'    => 1,
            'position_id'   => 1,
            'active'        => 1,
        ]);
    }
}
