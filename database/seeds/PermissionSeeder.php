<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('permissions')->insert([
            [
                'name'         => 'report.approve',
                'display_name' => 'Duyệt báo cáo',
            ],
            [
                'name'         => 'news.approve',
                'display_name' => 'Tạo mới tin',
            ],
            // [
            //     'name'         => 'user.create',
            //     'display_name' => 'Thêm user',
            // ],
            // [
            //     'name'         => 'user.update',
            //     'display_name' => 'Cập nhật user',
            // ],
            // [
            //     'name'         => 'user.delete',
            //     'display_name' => 'Xóa user',
            // ],
            // [
            //     'name'         => 'user.view',
            //     'display_name' => 'Xem user',
            // ],
            // [
            //     'name'         => 'department.create',
            //     'display_name' => 'Thêm phòng ban',
            // ],
            // [
            //     'name'         => 'department.update',
            //     'display_name' => 'Cập nhật phòng ban',
            // ],
            // [
            //     'name'         => 'department.delete',
            //     'display_name' => 'Xóa phòng ban',
            // ],
            // [
            //     'name'         => 'department.view',
            //     'display_name' => 'Xem phòng ban',
            // ]

            // , [
            //     'name'         => 'branch.create',
            //     'display_name' => 'Thêm chi nhánh',
            // ],
            // [
            //     'name'         => 'branch.update',
            //     'display_name' => 'Cập nhật chi nhánh’',
            // ],
            // [
            //     'name'         => 'branch.delete',
            //     'display_name' => 'Xóa chi nhánh’',
            // ],
            // [
            //     'name'         => 'branch.view',
            //     'display_name' => 'Xem chi nhánh’',
            // ]

            // , [
            //     'name'         => 'company.create',
            //     'display_name' => 'Thêm công ty',
            // ],
            // [
            //     'name'         => 'company.update',
            //     'display_name' => 'Cập nhật công ty',
            // ],
            // [
            //     'name'         => 'company.delete',
            //     'display_name' => 'Xóa công ty',
            // ],
            // [
            //     'name'         => 'company.view',
            //     'display_name' => 'Xem công ty',
            // ]

            // , [
            //     'name'         => 'position.create',
            //     'display_name' => 'Thêm chức vụ',
            // ],
            // [
            //     'name'         => 'position.update',
            //     'display_name' => 'Cập nhật chức vụ',
            // ],
            // [
            //     'name'         => 'position.delete',
            //     'display_name' => 'Xóa chức vụ',
            // ],
            // [
            //     'name'         => 'position.view',
            //     'display_name' => 'Xem chức vụ',
            // ],

            [
                'name'         => 'type.create',
                'display_name' => 'Thêm loại dự án',
            ],
            [
                'name'         => 'type.update',
                'display_name' => 'Cập nhật loại dự án',
            ],
            [
                'name'         => 'type.delete',
                'display_name' => 'Xóa loại dự án',
            ],
            [
                'name'         => 'type.view',
                'display_name' => 'Xem loại dự án',
            ],
            [
                'name'         => 'project.create',
                'display_name' => 'Thêm dự án',
            ],
            [
                'name'         => 'project.update',
                'display_name' => 'Cập nhật dự án',
            ],
            [
                'name'         => 'project.delete',
                'display_name' => 'Xóa dự án',
            ],
            [
                'name'         => 'project.view',
                'display_name' => 'Xem dự án',
            ],
            [
                'name'         => 'news-category.create',
                'display_name' => 'Thêm danh mục tin dự án',
            ],
            [
                'name'         => 'news-category.update',
                'display_name' => 'Cập nhật danh mục tin dự án',
            ],
            [
                'name'         => 'news-category.delete',
                'display_name' => 'Xóa danh mục tin dự án',
            ],
            [
                'name'         => 'news-category.view',
                'display_name' => 'Xem danh mục tin dự án',
            ],
            [
                'name'         => 'news.create',
                'display_name' => 'Thêm tin dự án',
            ],
            [
                'name'         => 'news.update',
                'display_name' => 'Cập nhật tin dự án',
            ],
            [
                'name'         => 'news.delete',
                'display_name' => 'Xóa tin dự án',
            ],
            [
                'name'         => 'news.view',
                'display_name' => 'Xem tin dự án',
            ],
            [
                'name'         => 'document-category.create',
                'display_name' => 'Thêm tài liệu',
            ],
            [
                'name'         => 'document-category.update',
                'display_name' => 'Cập nhật tài liệu',
            ],
            [
                'name'         => 'document-category.delete',
                'display_name' => 'Xóa tài liệu',
            ],
            [
                'name'         => 'document-category.view',
                'display_name' => 'Xem tài liệu',
            ],
            // [
            //     'name'         => 'role.create',
            //     'display_name' => 'Thêm vai trò phân quyền',
            // ],
            // [
            //     'name'         => 'role.update',
            //     'display_name' => 'Cập nhật vai trò phân quyền',
            // ],
            // [
            //     'name'         => 'role.delete',
            //     'display_name' => 'Xóa vai trò phân quyền',
            // ],
            // [
            //     'name'         => 'role.view',
            //     'display_name' => 'Xóa vai trò phân quyền',
            // ],
            [
                'name'         => 'list.create',
                'display_name' => 'Thêm danh sách khách hàng',
            ],
            [
                'name'         => 'list.update',
                'display_name' => 'Cập nhật danh sách khách hàng',
            ],
            [
                'name'         => 'list.delete',
                'display_name' => 'Xóa danh sách khách hàng',
            ],
            [
                'name'         => 'list.view',
                'display_name' => 'Xem danh sách khách hàng',
            ],
            [
                'name'         => 'customer_type.create',
                'display_name' => 'Thêm thể loại khách hàng',
            ],
            [
                'name'         => 'customer_type.update',
                'display_name' => 'Cập nhật thể loại khách hàng',
            ],
            [
                'name'         => 'customer_type.delete',
                'display_name' => 'Xóa thể loại khách hàng',
            ],
            [
                'name'         => 'customer_type.view',
                'display_name' => 'Xem thể loại khách hàng',
            ],
            [
                'name'         => 'level.create',
                'display_name' => 'Thêm cấp độ khách hàng',
            ],
            [
                'name'         => 'level.update',
                'display_name' => 'Cập nhật cấp độ khách hàng',
            ],
            [
                'name'         => 'level.delete',
                'display_name' => 'Xóa cấp độ khách hàng',
            ],
            [
                'name'         => 'level.view',
                'display_name' => 'Xem cấp độ khách hàng',
            ],
            [
                'name'         => 'task.create',
                'display_name' => 'Thêm công việc',
            ],
            [
                'name'         => 'task.update',
                'display_name' => 'Cập nhật công việc',
            ],
            [
                'name'         => 'task.delete',
                'display_name' => 'Xóa công việc',
            ],
            [
                'name'         => 'task.view',
                'display_name' => 'Xem công việc',
            ],
            [
                'name'         => 'target.create',
                'display_name' => 'Thêm mục tiêu công việc',
            ],
            [
                'name'         => 'target.update',
                'display_name' => 'Cập nhật mục tiêu công việc',
            ],
            [
                'name'         => 'target.delete',
                'display_name' => 'Xóa mục tiêu công việc',
            ],
            [
                'name'         => 'target.view',
                'display_name' => 'Xem mục tiêu công việc',
            ],
            [
                'name'         => 'report.create',
                'display_name' => 'Thêm báo cáo công việc',
            ],
            [
                'name'         => 'report.update',
                'display_name' => 'Cập nhật báo cáo công việc',
            ],
            [
                'name'         => 'report.delete',
                'display_name' => 'Xóa báo cáo công việc',
            ],
            [
                'name'         => 'report.view',
                'display_name' => 'Xem báo cáo công việc',
            ], [
                'name'         => 'report.list',
                'display_name' => 'Xem danh sách báo cáo',
            ],
            [
                'name'         => 'task-category.create',
                'display_name' => 'Thêm danh mục công việc',
            ],
            [
                'name'         => 'task-category.update',
                'display_name' => 'Cập nhật danh mục công việc',
            ],
            [
                'name'         => 'task-category.delete',
                'display_name' => 'Xóa danh mục công việc',
            ],
            [
                'name'         => 'task-category.view',
                'display_name' => 'Xem danh mục công việc',
            ],
            [
                'name'         => 'project-manager.create',
                'display_name' => 'Thêm dự án quản lý',
            ],
            [
                'name'         => 'project-manager.update',
                'display_name' => 'Cập nhật dự án quản lý',
            ],
            [
                'name'         => 'project-manager.delete',
                'display_name' => 'Xóa danh dự án quản lý',
            ],
            [
                'name'         => 'project-manager.view',
                'display_name' => 'Xem danh mục dự án quản lý',
            ],
            [
                'name'         => 'is-manager',
                'display_name' => 'Quyền quản lý',
            ],
            [
                'name'         => 'task.rate',
                'display_name' => 'Đánh giá công việc',
            ],
            [
                'name'         =>'is-admin',
                'display_name' => 'Quyền quản trị Toàn bộ hệ thống',
            ],
            [
                'name'         =>'is-hanh-chinh',
                'display_name' => 'Quyền hành chính',
            ],
            [
                'name'         =>'is-student',
                'display_name' => 'Quyền học viên',
            ],
            [
                'name'         =>'is-teacher',
                'display_name' => 'Quyền giáo viên',
            ],
            [
                'name'         =>'is-education',
                'display_name' => 'Quyền đào tạo',
            ],
            [
                'name'         =>'is-admin-customer',
                'display_name' => 'Quản trị khách hàng',
            ],
            [
                'name'          =>'is-telesale',
                'display_name'  => 'Quyền thu hồi khách hàng đã chia',
            ],
        ]);
    }
}
