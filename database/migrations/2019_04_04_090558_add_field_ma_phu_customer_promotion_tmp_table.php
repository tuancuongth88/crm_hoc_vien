<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldMaPhuCustomerPromotionTmpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customer_promotion_tmp', function (Blueprint $table) {
            $table->integer('so_ma')->nullable()->default(1);
            $table->string('ma_phu', 250)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customer_promotion_tmp', function (Blueprint $table) {
            $table->dropColumn('so_ma');
            $table->dropColumn('ma_phu');
        });
    }
}
