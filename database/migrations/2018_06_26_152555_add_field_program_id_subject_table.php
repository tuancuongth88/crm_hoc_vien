<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldProgramIdSubjectTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('education_subjects', function (Blueprint $table) {
            $table->integer('program_id')->after('created_by')->nullable();
            $table->integer('status')->after('program_id')->nullable()->default(0)->comment('1: xuat ban, 0: khong xuat ban');
            $table->integer('type_user')->after('status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('education_subjects', function (Blueprint $table) {
            $table->dropColumn('program_id');
            $table->dropColumn('status');
            $table->dropColumn('type_user');
        });
    }
}
