<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomerPromotionTmpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_promotion_tmp', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 250)->nullable();
            $table->string('phone', 250)->nullable();
            $table->string('ma_can', 250)->nullable();
            $table->string('du_an', 250)->nullable();
            $table->string('ngay_vao_hop_dong', 250)->nullable();
            $table->string('cmt', 250)->nullable();
            $table->string('maso', 250)->nullable();
            $table->integer('status')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_promotion_tmp');
    }
}
