<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class TaskCategoriesTable.
 */
class CreateTaskCategoriesTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */

    // public function up() {
    //     Schema::create('task_categories', function (Blueprint $table) {
    //         $table->increments('id');
    //         $table->string('name');
    //         $table->string('description')->nullable();
    //         $table->integer('created_by')->nullable();
    //         $table->integer('updated_by')->nullable();
    //         $table->softDeletes();
    //         $table->timestamps();
    //     });
    // }

    // /**
    //  * Reverse the migrations.
    //  *
    //  * @return void
    //  */
    // public function down() {
    //     Schema::drop('task_categories');
    // }
}
