<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class QuestionsTable.
 */
class CreateEducationQuestionsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('education_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('name');
            $table->integer('level')->default(0);
            $table->integer('subject_id');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('education_questions');
    }
}
