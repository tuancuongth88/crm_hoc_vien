<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMoreFieldCustomerTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('parent')->nullable();
            $table->string('relation')->nullable()->comment('Quản hệ người thân');
            $table->float('finance_min', 15, 0)->nullable();
            $table->float('finance_max', 15, 0)->nullable();
            $table->text('info')->nullable()->comment('sở thích nhu câu');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('parent');
            $table->dropColumn('relation');
            $table->dropColumn('finance_min');
            $table->dropColumn('finance_max');
            $table->dropColumn('info');
        });
    }
}
