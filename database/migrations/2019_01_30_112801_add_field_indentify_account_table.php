<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldIndentifyAccountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->string('identity', 250)->nullable()->after('last_login');
            $table->string('sms_code', 250)->nullable()->after('identity');
            $table->string('sms_response', 250)->nullable()->after('sms_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('accounts', function (Blueprint $table) {
            $table->dropColumn('identity');
            $table->dropColumn('sms_code');
            $table->dropColumn('sms_response');
        });
    }
}
