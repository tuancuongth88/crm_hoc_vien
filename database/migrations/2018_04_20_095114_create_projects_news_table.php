<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class NewsTable.
 */
class CreateProjectsNewsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('projects_news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('description', 250)->nullable();
            $table->longText('content')->nullable();
            $table->integer('is_comment')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('category_id')->nullable();
            $table->integer('is_approve')->nullable();
            $table->string('image_url')->nullable();
            $table->string('title_meta')->nullable();
            $table->string('description_meta')->nullable();
            $table->string('keyword_meta')->nullable();
            $table->timestamp('send_at')->nullable();
            $table->timestamp('approve_time')->nullable();
            $table->integer('author_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('projects_news');
    }
}
