<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationFirebaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_firebase', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->nullable();
            $table->integer('news_id')->nullable();
            $table->string('device_id', 250)->nullable()->comment('Thiet bi');
            $table->string('token', 250)->nullable()->comment('Ma code tren fire base tra ve');
            $table->integer('response_code')->nullable()->comment('Ma code tren fire base tra ve');
            $table->string('results', 250)->nullable();
            $table->dateTime('send_at')->nullable()->comment('Thời gian gửi notification');
            $table->dateTime('read_at')->nullable()->comment('Thoi gian xem');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_firebase');
    }
}
