<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CustomersTable.
 */
class CreateCustomersTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 255);
            $table->string('avatar', 255)->nullable();
            $table->string('phone', 255);
            $table->string('email', 255)->nullable();
            $table->timestamp('birthday')->nullable();
            $table->integer('level')->nullable()->comment('đánh giá mức độ khách hàng tiềm năng hay không');
            $table->integer('type')->nullable()->comment('thể loại khách hàng');
            $table->integer('user_id')->nullable()->comment('tư vấn viên chăm sóc');
            $table->string('address', 255)->nullable();
            $table->timestamp('assign_at')->nullable()->comment('thời gian giao khách hàng cho sale');;
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customers');
	}
}
