<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeSalerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_saler', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 250)->nullable();
            $table->integer('user_id')->nullable();
            $table->string('email', 250)->nullable();
            $table->string('phone', 250)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_saler');
    }
}
