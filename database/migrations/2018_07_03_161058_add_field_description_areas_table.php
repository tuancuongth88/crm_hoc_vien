<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldDescriptionAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('education_areas', function (Blueprint $table) {
            $table->string('slug')->after('name')->nullable();
            $table->text('description')->after('slug')->nullable();
            $table->string('image_url', 255)->after('description')->nullable();
            $table->string('icon_url', 255)->after('image_url')->nullable();
            $table->integer('position')->after('icon_url')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(  'education_areas', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('image_url');
            $table->dropColumn('icon_url');
            $table->dropColumn('position');
            $table->dropColumn('slug');
        });
    }
}
