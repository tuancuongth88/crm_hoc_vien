<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmailConfigTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('email_config', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email_sender')->nullable();
            $table->string('driver')->nullable();
            $table->string('fullname')->nullable();
            $table->string('password')->nullable();
            $table->longText('email_to')->nullable();
            $table->string('host')->nullable();
            $table->string('port')->nullable();
            $table->string('smtpsecure')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('email_config');
    }
}
