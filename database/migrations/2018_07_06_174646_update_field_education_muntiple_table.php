<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateFieldEducationMuntipleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('education_programs', function (Blueprint $table) {
            $table->dropColumn(['area']);
            $table->dropColumn(['is_hot']);
            $table->dropColumn(['short_name']);
            $table->string('image_url', 255)->after('name')->nullable();
            $table->string('icon_url', 255)->after('image_url')->nullable();
        });

        Schema::table('education_subjects', function (Blueprint $table) {
            $table->dropColumn(['thongdiep']);
            $table->string('short_name', 255)->after('name')->nullable()->comment('Tên viết tắt');
            $table->text('message')->after('short_name')->nullable()->comment('thông điệp');
            $table->integer('position')->after('message')->nullable()->comment('Trọng số hiển thị');
            $table->integer('is_hot')->after('position')->nullable()->comment('Đánh dấu hot');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('education_programs', function (Blueprint $table) {
            $table->dropColumn(['image_url']);
            $table->dropColumn(['icon_url']);
            $table->dropColumn(['position']);
        });
        Schema::table('education_subjects', function (Blueprint $table) {
            $table->dropColumn(['message']);
            $table->dropColumn(['short_name']);
            $table->dropColumn(['position']);
            $table->dropColumn(['is_hot']);
        });
    }
}
