<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingViewHouseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking_view_house', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id');
            $table->string('fullname', 250)->nullable();
            $table->string('phone', 250)->nullable();
            $table->string('email', 250)->nullable();
            $table->dateTime('time_view')->nullable();
            $table->text('note')->nullable();
            $table->integer('status')->nullable()->default(0)->comment('0: vua tao, 1: da dan khach, 2: huy');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_view_house');
    }
}
