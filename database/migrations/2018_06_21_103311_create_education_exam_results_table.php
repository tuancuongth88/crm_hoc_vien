<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class ExamResultsTable.
 */
class CreateEducationExamResultsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('education_exam_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('exam_id');
            $table->integer('point')->nullable();
            $table->text('answers')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('education_exam_results');
    }
}
