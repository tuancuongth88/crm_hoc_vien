<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldEducationSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('education_subjects', function (Blueprint $table) {
            $table->text('thongdiep')->after('name')->nullable()->comment('Thông điệp');
            $table->text('target')->after('thongdiep')->nullable()->comment('Mục tiêu');
            $table->timestamp('open_time')->after('target')->nullable()->comment('Thời gian bắt đầu');
            $table->timestamp('close_time')->after('open_time')->nullable()->comment('Thời gian kết thúc');
            $table->string('schedule', 255)->after('close_time')->nullable()->comment('lịch học');
            $table->string('school_time', 255)->after('schedule')->nullable()->comment('giờ học');
            $table->string('total_time', 255)->after('school_time')->nullable()->comment('thời lượng');
            $table->string('total_fee', 255)->after('total_time')->nullable()->comment('Tổng tiền khóa học');
            $table->integer('fee_discount')->after('total_fee')->nullable()->comment('Chiết khấu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('education_subjects', function (Blueprint $table) {
            $table->dropColumn('thongdiep');
            $table->dropColumn('target');
            $table->dropColumn('open_time');
            $table->dropColumn('close_time');
            $table->dropColumn('schedule');
            $table->dropColumn('school_time');
            $table->dropColumn('total_time');
            $table->dropColumn('total_fee');
            $table->dropColumn('fee_discount');
        });
    }
}
