<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldDescriptionProgramTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('education_programs', function (Blueprint $table) {
            $table->string('short_name', 255)->after('name')->nullable();
            $table->text('description')->after('short_name')->nullable();
            $table->string('position', 255)->after('description')->nullable();
            $table->integer('is_hot')->after('position')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('education_programs', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('short_name');
            $table->dropColumn('position');
            $table->dropColumn('is_hot');
        });
    }
}
