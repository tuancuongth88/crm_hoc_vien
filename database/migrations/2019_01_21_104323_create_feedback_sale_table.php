<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackSaleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('feedback_sale', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->nullable()->comment('id khach hang');
            $table->integer('transaction_id')->nullable()->comment('Mã id căn');
            $table->integer('sale_id')->nullable()->comment('id nhan vien kinh doanh');
            $table->integer('rank_sale')->nullable()->comment('diem danh gia');
            $table->text('note')->nullable()->comment('mo ta danh gia nhan vien');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_sale');
    }
}
