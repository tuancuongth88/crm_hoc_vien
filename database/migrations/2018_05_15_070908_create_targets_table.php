<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class TargetsTable.
 */
class CreateTargetsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('targets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('task');
            $table->string('target');
            $table->timestamp('day')->nullable();
            $table->integer('month')->nullable();
            $table->integer('year')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('targets');
    }
}
