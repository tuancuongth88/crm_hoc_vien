<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldAppMobileProjectManagerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_managers', function (Blueprint $table) {
            $table->string('image_url', 250)->nullable()->after('name');
            $table->string('email', 250)->nullable()->after('image_url');
            $table->string('hotline', 250)->nullable()->after('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_managers', function (Blueprint $table) {
            $table->dropColumn('image_url');
            $table->dropColumn('email');
            $table->dropColumn('hotline');
        });
    }
}
