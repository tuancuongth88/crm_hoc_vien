<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTokenFirebaseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_token_firebase', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id');
            $table->integer('type_device')->nullable()->comment('1: android, 2: ios, 3: other');
            $table->string('device_name',255)->nullable();
            $table->string('device_id',255)->nullable();
            $table->text('token')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_token_firebase');
    }
}
