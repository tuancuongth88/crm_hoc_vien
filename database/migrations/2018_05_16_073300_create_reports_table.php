<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class ReportsTable.
 */
class CreateReportsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_website')->nullable();
            $table->longText('post_website_detail')->nullable();
            $table->integer('post_facebook')->nullable();
            $table->longText('post_facebook_detail')->nullable();
            $table->integer('post_news')->nullable();
            $table->longText('post_news_detail')->nullable();
            $table->integer('post_forum')->nullable();
            $table->longText('post_forum_detail')->nullable();
            $table->integer('update_info_market')->nullable();
            $table->integer('update_info_project')->nullable();
            $table->integer('flypaper_personal')->nullable();
            $table->string('flypaper_personal_time')->nullable();
            $table->integer('flypaper_group')->nullable();
            $table->string('flypaper_group_time')->nullable();
            $table->integer('questionair')->nullable();
            $table->string('questionair_time')->nullable();
            $table->integer('guard_project_in_hour')->nullable();
            $table->string('guard_project_in_hour_time')->nullable();
            $table->integer('guard_project_out_hour')->nullable();
            $table->string('guard_project_out_hour_time')->nullable();
            $table->integer('open_sale')->nullable();
            $table->string('open_sale_time')->nullable();
            $table->integer('roadshow')->nullable();
            $table->string('roadshow_time')->nullable();
            $table->integer('event')->nullable();
            $table->string('event_time')->nullable();
            $table->integer('find_public')->nullable();
            $table->string('find_public_time')->nullable();
            $table->integer('setup_appointment')->nullable();
            $table->string('setup_appointment_time')->nullable();
            $table->integer('total')->nullable();
            $table->string('total_time')->nullable();
            $table->integer('like')->nullable();
            $table->integer('comment')->nullable();
            $table->integer('share')->nullable();

            $table->string('name')->nullable();
            $table->integer('is_approve')->default(0);
            $table->longText('evaluate')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('reports');
    }
}
