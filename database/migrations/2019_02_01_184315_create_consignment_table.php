<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsignmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consignment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id');
            $table->string('fullname', 250)->nullable()->comment('Ten nguoi cho thue');
            $table->string('phone', 250)->nullable()->comment('sdt nguoi cho thue');
            $table->string('email', 250)->nullable()->comment('email nguoi cho thue');
            $table->string('title', 255)->nullable();
            $table->integer('product_type')->comment('Hinh thuc');
            $table->integer('land_type')->comment('loại bds');
            $table->string('address', 255)->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('district_id')->nullable();
            $table->float('area')->nullable()->comment('dien tich');
            $table->integer('area_type')->nullable()->comment('loai dien tich');
            $table->float('price')->nullable()->comment('gia tien');
            $table->integer('price_type')->nullable()->comment('gia tien');
            $table->text('description')->nullable()->comment('Mo ta');
            $table->integer('status')->nullable()->comment('1: đang tiếp nhận, 2 đã nhận, 3 hoàn thành, 4: hủy');
            $table->text('images')->nullable()->comment('chuoi json anh');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consignment');
    }
}
