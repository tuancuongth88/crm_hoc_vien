<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationUserRegProgram extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_user_reg_program', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->integer('subject_id')->nullable();
            $table->timestamp('open_time')->nullable();
            $table->timestamp('close_time')->nullable();
            $table->integer('pass')->nullable();
            $table->integer('avg_point')->nullable();
            $table->integer('total_point')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('education_user_reg_program');
    }
}
