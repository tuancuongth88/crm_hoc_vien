<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEducationRelationSubjectRoom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_relation_subject_room', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('room_id')->comment('id của lớp');
            $table->integer('subject_id')->comment('id khoa hoc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('education_relation_subject_room');
    }
}
