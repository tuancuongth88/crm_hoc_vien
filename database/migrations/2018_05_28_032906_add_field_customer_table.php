<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('sex')->after('email')->default('0')->nullable();
            $table->string('first_name', 255)->after('fullname')->nullable();
            $table->string('last_name', 255)->after('fullname')->nullable();
            $table->integer('city_id')->after('fullname')->nullable();
            $table->integer('district_id')->after('fullname')->nullable();
            $table->string('cmt', 255)->after('fullname')->nullable();
            $table->timestamp('ngay_cap_cmt')->after('fullname')->nullable();
            $table->string('noi_cap_cmt', 255)->after('fullname')->nullable();
            $table->integer('source_id')->after('fullname')->nullable();
            $table->string('channel', 255)->after('fullname')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function(Blueprint $table) {
            $table->dropColumn(['sex', 'first_name', 'last_name', 'city_id', 'district_id', 'cmt', 'ngay_cap_cmt', 'noi_cap_cmt', 'source_id', 'channel']);
        });
    }
}
