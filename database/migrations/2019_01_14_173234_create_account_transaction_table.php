<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_transaction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('name', 255)->nullable();
            $table->integer('parent_id')->default(0);
            $table->dateTime('date_payment')->nullable();
            $table->integer('status')->default(0)->comment(' trang thai thanh toan 0: chua thanh toan, 1: da thanh toan');
            $table->integer('account_id');
            $table->integer('sale_id');
            $table->string('total_money', 255)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_transaction');
    }
}
