<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendSmsLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('send_sms_log', function (Blueprint $table) {
            $table->increments('id');
            $table->string('model', '250')->nullable();
            $table->integer('item_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('phone', 255)->nullable();
            $table->text('content')->nullable();
            $table->integer('sms_code')->nullable();
            $table->text('sms_response')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('send_sms_log');
    }
}
