<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldSkillTeacherEducationUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('education_user_info', function (Blueprint $table) {
            $table->text('skill_teacher')->after('experienced_param')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('education_user_info', function (Blueprint $table) {
            $table->dropColumn('skill_teacher');
        });
    }
}
