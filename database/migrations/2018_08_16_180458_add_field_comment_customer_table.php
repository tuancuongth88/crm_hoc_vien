<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldCommentCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('share_id')->nullable()->comment('Chiến dịch');
            $table->json('comment')->nullable();
            $table->integer('status')->nullable()->comment('Trạng thái khách hàng');
            $table->integer('total_assign')->nullable()->comment('Tổng số lần giao');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('share_id');
            $table->dropColumn('comment');
            $table->dropColumn('status');
            $table->dropColumn('total_assign');

        });
    }
}
