<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportAttachmentsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('report_attachments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_id');
            $table->integer('original_id');
            $table->string('field')->nullable();
            $table->string('name');
            $table->string('url');
            $table->integer('size');
            $table->string('extendsion');
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('report_attachments');
    }
}
