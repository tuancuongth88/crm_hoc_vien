<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateEducationUserInfosTable.
 */
class CreateEducationUserInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('education_user_info', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->nullable();
            $table->string('hobby', 500)->comment('sở thích')->nullable();
            $table->string('target', 500)->comment('mục tiêu')->nullable();
            $table->string('advantage', 500)->comment('ưu điểm')->nullable();
            $table->string('defect', 500)->comment('nhược điểm')->nullable();
            $table->string('skype')->comment('dia chi skype')->nullable();
            $table->string('viber')->comment('dia chi viber')->nullable();
            $table->string('zalo')->comment('dia chi zalo')->nullable();
            $table->integer('marriage')->comment('Hôn nhân')->nullable();
            $table->date('identity_date')->comment('Ngày cấp cmt')->nullable();
            $table->string('identity_place_release')->comment('Nơi cấp cmt')->nullable();
            $table->string('literacy', 255)->comment('Các trình độ học vấn')->nullable();
            $table->text('literacy_param')->comment('chi tiết trình độ học vấn')->nullable();
            $table->text('experienced_param')->comment('chi tiết kinh nghiệm')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()

    {
        Schema::drop('education_user_infos');
    }
}
