<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

/**
 * Class ProjectsTable.
 */
class CreateProjectsTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('investor')->nullable();
            $table->integer('type')->nullable();
            $table->string('address')->nullable();
            $table->integer('district_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->longText('content')->nullable();
            $table->longText('area_position')->nullable();
            $table->longText('utilitie_local')->nullable();
            $table->longText('area_connect')->nullable();
            $table->longText('area_ground')->nullable();
            $table->string('image_url')->nullable();
            $table->integer('male')->nullable();
            $table->integer('female')->nullable();
            $table->integer('customer_age_from')->nullable();
            $table->integer('customer_age_to')->nullable();
            $table->longText('customer_job')->nullable();
            $table->longText('customer_description')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('projects');
    }
}
