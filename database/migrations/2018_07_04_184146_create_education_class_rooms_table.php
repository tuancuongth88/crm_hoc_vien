<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class ClassRoomsTable.
 */
class CreateEducationClassRoomsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('education_class_rooms', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255)->comment('Tên lớp');
            $table->string('description', 255)->comment('Mô tả');
            $table->integer('taget')->default(0)->comment('Sô chỉ tiêu khóa học phải hoàn thành');
            $table->integer('created_by')->default(0)->comment('Người tạo');
            $table->integer('updated_by')->default(0)->comment('Người cập nhật');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('education_class_rooms');
	}
}
