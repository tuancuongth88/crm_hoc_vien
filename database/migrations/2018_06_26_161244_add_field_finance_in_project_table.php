<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldFinanceInProjectTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('projects', function (Blueprint $table) {
            $table->string('finance')->nullable();
            $table->string('profile')->nullable();
            $table->string('salekit')->nullable();
            $table->string('qa')->nullable();
            $table->string('contract')->nullable();
            $table->string('policy')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('finance');
            $table->dropColumn('profile');
            $table->dropColumn('salekit');
            $table->dropColumn('qa');
            $table->dropColumn('contract');
            $table->dropColumn('policy');
        });
    }
}
