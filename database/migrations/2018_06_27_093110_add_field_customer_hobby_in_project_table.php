<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldCustomerHobbyInProjectTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('projects', function (Blueprint $table) {
            $table->longText('customer_require')->nullable();
            $table->longText('customer_hobby')->nullable();
            $table->string('customer_level')->nullable();
            $table->string('customer_area')->nullable();
            $table->string('has_house')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('projects', function (Blueprint $table) {
            $table->dropColumn('customer_require');
            $table->dropColumn('customer_hobby');
            $table->dropColumn('customer_level');
            $table->dropColumn('has_house');
            $table->dropColumn('customer_area');
        });
    }
}
