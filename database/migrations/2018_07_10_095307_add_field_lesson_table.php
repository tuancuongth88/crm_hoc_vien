<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldLessonTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('education_lessons', function (Blueprint $table) {
            $table->integer('number_session')->after('description')->nullable();
            $table->integer('number_hours')->after('number_session')->nullable();
            $table->integer('parent')->after('number_hours')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('education_lessons', function (Blueprint $table) {
            $table->dropColumn('number_session');
            $table->dropColumn('number_hours');
            $table->dropColumn('parent');
        });
    }
}
